$(document).ready(function(){

	var consejo_id = $('#ConsejoEducativo_id').val();


	if(consejo_id==null){


		var data = {plantel : $('#ConsejoEducativo_plantel_id').val()};
		var direccion = 'verificarPlantel';
		var title = 'Actualizar Consejo Educativo';
	    $.ajax({
	        url: direccion,
	        data: data,
	        dataType: 'html',
	        type: 'POST',
	        success: function(datahtml, resp2, resp3)
	        {

	        	var response = jQuery.parseJSON(resp3.responseText);

	        	if(response.status == 'success'){

	        		var Dialogo= $("#dialogPantalla").removeClass('hide').dialog({
	                
	                	modal: true,
	                	width: '700px',
	                	dragable: false,
	                	resizable: false,
	                	//position: 'top',
	                	title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-pencil'></i> " + title + "</h4></div>",
		                title_html: true,
		                buttons: [
		                    {
		                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
		                        "class": "btn btn-danger btn-xs",
		                        click: function() {
	                                location.href ='/planteles/';
		                        }
		                    },
		                    {
		                        html: "<i class=' info bigger-110'></i>&nbsp; Actualizar",
		                        "class": "btn btn-primary btn-xs",
		                          click: function() {
		                            location.href = '/planteles/consejoEducativo/edicion/id/'+response.url;
		                            }
		                    }
	                ],
	                close: function() {
	                    location.href ='/planteles/';
	                }
	            });
	        	}

	               
	        }
	    });
    }
    else{
    	
    	id = $("#TaquillaConsejo_parroquia_id").val();

        if (id != "") {
        	$('#query').attr('readonly', false);

            $("#query").autocomplete({
                source: "/planteles/consejoEducativo/ViaAutoComplete?id=" + id,
                minLength: 1,
                select: function(event, ui) {
                    log(ui.item ?
                        "Selected: " + ui.item.value + " aka " + ui.item.id :
                        "Nothing selected, input was " + this.value);
                    }
                });

            }
            else {
                $('#query').attr('readonly', true);
                $("#query").autocomplete({
                source: "/planteles/consejoEducativo/ViaAutoComplete?id=" + id,
                minLength: 1,
                select: function(event, ui) {
                    log(ui.item ?
                        "Selected: " + ui.item.value + " aka " + ui.item.id :
                        "Nothing selected, input was " + this.value);
                    }
                });
            }
    }

    /* Enmascarando el RIF y el Numero de Cuenta*/
	$.mask.definitions['H'] = '[J|j|g|G]';
    $("#ConsejoEducativo_rif").mask("H-999999999");
    $('#ConsejoEducativo_numero_cuenta').mask('99999999999999999999');

    $("#ConsejoEducativo_rif").blur(function(){
    	this.value= this.value.toUpperCase();
    });

    $('#button-form').on('click',function(e){
    	
    	e.preventDefault();

    		enviarFormulario(consejo_id);
    	
    	
    });


    

    $("#TaquillaConsejo_direccion").on('keyup keydown blur',function(tecla){
    	keyTextDash(this, true, true);
    	makeUpper(this);
    });
    
    $("#ConsejoEducativo_peic").on('keyup keydown blur',function(tecla){
    	keyTextDash(this, true, true);
    	makeUpper(this);
    });

    /*********************Posee PEIC*************************************/

	$('.radio-peic').on ('change',function(){

		var valor = this.value;
		
		if(valor=='SI'){

			$('.peic').removeClass('hide'); // aparecer el div oculto en donde se ingresan los datos del peic
		}
		else{

			$('.peic').addClass('hide'); // esconder el div que contiene el input de peic	
			$('#ConsejoEducativo_peic').val(''); // limpiar el input de peic cuando elige la opcion de que no posee peic
		}
	});

	/*************************************************************/
        
/*****************Fecha Matricula***************************/
        $('#ConsejoEducativo_fecha_registro').unbind('click');
        $('#ConsejoEducativo_fecha_registro').unbind('focus');
        $('#ConsejoEducativo_fecha_registro').datepicker();
        var fecha = new Date();
        var anoActual = fecha.getFullYear();

        $.datepicker.setDefaults($.datepicker.regional['es']);
        $.datepicker.setDefaults($.datepicker.regional['es']= {
            dateFormat: 'yy-mm-dd',
            'showOn': 'focus',
            'showOtherMonths': false,
            'selectOtherMonths': true,
            'changeMonth': true,
            'changeYear': true,
            'yearRange': '1920:' + anoActual,
            minDate: new Date(1996, 1, 1),
            maxDate: 'today',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],            
        });
    /**********************************************************/

    $('#TaquillaConsejo_estado_id').on('change',function(){

    	var estado = $(this).val();

    	if(estado!=''){

    		if(consejo_id==null){
    			
    			url = 'ObtenerMunicipios';
    		}else{
    			
    			url = '../../ObtenerMunicipios';
    		}
	    	

	    	var data= { id: estado };
			$.ajax({

				url: url,
				data: data,
				dataType: 'html',
				type: 'post',
				success: function(respuesta) {
					$('#TaquillaConsejo_municipio_id').html(respuesta);					
				}
			});
		}
		else{

			$('#TaquillaConsejo_municipio_id, #TaquillaConsejo_parroquia_id, #TaquillaConsejo_poblacion_id,#TaquillaConsejo_urbanizacion_id, #TaquillaConsejo_tipo_via_id').empty();
		}
	});

    $('#TaquillaConsejo_municipio_id').on('change',function(){

    	var municipio = $(this).val();

    	if(municipio!=''){

    		if(consejo_id==null){
    			
    			url = 'ObtenerParroquias';
    		}else{

    			url = '../../ObtenerParroquias';
    		}

	    	var data= { id: municipio };
			$.ajax({

				url: url,
				data: data,
				dataType: 'html',
				type: 'post',
				success: function(respuesta) {

					$('#TaquillaConsejo_parroquia_id').html(respuesta);					
				}
			});
		}
		else{
			$('#TaquillaConsejo_parroquia_id, #TaquillaConsejo_poblacion_id,#TaquillaConsejo_urbanizacion_id, #TaquillaConsejo_tipo_via_id').empty();

		}
	});

    $('#TaquillaConsejo_parroquia_id').on('change',function(){

    	var parroquia = $(this).val();

    	if(parroquia!=''){

    		if(consejo_id==null){
    			
    			url = 'ObtenerPoblacion';
    		}else{
    			
    			url = '../../ObtenerPoblacion';
    		}

	    	var data= { id: parroquia };
			$.ajax({

				url: url,
				data: data,
				dataType: 'html',
				type: 'post',
				success: function(respuesta) {

					$('#TaquillaConsejo_poblacion_id').html(respuesta);
					$('#TaquillaConsejo_urbanizacion_id, #TaquillaConsejo_tipo_via_id').empty();					
				}
			});
		}
		else{
			
			$('#TaquillaConsejo_poblacion_id,#TaquillaConsejo_urbanizacion_id, #TaquillaConsejo_tipo_via_id').empty();
		}
	});

            $("#TaquillaConsejo_parroquia_id").change(function() {

            	id = $(this).val();

                if (id != "") {

                    $('#query').attr('readonly', false);


                    $("#query").autocomplete({
                        source: "/planteles/crear/ViaAutoComplete?id=" + id,
                        minLength: 1,
                        select: function(event, ui) {
                            log(ui.item ?
                                    "Selected: " + ui.item.value + " aka " + ui.item.id :
                                    "Nothing selected, input was " + this.value);
                        }
                    });

                }

                else {
                    $('#query').attr('readonly', true);
                    $("#query").autocomplete({
                        source: "/planteles/crear/ViaAutoComplete?id=" + id,
                        minLength: 1,
                        select: function(event, ui) {
                            log(ui.item ?
                                    "Selected: " + ui.item.value + " aka " + ui.item.id :
                                    "Nothing selected, input was " + this.value);
                        }
                    });
                }



            });
	
	$('#TaquillaConsejo_poblacion_id').on('change',function(){

    	var parroquia = $('#TaquillaConsejo_parroquia_id').val();
    	var poblacion = $(this).val();


    	if(parroquia!='' && poblacion!=''){

    		if(consejo_id==null){
    			
    			url = 'ObtenerUrbanizacion';
    		}else{

    			url = '../../ObtenerUrbanizacion';
    		}

	    	

	    	var data= { id: parroquia };
			$.ajax({

				url: url,
				data: data,
				dataType: 'html',
				type: 'post',
				success: function(respuesta) {

					$('#TaquillaConsejo_urbanizacion_id').html(respuesta);					
				}
			});
		}
		else{
			
			$('#TaquillaConsejo_urbanizacion_id, #TaquillaConsejo_tipo_via_id').empty();
		}
	});

	$('#TaquillaConsejo_urbanizacion_id').on('change',function(){

    	var parroquia = $('#TaquillaConsejo_parroquia_id').val();
    	var urbanizacion = $(this).val();


    	if(parroquia!='' && urbanizacion!=''){

    		if(consejo_id==null){
    			
    			url = 'ObtenerTipoVia';
    		}else{

    			url = '../../ObtenerTipoVia';
    		}

	    	

	    	var data= { id: parroquia };
			$.ajax({

				url: url,
				data: data,
				dataType: 'html',
				type: 'post',
				success: function(respuesta) {

					$('#TaquillaConsejo_tipo_via_id').html(respuesta);					
				}
			});
		}
		else{
			
			$('#TaquillaConsejo_tipo_via_id').empty();
		}
	});


});

function enviarFormulario(consejo_id) 
{
    var data = $('*').serialize();
    var divResult='';

    if(consejo_id==null){
    	var urlDir = 'registro';
    }else{
    	var urlDir = '';
    }
    
    var loadingEfect=true;
    var showResult=true;
    var method='POST';
    var responseFormat='html';

    var beforeSendCallback=function(){

            $('#'+divResult).html('');

    };
    
    var successCallback;
    
    var errorCallback=function(){};
    
    //executeFormatedAjax(divResult, urlDir, data, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback)
    
    successCallback=function(datahtml, resp2, resp3) {

    	$('*').scrollTop(100);
    	//alert(datahtml);

                            try {
                                var response = jQuery.parseJSON(resp3.responseText);
                                

                                 if(response.status =='error'){


                                 	divResult='div-result';
                                    displayDialogBox(divResult, response.status, response.mensaje,true,true);
                        
                                }else if(response.status =='success'){

                                	//alert(response.url+'/edicion/id/'+response.id);

                                	location.href =response.url+'/planteles/ConsejoEducativo/edicion/id/'+response.id;

                                }
                    
                                
                            } catch (e) {
                                
                                	//alert('mensaje');
                                	divResult='div-result';
                                    displayHtmlInDivId(divResult, datahtml,true);
                                    //$('*').scrollTop(50);
                               
                                }
                   
                };

    executeFormatedAjax(divResult, urlDir, data, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
    //$('#proyecto-consejo-form').reset(); 
}

function log(message) {
    $("<div>").text(message).prependTo("#log");
    $("#log").scrollTop(0);
}