/**
 * Created by isalaz01 on 29/09/14.
 */
function dialog_success(mensaje) {
    $("#dialog_success p").html(mensaje);
    var dialog_success = $("#dialog_success").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-check'></i> Actualización Exitosa </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    dialog_success.dialog("close");
                    window.location.reload();

                }
            }
        ]

    });
}
function dialogo_error(mensaje,title,redireccionar) {
    displayDialogBox('dialog_error','info',mensaje);
    //$("#dialog_error p").html(mensaje);
    var dialog = $("#dialog_error").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> "+title+" </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if(redireccionar){
                        window.location.reload();
                    }
                }
            }
        ]
    });
}
function editarAsignaturaDocente(e,id){
    e.preventDefault();
    var asignatura_docente_id=id;
    var divResult='dialog_docente';
    var urlDir='/planteles/docente/asignaturaDocente/';
    var conEfecto=true;
    var showHTML = true;
    var method ='GET';
    var callback;
    var datos = {'id':asignatura_docente_id};

    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);

    $("#"+divResult).removeClass('hide').dialog({
        width: 1100,
        resizable: false,
        draggable: false,
        position: ['center', 50],
        modal: true,
        title: "<div class='widget-header'><h4 class='smaller blue'><i class='icon-search'></i> Edición de Asignatura </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            }
        ],
        close: function() {

            $("#"+divResult).html("");
            $("#"+divResult).addClass("hide");
            $(this).dialog("close");

        }
    });
}

function liberarAsignaturaDocente(e,id){
    e.preventDefault();
    var asignatura_docente_id=id;
    var divResult='dialog_docente';
    var urlDir='/planteles/docente/liberarDocente/';
    var conEfecto=true;
    var showHTML = true;
    var method ='GET';
    var callback;
    var datos = {'id':asignatura_docente_id};

    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);

    $("#"+divResult).removeClass('hide').dialog({
        width: 1100,
        resizable: false,
        draggable: false,
        position: ['center', 50],
        modal: true,
        title: "<div class='widget-header'><h4 class='smaller blue'><i class='icon-search'></i> Liberar de Asignatura </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            }
        ],
        close: function() {

            $("#"+divResult).html("");
            $("#"+divResult).addClass("hide");
            $(this).dialog("close");

        }
    });
}
function cambiarEstatusProcesoDocentes(id, accion) {
    var accionDes = new String();
    var boton = new String();
    var botonClass = new String();

    $("#resultadoElim").addClass('hide');
    $("#resultadoElim").html('');
    if (accion == 'A') {
        accionDes = 'Cerrar';
        boton = "<i class='icon-ok bigger-110'></i>&nbsp; Cerrar Proceso de Asignación de Docentes";
        botonClass = 'btn btn-primary btn-xs';
    } else {
        accionDes = 'Aperturar';
        boton = "<i class='icon-trash bigger-110'></i>&nbsp; Aperturar Proceso de Asignación de Docentes";
        botonClass = 'btn btn-danger btn-xs';
    }
    $(".confirm-action").html(accionDes);

    $("#confirm-status").removeClass('hide').dialog({
        width: 800,
        resizable: false,
        draggable: false,
        modal: true,
        position: ['center', 50],
        title: "<div class='widget-header'><h4 class='smaller'><i class='icon-warning-sign red'></i> Cambio de Estatus del Proceso de Asignación de Docentes</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: boton,
                "class": botonClass,
                click: function() {

                    var divResult = "div-result-message";
                    var urlDir = "/planteles/docente/cambiarEstatusAsignacionDocente/";
                    var datos = {accion: accion, seccion_plantel_id: id};
                    var conEfecto = true;
                    var showHTML = true;
                    var method = "POST";
                    var callback = function() {
                        //refrescarGrid();
                    };

                    $.ajax({
                        type: method,
                        url: urlDir,
                        dataType: "html",
                        data: datos,
                        beforeSend: function() {
                            if (conEfecto) {
                                var url_image_load = "<div class='center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
                                displayHtmlInDivId(divResult, url_image_load);
                            }
                        },
                        success: function(datahtml, resp2, resp3) {

                            try {

                                var json = jQuery.parseJSON(resp3.responseText);
                                if (json.statusCode == 'success') {
                                    $("#confirm-status").dialog('close');
                                    dialog_success(json.mensaje);
                                }
                            } catch (e) {
                                $("html, body").animate({scrollTop: 0}, "fast");
                                if (showHTML) {
                                    $("#resultadoElim").removeClass('hide');
                                    displayHtmlInDivId('resultadoElim', datahtml, conEfecto);
                                }
                            }

                        },
                        statusCode: {
                            404: function() {
                                displayDialogBox(divResult, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            },
                            400: function() {
                                displayDialogBox(divResult, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
                            },
                            401: function() {
                                displayDialogBox(divResult, "error", "401: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            403: function() {
                                displayDialogBox(divResult, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            500: function() {
                                if (typeof callback == "function")
                                    callback.call();
                                displayDialogBox(divResult, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            },
                            503: function() {
                                displayDialogBox(divResult, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            },
                            999: function(resp) {
                                displayDialogBox(divResult, "error", resp.status + ': ' + resp.responseText);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            //alert(thrownError);
                            if (xhr.status == '401') {
                                document.location.href = "http://" + document.domain + "/";
                            } else if (xhr.status == '400') {
                                displayDialogBox(divResult, "error", "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            } else if (xhr.status == '500') {
                                displayDialogBox(divResult, "error", "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            } else if (xhr.status == '503') {
                                displayDialogBox(divResult, "error", "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            }
                            else if (xhr.status == '999') {
                                displayDialogBox('resultadoElim', "error", xhr.status + ': ' + xhr.responseText);
                            }
                        }
                    });

                    $(this).dialog("close");

                }
            }

        ]
    });

}

$(document).ready(function() {

    $('.edit-docente').unbind('click');
    $('.edit-docente').bind('click', function (e) {
        var id= $(this).attr('data-id');
        editarAsignaturaDocente(e,id);
    });

    $('.reset-asignatura').unbind('click');
    $('.reset-asignatura').bind('click', function (e) {
        var id= $(this).attr('data-id');
        liberarAsignaturaDocente(e,id);
    });
    $('.change-status').unbind('click');
    $('.change-status').on('click',
        function(e) {
            e.preventDefault();
            var id = $("#seccion_plantel_id").val();
            var accion = $(this).attr('data-action');
            cambiarEstatusProcesoDocentes(id, accion);
        }
    );
});
