////////////////  
function VentanaDialog(id,direccion,title,accion,datos)
{

    accion=accion;
    Loading.show();
    var data =
    {
        id: id,
        datos:datos
    };




    if(accion=="borrarPersonalPlantel")
    {

        $("#dialogPantallaAdmin").html('<div class="alert alert-warning"> ¿Esta seguro que desea inactivar este registro?</div>');

        var dialog = $("#dialogPantallaAdmin").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,


            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='icon-book'></i> " + title + "</h4></div>",
            title_html: true,

            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function()
                    {
                        $(this).dialog("close");
                    } // fin de la funcion click
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar ",
                    "class": "btn btn-danger btn-xs",
                    click: function()
                    {
                        var divResult = "resultadoOperacionAdmin";
                        var urlDir = "/planteles/estructura/"+accion+"/";
                        var datos = {id:id, accion:accion};
                        var conEfecto = true;
                        var showHTML = true;
                        var method = "POST";
                        var callback = function(){
                            $('#personal-plantel-grid').yiiGridView('update', {
                                data: $(this).serialize()
                            });
                        };

                        $("html, body").animate({ scrollTop: 0 }, "fast");
                        if(datos){
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                        }
                    } // fin de la funcion click
                }
            ],

        });


        Loading.hide();


    }

    if(accion=="activarPersonalPlantel")
    {

        $("#dialogPantallaAdmin").html('<div class="alertDialogBox"><p class="bolder center grey"> ¿Esta seguro que desea activar este registro? </p></div>');

        var dialog = $("#dialogPantallaAdmin").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,


            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + "</h4></div>",
            title_html: true,

            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-check bigger-110'></i>&nbsp; Reactivar",
                    "class": "btn btn-success btn-xs",
                    click: function() {
                        var divResult = "resultadoOperacionAdmin";
                        var urlDir = "/planteles/estructura/"+accion+"/";
                        var datos = {id:id, accion:accion};
                        var conEfecto = true;
                        var showHTML = true;
                        var method = "POST";
                        var callback = function(){
                            $('#personal-plantel-grid').yiiGridView('update', {
                                data: $(this).serialize()
                            });
                        };

                        $("html, body").animate({ scrollTop: 0 }, "fast");
                        if(datos){
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                        }
                    }
                }
            ], // botones


        });


        Loading.hide();


    }

} // fin de la funcion para activacion i desactivacion de registros en la interfaz admin perteneciente a personal-plantel







function cerrarDialog()
{
    $("#dialogoVentana").dialog("close");



} // fin de la funcion cerrar la ventana de dialogo // 


$(document).ready(function(){


    $('#btnCerrarProcesoRegistroPersonal').click(function() {



        var plantel_id=$("#admin_plantel_id").val();
        var periodo_id=$("#admin_periodo_id").val();


        $("#dialogPantallaAdmin").html('<div class="alertDialogBox"><p class="bolder center grey"> ¿Esta seguro que desea cerrar el proceso de registro personal para el actual periodo? </p></div>');

        var accion="cerrarProcesoRegistroPersonal";
        var title="Cerrar Proceso de Registro de Personal para el actual periodo";
        var dialog = $("#dialogPantallaAdmin").removeClass('hide').dialog({
            modal: true,
            width: '600px',
            dragable: false,
            resizable: false,


            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + "</h4></div>",
            title_html: true,

            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-check bigger-110'></i>&nbsp; Cerrar Proceso de Registro",
                    "class": "btn btn-success btn-xs",
                    click: function() {
                        var divResult = "resultadoOperacionAdmin";
                        var urlDir = "/planteles/estructura/"+accion+"/";
                        var datos = {plantel_id:plantel_id,periodo_id:periodo_id, accion:accion};
                        var conEfecto = true;
                        var showHTML = true;
                        var method = "POST";
                        var callback = function()
                        {
                            window.location.href = '/planteles/estructura/lista/id/'+plantel_id;

                        };

                        $("html, body").animate({ scrollTop: 0 }, "fast");
                        if(datos){
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                        }
                    }
                }
            ], // botones


        });


        Loading.hide();



    }); // fin de la funcionalidad cuando dan click a la opcion de cerrar proceso






    $('#btnApeturarProcesoRegistroPersonal').click(function() {



        var plantel_id=$("#admin_plantel_id").val();
        var periodo_id=$("#admin_periodo_id").val();


        $("#dialogPantallaAdmin").html('<div class="alertDialogBox"><p class="bolder center grey"> ¿Esta seguro que desea aperturar el proceso de registro personal para el actual periodo? </p></div>');

        var accion="aperturarProcesoRegistroPersonal";
        var title="Aperturar Proceso de Registro de Personal para el actual periodo";
        var dialog = $("#dialogPantallaAdmin").removeClass('hide').dialog({
            modal: true,
            width: '600px',
            dragable: false,
            resizable: false,


            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + "</h4></div>",
            title_html: true,

            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-check bigger-110'></i>&nbsp; Aperturar Proceso de Registro",
                    "class": "btn btn-success btn-xs",
                    click: function() {
                        var divResult = "resultadoOperacionAdmin";
                        var urlDir = "/planteles/estructura/"+accion+"/";
                        var datos = {plantel_id:plantel_id,periodo_id:periodo_id, accion:accion};
                        var conEfecto = true;
                        var showHTML = true;
                        var method = "POST";
                        var callback = function()
                        {
                            window.location.href = '/planteles/estructura/lista/id/'+plantel_id;

                        };

                        $("html, body").animate({ scrollTop: 0 }, "fast");
                        if(datos){
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                        }
                    }
                }
            ], // botones


        });


        Loading.hide();



    }); // fin de la funcionalidad cuando dan click a la opcion de aperturar proceso



//  


}); // fin del document.ready 
