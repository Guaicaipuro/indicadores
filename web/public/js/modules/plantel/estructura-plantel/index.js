/**
 * Created by isalaz01 on 29/09/14.
 */
$(document).ready(function() {
    $('#DocentePlantel_documento_identidad').unbind('keyup');
    $('#DocentePlantel_documento_identidad').bind('keyup', function () {
        makeUpper(this);
        keyAlphaNum(this, false, false);
    });
    $('#DocentePlantel_tdocumento_identidad').unbind('keyup');
    $('#DocentePlantel_tdocumento_identidad').bind('keyup', function () {
        makeUpper(this);
        keyText(this,false);
    });
    $('#DocentePlantel_nombres').unbind('keyup');
    $('#DocentePlantel_nombres').bind('keyup', function () {
        makeUpper(this);
        keyAlphaNum(this, false, false);
    });
    $('#DocentePlantel_apellidos').unbind('keyup');
    $('#DocentePlantel_apellidos').bind('keyup', function () {
        makeUpper(this);
        keyAlphaNum(this, false, false);
    });

    $('#DocentePlantel_documento_identidad').unbind('blur');
    $('#DocentePlantel_documento_identidad').bind('blur', function () {
        clearField(this);
    });
    $('#DocentePlantel_tdocumento_identidad').unbind('blur');
    $('#DocentePlantel_tdocumento_identidad').bind('blur', function () {
        clearField(this);
    });
    $('#DocentePlantel_nombres').unbind('blur');
    $('#DocentePlantel_nombres').bind('blur', function () {
        clearField(this);
    });
    $('#DocentePlantel_apellidos').unbind('blur');
    $('#DocentePlantel_apellidos').bind('blur', function () {
        clearField(this);
    });
});
