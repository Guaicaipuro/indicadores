/**
 * Created by ignacio on 12/09/14.
 */
function dialogo_error(mensaje,title,redireccionar) {
    displayDialogBox('dialog_error','info',mensaje);
    //$("#dialog_error p").html(mensaje);
    var dialog = $("#dialog_error").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> "+title+" </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if(redireccionar){
                        window.location.reload();
                    }
                }
            }
        ]
    });
}



function consultarCongresoP(id) {

    direccion = '/planteles/modificar/informacionCongreso';
    title = 'Detalles del Congreso Pedagogico';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantallaConsultar").html(result);
            var dialog = $("#dialogPantallaConsultar").removeClass('hide').dialog({
                modal: true,
                width: '1000px',
                //height: '800px',
                draggable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>" + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGridCongreso();
                            $(this).dialog("close");
                        }

                    }

                ]
            });
            
        }
    });
    Loading.hide();
}

function editarCongresoP(id) {

    direccion = '/planteles/modificar/editarCongreso';
    title = 'Congreso Pedagogico - Editar';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantallaEditar").removeClass('hide').dialog({
                modal: true,
                width: '1000px',
                //height: '800px',
                draggable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>" + title + "</h4></div>",
                title_html: true,
                buttons: [
                        {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGridCongreso();
                            $(this).dialog("close");}
                        },
                        {

                        html: "<i class='icon-save-left bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        "type": "submit",
                        click: function() {
                            var divResult = "_form";
                            var urlDir = "updateCongreso";
                            var datos = $('#congreso-editar-form').serialize();
                            var conEfecto = true;
                            var showHTML = true;
                            var method = "POST";

                            executeAjaxRequest(divResult, urlDir, datos, conEfecto, showHTML,method, refrescarGridCongreso);

                            refrescarGridCongreso();
                            $(this).dialog("close");
                        }

                        }

                ]
            });
            $("#dialogPantallaEditar").html(result);
        }
    });
    Loading.hide();
}

function editarDocente(id) {

    direccion = '/planteles/modificar/editarDocente';
    title = 'Congreso Pedagogico - Editar Docente';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantallaEditar").html(result);
            var dialog = $("#dialogPantallaEditar").removeClass('hide').dialog({
                modal: true,
                width: '1000px',
                //height: '800px',
                draggable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>" + title + "</h4></div>",
                title_html: true,
                buttons: [
                        {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGridDocente();
                            $(this).dialog("close");}
                        },
                        {

                        html: "<i class='icon-save-left bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        "type": "submit",
                        click: function() {
                            var divResult = "respEdicion";
                            var urlDir = "/planteles/modificar/updateDocente";
                            var datos = $('#docente-editar-form').serialize();
                            var loadingEfect = true;
                            var showResult = true;
                            var method = "POST";
                            var responseFormat = "html";
                            var successCallback;
                            $('#'+divResult).html('');
                            $('#'+divResult).addClass('hide');

                            successCallback = function(){
                                $('#'+divResult).removeClass('hide');
                                refrescarGridDocente();
                            }
                            executeAjaxRequest(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback);


                            $(this).dialog("close");
                        }

                        }

                ]
            });

        }
    });
    Loading.hide();
}




function eliminarCongreso(id) {
    var dialog = $("#dialogPantallaEliminar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggale: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Congreso</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id,
                        //plantel_id: $("#plantel_id").val()
                    };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('_form', 'eliminarCongreso', data, true, true, 'GET', refrescarGridCongreso);
                    $(this).dialog("close");
                }
            }
        ]
    });
}


function eliminarDocente(id) {
    var dialog = $("#dialogPantallaEliminarDocente").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggale: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Docente</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id,

                        //plantel_id: $("#plantel_id").val()
                    };
                    
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('_form', '/planteles/modificar/eliminarDocente', data, true, true, 'GET', refrescarGridDocente);
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function activarCongreso(id) {
    var dialog = $("#dialogPantallaActivar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggale: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Congreso</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-check bigger-110'></i>&nbsp; Activar",
                "class": "btn btn-primary btn-xs",
                click: function() {
                    var data = {
                        id: id,
                        //plantel_id: $("#plantel_id").val()
                    };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('_form', 'activarCongreso', data, true, true, 'GET', refrescarGridCongreso);
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function activarDocente(id) {
    var dialog = $("#dialogPantallaActivar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggale: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Docente</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-check bigger-110'></i>&nbsp; Activar",
                "class": "btn btn-primary btn-xs",
                click: function() {
                    var data = {
                        id: id,
                        //plantel_id: $("#plantel_id").val()
                    };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('_form', '/planteles/modificar/activarDocente', data, true, true, 'GET', refrescarGridDocente);
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function refrescarGridCongreso() {
    var data = {
        plantel_id: $("#plantel_id").val()
    };
    $('#cong-ped-grid').yiiGridView('update', {
        data:data
    });
}

function refrescarGridDocente() {
    var data = {
        plantel_id: $("#plantel_id").val()
    };
    $('#docente-grid').yiiGridView('update', {
        data:data
    });
}



$(document).ready(function(){
    $('.data-doc-ident').unbind('keyup');
    $('.data-doc-ident').bind('keyup', function() {
        keyAlphaNum(this,false,false);
        clearField(this);
        makeUpper(this);
    });
    $('.data-doc-ident').unbind('blur');
    $('.data-doc-ident').bind(' blur', function() {
        var documento_identidad = $(this).val();
        var index_input = base64_decode($(this).attr('data-id'));
        var id_nombres;
        var id_apellidos;
        var id_tdocumento_identidad;
        var tdocumento_identidad;
        var title;
        var mensaje;
        var divResult='';
        var urlDir = '/planteles/modificar/obtenerDatosPersona';
        var datos;
        var loadingEfect=false;
        var showResult=false;
        var method='POST';
        var responseFormat='json';
        var beforeSendCallback=function(){};
        var successCallback;
        var errorCallback=function(){};

        if(documento_identidad !=null && documento_identidad != '' ){
            id_nombres='CongresoPedagogicoDocente_'+index_input+'_nombres';
            id_apellidos='CongresoPedagogicoDocente_'+index_input+'_apellidos';
            id_tdocumento_identidad='CongresoPedagogicoDocente_'+index_input+'_tdocumento_identidad';
            tdocumento_identidad = $('#'+id_tdocumento_identidad).val();
            if(tdocumento_identidad !=null && tdocumento_identidad != '' ){
                datos=$('#congreso-form').serialize()+'&documento_identidad='+documento_identidad+'&tdocumento_identidad='+tdocumento_identidad+'&index='+base64_encode(index_input);
                successCallback=function(response){
                    if(response.statusCode =='SUCCESS'){
                        $('#'+id_apellidos).val(response.apellidos);
                        $('#'+id_nombres).val(response.nombres);
                    }
                    if(response.statusCode =='ERROR'){
                        dialogo_error(response.mensaje,response.title);
                    }
                };
                executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
            }
            else {
                title = 'Notificación de Error';
                mensaje='Estimado usuario debe seleccionar un Tipo de Documento de Identidad Valido.';
                dialogo_error(mensaje,title);
            }

        }
    });
    //Array para dar formato en español
    $.datepicker.regional['es'] =
    {
        closeText: 'Cerrar',
        prevText: 'Previo',
        nextText: 'Próximo',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
            'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        dateFormat: 'dd-mm-yy',
        'showOn': 'focus',
        'showOtherMonths': false,
        'selectOtherMonths': true,
        'changeMonth': true,
        'changeYear': true,
        minDate: 'today',
        maxDate: '+1Y',
        initStatus: 'Selecciona la fecha', isRTL: false};
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $('#CongresoPedagogico_fecha_inicio').datepicker();

    //Array para dar formato en español
    $.datepicker.regional['es'] =
    {
        closeText: 'Cerrar',
        prevText: 'Previo',
        nextText: 'Próximo',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
            'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        dateFormat: 'dd-mm-yy',
        'showOn': 'focus',
        'showOtherMonths': false,
        'selectOtherMonths': true,
        'changeMonth': true,
        'changeYear': true,
        minDate: 'today',
        maxDate: '+0D +0M +1Y',
        initStatus: 'Selecciona la fecha', isRTL: false};
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $('#CongresoPedagogico_fecha_fin').datepicker();

    $("#btnGuardarCongreso").click(function(event){
        event.preventDefault();
        var divResult='resultadoCongreso';
        var urlDir = '/planteles/modificar/guardarDatosCongreso';
        var datos;
        var loadingEfect=true;
        var showResult=false;
        var method='POST';
        var responseFormat='html';
        var beforeSendCallback=function(){};
        var successCallback;
        var errorCallback=function(){};
        var plantel_id = $("#plantel_id").val();
        $("#"+divResult).addClass('hide');
        $("#"+divResult).html('');
        successCallback=function(response,statusCode,dom){
            try{
                var json = jQuery.parseJSON(dom.responseText);
                if(json.statusCode =='SUCCESS'){
                    //redireccionar
                    dialogo_error(json.mensaje,json.title,true);
                }
                if(json.statusCode =='ERROR'){
                    dialogo_error(json.mensaje,json.title);
                }
            }
            catch(e){
                $("#"+divResult).removeClass('hide');
                displayHtmlInDivId(divResult,response,true);
                $("html, body").animate({scrollTop: 0}, "fast");
            }
        };
        datos=$('#congreso-form').serialize()+'&'+$('#congreso-datos-form').serialize()+'&plantel_id='+base64_encode(plantel_id);
        executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
    });
});
