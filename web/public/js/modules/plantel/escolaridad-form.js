/**
 * Created by isalaz01 on 05/06/15.
 */
$(document).ready(function(){
    $('#EscolaridadForm_documento_identidad').unbind('keyup');
    $('#EscolaridadForm_documento_identidad').bind('keyup', function() {
        keyAlphaNum(this,false,false);
        makeUpper(this);
    });
    $('#EscolaridadForm_documento_identidad').unbind('blur');
    $('#EscolaridadForm_documento_identidad').bind('blur', function() {
        clearField(this);
        var documento_identidad = $(this).val();
        var id_nombres;
        var id_apellidos;
        var id_tdocumento_identidad;
        var tdocumento_identidad;
        var periodo_id;
        var title='Notificación de Error';
        var mensaje;
        var divResult='';
        var urlDir = '/planteles/escolaridad/buscarEstudiante';
        var datos;
        var loadingEfect=false;
        var showResult=false;
        var method='GET';
        var responseFormat='json';
        var beforeSendCallback=function(){};
        var successCallback;
        var errorCallback=function(){};

        if(documento_identidad !=null && documento_identidad != '' ){
            id_nombres='EscolaridadForm_nombres';
            id_apellidos='EscolaridadForm_apellidos';
            id_tdocumento_identidad='EscolaridadForm_tdocumento_identidad';
            id_documento_identidad='EscolaridadForm_documento_identidad';
            id_periodo='EscolaridadForm_periodo_id';
            id_estudiante='EscolaridadForm_estudiante_id';
            tdocumento_identidad = $('#'+id_tdocumento_identidad).val();
            periodo_id = $('#'+id_periodo).val();
            if(tdocumento_identidad !=null && tdocumento_identidad != '' ){
                datos= {
                    'tdocumento_identidad':tdocumento_identidad,
                    'documento_identidad':documento_identidad,
                    'periodo_id':periodo_id
                };
                successCallback=function(response){
                    if(response.statusCode =='SUCCESS'){
                        $('#'+id_apellidos).val(response.datosEstudiante.apellidos);
                        $('#'+id_nombres).val(response.datosEstudiante.nombres);
                        $('#'+id_estudiante).val(response.datosEstudiante.id);
                    }
                    if(response.statusCode =='ERROR'){
                        $('#'+id_documento_identidad).val('');
                        $('#'+id_apellidos).val('');
                        $('#'+id_nombres).val('');
                        $('#'+id_estudiante).val('');
                        dialogo_error(response.mensaje,title);
                    }
                };
                executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
            }
            else {
                title = 'Notificación de Error';
                mensaje='Estimado usuario debe seleccionar un Tipo de Documento de Identidad Valido.';
                dialogo_error(mensaje,title);
            }

        }
    });
});
function dialogo_error(mensaje,title,redireccionar) {
    displayDialogBox('dialog_error','info',mensaje);
    //$("#dialog_error p").html(mensaje);
    var dialog = $("#dialog_error").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> "+title+" </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if(redireccionar){
                        window.location.reload();
                    }
                }
            }
        ]
    });
}