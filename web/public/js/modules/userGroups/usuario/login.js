$(document).ready(function(){
    $("#linkRefreshCaptcha").on('click', function (){
        reloadCaptcha();
    });

    if (document.layers) {
        document.captureEvents(Event.KEYDOWN);
    }

    document.onkeydown = function (evt) {
        var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
        if (keyCode == 13) {
            $("#btnLogin").click();
        }
    };
    $("#UserGroupsUser_username").val("");
    $("#UserGroupsUser_password").val("");
    $("#UserGroupsUser_verifyCode").val("");

    $("#btnLogin").on('click', function (){
        var m;
        var n;
        var key;
        var keyDecoded;
        var horaKeyEncoded;
        var hora;
        var username;
        var password;
        var verifyCode;

        verifyCode= $("#UserGroupsUser_verifyCode").val();
        username= $("#UserGroupsUser_username").val();
        password= $("#UserGroupsUser_password").val();
        if(username.length>0 && password.length>1){
            key= $("#th").val();
            keyDecoded=base64_decode(key);
            horaKeyEncoded = keyDecoded.split('-');
            hora = horaKeyEncoded[0];
            key = horaKeyEncoded[1];
            keyDecoded=base64_decode(key);
            horaDecoded=base64_decode(hora);

            m = base64_encode(crypData(key,password));
            n = base64_encode(crypData(key,username));

            $('#ik').val(m);
            $('#prf').val(n);
            $("#UserGroupsUser_username").val(randomNumberFromRange(2000000,3100000));
            $("#UserGroupsUser_password").val(base64_encode(password));
            $("#mb").val(base64_encode(randomNumberFromRange(2000000,3100000)));
            $("#rid").val(base64_encode(randomNumberFromRange(2000000,3100000)));
            $("#rt").val(base64_encode(randomNumberFromRange(2000000,3100000)));
            $("#nsc").val(base64_encode(randomNumberFromRange(2000000,3100000)));
            $("#ui").val(base64_encode(randomNumberFromRange(2000000,3100000)));
        }
        $('#login-form').submit();

    });
});

function randomNumberFromRange(min,max)
{
    var randomNumber = Math.floor(Math.random()*(max-min+1)+min);
    return randomNumber;
}
function crypData(key,encoded_data){
    var key_length = key.length;
    var length = encoded_data.length;
    var result = '';
    var tmp = '';

    for (i = 0; i < length; i++) {
        tmp = encoded_data[i];

        for (j = 0; j < key_length; j++) {
            tmp = chr(ord(tmp) ^ ord(key[j]));
        }

        result = result+tmp;
    }
    return result;
}
function getFormObj(formId) {
    var formObj = {};
    var inputs = $('#'+formId).serializeArray();
    $.each(inputs, function (i, input) {
        formObj[input.name] = input.value;
    });
    return formObj;
}
function reloadCaptcha(){
    jQuery('#siimage').attr('src', '/login/captcha/sid/' + Math.random());
    $("#UserGroupsUser_verifyCode").val("");
}