$(document).ready(function() {
           $('#EspecificacionEstatus_nombre').bind('keyup blur', function() {
                keyText(this, true);

            });
            $('#EspecificacionEstatus_nombre').bind('blur', function() {
                clearField(this);
            });

    $('#EspecificacionEstatus_fecha_ini').datepicker();
    $.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: 'dd-mm-yy',
        showOn: 'focus',
        showOtherMonths: false,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        minDate: new Date(1800, 1, 1),
        maxDate: 'today'
    });
    

    
    $('#EspecificacionEstatus_fecha_ini').on('dblclick', function(){
        $(this).val('');
        $('#especificacion-estatus-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
    });
    
});
   function VentanaDialog(id, direccion, title, accion, datos) {
    accion = accion;
    Loading.show();
    var data =
            {
                id: id,
                datos: datos
            };
    if (accion == "create") {
        $.ajax({
            url: direccion,
            data: data,
            dataType: 'html',
            type: 'GET',
            success: function(result, action)
            {
                var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                    modal: true,
                    width: '700px',
                    dragable: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                            "class":"btn btn-danger",
                            click: function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                            html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar",
                            "class": "btn btn-primary",
                            /*executeAjax('_formEndogeno', '../../agregarProyecto', data, false, true, 'post', '');*/

                            click: function() {
                             
                                var divResult = "dialogPantalla";
                                var urlDir = "/catalogo/especificacionEstatus/registro"//;?id=" + id;
                                //
                                var datos = $("#especificacion-estatus-form").serialize();
                                var conEfecto = true;
                                var showHTML = true;
                                var method = "POST";
                                var callback = function() {
                                    $('#especificacion-estatus-grid').yiiGridView('update', {
                                        data: $(this).serialize()
                                    });
                                };
                                  var EspecificacionEstatus_nombre = $.trim($('#EspecificacionEstatus_nombre').val());
                                    if(EspecificacionEstatus_nombre==" "){
                                      
                                    }else if (datos) {
                                        
                                    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                                    }
                                }
                            
                        },
                    ],
                });
                $("#dialogPantalla").html(result);
                $('#dialogPantalla').dialog('option', 'position', 'center');
            }
        });
        Loading.hide();
    }
    else if (accion=="update"){
    $.ajax({
            url: direccion,
            data: data,
            dataType: 'html',
            type: 'GET',
            success: function(result, action)
            {
                var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                    modal: true,
                    width: '700px',
                    dragable: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                            "class": "btn btn-danger",
                            click: function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                            html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar",
                            "class": "btn btn-primary",
                            /*executeAjax('_formEndogeno', '../../agregarProyecto', data, false, true, 'post', '');*/

                            click: function() {
                             
                                var divResult = "dialogPantalla";
                                var urlDir = "/catalogo/especificacionEstatus/edicion?id=" + id;
                                //
                                var datos = $("#especificacion-estatus-form").serialize();
                                var conEfecto = true;
                                var showHTML = true;
                                var method = "POST";
                                var callback = function() {
                                    $('#especificacion-estatus-grid').yiiGridView('update', {
                                        data: $(this).serialize()
                                    });
                                };
                                  var EspecificacionEstatus_nombre = $.trim($('#EspecificacionEstatus_nombre').val());
                                    if(EspecificacionEstatus_nombre==""){
                                        
                                    }else if (datos) {
                                        
                                    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                                    }
                                }
                            
                        },
                    ],
                });
                $("#dialogPantalla").html(result);
                $('#dialogPantalla').dialog('option', 'position', 'center');
            }
        });
        Loading.hide();
       }
    else if (accion == "view") {
        $.ajax({
            url: direccion,
            data: data,
            dataType: 'html',
            type: 'GET',
            success: function(result, action)
            {
                var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                    modal: true,
                    width: '700px',
                    dragable: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                            "class": "btn btn-danger",
                            click: function() {
                                $(this).dialog("close");
                            }
                        },
                    ],
                });

                $("#dialogPantalla").html(result);
                $('#dialogPantalla').dialog('option', 'position', 'center');
            }
        });
        Loading.hide();

    }
    else if (accion == "borrar") {

        //$("#dialogPantalla").html('¿Desea Inhabilitar este Especificación de Estatus?');
        //$("#dialogPantalla").html();

        var dialog = $("#dialogInhabilitar").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,           
            title: "<div class='widget-header'><h4 class='smaller'><i class='icon-warning-sign red'></i>"  + title + "</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Inhabilitar Especificación de Estatus",
                    "class": "btn btn-danger btn-xs",
                    click: function() {
                        var divResult = "resultadoOperacion";
                        var urlDir = "/catalogo/especificacionEstatus/eliminacion?id=" + id;
                        var datos = {id: id, accion: accion};
                        var conEfecto = true;
                        var showHTML = true;
                        var method = "POST";
                        var callback = function() {
                            $('#especificacion-estatus-grid').yiiGridView('update', {
                                data: $(this).serialize()
                            });
                        };

                        $("html, body").animate({scrollTop: 0}, "fast");
                        if (datos) {
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                        }
                    }
                }

            ],
        });
        Loading.hide();

    }
    else if (accion == "activar") {
        //$("#dialogPantalla").html('Estas seguro que desea activarlo?');
        var dialog = $("#dialogActivar").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Activar Especificación de Estatus",
                    "class": "btn btn-danger btn-xs",
                    click: function() {
                        var divResult = "resultadoOperacion";
                        var urlDir = "/catalogo/especificacionEstatus/activar?id=" + id;
                        var datos = {id: id, accion: accion};
                        var conEfecto = true;
                        var showHTML = true;
                        var method = "POST";
                        var callback = function() {
                            $('#especificacion-estatus-grid').yiiGridView('update', {
                                data: $(this).serialize()
                            });
                        };

                        $("html, body").animate({scrollTop: 0}, "fast");
                        if (datos) {
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                        }
                    }
                }
            ],
        });
        Loading.hide();
    }
}