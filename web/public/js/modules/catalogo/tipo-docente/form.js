/**
 * Created by ignacio on 22/09/14.
 */
$(document).ready(function(){
    $('#TipoDocente_nombre').unbind('keyup');
    $('#TipoDocente_nombre').bind('keyup', function() {
        keyAlphaNum(this,true,false);
        makeUpper(this);
    });
    $('#TipoDocente_nombre').unbind('blur');
    $('#TipoDocente_nombre').bind('blur', function() {
        clearField(this);
    });
});