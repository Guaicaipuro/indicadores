$(document).ready(
	function(){
		//alert('hola mundo');

		$('#newRegister').on('click',
			function(e){
				
                e.preventDefault();
                registrarProyecto();
            }
        );

        $('#ProyectoConsejo_nombre').bind('keyup keydown blur', function() {
            
            keyTextDash(this, true, true);
            makeUpper(this);
            
        });

    }
);
//VENTANA DE DIALOGO PARA REGISTRAR UN PROYECTO
function registrarProyecto() {
    
    direccion = 'create';
    title = 'Crear Nuevo Proyecto';
    //Loading.show();
    
    $.ajax({
        url: direccion,
         //data: data,
        dataType: 'html',
        type: 'POST',
        success: function(result)
        {
            var Dialogo= $("#dialogPantalla").removeClass('hide').dialog({
                
                modal: true,
                width: '700px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-pencil'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                          click: function() {
                            crearProyecto();
                            //Dialogo.dialog('close');

                            }

                    }

                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
               
        }
    });
    //Loading.hide();
}

//FUNCION QUE ENVIA LA INFORMACION AL CONTROLLADOR PARA REGISTRAR UN PROYECTO NUEVO
function crearProyecto() 
{
    var data = $('#proyecto-consejo-form').serialize();
    var divResult='div-result';
    var urlDir = 'registro';
    var loadingEfect=true;
    var showResult=true;
    var method='POST';
    var responseFormat='html';

    var beforeSendCallback=function(){

            $('#'+divResult).html('');

    };
    
    var successCallback;
    
    var errorCallback=function(){};
    
    //executeFormatedAjax(divResult, urlDir, data, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback)
    
    successCallback=function(datahtml, resp2, resp3) {

                            try {
                                var response = jQuery.parseJSON(resp3.responseText);
                                

                                 if(response.status =='success' || response.status =='error'){

                                    if(response.status =='success') refrescarGrid();

                                    displayDialogBox(divResult, response.status, response.mensaje,true,true);
                        
                                }
                    
                                
                            } catch (e) {
                                
                                
                                    displayHtmlInDivId(divResult, datahtml,true);
                               
                                }
                   
                };

    executeFormatedAjax(divResult, urlDir, data, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
    //$('#proyecto-consejo-form').reset(); 
}


// FUNCION PARA CONSULTAR LOS DATOS DE UN PROYECTO MEDIANTE UNA VENTANA DE DIALOGO
function consultarProyecto(id){

    direccion = 'consulta';
    title = 'Proyecto';
    data = { id: id};
    //Loading.show();
    
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var Dialogo= $("#dialogPantalla").removeClass('hide').dialog({
                
                modal: true,
                width: '700px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-pencil'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }

                    },
                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
               
        }
    });
    //Loading.hide();

}

//VENTANA DE DIALOGO PARA MODIFICAR UN PROYECTO

function modificarProyecto(id) {

    //console.log(id);
    
    direccion = 'edicion';
    title = 'Editar Proyecto';
    data = {id:id}
    //Loading.show();
    
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var Dialogo= $("#dialogPantalla").removeClass('hide').dialog({
                
                modal: true,
                width: '700px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-pencil'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                          click: function() {
                            editarProyecto(id);
                            //Dialogo.dialog('close');

                            }

                    }

                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
               
        }
    });
    //Loading.hide();
}

//FUNCION QUE ENVIA LA INFORMACION AL CONTROLLADOR PARA MODIFICAR UN PROYECTO
function editarProyecto(id) 
{
    var data = $('#proyecto-consejo-form').serialize();
    var divResult='div-result';
    var urlDir = 'edicion';
    var loadingEfect=true;
    var showResult=true;
    var method='POST';
    var responseFormat='html';

    var beforeSendCallback=function(){

            $('#'+divResult).html('');

    };
    
    var successCallback;
    
    var errorCallback=function(){};
    
    //executeFormatedAjax(divResult, urlDir, data, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback)
    
    successCallback=function(datahtml, resp2, resp3) {

                            try {
                                var response = jQuery.parseJSON(resp3.responseText);
                                

                                 if(response.status =='success' || response.status =='error'){

                                    if(response.status =='success') refrescarGrid();

                                    displayDialogBox(divResult, response.status, response.mensaje,true,true);
                        
                                }
                    
                                
                            } catch (e) {
                                
                                
                                    displayHtmlInDivId(divResult, datahtml,true);
                               
                                }
                   
                };

    executeFormatedAjax(divResult, urlDir, data, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
    //$('#proyecto-consejo-form').reset(); 
}

// ACTIVAR UN PROYECTO
function activarProyecto(id){

    var title = 'Activar Proyecto'

    $("#dialogPantalla").html('<div class="alertDialogBox"><p class="bolder center grey"> ¿Desea habilitar este Proyecto? </p></div>');

    var dialog = $("#dialogPantalla").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
        title_html: true,

        buttons: [
        {
            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
            "class": "btn btn-xs",

            click: function() {
                $(this).dialog("close");
            }
        },
        {
            html: "<i class='icon-check bigger-110'></i>&nbsp; Reactivar",
            "class": "btn btn-success btn-xs",
            
            click: function() {
                
                var divResult = "result";
                var urlDir = "activacion";
                var data = {id:id};
                var loadingEfect = true;
                var showResult = true;
                var method = "POST";
                var responseFormat='html';
                
                var beforeSendCallback=function(){
                    $('#'+divResult).html('');
                };


                var successCallback = function(datahtml, resp2, resp3){
                    refrescarGrid();
                    displayHtmlInDivId(divResult, datahtml,true);
                };

                $("html, body").animate({ scrollTop: 0 }, "fast");
                
                if(data){
                    
                    //executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                    executeFormatedAjax(divResult, urlDir, data, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
                    $(this).dialog("close");
                }
            }
        }
        ],
    });
    
    //Loading.hide();

}

// INHABILITAR UN PROYECTO
function eliminarProyecto(id){

    var title = 'Eliminar Proyecto'

    $("#dialogPantalla").html('<div class="alert alert-warning"> ¿Desea inactivar este Proyecto?</div>');

    var dialog = $("#dialogPantalla").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
        title_html: true,

        buttons: [
        {
            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
            "class": "btn btn-xs",

            click: function() {
                $(this).dialog("close");
            }
        },
        {
            html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar ",
            "class": "btn btn-danger btn-xs",
            
            click: function() {
                
                var divResult = "result";
                var urlDir = "eliminacion";
                var data = {id:id};
                var loadingEfect = true;
                var showResult = true;
                var method = "POST";
                var responseFormat='html';
                
                var beforeSendCallback=function(){
                    $('#'+divResult).html('');
                };


                var successCallback = function(datahtml, resp2, resp3){
                    refrescarGrid();
                    displayHtmlInDivId(divResult, datahtml,true);
                };

                $("html, body").animate({ scrollTop: 0 }, "fast");
                
                if(data){
                    
                    //executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                    executeFormatedAjax(divResult, urlDir, data, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
                    $(this).dialog("close");
                }
            }
        }
        ],
    });
    
    //Loading.hide();

}


function refrescarGrid(){
    
    $('#proyecto-consejo-grid').yiiGridView('update', {
    data: $(this).serialize()
});
}