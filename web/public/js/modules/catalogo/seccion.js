// keyText(this, true); true acepta la ñ y para que sea español
$('#seccionNombre').bind('keyup blur', function() {
    keyAlpha(this, false);// true acepta la ñ y para que sea español
    makeUpper(this);
});

$('#nombreDeSeccion').bind('keyup blur', function() {
    keyAlpha(this, false);// true acepta la ñ y para que sea español
    makeUpper(this);
});

function agregarSeccion() {

    Loading.show();
    $.ajax({
        url: "mostrarSeccionForm",
        data: $("#seccion-form").serialize(),
        dataType: 'html',
        type: 'post',
        success: function(resp) {
            //  $("#dialog_registrarSeccion span").html($("#SeccionPlantel_seccion_id option:selected").text());
            var dialogRegistrar = $("#dialog_registrarSeccion").removeClass('hide').dialog({
                modal: true,
                width: '850px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Agregar Nueva Sección</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            Loading.show();
                            $.ajax({
                                url: "guardarSeccion",
                                data: $("#seccion-form").serialize(),
                                dataType: 'html',
                                type: 'post',
                                success: function(resp) {

                                    if (isNaN(resp)) { // si la respuesta son caracteres muestra el error de ingreso
                                        document.getElementById("resultadoRegistrar").style.display = "none";
                                        document.getElementById("resultadoSeccionRegistrar").style.display = "block";
                                        $("#resultadoSeccionRegistrar").html(resp);
                                        //document.getElementById("resultadoSeccion").style.display = "none";
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    } else { //muestra mensaje que guardo
                                        // alert('guardo');
                                        $("#nombreDeSeccion").val('');
                                        refrescarGrid();
                                        document.getElementById("guardoRegistro").style.display = "block";
                                        dialogRegistrar.dialog('close');
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }

                                    Loading.hide();
                                }
                            })

                        }
                    }
                ]
            });
            $("#dialog_registrarSeccion").html(resp);
            Loading.hide();
        }
    });
}

$("#resultadoSeccionRegistrar").click(function() {
    document.getElementById("resultadoSeccionRegistrar").style.display = "none";
    document.getElementById("resultadoRegistrar").style.display = "block";
});

$("#resultadoElim").click(function(){
    document.getElementById("resultadoElim").style.display = "none";
});

$("#guardoRegistro").click(function() {
    //document.getElementById("resultadoSeccionRegistrar").style.display = "none";
    document.getElementById("guardoRegistro").style.display = "none";
    document.getElementById("resultadoElim").style.display = "none";
    //document.getElementById("resultadoRegistrar").style.display = "block";
});


function mostrarSeccion(id) {

    Loading.show();
    var data = {
        id: id
    }
    //alert(id);
    $.ajax({
        url: "mostrarSeccion",
        data: data,
        dataType: 'html',
        type: 'get',
        success: function(resp) {

            //  $("#dialog_registrarSeccion span").html($("#SeccionPlantel_seccion_id option:selected").text());
            var dialogRegistrar = $("#dialog_registrarSeccion").removeClass('hide').dialog({
                modal: true,
                width: '850px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Modificar Sección</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                        "class": "btn btn-primary btn-xs",
                        click: function() {


                            var data = {
                                id: id,
                                nombre: $("#nombreDeSeccion").val(),
                            }
                            Loading.show();
                            $.ajax({
                                url: "modificarSeccion?id=" + id,
                                data: data,
                                dataType: 'html',
                                type: 'post',
                                success: function(resp) {

                                    if (isNaN(resp)) { // si la respuesta son caracteres muestra el error de ingreso
                                        document.getElementById("resultadoRegistrar").style.display = "none";
                                        document.getElementById("resultadoSeccionRegistrar").style.display = "block";
                                        $("#resultadoSeccionRegistrar").html(resp);
                                        //document.getElementById("resultadoSeccion").style.display = "none";
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    } else { //muestra mensaje que guardo
                                        // alert('guardo');
                                        $("#nombreDeSeccion").val('');
                                        refrescarGrid();
                                        document.getElementById("guardoRegistro").style.display = "block";
                                        dialogRegistrar.dialog('close');
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }
                                    Loading.hide();

                                }
                            })
                            //  $(this).dialog("close");

                        }
                    }
                ],
            });
            $("#dialog_registrarSeccion").html(resp);
            Loading.hide();
        }
    })
    //  $("#dialog_registrarSeccion").show();
}


function confirmarEliminacion(id) {
    var dialogElim = $("#dialog_eliminacion").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Sección</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar Sección",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id
                    }
                    Loading.show();
                    $.ajax({
                        url: "eliminarSeccion?id=" + id,
                        data: data,
                        dataType: 'html',
                        type: 'post',
                        success: function(resp) {
                            // alert(resp);
                            refrescarGrid();
                            dialogElim.dialog('close');
                            $('#resultadoElim').html(resp);
                            document.getElementById("resultadoElim").style.display = "block";
                            $("html, body").animate({scrollTop: 0}, "fast");
                            Loading.hide();
                        }
                    })
                }
            }
        ]
    });
    $("#dialog_registrarSeccion").show();
}


function activarSeccion(id) {
    var dialogAct = $("#dialog_activacion").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Sección</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='fa icon-ok bigger-110'></i>&nbsp; Activar Sección",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: id
                    }
                    Loading.show();
                    $.ajax({
                        url: "activarSeccion?id=" + id,
                        data: data,
                        dataType: 'html',
                        type: 'post',
                        success: function(resp) {
                            // alert(resp);
                            refrescarGrid();
                            dialogAct.dialog('close');
                            $('#resultadoElim').html(resp);
                            document.getElementById("resultadoElim").style.display = "block";
                            $("html, body").animate({scrollTop: 0}, "fast");
                            Loading.hide();
                        }
                    })
                }
            }
        ]
    });
    $("#dialog_registrarSeccion").show();
}



function vizualizar(id) {
    var data =
            {
                id: id
            };
    Loading.show();
    $.ajax({
        url: "vizualizar",
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(resp)
        {
            var dialog_vizualizarI = $("#dialog_vizualizar").removeClass('hide').dialog({
                modal: true,
                width: '800px',
                draggable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Detalles de Sección</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            dialog_vizualizarI.dialog("close");
                        }

                    }

                ]
            });
            $("#dialog_vizualizar").html(resp);
            Loading.hide();
        }
    });
}


function refrescarGrid() {

    $('#seccion-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
}
