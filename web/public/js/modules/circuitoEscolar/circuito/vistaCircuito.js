$(document).on('ready',function(){


	$('#newRegister').on('click',function(e){
		e.preventDefault();
			
			var data = { circuito_id : $('#circuito').val() };
			$.ajax({
					 url: '../../MostrarFormCircuitoPlantel',
					 data: data,
					 type: 'post',
					 success: function(respuesta)
					 {
									var dialogRegistrar = $("#dialogoCircuitoPlantel").removeClass('hide').dialog({
							                modal: true,
							                width: '850px',
							                draggable: false,
							                resizable: false,
							                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Registrar Plantel en Circuito</h4></div>",
							                title_html: true,
							                buttons: [
							                    {
							                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
							                        "class": "btn btn-danger btn-xs",
							                        click: function() {
							                            $(this).dialog("close");
							                        }
							                    },
							                    {
							                        html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
							                        "class": "btn btn-primary btn-xs",
							                        click: function() {
							                        						Loading.show();
							                            					guardarCircuitoPlantel();
							                        				  }
							                    }
							                		]
							            																		});
											document.onkeydown = function (evt) 
																{
																   var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
																   if (keyCode == 13) 
																   {
																       guardarCircuitoPlantel();
																   }
																};
	
							            	$("#dialogoCircuitoPlantel").html(respuesta);

							            	$('#CircuitoPlantel_plantel_id').bind('keyup blur', function()
							            	{
										        keyAlphaNum(this, false,false);
										        makeUpper(this);
										    });

										    $('#CircuitoPlantel_plantel_id').on('blur',function(){


										    
										    	if( $('#CircuitoPlantel_plantel_id').val() !='' )
										    	{
										    		Loading.show();

										    		var data= { codigo_dea : $('#CircuitoPlantel_plantel_id').val(), circuito_id: $('#CircuitoPlantel_circuito_id').val() }
										    		$.ajax({
										    				url: '../../verificarCodigoPlantel',
										    				data: data,
										    				type: 'post',
										    				success: function (respuesta){

										    					var json= jQuery.parseJSON(respuesta);
										    					if(json.estatus=='success')
										    					{
										    						$('#nombre_plantel').val(json.plantel);
										    						//$('#plantelId').val(json.plantelId);
										    						$('#div-result').html('');
										    						Loading.hide();
										    					}
										    					if(json.estatus=='error')
										    					{
										    						$('#nombre_plantel').val('');
										    						$('#CircuitoPlantel_plantel_id').val('');
										    						displayDialogBox('div-result', 'errorDialogBox', json.mensaje);
										    						//$('#div-result').html(json.mensaje);
										    						Loading.hide();
										    					}
										    				}
										    		});
										    	}
										    })
					 }
				});
			});
});
function guardarCircuitoPlantel()
{
	$.ajax({
							                                url: "../../guardarCircuitoPlantel",
							                                data: $("#circuito-plantel-form").serialize(),
							                                dataType: 'html',
							                                type: 'post',
							                                success: function(resp, resp2, resp3) {

							                                	$('#CircuitoPlantel_plantel_id').on('blur',function(){

							                                		if($('#CircuitoPlantel_plantel_id').val()==''){
							                                			$('#nombre_plantel').val('');
							                                		}
							                                	});

							                                	

							                                    try {
							                                        var json = jQuery.parseJSON(resp3.responseText);
							                                        if (json.estatus == "success") 
							                                        {
							                                        	$('#exportar').removeClass('disabled');
							                                        	refrescarGrid();
							                                        	$("#dialogoCircuitoPlantel").addClass('hide').dialog("close");
							                                        	displayDialogBox('resultadoOperacion', 'successDialogBox', json.mensaje);
							                                         	$("html, body").animate({scrollTop: 0}, "fast");
							                                         	Loading.hide();
							                                        }
							                                        if (json.estatus == "error") {
							                                            displayDialogBox('div-result', 'errorDialogBox', json.mensaje);
							                                            $("html, body").animate({scrollTop: 0}, "fast");
							                                            Loading.hide();
							                                        }
							                                    }
							                                    catch (e) {
							                                    				$('#div-result').html('');
							            										$('#div-result').html(resp);
							            										Loading.hide();
							                                    		  }
							                                }
							                            });
}