$(document).on('ready',function(){

	verificarCampos();

	$('#Circuito_nombre_circuito').bind('keyup blur', function() {
        keyAlphaNum(this, true);
        makeUpper(this);
    });

    $('#Circuito_tipo_circuito').on('change',function(){


    	if($('#Circuito_tipo_circuito').val()=='' || $('#Circuito_tipo_circuito').val()=='M')
    	{
    		$('#Circuito_parroquia_id').html('').html('<option value="">-Seleccione-</option>');
    	}
    	if($('#Circuito_tipo_circuito').val()=='P' && $('#Circuito_municipio_id').val()!='' )
    	{
    		var municipioId = $('#Circuito_municipio_id').val();
    		buscarParroquias(municipioId);
    	}
    	verificarCampos();
    });

    var estadoId = $('#Circuito_estado_id').val();

    if(estadoId!='')
    {
    	var formType = $('#circuito-form').attr('data-form-type');
    		if(formType!='edicion')
    		{
		    buscarMunicipios(estadoId);
			}
    }
	$('#Circuito_estado_id').on('change',function(){
			var estadoId = $('#Circuito_estado_id').val();
			if(estadoId!='')
			{
				buscarMunicipios(estadoId);
			}
			if(estadoId=='')
			{
				$('#Circuito_municipio_id,#Circuito_parroquia_id').html('');
				$('#Circuito_municipio_id,#Circuito_parroquia_id').html('<option value="">-Seleccione-</option>');
			}
	});

	$('#Circuito_municipio_id').on('change',function(){
			var municipioId = $('#Circuito_municipio_id').val();
			if(municipioId!='' && $('#Circuito_tipo_circuito').val()=='P')
			{
				buscarParroquias(municipioId);
			}
			if(municipioId=='')
			{
				$('#Circuito_parroquia_id').html('').html('<option value="">-Seleccione-</option>');
			}
			
	});

	$('#btnRegistrarCircuito').on('click',function(e){
			e.preventDefault();
			verificarEstadoMunicipioParroquia();
	});

});

function buscarMunicipios(estadoId){

			var data= { estado_id : estadoId };

	$.ajax({
            url: "/circuitoEscolar/circuito/buscarMunicipios",
            data: data,
            type: 'post',
            success: function(resp) {
            							$('#Circuito_municipio_id').html(resp);
            						}
        });
}

function buscarParroquias(municipioId){

			var data= { municipio_id : municipioId };

	$.ajax({
            url: "/circuitoEscolar/circuito/buscarParroquias",
            data: data,
            type: 'post',
            success: function(resp) {
            							$('#Circuito_parroquia_id').html(resp);
            						}
        });
}

function verificarCampos()
{
	var tipoCircuito= $('#Circuito_tipo_circuito').val();
    	if(tipoCircuito=='')
    	{
    		$('.municipio').addClass('hide');
    		$('.parroquia').addClass('hide');
    	}
    	if(tipoCircuito=='M')
    	{
    		$('.municipio').removeClass('hide');
    		$('.parroquia').addClass('hide');
    	}
    	if(tipoCircuito=='P')
    	{
    		$('.parroquia').removeClass('hide');
    		$('.municipio').removeClass('hide');
    	}
}
function verificarEstadoMunicipioParroquia()
{

	var formType = $('#circuito-form').attr('data-form-type');

	$.ajax({
            url: "/circuitoEscolar/circuito/VerificarEstadoMunicipioParroquia",
            data: $('#circuito-form').serialize()+'&formType='+formType,
            type: 'post',
            success: function(resp, resp2, resp3){
            	try{
	            								var json = jQuery.parseJSON(resp3.responseText);
	            								if(json.estatus=='error')
	            								{
	            									var mensaje= json.mensaje;
							                        var title = 'Circuito Escolar';
							                        $('#div-result').html('');
							                        verDialogo(mensaje, title);
	            								}	
	            								if(json.estatus=='success')
	            								{
	            									$('#circuito-form').submit();
	            								}
	            	}
	            							catch (e) {
							            				$('#div-result').html('');
							            				$('#div-result').html(resp);
							            			}
            							 }
        	});
}

function verDialogo(message, title, style, clickCallback, reload, buttonsExtra) {
        if (!style)
            style = 'info';

        var buttonsDialog = [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if (clickCallback) {
                        clickCallback();
                    }
                    if (reload) {
                        window.location.reload();
                    }
                }
            }
        ];

        if (buttonsExtra)
            buttonsDialog.push(buttonsExtra);

        displayDialogBox('dialog_error', style, message);

        var dialog = $("#dialog_error").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: buttonsDialog
        });
    }