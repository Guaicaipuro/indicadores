/**
 * Created by developer on 24/04/15.
 */
$(document).on('ready', function(){

    $("#repEstadistico").on('click', function(evt){
        evt.preventDefault();
        reporteEstadistico('region', 'x');
    });

});

function reporteEstadistico(){

    var divResult = "resultado";
    var urlDir = "/control/personal/reporteGeneral";
    var conEfecto = true;
    var showHTML = true;
    var method = "POST";
    var callback = null;

    $("#tipoReporteText").html("Estadístico General");
    $("#selectTipoReporteText").html(": Estadístico General");

    executeAjax(divResult, urlDir, conEfecto, showHTML, method, callback);

}