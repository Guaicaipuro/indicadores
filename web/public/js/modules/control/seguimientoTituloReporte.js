/**
 * Created by mari on 02/07/15.
 */

$(document).on('ready', function(){

    $("#repEstadisticoTitulo").on('click', function(evt){
        evt.preventDefault();
        repEstadisticoTitulo();
    });



    $('#control_zona_observacion').bind('keyup blur', function () {
        keyText(this, true);
    });

    $('#control_zona_observacion').bind('blur', function () {
        clearField(this);
    });

});

/**
 * Genera el reporte Estadístico de Carga de Autoridades.
 * Si el nivel es "region" la dependency no es requerida, pero, si el nivel es "estado" la dependency debe ser el Id de la región de esos estados
 *
 * @param String nivel (estado, region)
 * @param Integer dependency
 */
function repEstadisticoTitulo(){

    //$("#fechaCondicion").addClass('hide');

    var divResult = "resultado";
    var urlDir = "/control/seguimientoTituloReporte/estadisticoSegTitulo";
    var datos = '';
    var conEfecto = true;
    var showHTML = true;
    var method = "POST";
    var callback = null;

    $("#tipoReporteText").html("Estadístico General");
    $("#selectTipoReporteText").html(": Estadístico General");
    //$("html, body").animate({ scrollTop: 0 }, "fast");


    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);

}


function mostrarTitulosMunicipios(estado_id){

        var divResult = "resultado";
        var urlDir = "/control/seguimientoTituloReporte/estadisticoSegTituloMunicipio";
        var datos = {id:estado_id};
        var conEfecto = true;
        var showHTML = true;
        var method = "POST";
        var callback = null;
        $("#resultado").html("");

        executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);

}


function mostrarTitulosParroquia(municipio_id,estado_id){
    var divResult = "resultado";
    var urlDir = "/control/seguimientoTituloReporte/estadisticoSegTituloParroquia";
    var datos = {id:municipio_id,estado_id:estado_id};
    var conEfecto = true;
    var showHTML = true;
    var method = "POST";
    var callback = null;
    $("#resultado").html("");

    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
}



function mostrarTitulosPlanteles(estado_id,municipio_id,parroquia_id){
    var divResult = "resultado";
    var urlDir = "/control/seguimientoTituloReporte/estadisticoSegTituloPlanteles";
    var datos = {municipio_id:municipio_id,estado_id:estado_id,parroquia_id:parroquia_id};
    var conEfecto = true;
    var showHTML = true;
    var method = "POST";
    var callback = null;
    $("#resultado").html("");

    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
}