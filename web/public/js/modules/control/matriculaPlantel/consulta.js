/**
 * Created by nelson on 21/05/15.
 */
$(document).ready(function() {

    $('#codPlantel').on('change', function(){
        var codPlantel = $("#codPlantel").val();
        if (codPlantel != ''){
            validarCodplantel(codPlantel);
        }
    });

    function validarCodplantel(gradoId,codPlantel){
        var divResult="resultadoOperacion";
        var urlDir="matriculaPlantel/validarCodPlantel";
        var datos="codPlantel="+$('#codPlantel').val();
        var conEfecto=true;
        var showHTML=true;
        var method="POST";
        var responseFormat="html";
        var callback=null;

        executeAjax(divResult,urlDir,datos,conEfecto,showHTML,method,responseFormat,callback);
    }

    /*function plantelMatriculado(gradoId,codPlantel){
        var divResult="resultado";
        var urlDir="control/matriculaPlantel/loadGradoSeccion";
        var datos="periodoId="+$('#periodoId').val()+'&codPlantel='+$('#codPlantel').val();
        var conEfecto=true;
        var showHTML=true;
        var method="POST";
        var responseFormat="html";
        var callback=null;

        executeAjax(divResult,urlDir,datos,conEfecto,showHTML,method,responseFormat,callback);
    }


    function plantelMatriculado(gradoId,codPlantel){
        var divResult="resultado";
        var urlDir="control/matriculaPlantel/loadGradoSeccion";
        var datos="periodoId="+$('#periodoId').val()+'&codPlantel='+$('#codPlantel').val();
        var conEfecto=true;
        var showHTML=true;
        var method="POST";
        var responseFormat="html";
        var callback=null;

        executeAjax(divResult,urlDir,datos,conEfecto,showHTML,method,responseFormat,callback);
    }*/

    $("#btnConsultar").unbind('click');
    $("#btnConsultar").click(function(e) {
        e.preventDefault();
        var tipo_busqueda;
        var EstuadianteEgresado;
        var style = 'alerta';
        var msgtitulo = "<p>Estimado usuario, por favor se requiere que ingrese todos los campos para poder realizar esta acción .</p>";
        periodoId = $("#periodoId").val();
        codPlantel = $("#codPlantel").val();
        seccionPlantelId = $("#seccionPlantelId").val();
        //EstuadianteEgresado = $("#EstuadianteEgresado").val();

        if (periodoId != '' && codPlantel != '' && seccionPlantelId != '' && seccionPlantelId != null) {
            var conEfecto = true;
            var showHTML = true;
            var method = 'GET';
            //var divResult = 'result-solicitud';
            var divResult = 'seccionPlantelId';
            var urlDir = '/control/matriculaPlantel/detallesMatricula';
            var datos;
            datos = {
                periodoId: periodoId,
                codPlantel: codPlantel,
                seccionPlantelId: seccionPlantelId
            };
            Loading.show();
            $.ajax({
                url: urlDir,
                data: datos,
                dataType: 'json',
                type: method,
                success: function(resp3) {

                    if(resp3.status=='exito'){
                        console.log(resp3.url);
                        window.location.href=resp3.url;
                    }else if(resp3.status=='error'){

                    }
                }
            });
        } else {
            $("#ResultMatricula").addClass('hide');
            displayDialogBox('alertaConsultarMatricula', style, msgtitulo);
            $("#ResultMatricula").addClass('hide');

        }
    });


    /////////////////////////////////////
    //function refrescarGradoSeccion(periodoId, cod_plantel){
    //    var data ={
    //        periodoId: periodoId,
    //        cod_plantel: cod_plantel
    //    }
    //    //Loading.show();
    //    $.ajax({
    //        url: "/control/matriculaPlantel/loadGradoSeccion",
    //        data: data,
    //        dataType: 'html',
    //        type: 'post',
    //        success:function(resp){
    //
    //        },
    //        error: function(e){
    //            $('#ResultMatricula').html(e.responseText);
    //
    //        }
    //    })
    //}
    //'ajax' => array(
    //    'type' => 'GET',
    //    'url' => CController::createUrl('matriculaPlantel/loadGradoSeccion'),
    //    'update' => '#seccionPlantelId',
});