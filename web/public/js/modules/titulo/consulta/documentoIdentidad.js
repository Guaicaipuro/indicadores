/**
 * Created by nelson on 21/05/15.
 */
$(document).ready(function() {


    $("#EstuadianteEgresado").bind('keyup blur', function () {
        keyAlphaNum(this, true, true);
        makeUpper(this);
    });


    $("#btnConsultar").unbind('click');
    $("#btnConsultar").click(function(e) {
        e.preventDefault();
        var tipo_busqueda;
        var EstuadianteEgresado;
        var style = 'alerta';
        var msgtitulo = "<p>Estimado usuario, por for se requiere que ingrese ambos campos para poder realizar esta acción .</p>";
        tipoNacionalidad = $("#tipoNacionalidad").val();
        EstuadianteEgresado = $("#EstuadianteEgresado").val();

        if (tipoNacionalidad != '' && EstuadianteEgresado != '') {

            var conEfecto = true;
            var showHTML = true;
            var method = 'POST';
            var divResult = 'result-solicitud';
            var urlDir = '/titulo/ConsultarTitulo/busquedaEstudianteEgresado/';
            var datos;
            datos = {
                tipoNacionalidad: tipoNacionalidad,
                EstuadianteEgresado: EstuadianteEgresado
            };
            Loading.show();
            $.ajax({
                url: urlDir,
                data: datos,
                dataType: 'html',
                type: method,
                success: function(resp, resp2, resp3) {


                    try {
                        var json = jQuery.parseJSON(resp3.responseText);

                        if (json.statusCode == 'alert') {
//                                $("#errorConsultaTitulo").addClass('hide');
//                                $("#errorConsultaTitulo").addClass('errorDialogBox');
//                                $("#errorConsultaTitulo p").html('');
//                                $("#ResultEgresados").html('');
//                                $("#ResultEgresados").addClass('hide');
//                                $("#errorConsultaTitulo").removeClass('hide');
//                                $("#errorConsultaTitulo p").html(json.mensaje);
//                            $("html, body").animate({scrollTop: 0}, "fast");
                            $("#alertaConsultarTitulo").html('');
                            $("#ResultEgresados").html('');
                            $("#ResultEgresados").addClass('hide');
                            $("#errorConsultaTitulo").removeClass('hide');
                            $("#errorConsultaTitulo p").html(json.mensaje);

//                            $("html, body").animate({scrollTop: 0}, "fast");

                            Loading.hide();

                        }

                    } catch (e) {
//                            $("#errorConsultaTitulo p").html('');
//                            $("#errorConsultaTitulo").addClass('hide');
//                            $("#errorConsultaTitulo p").html('');
//                            $("#indexConsultarTitulo").html('');
//                            $("#ResultEgresados").removeClass('hide');
//                            $("#ResultEgresados").html(resp);



                        $("#errorConsultaTitulo").addClass('hide');
                        $("#errorConsultaTitulo p").html('');
                        $("#indexConsultarTitulo").html('');
                        $("#ResultEgresados").removeClass('hide');
                        $("#ResultEgresados").html(resp);
                        Loading.hide();
                    }


                }

            });
        } else {
//                $("#errorConsultaTitulo").addClass('hide');
//                $("#errorConsultaTitulo p").html('');
//                $("#errorConsultaTitulo").removeClass('hide');
//                $("#errorConsultaTitulo").removeClass('errorDialogBox');
            $("#errorConsultaTitulo").addClass('hide');
            displayDialogBox('alertaConsultarTitulo', style, msgtitulo);
            $("#errorConsultaTitulo").addClass('hide');

        }
    });
});