$(document).ready(function() {

    $("#btn_registro_programado").on("click", function(evt){

        getFormRegistroProgramado();

    });

    $("#form-registro-programado-serial").on('submit', function(evt){
        evt.preventDefault();
        proccessFormSerialesProgramados();
    });

});

function getFormRegistroProgramado(){

    var divResult = "#dialogFormCargaProgramada";
    var urlDir = "/titulo/registroProgramadoSerial/crear";
    var datos = null;
    var loadingEfect = true;
    var showResult = true;
    var method = 'GET';
    var responseFormat = 'html';
    var beforeSend = null;
    var callback = null;

    executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSend, callback);

    var dialog = $("#dialogFormCargaProgramada").removeClass('hide').dialog({
        modal: true,
        width: '1100px',
        draggable: false,
        resizable: false,
        position: ['center', 20],
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-credit-card'></i> &nbsp;Registro Programado de Seriales</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-danger",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar",
                "class": "btn btn-primary",
                click: function() {

                    proccessFormSerialesProgramados();
                    // $(this).dialog("close");
                    // $("html, body").animate({scrollTop: 0}, "fast");
                }
            }
        ]
    });

}

function proccessFormSerialesProgramados(){

    var prefijo = $("#FormRegistroProgramadoSerial_prefijo").val();
    var serialInicial = $('#FormRegistroProgramadoSerial_serial_inicial').val();
    var serialFinal = $('#FormRegistroProgramadoSerial_serial_final').val();

    var mensaje = "";

    if(prefijo.length!=2 || !isNaN(prefijo)){
        mensaje = "El Prefijo de los seriales debe contener dos caracteres y solo deben ser letras mayúsculas.";
    }
    else if(serialInicial=="" || isNaN(serialInicial)){
        mensaje = "El Serial Inicial es un campo requerido y debe tener solo caracteres numéricos.";
    }
    else if(serialFinal=="" || isNaN(serialFinal)){
        mensaje = "El Serial Final es un campo requerido y debe tener solo caracteres numéricos.";
    }
    else if((serialInicial*1)>=(serialFinal*1)){
        mensaje = "El Serial Inicial debe ser menor al Serial Final.";
    }

    if(mensaje!=""){
        displayDialogBox('#resultCargaProgramada', 'error', mensaje);
        return false;
    }else{
        guardarFormSerialesProgramados();
    }

}

function guardarFormSerialesProgramados(){

    var divResult = "#dialogFormCargaProgramada";
    var urlDir = $("#form-registro-programado-serial").attr('action');
    var datos = $("#form-registro-programado-serial").serialize();
    var loadingEfect = false;
    var showResult = true;
    var method = 'POST';
    var responseFormat = 'html';
    var beforeSend = null;
    var callback = function(){
        refreshRegistrosProgramadosSeriales();
    };

    executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSend, callback);

}

function getFormEditarRegistroProgramado(id){

    var divResult = "#dialogFormCargaProgramada";
    var urlDir = "/titulo/registroProgramadoSerial/editar/id/"+id;
    var datos = null;
    var loadingEfect = true;
    var showResult = true;
    var method = 'POST';
    var responseFormat = 'html';
    var beforeSend = null;
    var callback = null;

    executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSend, callback);

}

function proccessActionSerialesProgramados(action, id){
    
    console.log(action);
    
    if(action=='activate' || action=='delete'){
        
        if(action=='activate'){
            var urlDir = "/titulo/registroProgramadoSerial/activar/id/"+id;
        }
        else{
            var urlDir = "/titulo/registroProgramadoSerial/anular/id/"+id;
        }
        
        var divResult = "#dialogFormCargaProgramada";
        var datos = null;
        var loadingEfect = true;
        var showResult = true;
        var method = 'POST';
        var responseFormat = 'html';
        var beforeSend = null;
        var callback = function(){
            $("#btn-accion-registro-programada").addClass('hide');
            refreshRegistrosProgramadosSeriales();
        };

        executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSend, callback);
    }
    
}

function refreshRegistrosProgramadosSeriales(){

    var divResult = "#registro-programado-serial-grid";
    var urlDir = "/titulo/registro";
    var datos =  "RegistroProgramadoSerial[serial_inicial]=&RegistroProgramadoSerial[serial_final]=&RegistroProgramadoSerial[username_ini]=";
    var loadingEfect = false;
    var showResult = true;
    var method = 'GET';
    var responseFormat = 'html';
    var beforeSend = null;
    var callback = null;

    executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSend, callback);

}

/**
 * programadaDialog('MTc=','/titulo/registroProgramadoSerial/editar','Atender Solicitud de Registro','update')
 */
function registroProgramadoDialog(id, url, title, action){
    
    if(action=='delete'){

        displayDialogBox('#dialogFormCargaProgramada', 'alert', '¿Confirma usted que desea Anular la Solicitud de Registro Programado de Seriales?');

    }
    else if(action=='activate'){

        displayDialogBox('#dialogFormCargaProgramada', 'alert', '¿Confirma usted que desea Reactivar la Solicitud de Registro Programado de Seriales?');

    }
    else{

        getFormEditarRegistroProgramado(id);

    }
    
        var dialog = $("#dialogFormCargaProgramada").removeClass('hide').dialog({
            modal: true,
            width: '1100px',
            draggable: false,
            resizable: false,
            position: ['center', 20],
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-credit-card'></i> &nbsp;"+title+"</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-danger",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar",
                    "class": "btn btn-primary",
                    "id": "btn-accion-registro-programada",
                    click: function() {
                        if(action!='activate' && action!='delete'){
                            proccessFormSerialesProgramados();
                        }
                        else{
                            proccessActionSerialesProgramados(action, id);
                        }
                    }
                }
            ]
        });
        
        if(action=='delete'){
        
            $("#btn-accion-registro-programada").html("<span class='ui-button-text'><i class='fa fa-times bigger-110'></i> &nbsp; Anular</span>");

        }
        else if(action=='activate'){

            $("#btn-accion-registro-programada").html("<span class='ui-button-text'><i class='fa fa-check bigger-110'></i> &nbsp; Activar</span>");

        }
    
}

$(document).ready(function(){
    $('#RegistroProgramadoSerial_fecha_ini').datepicker();
    $.datepicker.setDefaults($.datepicker.regional = {
            dateFormat: 'dd-mm-yy',
            showOn:'focus',
            showOtherMonths: false,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            minDate: new Date(1800, 1, 1),
            maxDate: 'today'
        });

    $('#RegistroProgramadoSerial_fecha_carga').datepicker();
    $.datepicker.setDefaults($.datepicker.regional = {
            dateFormat: 'dd-mm-yy',
            showOn:'focus',
            showOtherMonths: false,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            minDate: new Date(1800, 1, 1),
            maxDate: 'today'
        });

    $('#RegistroProgramadoSerial_serial_inicial').bind('keyup blur', function () {
        keyNum(this, false);
    });

    $('#RegistroProgramadoSerial_serial_inicial').bind('blur', function () {
        clearField(this);
    });

    $('#RegistroProgramadoSerial_serial_final').bind('keyup blur', function () {
        keyNum(this, false);
    });

    $('#RegistroProgramadoSerial_serial_final').bind('blur', function () {
        clearField(this);
    });
});