$(document).ready(function(){
    $('#departamento_id').unbind('change');

    $('#departamento_id').on("change", function(){

        var departamentoId = categoriaDepartamento.getDepartamentoId();
        var divResult = "#div-categorias";
        if(!isNaN(departamentoId) && departamentoId.length>0){
            var urlDir = "/indicadores/categoriaDepartamento/lista/dep/"+base64_encode(departamentoId);
            var datos = null;
            var loadingEfect = true;
            var showResult = true;
            var method = 'GET';
            var responseFormat = 'html';
            var beforeSendCallback = null;
            var successCallback = function(response){
                categoriaDepartamento.manejarEventosLista();
            };
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
        }   
        else{
            $("#div-indicadores").html("");
            displayDialogBox(divResult, 'info', 'En este módulo podrá registrar y/o actualizar los Datos de Categorías. Para esto debe primero seleccionar un departamento.');
        }

    });

});

var categoriaDepartamento = {
    
    getDepartamentoId: function(){
        var departamentoId = $('#departamento_id').val();
        return departamentoId;
    },
    
    manejarEventosLista: function(){

        categoriaDepartamento.eventosCgridview();
        
        $("#newRegisterCategoria").on('click', function(evt){
            evt.preventDefault();
            categoriaDepartamento.registrarNuevaCategoria($(this));
        });
        
        $(".ver-categoria").on('click', function(evt){
            evt.preventDefault();
            categoriaDepartamento.verCategoria($(this));
        });
        $(".editar-categoria").on('click', function(evt){
            evt.preventDefault();
            categoriaDepartamento.editarCategoria($(this));
        });
        $(".ver-indicadores-categoria").on('click', function(evt){
            evt.preventDefault();
            categoriaDepartamento.verIndicadoresCategoria($(this));
        });
        $(".estatus-categoria").on('click', function(evt){
            evt.preventDefault();
            categoriaDepartamento.cambiarEstatusCategoria($(this));
        });

    },
    
    registrarNuevaCategoria: function (link) {
        var departamentoId = categoriaDepartamento.getDepartamentoId();
        var divResult = "#div-modal-categoria-departamento";
        if (!isNaN(departamentoId) && departamentoId.length > 0) {
            var urlDir = link.attr('href');
            var datos = null;
            var loadingEfect = true;
            var showResult = true;
            var title = "Registro de Categoría";
            var method = 'GET';
            var responseFormat = 'html';
            var beforeSendCallback = null;
            var successCallback = function (response) {
                categoriaDepartamento.mostrarVentanaModalRegistro(divResult, title);
            };
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
        }
    },
    
    verCategoria: function (link) {
        console.log(link);
    },
    
    editarCategoria: function(link){
        var departamentoId = categoriaDepartamento.getDepartamentoId();
        var divResult = "#div-modal-categoria-departamento";
        if (!isNaN(departamentoId) && departamentoId.length > 0) {
            var urlDir = link.attr('href');
            var datos = null;
            var loadingEfect = true;
            var showResult = true;
            var title = "Editar Datos de Categoría";
            var method = 'GET';
            var responseFormat = 'html';
            var beforeSendCallback = null;
            var successCallback = function (response) {
                categoriaDepartamento.mostrarVentanaModalRegistro(divResult, title);
            };
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
        }
    },
    
    verIndicadoresCategoria: function(link){
        
        var divResult = "#div-indicadores";
        var urlDir = link.attr("href");
        var datos = { categoria: link.attr("data-categoria") };
        var loadingEfect = true;
        var showResult = true;
        var method = 'GET';
        var responseFormat = 'html';
        var beforeSendCallback = null;
        var successCallback = function(response){
            indicadores.manejarEventosLista();
            $('html,body').animate({scrollTop: $("#div-indicadores").offset().top}, 'slow');
        };
        executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);

    },
    
    cambiarEstatusCategoria: function(link){
        var title = link.attr("title")+" Categoría";
        var divResult = "#div-modal-categoria-departamento";
        var href = link.attr("href");
        displayDialogBox(divResult, 'alert', '&iquest;Desea usted '+link.attr("title")+' esta Categoría?');
        categoriaDepartamento.mostrarVentanaModalCambiarEstatus(divResult, title, href);
    },
    
    eventosCgridview: function(){
        $("#CategoriaDepartamento_nombre").on('keyup blur', function () {
            keyAlphaNum(this, true, true);
        });
    },

    eventosFormulario: function(){
        $("#CategoriaDepartamento_nombre_registro").on("keyup blur", function(){
            keyAlphaNum(this, true, true);
        });
        $("#categoria-departamento-form").on("submit", function(evt){
            
            evt.preventDefault();
            
            var divResult = "#div-resultado-categoria-departamento";
            var urlDir = $(this).attr("action");
            var datos = $(this).serialize();
            var loadingEfect = true;
            var showResult = true;
            var title = "Registro de Categoría";
            var method = $(this).attr("method");
            var responseFormat = 'html';
            var beforeSendCallback = function(){
                if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                    Loading.show();
                }
            };
            var successCallback = function (response) {
                if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                    Loading.hide();
                }
                $("#btn-volver-categoria-departamento").click();
                $.fn.yiiGridView.update("categoria-departamento-grid");
                $("#div-modal-categoria-departamento").html("");
                scrollUp("slow");
            };
            var errorCallback = function(xhr, ajaxOptions, thrownError, ajaxDataResponse){
                $("#div-result").html(ajaxDataResponse+" : "+xhr.responseText);
                if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                    Loading.hide();
                }
                scrollUp("slow");
            };
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
        });
    },                

    mostrarVentanaModalRegistro: function(div, title, close){
        $(div).removeClass("hide").dialog({
            width: 800,
            resizable: false,
            draggable: false,
            title: "<div class='widget-header widget-header-small'><h5 class='smaller'>"+title+"</h5></div>",
            title_html: true,
            modal: true,
            buttons: [
                {
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-danger btn-xs",
                    "id": "btn-volver-categoria-departamento",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar",
                    "class": "btn btn-primary btn-xs",
                    "id": "btn-guardar-categoria-departamento",
                    click: function() {
                        $("#submitGuardarCategoriaDepartamento").click();
                    }
                }
            ],
            close: function() {
                $(this).dialog('close');
                if(typeof close == "function" ){
                    close();
                }
            }
        });
        categoriaDepartamento.eventosFormulario();
    },
    
    mostrarVentanaModalCambiarEstatus: function(div, title, href){
        $(div).removeClass("hide").dialog({
            width: 800,
            resizable: false,
            draggable: false,
            title: "<div class='widget-header widget-header-small'><h5 class='smaller'>"+title+"</h5></div>",
            title_html: true,
            modal: true,
            buttons: [
                {
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-danger btn-xs",
                    "id": "btn-volver-categoria-departamento",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp; "+title,
                    "class": "btn btn-warning btn-xs",
                    "id": "btn-cambiar-estatus-categoria-departamento",
                    click: function() {
                        
                        var divResult = "#div-resultado-categoria-departamento";
                        var urlDir = href;
                        var datos = null;
                        var loadingEfect = true;
                        var showResult = true;
                        var method = "DELETE";
                        var responseFormat = 'html';
                        var beforeSendCallback = function(){
                            if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                                Loading.show();
                            }
                        };
                        var successCallback = function (response) {
                            if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                                Loading.hide();
                            }
                            $("#btn-volver-categoria-departamento").click();
                            $.fn.yiiGridView.update("categoria-departamento-grid");
                            scrollUp("slow");
                        };
                        var errorCallback = function(xhr, ajaxOptions, thrownError, ajaxDataResponse){
                            $("#div-resultado-categoria-departamento").html(ajaxDataResponse+" : "+xhr.responseText);
                            if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                                Loading.hide();
                            }
                            scrollUp("slow");
                        };
                        executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
                        
                    }
                }
            ],
            close: function() {
                $(this).dialog('close');
            }
        });
        categoriaDepartamento.eventosFormulario();
    }

};

var indicadores = {
    
    categoriaId: null,
    
    departamentoId: null,
    
    categoria: null,
    
    manejarEventosLista: function(){

        indicadores.eventosCgridview();
        
        $("#newRegisterIndicador").on('click', function(evt){
            evt.preventDefault();
            indicadores.registrarNuevoIndicador($(this));
        });
        
        $(".editar-indicador").on('click', function(evt){
            evt.preventDefault();
            indicadores.editarIndicador($(this));
        });
        $(".registrar-valor").on('click', function(evt){
            evt.preventDefault();
            indicadores.registrarValorIndicador($(this));
        });
        $(".estatus-indicador").on('click', function(evt){
            evt.preventDefault();
            indicadores.cambiarEstatusIndicador($(this));
        });

    },
    
    registrarNuevoIndicador: function (link) {
        var categoriaId = link.attr("data-categoria-id"); 
        var departamentoId = $("#departamento_id").val();
        var categoria = link.attr("data-categoria");
        console.log(categoria);
        var divResult = "#div-modal-indicadores";
        if (!isNaN(categoriaId) && categoriaId.length > 0) {
            this.categoriaId = categoriaId;
            this.departamentoId = departamentoId;
            this.categoria = categoria;
            var urlDir = link.attr('href');
            var datos = null;
            var loadingEfect = true;
            var showResult = true;
            var title = "Registro de Indicador";
            var method = 'GET';
            var responseFormat = 'html';
            var beforeSendCallback = null;
            var successCallback = function (response) {
                indicadores.mostrarVentanaModalRegistro(divResult, title);
            };
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
        }
    },
    
    registrarValorIndicador: function (link) {
        var categoriaId = link.attr("data-categoria-id"); 
        var departamentoId = $("#departamento_id").val();
        var categoria = link.attr("data-categoria");
        console.log(categoria);
        var divResult = "#div-modal-indicadores";
        if (!isNaN(categoriaId) && categoriaId.length > 0) {
            this.categoriaId = categoriaId;
            this.departamentoId = departamentoId;
            this.categoria = categoria;
            var urlDir = link.attr('href');
            var datos = null;
            var loadingEfect = true;
            var showResult = true;
            var title = "Registro de Valor de Indicador";
            var method = 'GET';
            var responseFormat = 'html';
            var beforeSendCallback = null;
            var successCallback = function (response) {
                indicadores.mostrarVentanaModalRegistro(divResult, title);
            };
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
        }
    },
    
    editarIndicador: function(link){
        var categoriaId = link.attr('data-categoria-id');
        var departamentoId = $("#departamento_id").val();
        var categoria = link.attr("data-categoria");
        var divResult = "#div-modal-indicadores";
        if (!isNaN(categoriaId) && categoriaId.length > 0) {
            this.categoriaId = categoriaId;
            this.departamentoId = departamentoId;
            this.categoria = categoria;
            var urlDir = link.attr('href');
            var datos = null;
            var loadingEfect = true;
            var showResult = true;
            var title = "Editar Datos de Categoría";
            var method = 'GET';
            var responseFormat = 'html';
            var beforeSendCallback = null;
            var successCallback = function (response) {
                indicadores.mostrarVentanaModalRegistro(divResult, title);
            };
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
        }
    },
    
    cambiarEstatusIndicador: function(link){
        var categoriaId = link.attr('data-categoria-id');
        var departamentoId = $("#departamento_id").val();
        var categoria = link.attr("data-categoria");
        this.categoriaId = categoriaId;
        this.departamentoId = departamentoId;
        this.categoria = categoria;
        var title = link.attr("title")+" Indicador";
        var divResult = "#div-modal-indicadores";
        var href = link.attr("href");
        displayDialogBox(divResult, 'alert', '&iquest;Desea usted '+link.attr("title")+' este Indicador?');
        indicadores.mostrarVentanaModalCambiarEstatus(divResult, title, href);
    },
    
    eventosCgridview: function(){
        $("#indicadores_nombre").on('keyup blur', function () {
            keyAlphaNum(this, true, true);
        });
    },

    eventosFormulario: function(){
        $("#Indicador_variable").on("keyup blur", function(){
            keyAlphaNum(this, true, true);
        });
        
        $("#ValorIndicador_valor").on("keyup blur", function(){
            keyText(this, true);
        });
        
        $("#indicador-form").on("submit", function(evt){
            
            evt.preventDefault();
            
            var divResult = "#div-resultado-indicador";
            var urlDir = $(this).attr("action");
            var datos = $(this).serialize();
            var loadingEfect = true;
            var showResult = true;
            var title = "Registro de Indicador";
            var method = $(this).attr("method");
            var responseFormat = 'html';
            var beforeSendCallback = function(){
                if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                    Loading.show();
                }
            };
            var successCallback = function (response) {
                if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                    Loading.hide();
                }
                $("#btn-volver-indicadores").click();
                indicadores.refrescarIndicadoresCategoria();
                $("#div-modal-indicadores").html("");
            };
            var errorCallback = function(xhr, ajaxOptions, thrownError, ajaxDataResponse){
                $("#div-result").html(ajaxDataResponse+" : "+xhr.responseText);
                if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                    Loading.hide();
                }
                scrollUp("slow");
            };
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
        });
    },                

    mostrarVentanaModalRegistro: function(div, title, close){
        $(div).removeClass("hide").dialog({
            width: 800,
            resizable: false,
            draggable: false,
            title: "<div class='widget-header widget-header-small'><h5 class='smaller'>"+title+"</h5></div>",
            title_html: true,
            modal: true,
            buttons: [
                {
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-danger btn-xs",
                    "id": "btn-volver-indicadores",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar",
                    "class": "btn btn-primary btn-xs",
                    "id": "btn-guardar-indicadores",
                    click: function() {
                        $("#submitGuardarindicadores").click();
                    }
                }
            ],
            close: function() {
                $(this).dialog('close');
                if(typeof close == "function" ){
                    close();
                }
            }
        });
        indicadores.eventosFormulario();
    },
    
    mostrarVentanaModalCambiarEstatus: function(div, title, href){
        $(div).removeClass("hide").dialog({
            width: 800,
            resizable: false,
            draggable: false,
            title: "<div class='widget-header widget-header-small'><h5 class='smaller'>"+title+"</h5></div>",
            title_html: true,
            modal: true,
            buttons: [
                {
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-danger btn-xs",
                    "id": "btn-volver-indicadores",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp; "+title,
                    "class": "btn btn-warning btn-xs",
                    "id": "btn-cambiar-estatus-indicadores",
                    click: function() {
                        
                        var divResult = "#div-resultado-indicadores";
                        var urlDir = href;
                        var datos = null;
                        var loadingEfect = true;
                        var showResult = true;
                        var method = "DELETE";
                        var responseFormat = 'html';
                        var beforeSendCallback = function(){
                            if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                                Loading.show();
                            }
                        };
                        var successCallback = function (response) {
                            if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                                Loading.hide();
                            }
                            indicadores.refrescarIndicadoresCategoria();
                            $("#btn-volver-indicadores").click();
                        };
                        var errorCallback = function(xhr, ajaxOptions, thrownError, ajaxDataResponse){
                            $("#div-resultado-indicadores").html(ajaxDataResponse+" : "+xhr.responseText);
                            if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                                Loading.hide();
                            }
                        };
                        executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
                        
                    }
                }
            ],
            close: function() {
                $(this).dialog('close');
            }
        });
        indicadores.eventosFormulario();
    },

    refrescarIndicadoresCategoria: function(){
        console.log(this.categoria);
        var divResult = "#div-indicadores";
        var urlDir = "/indicadores/indicadorCategoria/lista/cat/"+ base64_encode(this.categoriaId)+"/dep/"+base64_encode(this.departamentoId);
        var datos = { categoria: this.categoria};
        var loadingEfect = true;
        var showResult = true;
        var method = 'GET';
        var responseFormat = 'html';
        var beforeSendCallback = null;
        var successCallback = function(response){
            indicadores.manejarEventosLista();
            $('html,body').animate({scrollTop: $("#div-indicadores").offset().top}, 'slow');
        };
        executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);

    }
};



