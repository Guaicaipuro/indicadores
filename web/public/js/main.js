var dialog = [];

dialog['error'] = 'errorDialogBox';
dialog['exito'] = 'successDialogBox';
dialog['alerta'] = 'alertDialogBox';
dialog['info'] = 'infoDialogBox';
dialog['success'] = 'successDialogBox';
dialog['alert'] = 'alertDialogBox';
dialog['errorDialogBox'] = 'errorDialogBox';
dialog['successDialogBox'] = 'successDialogBox';
dialog['alertDialogBox'] = 'alertDialogBox';
dialog['infoDialogBox'] = 'infoDialogBox';

var ajaxDataResponse = {};
var ajaxestatusCode = {};
var ajaxDom = {};
var ajaxError = {};

function displayHtmlInDivId(divResult, dataHtml, conEfecto) {
    if (conEfecto) {
        var elemento = "#" + divResult;
        elemento = elemento.replace("##", "#");
        $(elemento).html("").html(dataHtml).fadeIn();
    } else {
        var elemento = "#" + divResult;
        elemento = elemento.replace("##", "#");
        $(elemento).html("").html(dataHtml);
    }
}

/**
 *
 * @param string divResult id del div sin el #
 * @param string style error, info, alert
 * @param string mensaje un mensaje
 */
function displayDialogBox(divResult, style, mensaje,efecto,dismissable) {
    var classStyle = dialog[style];
    if(dismissable){
        var dataHtml = "<div class='" + classStyle + "'><button style=\"padding-right: 13px;\" onclick=\"$(this).parent().fadeOut('slow');\" class=\"close\" type=\"button\"><span aria-hidden=\"true\">&times;</span></button><p>" + mensaje + "</p></div>";
    }
    else {
        var dataHtml = "<div class='" + classStyle + "'><p>" + mensaje + "</p></div>";
    }

    displayHtmlInDivId(divResult, dataHtml,efecto);
}


/**
 *
 * @param String divResult id del div sin el #
 * @param String style error, info, alert
 * @param String mensaje un mensaje
 * @returns String
 */
function getDialogBox(style, mensaje) {
    var classStyle = dialog[style];
    var dataHtml = "<div class='" + classStyle + "'><p>" + mensaje + "</p></div>";
    return dataHtml;
}

/**
 * Esta funcion efectúa una petición ajax mediante la funcion $.ajax de jquery
 * @param string divResult Ej: "div_result"
 * @param string urlDir
 * @param string datos datos serializados Ej: "name=gabriel&age=20&hair=long"
 * @param boolean conEfecto
 * @param boolean showHTML
 * @param string method POST, GET, entre otros...
 * @param function callback
 */
function executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback) {

    if (!method) {
        method = "POST";
    }

    $.ajax({
        type: method,
        url: urlDir,
        dataType: "html",
        data: datos,
        beforeSend: function() {
            if (conEfecto) {
                var url_image_load = "<div class='center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
                displayHtmlInDivId(divResult, url_image_load);
            }
        },
        success: function(datahtml) {
            ajaxDataResponse = datahtml;
            if (showHTML) {
                displayHtmlInDivId(divResult, datahtml, conEfecto);
            }
            if (typeof callback == "function" && callback) {
                callback.call();
            }
        },
        statusCode: {
            404: function() {
                displayDialogBox(divResult, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
            },
            400: function() {
                displayDialogBox(divResult, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
            },
            401: function() {
                displayDialogBox(divResult, "error", "401: Datos insuficientes para efectuar esta acci&oacute;n.");
            },
            403: function() {
                displayDialogBox(divResult, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
            },
            500: function() {
                displayDialogBox(divResult, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
            },
            503: function() {
                displayDialogBox(divResult, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
            },
            999: function(resp) {
                displayDialogBox(divResult, "error", resp.status + ': ' + resp.responseText);
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
            if (xhr.status == '403') {
                displayDialogBox(divResult, "error", "Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
            } else if (xhr.status == '401') {
                displayDialogBox(divResult, "error", "Datos insuficientes para efectuar esta acci&oacute;n..");
            } else if (xhr.status == '400') {
                displayDialogBox(divResult, "error", "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.");
            } else if (xhr.status == '500') {
                displayDialogBox(divResult, "error", "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
            } else if (xhr.status == '503') {
                displayDialogBox(divResult, "error", "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
            }
        }
    });
}


/**
 * Esta funcion efectúa una petición ajax mediante la funcion $.ajax de jquery con manejo de errores.
 *
 * @param string divResult Indica el string selector jquery donde se mostrará el resultado.
 * @param string urlDir Dirección a donde se efectuará la petición Ajax.
 * @param mixed datos los datos a enviar junto a la petición ajax, puede estar en formato string serializado o en formato json...
 * @param boolean loadingEfect Indica si muestra el efecto de cargando o no.
 * @param boolean showResult Indica si muestra el Resultado de la petición Ajax o no.
 * @param string method POST, GET, entre otros...
 * @param string responseFormat json, html, xml...
 * @param function beforeSendCallback function que se ejecutará antes de enviar la petición.
 * @param function successCallback function que se ejecutará luego de enviar la petición, se recibe el dataResponse.
 * @param function errorCallback function que se ejecutará si se produce un error
 */
function executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback) {

    if (!method) {
        method = "POST";
    }

    if (!responseFormat) {
        responseFormat = "html";
    }

    $.ajax({
        type: method,
        url: urlDir,
        dataType: responseFormat,
        data: datos,
        beforeSend: function() {
            if (loadingEfect) {
                var url_image_load = "<div class='padding-5 center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
                displayHtmlInDivId(divResult, url_image_load);
            }
            if (typeof beforeSendCallback == "function" && beforeSendCallback) {
                beforeSendCallback();
            }
        },
        success: function(response, estatusCode, dom) {
            ajaxDataResponse = response;
            ajaxestatusCode = estatusCode;
            ajaxDoom = dom;
            //ajaxDataResponse.error = null;
            if (showResult) {
                displayHtmlInDivId(divResult, response, loadingEfect);
            }
            if (typeof beforeSendCallback == "function" && beforeSendCallback && typeof successCallback != "function") {
                beforeSendCallback();
            }
            if (typeof successCallback == "function" && successCallback) {
                successCallback(response, estatusCode, dom);
            }
        },
        statusCode: {
            404: function() {
                displayDialogBox(divResult, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
            },
            400: function() {
                displayDialogBox(divResult, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
            },
            401: function() {
                displayDialogBox(divResult, "error", "401: Datos insuficientes para efectuar esta acci&oacute;n.");
            },
            403: function() {
                displayDialogBox(divResult, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
            },
            500: function() {
                displayDialogBox(divResult, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
            },
            503: function() {
                displayDialogBox(divResult, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
            },
            999: function(resp) {
                displayDialogBox(divResult, "error", resp.status + ': ' + resp.responseText);
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            //alert(xhr);
            //alert(thrownError);
            ajaxError = xhr;
            //ajaxDataResponse.error = xhr;
            //ajaxDataResponse['mensaje'] = '';
            if (xhr.status == '403') {
                ajaxDataResponse['mensaje'] = "Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.";
                displayDialogBox(divResult, "error", "Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
            } else if (xhr.status == '401') {
                ajaxDataResponse['mensaje'] = "Datos insuficientes para efectuar esta acci&oacute;n.";
                displayDialogBox(divResult, "error", "Datos insuficientes para efectuar esta acci&oacute;n.");
            } else if (xhr.status == '400') {
                ajaxDataResponse['mensaje'] = "Datos insuficientes para efectuar esta acci&oacute;n.";
                displayDialogBox(divResult, "error", "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.");
            } else if (xhr.status == '500') {
                ajaxDataResponse['mensaje'] = "Datos insuficientes para efectuar esta acci&oacute;n.";
                displayDialogBox(divResult, "error", "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
            } else if (xhr.status == '503') {
                ajaxDataResponse['mensaje'] = "Datos insuficientes para efectuar esta acci&oacute;n.";
                displayDialogBox(divResult, "error", "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
            }
            if (typeof errorCallback == "function" && errorCallback) {
                errorCallback(xhr, ajaxOptions, thrownError, ajaxDataResponse['mensaje']);
            }
        }
    });
}

/**
 * Esta funcion efectúa una petición ajax mediante la funcion $.ajax de jquery con manejo de errores.
 *
 * @param string divResult Indica el string selector jquery por atributo ID (#) donde se mostrará el resultado.
 * @param string urlDir Dirección a donde se efectuará la petición Ajax.
 * @param mixed datos los datos a enviar junto a la petición ajax, puede estar en formato string serializado o en formato json...
 * @param boolean loadingEfect Indica si muestra el efecto de cargando o no.
 * @param boolean showResult Indica si muestra el Resultado de la petición Ajax o no.
 * @param string method POST, GET, entre otros...
 * @param string responseFormat json, html, xml...
 * @param function successCallback function que se ejecutará luego de enviar la petición, se recibe el dataResponse.
 * @param function errorCallback function que se ejecutará si se produce un error
 * @param function beforeSendCallback function que se ejecutará antes de enviar la petición.
 */
function executeAjaxRequest(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, beforeSendCallback, errorCallback) {

    if (!method) {
        method = "POST";
    }

    if (!responseFormat) {
        responseFormat = "html";
    }

    $.ajax({
        type: method,
        url: urlDir,
        dataType: responseFormat,
        data: datos,
        beforeSend: function() {
            if (loadingEfect) {
                var url_image_load = "<div class='padding-5 center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
                displayHtmlInDivId(divResult, url_image_load);
            }
            if (typeof beforeSendCallback == "function" && beforeSendCallback) {
                beforeSendCallback();
            }
        },
        success: function(response, estatusCode, dom) {
            ajaxDataResponse = response;
            ajaxestatusCode = estatusCode;
            ajaxDoom = dom;
            ajaxDataResponse.error = null;
            if (showResult) {
                displayHtmlInDivId(divResult, response, loadingEfect);
            }
            if (typeof beforeSendCallback == "function" && beforeSendCallback) {
                beforeSendCallback(response);
            }
            if (typeof successCallback == "function" && successCallback) {
                successCallback(response, estatusCode, dom);
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            ajaxDataResponse = {mensaje: '', response: null, error: null};
            ajaxError = xhr;
            ajaxDataResponse.error = xhr;
            ajaxDataResponse.mensaje = '';
            if (xhr.status == '403') {
                ajaxDataResponse.mensaje = "Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.";
            } else if (xhr.status == '404') {
                ajaxDataResponse.mensaje = "ERROR 404: No se ha podido encontrar el recurso solicitado.";
            } else if (xhr.status == '401') {
                ajaxDataResponse.mensaje = "Datos insuficientes para efectuar esta acci&oacute;n.";
            } else if (xhr.status == '400') {
                ajaxDataResponse.mensaje = "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.";
            } else if (xhr.status == '500') {
                ajaxDataResponse.mensaje = "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.";
            } else if (xhr.status == '503') {
                ajaxDataResponse.mensaje = "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.";
            } else if (xhr.status == '302') {
                ajaxDataResponse.mensaje = "El servidor ha intentado redirigirle a una nueva página.";
                if (xhr.redirect) {
                    // data.redirect contains the string URL to redirect to
                    window.location.replace(xhr.redirect);
                }
            }
            
            displayDialogBox(divResult, "error", ajaxDataResponse.mensaje);
            if (typeof errorCallback == "function" && errorCallback) {
                errorCallback(xhr, ajaxOptions, thrownError, ajaxDataResponse.mensaje);
            }
        }
    });
}


/**
 * Esta funcion permite restringir los valores ingresados en un elemento (Solo Alfanumericos). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyAlphaNum(this, true, true);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_space Denota si debe o no tener Espacios
 * @param Boolean with_spanhol Denota si debe o no tener SÃ­mbolos o Letras del EspaÃ±ol.
 */
function keyAlphaNum(element, with_space, with_spanhol) {

    if (with_space && with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\- ]/g)) {
            element.value = element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\- ]/g, '');
        }
//alert('1.- '+with_space+with_spanhol);
    } else if (with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-]/g)) {
            element.value = element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-]/g, '');
        }
//alert('2.- '+with_space+with_spanhol);
    } else if (with_space) {
        if (element.value.match(/[^0-9a-zA-Z\- ]/g)) {
            element.value = element.value.replace(/[^0-9a-zA-Z\- ]/g, '');
        }
//alert('3.- '+with_space+with_spanhol);
    } else {
        if (element.value.match(/[^0-9a-zA-Z\-]/g)) {
            element.value = element.value.replace(/[^0-9a-zA-Z\-]/g, '');
        }
//alert('4.- '+with_space+with_spanhol);
    }

}

/**
 * Esta funcion permite restringir los valores ingresados en un elemento (Texto). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyText(this, true);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_spanhol Denota si debe o no tener SÃ­mbolos o Letras del EspaÃ±ol lo que se ingrese mediante teclado en el campo.
 */
function keyText(element, with_spanhol) {
    if (with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ(\n)\-.(),;:_ ]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ(\n)\-.(),;:_ ]/g, ''));
        }
    } else {
        if (element.value.match(/[^0-9a-zA-Z(\n)\-.(),;:_ ]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-Z(\n)\-.(),;:_ ]/g, ''));
        }
    }
}

/**
 * Esta funcion permite restringir los valores ingresados en un elemento (Texto). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyText(this, true);
 * });
 *
 * PERMITE ADICIONAL A LOS CARACTERES COMUNES LAS LETRAS Ññ, - (GUION), () Y ACENTOS.
 *
 * @param Javascript Element element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_spanhol Denota si debe o no tener SÃ­mbolos o Letras del EspaÃ±ol lo que se ingrese mediante teclado en el campo.
 */
function keyTextDash(element, with_spanhol, with_space) {
    if (with_space && with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-() ]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-() ]/g, ''));
        }
    } else {
        if (element.value.match(/[^0-9a-zA-Z\-.()]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-Z-() ]/g, ''));

        }
    }
}

/**
 * Esta funcion permite restringir los valores ingresados en un elemento (Texto). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyTextOnly(this);
 * });
  * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 */
function keyTextOnly(element) {
    if (element.value.match(/[^a-zA-Z]/g)) {
        element.value = $.trim(element.value.replace(/[^a-zA-Z]/g, ''));
    }
}
function keyAlpha(element, with_spanhol) {
    if (with_spanhol) {
        if (element.value.match(/[^a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-.(),;: ]/g)) {
            element.value = $.trim(element.value.replace(/[^a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-.(),;: ]/g, ''));
        }
    } else {
        if (element.value.match(/[^a-zA-Z]/g)) {
            element.value = $.trim(element.value.replace(/[^a-zA-Z]/g, ''));
        }
    }
}

function keyLettersAndSpaces(element, with_spanhol) {
    if (with_spanhol) {
        if (element.value.match(/[^a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\ ]/g)) {
            element.value = $.trim(element.value.replace(/[^a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\ ]/g, ''));
        }
    } else {
        if (element.value.match(/[^a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\ ]/g)) {
            element.value = $.trim(element.value.replace(/[^a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\ ]/g, ''));
        }
    }
}


function keyHexa(element, with_dash) {
    if (with_dash) {
        if (element.value.match(/[^0-9a-fA-F\-]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-fA-F\-]/g, ''));
        }
    } else {
        if (element.value.match(/[^0-9a-fA-F]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-fA-F]/g, ''));
        }
    }
}


/**
 * Esta funcion permite restringir los valores ingresados en un elemento (NÃºmeros). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyNum(this, false);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_point Denota si debe o no tener puntos (.) lo que se ingrese mediante teclado en el campo.
 */
function keyNum(element, with_point, negative) {

    if (with_point) {
        if (negative) {
            if (element.value.match(/[^0-9.\-]/g)) {
                element.value = $.trim(element.value.replace(/[^0-9.\-]/g, ''));
            }
        }
        else {
            if (element.value.match(/[^0-9.]/g)) {
                element.value = $.trim(element.value.replace(/[^0-9.]/g, ''));
            }
        }
    } else {
        if (negative) {
            if (element.value.match(/[^0-9\-]/g)) {
                element.value = $.trim(element.value.replace(/[^0-9\-]/g, ''));
            }
        } else {
            if (element.value.match(/[^0-9]/g)) {
                element.value = $.trim(element.value.replace(/[^0-9]/g, ''));
            }
        }
    }
}

/**
 * Esta funcion permite restringir los valores ingresados en un elemento (NÃºmeros). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyNum(this, false);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_point Denota si debe o no tener puntos (.) lo que se ingrese mediante teclado en el campo.
 */
function keyNumCompare(element, with_point) {

    if (with_point) {
        if (element.value.match(/[^0-9.<>=]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9.<>=]/g, ''));
        }
    } else {
        if (element.value.match(/[^0-9<>=]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9<>=]/g, ''));
        }
    }
}


/**
 * Esta funcion permite restringir los valores ingresados en un elemento (Texto). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyTwitter(this, true);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_spanhol Denota si debe o no tener SÃ­mbolos o Letras del EspaÃ±ol lo que se ingrese mediante teclado en el campo.
 */
function keyTwitter(element, with_spanhol) {
    if (with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ_@]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ_@]/g, ''));
        }
    } else {
        if (element.value.match(/[^0-9a-zA-Z_@]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-Z_@]/g, ''));
        }
    }
}

/**
 * Esta funcion permite restringir los valores ingresados en un elemento (Texto). Su utilizacion debe ser activada mediante un evento HTML.
 * Ej.: $('#mi_campo').bind('keyup blur', function () {
 * keyEmail(this, true);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 * @param Boolean with_spanhol Denota si debe o no tener SÃ­mbolos o Letras del EspaÃ±ol lo que se ingrese mediante teclado en el campo.
 */
function keyEmail(element, with_spanhol) {
    if (with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-._@]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-._@]/g, ''));
        }
    } else {
        if (element.value.match(/[^0-9a-zA-Z\-._@]/g)) {
            element.value = $.trim(element.value.replace(/[^0-9a-zA-Z\-._@]/g, ''));
        }
    }
}

/**
 * Esta función permite limpiar de espacios al inicio o final de los valores ingresados en un campo.
 * Ej.: $('#mi_campo').bind('blur', function () {
 * clearField(this);
 * });
 *
 * @param JavascriptElement element Puede Ser un Campo de Texto. Ej.: this
 */
function clearField(element) {
    element.value = $.trim(element.value);
}

function makeUpper(f) {
    $(f).val($(f).val().toUpperCase());
}

function makeLower(f) {
    $(f).val($(f).val().toLowerCase());
}

function slug(Text) {
    return Text
            .toLowerCase()
            .replace(/ /g, '-')
            .replace(/[^\w-]+/g, '')
            ;
}

function isValidDate(date) {
    var matches = /^(\d{2})[-\/](\d{2})[-\/](\d{4})$/.exec(date);
    if (matches == null)
        return false;
    var d = matches[2]*1;
    var m = matches[1]-1;
    var y = matches[3]*1;
    var composedDate = new Date(y, m, d);
    return composedDate.getDate() == d &&
            composedDate.getMonth() == m &&
            composedDate.getFullYear() == y;
}
/**
 * 
 * @param {String} date Fecha en Formato DD-MM-YYYY
 * @returns {Boolean}
 */
function isValidAppDate(date) {
    var matches = /^(\d{2})[-\/](\d{2})[-\/](\d{4})$/.exec(date);
    if (matches == null)
        return false;
    //console.log(matches);
    var d = matches[1]*1;
    var m = matches[2]-1;
    var y = matches[3]*1;
    var composedDate = new Date(y, m, d);
    return composedDate.getDate() == d &&
            composedDate.getMonth() == m &&
            composedDate.getFullYear() == y;
}

/**
 * 
 * @param {String} date Fecha en Formato YYYY-MM-DD
 * @returns {Boolean}
 */
function isValidServerDate(date) {
    var matches = /^(\d{4})[-\/](\d{2})[-\/](\d{2})$/.exec(date);
    if (matches == null)
        return false;
    var m = matches[2]-1;
    var y = matches[1]*1;
    var d = matches[3]*1;
    var composedDate = new Date(y, m, d);

    return composedDate.getDate() == d &&
            composedDate.getMonth() == m &&
            composedDate.getFullYear() == y;
}


/**
 *
 * @param String phone
 * @param String type
 * @returns {Boolean}
 */
function isValidPhone(phone, type) {

    phone = phone + "";
    phone = phone.trim();

    if (phone.length == 11 || phone.length == 10) {

        if (type == "fijo" || type == "fixed") {

            if (!startWith(phone, "02") && !startWith(phone, "2")) {
                return false;
            } else if (isNaN(phone)) {
                return false;
            }
            else {
                return true;
            }

        } else if (type == 'movil' || type == 'mobile') {

            var movilnet1 = "0416";
            var movilnet2 = "0426";
            var movistar1 = "0414";
            var movistar2 = "0424";
            var digitel1 = "0412";

            if (!startWith(phone, movilnet1) && !startWith(phone, movilnet2) && !startWith(phone, movistar1) && !startWith(phone, movistar2) && startWith(phone, digitel1) && !startWith(phone, movilnet1 * 1) && !startWith(phone, movilnet2 * 1) && !startWith(phone, movistar1 * 1) && !startWith(phone, movistar2 * 1) && startWith(phone, digitel1 * 1)) {
                return false;
            } else if (isNaN(phone)) {
                return false;
            }
            else {
                return true;
            }

        } else {
            return false;
        }

    } else {
        return false;
    }
}


/**
 * Validate email function with regualr expression
 *
 * If email isn't valid then return false
 *
 * @param email
 * @return Boolean
 */
function isValidEmail(email) {

    var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    var valid = emailReg.test(email);

    if (!valid) {
        return false;
    } else {
        return true;
    }

}

function isValidTwitter(twitter) {

    var twitterReg = new RegExp(/(^|[^@\w])@(\w{1,15})\b/);
    var valid = twitterReg.test(twitter);

    if (!valid) {
        return false;
    } else {
        return true;
    }

}

function startWith(subject, search) {

    if (subject.indexOf(search) == 0) {
        return true;
    }

    return false;

}

function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function scrollUp(speed) {
    if (speed == null || speed == '' || speed != "fast" || speed != "slow") {
        speed = "fast";
    }
    console.log(speed);
    $("html, body").animate({scrollTop: 0}, speed);
}

//function mostrarNotificacion() {
//    new PNotify({
//        title: '<font size="3.5"><strong>Proceso de Matriculación</strong></font>',
//        text: '<p style="text-align: justify">Estimado usuario, esta tarea puede tardar varios minutos. Espere mientras se culmina el proceso.</p>',
//        icon: 'icon-group',
//        animate_speed: 700,
//        delay: 5000,
//        styling: 'fontawesome',
//        animation: {
//            'effect_in': 'drop',
//            'options_in': {easing: 'easeOutBounce'},
//            'effect_out': 'drop',
//            'options_out': {easing: 'easeInExpo'},
//        }
//    });
//}

jQuery.fn.reset = function() {
    $(this).each(function() {
        this.reset();
    });
};

function base64_encode(data) {
    // discuss at: http://phpjs.org/functions/base64_encode/
    // original by: Tyler Akins (http://rumkin.com)
    // improved by: Bayron Guevara
    // improved by: Thunder.m
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Rafał Kukawski (http://kukawski.pl)
    // bugfixed by: Pellentesque Malesuada
    // example 1: base64_encode('Kevin van Zonneveld');
    // returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='
    // example 2: base64_encode('a');
    // returns 2: 'YQ=='
    // example 3: base64_encode('✓ à la mode');
    // returns 3: '4pyTIMOgIGxhIG1vZGU='

    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
            ac = 0,
            enc = '',
            tmp_arr = [];

    if (!data) {
        return data;
    }

    data = unescape(encodeURIComponent(data))

    do {
        // pack three octets into four hexets
        o1 = data.charCodeAt(i++);
        o2 = data.charCodeAt(i++);
        o3 = data.charCodeAt(i++);

        bits = o1 << 16 | o2 << 8 | o3;

        h1 = bits >> 18 & 0x3f;
        h2 = bits >> 12 & 0x3f;
        h3 = bits >> 6 & 0x3f;
        h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < data.length);

    enc = tmp_arr.join('');

    var r = data.length % 3;

    return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
}
function base64_decode(data) {
    // discuss at: http://phpjs.org/functions/base64_decode/
    // original by: Tyler Akins (http://rumkin.com)
    // improved by: Thunder.m
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // input by: Aman Gupta
    // input by: Brett Zamir (http://brett-zamir.me)
    // bugfixed by: Onno Marsman
    // bugfixed by: Pellentesque Malesuada
    // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // example 1: base64_decode('S2V2aW4gdmFuIFpvbm5ldmVsZA==');
    // returns 1: 'Kevin van Zonneveld'
    // example 2: base64_decode('YQ===');
    // returns 2: 'a'
    // example 3: base64_decode('4pyTIMOgIGxhIG1vZGU=');
    // returns 3: '✓ à la mode'

    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
            ac = 0,
            dec = '',
            tmp_arr = [];

    if (!data) {
        return data;
    }

    data += '';

    do {
        // unpack four hexets into three octets using index points in b64
        h1 = b64.indexOf(data.charAt(i++));
        h2 = b64.indexOf(data.charAt(i++));
        h3 = b64.indexOf(data.charAt(i++));
        h4 = b64.indexOf(data.charAt(i++));

        bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

        o1 = bits >> 16 & 0xff;
        o2 = bits >> 8 & 0xff;
        o3 = bits & 0xff;

        if (h3 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1);
        } else if (h4 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1, o2);
        } else {
            tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
        }
    } while (i < data.length);

    dec = tmp_arr.join('');

    return decodeURIComponent(escape(dec.replace(/\0+$/, '')));
}

function chr(codePt) {
    //  discuss at: http://phpjs.org/functions/chr/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Brett Zamir (http://brett-zamir.me)
    //   example 1: chr(75) === 'K';
    //   example 1: chr(65536) === '\uD800\uDC00';
    //   returns 1: true
    //   returns 1: true

    if (codePt > 0xFFFF) { // Create a four-byte string (length 2) since this code point is high
        //   enough for the UTF-16 encoding (JavaScript internal use), to
        //   require representation with two surrogates (reserved non-characters
        //   used for building other characters; the first is "high" and the next "low")
        codePt -= 0x10000;
        return String.fromCharCode(0xD800 + (codePt >> 10), 0xDC00 + (codePt & 0x3FF));
    }
    return String.fromCharCode(codePt);
}
function ord(string) {
    //  discuss at: http://phpjs.org/functions/ord/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // bugfixed by: Onno Marsman
    // improved by: Brett Zamir (http://brett-zamir.me)
    //    input by: incidence
    //   example 1: ord('K');
    //   returns 1: 75
    //   example 2: ord('\uD800\uDC00'); // surrogate pair to create a single Unicode character
    //   returns 2: 65536

    var str = string + '',
        code = str.charCodeAt(0);
    if (0xD800 <= code && code <= 0xDBFF) { // High surrogate (could change last hex to 0xDB7F to treat high private surrogates as single characters)
        var hi = code;
        if (str.length === 1) {
            return code; // This is just a high surrogate with no following low surrogate, so we return its value;
            // we could also throw an error as it is not a complete character, but someone may want to know
        }
        var low = str.charCodeAt(1);
        return ((hi - 0xD800) * 0x400) + (low - 0xDC00) + 0x10000;
    }
    if (0xDC00 <= code && code <= 0xDFFF) { // Low surrogate
        return code; // This is just a low surrogate with no preceding high surrogate, so we return its value;
        // we could also throw an error as it is not a complete character, but someone may want to know
    }
    return code;
}

function md5(str) {
    //  discuss at: http://phpjs.org/functions/md5/
    // original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // improved by: Michael White (http://getsprink.com)
    // improved by: Jack
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    //    input by: Brett Zamir (http://brett-zamir.me)
    // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    //  depends on: utf8_encode
    //   example 1: md5('Kevin van Zonneveld');
    //   returns 1: '6e658d4bfcb59cc13f96c14450ac40b9'

    var xl;

    var rotateLeft = function(lValue, iShiftBits) {
        return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
    };

    var addUnsigned = function(lX, lY) {
        var lX4, lY4, lX8, lY8, lResult;
        lX8 = (lX & 0x80000000);
        lY8 = (lY & 0x80000000);
        lX4 = (lX & 0x40000000);
        lY4 = (lY & 0x40000000);
        lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
        if (lX4 & lY4) {
            return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
        }
        if (lX4 | lY4) {
            if (lResult & 0x40000000) {
                return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
            } else {
                return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
            }
        } else {
            return (lResult ^ lX8 ^ lY8);
        }
    };

    var _F = function(x, y, z) {
        return (x & y) | ((~x) & z);
    };
    var _G = function(x, y, z) {
        return (x & z) | (y & (~z));
    };
    var _H = function(x, y, z) {
        return (x ^ y ^ z);
    };
    var _I = function(x, y, z) {
        return (y ^ (x | (~z)));
    };

    var _FF = function(a, b, c, d, x, s, ac) {
        a = addUnsigned(a, addUnsigned(addUnsigned(_F(b, c, d), x), ac));
        return addUnsigned(rotateLeft(a, s), b);
    };

    var _GG = function(a, b, c, d, x, s, ac) {
        a = addUnsigned(a, addUnsigned(addUnsigned(_G(b, c, d), x), ac));
        return addUnsigned(rotateLeft(a, s), b);
    };

    var _HH = function(a, b, c, d, x, s, ac) {
        a = addUnsigned(a, addUnsigned(addUnsigned(_H(b, c, d), x), ac));
        return addUnsigned(rotateLeft(a, s), b);
    };

    var _II = function(a, b, c, d, x, s, ac) {
        a = addUnsigned(a, addUnsigned(addUnsigned(_I(b, c, d), x), ac));
        return addUnsigned(rotateLeft(a, s), b);
    };

    var convertToWordArray = function(str) {
        var lWordCount;
        var lMessageLength = str.length;
        var lNumberOfWords_temp1 = lMessageLength + 8;
        var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
        var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
        var lWordArray = new Array(lNumberOfWords - 1);
        var lBytePosition = 0;
        var lByteCount = 0;
        while (lByteCount < lMessageLength) {
            lWordCount = (lByteCount - (lByteCount % 4)) / 4;
            lBytePosition = (lByteCount % 4) * 8;
            lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount) << lBytePosition));
            lByteCount++;
        }
        lWordCount = (lByteCount - (lByteCount % 4)) / 4;
        lBytePosition = (lByteCount % 4) * 8;
        lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
        lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
        lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
        return lWordArray;
    };

    var wordToHex = function(lValue) {
        var wordToHexValue = '',
            wordToHexValue_temp = '',
            lByte, lCount;
        for (lCount = 0; lCount <= 3; lCount++) {
            lByte = (lValue >>> (lCount * 8)) & 255;
            wordToHexValue_temp = '0' + lByte.toString(16);
            wordToHexValue = wordToHexValue + wordToHexValue_temp.substr(wordToHexValue_temp.length - 2, 2);
        }
        return wordToHexValue;
    };

    var x = [],
        k, AA, BB, CC, DD, a, b, c, d, S11 = 7,
        S12 = 12,
        S13 = 17,
        S14 = 22,
        S21 = 5,
        S22 = 9,
        S23 = 14,
        S24 = 20,
        S31 = 4,
        S32 = 11,
        S33 = 16,
        S34 = 23,
        S41 = 6,
        S42 = 10,
        S43 = 15,
        S44 = 21;

    str = this.utf8_encode(str);
    x = convertToWordArray(str);
    a = 0x67452301;
    b = 0xEFCDAB89;
    c = 0x98BADCFE;
    d = 0x10325476;

    xl = x.length;
    for (k = 0; k < xl; k += 16) {
        AA = a;
        BB = b;
        CC = c;
        DD = d;
        a = _FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
        d = _FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
        c = _FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
        b = _FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
        a = _FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
        d = _FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
        c = _FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
        b = _FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
        a = _FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
        d = _FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
        c = _FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
        b = _FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
        a = _FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
        d = _FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
        c = _FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
        b = _FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
        a = _GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
        d = _GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
        c = _GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
        b = _GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
        a = _GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
        d = _GG(d, a, b, c, x[k + 10], S22, 0x2441453);
        c = _GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
        b = _GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
        a = _GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
        d = _GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
        c = _GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
        b = _GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
        a = _GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
        d = _GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
        c = _GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
        b = _GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
        a = _HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
        d = _HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
        c = _HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
        b = _HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
        a = _HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
        d = _HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
        c = _HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
        b = _HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
        a = _HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
        d = _HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
        c = _HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
        b = _HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
        a = _HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
        d = _HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
        c = _HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
        b = _HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
        a = _II(a, b, c, d, x[k + 0], S41, 0xF4292244);
        d = _II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
        c = _II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
        b = _II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
        a = _II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
        d = _II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
        c = _II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
        b = _II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
        a = _II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
        d = _II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
        c = _II(c, d, a, b, x[k + 6], S43, 0xA3014314);
        b = _II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
        a = _II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
        d = _II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
        c = _II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
        b = _II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
        a = addUnsigned(a, AA);
        b = addUnsigned(b, BB);
        c = addUnsigned(c, CC);
        d = addUnsigned(d, DD);
    }

    var temp = wordToHex(a) + wordToHex(b) + wordToHex(c) + wordToHex(d);

    return temp.toLowerCase();
}

/**
 * Dada un fecha en formato US(YYYY-MM-DD) devuelve la fecha en formato latino (DD-MM-AAAA)
 * @param String input
 * @returns String
 */
function toAppDate(input, separator){
    var response = input;
    var arrDate = [];
    if(separator===undefined || separator==null){
        separator = "-";
    }
    
    console.log(isValidServerDate(input));
    
    if(isValidServerDate(input)){
        arrDate = input.split(separator);
        if(arrDate.length==3){
            response = arrDate[2]+separator+arrDate[1]+separator+arrDate[0];
        }
    }
    return response;
}

function predicatBy(prop){
   return function(a,b){
      if( a[prop] > b[prop]){
          return 1;
      }else if( a[prop] < b[prop] ){
          return -1;
      }
      return 0;
   };
}

function levenshtein(s1, s2) {
  //       discuss at: http://phpjs.org/functions/levenshtein/
  //      original by: Carlos R. L. Rodrigues (http://www.jsfromhell.com)
  //      bugfixed by: Onno Marsman
  //       revised by: Andrea Giammarchi (http://webreflection.blogspot.com)
  // reimplemented by: Brett Zamir (http://brett-zamir.me)
  // reimplemented by: Alexander M Beedie
  //        example 1: levenshtein('Kevin van Zonneveld', 'Kevin van Sommeveld');
  //        returns 1: 3

  if (s1 == s2) {
    return 0;
  }

  var s1_len = s1.length;
  var s2_len = s2.length;
  if (s1_len === 0) {
    return s2_len;
  }
  if (s2_len === 0) {
    return s1_len;
  }

  // BEGIN STATIC
  var split = false;
  try {
    split = !('0')[0];
  } catch (e) {
    split = true; // Earlier IE may not support access by string index
  }
  // END STATIC
  if (split) {
    s1 = s1.split('');
    s2 = s2.split('');
  }

  var v0 = new Array(s1_len + 1);
  var v1 = new Array(s1_len + 1);

  var s1_idx = 0,
    s2_idx = 0,
    cost = 0;
  for (s1_idx = 0; s1_idx < s1_len + 1; s1_idx++) {
    v0[s1_idx] = s1_idx;
  }
  var char_s1 = '',
    char_s2 = '';
  for (s2_idx = 1; s2_idx <= s2_len; s2_idx++) {
    v1[0] = s2_idx;
    char_s2 = s2[s2_idx - 1];

    for (s1_idx = 0; s1_idx < s1_len; s1_idx++) {
      char_s1 = s1[s1_idx];
      cost = (char_s1 == char_s2) ? 0 : 1;
      var m_min = v0[s1_idx + 1] + 1;
      var b = v1[s1_idx] + 1;
      var c = v0[s1_idx] + cost;
      if (b < m_min) {
        m_min = b;
      }
      if (c < m_min) {
        m_min = c;
      }
      v1[s1_idx + 1] = m_min;
    }
    var v_tmp = v0;
    v0 = v1;
    v1 = v_tmp;
  }
  return v0[s1_len];
}

/**
 * 
 * //Example jQuery get cursor position function call
 * $("input[name='username']").getCursorPosition();
 *
 * @returns {Number}
 */
jQuery.fn.getCursorPosition = function() {
    if (this.lengh == 0)
        return -1;
    return $(this).getSelectionStart();
};

/**
 *
 * //Example jQuery set cursor position function call
 * $("input[name='username']").setCursorPosition(5);
 *
 * @param {type} position
 * @returns {jQuery.fn}
 */
jQuery.fn.setCursorPosition = function(position) {
    if (this.lengh == 0)
        return this;
    return $(this).setSelection(position, position);
};

/**
 *
 * //Example jQuery get text selection function call
 * $("input[name='username']").getSelection();
 *
 * @returns {Number}
 */
jQuery.fn.getSelection = function() {
    if (this.lengh == 0)
        return -1;
    var s = $(this).getSelectionStart();
    var e = $(this).getSelectionEnd();
    return this[0].value.substring(s, e);
};

/**
 * 
 * //Example jQuery get text selection start function call
 * $("input[name='username']").getSelectionStart();
 * 
 * @returns {document@call;getElementById.selectionStart|input.selectionStart|document@call;getElementById.value.length|choice.length|Number}
 */
jQuery.fn.getSelectionStart = function() {
    if (this.lengh == 0)
        return -1;
    input = this[0];

    var pos = input.value.length;

    if (input.createTextRange) {
        var r = document.selection.createRange().duplicate();
        r.moveEnd('character', input.value.length);
        if (r.text == '')
            pos = input.value.length;
        pos = input.value.lastIndexOf(r.text);
    } else if (typeof (input.selectionStart) != "undefined")
        pos = input.selectionStart;

    return pos;
};

/**
 *
 * //Example jQuery get text selection end function call
 * $("input[name='username']").getSelectionEnd();
 *
 * @returns {Number|document@call;getElementById.value.length|choice.length|document@call;getElementById.selectionEnd|input.selectionEnd}
 */
jQuery.fn.getSelectionEnd = function() {
    if (this.lengh == 0)
        return -1;
    input = this[0];

    var pos = input.value.length;

    if (input.createTextRange) {
        var r = document.selection.createRange().duplicate();
        r.moveStart('character', -input.value.length);
        if (r.text == '')
            pos = input.value.length;
        pos = input.value.lastIndexOf(r.text);
    } else if (typeof (input.selectionEnd) != "undefined")
        pos = input.selectionEnd;

    return pos;
};

/**
 *
 * //Example jQuery set text selection function call
 * $("input[name='username']").setSelection(4, 20);
 *
 * @param {type} selectionStart
 * @param {type} selectionEnd
 * @returns {jQuery.fn}
 */
jQuery.fn.setSelection = function(selectionStart, selectionEnd) {
    if (this.lengh == 0)
        return this;
    input = this[0];

    if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', selectionEnd);
        range.moveStart('character', selectionStart);
        range.select();
    } else if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(selectionStart, selectionEnd);
    }

    return this;
};

/**
 * //Example jQuery set text selection function between a range call
 * $('#elem').selectRange(3,5);
 *
 * @param {type} start
 * @param {type} end
 * @returns {jQuery.fn@call;each}
 */
jQuery.fn.selectRange = function(start, end) {
    return this.each(function() {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};

function isValidOrigen(origen) {
    var origenes = ['V', 'E', 'P'];
    // La función inArray de jQuery te devuelve el índice del array en donde se encuentra el valor buscado si no lo encuentra devuelve -1
    if ($.inArray(origen, origenes) >= 0) {
        return true;
    }
    return false;
}

function var_dump(input){
    console.log(input);
}

function htmlentities(e,t,n,r){var i=get_html_translation_table("HTML_ENTITIES",t),s="";e=e==null?"":e+"";if(!i){return false}if(t&&t==="ENT_QUOTES"){i["'"]="&#039;"}if(!!r||r==null){for(s in i){if(i.hasOwnProperty(s)){e=e.split(s).join(i[s])}}}else{e=e.replace(/([\s\S]*?)(&(?:#\d+|#x[\da-f]+|[a-zA-Z][\da-z]*);|$)/g,function(e,t,n){for(s in i){if(i.hasOwnProperty(s)){t=t.split(s).join(i[s])}}return t+n})}return e}

function get_html_translation_table(e,t){var n={},r={},i;var s={},o={};var u={},a={};s[0]="HTML_SPECIALCHARS";s[1]="HTML_ENTITIES";o[0]="ENT_NOQUOTES";o[2]="ENT_COMPAT";o[3]="ENT_QUOTES";u=!isNaN(e)?s[e]:e?e.toUpperCase():"HTML_SPECIALCHARS";a=!isNaN(t)?o[t]:t?t.toUpperCase():"ENT_COMPAT";if(u!=="HTML_SPECIALCHARS"&&u!=="HTML_ENTITIES"){throw new Error("Table: "+u+" not supported")}n["38"]="&";if(u==="HTML_ENTITIES"){n["160"]="&nbsp;";n["161"]="&iexcl;";n["162"]="&cent;";n["163"]="&pound;";n["164"]="&curren;";n["165"]="&yen;";n["166"]="&brvbar;";n["167"]="&sect;";n["168"]="&uml;";n["169"]="&copy;";n["170"]="&ordf;";n["171"]="&laquo;";n["172"]="&not;";n["173"]="&shy;";n["174"]="&reg;";n["175"]="&macr;";n["176"]="&deg;";n["177"]="&plusmn;";n["178"]="&sup2;";n["179"]="&sup3;";n["180"]="&acute;";n["181"]="&micro;";n["182"]="&para;";n["183"]="&middot;";n["184"]="&cedil;";n["185"]="&sup1;";n["186"]="&ordm;";n["187"]="&raquo;";n["188"]="&frac14;";n["189"]="&frac12;";n["190"]="&frac34;";n["191"]="&iquest;";n["192"]="&Agrave;";n["193"]="&Aacute;";n["194"]="&Acirc;";n["195"]="&Atilde;";n["196"]="&Auml;";n["197"]="&Aring;";n["198"]="&AElig;";n["199"]="&Ccedil;";n["200"]="&Egrave;";n["201"]="&Eacute;";n["202"]="&Ecirc;";n["203"]="&Euml;";n["204"]="&Igrave;";n["205"]="&Iacute;";n["206"]="&Icirc;";n["207"]="&Iuml;";n["208"]="&ETH;";n["209"]="&Ntilde;";n["210"]="&Ograve;";n["211"]="&Oacute;";n["212"]="&Ocirc;";n["213"]="&Otilde;";n["214"]="&Ouml;";n["215"]="&times;";n["216"]="&Oslash;";n["217"]="&Ugrave;";n["218"]="&Uacute;";n["219"]="&Ucirc;";n["220"]="&Uuml;";n["221"]="&Yacute;";n["222"]="&THORN;";n["223"]="&szlig;";n["224"]="&agrave;";n["225"]="&aacute;";n["226"]="&acirc;";n["227"]="&atilde;";n["228"]="&auml;";n["229"]="&aring;";n["230"]="&aelig;";n["231"]="&ccedil;";n["232"]="&egrave;";n["233"]="&eacute;";n["234"]="&ecirc;";n["235"]="&euml;";n["236"]="&igrave;";n["237"]="&iacute;";n["238"]="&icirc;";n["239"]="&iuml;";n["240"]="&eth;";n["241"]="&ntilde;";n["242"]="&ograve;";n["243"]="&oacute;";n["244"]="&ocirc;";n["245"]="&otilde;";n["246"]="&ouml;";n["247"]="&divide;";n["248"]="&oslash;";n["249"]="&ugrave;";n["250"]="&uacute;";n["251"]="&ucirc;";n["252"]="&uuml;";n["253"]="&yacute;";n["254"]="&thorn;";n["255"]="&yuml;"}if(a!=="ENT_NOQUOTES"){n["34"]="&quot;"}if(a==="ENT_QUOTES"){n["39"]="&#39;"}n["60"]="&lt;";n["62"]="&gt;";for(i in n){if(n.hasOwnProperty(i)){r[String.fromCharCode(i)]=n[i]}}return r}

String.prototype.capitalize = function() {return this.toLowerCase().charAt(0).toUpperCase() + this.slice(1);};

function containsHtml(str){
    return /<[a-z][\s\S]*>/i.test(str);
}

