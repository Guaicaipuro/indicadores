/**
 * Created by isalaz01 on 12/12/14.
 */
var matricula = angular.module('Matricula',['trNgGrid','ngResource']);

matricula.factory('preCaracterizarInterceptor',['$rootScope',function($rootScope) {
        return {
            request: function (config) {
                return config;
            },
            response: function(response) {
                if(response.data.status =="SUCCESS"){
                    window.location.href="/planteles/matricula15/caracterizarInscripcionAngularJs?c="+response.data.codigo;
                }
                else if(response.data.status =="ERROR"){
                    console.log(response.data);
                }
            },
            responseError : function(response){
            }

        }
    }
    ]
);

matricula.factory('getEstudiantesInterceptor',['$rootScope',function($rootScope) {
        return {
            request: function (config) {
                return config;
            },
            response: function(response) {
                if(response.data.status =="SUCCESS"){
                    $('#loadingWidget').addClass('hide');
                    $rootScope.estudiantesSinMatricular = response.data.estudiantes;
                    //console.log($rootScope.estudiantesSinMatricular);

                    $rootScope.listado_estudiantes_disponibles = [];
                    angular.forEach($rootScope.estudiantesSinMatricular,function(value,key){
                        id = {id:value.id};
                        $rootScope.listado_estudiantes_disponibles.push(id);
                    });
                    $(".contenidoInscripcion").removeClass('hide').fadeIn();

                }
                if(response.data.status =="ERROR"){
                    $('#loadingWidget').addClass('hide');
                    displayDialogBox('msgAlerta', response.data.classDialog, response.data.mensaje);
                }
            },
            responseError : function(response){
            }

        }
    }
    ]
);
matricula.factory('getEstudianteInterceptor',['$rootScope',function($rootScope) {
        return {
            request: function (config) {
                return config;
            },
            response: function(response) {
                $rootScope.msgLoadingEstud='Procesando Respuesta...';
                if(response.data.status == "SUCCESS"){
                    $('#loadingWidgetEstudDialog').addClass('hide');
                    $('#respuestaBusqueda2').addClass('hide');
                    $('#respuestaBusqueda').removeClass('hide');
                    $('#gridEstudBusqueda').removeClass('hide');

                    for (var i=0 ; i < response.data.estudiantes.length ; i++)
                    {
                        var estudiante = {
                            id: response.data.estudiantes[i].id,
                            nom_completo:response.data.estudiantes[i].nom_completo,
                            cedula_escolar:response.data.estudiantes[i].cedula_escolar,
                            fecha_nacimiento:response.data.estudiantes[i].fecha_nacimiento,
                            documento_identidad : response.data.estudiantes[i].documento_identidad,
                            tdocumento_identidad:response.data.estudiantes[i].tdocumento_identidad,
                            nuevoregistro : "no"
                        };
                        response.data.estudiantes[i].datos_inscripcion = estudiante;
                    }
                    $rootScope.estudiantesBusqueda = response.data.estudiantes;

                }
                if(response.data.status == "NO_EXISTE"){
                    $('#loadingWidgetEstudDialog').addClass('hide');
                    $('#respuestaBusqueda2').removeClass('hide');
                    $('#respuestaBusqueda').addClass('hide');
                    displayDialogBox('respuestaBusqueda2', response.data.classDialog, response.data.mensaje,true,true);
                }
                if(response.data.status =="ERROR"){
                    $('#loadingWidgetEstudDialog').addClass('hide');
                    $('#respuestaBusqueda2').removeClass('hide');
                    $('#respuestaBusqueda').addClass('hide');
                    displayDialogBox('respuestaBusqueda2', response.data.classDialog, response.data.mensaje,true,true);
                }
            },
            responseError : function(response){
                $('#loadingWidgetEstudDialog').addClass('hide');
                $('#respuestaBusqueda2').removeClass('hide');
                displayDialogBox('respuestaBusqueda2', response.data.classDialog, response.data.mensaje,true,true)
            }
        }
    }
    ]
);

matricula.factory('sendEstudianteIndividualInterceptor',['$rootScope',function($rootScope) {
        return {
            request: function (config) {
                return config;
            },
            response: function(response) {



                if(response.data.estatus=='success'){

                    $('#infoEstudiante').removeClass();
                    $('#infoRepresentante').removeClass();
                    $('#infoEstudiante').addClass('infoDialogBox');
                    $('#infoRepresentante').addClass('infoDialogBox');

                    $('#infoEstudiante').html("<p>Por Favor Ingrese los datos correspondientes, Los campos marcados con" +
                    "<b>" +
                    "<span class='required'>*</span>" +
                    "</b>" +
                    "son requeridos.</p>");
                    $('#infoRepresentante').html("<p>Por Favor Ingrese los datos correspondientes, Los campos marcados con" +
                    "<b>" +
                    "<span class='required'>*</span>" +
                    "</b>" +
                    "son requeridos.</p>");
                    //lastElementTop = $('#infoRepresentante').position().top ;
                    //$("html, body").animate({ scrollTop:lastElementTop+"px" }, "swing");


                }
                else if(response.data.estatus=='error'){
                    $('#infoEstudiante').removeClass();
                    $('#infoEstudiante').addClass('errorDialogBox');
                    $('#infoEstudiante').html(response.data.mensaje);
                    lastElementTop = $('#infoEstudiante').position().top ;
                    $("html, body").animate({ scrollTop:lastElementTop+"px" }, "swing");
                }
                else if(response.data.estatus=='errorEstudiante'){
                    $('#infoEstudiante').removeClass();
                    $('#infoEstudiante').html(response.data.mensaje);
                    lastElementTop = $('#infoEstudiante').position().top ;
                    $("html, body").animate({ scrollTop:lastElementTop+"px" }, "swing");
                }
                else if(response.data.estatus=='errorRepresentante'){
                    $('#infoRepresentante').removeClass();
                    $('#infoRepresentante').html(response.data.mensaje);
                    lastElementTop = $('#infoRepresentante').position().top ;
                    $("html, body").animate({ scrollTop:lastElementTop+"px" }, "swing");

                }
                return response;

            },
            responseError : function(response){
                return response;
            }
        }
    }
    ]
);
matricula.factory('getEstudiantesService', ['$resource', 'getEstudiantesInterceptor','getEstudianteInterceptor', function($resource, getEstudiantesInterceptor,getEstudianteInterceptor) {

        return $resource(
            "/",
            {},
            {
                buscarEstudiante: {method: 'GET', isArray: false, interceptor: getEstudianteInterceptor,url: "/planteles/matricula15/getEstudiante/"},
                buscarEstudiantes: {method: 'GET', isArray: false, interceptor: getEstudiantesInterceptor,url: "/planteles/matricula15/getEstudiantes/"}


            }
        );
    }
    ]
);
matricula.factory('getPrecaracterizarService', ['$resource','preCaracterizarInterceptor', function($resource,preCaracterizarInterceptor) {

        return $resource(
            "/",
            {},
            {
                preCaracterizar: {method: 'GET',interceptor:preCaracterizarInterceptor,url: "/planteles/matricula15/preCaracterizarInscripcionAngularJs/"}
            }
        );
    }
    ]
);
matricula.factory('sendEstudianteIndividual', ['$resource', 'sendEstudianteIndividualInterceptor', function($resource,sendEstudianteIndividualInterceptor) {

        return $resource(
            "/",
            {},
            {
                sendEstudiante: {method: 'POST', isArray: false, interceptor: sendEstudianteIndividualInterceptor,url: "/planteles/matricula15/agregarEstudiante"}
            }
        );
    }
    ]
);
matricula.filter('ageFilter', function () {
        function calculateAge (fecha_nacimiento) {
            var date = new Date(fecha_nacimiento);
            var ageDifMs = Date.now() - date.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }
        return function (unused,item) {
            return calculateAge(item.fecha_nacimiento);
        };
    }
);
matricula.filter("documentoIdentidadFilter", function () {
        return function (documento_identidad, estudiante) {
            var identificacion;
            if (estudiante.documento_identidad != '' && estudiante.documento_identidad != null) {
                if (estudiante.tdocumento_identidad != '' && estudiante.tdocumento_identidad != null) {
                    identificacion = estudiante.tdocumento_identidad + '-' + estudiante.documento_identidad;
                } else {
                    identificacion = estudiante.documento_identidad;
                }
            } else {
                if (estudiante.cedula_escolar != '' && estudiante.cedula_escolar != null) {
                    identificacion = 'C.E: ' + estudiante.cedula_escolar;
                } else {
                    identificacion = 'NO POSEE';
                }
            }
            return identificacion;
        };
    }
);
matricula.controller('MatriculaController',function($rootScope,$scope,$http,getEstudiantesService,getPrecaracterizarService,sendEstudianteIndividual) {
        $scope.estudSelecSinMatricular=[];
        $scope.estudSelecPreInscritos=[];
        $scope.estudiantesPreInscritos=[];

        $scope.patternNumbers = /^\d+$/;
        $scope.estudiante = {
            tdocumento_identidad:'',
            documento_identidad:'',
            nombres:'',
            apellidos:'',
            cedula_escolar:'',
            ci_representante:''
        };
        $scope.myLocale = 'es';
        $scope.defaultOrder = 'documento_identidad';


        $scope.alertOnSelectionChange = function(){
            $scope.$watch("estudSelecSinMatricular.length", function(){});
            $scope.$watch("estudSelecPreInscritos.length", function(){});
        };

        $scope.getEstudiantes = function(){
            var params = {
                seccion_plantel_id: $scope.seccion_plantel_id,
                plantel_id: $scope.plantel_id,
                periodo_id: $scope.periodo_id
            };
            getEstudiantesService.buscarEstudiantes(params);
        };

        $scope.inicializar = function(){
            //$scope.campos = {};
            $scope.tr_ng_grid_hide = 'hide';
            //$scope.campos = {};
            //$scope.DatosGenerales={};
            //$scope.DatosGenerales.nombreEstudiante='';
            //$scope.DatosGenerales.apellidoEstudiante='';
            //$scope.DatosGenerales.cedulaEscolar='';
            //$scope.DatosGenerales.cedula='1';
            //$scope.DatosGenerales.afinidad='';
            //$scope.DatosGenerales.sexo='';
            //$scope.DatosGenerales.lateralidad='';



            var defaultTranslation = {};
            var esTranslation = angular.extend({}, defaultTranslation,
                {
                    "Search": "Buscar",
                    "Page":"Página",
                    "First Page": "Primera Página",
                    "Next Page": "Página Siguiente",
                    "Previous Page": "Página Anterior",
                    "Last Page": "Última Página",
                    "Sort": "Ordenar",
                    "No items to display": "No se ha encontrado ningun resultado",
                    "displayed": "mostrando",
                    "in total": "en total"
                }
            );
            esTranslation[TrNgGrid.translationDateFormat] = "dd/MM/yyyy";
            TrNgGrid.translations["es"] = esTranslation;
            TrNgGrid.columnSortActiveCssClass = "tr-ng-sort-active text-info";
            TrNgGrid.columnSortInactiveCssClass = "tr-ng-sort-inactive text-muted ";
            TrNgGrid.columnSortReverseOrderCssClass = "tr-ng-sort-order-reverse icon-chevron-down";
            TrNgGrid.columnSortNormalOrderCssClass = "tr-ng-sort-order-normal icon-chevron-up";

            $scope.seccion_plantel_id = $("#seccion_plantel_id").val();
            $scope.plantel_id = $("#plantel_id").val();
            $scope.periodo_id = $("#periodo_id").val();
            $scope.grado_id = $("#grado_id").val();
            $scope.msgLoadingInit='Cargando estudiantes...';
            $('#loadingWidget').removeClass('hide');
            $('#wBoxInscripcion').click();
            $scope.getEstudiantes();
        };

        $scope.pushEstudiantePreInscrito = function(item,tipo_item) {
            if(tipo_item==1){
                var item_temp;
                var match_test=null;

                for (var i=0 ; i < $scope.estudiantesSinMatricular.length ; i++)
                {
                    if ($scope.estudiantesSinMatricular[i]['id'] == item.id) {
                        match_test = 1;
                        item_temp = $scope.estudiantesSinMatricular[i];
                    }
                }

                if(match_test == 1){
                    item = item_temp
                }
            }
            var match = null;
            if(item){

                if ($scope.estudiantesPreInscritos.length) {


                    for (var i=0 ; i < $scope.estudiantesPreInscritos.length ; i++)
                    {
                        if ($scope.estudiantesPreInscritos[i]['id'] == item.id) {
                            match = 1;
                        }
                    }
                    if(match){
                        $('#respuestaBusqueda2').removeClass('hide');
                        displayDialogBox('respuestaBusqueda2', 'alertDialogBox', 'Ya el estudiante fua asignado a la lista de Inscripcion',true,true);
                        //console.log('Ya fue asigando a lista de preinscripcnmsioi');
                    }else{


                        $scope.estudiantesPreInscritos.push(item);
                        $scope.estudiantesSinMatricular.splice($scope.estudiantesSinMatricular.indexOf(item),1);

                    }

                }else{
                    $scope.estudiantesPreInscritos.push(item);
                    $scope.estudiantesSinMatricular.splice($scope.estudiantesSinMatricular.indexOf(item),1);

                }

            }
            else {
                if ($scope.estudSelecSinMatricular.length) {
                    angular.forEach($scope.estudSelecSinMatricular, function (estudiante) {
                        $scope.estudiantesPreInscritos.push(estudiante);
                        $scope.estudiantesSinMatricular.splice($scope.estudiantesSinMatricular.indexOf(estudiante),1);
                    });
                }
                $scope.estudSelecSinMatricular = [];
            }
        };

        $scope.pushEstudianteSinMatricular = function(item){
            if(item){
                $scope.estudiantesSinMatricular.push(item);
                $scope.estudiantesPreInscritos.splice($scope.estudiantesPreInscritos.indexOf(item),1);
            }
            else {
                angular.forEach($scope.estudSelecPreInscritos, function (estudiante) {
                    $scope.estudiantesSinMatricular.push(estudiante);
                    $scope.estudiantesPreInscritos.splice($scope.estudiantesPreInscritos.indexOf(estudiante),1);

                });
                $scope.estudSelecPreInscritos = [];
            }
        };

        $scope.getEstudiante = function(){
            var params = {
                seccion_plantel_id: $scope.seccion_plantel_id,
                plantel_id: $scope.plantel_id,
                periodo_id: $scope.periodo_id,
                grado_id: $scope.grado_id,
                tdocumento_identidad: $scope.estudiante.tdocumento_identidad,
                documento_identidad: $scope.estudiante.documento_identidad,
                nombres: $scope.estudiante.nombres,
                apellidos: $scope.estudiante.apellidos,
                cedula_escolar: $scope.estudiante.cedula_escolar,
                ci_representante: $scope.estudiante.ci_representante,
                'probar_inscritos' : [$scope.estudiantesPreInscritos]
            };
            $('#loadingWidgetEstudDialog').removeClass('hide');
            $scope.msgLoadingEstud='Realizando la búsqueda...';
            getEstudiantesService.buscarEstudiante(params);

        };

        $scope.dialogEstudianteExistente = function () {
            $('#respuestaBusqueda').addClass('hide');
            // $scope.tr_ng_grid_hide = 'hide';
            $('#gridEstudBusqueda').addClass('hide');
            $('#loadingWidgetEstudDialog').addClass('hide');
            $("#incluir_Estudiante").removeClass('hide').dialog({
                    width: 900,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    position: ['center', 50],
                    title: "<div class='widget-header'><h4 class='smaller blue'><i class='icon-search blue'></i> Búsqueda de Estudiantes</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp;Volver",
                            "class": "btn btn-xs btn-danger",
                            click: function() {
                                $(this).dialog("close");
                            }
                        },
                    ]
                }
            );
        };
        $scope.dialogEstudianteNuevo = function () {
//            $('#respuestaBusqueda').addClass('hide');
            // $scope.tr_ng_grid_hide = 'hide';
            //          $('#gridEstudBusqueda').addClass('hide');
            //        $('#loadingWidgetEstudDialog').addClass('hide');
            $("#incluir_Estudiante_Nuevo").removeClass('hide').dialog({
                    width: 900,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    position: ['center', 50],
                    title: "<div class='widget-header'><h4 class='smaller blue'><i class='icon-search blue'></i> Nuevo Registro de Estudiante</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp;Volver",
                            "class": "btn btn-xs btn-danger",
                            click: function() {

                                $('#infoEstudiante').removeClass();
                                $('#infoRepresentante').removeClass();
                                $('#infoEstudiante').addClass('infoDialogBox');
                                $('#infoRepresentante').addClass('infoDialogBox');

                                $('#infoEstudiante').html("<p>Por Favor Ingrese los datos correspondientes, Los campos marcados con" +
                                "<b>" +
                                "<span class='required'>*</span>" +
                                "</b>" +
                                "son requeridos.</p>");
                                $('#infoRepresentante').html("<p>Por Favor Ingrese los datos correspondientes, Los campos marcados con" +
                                "<b>" +
                                "<span class='required'>*</span>" +
                                "</b>" +
                                "son requeridos.</p>");
                                //$scope.campos.DatosGenerales.nombreEstudiante = '';
                                //$scope.campos.DatosGenerales.apellidoEstudiante = '';
                                //$scope.campos.DatosGenerales.fechaNacimiento = '';
                                //$scope.campos.DatosGenerales.estatura = '';
                                //$scope.campos.DatosGenerales.lateralidad = '';
                                //$scope.campos.DatosGenerales.orden_nacimiento = '';
                                //$scope.campos.DatosGenerales.cedula_escolar = 0;
                                //$scope.campos.DatosGenerales.estadoEstudiante = '';
                                //$scope.campos.DatosGenerales.sexo = '';
                                //$scope.campos.DatosGenerales.afinidad = '';
                                $(this).dialog("close");

                            }
                        },
                        {
                            html: "<i class='icon-save bigger-110'></i>&nbsp;Guardar",
                            "class": "btn btn-xs btn-primary",
                            click: function() {
                                $('#guardarEstudianteNuevo').click();
                            }
                        }

                    ]
                }
            );
        };

        $scope.preInscripcion = function() {
            if ($scope.estudiantesPreInscritos.length) {


                var params = {
                    inscritos: [$scope.estudiantesPreInscritos],
                    seccion_plantel_id: $scope.seccion_plantel_id,
                    plantel_id: $scope.plantel_id

                };
                getPrecaracterizarService.preCaracterizar(params);
            }else{
                $('#msgAlerta').removeClass('hide');
                displayDialogBox('msgAlerta', 'alertDialogBox', 'Debe asignar algun estudiante al listado para continuar con la inscripci&oacute;n',true,true);
                lastElementTop = $('#msgAlerta').position().top ;
                scrollAmount = lastElementTop ;
                $("html, body").animate({ scrollTop:lastElementTop+"px" }, "swing");
                //lastElementTop = $('#msgAlerta').position().top ;
                //scrollAmount = lastElementTop ;
                //
                //$('#msgAlerta').animate({scrollTop: scrollAmount},1000);
            }


        };

        $scope.cargarNuevoEstudiante = function(){
            $scope.campos.DatosGenerales.plantel_id = atob($("#plantel_id").val());
            $scope.campos.DatosGenerales.fechaNacimiento = $("#fecha").val();
            $scope.campos.DatosGenerales.fecha_nacimiento = $("#fecha_nacimiento_representante").val();
            $scope.campos.DatosGenerales.cedulaEscolar = $("#cedula_escolar").val();
            $scope.campos.DatosGenerales.nombreRepresentante = $("#nombreRepresentante").val();
            $scope.campos.DatosGenerales.apellidoRepresentante = $("#apellidoRepresentante").val();
            $scope.campos.DatosGenerales.estado = $("#estado_id").val();

            // console.log( $scope.campos);
            sendEstudianteIndividual.sendEstudiante($scope.campos).$promise.then(function(response) {
                if(response.data.estatus=='success'){
                    $scope.pushEstudiantePreInscrito(response.data.estudiante);
                   // $scope.campos.DatosGenerales.cedula = '';
                    $scope.campos.DatosGenerales.nombreEstudiante = '';
                    $scope.campos.DatosGenerales.apellidoEstudiante = '';
                    $scope.campos.DatosGenerales.fechaNacimiento = '';
                    $scope.campos.DatosGenerales.estatura = '';
                    $scope.campos.DatosGenerales.lateralidad = '';
                    $scope.campos.DatosGenerales.orden_nacimiento = '';
                    $scope.campos.DatosGenerales.cedula_escolar = 0;
                    $scope.campos.DatosGenerales.estadoEstudiante = '';
                    $scope.campos.DatosGenerales.sexo = '';
                    $scope.campos.DatosGenerales.afinidad = '';


                    $("#incluir_Estudiante_Nuevo").dialog("close");
                }

                //console.log(response)
            });

        };
    }

);