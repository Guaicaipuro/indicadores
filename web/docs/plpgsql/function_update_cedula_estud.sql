CREATE OR REPLACE FUNCTION matricula.update_calumno_estudiante()
  RETURNS trigger AS
$BODY$
  DECLARE
vdocumento_identidad_old character varying (20);
vdocumento_identidad_new character varying (20);
vcedula_escolar_old character varying (20);
vcedula_escolar_new character varying (20);
vusername character varying(25);
vexiste_reg_doc integer = 0;
vexiste_reg_ced integer = 0;
fecha_hora timestamp;
  BEGIN

IF (TG_OP = 'UPDATE') THEN
SELECT INTO fecha_hora CURRENT_TIMESTAMP;
  vdocumento_identidad_old := OLD.documento_identidad;
  vdocumento_identidad_new := NEW.documento_identidad;
  vcedula_escolar_old := OLD.cedula_escolar;
  vcedula_escolar_new := NEW.cedula_escolar;

  -- VERIFICAMOS SI HUBO ALGUN CAMBIO EN EL DOCUMENTO DE IDENTIDAD
    IF (vdocumento_identidad_old <> vdocumento_identidad_new AND  vdocumento_identidad_old <> '' AND vdocumento_identidad_old <> null) THEN
          SELECT COUNT(1) INTO vexiste_reg_doc FROM legacy.talumno_acad WHERE calumno=vdocumento_identidad_old
          IF(vexiste_reg_doc > 0) THEN
            UPDATE legacy.talumno_acad
            SET calumno=vdocumento_identidad_new
            WHERE calumno=vdocumento_identidad_old
          END IF;
    END IF;

-- VERIFICAMOS SI HUBO ALGUN CAMBIO EN EL CEDULA ESCOLAR
    IF(vcedula_escolar_old <> vcedula_escolar_new  AND  vcedula_escolar_old <> '' AND vcedula_escolar_old <> null) THEN
    SELECT COUNT(1) INTO vexiste_reg_ced FROM legacy.talumno_acad WHERE calumno=vcedula_escolar_old;
          IF(vexiste_reg_ced > 0) THEN
          UPDATE legacy.talumno_acad
          SET calumno=vcedula_escolar_new
          WHERE calumno=vcedula_escolar_old
          END IF;


    END IF;


END IF;
RETURN OLD;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
