<?php
/* @var $this SiteController */
$this->pageTitle = Yii::app()->name;

?>

    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/styles-iview.css'; ?>" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/iview.css'; ?>" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/skin-4/style.css'; ?>" />

    <div class="col-xs-12">
        <div class="row row-fluid">
            <div class="accordion-style1 panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                                <i data-icon-show="icon-angle-right" data-icon-hide="icon-angle-down" class="bigger-120 icon-angle-down"></i>
                                &nbsp;AVISO IMPORTANTE
                            </a>
                        </h4>
                    </div>

                    <div id="collapseOne" class="panel-collapse in" style="height: auto;">
                        <div class="panel-body">
                            <!--<p style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
                                La Dirección de Registro y Control Académico ha activado desde hoy 29/09/14 el proceso de <strong>Carga de Matrícula</strong> correspondiente a los periodos escolares <strong>2013-2014</strong> y <strong>2014-2015</strong>.
                            </p>-->
                            <p style="text-align: justify;font-size:17px;font-family: 'Helvetica';">
                                La Dirección de Registro y Control Académico ha activado desde el día 20 de Febrero de 2015 el proceso de <strong>Carga de Matrícula</strong> correspondiente al periodo escolar año <strong>2014 – 2015</strong>.
                            </p>
                            <p style="text-align: justify;font-size:17px;font-family: 'Helvetica';">
                                La <strong>Carga de Matrícula</strong> para el periodo escolar año  <strong>2014 – 2015</strong>  abarcará desde el día <strong>20 de febrero al 01 de marzo</strong> sólo para el <strong>Nivel Inicial</strong>.
                            </p>
                            <!--<p style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
                                La Carga de Matrícula para el periodo escolar <strong>2014-2015</strong> estará disponible inicialmente para los niveles <strong>Inicial</strong> y <strong>Primaria</strong>.
                            </p>-->
                        </div>
                    </div>
                </div>

               <!-- <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle collapsed">
                                <i data-icon-show="icon-angle-right" data-icon-hide="icon-angle-down" class="bigger-110 icon-angle-right"></i>
                                &nbsp;DIA MUNDIAL DEL HABITAT Y LUCHA CONTRA EL DENGUE
                            </a>
                        </h4>
                    </div>

                    <div id="collapseTwo" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <p style="text-align: justify; font-size:15px;font-family: 'Helvetica';" > La Asamblea General de la Organización de Naciones Unidas (ONU), en 1985
                            estableció el primer lunes del mes de octubre como Día Mundial del Hábitat.
                            El Día Mundial del Hábitat, nos brinda la oportunidad para reflexionar sobre el estado
                            actual de las ciudades y la vivienda en general y tomar acciones para hacer de las
                            ciudades comunidades más seguras y más habitables para todas y todos... <a href="http://nube.fundabit.gob.ve/public.php?service=files&t=96bbf43d46e44180670a8ab428742855&download"> Ver más</a> </p>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/raphael-min.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.easing.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.fullscreen.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/iview.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/slider.js',CClientScript::POS_END); ?>