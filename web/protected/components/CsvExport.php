<?php
/**
 *   CsvExport
 *
 *   helper class to output an CSV from a CActiveRecord array.
 *
 *   example usage:
 *
 *       CsvExport::export(
 *           People::model()->findAll(), // a CActiveRecord array OR any CModel array
 *           array(
 *               'idpeople'=>array('number'),      'number' and 'date' are strings used by CFormatter
 *               'birthofdate'=>array('date'),
 *           )
 *       ,true,'registros-hasta--'.date('d-m-Y H-i').".csv");
 *
 *
 *   Please refer to CFormatter about column definitions, this class will use CFormatter.
 *
 *   @author    Christian Salazar <christiansalazarh@gmail.com> @bluyell @yiienespanol (twitter)
 *   @licence Protected under MIT Licence.
 *   @date 07 october 2012.
 */
class CsvExport {

    protected static $separator = ";";
    protected static $replaceSeparator = ". ";

    /*
        export a data set to CSV output.

        Please refer to CFormatter about column definitions, this class will use CFormatter.

        @rows    CModel array. (you can use a CActiveRecord array because it extends from CModel)
        @coldefs    example: 'colname'=>array('number') (See also CFormatter about this string)
        @boolPrintRows    boolean, true print col headers taken from coldefs array key
        @csvFileName if set (defaults null) it echoes the output to browser using binary transfer headers
        @separator if set (defaults to ';') specifies the separator for each CSV field
    */
    public static function export($rows, $coldefs, $headers, $boolPrintRows=true, $csvFileName=null, $separator=';',$fechaHora = false)
    {
        $endLine = PHP_EOL;
        $returnVal = '';
        $headerRow = '';

        if(is_null($csvFileName)){
            $csvFileName = 'CSVFILE'.date('YmdHis').'.csv';
        }

        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$csvFileName);
        header("Content-Type: text/csv");
        header("Content-Transfer-Encoding: UTF-8");
        header('Pragma: no-cache');
        header('Expires: 0');

        // Print Header
        if($boolPrintRows == true){
            $names = '';
            foreach($headers as $col){
                $names .= '"'.$col.'"'.$separator;
            }
            $names = rtrim($names,$separator);
            $headerRow .= $names.$endLine;
        }

        foreach($rows as $row){
            $r = '';
            foreach($coldefs as $col=>$config){
                if(array_key_exists($col,$row)){
                    $val = $row[$col];

                    foreach($config as $conf)
                        $val = Yii::app()->format->format($val,$conf);

                    $r .= '"'.self::clearData($val).'"'.$separator;
                }
            }
            $item = trim(rtrim($r,$separator)).$endLine;
            $returnVal .= $item;
        }
        echo $headerRow;
        echo $returnVal;
        if(!$fechaHora){
            echo "Reporte generado a la fecha y hora: ".date("d-m-Y H:i:s");
        }
        else {
            echo "Reporte generado a la fecha y hora: ".$fechaHora;
        }

    }


    /*
        export a data set to CSV output.

        Please refer to CFormatter about column definitions, this class will use CFormatter.

        @rows    CModel array. (you can use a CActiveRecord array because it extends from CModel)
        @coldefs    example: 'colname'=>array('number') (See also CFormatter about this string)
        @boolPrintRows    boolean, true print col headers taken from coldefs array key
        @csvFileName if set (defaults null) it echoes the output to browser using binary transfer headers
        @separator if set (defaults to ';') specifies the separator for each CSV field
    */
    public static function exportUnique($rows, $coldefs, $headers, $fieldUnique='', $repetedFields=array(), $boolPrintRows=true, $csvFileName=null, $endLine=PHP_EOL, $separator=';')
    {
        $returnVal = '';
        $headerRow = '';

        if(is_null($csvFileName)){
            $csvFileName = 'CSVFILEU'.date('YmdHis').'.csv';
        }

        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$csvFileName);
        header("Content-Type: text/csv");
        header("Content-Transfer-Encoding: UTF-8");
        header('Pragma: no-cache');
        header('Expires: 0');

        // Print Header
        if($boolPrintRows == true){
            $names = '';
            foreach($headers as $col){
                $names .= '"'.$col.'"'.$separator;
            }
            $names = rtrim($names,$separator);
            $headerRow .= $names;
        }

        $beforeCodeUnique = '';

        foreach($rows as $row){
            $r = '';

            if($beforeCodeUnique!=$row[$fieldUnique]){
                foreach($coldefs as $col=>$config){
                    if(array_key_exists($col,$row)){
                        $val = $row[$col];
                        foreach($config as $conf)
                            $val = Yii::app()->format->format($val,$conf);

                        $r .= '"'.self::clearData($val).'"'.$separator;
                    }
                }
                $item = $endLine.trim(rtrim($r,$separator));
            }else{
                foreach($repetedFields as $col=>$config){
                    if(array_key_exists($col,$row)){
                        $val = $row[$col];
                        foreach($config as $conf)
                            $val = Yii::app()->format->format($val,$conf);

                        $r .= '"'.self::clearData($val).'"'.$separator;
                    }
                }
                $item = $separator.trim(rtrim($r,$separator));
            }

            $returnVal .= $item;
            $beforeCodeUnique = (array_key_exists($fieldUnique,$row))?$row[$fieldUnique]:'';

        }

        $returnVal = str_replace($separator.$separator, $separator, $returnVal);

        return $headerRow.$returnVal;
    }

    private static function clearData($input){
        $output = str_replace(self::$separator, self::$replaceSeparator, $input);
        $output = str_replace('"', "'", $output);
        return $output;
    }

}