<?php

class LeerFormularioInscripcionCommand extends CConsoleCommand {
    public $archivo_correo ='';
    public $fila =array();
    public $formulario_id ='';
    public $archivos='';
    CONST MODULO='LeerFormularioInscripcionCommand';
    public function actionIndex() {
        date_default_timezone_set('America/Caracas');
        $directorio = realpath(Yii::app()->basePath.'/..');
        $directorio_base = $directorio.'/public/uploads/Faltantes/';
        $modelFormulario = new FormularioInscripcionPeriodo();
        $existe_proceso_activo = $modelFormulario->existeProcesoActivo();

        if($existe_proceso_activo==0){
            while($proceso_pendiente = $modelFormulario->buscarFormularioPendiente()){
                $paso = false;
                $existe_proceso_activo = 0;
                $esInicialPrimaria = 0;
                $periodo_id=null;
                $seccion_plantel_id=null;
                $plantel_id=null;
                $operacion = false;
                $result = new stdClass();
                $estudiantes=array();
                $log='';
                $id=null;
                $estudiante_errores=array();
                $modelEstudiante= '';

                echo "PROCESANDO ARCHIVO ".$proceso_pendiente['archivo'].PHP_EOL;

                //$proceso_pendiente = $modelFormulario->buscarFormularioPendiente();
                if($proceso_pendiente){
                    $datosSeccion= SeccionPlantel::model()->obtenerDatosSeccion($seccion_plantel_id, $plantel_id);
                    $this->formulario_id=$proceso_pendiente['id'];
                    $seccion_plantel_id=$proceso_pendiente['seccion_plantel_id'];
                    $plantel_id=$proceso_pendiente['plantel_id'];
                    $periodo_id=$proceso_pendiente['periodo_id'];
                    $this->archivo_correo=$archivo=$proceso_pendiente['archivo'];
                    $directorio_archivo=$directorio_base.$archivo;
                    $result = new stdClass();
                    $readDataOnly = true;
                    $excelReader = new ExcelReader($directorio_archivo);
                    $result->archivo = $archivo;
                    $operacion = false;
                    try {
                        list($operacion,
                            $result->class_style,
                            $result->message,
                            $objReaderExcel,
                            $objReader) = $excelReader->getReader($readDataOnly);
                    } catch (Exception $e){
                        echo "NO SE PUEDO ABRIR EL ARCHIVO ".$proceso_pendiente['archivo'].PHP_EOL;
                        FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'Z');
                    }

                    //Verifico que las extensiones del archivo sean las requeridas
                    if($operacion){
                        $sheetData = $objReader->getActiveSheet()->toArray(null,true,true,true);
                        $i = 2; //$i sirve para recorrer el excel

                        /*
                         *      ESTRUCTURA ARCHIVO
                         * [A] => Origen Del Estudiante -> requerido
                         * [B] => Identificación Del Estudiante -> requerido
                         * [C] => Nombres Del Estudiante -> requerido solo si es nuevo ingreso
                         * [D] => Apellidos Del Estudiante -> requerido solo si es nuevo ingreso
                         * [E] => Cédula Del Representante -> requerido solo si es nuevo ingreso
                         * [F] => Teléfono del Representante
                         * [G] => Fecha_Nacimiento -> requerido
                         * [H] => Sexo -> requerido
                         * [I] => Afinidad -> requerido solo si es nuevo ingreso
                         * [J] => Orden_Nacimiento -> requerido solo si es nuevo ingreso
                         */
                        while (array_key_exists($i, $sheetData)) {
                            $paso = true;
                            if(trim($sheetData[$i]['A']) != '' AND trim($sheetData[$i]['B']) != '' AND trim($sheetData[$i]['C']) != '' AND trim($sheetData[$i]['D']) != ''
                                AND trim($sheetData[$i]['E']) != ''  AND trim($sheetData[$i]['G']) != '' AND trim($sheetData[$i]['H']) != '' AND trim($sheetData[$i]['I']) != ''
                            ){
                                $model = new DataFormularioInscripcion();
                                $model->origen=strtoupper(trim($sheetData[$i]['A']));
                                $model->identificacion=trim($sheetData[$i]['B']);
                                $model->nombres=strtoupper(trim(Utiles::onlyAlphaNumericWithSpace($sheetData[$i]['C'])));
                                $model->apellidos=strtoupper(trim(Utiles::onlyAlphaNumericWithSpace($sheetData[$i]['D'])));
                                $model->documento_identidad=trim($sheetData[$i]['E']);
                                $model->fecha_nacimiento=date('Y-m-d',self::ExcelToPHP(trim($sheetData[$i]['G'])));
                                $model->sexo=strtoupper(trim($sheetData[$i]['H']));
                                $model->afinidad=trim($sheetData[$i]['I']);
                                $model->orden_nacimiento=trim($sheetData[$i]['J']);
                                $model->formulario_id=$this->formulario_id;
                                $model->estatus='P';
                                $model->usuario_ini_id=1;
                                $model->fecha_ini=date('Y-m-d H:i:s');
                                $transaction = Yii::app()->db->beginTransaction();
                                try {
                                    if(!$model->save()){
                                        //sleep(60);
                                        $transaction->commit();
                                        var_dump('NO GUARDO SAVE ',$model->formulario_id);
                                        //$this->enviarCorreo('isalazar@me.gob.ve', 'Sistema de Gestión Escolar | Data Formulario de Inscripción Masiva','NO GUARDO DESDE EL SAVE', 'soporte_gescolar@me.gob.ve','Sistema de Gestión Escolar');
                                    }
                                    else {
                                        $transaction->commit();
                                    }
                                }
                                catch (Exception $e){
                                    //sleep(60);
                                    $transaction->commit();
                                    var_dump('NO GUARDO ROLLBACK ',$e->getMessage(),$model->formulario_id);
                                    //$this->enviarCorreo('isalazar@me.gob.ve', 'Sistema de Gestión Escolar | Data Formulario de Inscripción Masiva','NO GUARDO ROLLBACK '.$e->getMessage(), 'soporte_gescolar@me.gob.ve','Sistema de Gestión Escolar');
                                }
                            }
                            $i++;
                            FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'M');
                        }
                        if($paso==false){
                            FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'V');
                        }
                    }else{
                        FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'L');
                        $result->class_style = "error";
                        $result->message = "Ha ocurrido un error en el proceso de lectura del archivo. Recargue la página e intentelo de nuevo. Si el error se repite notifiquelo a los administradores del sistema.";
                    }
                }
            }
            Yii::app()->end();
        }
        else {
            Yii::app()->end();
        }
    }

    public function getViewPath() {
        return Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'views';
    }

    /**
     * Modified copy of getViewFile
     * @see CController::getViewFile
     * @param $viewName
     * @return string
     */
    public function getViewFile($viewName) {
        return $this->getViewPath() . $viewName . '.php';
    }

    /**
     * Modified copy of renderPartial from CController
     * @see CController::renderPartial
     * @param $view
     * @param $data
     * @param $return
     * @return mixed
     * @throws CException
     */
    public function renderPartial($view, $data, $return) {
        if (($viewFile = $this->getViewFile($view)) !== false) {
            $output = $this->renderFile($viewFile, $data, true);
            if ($return)
                return $output;
            else
                echo $output;
        } else
            throw new CException(Yii::t('yii', '{class} cannot find the requested view "{view}".', array('{class}' => get_class($this), '{view}' => $view)));
    }
    public static function ExcelToPHP($dateValue = 0) {
        $myExcelBaseDate = 25569;
        //	Adjust for the spurious 29-Feb-1900 (Day 60)
        if ($dateValue < 60) {
            --$myExcelBaseDate;
        }

        // Perform conversion
        if ($dateValue >= 1) {
            $utcDays = $dateValue - $myExcelBaseDate;
            $returnValue = round($utcDays * 86400);
            if (($returnValue <= PHP_INT_MAX) && ($returnValue >= -PHP_INT_MAX)) {
                $returnValue = (integer) $returnValue;
            }
        } else {
            $hours = round($dateValue * 24);
            $mins = round($dateValue * 1440) - round($hours * 60);
            $secs = round($dateValue * 86400) - round($hours * 3600) - round($mins * 60);
            $returnValue = (integer) gmmktime($hours, $mins, $secs);
        }

        // Return
        return $returnValue;
    }	//	function ExcelToPHP()

    static public function enviarCorreo($to, $subject = 'SIR-SWL', $msj = '', $from = '', $from_name = '') {
        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $mailer->Host = 'mail.me.gob.ve:25';
        $mailer->IsSMTP();

        if (is_array($to)) {
            foreach ($to as $sendTo) {
                $mailer->AddAddress("{$sendTo}");
            }
        } else {
            $mailer->AddAddress("{$to}");
        }

        if (isset($from) and $from != '' and $from != null)
            $mailer->From = $from;
        else
            $mailer->From = Yii::app()->params->adminEmail;
        if (isset($from_name) and $from_name != '' and $from_name != null)
            $mailer->FromName = $from_name;
        else
            $mailer->FromName = Yii::app()->params->adminName;

        $mailer->CharSet = 'UTF-8';
        $mailer->Subject = $subject;
        $mailer->MsgHTML($msj);
        return $mailer->Send();
    }


}
