<?php

class SeguimientoTituloCommand extends CConsoleCommand {

    public function actionAsignarZonaEducativa($zona_educativa_id, $username, $nombre, $apellido, $cedula, $usuario_id, $grupo_id, $ip) {
        date_default_timezone_set('America/Caracas');
        if (isset($zona_educativa_id)) {

            $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_actual_id = $periodo_escolar_actual_id['id'];
            $datosZonaEdu = array();
            $mensajeExitoso = '';
            $estatus_ubicacion_titulo = 1; // Asignado a Zona Educativa.
            $datosZonaEdu = Titulo::model()->datosPorZonaEdu($periodo_actual_id, $mostrar = 0); // verifico que existan zonas educativas con estudiantes disponibles para la verificación de entrega de lostes de seriales a las zonas educativas

            $grupoDRCEE = UserGroups::JEFE_DRCEE;
            if ($grupoDRCEE != null)
                $correoDrcee = Titulo::model()->obtenerCorreos($grupoDRCEE);
            else
                $correoDrcee = array();

            $correoUsuario = Titulo::model()->obtenerCorreosUsuarios($usuario_id);
            if ($correoUsuario != false) {
                $correos = array_merge($correoDrcee, $correoUsuario);
            } else {
                $correoUsuario = array();
                $correos = array_merge($correoDrcee, $correoUsuario);
            }

            if ($datosZonaEdu != false) {
                $guardarControlZonaEducativaAntes = Titulo::model()->guardarControlSeguimientoAntes($estatus_ubicacion_titulo, $periodo_actual_id);

//                    foreach ($correos as $key => $data) {
//                        $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
//                        $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
//                        $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;


                $transaction = Yii::app()->db->beginTransaction();

//                foreach ($planteles as $key => $value) {
//                    $plantel_id[] = (int) $value['id'];
//                }
//   $plantelesTotal = Utiles::toPgArray($plantel_id);
                $modulo = "Titulo.SeguimientoTitulo.AsignarZonaEducativa";
                //$ip = Yii::app()->request->userHostAddress;


                try {

                    $resultadoGuardarAsignacionZonaEducativa = Titulo::model()->asignarZonaEducativa($periodo_actual_id, $zona_educativa_id, $cedula, $nombre, $apellido, $modulo, $ip, $username, $usuario_id);

                    $guardarControlZonaEducativaDespues = Titulo::model()->guardarControlSeguimientoDespues($estatus_ubicacion_titulo, $periodo_actual_id);
                    $mensajeExitoso = "Estimado Usuario, el proceso de validación de entrega de lotes de papel moneda a la zona educativa tardara un tiempo para realizarse. Por favor ingrese en otra oportunidad";
                    $respuesta['statusCode'] = 'success';
                    $respuesta['mensaje'] = $mensajeExitoso;
                    echo json_encode($respuesta);

                    echo "SE INICIO EL PROCESO DE ASIGNACION DE SERIALES A LA ZONA EDUCATIVA " . PHP_EOL;

                    $datosZonaEduVerificado = Titulo::model()->datosPorZonaEdu($periodo_actual_id, $mostrar = 1);

                    foreach ($correos as $key => $data) {
                        $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;

                        $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
                        $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;
                        /*$destinatario_nombre = 'Administrador del';
                        $destinatario_apellido = 'Sistema';
                        $remitente_correo = 'soporte_gescolar@me.gob.ve';*/

                        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                        $mailer->Host = 'mail.me.gob.ve:25';
                        $mailer->IsSendmail();
                        $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                        $mailer->FromName = 'Gescolar ';
                        $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                        $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                        $mailer->CharSet = 'UTF-8';
                        $mailer->Subject = 'Notificación de Validación de Entrega de Lotes de Papel Moneda a las Zonas Educativas del MPPE';
                        $mailer->IsHTML(true);
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                        if (isset($datosZonaEduVerificado) && $datosZonaEduVerificado !== false) {

                            $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                            $body .= "<b>Estimado usuario se le notifica que el proceso de validación de entrega de lotes de papel moneda a las zonas educativas ha culminado con exito, ya puede ingresar al sistema gescolar y verificar las zonas educativas a los que se le asígno los seriales de títulos correspondientes del MPPE.</b><br>";
                            $body .='<br>
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                <div id="apDiv1">
                                  <table width="100%" style="border:1px solid #9F9D9D; border-collapse:collapse;" border="1" bordercolor="#E5E5E5" cellpadding="0" cellspacing="0" class="pull-center">

                                              <tr style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                              <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b></b>
                                                   </th>
                                                  <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Zona Educativa</b>
                                                   </th>
                                                  <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Seriales Asignados a Estudiantes</b>
                                                   </th>
                                                </tr>';
                            foreach ($datosZonaEduVerificado as $key => $value) {
                                $keys = $key + 1;
                                $body .= '<tr style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                    <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                    ' . $keys . '
                                                    </td>
                                                  <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $datosZonaEduVerificado[$key]['zona_educativa'] . '
                                                    </td>
                                                    <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $datosZonaEduVerificado[$key]['cant_seriales_asignado'] . '
                                                    </td>
                                               </tr>';
                            }
                            $body .=' </table>
                        </div>
                        </body>
                        </html>';
                        } else {
                            $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br>
                                <br>
                                </head>';
                            $body .= "<b>Estimado usuario se le notifica que el proceso de validación de entrega de lotes de papel moneda a las zonas educativas ha culminado con exito, ya puede ingresar al sistema gescolar y verificar las zonas educativas a los que se le asígno los seriales de títulos correspondientes del MPPE.</b><br>";
                            $body .='<br><title>
                                </title>
                                <style>
                                </style>
                                <body>
                                </body>
                        </html>';
                        }
                        $mailer->Body = $body;
                        $exito = $mailer->Send();
                        $intentos = 1;
                        while ((!$exito) && ($intentos < 2)) {
                            sleep(5);
                            $exito = $mailer->Send();
                            $intentos = $intentos + 1;
                        }
                        if (!$exito) {
                            echo "Problemas enviando correo electrónico";
                            echo "<br>" . $mailer->ErrorInfo;
                            echo " ";

                            $destinatario_nombre = 'Administrador del';
                            $destinatario_apellido = 'Sistema';
                            $remitente_correo = 'soporte_gescolar@me.gob.ve';
                            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                            $mailer->Host = 'mail.me.gob.ve:25';
                            $mailer->IsSendmail();
                            $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                            $mailer->FromName = 'Gescolar ';
                            $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                            $mailer->CharSet = 'UTF-8';
                            $mailer->Subject = 'Notificación de Validación de Entrega de Lotes de Papel Moneda a las Zonas Educativas del MPPE, paso 1';
                            $mailer->Body = 'Problemas enviando correo electrónico, paso 1';
                            $exito = $mailer->Send();
                        }
                    }
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();

                    $borrarControlZonaEducativa = Titulo::model()->borrarControlSeguimientoError($estatus_ubicacion_titulo, $periodo_actual_id);

                    $respuesta['statusCode'] = 'error';
                    $respuesta['error'] = $ex;
                    $error = $ex->getMessage();
                    $respuesta['mensaje'] = $error;
                    echo json_encode($respuesta);

                    echo "Ocurrio un error en el proceso de validación de entrega de lotes de papel moneda a las zonas educativas" . PHP_EOL;

                    $mensaje = $error;

                    $destinatario_nombre = 'Gescolar';
                    $destinatario_apellido = 'Gescolar';
                    $remitente_correo = 'soporte_gescolar@me.gob.ve';
//soporte_gescolar@me.gob.ve
                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSendmail();
                    $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                    $mailer->FromName = 'Gescolar ';
                    $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Notificación de Error en la Validación de Entrega de Lotes de Papel Moneda a las Zonas Educativas en el MPPE';
                    $mailer->Body = $mensaje;
                    if ($mailer->Send()) {

                        $mensaje_error = "Estimado usuario se le notifica que el proceso de Validación de Entrega de Lotes de Papel Moneda a las Zonas Educativas no se puedo culminar con exito, ya que ocurrio un error en el proceso, por favor notifique este inconveniente al departamento de sistema del MPPE, para solventar el inconveniente ocasionado.";

//                        foreach ($correos as $key => $data) {
//                            $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
//                            $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
//                            $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;
                        $destinatario_nombre = 'Administrador del';
                        $destinatario_apellido = 'Sistema';
                        $remitente_correo = 'soporte_gescolar@me.gob.ve';

                        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                        $mailer->Host = 'mail.me.gob.ve:25';
                        $mailer->IsSendmail();
                        $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                        $mailer->FromName = 'Gescolar ';
                        $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                        $mailer->CharSet = 'UTF-8';
                        $mailer->Subject = 'Notificación de Error en la Validación de Entrega de Lotes de Papel Moneda a las Zonas Educativas en el MPPE';
                        $mailer->IsHTML(true);
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                        $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                        $body .='
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                      <b>  ' . $mensaje_error . ' </b>
                                </body>
                                </html>';
                        $mailer->Body = $body;
                        $exito = $mailer->Send();
                        $intentos = 1;
                        while ((!$exito) && ($intentos < 2)) {
                            sleep(5);
                            $exito = $mailer->Send();
                            $intentos = $intentos + 1;
                        }
                        if (!$exito) {
                            echo "Problemas enviando correo electrónico";
                            echo "<br>" . $mailer->ErrorInfo;
                            echo " ";

                            $destinatario_nombre = 'Administrador del';
                            $destinatario_apellido = 'Sistema';
                            $remitente_correo = 'soporte_gescolar@me.gob.ve';
                            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                            $mailer->Host = 'mail.me.gob.ve:25';
                            $mailer->IsSendmail();
                            $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                            $mailer->FromName = 'Gescolar ';
                            $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                            $mailer->CharSet = 'UTF-8';
                            $mailer->Subject = 'Notificación de Validación de Entrega de Lotes de Papel Moneda a las Zonas Educativas del MPPE, paso 1 error';
                            $mailer->Body = 'Problemas enviando correo electrónico, paso 1 error';
                            $exito = $mailer->Send();
                        }
                    }
                }
            } else {

                echo "\n " . "NO EXISTE NINGUNA ZONA EDUCATIVA PARA LA ENTREGA DE SERIALES DISPONIBLE \n";

                $borrarControlZonaEducativa = Titulo::model()->borrarControlSeguimientoError($estatus_ubicacion_titulo, $periodo_actual_id);

                $mensaje = "NO EXISTE NINGUNA ZONA EDUCATIVA PARA LA ENTREGA DE SERIALES DISPONIBLE";

                $destinatario_nombre = 'Gescolar';
                $destinatario_apellido = 'Gescolar';
                $remitente_correo = 'soporte_gescolar@me.gob.ve';
//soporte_gescolar@me.gob.ve
                $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                $mailer->Host = 'mail.me.gob.ve:25';
                $mailer->IsSendmail();
                $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                $mailer->FromName = 'Gescolar ';
                $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                $mailer->CharSet = 'UTF-8';
                $mailer->Subject = 'Notificación de Error en la Validación de Entrega de Lotes de Papel Moneda a las Zonas Educativas en el MPPE';
                $mailer->IsHTML(true);
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                $body .=' <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                        <b>' . $mensaje . '</b>
                                </body>
                                </html>';
                $mailer->Body = $body;
                if ($mailer->Send()) {

                    $mensaje_error = "Estimado usuario se le notifica que el proceso de Validación de Entrega de Lotes de Papel Moneda a las Zonas Educativas no se puedo culminar con exito, ya que ocurrio un error en el proceso, por favor notifique este inconveniente al departamento de sistema del MPPE, para solventar el inconveniente ocasionado.";

//                        foreach ($correos as $key => $data) {
//                            $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
//                            $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
//                            $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;
                    $destinatario_nombre = 'Administrador del';
                    $destinatario_apellido = 'Sistema';
                    $remitente_correo = 'soporte_gescolar@me.gob.ve';

                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSendmail();
                    $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                    $mailer->FromName = 'Gescolar ';
                    $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Notificación de Error en la Validación de Entrega de Lotes de Papel Moneda a las Zonas Educativas en el MPPE';
                    $mailer->IsHTML(true);
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                    $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                    $body .='
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                      <b>  ' . $mensaje_error . ' </b>
                                </body>
                                </html>';
                    $mailer->Body = $body;
                    $exito = $mailer->Send();
                    $intentos = 1;
                    while ((!$exito) && ($intentos < 2)) {
                        sleep(5);
                        $exito = $mailer->Send();
                        $intentos = $intentos + 1;
                    }
                    if (!$exito) {
                        echo "Problemas enviando correo electrónico";
                        echo "<br>" . $mailer->ErrorInfo;
                        echo " ";

                        $destinatario_nombre = 'Administrador del';
                        $destinatario_apellido = 'Sistema';
                        $remitente_correo = 'soporte_gescolar@me.gob.ve';
                        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                        $mailer->Host = 'mail.me.gob.ve:25';
                        $mailer->IsSendmail();
                        $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                        $mailer->FromName = 'Gescolar ';
                        $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                        $mailer->CharSet = 'UTF-8';
                        $mailer->Subject = 'Notificación de Validación de Entrega de Lotes de Papel Moneda a las Zonas Educativas del MPPE, paso 1 error';
                        $mailer->Body = 'Problemas enviando correo electrónico, paso 1 error';
                        $exito = $mailer->Send();
                    }
                }
            }
//     }
        } else
            echo "\n " . "EL USUARIO_ID :" . $usuario_id . " NO ES UN NUMERO \n"; //
    }

    public function actionAsignarPlantel($plantel_id, $username, $nombre, $apellido, $cedula, $usuario_id, $grupo_id, $ip) {
        date_default_timezone_set('America/Caracas');
        if (isset($plantel_id)) {

            $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_actual_id = $periodo_escolar_actual_id['id'];
            $datosPlantel = array();
            $mensajeExitoso = '';
            $estatus_ubicacion_titulo = 2;  // El estatus 2 es Asignado a Plantel.

            $zona_educativa_id = Titulo::model()->obtenerIdZonaEdu($usuario_id);
            if ($zona_educativa_id != false)
                $datosPlantel = Titulo::model()->datosPorPlantel($periodo_actual_id, $zona_educativa_id, $mostrar = 0); // verifico que existen planteles con estudiantes disponibles para la verificación de entrega de lotes de seriales a los planteles
            else
                $datosPlantel = array();

            $grupoDRCEE = UserGroups::JEFE_DRCEE;
            if ($grupoDRCEE != null)
                $correoDrcee = Titulo::model()->obtenerCorreos($grupoDRCEE);
            else
                $correoDrcee = array();

            $correoUsuario = Titulo::model()->obtenerCorreosUsuarios($usuario_id);
            if ($correoUsuario != false) {
                $correos = array_merge($correoDrcee, $correoUsuario);
            } else {
                $correoUsuario = array();
                $correos = array_merge($correoDrcee, $correoUsuario);
            }
            if ($datosPlantel != false) {
                $guardarControlPlantelAntes = Titulo::model()->guardarControlSeguimientoAntes($estatus_ubicacion_titulo, $periodo_actual_id);

                $transaction = Yii::app()->db->beginTransaction();
                $modulo = "Titulo.SeguimientoTitulo.AsignarPlantel";
              //  $ip = Yii::app()->request->userHostAddress;

                try {

                    $resultadoGuardarAsignacionPlantel = Titulo::model()->asignarPlantel($periodo_actual_id, $plantel_id, $cedula, $nombre, $apellido, $modulo, $ip, $username, $usuario_id);

//       $transaction->commit();
                    $guardarControlPlantelDespues = Titulo::model()->guardarControlSeguimientoDespues($estatus_ubicacion_titulo, $periodo_actual_id);
                    $mensajeExitoso = "Estimado Usuario, el proceso de validación de entrega de lotes de papel moneda a los planteles tardara un tiempo para realizarse. Por favor ingrese en otra oportunidad";
                    $respuesta['statusCode'] = 'success';
                    $respuesta['mensaje'] = $mensajeExitoso;
                    echo json_encode($respuesta);

                    echo "SE INICIO EL PROCESO DE ASIGNACION DE SERIALES A LOS PLANTELES PERTENECIENTES A UNA ZONA EDUCATIVA EN ESPECIFICO " . PHP_EOL;

                    $datosPlantelVerifico = Titulo::model()->datosPorPlantel($periodo_actual_id, $zona_educativa_id, $mostrar = 1);
//                    foreach ($correos as $key => $data) {
//                        $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
//                        $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
//                        $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;
                    $destinatario_nombre = 'Administrador del';
                    $destinatario_apellido = 'Sistema';
                    $remitente_correo = 'soporte_gescolar@me.gob.ve';

                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSendmail();
                    $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                    $mailer->FromName = 'Gescolar ';
                    $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Notificación de Validación de Entrega de Lotes de Papel Moneda a los planteles por parte de la zona educativa respectiva del MPPE';
                    $mailer->IsHTML(true);
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');

                    if (isset($datosPlantelVerifico) && $datosPlantelVerifico !== false) {

                        $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br>
                                <br>
                                </head>';
                        $body .= "<b>Estimado usuario se le notifica que el proceso de validación de entrega de lotes de papel moneda a los planteles ha culminado con exito, ya puede ingresar al sistema gescolar y verificar los planteles a los que se les asígno los seriales de títulos correspondientes del MPPE.</b><br>";
                        $body .='<br><title>
                                </title>
                                <style>
                                </style>
                                <body>
                                <div id="apDiv1">
                                  <table width="100%" style="border:1px solid #9F9D9D; border-collapse:collapse;" border="1" bordercolor="#E5E5E5" cellpadding="0" cellspacing="0" class="pull-center">

                                              <tr style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                              <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b></b>
                                                   </th>
                                                  <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Plantel</b>
                                                   </th>
                                                  <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Seriales Asignados al Plantel</b>
                                                   </th>
                                                </tr>';
                        foreach ($datosPlantelVerifico as $key => $value) {
                            $keys = $key + 1;
                            $body .= '<tr style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                 <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $keys . '
                                                    </td>
                                                  <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $datosPlantelVerifico[$key]['plantel'] . '
                                                    </td>
                                                    <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $datosPlantelVerifico[$key]['cant_seriales_asignado_por_plantel'] . '
                                                    </td>
                                               </tr>';
                        }
                        $body .=' </table>
                        </div>
                        </body>
                        </html>';
                    } else {
                        $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br>
                                <br>
                                </head>';
                        $body .= "<b>Estimado usuario se le notifica que el proceso de validación de entrega de lotes de papel moneda a los planteles ha culminado con exito, ya puede ingresar al sistema gescolar y verificar los planteles a los que se les asígno los seriales de títulos correspondientes del MPPE.</b><br>";
                        $body .='<br><title>
                                </title>
                                <style>
                                </style>
                                <body>
                                </body>
                        </html>';
                    }
                    $mailer->Body = $body;
                    $exito = $mailer->Send();
                    $intentos = 1;
                    while ((!$exito) && ($intentos < 2)) {
                        sleep(5);
                        $exito = $mailer->Send();
                        $intentos = $intentos + 1;
                    }
                    if (!$exito) {
                        echo "Problemas enviando correo electrónico";
                        echo "<br>" . $mailer->ErrorInfo;
                        echo " ";

                        $destinatario_nombre = 'Administrador del';
                        $destinatario_apellido = 'Sistema';
                        $remitente_correo = 'soporte_gescolar@me.gob.ve';
                        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                        $mailer->Host = 'mail.me.gob.ve:25';
                        $mailer->IsSendmail();
                        $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                        $mailer->FromName = 'Gescolar ';
                        $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                        $mailer->CharSet = 'UTF-8';
                        $mailer->Subject = 'Notificación de Validación de Entrega de Lotes de Papel Moneda a los planteles por parte de la zona educativa respectiva del MPPE, paso 2';
                        $mailer->Body = 'Problemas enviando correo electrónico, paso 2';
                        $exito = $mailer->Send();
                    }
//}
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();

                    $borrarControlPlantel = Titulo::model()->borrarControlSeguimientoError($estatus_ubicacion_titulo, $periodo_actual_id);

                    $respuesta['statusCode'] = 'error';
                    $respuesta['error'] = $ex;
                    $error = $ex->getMessage();
                    $respuesta['mensaje'] = $error;
                    echo json_encode($respuesta);

                    echo "Ocurrio un error en el proceso de validación de entrega de lotes de papel moneda a los planteles" . PHP_EOL;

                    $mensaje = $error;

                    $destinatario_nombre = 'Gescolar';
                    $destinatario_apellido = 'Gescolar';
                    $remitente_correo = 'soporte_gescolar@me.gob.ve';
//soporte_gescolar@me.gob.ve
                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSendmail();
                    $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                    $mailer->FromName = 'Gescolar ';
                    $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Notificación de Error en la Validación de Entrega de Lotes de Papel Moneda a los planteles en el MPPE';
                    $mailer->IsHTML(true);
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                    $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                    $body .='
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                      <b>  ' . $mensaje . ' </b>
                                </body>
                                </html>';
                    $mailer->Body = $body;
                    if ($mailer->Send()) {

                        $mensaje_error = "Estimado usuario se le notifica que el proceso de Validación de Entrega de Lotes de Papel Moneda a los planteles no se puedo culminar con exito, ya que ocurrio un error en el proceso, por favor notifique este inconveniente al departamento de sistema del MPPE, para solventar el inconveniente ocasionado.";

//                        foreach ($correos as $key => $data) {
//                            $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
//                            $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
//                            $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;
                        $destinatario_nombre = 'Administrador del';
                        $destinatario_apellido = 'Sistema';
                        $remitente_correo = 'soporte_gescolar@me.gob.ve';

                        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                        $mailer->Host = 'mail.me.gob.ve:25';
                        $mailer->IsSendmail();
                        $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                        $mailer->FromName = 'Gescolar ';
                        $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                        $mailer->CharSet = 'UTF-8';
                        $mailer->Subject = 'Notificación de Error en la Validación de Entrega de Lotes de Papel Moneda a los planteles en el MPPE';
                        $mailer->IsHTML(true);
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                        $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                        $body .='
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                      <b>  ' . $mensaje_error . ' </b>
                                </body>
                                </html>';
                        $mailer->Body = $body;
                        $exito = $mailer->Send();
                        $intentos = 1;
                        while ((!$exito) && ($intentos < 2)) {
                            sleep(5);
                            $exito = $mailer->Send();
                            $intentos = $intentos + 1;
                        }
                        if (!$exito) {
                            echo "Problemas enviando correo electrónico";
                            echo "<br>" . $mailer->ErrorInfo;
                            echo " ";

                            $destinatario_nombre = 'Administrador del';
                            $destinatario_apellido = 'Sistema';
                            $remitente_correo = 'soporte_gescolar@me.gob.ve';
                            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                            $mailer->Host = 'mail.me.gob.ve:25';
                            $mailer->IsSendmail();
                            $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                            $mailer->FromName = 'Gescolar ';
                            $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                            $mailer->CharSet = 'UTF-8';
                            $mailer->Subject = 'Notificación de Error en la Validación de Entrega de Lotes de Papel Moneda a los planteles en el MPPE, paso 2 error';
                            $mailer->Body = 'Problemas enviando correo electrónico, paso 2 error';
                            $exito = $mailer->Send();
                        }
                    }
                }
            } else {
                echo "\n " . "NO EXISTE NINGUN PLANTEL PARA LA ENTREGA DE SERIALES DISPONIBLE \n";

                $borrarControlPlantel = Titulo::model()->borrarControlSeguimientoError($estatus_ubicacion_titulo, $periodo_actual_id);

                $mensaje = "NO EXISTE NINGUN PLANTEL PARA LA ENTREGA DE SERIALES DISPONIBLE";

                $destinatario_nombre = 'Gescolar';
                $destinatario_apellido = 'Gescolar';
                $remitente_correo = 'soporte_gescolar@me.gob.ve';
//soporte_gescolar@me.gob.ve
                $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                $mailer->Host = 'mail.me.gob.ve:25';
                $mailer->IsSendmail();
                $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                $mailer->FromName = 'Gescolar ';
                $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                $mailer->CharSet = 'UTF-8';
                $mailer->Subject = 'Notificación de Error en la Validación de Entrega de Lotes de Papel Moneda a los planteles en el MPPE';
                $mailer->IsHTML(true);
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                $body .='
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                      <b>  ' . $mensaje . ' </b>
                                </body>
                                </html>';
                $mailer->Body = $body;
                if ($mailer->Send()) {

                    $mensaje_error = "Estimado usuario se le notifica que el proceso de Validación de Entrega de Lotes de Papel Moneda a los planteles no se puedo culminar con exito, ya que ocurrio un error en el proceso, por favor notifique este inconveniente al departamento de sistema del MPPE, para solventar el inconveniente ocasionado.";

//                        foreach ($correos as $key => $data) {
//                            $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
//                            $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
//                            $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;
                    $destinatario_nombre = 'Administrador del';
                    $destinatario_apellido = 'Sistema';
                    $remitente_correo = 'soporte_gescolar@me.gob.ve';

                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSendmail();
                    $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                    $mailer->FromName = 'Gescolar ';
                    $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Notificación de Error en la Validación de Entrega de Lotes de Papel Moneda a los planteles en el MPPE';
                    $mailer->IsHTML(true);
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                    $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                    $body .='
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                      <b>  ' . $mensaje_error . ' </b>
                                </body>
                                </html>';
                    $mailer->Body = $body;
                    $exito = $mailer->Send();
                    $intentos = 1;
                    while ((!$exito) && ($intentos < 2)) {
                        sleep(5);
                        $exito = $mailer->Send();
                        $intentos = $intentos + 1;
                    }
                    if (!$exito) {
                        echo "Problemas enviando correo electrónico";
                        echo "<br>" . $mailer->ErrorInfo;
                        echo " ";

                        $destinatario_nombre = 'Administrador del';
                        $destinatario_apellido = 'Sistema';
                        $remitente_correo = 'soporte_gescolar@me.gob.ve';
                        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                        $mailer->Host = 'mail.me.gob.ve:25';
                        $mailer->IsSendmail();
                        $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                        $mailer->FromName = 'Gescolar ';
                        $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                        $mailer->CharSet = 'UTF-8';
                        $mailer->Subject = 'Notificación de Error en la Validación de Entrega de Lotes de Papel Moneda a los planteles en el MPPE, paso 2 error';
                        $mailer->Body = 'Problemas enviando correo electrónico, paso 2 error';
                        $exito = $mailer->Send();
                    }
                }
            }
//     }
        } else
            echo "\n " . "EL USUARIO_ID :" . $usuario_id . " NO ES UN NUMERO \n";
    }

    public function actionAsignarEstudiante($estatusTitulo_array_pg_array, $fechaOtorgamiento, $anioEgreso_array_pg_array, $tipoEvaluacion_array_pg_array, $tdocumento_director_array_, $documento_director_array_, $tdocumento_jefe_array_, $documento_jefe_array_, $tdocumento_funcionario_array_, $documento_funcionario_array_, $plantel_id, $estudiantes_array, $estudiante_pg_array, $serialAnterior, $serialNuevo, $username, $nombre, $apellido, $cedula, $usuario_id, $grupo_id, $ip, $count) {
        date_default_timezone_set('America/Caracas');
        if (isset($usuario_id)) {
            $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_actual_id = $periodo_escolar_actual_id['id'];
            $mensajeExitoso = '';
            $estatus_ubicacion_titulo = 3;  // El estatus 3 es Asignado a Estudiante.

            $serialAnterior = Utiles::toPgArray2($serialAnterior, true);
            $serialNuevo = Utiles::toPgArray2($serialNuevo, true);
            $anioEgreso_array_pg_array = Utiles::toPgArray2($anioEgreso_array_pg_array, true);

            $nombre_plantel = Plantel::model()->findByPk($plantel_id);
            $nombrePlantel = ($nombre_plantel != null) ? '"' . $nombre_plantel->nombre . '"' : "";
            $documento = '';
            $nombres = '';
            $apellidos = '';
            $serial = '';
            $sexo = '';
            $anio_egreso = '';
            $tipo_evaluacion = '';
            $fecha_otorgamiento = '';
            $estatus_titulo = '';
//            var_dump($serialAnterior);
//            die();

            $grupoDRCEE = UserGroups::JEFE_DRCEE;
            if ($grupoDRCEE != null)
                $correoDrcee = Titulo::model()->obtenerCorreos($grupoDRCEE);
            else
                $correoDrcee = array();

            $correoUsuario = Titulo::model()->obtenerCorreosUsuarios($usuario_id);
            if ($correoUsuario != false) {
                $correos = array_merge($correoDrcee, $correoUsuario);
            } else {
                $correoUsuario = array();
                $correos = array_merge($correoDrcee, $correoUsuario);
            }


            if ($count > 0) {
                $guardarControlEstudianteAntes = Titulo::model()->guardarControlSeguimientoAntes($estatus_ubicacion_titulo, $periodo_actual_id);


                $transaction = Yii::app()->db->beginTransaction();
                $modulo = "Titulo.SeguimientoTitulo.AsignarEstudiante";
               // $ip = Yii::app()->request->userHostAddress;

                try {

                    $resultadoGuardarAsignacionEstudiante = Titulo::model()->asignarEstudiante($estatusTitulo_array_pg_array, $fechaOtorgamiento, $anioEgreso_array_pg_array, $tipoEvaluacion_array_pg_array, $tdocumento_director_array_, $documento_director_array_, $tdocumento_jefe_array_, $documento_jefe_array_, $tdocumento_funcionario_array_, $documento_funcionario_array_, $plantel_id, $estudiantes_array, $estudiante_pg_array, $serialAnterior, $serialNuevo, $username, $nombre, $apellido, $cedula, $usuario_id, $grupo_id, $ip, $modulo);

//       $transaction->commit();
                    $guardarControlEstudianteDespues = Titulo::model()->guardarControlSeguimientoDespues($estatus_ubicacion_titulo, $periodo_actual_id);
                    $mensajeExitoso = "Estimado Usuario, el proceso de validación de entrega de título a los estudiantes del plantel " . $nombrePlantel . "tardara un tiempo para realizarse. Por favor ingrese en otra oportunidad";
                    $respuesta['statusCode'] = 'success';
                    $respuesta['mensaje'] = $mensajeExitoso;
                    echo json_encode($respuesta);

                    echo "SE INICIO EL PROCESO DE ASIGNACION DE TÍTULO A LOS ESTUDIANTES PERTENECIENTES AL PLANTEL " . $nombrePlantel . PHP_EOL;

                    $mensaje = "Estimado usuario se le notifica que el proceso de validación de entrega de título a los estudiantes del plantel " . $nombrePlantel . " ha culminado con exito, ya puede ingresar al sistema gescolar y verificar los estudiantes a los que les fue otorgados los títulos correspondientes del MPPE.";

                    $datosEstudiantesVerifico = Titulo::model()->datosPorEstudiante($periodo_actual_id, $plantel_id, $mostrar = 1);

                    foreach ($correos as $key => $data) {
                        $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
                        $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
                        $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;
                        /* $destinatario_nombre = 'Administrador del';
                         $destinatario_apellido = 'Sistema';
                         $remitente_correo = 'soporte_gescolar@me.gob.ve';*/

                        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                        $mailer->Host = 'mail.me.gob.ve:25';
                        $mailer->IsSendmail();
                        $mailer->From = 'soporte_gescolar@me.gob.ve';  //Es quien lo envia
                        $mailer->FromName = 'Gescolar ';
                        $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                        //           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                        $mailer->CharSet = 'UTF-8';
                        $mailer->Subject = 'Notificación de Validación de Entrega de Títulos a los Estudiantes del Plantel ' . $nombrePlantel . ' del MPPE';
                        $mailer->IsHTML(true);
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');

                        if (isset($datosEstudiantesVerifico) && $datosEstudiantesVerifico !== false) {

                            $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br>
                                <br>
                                </head>';
                            $body .= "<b>$mensaje</b><br>";
                            $body .='<br><title>
                                </title>
                                <style>
                                </style>
                                <body>
                                <div id="apDiv1">
                                  <table width="100%" style="border:1px solid #9F9D9D; border-collapse:collapse;" border="1" bordercolor="#E5E5E5" cellpadding="0" cellspacing="0" class="pull-center">

                                              <tr style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                  <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b></b>
                                                   </th>
                                                  <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Documento Identidad</b>
                                                   </th>
                                                  <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Nombres</b>
                                                   </th>
                                                   <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Apellidos</b>
                                                   </th>
                                                   <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Serial</b>
                                                   </th>
                                                   <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Sexo</b>
                                                   </th>
                                                   <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Año de Egreso</b>
                                                   </th>
                                                   <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Tipo de Evaluación</b>
                                                   </th>
                                                   <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Fecha Otorgamiento</b>
                                                   </th>
                                                   <th style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                           <b>Estatus del Título Anterior</b>
                                                   </th>
                                                </tr>';
                            foreach ($datosEstudiantesVerifico as $key => $value) {
                                $keys = $key + 1;
                                $documento = (isset($datosEstudiantesVerifico[$key]['documento_identidad']) && $datosEstudiantesVerifico[$key]['documento_identidad'] != '' && $datosEstudiantesVerifico[$key]['documento_identidad'] != null)? $datosEstudiantesVerifico[$key]['tdocumento_identidad'] . '-' . $datosEstudiantesVerifico[$key]['documento_identidad']:'';
                                $nombres = (isset($datosEstudiantesVerifico[$key]['nombres']) && $datosEstudiantesVerifico[$key]['nombres'] != '' && $datosEstudiantesVerifico[$key]['nombres'] != null)? $datosEstudiantesVerifico[$key]['nombres']:'';
                                $apellidos = (isset($datosEstudiantesVerifico[$key]['apellidos']) && $datosEstudiantesVerifico[$key]['apellidos'] != '' && $datosEstudiantesVerifico[$key]['apellidos'] != null)? $datosEstudiantesVerifico[$key]['apellidos']:'';
                                $serial = (isset($datosEstudiantesVerifico[$key]['serial']) && $datosEstudiantesVerifico[$key]['serial'] != '' && $datosEstudiantesVerifico[$key]['serial'] != null)? $datosEstudiantesVerifico[$key]['prefijo'] . ' ' . $datosEstudiantesVerifico[$key]['serial']:'';
                                $sexo = (isset($datosEstudiantesVerifico[$key]['sexo']) && $datosEstudiantesVerifico[$key]['sexo'] != '' && $datosEstudiantesVerifico[$key]['sexo'] != null)? $datosEstudiantesVerifico[$key]['sexo']:'';
                                $anio_egreso = (isset($datosEstudiantesVerifico[$key]['anio_egreso']) && $datosEstudiantesVerifico[$key]['anio_egreso'] != '' && $datosEstudiantesVerifico[$key]['anio_egreso'] != null)? $datosEstudiantesVerifico[$key]['anio_egreso']:'';
                                $tipo_evaluacion = (isset($datosEstudiantesVerifico[$key]['tipo_evaluacion']) && $datosEstudiantesVerifico[$key]['tipo_evaluacion'] != '' && $datosEstudiantesVerifico[$key]['tipo_evaluacion'] != null)? $datosEstudiantesVerifico[$key]['tipo_evaluacion']:'';
                                $fecha_otorgamiento = (isset($datosEstudiantesVerifico[$key]['fecha_otorgamiento']) && $datosEstudiantesVerifico[$key]['fecha_otorgamiento'] != '' && $datosEstudiantesVerifico[$key]['fecha_otorgamiento'] != null)? $datosEstudiantesVerifico[$key]['fecha_otorgamiento']:'';
                                $estatus_titulo = (isset($datosEstudiantesVerifico[$key]['estatus_titulo']) && $datosEstudiantesVerifico[$key]['estatus_titulo'] != '' && $datosEstudiantesVerifico[$key]['estatus_titulo'] != null)? $datosEstudiantesVerifico[$key]['estatus_titulo']:'';
                                $body .= '<tr style="border:1px solid #9F9D9D; border-collapse:collapse;">
                                                <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $keys . '
                                                    </td>
                                                  <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $documento . '
                                                    </td>
                                                    <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $nombres . '
                                                    </td>
                                                    <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $apellidos . '
                                                    </td>
                                                    <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $serial . '
                                                    </td>
                                                      <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $sexo . '
                                                    </td>
                                                    <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $anio_egreso . '
                                                    </td>
                                                    <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $tipo_evaluacion . '
                                                    </td>
                                                    <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $fecha_otorgamiento . '
                                                    </td>
                                                    <td style="text-align: center; border:1px solid #9F9D9D; border-collapse:collapse;" >
                                                            ' . $estatus_titulo . '
                                                    </td>
                                               </tr>';
                            }
                            $body .=' </table>
                        </div>
                        </body>
                        </html>';
                        } else {
                            $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br>
                                <br>
                                </head>';
                            $body .= "<b>$mensaje</b><br>";
                            $body .='<br><title>
                                </title>
                                <style>
                                </style>
                                <body>
                                </body>
                        </html>';
                        }
                        $mailer->Body = $body;
                        $exito = $mailer->Send();
                        $intentos = 1;
                        while ((!$exito) && ($intentos < 2)) {
                            sleep(5);
//echo $mail->ErrorInfo;
                            $exito = $mailer->Send();
                            $intentos = $intentos + 1;
                        }
                        if (!$exito) {
                            echo "Problemas enviando correo electrónico";
                            echo "<br>" . $mailer->ErrorInfo;
                            echo " ";

                            /*$destinatario_nombre = 'Administrador del';
                            $destinatario_apellido = 'Sistema';
                            $remitente_correo = 'soporte_gescolar@me.gob.ve'*/;
                            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                            $mailer->Host = 'mail.me.gob.ve:25';
                            $mailer->IsSendmail();
                            $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                            $mailer->FromName = 'Gescolar ';
                            $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                            $mailer->CharSet = 'UTF-8';
                            $mailer->Subject = 'Notificación de Validación de Entrega de Títulos a los Estudiantes del Plantel ' . $nombrePlantel . ' del MPPE, paso 3';
                            $mailer->Body = $mensaje;
                            $exito = $mailer->Send();
                        }
                    }
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();

                    $borrarControlEstudiante = Titulo::model()->borrarControlSeguimientoError($estatus_ubicacion_titulo, $periodo_actual_id);

                    $respuesta['statusCode'] = 'error';
                    $respuesta['error'] = $ex;
                    $error = $ex->getMessage();
                    $respuesta['mensaje'] = $error;
                    echo json_encode($respuesta);

                    echo "Ocurrio un error en el proceso de validación de entrega de títulos a los estudiantes del plantel" . $nombrePlantel . PHP_EOL;

                    $mensaje = $error;
                    echo $mensaje;
                    // die();
                    $destinatario_nombre = 'Asignación de seriales a los estudiantes';
                    $destinatario_apellido = 'Plantel ' . $nombrePlantel;
                    $remitente_correo = 'gescolar.mppe@gmail.com';
//soporte_gescolar@me.gob.ve
                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSendmail();
                    $mailer->From = 'soporte_gescolar@me.gob.ve';  //Es quien lo envia
                    $mailer->FromName = 'Gescolar ';
                    $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                    //           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Notificación de Error en la Validación de Entrega de Títulos a los Estudiantes del Plantel ' . $nombrePlantel . ' del MPPE';
                    $mailer->IsHTML(true);
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                    $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                    $body .='
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                      <b>  ' . $mensaje . ' </b>
                                </body>
                                </html>';
                    $mailer->Body = $body;
                    if ($mailer->Send()) {

                        $mensaje_error = "Estimado usuario se le notifica que el proceso de Validación de Entrega de Títulos a los Estudiantes del Plantel " . $nombrePlantel . " no se puedo culminar con exito, ya que ocurrio un error en el proceso, por favor notifique este inconveniente al departamento de sistema del MPPE, para solventar el inconveniente ocasionado.";

                        $destinatario_nombre = 'Asignación de seriales a los estudiantes';
                        $destinatario_apellido = 'Plantel ' . $nombrePlantel;
                        $remitente_correo = 'gescolar.mppe@gmail.com';

                        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                        $mailer->Host = 'mail.me.gob.ve:25';
                        $mailer->IsSendmail();
                        $mailer->From = 'soporte_gescolar@me.gob.ve';  //Es quien lo envia
                        $mailer->FromName = 'Gescolar ';
                        $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                        //           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                        $mailer->CharSet = 'UTF-8';
                        $mailer->Subject = 'Notificación de Error de Validación de Entrega de Títulos a los Estudiantes del Plantel ' . $nombrePlantel . ' del MPPE';
                        $mailer->IsHTML(true);
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                        $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                        $body .='
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                      <b>  ' . $mensaje_error . ' </b>
                                </body>
                                </html>';
                        $mailer->Body = $body;
                        $exito = $mailer->Send();
                        $intentos = 1;
                        while ((!$exito) && ($intentos < 2)) {
                            sleep(5);
//echo $mail->ErrorInfo;
                            $exito = $mailer->Send();
                            $intentos = $intentos + 1;
                        }
                        if (!$exito) {
                            echo "Problemas enviando correo electrónico";
                            echo "<br>" . $mailer->ErrorInfo;
                            echo " ";

                            $destinatario_nombre = 'Administrador del';
                            $destinatario_apellido = 'Sistema';
                            $remitente_correo = 'gescolar.mppe@gmail.com';
                            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                            $mailer->Host = 'mail.me.gob.ve:25';
                            $mailer->IsSendmail();
                            $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                            $mailer->FromName = 'Gescolar ';
                            $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                            $mailer->CharSet = 'UTF-8';
                            $mailer->Subject = 'Notificación de Validación de Entrega de Títulos a los Estudiantes del Plantel ' . $nombrePlantel . ' del MPPE, paso 3';
                            $mailer->Body = 'Problemas enviando correo electrónico, paso 3';
                            $exito = $mailer->Send();
                        }
                    } else {
                        $exito = $mailer->Send();
                        $intentos = 1;
                        while ((!$exito) && ($intentos < 2)) {
                            sleep(5);
//echo $mail->ErrorInfo;
                            $exito = $mailer->Send();
                            $intentos = $intentos + 1;
                        }
                        if (!$exito) {
                            echo "Problemas enviando correo electrónico";
                            echo "<br>" . $mailer->ErrorInfo;
                            echo " ";

                            $destinatario_nombre = 'Administrador del';
                            $destinatario_apellido = 'Sistema';
                            $remitente_correo = 'gescolar.mppe@gmail.com';
                            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                            $mailer->Host = 'mail.me.gob.ve:25';
                            $mailer->IsSendmail();
                            $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                            $mailer->FromName = 'Gescolar ';
                            $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                            $mailer->CharSet = 'UTF-8';
                            $mailer->Subject = 'Notificación de Validación de Entrega de Títulos a los Estudiantes del Plantel ' . $nombrePlantel . ' del MPPE, paso 3';
                            $mailer->Body = 'Problemas enviando correo electrónico, paso 3';
                            $exito = $mailer->Send();
                        }
                    }
                }
            } else {
                echo "\n " . "NO EXISTE NINGUN ESTUDIANTE PARA LA ENTREGA DE SERIALES DISPONIBLE \n";

                $borrarControlEstudiante = Titulo::model()->borrarControlSeguimientoError($estatus_ubicacion_titulo, $periodo_actual_id);

                $mensaje_error = "Estimado usuario se le notifica que no se encontraron estudiantes disponibles del plantel " . $nombrePlantel;

                $destinatario_nombre = 'Asignación de seriales a los estudiantes';
                $destinatario_apellido = 'Plantel ' . $nombrePlantel;
                $remitente_correo = 'gescolar.mppe@gmail.com';

                $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                $mailer->Host = 'mail.me.gob.ve:25';
                $mailer->IsSendmail();
                $mailer->From = 'soporte_gescolar@me.gob.ve';  //Es quien lo envia
                $mailer->FromName = 'Gescolar ';
                $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                //           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                $mailer->CharSet = 'UTF-8';
                $mailer->Subject = 'Notificación de Error de Entrega de Títulos a los Estudiantes del Plantel ' . $nombrePlantel . ' del MPPE';
                $mailer->IsHTML(true);
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                $body .='
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                      <b>  ' . $mensaje_error . ' </b>
                                </body>
                                </html>';
                $mailer->Body = $body;
                if ($mailer->Send()) {

                    $mensaje_error = "Estimado usuario se le notifica que el proceso de Validación de Entrega de Títulos a los Estudiantes del Plantel " . $nombrePlantel . " no se puedo culminar con exito, ya que ocurrio un error en el proceso, por favor notifique este inconveniente al departamento de sistema del MPPE, para solventar el inconveniente ocasionado.";

                    $destinatario_nombre = 'Mari';
                    $destinatario_apellido = 'Sistema';
                    $remitente_correo = 'gescolar.mppe@gmail.com';

                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSendmail();
                    $mailer->From = 'soporte_gescolar@me.gob.ve';  //Es quien lo envia
                    $mailer->FromName = 'Gescolar ';
                    $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                    //           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Notificación de Error de Validación de Entrega de Títulos a los Estudiantes del Plantel ' . $nombrePlantel . ' del MPPE';
                    $mailer->IsHTML(true);
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                    $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                    $body .='
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                      <b>  ' . $mensaje_error . ' </b>
                                </body>
                                </html>';
                    $mailer->Body = $body;
                    $mailer->Send();
                }
            }
            //     }
        } else
            echo "\n " . "EL USUARIO_ID :" . $usuario_id . " NO ES UN NUMERO \n";
    }

    public function getViewPath() {
        return Yii::app()->getBasePath() . DIRECTORY_SEPARATOR;
    }

    /**
     * Modified copy of getViewFile
     * @see CController::getViewFile
     * @param $viewName
     * @return string
     */
    public function getViewFile($viewName) {
        return $this->getViewPath() . $viewName . '.php';
    }

    /**
     * Modeified copy of renderPartial from CController
     * @see CController::renderPartial
     * @param $view
     * @param $data
     * @param $return
     * @return mixed
     * @throws CException
     */
    public function renderPartial($view, $data, $return) {
        if (($viewFile = $this->getViewFile($view)) !== false) {
            $output = $this->renderFile($viewFile, $data, true);
            if ($return)
                return $output;
            else
                echo $output;
        } else
            throw new CException(Yii::t('yii', '{class} cannot find the requested view "{view}".', array('{class}' => get_class($this), '{view}' => $view)));
    }

}
