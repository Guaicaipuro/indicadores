<?php
date_default_timezone_set('America/Caracas');
class RegistroProgramadoSerialesCommand extends CConsoleCommand {

    const MODULO = "Titulo.RegistroProgramadoSerialesCommand";
    
    private static $testing = true;
    
    public function actionRegistroProgramado($prefijo, $serialInicial, $serialFinal, $grupoId) {

        $modulo = self::MODULO;
        $ip = Yii::app()->request->userHostAddress;

        try {

            $fechaInicio = date('Y-m-d H:i:s');

            $resultado = RegistroProgramadoSerial::model()->registroProgramado();

            $fechaFin = date('Y-m-d H:i:s');

            $mensajeExitoso = "Se han Procesado Exitosamente $resultado Registros";
            $respuesta['error'] = '0';
            $respuesta['statusCode'] = 'success';
            $respuesta['mensaje'] = $mensajeExitoso;

            echo json_encode($respuesta);

            $mensaje = "Estimado usuario se le notifica que el proceso de Registro Programado de Seriales correspondiente a los datos indicados (Prefijo: $prefijo, Serial Inicial: $serialInicial y Serial Final: $serialFinal) ha culminado, puede ingresar al Sistema de Gestión Escolar y verificar que los mismos estén debidamente registrado."
                     . "\n\nFecha y Hora de Inicio: $fechaInicio"
                     . "\n\nFecha y Hora de Culminación: $fechaFin";
            
            self::$testing = Yii::app()->params['testing'];
            
            if(!self::$testing){

                $correos = Titulo::model()->obtenerCorreos(array(UserGroups::JEFE_DRCEE, $grupoId));

                $administrador['nombre'] = "Administrador";
                $administrador['apellido'] = "Gescolar";
                $administrador['correo'] = Yii::app()->params['adminEmailSend'];
                
                $key = '';
                
                foreach ($correos as $key => $data) {
                    $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
                    $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
                    $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;

                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSMTP();
                    $mailer->From = 'no_reply_soporte_gescolar@me.gob.ve'; //Es quien lo envia
                    $mailer->FromName = 'Gescolar ';
                    $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                    //$mailer->AddBCC(Yii::app()->params['adminGmail'], 'Sistema de Gestión Escolar');
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Notificación de la Registro Programado de Seriales del MPPPE';
                    $mailer->Body = $mensaje;
                    $mailer->Send();
                }
                
                Utiles::enviarCorreo($administrador['correo'], 'Notificación de Registro Programado de Seriales del MPPE', $mensaje);
                
            }

        } catch (Exception $ex) {
            $respuesta['statusCode'] = 'error';
            $respuesta['error'] = $ex;
            $respuesta['mensaje'] = 'Ha ocurrido un error durante el proceso de asignación de título. Intente nuevamente.\n\n'.json_encode($respuesta);
            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
            $mailer->Host = 'mail.me.gob.ve:25';
            $mailer->IsSMTP();
            $mailer->From = Yii::app()->params['adminEmailSend']; //Es quien lo envia
            $mailer->FromName = 'Sistema de Gestión Escolar';
            $mailer->AddAddress(Yii::app()->params['adminEmailSend'], 'Equipo de Soporte del Sistema de Gestión Escolar');
            $mailer->CharSet = 'UTF-8';
            $mailer->Subject = 'Notificación de Registro Programado de Seriales del MPPPE';
            $mailer->Body = $respuesta['mensaje'];
            $mailer->Send();
            echo json_encode($respuesta);
        }
    }

}


