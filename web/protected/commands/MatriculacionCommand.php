<?php

class MatriculacionCommand extends CConsoleCommand {

    public function actionInscripcionManual($estudiantes, $seccion_plantel_id, $plantel_id, $periodo_id, $usuario_id, $ip, $username, $inscripcion_regular) {
        date_default_timezone_set('America/Caracas');
        $fecha = date('Y-m-d H:i:s');
        $modulo = "Planteles.InscripcionManual.MatriculacionCommand.InscripcionManual";
        $mensaje = '';
        echo "\n " . "INGRESO SATISFACTORIAMENTE A MatriculacionCommand->actionInscripcionManual" . "\n ";
        //  $estudiantes = Utiles::toPgArray2($estudiantes, false);
        $inscripcion_regular = '{1}';
        var_dump($estudiantes);
        echo 'aqui <br>';
        //matricula.inscribir_estudiantes(estudiantes integer[], vseccion_plantel_id integer, vplantel_id integer, vperiodo_id integer, vusuario_id integer, direccion_ip character varying, username character varying, modulo character varying, vinscripcion_regular integer[] DEFAULT NULL::integer[], vdoble_inscripcion integer[] DEFAULT NULL::integer[], vrepitiente_completo integer[] DEFAULT NULL::integer[], vmateria_pendiente integer[] DEFAULT NULL::integer[], vobservacion text[] DEFAULT NULL::text[], vrepitiente integer[] DEFAULT NULL::integer[], vdiferido integer[] DEFAULT NULL::integer[])

        $transaction = Yii::app()->db->beginTransaction();
        try {
            echo "\n " . "ENTRO EN EL BLOQUE DE TRANSACTION SATISFACTORIAMENTE." . "\n ";
            $resultadoMatriculacion = Estudiante::model()->inscribirEstudiantes($estudiantes, $plantel_id, $seccion_plantel_id, $periodo_id, $modulo, $inscripcion_regular);
            echo "\n " . $resultadoMatriculacion . "\n";
            die();
            $transaction->commit();
            $this->registerLog('ESCRITURA', $modulo, 'EXITOSO', 'Ha matriculado los estudiantes ' . $estudiantes . ' que faltaban matricular en el periodo escolar ' . $periodo_id . ' en la Seccion Plantel ' . $seccion_plantel_id . ' del plantel ' . $plantel_id, $ip, $usuario_id, $username, $fecha);
            $respuesta['statusCode'] = 'success';
            $respuesta['mensaje'] = 'Estimado Usuario, el proceso de inscripción se ha realizado exitosamente.';
            echo json_encode($respuesta);
        } catch (Exception $ex) {
            $transaction->rollback();

            $respuesta['statusCode'] = 'error';
            $respuesta['error'] = $ex;
            $error = $ex->getMessage();
            $respuesta['mensaje'] = $error;
            echo json_encode($respuesta);

            echo "Ocurrio un error en el proceso de matriculación de los estudiantes que faltan por matricular en el periodo escolar 2014" . PHP_EOL;

            $mensaje = $error;

            $destinatario_nombre = 'Gescolar';
            $destinatario_apellido = 'MPPE';
            $remitente_correo = 'mari.lac.mor@gmail.com';
//soporte_gescolar@me.gob.ve
            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
            $mailer->Host = 'mail.me.gob.ve:25';
            $mailer->IsSMTP();
            $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
            $mailer->FromName = 'Gescolar ';
            $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
            //       $mailer->AddBCC('soporte_gescolar@me.gob.ve', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
            $mailer->CharSet = 'UTF-8';
            $mailer->Subject = 'Notificación de error en el proceso de matriculación de los estudiantes que faltan por matricular en el periodo escolar 2014.';
            $mailer->IsHTML(true);
            $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
            $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
            $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
            $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
            $body .='
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                      <b>  ' . $mensaje . ' </b>
                                </body>
                                </html>';
            $mailer->Body = $body;
            if ($mailer->Send()) {

                $mensaje_error = "Estimado usuario se le notifica que Ocurrio un error en el proceso de matriculación de los estudiantes que faltan por matricular en el periodo escolar 2014, por favor notifique este inconveniente al departamento de sistema del MPPE a través del correo <b>soporte_gescolar@me.gob.ve</b>, para que obtenga una solución.";

//                        foreach ($correos as $key => $data) {
//                            $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
//                            $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
//                            $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;
                $destinatario_nombre = 'Marisela';
                $destinatario_apellido = 'La Cruz';
                $remitente_correo = 'mari.lac.mor@gmail.com';

                $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                $mailer->Host = 'mail.me.gob.ve:25';
                $mailer->IsSMTP();
                $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                $mailer->FromName = 'Gescolar ';
                $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                $mailer->CharSet = 'UTF-8';
                $mailer->Subject = 'Notificación de error en el proceso de matriculación de los estudiantes que faltan por matricular en el periodo escolar 2014.';
                $mailer->IsHTML(true);
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                $body = '<html>
                                <head>
                                <img src="cid:barra" />
                                <img class="pull-left"  src="cid:sintillo" height="46" />
                                <img class="pull-right"  src="cid:logo"  />
                                <br><br>
                                </head>';
                $body .='
                                <title>
                                </title>
                                <style>
                                </style>
                                <body>
                                      <b>  ' . $mensaje_error . ' </b>
                                </body>
                                </html>';
                $mailer->Body = $body;
                $exito = $mailer->Send();
                $intentos = 1;
                while ((!$exito) && ($intentos < 2)) {
                    sleep(5);
                    $exito = $mailer->Send();
                    $intentos = $intentos + 1;
                }
                if (!$exito) {
                    echo $mensaje .="Problemas enviando correo electrónico";
                    echo $mensaje .="<br>" . $mailer->ErrorInfo;
                    echo " ";
                    Utiles::enviarCorreo('mari.lac.mor@gmail.com', 'Notificación de error en el proceso de matriculación manual.', $mensaje);
                }
            }
        } //end catch
    }

    public function getViewPath() {
        return Yii::app()->getBasePath() . DIRECTORY_SEPARATOR;
    }

    /**
     * Modified copy of getViewFile
     * @see CController::getViewFile
     * @param $viewName
     * @return string
     */
    public function getViewFile($viewName) {
        return $this->getViewPath() . $viewName . '.php';
    }

    /**
     * Modeified copy of renderPartial from CController
     * @see CController::renderPartial
     * @param $view
     * @param $data
     * @param $return
     * @return mixed
     * @throws CException
     */
    public function renderPartial($view, $data, $return) {
        if (($viewFile = $this->getViewFile($view)) !== false) {
            $output = $this->renderFile($viewFile, $data, true);
            if ($return)
                return $output;
            else
                echo $output;
        } else
            throw new CException(Yii::t('yii', '{class} cannot find the requested view "{view}".', array('{class}' => get_class($this), '{view}' => $view)));
    }

}
