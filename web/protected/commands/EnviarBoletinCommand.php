<?php

class EnviarBoletinCommand extends CConsoleCommand {

    public function actionIndex($ids,$lapso)
    {

        $count = 0;
        //$idsArreglos = Utiles::toPgArray($ids).'::INTEGER[]';
        $informaciones = CalificacionAsignaturaEstudiante::cargarCorreosRepresentantes($ids);


        if(isset($informaciones) and $informaciones!=array()){


        foreach ($informaciones as $informacion) {
//            var_dump($informacion);
//            die();

          if(isset($informacion['correo']) and strlen($informacion['correo'])>12){


            if ($count > 10) {
                sleep(20);
                $count = 0;
            }


            $nombrePdf = 'Boletin_' . $informacion['id'] . '_' . $lapso . '_' . $informacion['periodo_id'] . '.pdf';

            $nivel =$informacion['nivel_id'] ;
            $datosCalificaciones = AsignaturaEstudiante::model()->obtenerAsignaturasEstudianteBoletin($ids,base64_encode($lapso),$nivel);

            //var_dump($datosCalificaciones);
            $ruta = realpath(yii::app()->basePath . '/../public/boletines/')."/";
            $nombre_pdf = $ruta . 'Boletin_' . $informacion['id'] . '_' . $informacion['periodo_id']; //(isset($datosPlantel['cod_plantel']) AND $datosPlantel['cod_plantel'] != '') ? $datosPlantel['cod_plantel'] . '-Matricula' : '-Matricula';


            Yii::import('ext.qrcode.QRCode');
            $url = 'escolar.dev/boletines/boletin/verificar/id/';
            $code = new QRCode($url);
            //$command = 'chmod 777 -R . '.Yii::app()->basePath . '/boletinesqr/';
           // exec($command);
            $code->create(Yii::app()->basePath.'/boletinesqr/'.'Boletin_' . $informacion['id'] . '_' . $informacion['periodo_id'].'.png');
            $nombreQr = 'Boletin_' . $informacion['id'] .'_' . $informacion['periodo_id'].'.png';
            //$command = 'chmod 777 -R . '.Yii::app()->basePath . '/boletinesqr/';
           // exec($command);

            $mpdf = new mpdf('', 'A4', 0, '', 15, 15, 75, 100);            //$mpdf->SetMargins(3,69.1,70);




                var_dump($nivel);

            $header = $this->renderPartial('modules/estudiante/views/consultar/_headerBoleta', array('datos_header'=>$informacion,'nombreQr'=>$nombreQr), true);
            $body = $this->renderPartial('modules/estudiante/views/consultar/_bodyBoleta', array('datosCalificaciones'=>$datosCalificaciones,'nivel'=>$nivel), true);

            $mpdf->SetFont('sans-serif');
            $mpdf->SetHTMLHeader($header);
            $mpdf->WriteHTML($body);

            //$this->registerLog('LECTURA', self::MODULO . 'Reporte', 'EXITOSO', 'Entró matricular la Seccion Plantel' . $seccion_plantel_id);
            $mpdf->Output($nombre_pdf . '.pdf', 'F');

            $existe_nombre_pdf = $nombre_pdf . '.pdf';


            if (file_exists($existe_nombre_pdf)) {
                $this->enviarCorreo($informacion['correo'], $subject = 'Boletin de Calificaciones', $msj = 'Boleta de calificacion', $from = '', $from_name = '', $nombre_pdf . '.pdf');
               // unlink($existe_nombre_pdf);
            }

            $count++;
       }
        }
    }
    }

    static public function enviarCorreo($to, $subject = 'SIR-SWL', $msj = '', $from = '', $from_name = '',$archivo=null,$nombre_archivo=null) {
        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $mailer->Host = 'mail.me.gob.ve:25';
        $mailer->IsSMTP();

        if (is_array($to)) {
            foreach ($to as $sendTo) {
                $mailer->AddAddress("{$sendTo}");
            }
        } else {
            $mailer->AddAddress("{$to}");
        }

        if (isset($from) and $from != '' and $from != null)
            $mailer->From = $from;
        else
            $mailer->From = Yii::app()->params->adminEmail;
        if (isset($from_name) and $from_name != '' and $from_name != null)
            $mailer->FromName = $from_name;
        else
            $mailer->FromName = Yii::app()->params->adminName;
        if($archivo){
            //$mailer->AddBCC('yguerra@me.gob.ve');
            //$mailer->AddBCC('jarojasm@me.gob.ve');
            //$mailer->AddBCC('gescolar.mppe@gmail.com');
            $mailer->AddAttachment($archivo);
        }

        $mailer->CharSet = 'UTF-8';
        $mailer->Subject = $subject;
        $mailer->MsgHTML($msj);
        return $mailer->Send();
    }


    public function getViewPath() {
        return Yii::app()->getBasePath() . DIRECTORY_SEPARATOR;
    }

    /**
     * Modified copy of getViewFile
     * @see CController::getViewFile
     * @param $viewName
     * @return string
     */
    public function getViewFile($viewName) {
        return $this->getViewPath() . $viewName . '.php';
    }

    /**
     * Modeified copy of renderPartial from CController
     * @see CController::renderPartial
     * @param $view
     * @param $data
     * @param $return
     * @return mixed
     * @throws CException
     */
    public function renderPartial($view, $data, $return) {
        if (($viewFile = $this->getViewFile($view)) !== false) {
            $output = $this->renderFile($viewFile, $data, true);
            if ($return)
                return $output;
            else
                echo $output;
        } else
            throw new CException(Yii::t('yii', '{class} cannot find the requested view "{view}".', array('{class}' => get_class($this), '{view}' => $view)));
    }
}
