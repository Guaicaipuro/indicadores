<?php

class ReportePersonalCacheCommand extends CConsoleCommand {

    public function actionIndex() {
        date_default_timezone_set('America/Caracas');
        echo "Generando Reporte de Personal".PHP_EOL;
        $model = new Personal();
        $dataReport = $model->reportePersonalGeneral();
        echo "Reporte de Personal Generado Exitosamente".PHP_EOL;

    }

    public function getViewPath() {
        return Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'views';
    }

    /**
     * Modified copy of getViewFile
     * @see CController::getViewFile
     * @param $viewName
     * @return string
     */
    public function getViewFile($viewName) {
        return $this->getViewPath() . $viewName . '.php';
    }

    /**
     * Modeified copy of renderPartial from CController
     * @see CController::renderPartial
     * @param $view
     * @param $data
     * @param $return
     * @return mixed
     * @throws CException
     */
    public function renderPartial($view, $data, $return) {
        if (($viewFile = $this->getViewFile($view)) !== false) {
            $output = $this->renderFile($viewFile, $data, true);
            if ($return)
                return $output;
            else
                echo $output;
        } else
            throw new CException(Yii::t('yii', '{class} cannot find the requested view "{view}".', array('{class}' => get_class($this), '{view}' => $view)));
    }

}
