<?php
/* @var $this AsignaturaController */
/* @var $model Asignatura */
/* @var $form CActiveForm */
?>

<div class="widget-box">
    <div id="error"></div>

    <div class="widget-header">
        <h5>
            Asignatura <?php echo $model->nombre; ?>
        </h5>

        <div class="widget-toolbar">
            <a>
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div class="widget-body-inner" style="">
            <div class="widget-main">

                <a href="#" class="search-button"></a>
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'asignatura-form',
                    'enableAjaxValidation' => false,
                ));
                ?>
                <div style="display:block" class="form">
                    <div>
                       <?php if ($form->error($model, 'nombre')): ?>
                        
                        
                            <div class="errorDialogBox">
                                <p>
                                    <?php echo $form->errorSummary($model); ?>
                                </p>
                           
                        <?php endif; ?>
                                <input type="hidden" id='id' name="id" value="<?php echo $model->id ?>" />
                            </div>
                        <div class="row" align="left">
                            <div class="col-md-12">
                                <label class="col-sm-12" for="nombre"> Nombre <span class="required">*</span></label>
                                <div class="col-md-12"><?php echo $form->textField($model, 'nombre', array('required' => 'required', 'maxlength' => 180, 'id' => 'nombre_asignatura', 'placeholder' => 'Nombre de la Asignatura', 'class' => 'span-7')); ?></div>
                            </div>
                            &nbsp;&nbsp;
                            <div class="col-md-12">
                                <label class="col-sm-12" for="abrev"> Abreviatura <span class="required">*</span></label>
                                <div class="col-md-12"><?php echo $form->textField($model, 'abreviatura', array('required' => 'required', 'maxlength' => 8, 'id' => 'abreviatura_asignatura', 'placeholder' => 'Abreviatura de la Asignatura', 'class' => 'span-7')); ?>
                                </div>
                            </div>
                        </div>
                    </div><!-- search-form -->
                </div><!-- search-form -->
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function() {
        $('#asignatura-form').on('submit', function(evt) {
            evt.preventDefault();
            crearAsignatura();
        });

        $('#nombre_asignatura').bind('keyup blur', function() {
            keyTextDash(this, true,true);
            makeUpper(this);
        });
            $('#abreviatura_asignatura').bind('keyup blur', function() {
            keyAlpha(this, true);
            makeUpper(this);
        });

        $('#nombre_asignatura').bind('blur', function() {
            clearField(this);
        });
    });
</script>