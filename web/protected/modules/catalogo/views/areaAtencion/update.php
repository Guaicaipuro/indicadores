<?php
/* @var $this AreaAtencionController */
/* @var $model AreaAtencion */

$this->pageTitle = 'Actualización de Datos de Area  de Atención';
      $this->breadcrumbs=array(
      	'Catálogo' => array('/catalogo'),
	'Areas de Atención'=>array('/catalogo/areaAtencion/'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>