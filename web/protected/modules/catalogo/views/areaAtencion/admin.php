<?php

/* @var $this AreaAtencionController */
/* @var $model AreaAtencion */

$this->breadcrumbs=array(
        'Catálogo' => array('/catalogo'),
	'Areas de Atención',
);
$this->pageTitle = 'Areas de Atención';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Areas de Atención</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>


                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/catalogo/areaAtencion/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                           Nueva Area  de Atención                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'area-atencion-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
                    
                }",
	'columns'=>array(

        array(
            'header' => '<center>Nombre</center>',
            'name' => 'nombre',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('AreaAtencion[nombre]', $model->nombre, array('title' => '',)),
        ),

        array(
            'header' => '<center>Fecha de Creación</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('AreaAtencion[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),

        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'value' => array($this, 'getEstatus'),
            'filter' => CHtml::dropDownList('AreaAtencion[estatus]', $model->estatus, array('' => '- - -', 'A' => 'Activo', 'I' => 'Inactivo', 'E' => 'Eliminado'), array('title' => '')),
            ),
		
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>

<?php
    /**
     * Yii::app()->clientScript->registerScriptFile(
     *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/AreaAtencionController/area-atencion/admin.js', CClientScript::POS_END
     *);
     */
?>