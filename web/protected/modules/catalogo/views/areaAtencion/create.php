<?php
/* @var $this AreaAtencionController */
/* @var $model AreaAtencion */

$this->pageTitle = 'Registro de Area de Atención';
      $this->breadcrumbs=array(
    'Catálogo' => array('/catalogo'),
	'Areas de Atención'=>array('/catalogo/areaAtencion/'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>