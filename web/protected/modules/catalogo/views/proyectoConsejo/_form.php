<?php
/* @var $this ProyectoConsejoController */
/* @var $model ProyectoConsejo */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'proyecto-consejo-form',
                                'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>


                        <div id="div-result">
                        <?php
           if($model->hasErrors()):
               $this->renderPartial('//errorSumMsg', array('model' => $model));
           elseif(Yii::app()->user->hasFlash('success')):               $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => Yii::app()->user->getFlash('success')));
           else:
                ?>
                        <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                <?php
                   endif;
               ?>
                        </div>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-8">
                                                            <?php echo $form->labelEx($model,'nombre'); ?>
                                                            <?php echo $form->textField($model,'nombre',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12 clasenombre', "required"=>"required",'placeholder'=>'NOMBRE DEl PROYECTO','style'=>'text-transform:uppercase;')); ?>
                                                        </div>

                                                        <?php if(isset($model->id)){ ?>
                                                         <div class="col-md-4 ">
                                                            <?php echo $form->textField($model,'id',array('class' => 'span-12 hide','value'=>base64_encode($model->id))); ?>
                                                        </div>  
                                                        <?php } ?>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6 hide">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/catalogo/proyectoConsejo"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                    <div class="col-md-6 wizard-actions hide">
                                        <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                            Guardar
                                            <i class="icon-save icon-on-right"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <div id="resultDialog" class="hide"></div>

        <?php
            /**
             * Yii::app()->clientScript->registerScriptFile(
             *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/ProyectoConsejoController/proyecto-consejo/form.js',CClientScript::POS_END
             *);
             */

            /*
             Yii::app()->clientScript->registerScriptFile(
        Yii::app()->request->baseUrl . '/public/js/modules/catalogo/proyectoConsejo/form.js', CClientScript::POS_END
                                                          );
                                                          */
        ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(
    function(){

                $('.clasenombre').bind('keyup keydown blur', function(tecla) {
                        keyTextDash(this, true, true);
                        //keyText(this, true);
                        makeUpper(this);
                        });
              
                $('#proyecto-consejo-form').submit(function(e){
                    e.preventDefault();
                    var id = $('#ProyectoConsejo_id').val();
                    if(id=='' || id ==null){
                        crearProyecto();    
                    }else{
                        editarProyecto(id);
                    }
                    
                });
                $('.clasenombre').on('blur',function(){

                    this.value = this.value.trim();
                });
                
    }       
    );
</script>