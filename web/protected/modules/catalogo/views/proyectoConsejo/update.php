<?php
/* @var $this ProyectoConsejoController */
/* @var $model ProyectoConsejo */

$this->pageTitle = 'Actualización de Datos de Proyecto Consejos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Proyecto Consejos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>