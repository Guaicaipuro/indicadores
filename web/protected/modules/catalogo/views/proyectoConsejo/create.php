<?php
/* @var $this ProyectoConsejoController */
/* @var $model ProyectoConsejo */

$this->pageTitle = 'Registro de Proyecto Consejos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Proyecto Consejos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>