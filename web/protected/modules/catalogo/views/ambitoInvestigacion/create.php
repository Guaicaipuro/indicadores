<?php
/* @var $this AmbitoInvestigacionController */
/* @var $model AmbitoInvestigacion */

$this->pageTitle = 'Registro de Ambito Investigacions';

      $this->breadcrumbs=array(
	'Ambito Investigacions'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>