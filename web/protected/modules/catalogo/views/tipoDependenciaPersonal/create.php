<?php
/* @var $this TipoDependenciaPersonalController */
/* @var $model TipoDependenciaPersonal */

$this->pageTitle = 'Registro de Tipo Dependencia Personals';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Tipo Dependencia Personals'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>