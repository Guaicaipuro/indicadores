<?php

/* @var $this TipoDependenciaPersonalController */
/* @var $model TipoDependenciaPersonal */

$this->breadcrumbs=array(
        'Catálogo' => array('/catalogo'),
	'Tipo de Dependencia Personal'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Tipo de Dependencia Personal';

?>
<div id="msj_exitoso" class="successDialogBox hide">
    <p>

    </p>
</div>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Tipo Dependencia Personal</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Tipo de Dependencia Personal.
                            </p>
                        </div>
                    </div>

                    <div  class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/catalogo/tipoDependenciaPersonal/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Tipo de Dependencia Personal                      
                        </a>
                    </div>

                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tipo-dependencia-personal-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                                function(){
                                
                                    function inactivar(registro_id) {
    $('#pregunta_inactivar').removeClass('hide');
    $('#msj_error_inactivar').addClass('hide');
    $('#msj_error_inactivar p').html('');

    var dialogInactivar = $('#dialog_inactivacion').removeClass('hide').dialog({
        modal: true,
        width: '600px',
        draggable: false,
        resizable: false,
        title: '<div><h4> &nbsp;&nbsp;&nbsp;Inactivar Registro de Dependencia Personal</h4></div>',
        title_html: true,
        buttons: [
            {
                html: '&nbsp; Cancelar',
                'class': 'btn btn-xs',
                click: function() {
                    $(this).dialog('close');
                }
            },
            {
                html: '&nbsp; Inactivar',
                'class': 'btn btn-danger btn-xs',
                click: function() {
                    var data = {
                        id: registro_id
                    };
                    $.ajax({
                        url: '/catalogo/tipoDependenciaPersonal/inactivarTipoDependenciaPersonal',
                        data: data,
                        dataType: 'html',
                        type: 'post',
                        success: function(resp, resp2, resp3) {

                            try {
                                var json = jQuery.parseJSON(resp3.responseText);

                                if (json.statusCode == 'success') {

                                    refrescarGrid();
                                    dialogInactivar.dialog('close');
                                    $('#msj_error_inactivar').addClass('hide');
                                    $('#msj_error_inactivar p').html('');
                                    $('#msj_exitoso').removeClass('hide');
                                    $('#msj_exitoso p').html(json.mensaje);
                                    $('html, body').animate({scrollTop: 0}, 'fast');
                                }

                                if (json.statusCode == 'error') {

                                    $('#pregunta_inactivar').addClass('hide');
                                    $('#msj_exitoso').addClass('hide');
                                    $('#msj_exitoso p').html('');
                                    $('#msj_error_inactivar').removeClass('hide');
                                    $('#msj_error_inactivar p').html(json.mensaje);
                                    $('html, body').animate({scrollTop: 0}, 'fast');
                                }
                            }
                            catch (e) {

                            }
                        }
                    })
                }
            }
        ]
    });
    $('#dialog_inactivacion').show();
}

$('.add-activar').unbind('click');
$('.add-activar').on('click', function(e)
{
    e.preventDefault();
    var registro_id;
    registro_id = $(this).attr('data');
    activar(registro_id);
});


function activar(registro_id) {

    $('#pregunta_activar').removeClass('hide');
    $('#msj_error_activar').addClass('hide');
    $('#msj_error_activar p').html('');

    var dialogActivar = $('#dialog_activacion').removeClass('hide').dialog({
        modal: true,
        width: '600px',
        draggable: false,
        resizable: false,
        title: '<div><h4>&nbsp;&nbsp;&nbsp;Activar Registro del Tipo de Dependencia Personal</h4></div>',
        title_html: true,
        buttons: [
            {
                html: '&nbsp; Cancelar',
                'class': 'btn btn-xs',
                click: function() {
                    $(this).dialog('close');
                }
            },
            {
                html: '&nbsp; Activar',
                'class': 'btn btn-success btn-xs',
                click: function() {
                    var data = {
                        id: registro_id
                    }

                    $.ajax({
                        url: '/catalogo/tipoDependenciaPersonal/activarTipoDependenciaPersonal',
                        data: data,
                        dataType: 'html',
                        type: 'post',
                        success: function(resp, resp2, resp3) {

                            try {
                                var json = jQuery.parseJSON(resp3.responseText);

                                if (json.statusCode == 'success') {

                                    refrescarGrid();
                                    dialogActivar.dialog('close');
                                    $('#msj_error_activar').addClass('hide');
                                    $('#msj_error_activar p').html('');
                                    $('#msj_exitoso').removeClass('hide');
                                    $('#msj_exitoso p').html(json.mensaje);
                                    $('html, body').animate({scrollTop: 0}, 'fast');
                                }

                                if (json.statusCode == 'error') {

                                    $('#pregunta_activar').addClass('hide');
                                    $('#msj_exitoso').addClass('hide');
                                    $('#msj_exitoso p').html('');
                                    $('#msj_error_activar').removeClass('hide');
                                    $('#msj_error_activar p').html(json.mensaje);
                                    $('html, body').animate({scrollTop: 0}, 'fast');
                                }
                            }
                            catch (e) {

                            }
                        }
                    })
                }
            }
        ]
    });
    $('#dialog_activacion').show();
}

function refrescarGrid() {

    $('#tipo-dependencia-personal-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
}


                                    $('#TipoDependenciaPersonal_nombre').on('keyup keydown',function(){
                                        keyAlphaNum(this, true, true);
                                        makeUpper(this);
                                    });
                                    
                                    $('.add-inactivar').unbind('click');
                                    $('.add-inactivar').on('click',
                                            function(e) {
                                                e.preventDefault();
                                                var registro_id;
                                                 registro_id=$(this).attr('data');

                                                 inactivar(registro_id);
                                            }
                                    );

                                    $('.add-activar').unbind('click');
                                    $('.add-activar').on('click',
                                            function(e) {
                                                e.preventDefault();
                                                var registro_id;
                                                 registro_id=$(this).attr('data');

                                                 activar(registro_id);
                                            }
                                    );
                                }",
	'columns'=>array(
       /* array(
            'header' => '<center>Id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('TipoDependenciaPersonal[id]', $model->id, array('title' => '',)),
        ), */
        array(
            'header' => '<center>Nombre</center>',
            'name' => 'nombre',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('TipoDependenciaPersonal[nombre]', $model->nombre, array('title' => '',)),
        ),
            /*
        array(
            'header' => '<center>Usuario_ini_id</center>',
            'name' => 'usuario_ini_id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('TipoDependenciaPersonal[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Usuario_act_id</center>',
            'name' => 'usuario_act_id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('TipoDependenciaPersonal[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Fecha_ini</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('TipoDependenciaPersonal[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),
        array(
            'header' => '<center>Fecha_act</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('TipoDependenciaPersonal[fecha_act]', $model->fecha_act, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>fecha_elim</center>',
            'name' => 'fecha_elim',
            'htmlOptions' => array(),
                        'filter' => CHtml::textField('TipoDependenciaPersonal[fecha_elim]', $model->fecha_elim, array('title' => '',)),
                    ),
             * */
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            'value'=>array($this,'estatus'),
            'filter' => array('A' => 'Activo', 'I' => 'Inactivo'),
             ),
		array(
                    'type' => 'raw',
                    'header' => '<center>Acciones</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>
<div id="dialog_inactivacion" class="hide">
    <div id="pregunta_inactivar" class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas Seguro(a) que Desea Inactivar este Registro?
        </p>
    </div>
    <div id="msj_error_inactivar" class="errorDialogBox hide">
        <p>

        </p>
    </div>
</div>

<div id="dialog_activacion" class="hide">
    <div id="pregunta_activar" class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas Seguro(a) que Desea Activar este Registro?
        </p>
    </div>
    <div id="msj_error_activar" class="errorDialogBox hide">
        <p>

        </p>
    </div>
</div>

<?php
    
     Yii::app()->clientScript->registerScriptFile(
      Yii::app()->request->baseUrl . '/public/js/modules/catalogo/TipoDependenciaPersonal/admin.js', CClientScript::POS_END
                                                 );
     
?>