<?php
/* @var $this TipoDependenciaPersonalController */
/* @var $model TipoDependenciaPersonal */

$this->pageTitle = 'Actualización de Datos de Tipo Dependencia Personals';
      $this->breadcrumbs=array(
        'Catálogos'=>array('/catalogo/'),
	'Tipo de Dependencia Personal'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>