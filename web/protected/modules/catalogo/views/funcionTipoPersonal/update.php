<?php
/* @var $this FuncionTipoPersonalController */
/* @var $model FuncionTipoPersonal */

$this->pageTitle = 'Actualización de Datos de Función del Tipo de Personal';

      $this->breadcrumbs=array(
    'Catálogo'=>array('/catalogo'),
	'Función del Tipo de Personal'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>