<?php
/* @var $this FuncionTipoPersonalController */
/* @var $model FuncionTipoPersonal */

$this->pageTitle = 'Registro de Función del Tipo de Personal';

      $this->breadcrumbs=array(
    'Catálogo'=>array('/catalogo'),
	'Función del Tipo de Personal'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>