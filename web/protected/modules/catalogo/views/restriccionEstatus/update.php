<?php
/* @var $this RestrinccionEstatusController */
/* @var $model RestrinccionEstatus */

$this->pageTitle = 'Actualización de Datos de Restriccion Estatus';

      $this->breadcrumbs=array(
	'Restriccion Estatus'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>