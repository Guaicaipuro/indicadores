<?php
/* @var $this TiempoDedicacionController */
/* @var $model TiempoDedicacion */

$this->pageTitle = 'Actualización de Datos de Tiempo Dedicacions';

$this->breadcrumbs=array(
    'Catálogos'=>array('/catalogo/'),
    'Tiempo de Dedicación'=>array('/catalogo/tiempoDedicacion'),
    'Edición',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion','exitoA'=>$exitoA)); ?>