<?php
/* @var $this TipoPersonalCargoController */
/* @var $model TipoPersonalCargo */

$this->pageTitle = 'Registro de Tipo Personal Cargos';

      $this->breadcrumbs=array(
	'Tipo Personal Cargos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>