<?php
/* @var $this CredencialController */
/* @var $model Credencial */

$this->breadcrumbs=array(
	'Credencials'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Credencial', 'url'=>array('index')),
	array('label'=>'Create Credencial', 'url'=>array('create')),
	array('label'=>'View Credencial', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Credencial', 'url'=>array('admin')),
);
?>

<h1>Update Credencial <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>