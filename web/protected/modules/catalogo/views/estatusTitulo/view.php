<?php
/* @var $this EstatusTituloController */
/* @var $model EstatusTitulo */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
    'Catálogos' => array('/catalogo'),
    'Estatus de Titulos'=>array('/catalogo/estatusTitulo'),
    'Detalles',

);
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'estatus-titulo-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Vista de Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php $model['nombre'] = strtoupper($model['nombre']); ?>
                                                            <label class="col-md-12">Nombre <span></span></label>
                                                            <?php echo $form->textField($model,'nombre',array('size'=>60, 'maxlength'=>150, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <label class="col-md-12">Fecha de creación <span></span></label>
                                                            
                                                        
                                                            <?php if (isset($model->fecha_ini) || $model->fecha_ini != ''){
                                                                    $fecha_i = CHtml::encode(date("d-m-Y H:i:s",strtotime($model->fecha_ini)));
                                                                    }else {
                                                                        $fecha_i = 'NO POSEE';
                                                                    } ?>
                                                            <?php echo CHtml::textField('fecha_ini',$fecha_i, array('class' => 'span-12', 'disabled'=>'disabled','value'=>$fecha_i)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <label class="col-md-12">Fecha de actualización <span></span></label>
                                                            
                                                            <?php if (isset($model->fecha_act) || $model->fecha_act != ''){
                                                                    $fecha_a = CHtml::encode(date("d-m-Y H:i:s",strtotime($model->fecha_act)));
                                                                    }else {
                                                                        $fecha_i = 'NO POSEE';
                                                                    } ?>
                                                            <?php echo CHtml::textField('fecha_act',$fecha_a, array('class' => 'span-12', 'disabled'=>'disabled','value'=>$fecha_a)); ?>

                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>


                                                    <div class="col-md-12">


                                                        <div class="col-md-4">
                                                            <label class="col-md-12">Estatus <span></span></label>
                                                            <?php echo $form->textField($model,'estatus', array('class' => 'span-12', 'disabled'=>'disabled','value' => ($model->estatus == 'A')? 'ACTIVO':'INACTIVO')); ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/catalogo/estatusTitulo"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>