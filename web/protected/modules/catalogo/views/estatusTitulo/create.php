<?php
/* @var $this EstatusTituloController */
/* @var $model EstatusTitulo */

$this->pageTitle = 'Registro de Estatus de Titulo';
      $this->breadcrumbs=array(
        'Catálogos' => array('/catalogo'),
	'Estatus de Titulos'=>array('/catalogo/estatusTitulo'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>