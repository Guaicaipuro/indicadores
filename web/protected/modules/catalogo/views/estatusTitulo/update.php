<?php
/* @var $this EstatusTituloController */
/* @var $model EstatusTitulo */

$this->pageTitle = 'Actualización de Datos de Estatus Titulos';
      $this->breadcrumbs=array(
        'Catálogos' => array('/catalogo'),
	'Estatus de Titulos'=>array('/catalogo/estatusTitulo'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>