<?php
/* @var $this DenominacionTipoPersonalController */
/* @var $model DenominacionTipoPersonal */

$this->pageTitle = 'Registro de Denominación  por Tipo de Personal';

$this->breadcrumbs=array(
	'Catálogos'=>array('/catalogo/'),
	'Denominacion por Tipo de Personal'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>