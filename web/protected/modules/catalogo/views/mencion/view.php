
<?php $resultado = Mencion::model()->buscar($model->id); ?>
<?php $resultado2 = Mencion::model()->buscar2($model->id); ?>


<div class="view">

    <div class="tabbable">

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#datosGenerales">Datos Generales</a></li>
        </ul>

        <div class="tab-content">

            <div id="datosGenerales" class="tab-pane active">

                <div class="widget-main form">

                    <?php foreach ($resultado as $r) { ?>

                        <?php $fecha_creacion = date("d-m-Y H:i:s", strtotime($r['fecha_ini'])); ?>
                    <?php $fecha_eliminacion = date("d-m-Y H:i:s", strtotime($r['fecha_elim'])); ?>

                        <div class="row">

                            <div class="col-md-6">
                                <label class="col-md-12" ><b>Mencion:</b></label>
                            </div>

                            <div class="col-md-6">
                                <label class="col-md-12" ><?php echo $r['nombre']; ?></label>
                            </div>


                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <label class="col-md-12" ><b>Creado por:</b></label>
                            </div>

                            <div class="col-md-6">
                                <label class="col-md-12" ><?php echo $r['nombre_usuario']; ?></label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label class="col-md-12" ><b>Fecha de Creación:</b></label>
                            </div>

                            <div class="col-md-6">
                                <label class="col-md-12" ><?php echo $fecha_creacion; ?></label>
                            </div>
                        </div>



                        <?php if ($r['fecha_elim'] != null) { ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-md-12" ><b>Fecha de Eliminación:</b></label>
                                </div>

                                <div class="col-md-6">
                                    <label class="col-md-12" ><?php echo $fecha_eliminacion; ?></label>
                                </div>
                            </div>

                        <?php } ?>

                        <div class="row">
                            <div class="col-md-6">
                                <label class="col-md-12" ><b>Estatus:</b></label>
                            </div>
                            <div class="col-md-6">
                                <?php
                                if (($r['estatus'] == 'A')) {
                                    $columna = "Activo";
                                } else {
                                    $columna = "Inactivo";
                                }
                                ?>
                                <label class="col-md-12" ><?php echo $columna; ?></label>
                            </div>
                        </div>

                    <?php } ?>
                    <div class="row">

                        <?php foreach ($resultado2 as $r2) { ?>
                            <?php $fecha_creacion2 = date("d-m-Y H:i:s", strtotime($r2['fecha_ini'])); ?>
                            <?php $fecha_actualizacion2 = date("d-m-Y H:i:s", strtotime($r2['fecha_act'])); ?>
                            <?php $fecha_eliminacion2 = date("d-m-Y H:i:s", strtotime($r2['fecha_elim'])); ?>

                            <?php if ($r2['nombre'] != null and $r2['fecha_act'] != null) { ?>							<div class="col-md-6">
                                    <label class="col-md-12" ><b>Modificado por:</b></label>
                                </div>

                                <div class="col-md-6">

                                    <label class="col-md-12" ><?php echo $r['nombre_usuario']; ?></label>
                                    </label>
                                </div>
                            <?php } ?>						</div>

                        <div class="row">
                            <?php if ($r2['fecha_act'] != null) { ?>
                                <div class="col-md-6">
                                    <label class="col-md-12"><b>Fecha de Actualización:</b></label>
                                </div>

                                <div class="col-md-6">
                                    <label class="col-md-12">
                                        <?php echo $fecha_creacion2; ?>
                                    </label>
                                </div>

                            </div>
                            <div class="row">
                            </div>
                        <?php } ?>

                    <?php } ?>

                </div>

            </div>
        </div>
    </div>
</div>