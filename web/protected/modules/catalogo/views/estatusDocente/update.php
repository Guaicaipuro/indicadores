<?php
/* @var $this EstatusDocenteController */
/* @var $model EstatusDocente */

$this->pageTitle = 'Actualización de Datos de Estatus Personal';


$this->breadcrumbs=array(
    'Catalogo'=>array('/catalogo/'),
    'Estatus Personal'=>array('/catalogo/estatusDocente/lista'),
    'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>