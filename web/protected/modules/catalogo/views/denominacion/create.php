<?php
/* @var $this DenominacionController */
/* @var $model Denominacion */

$this->pageTitle = 'Registro de Denominación';

$this->breadcrumbs=array(
    'Catálogos'=>array('/catalogo/'),
    'Denominación'=>array('lista'),
    'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro','estatus'=>$estatus)); ?>