<?php
/* @var $this BancoController */
/* @var $model Banco */

$this->pageTitle = 'Actualización de Datos de Bancos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Bancos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>