<?php
/* @var $this UnidadGrupoController */
/* @var $model UnidadGrupo */

$this->pageTitle = 'Actualización de Datos de Unidad Grupos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Unidad Grupos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>