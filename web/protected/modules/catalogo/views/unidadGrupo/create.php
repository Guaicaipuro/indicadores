<?php
/* @var $this UnidadGrupoController */
/* @var $model UnidadGrupo */

$this->pageTitle = 'Registro de Unidad Grupos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Unidad Grupos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>