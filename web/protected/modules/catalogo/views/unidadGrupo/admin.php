<?php

/* @var $this UnidadGrupoController */
/* @var $model UnidadGrupo */

$this->breadcrumbs=array(
        'Catálogo' => array('/catalogo'),
	'Unidad Grupos'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Unidad Grupos';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Unidad Grupos</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Unidad Grupos.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/catalogo/unidadGrupo/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Unidad Grupos                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'unidad-grupo-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>Unidad Responsable</center>',
            'name' => 'unidad_resp_ticket_id',
            'htmlOptions' => array(),
            'value'=>'(is_object($data->unidadRespTicket))?$data->unidadRespTicket->nombre:""',
            'filter' => CHtml::listData(UnidadRespTicket::model()->findAll(),'id','nombre'),
        ),
        array(
            'header' => '<center>Grupo</center>',
            'name' => 'group_id',
            'htmlOptions' => array(),
            'value'=>'(is_object($data->group))?$data->group->groupname:""',
            'filter' =>  CHtml::listData(UsergroupsGroup::model()->findAll(),'id','groupname'),
        ),
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            'value'=>array($this,'estatus'),
            'filter' => array('A'=>'Activo','E'=>'Eliminado'),
        ),
			array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>

<?php
    /**
     * Yii::app()->clientScript->registerScriptFile(
     *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/UnidadGrupoController/unidad-grupo/admin.js', CClientScript::POS_END
     *);
     */
?>