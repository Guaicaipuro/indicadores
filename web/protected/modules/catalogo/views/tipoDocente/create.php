<?php
/* @var $this TipoDocenteController */
/* @var $model TipoDocente */

$this->pageTitle = 'Registro de Tipo Docentes';

      $this->breadcrumbs=array(
	'Tipo Docentes'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>