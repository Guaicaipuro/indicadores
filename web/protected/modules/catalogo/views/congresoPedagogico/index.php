<?php

/* @var $this CongresoPedagogicoController */
/* @var $model CongresoPedagogico */

$this->breadcrumbs=array(
	'Congreso Pedagogicos'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Congreso Pedagogicos';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Congreso Pedagogicos</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Congreso Pedagogicos.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/catalogo/congresoPedagogico/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Congreso Pedagogicos                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'congreso-pedagogico-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[id]', $model->id, array('title' => '',)),
        ),
        array(
            'header' => '<center>plantel_id</center>',
            'name' => 'plantel_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[plantel_id]', $model->plantel_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>periodo_id</center>',
            'name' => 'periodo_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[periodo_id]', $model->periodo_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>linea_investigacion_id</center>',
            'name' => 'linea_investigacion_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[linea_investigacion_id]', $model->linea_investigacion_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>ambito_investigacion_id</center>',
            'name' => 'ambito_investigacion_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[ambito_investigacion_id]', $model->ambito_investigacion_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_inicio</center>',
            'name' => 'fecha_inicio',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[fecha_inicio]', $model->fecha_inicio, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>fecha_fin</center>',
            'name' => 'fecha_fin',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[fecha_fin]', $model->fecha_fin, array('title' => '',)),
        ),
        array(
            'header' => '<center>observaciones</center>',
            'name' => 'observaciones',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[observaciones]', $model->observaciones, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_ini_id</center>',
            'name' => 'usuario_ini_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_ini</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_act_id</center>',
            'name' => 'usuario_act_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_act</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[fecha_act]', $model->fecha_act, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_elim</center>',
            'name' => 'fecha_elim',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[fecha_elim]', $model->fecha_elim, array('title' => '',)),
        ),
        array(
            'header' => '<center>estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CongresoPedagogico.php[estatus]', $model->estatus, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>