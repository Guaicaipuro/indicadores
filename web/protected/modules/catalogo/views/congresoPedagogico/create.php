<?php
/* @var $this CongresoPedagogicoController */
/* @var $model CongresoPedagogico */

$this->pageTitle = 'Registro de Congreso Pedagogicos';

      $this->breadcrumbs=array(
	'Congreso Pedagogicos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>