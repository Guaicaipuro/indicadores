<?php
/* @var $this ProyectosEndogenosController */
/* @var $model ProyectosEndogenos */

$this->breadcrumbs=array(
	'Proyectos Endogenoses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProyectosEndogenos', 'url'=>array('index')),
	array('label'=>'Create ProyectosEndogenos', 'url'=>array('create')),
	array('label'=>'Update ProyectosEndogenos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProyectosEndogenos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProyectosEndogenos', 'url'=>array('admin')),
);
?>

<h1>View ProyectosEndogenos #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		'usuario_ini_id',
		'fecha_ini',
		'usuario_act_id',
		'fecha_act',
		'fecha_elim',
		'estatus',
	),
)); ?>
