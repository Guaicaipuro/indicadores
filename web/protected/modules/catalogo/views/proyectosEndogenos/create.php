<?php
/* @var $this ProyectosEndogenosController */
/* @var $model ProyectosEndogenos */

$this->breadcrumbs=array(
	'Proyectos Endogenoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProyectosEndogenos', 'url'=>array('index')),
	array('label'=>'Manage ProyectosEndogenos', 'url'=>array('admin')),
);
?>

<h1>Create ProyectosEndogenos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>