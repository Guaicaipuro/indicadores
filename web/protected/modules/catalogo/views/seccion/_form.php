<?php
/* @var $this SeccionController */
/* @var $model Seccion */
/* @var $form CActiveForm */
?>
<script>
$(document).ready(function() {    
$('#nombreDeSeccion').bind('keyup blur', function() {
    keyAlpha(this, false);// true acepta la ñ y para que sea español
    makeUpper(this);
});

$("#resultadoSeccionRegistrar").click(function() {
    document.getElementById("resultadoSeccionRegistrar").style.display = "none";
    document.getElementById("resultadoRegistrar").style.display = "block";
});

function noENTER(evt)
        {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text"))
            {
                return false;
            }
        }
        document.onkeypress = noENTER;
});
   
</script>
<div class="form" id="_form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'seccion-form',
	'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions' => array(
    //  'validateOnSubmit' => true,
            'validateOnType' => true,
            'validateOnChange' => true),
)); ?>

    
    
    <div class="tab-pane active" id="registrarS">

        <div id="autor" class="widget-box ">

            <div id="resultadoSeccionRegistrar">
            </div>

            <div id="resultadoRegistrar" class="infoDialogBox">
                <p>
                    Por Favor Ingrese los datos correspondientes para registrar una sección, Los campos marcados con <span class="required">*</span> son requeridos.
                </p>
            </div>

            <div id ="guardoRegistro" class="successDialogBox" style="display: none">
                <p>
                    Registro Exitoso
                </p>
            </div> 

            <div class="widget-header">
                <h5>Registrar Sección</h5>
                <div class="widget-toolbar">
                    <a  href="#" data-action="collapse">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div id="registrarSeccion" class="widget-body" >
                <div class="widget-body-inner" >
                    <div class="widget-main form">                      

                        <div style="padding: 20px" class="row">
                           
                            <div  id="divCapacidad" class="col-md-4" >
                                        <?php echo $form->labelEx($model,'nombre', array("class" => "col-md-12")); ?>
                                        <?php echo $form->textField($model,'nombre', array('class' => 'span-7', 'id' => 'nombreDeSeccion', 'maxlength'=>'1', 'size'=>'1')); ?>
                                        
                            </div>
                           
                            
                        </div>
                        <br>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
  <!--  <div id="botones" class="row">

        <div class="col-md-6">
            <a id="btnRegresar" href=" // echo Yii::app()->createUrl("planteles/consultar/"); " class="btn btn-danger">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
        </div>


    </div> -->

        <?php
        $this->endWidget();?>

</div><!-- form -->
    
  
    
    
    
    
    
   
