<?php
/* @var $this SituacionCargoController */
/* @var $model SituacionCargo */

$this->pageTitle = 'Actualización de Datos de Situacion Cargos';

      $this->breadcrumbs=array(
	'Catálogos'=>array('/catalogo/'),
	'Situación de Cargos'=>array('/catalogos/situacionCargo'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>