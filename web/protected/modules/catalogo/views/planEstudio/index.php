<?php
$this->breadcrumbs = array(
    'Catalogo' => array('/catalogo'),
    'Planes de Estudio',
);
?>
<div class = "widget-box collapsed">

    <div class = "widget-header">
        <h4>Búsqueda Avanzada de Planes de Estudio</h4>

        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-down"></i>
            </a>
        </div>

    </div>

    <div class = "widget-body">
        <div style = "display: none;" class = "widget-body-inner">
            <div class = "widget-main">

                <?php echo CHtml::link('', '#', array('class' => 'search-button'));
                ?>
                <div class="search-form" style="display:block">
                    <?php
                    $this->renderPartial('_search', array(
                        'dropDownCredencial' => $dropDownCredencial,
                        'dropDownMencion' => $dropDownMencion,
                        'dropDownFundJuridico' => $dropDownFundJuridico,
                        'model' => $model
                    ));
                    ?>
                </div><!-- search-form -->
            </div>
        </div>
    </div>

</div>



<div class="widget-box">

    <?php
    Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	$('.search-form form').submit(function(){
		$('#plantel-grid').yiiGridView('update', {
			data: $(this).serialize()
		});
		return false;
	});
	");
    ?>

    <div class="widget-header">
        <h4>Planes de Estudio</h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
                <div>
                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo Yii::app()->baseUrl . '/catalogo/planEstudio/crear/' ?>" data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar nuevo Plan de Estudio
                        </a>
                    </div>

                    <div class="col-lg-12"><div class="space-6"></div></div>

                    <?php
                    $nombre = array(
                        'header' => '<center>Nombre</center>',
                        'name' => 'nombre',
                            //'value' => '(is_object($data->nombre) && isset($data->nombre))? $data->nombre : ""',
                    );
                    $cod_plan = array(
                        'header' => '<center>Código de Plan</center>',
                        'name' => 'cod_plan',
                            //'value' => '(is_object($data->cod_plan) && isset($data->cod_plan))? $data->cod_plan : ""',
                    );
                    $mencion = array(
                        'header' => '<center>Mención</center>',
                        'name' => 'mencion_id',
                        'value' => '(is_object($data->mencion) && isset($data->mencion->nombre))? $data->mencion->nombre : ""',
                        'filter' => CHtml::listData(
                                Mencion::model()->findAll(
                                        array(
                                            'order' => 'nombre ASC'
                                        )
                                ), 'id', 'nombre'
                        ),
                    );
                    $credencial = array(
                        'header' => '<center>Credencial</center>',
                        'name' => 'credencial_id',
                        'value' => '(is_object($data->credencial) && isset($data->credencial->nombre))? $data->credencial->nombre : ""',
                        'filter' => CHtml::listData(
                                Credencial::model()->findAll(
                                        array(
                                            'order' => 'nombre ASC'
                                        )
                                ), 'id', 'nombre'
                        ),
                    );
                    $fund_juridico = array(
                        'header' => '<center>Fundamento Juridico</center>',
                        'name' => 'fund_juridico_id',
                        'value' => '(is_object($data->fund_juridico_id) && isset($data->fund_juridico_id->nombre))? $data->fund_juridico_id->nombre : ""',
                        'filter' => CHtml::listData(
                                FundamentoJuridico::model()->findAll(
                                        array(
                                            'order' => 'nombre ASC'
                                        )
                                ), 'id', 'nombre'
                        ),
                    );
                    /* $estatusPlantel = array(
                      'header' => '<center title="Estatus del Plantel">Estatus</center>',
                      'name' => 'estatus_plantel_id',
                      'value' => '(is_object($data->estatusPlantel) && isset($data->estatusPlantel->nombre))? $data->estatusPlantel->nombre: ""',
                      'filter' => CHtml::listData(
                      EstatusPlantel::model()->findAll(
                      array(
                      'order' => 'id ASC',
                      )
                      ), 'id', 'nombre'
                      ),
                      );
                     * 
                     */


                    $this->widget('zii.widgets.grid.CGridView', array(
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'id' => 'plantel-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'pager' => array('pageSize' => 1),
                        'summaryText' => false,
                        'afterAjaxUpdate' => "function(){
                            
                             $('#Plan_nombre').bind('keyup blur', function() {
                                 keyText(this, false);
                             });
                             $('#Plan_cod_plan').bind('keyup blur', function() {
                                 keyNum(this, false);
                             });

                             $('.look-data-plan').unbind('click');
                             $('.look-data-plan').on('click',
                                function(e) {
                                    e.preventDefault();
                                    var id = $(this).attr('data-id');
                                    var plan_id = $(this).attr('data-plan_id');
                                    verDetalleGrado(id, plan_id);
                                }
    );
 
                         }",
                        'columns' => array(
                            $nombre,
                            $cod_plan,
                            $mencion,
                            $credencial,
                            $fund_juridico,
                            //$estatus,
                            array(
                                'type' => 'raw',
                                'header' => '<center>Acciones</center>',
                                'value' => array($this, 'columnaAcciones'),
                                'htmlOptions' => array('nowrap' => 'nowrap'),
                            ),
                        ),
                        'pager' => array(
                            'header' => '',
                            'htmlOptions' => array('class' => 'pagination'),
                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>

<div class="row">

    <div class="col-md-6">
        <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("catalogo"); ?>" class="btn btn-danger">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>

    <!-- <div class="col-md-6 wizard-actions">
         <button type="submit" data-last="Finish" class="btn btn-primary btn-next">
             Guardar
             <i class="icon-save icon-on-right"></i>
         </button>
     </div>
    -->

</div>


<div id="detalleGrado" class="hide"></div>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/planes.js', CClientScript::POS_END);
?>
