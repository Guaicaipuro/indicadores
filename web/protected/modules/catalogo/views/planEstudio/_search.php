<div class="widget-main form">



    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="row">

        <div id="1eraFila" class="col-md-12">
            <div class="col-md-4" >
                <?php echo $form->labelEx($model, 'nombre', array("class" => "col-md-12")); ?>
                <?php echo $form->textField($model, 'nombre', array('class' => 'span-7')); ?>
            </div>

            <div class="col-md-4" >
                <?php echo $form->labelEx($model, 'credencial_id', array("class" => "col-md-12")); ?>
                <?php echo $form->dropDownList($model, 'credencial_id', CHtml::listData($dropDownCredencial, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-7')); ?>
            </div>
            <div class="col-md-4" >
                <?php echo $form->labelEx($model, 'mencion_id', array("class" => "col-md-12")); ?>
                <?php echo $form->dropDownList($model, 'mencion_id', CHtml::listData($dropDownMencion, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-7')); ?>
            </div>

        </div>
        <div id="2daFila" class="col-md-12">
            <div class="col-md-4" >
                <?php echo $form->labelEx($model, 'fund_juridico_id', array("class" => "col-md-12")); ?>
                <?php echo $form->dropDownList($model, 'fund_juridico_id', CHtml::listData($dropDownFundJuridico, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-7')); ?>
            </div>

            <div class="col-md-4" >
                <?php echo $form->labelEx($model, 'cod_plan', array("class" => "col-md-12")); ?>
                <?php echo $form->textField($model, 'cod_plan', array('class' => 'span-7')); ?>

            </div>
            <div class="col-md-4">
            </div>

        </div>
    </div> 

    <div class="space-6"></div>

    <div class="row">
        <div class=" pull-right">
            <div class="col-md-12" style="margin-right: 35px;">
                <button class="btn btn-primary btn-next btn-sm" data-last="Finish" type="submit" id="buscarPlan">
                    Buscar
                    <i class="fa fa-search icon-on-right"></i>
                </button>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
