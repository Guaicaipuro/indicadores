<?php
/* @var $this ProgramaApoyoController */
/* @var $model ProgramaApoyo */

$this->pageTitle = 'Registro de Programa Apoyos';
      $this->breadcrumbs=array(
    'Catálogo' => array('/catalogo'),
	'Programa Apoyos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>