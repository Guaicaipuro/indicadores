<?php
/* @var $this ProgramaApoyoController */
/* @var $model ProgramaApoyo */

$this->pageTitle = 'Actualización de Datos de Programas de Apoyo';
      $this->breadcrumbs=array(
        'Catálogo' => array('/catalogo'),
	'Programa de Apoyo'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>