<?php
/* @var $this DefaultController */
$this->pageTitle='Datos Catálogo';
$this->breadcrumbs = array(
    'Catálogo'
);
?>


<div  class="col-xs-12">
    <p>
        <?php if(Yii::app()->user->pbac('catalogo.areaAtencion.read')):?>
    <div class="linkCatalogo" onclick="">
        <span class="titulo">Area de Atencion</span>
        <a href="/catalogo/areaAtencion/" class="circle" style="background-image: url('#');">
        </a>
    </div>
    <?php endif; ?>
        <?php if(Yii::app()->user->pbac('catalogo.areaComun.read')):?>
    <div class="linkCatalogo" onclick="">
        <span class="titulo">Areas Comunes</span>
        <a href="/catalogo/areaComun/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Estructura_Espacios.png' ?>');">
        </a>
    </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.asignatura.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Asignatura</span>
            <a href="/catalogo/asignatura/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/asignaturas.png' ?>');">
            </a>
        </div>
    <?php endif; ?>

    <?php if(Yii::app()->user->pbac('catalogo.banco.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo"> Bancos</span>
            <a href="/catalogo/banco/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/banco.png'?>');">
            </a>
        </div>
    <?php endif; ?> 

    <?php if(Yii::app()->user->pbac('catalogo.cargo.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Cargos</span>
            <a href="/catalogo/cargo/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Cargos.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.clasePlantel.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Clases de Planteles</span>
            <a href="/catalogo/clasePlantel/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/clasesDePlanteles.png' ?>');">
            </a>
        </div>
    <?php endif; ?>

    <?php if(Yii::app()->user->pbac('catalogo.categoriaConsejo.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo"> Categoría de Proyectos de los Consejos Educativos </span>
            <a href="/catalogo/categoriaConsejo/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/CategoriasDeProyectos.png'?>');">
            </a>
        </div>
    <?php endif; ?> 

    <?php if(Yii::app()->user->pbac('catalogo.condicionEstudio.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Condiciones de Estudio</span>
            <a href="/catalogo/condicionEstudio/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/condicionesDeEstudios.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.condicionServicio.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Condici&oacuten de Servicio</span>
            <a href="/catalogo/condicionServicio/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/condicionesServicios.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.credencial.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Credenciales</span>
            <a href="/catalogo/credencial/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/credenciales.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.denominacionPersonal.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Denominación de Cargo</span>
            <a href="/catalogo/denominacionPersonal/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Denominacion-Cargo.png' ?>');" >
                <!--style="background-image: url('<?php /*echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Grado_Instruccion.png' */?>');"-->
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.denominacionTipoPersonal.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Denominación de Cargo por Tipo de Personal</span>
            <a href="/catalogo/denominacionTipoPersonal/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/DenominacionCargoTP.png' ?>');">
            </a>
        </div>
    <?php endif; ?>

    <?php if(Yii::app()->user->pbac('catalogo.denominacion.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Denominación de Plantel</span>
            <a href="/catalogo/denominacion/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/DenominacionPlantel.png' ?>');">

            </a>
        </div>
    <?php endif; ?>

    <?php if(Yii::app()->user->pbac('catalogo.dependenciaNominal.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Dependencia Nominal</span>
            <a href="/catalogo/dependenciaNominal/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/dependenciaNominal.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.especialidad.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Especialidad</span>
            <a href="/catalogo/especialidad/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Especialidad.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.especialidadTipoPersonal.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Especialidad por Tipo de Personal</span>
            <a href="/catalogo/especialidadTipoPersonal/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Especialidad-Tipo-Personal.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.especificacionEstatus.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Especificación de Estatus</span>
            <a href="/catalogo/especificacionEstatus/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Especificacion-Status.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.estatusDocente.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Estatus Personal</span>
            <a href="/catalogo/estatusDocente/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Estatus.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.estatusPlantel.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Estatus Plantel</span>
            <a href="/catalogo/estatusPlantel/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/estadoPlantel.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.estatusTitulo.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Estatus Titulo</span>
            <a href="/catalogo/estatusTitulo/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Estatus_Titulo.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.estudio.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Estudios Especializados [Personal]</span>
            <a href="/catalogo/estudio/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Estudios.png' ?>');" >
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.funcion.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Función Personal</span>
            <a href="/catalogo/funcion/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Funcionalidades.png' ?>');">
            </a>
        </div>
    <?php endif; ?>

    <?php if(Yii::app()->user->pbac('catalogo.funcionTipoPersonal.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Función por Tipo de Personal</span>
            <a href="/catalogo/funcionTipoPersonal/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Funciones-Tipo-Personal.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.tiempoDedicacion.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Tiempo de Dedicación</span>
            <a href="/catalogo/tiempoDedicacion/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Tiempo-de-Dedicacion.png' ?>');" >
            </a>

        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.tipoEvaluacion.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Tipo de Evaluación</span>
            <a href="/catalogo/tipoEvaluacion/" class="circle" );" >
            </a>

        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.tipoFundamento.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Fundamentos</span>
            <a href="/catalogo/tipoFundamento/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/fundamentos.png' ?>');">
            </a>
        </div>
    <?php endif; ?>

    <?php if(Yii::app()->user->pbac('catalogo.grado.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Grados</span>
            <a href="/catalogo/grado/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/grados.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.gradoInstruccion.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Grados de Instrucción</span>
            <a href="/catalogo/gradoInstruccion/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Grado_Instruccion.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.mencion.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Menciones</span>
            <a href="/catalogo/mencion/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/menciones.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.modalidad.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Modalidades</span>
            <a href="/catalogo/modalidad/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/modalidades.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.nivel.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Nivel</span>
            <a href="/catalogo/nivel/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/niveles.png' ?>');">
            </a>
        </div>
    <?php endif; ?>

    <?php if(Yii::app()->user->pbac('catalogo.periodoEscolar.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Período Escolar</span>
            <a href="/catalogo/periodoEscolar/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/periodoEscolar.png' ?>');">
            </a>
        </div>
    <?php endif; ?>

    <?php if(Yii::app()->user->pbac('catalogo.planEstudio.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Planes de Estudio</span>
            <a href="/catalogo/planEstudio/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/planesDeEstudio.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.programaApoyo.read')):?>
    <div class="linkCatalogo" onclick="">
        <span class="titulo">Programa de Apoyo</span>
        <a href="/catalogo/programaApoyo/" class="circle" style="background-image: url('#');">
        </a>
    </div>
    <?php endif; ?>
    
    <?php if(Yii::app()->user->pbac('catalogo.proyectoConsejo.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo"> Proyectos de Consejos Educativos</span>
            <a href="/catalogo/proyectoConsejo/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/ProyectosDeCons-Educativo.png'?>');">
            </a>
        </div>
    <?php endif; ?>   

    <?php if(Yii::app()->user->pbac('catalogo.tipoPersonal.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Tipo de Personal</span>
            <a href="/catalogo/tipoPersonal/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Tipo_Personal.png'?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.tipoTicket.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Tipo de Solicitud</span>
            <a href="/catalogo/tipoTicket/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Tipo_Solicitud.png'?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.turno.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Turnos</span>
            <a href="/catalogo/turno/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/turno.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.situacionCargo.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Situación de Cargo</span>
            <a href="/catalogo/situacionCargo/" class="circle"  style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Situacion-del-Cargo.png' ?>');" >
                <!--style="background-image: url('<?php /*echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Grado_Instruccion.png' */?>');"-->
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.seccion.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Sección</span>
            <a href="/catalogo/seccion/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/secciones.png' ?>');">
            </a>
        </div>
        <!--        <a href="/catalogo/turno/" class="lol">

                <img src="<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/turno.png' ?>"/>
                <span class="titulo">Turnos</span>
            </a>-->
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.servicio.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Servicios</span>
            <a href="/catalogo/servicio/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Servicios.png' ?>');">
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->pbac('catalogo.unidadRespTicket.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo"> Unidades Responsables </span>
            <a href="/ayuda/unidadRespTicket/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Unidad_responsable.png'?>');">
            </a>
        </div>
    <?php endif; ?>
        <?php if(Yii::app()->user->pbac('catalogo.tipoDependenciaPersonal.read')):?>
        <div class="linkCatalogo" onclick="">
            <span class="titulo">Tipo de Dependencia Personal</span>
            <a href="/catalogo/tipoDependenciaPersonal/" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Unidad_responsable.png'?>');">
            </a>
        </div>
    <?php endif; ?>


    </p>
</div>
<?php echo CHtml::cssFile('/public/css/iconosCatalogo.css'); ?>
