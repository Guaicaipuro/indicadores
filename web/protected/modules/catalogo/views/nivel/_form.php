<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'nivel-form',
        'enableAjaxValidation' => false,
        #'id'=>'seccion-form',
        #'enableAjaxValidation' => false,
        #'enableClientValidation' => true,
        'clientOptions' => array(
            //'validateOnSubmit' => true,
            'validateOnType' => true,
            'validateOnChange' => true),
    ));
    ?>

    <div class="widget-box">
        <div id="resultadoRegistrar" class="infoDialogBox">
            <p>
                Por Favor Ingrese los datos correspondientes para registrar una sección, Los campos marcados con <span class="required">*</span> son requeridos.
            </p>
        </div>
        <div class="widget-header">
            <h4>Nivel</h4>

            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>

        </div>

        <div class="widget-body">
            <div class="widget-body-inner" style="display: block;">
                <div class="widget-main">

                    <a href="#" class="search-button"></a>
                    <div style="display:block" class="search-form">
                        <div class="widget-main form">
                            <input type="hidden" id='id' name="id" value="<?php echo $model->id ?>" />

                            <div class="row">
                                <div class="col-md-6">
                                    Nombre del Nivel <span class="required">*</span>
                                    <br>
                                    <?php echo $form->textField($model, 'nombre', array('size' => 70, 'maxlength' => 100, 'class' => 'col-sm-12', 'id' => 'Nivel_nombre_form')); ?>
                                </div>
                                <div class="col-md-6">
                                    Permite materia pendiente <span class="required">*</span><br>
                                    <?php echo $dropDownList;?>
                                    <?php //echo $form->dropDownList($model,'permite_materia_pendiente',array('1'=>'men','2'=>'women'));?>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-4">
                                    Tipo de periodo <span class="required">*</span><br>
                                    <?php
                                    echo $form->dropDownList(
                                            $model, 'tipo_periodo_id', CHtml::listData($tipoPeriodo, 'id', 'nombre'), array('class' => 'span-5', 'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7')
                                    );
                                    ?>
                                </div>

                                <div class="col-md-4">
                                    <?php
                                    $tipo_periodo = $model->tipo_periodo_id;
                                    if($tipo_periodo == 1) {
                                        $textCantidad = "Cantidad de año(s)";
                                    }
                                    else if($tipo_periodo == 2) {
                                        $textCantidad = "Cantidad de semestre(s)";
                                    }
                                    else if($tipo_periodo == 3) {
                                        $textCantidad = "Cantidad de trimestre(s)";
                                    }
                                    else {
                                        $textCantidad = "Cantidad de periodo(s)";
                                    }
                                    ?>
                                    <div id="cantidadPeriodo"><?php echo $textCantidad; ?> <span class="required">*</span></div>
                                    <?php echo $form->textField($model, 'cantidad', array('maxlength' => 2, 'class' => 'span-7', 'id' => 'Nivel_cantidad_form')); ?>
                                </div>

                                <div class="col-md-4">
                                    Cantidad de lapsos <span class="required">*</span><br>
                                    <?php echo $form->textField($model, 'cant_lapsos', array('maxlength' => 2, 'class' => 'span-7', 'id' => 'Nivel_cant_lapsos_form')); ?>
                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- search-form -->
                </div><!-- search-form -->
            </div>
        </div>
    </div>

</div>


<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/nivel.js', CClientScript::POS_END);
?>

<script>
    $(document).ready(function() {
        $('#nivel-form').on('submit', function(evt) {
            evt.preventDefault();
            registrarNivel();
        });
        $('#Nivel_nombre_form').bind('keyup blur', function() {
            makeUpper(this);
            keyAlphaNum(this, true, true);
        });
        $('#Nivel_cantidad_form').bind('keyup blur', function() {
            keyNum(this, false);// true acepta la ñ y para que sea español
        });
        $('#Nivel_nombre_form').bind('blur', function() {
            clearField(this);
        });
        $('#Nivel_cant_lapsos_form').bind('keyup blur', function() {
            keyNum(this, false);// true acepta la ñ y para que sea español
        });
        
        $('#Nivel_tipo_periodo_id').bind('change', function() {
            var tipo_periodo = $('#Nivel_tipo_periodo_id').val();
            if(tipo_periodo == 1) {
                $("#cantidadPeriodo").html("Cantidad de año(s) <span class='required'>*</span>");
            }
            else if(tipo_periodo == 2) {
                $("#cantidadPeriodo").html("Cantidad de semestre(s) <span class='required'>*</span>");
            }
            else if(tipo_periodo == 3) {
                $("#cantidadPeriodo").html("Cantidad de trimestre(s) <span class='required'>*</span>");
            }
            else {
                $("#cantidadPeriodo").html("Cantidad de periodo(s) <span class='required'>*</span>");
            }
        });
        /*$('#nombre_turno').bind('keyup blur', function () {
         keyAlphaNum(this, true, true);
         //makeUpper(this);
         });
         
         $('#nombre_turno').bind('blur', function () {
         clearField(this);
         });*/
    });
</script>