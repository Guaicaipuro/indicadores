<?php
/* @var $this NivelController */
/* @var $model Nivel */

$this->breadcrumbs = array(
    'Catálogo' => array('/catalogo'),
    'Nivel',
);
?>

<div class="widget-box">

    <div class="widget-header">
        <h5>Nivel</h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div class="widget-body-inner" style="display: block;">
            <div class="widget-main">


                <div style="padding-left:10px;" class="pull-right">
                    <a class="btn btn-success btn-next btn-sm" data-last="Finish" href="#" onclick="registrarNivel();">
                        <i class="fa fa-plus icon-on-right"></i>
                        Registrar nuevo nivel
                    </a>
                </div>

                <div class="col-lg-12"><div class="space-6"></div></div>

                <a href="#" class="search-button"></a>
                <div style="display:block" class="search-form">
                    <div>


                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                            'id' => 'plantel-grid',
                            'dataProvider' => $model->search(),
                            'filter' => $model,
                            'pager' => array('pageSize' => 1),
                            'summaryText' => false,
                            'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination'),
                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                            ),
                            'id' => 'nivel-grid',
                            'dataProvider' => $model->search(),
                            'filter' => $model,
                            'afterAjaxUpdate' => "function(){
                            
                                    $('#nombreNivel').bind('keyup blur', function() {
                                       makeUpper(this);
                                       keyAlphaNum(this,true,true);
                                   });

                                }",
                            'columns' => array(
                                array(
                                    'header' => 'Nombre del Nivel',
            'name' => 'nombre',
                                    'filter' => CHtml::textField('Nivel[nombre]', null, array('maxlength' => 70, 'id' => 'nombreNivel')),
                                ),
                                array(
                                    'header' => '<center>Tipo de Nivel</center>',
            'name' => 'tipo_periodo_id',
                                    'value' => '(is_object($data->tipoPeriodo) && isset($data->tipoPeriodo->nombre))? $data->tipoPeriodo->nombre: ""',
                                    'filter' => CHtml::listData(
                                            TipoPeriodo::model()->findAll(
                                                    array(
                                                        'order' => 'id ASC'
                                                    )
                                            ), 'id', 'nombre'
                                    ),
                                ),
                                array(
                                    'header' => 'Cantidad de periodo',
                                    'name' => 'cantidad',
                                    'filter' => CHtml::textField('Nivel[cantidad]', null, array('maxlength' => 70, 'id' => 'cantidad')),
                                ),
                                array(
                                    'header' => 'Cantidad de lapsos',
                                    'name' => 'cant_lapsos',
                                    'filter' => CHtml::textField('Nivel[cant_lapsos]', null, array('maxlength' => 70, 'id' => 'cant_lapsos')),
                                ),
                                array(
                                    'header' => 'Estatus',
                                    'name' => 'estatus',
                                    'filter' => array('A' => 'Activo', 'E' => 'Inactivo'),
                                    'value' => array($this, 'columnaEstatus'),
                                ),
                                array('type' => 'raw',
                                    'header' => '<center>Acciones</center>',
                                    'value' => array($this, 'columnaAcciones'),
                                ),
                            ),
                        ));
                        ?>
                        <div class="row-fluid-actions">
                            <a href="/catalogo" class="btn btn-danger">
                                <i class="icon-arrow-left bigger-110"></i>
                                Volver
                            </a>
                        </div>


                    </div><!-- search-form -->
                </div><!-- search-form -->
            </div>
        </div>
    </div>

</div>

<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="dialogPantalla" class="hide"></div>
<div id="dialogPantallaConsultar" class="hide"></div>
<div id="dialogPantallaEliminar" class="hide"> 
    <div class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas seguro de eliminar este Nivel?
        </p>
    </div>
</div>

<div id="dialogPantallaActivacion" class="hide"> 
    <div class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas seguro que desea Activar este Nivel?
        </p>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/nivel.js', CClientScript::POS_END);
?>

