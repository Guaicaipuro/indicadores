<?php
/* @var $this CategoriaConsejoController */
/* @var $model CategoriaConsejo */

$this->pageTitle = 'Registro de Categoria Consejos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Categoria Consejos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'lista_proyectos'=>$lista_proyectos, 'formType'=>'registro')); ?>