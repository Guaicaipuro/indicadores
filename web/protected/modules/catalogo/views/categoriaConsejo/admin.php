<?php

/* @var $this CategoriaConsejoController */
/* @var $model CategoriaConsejo */

$this->breadcrumbs=array(
        'Catálogo' => array('/catalogo'),
	'Categoria Consejos',
);
$this->pageTitle = 'Administración de Categoria Consejos';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Categoria Consejos</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
            <div id="result">
                
            </div>
                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Categoria.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/catalogo/categoriaConsejo/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nueva Categoria                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'categoria-consejo-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                    $('#CategoriaConsejo_nombre').bind('keyup keydown blur', function() {
                        
                        keyTextDash(this, true, true);
                        makeUpper(this);
                        
                    });

                    $('#CategoriaConsejo_proyecto_nom').bind('keyup keydown blur', function() {
                        
                        keyTextDash(this, true, true);
                        makeUpper(this);
                        
                    });
                }",
	'columns'=>array(
       /* array(
            'header' => '<center>Id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('CategoriaConsejo[id]', $model->id, array('title' => '',)),
        ),*/
        array(
            'header' => '<center>Nombre</center>',
            'name' => 'nombre',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('CategoriaConsejo[nombre]', $model->nombre, array('title' => '',)),
        ),
        array(
            'header' => '<center>Proyecto</center>',
            'name' => 'proyecto_id',
            'htmlOptions' => array(),
            //'value' => array($this, 'nombreProyecto'),
            //'filter' => CHtml::textField('CategoriaConsejo[proyecto_id]', $model->proyecto_id, array('title' => '',)),
            'value' => '(is_object($data->proyecto) && isset($data->proyecto->nombre))? $data->proyecto->nombre: ""',
            //'filter' => CHtml::textField('CategoriaConsejo[proyecto_nom]', $model->proyecto_nom, array('title' => '',)),
            'filter' => CHtml::listData(
                                ProyectoConsejo::model()->findAll(
                                        array(
                                            'order' => 'nombre ASC'
                                        )
                                ), 'id', 'nombre')
        ),
        /*array(
            'header' => '<center>Usuario_ini_id</center>',
            'name' => 'usuario_ini_id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('CategoriaConsejo[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
        ),*/
        /*array(
            'header' => '<center>Fecha_ini</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('CategoriaConsejo[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),*/
        /*array(
            'header' => '<center>Usuario_act_id</center>',
            'name' => 'usuario_act_id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('CategoriaConsejo[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
        ),*/
		/*
        array(
            'header' => '<center>fecha_act</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
                        'filter' => CHtml::textField('CategoriaConsejo[fecha_act]', $model->fecha_act, array('title' => '',)),
                    ),*/
        /*array(
            'header' => '<center>Fecha_elim</center>',
            'name' => 'fecha_elim',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('CategoriaConsejo[fecha_elim]', $model->fecha_elim, array('title' => '',)),
        ),*/
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'value' => array($this, 'estatus'),
            'filter' => array('A' => 'Activo', 'I' => 'Inactivo'),
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('CategoriaConsejo[estatus]', $model->estatus, array('title' => '',)),
        ),
		
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>

<?php
    /**
     * Yii::app()->clientScript->registerScriptFile(
     *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/CategoriaConsejoController/categoria-consejo/admin.js', CClientScript::POS_END
     *);
     */

     Yii::app()->clientScript->registerScriptFile(
        Yii::app()->request->baseUrl . '/public/js/modules/catalogo/categoriaConsejo/admin.js', CClientScript::POS_END
     );
?>
<div id="dialogPantalla" class="hide" ></div>