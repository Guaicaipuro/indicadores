<?php
/* @var $this ClasePlantelController */
/* @var $model ClasePlantel */
/* @var $form CActiveForm */
?>
<!--Evitar la redireccion-->
 <script>
        function noENTER(evt)
        {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text"))
            {
                return false;
            }
        }
        document.onkeypress = noENTER;
</script>




<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'clase-plantel-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
 

        <div class="widget-body">
        
            	<div class="widget-main form">

                             
                                <?php
                                    if($form->errorSummary($model)):
                                ?>
                                <div id ="div-result-message" class="errorDialogBox" >
                                    <?php echo $form->errorSummary($model); ?>
                                </div>
                                <?php
                                    endif;
                                ?>
	                               


                                    <div class="row">

                                         <input type="hidden" id='id' name="id" value="<?php echo base64_encode($model->id); ?>" />
                                                                
                                             <div class="col-md-6">
<label class="col-md-12" for="groupname">Nombre o Descripción</label>
<?php echo
$form->textField($model,'nombre',array('size'=>30,'maxlength'=>30,'class
'=>'col-xs-10 col-sm-9', 'required'=>'required')); ?>
</div>                                     </div>
<div class="row">
                                        		
                                        		
                                        	</div>
                </div>

   
                     


         </div>


<?php $this->endWidget(); ?>
<!-- form -->
<script>

$(document).ready(function (){

$("#ClasePlantel_nombre").keyup(function(){

makeUpper(this);

});

$('#ClasePlantel_nombre').bind('keyup blur', function () {
 keyText(this, true);

});

$('#ClasePlantel_nombre').bind('blur', function () {
  clearField(this);
 });

                            });
</script>