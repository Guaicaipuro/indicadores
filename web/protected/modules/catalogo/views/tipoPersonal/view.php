<?php
/* @var $this TipoPersonalController */
/* @var $model TipoPersonal */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
	'Tipo Personals'=>array('lista'),
);
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'tipo-personal-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-datos-generales">                                                                                                                                                                                                                                                                
                                                        <div class="widget-main form">
                                                                    <div class="row">                                                    

                                                                        <div class="col-md-6">
                                                                            <label class="col-md-12" ><b>Nombre:</b></label>					
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <label class="col-md-12" ><?php echo CHtml::encode($model->nombre); ?></label>					
                                                                        </div>
                                                                    </div>
                                                            
                                                                    <div class="row">  
                                                                        <div class="col-md-6">
                                                                            <label class="col-md-12" ><b>Creado por:</b></label>			
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <label class="col-md-12" ><?php echo CHtml::encode($model->usuarioIni->nombre . " " . $model->usuarioIni->apellido); ?></label>
                                                                        </div>

                                                                </div>
                                                   
                                                                                                              
                                                                <div class="row" >
                                                                    <div class="col-md-6">
                                                                    <label class="col-md-12" ><b>Fecha de Creación:</b></label>		
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                    <label class="col-md-12" ><?php echo CHtml::encode(date("d-m-Y H:i:s",strtotime($model->fecha_ini)));?></label>				
                                                                    </div>
                                                                </div>
                                                                                              
                                                                <div class="row" >
                                                                    <div class="col-md-6">
                                                                        <label class="col-md-12" ><b>Estatus:</b></label>		
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label class="col-md-12" >
                                                                            <?php
                                                                            if ($model->estatus == "A") {
                                                                                echo "Activo";
                                                                            } else if ($model->estatus == "I") {
                                                                                echo "Inactivo";
                                                                            } else if ($model->estatus == "E") {
                                                                                echo "Eliminado";
                                                                            }
                                                                            ?>			
                                                                        </label>
                                                                    </div>
                                                                </div> 
                                                           
                                                            <div class="row">  
                                                                <?php if ($model->estatus =="I") { ?>
                                                                    <div class="col-md-6">
                                                                            <label class="col-md-12" ><b>Modificado por:</b></label>			
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <label class="col-md-12" ><?php echo CHtml::encode($model->usuarioAct->nombre . " " . $model->usuarioAct->apellido); ?></label>
                                                                        </div>
                                                                    <div class="col-md-6">
                                                                        <label class="col-md-12"><b>Inhabilitado el:</b></label>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <label class="col-md-12"><?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime($model->fecha_elim))); ?></label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label class="col-md-12"><b>Fecha de Actualización:</b></label>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <label class="col-md-12">
                                                                            <?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime($model->fecha_act))); ?>	
                                                                        </label>
                                                                    </div>

                                                                <?php } ?>
                                                            </div>
                                                </div>                            
                        <?php $this->endWidget(); ?>
                    <!-- form -->
                </div>               
            </div>
        </div>

    </div>
</div>