<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/TipoPersonal/index.js', CClientScript::POS_END
);
?>
<?php
/* @var $this TipoPersonalController */
/* @var $model TipoPersonal */

$this->breadcrumbs = array(
    'Catálogo' => array('/catalogo/'),
    'Tipo de Personal',
    'Administración',
);
$this->pageTitle = 'Administración de Tipo Personal';
?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Tipo Personal</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <!--        <div style="display:block;" class="widget-body-inner">-->
        <div class="widget-main">

            <div class="row space-6"></div>
            <div>
                <div id="resultadoOperacion">
                </div>
                <div class="row space-6">
                </div>
                <div class="pull-right" style="padding-left:10px;">
                    <a  type="submit" onclick="VentanaDialog('', '/catalogo/tipoPersonal/registro', 'Tipo Personal', 'create', '')" data-last="Finish" class="btn btn-success btn-next btn-sm">
                        <i class="fa fa-plus icon-on-right"></i>
                        Registrar Tipo de Personal
                    </a>
                </div>
                <div class="row space-20"></div>
                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'tipo-personal-grid',
                    'dataProvider' => $dataProvider,
                    'filter' => $model,
                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                    //'summaryText' => 'Mostrando {start}-{end} de {count}',
                    'summaryText' => false,
                    'pager' => array(
                        'header' => '',
                        'htmlOptions' => array('class' => 'pagination'),
                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                    ),
                    'afterAjaxUpdate' => "
                function(){
                  
                    $('#TipoPersonal_fecha_ini').datepicker();
                    $('#TipoPersonal_fecha_act').datepicker();
                    $.datepicker.setDefaults($.datepicker.regional = {
                            dateFormat: 'dd-mm-yy',
                            showOn:'focus',
                            showOtherMonths: false,
                            selectOtherMonths: true,
                            changeMonth: true,
                            changeYear: true,
                            minDate: new Date(1800, 1, 1),
                            maxDate: 'today'
                        });
                    
                    $('#TipoPersonal_nombre').unbind('keyup blur');
                    $('#TipoPersonal_nombre').on('keyup blur', function () {
                        keyText(this, true);
                    });
                    
                    $('#TipoPersonal_nombre').unbind('blur');
                    $('#TipoPersonal_nombre').on('blur', function () {
                        clearField(this);
                    });
                    
                    $('#TipoPersonal_fecha_act').on('dblclick', function(){
                        $(this).val('');
                        $('#tipo-personal-grid').yiiGridView('update', {
                            data: $(this).serialize()
                        });
                    });
                     $('#TipoPersonal_fecha_ini').on('dblclick', function(){
                        $(this).val('');
                        $('#tipo-personal-grid').yiiGridView('update', {
                            data: $(this).serialize()
                        });
                    });

                }",
                    'columns' => array(
                        array(
                            'header' => '<center>Nombre</center>',
                            'name' => 'nombre',
                            'htmlOptions' => array(),
                            'filter' => CHtml::textField('TipoPersonal[nombre]', $model->nombre, array('title' => '',)),
                        ),
                        array(
                            'header' => '<center>Fecha de Creación</center>',
                            'name' => 'fecha_ini',
                            'htmlOptions' => array(),
                            'value' => array($this, 'getFechaIni'),
                            'filter' => CHtml::textField('TipoPersonal[fecha_ini]', $model->fecha_ini, array('title' => 'Fecha de Creación', 'readOnly' => 'readOnly')),
                        ),
                        array(
                            'header' => '<center>Fecha de Actualización</center>',
                            'name' => 'fecha_act',
                            'htmlOptions' => array(),
                            'value' => array($this, 'getFechaAct'),
                            'filter' => CHtml::textField('TipoPersonal[fecha_act]', $model->fecha_act, array('title' => 'Fecha de Actualización al hacer, Para ver lista hacer Doble Click', 'readOnly' => 'readOnly',)),
                        ),
                        array(
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            'value' => array($this, 'getEstatus'),
                            'filter' => CHtml::dropDownList('TipoPersonal[estatus]', $model->estatus, array('' => '- - -', 'A' => 'Activo', 'I' => 'Inactivo', 'E' => 'Eliminado'), array('title' => '')),
                        ),
                        array(
                            'type' => 'raw',
                            'header' => '<center>Acción</center>',
                            'value' => array($this, 'getActionButtons'),
                            // 'htmlOptions' => array('nowrap' => 'nowrap'),
                        ),
                    ),
                ));
                ?>
            </div>
        </div>
        <!--        </div>-->
    </div>
    <div class="space-6"><div class="row"></div></div>
    <div class="row">
        <div class="col-md-6">
            <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("catalogo"); ?>" class="btn btn-danger">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
        </div>
        <!-- <div class="col-md-6 wizard-actions">
             <button type="submit" data-last="Finish" class="btn btn-primary btn-next">
                 Guardar
                 <i class="icon-save icon-on-right"></i>
             </button>
         </div>
        -->

    </div>
</div>
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="dialogPantalla" class="hide"></div>
<div id="dialogInhabilitar" class="hide">

    <div class="alert alert-info bigger-110">
        <p class="bigger-110 center"> ¿Desea usted Inhabilitar este Tipo de Personal?</p>
    </div>
</div>
<div id="dialogActivar" class="hide">

    <div class="alert alert-info bigger-110">
        <p class="bigger-110 center"> ¿Desea usted Activar este Tipo de Personal?</p>
    </div>
</div>

