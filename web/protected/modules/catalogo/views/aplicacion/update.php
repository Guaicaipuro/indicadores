<?php
/* @var $this AplicacionController */
/* @var $model Aplicacion */

$this->pageTitle = 'Actualización de Datos de Aplicacions';

      $this->breadcrumbs=array(
	'Aplicacions'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>