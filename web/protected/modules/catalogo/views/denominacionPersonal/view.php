<?php
/* @var $this DenominacionController */
/* @var $model Denominacion */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
    'Catálogos'=>array('/catalogo/'),
    'Denominación de Cargo'=>array('lista'),
    'Consulta',
);
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'denominacion-personal-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Vista de Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'nombre'); ?>
                                                            <?php echo $form->textField($model,'nombre',array('size'=>35, 'maxlength'=>35, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <?php if(isset($model->usuario_ini_id)):
                                                            $usuario_ini = $model->usuarioIni->nombre." ".$model->usuarioIni->apellido;
                                                            ?>

                                                            <div class="col-md-4">
                                                                <?php echo $form->labelEx($model,'usuario_ini_id'); ?>
                                                                <?php echo $form->textField($model,'usuario_ini_id',array('size'=>6,'maxlength'=>6, 'class' => 'span-12', 'disabled'=>'disabled','value'=>$usuario_ini)); ?>
                                                            </div>

                                                        <?php endif;?>

														
														<?php if(isset($model->fecha_ini)):
														$fecha_ini = date("d-m-Y H:i:s",strtotime($model->fecha_ini));
														?> 
														
                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_ini'); ?>
                                                            <?php echo $form->textField($model,'fecha_ini',array('size'=>6, 														'maxlength'=>6, 'class' => 'span-12', 'disabled'=>'disabled','value'=>$fecha_ini)); ?>
                                                        </div>
														
														<?php endif;?>

                                                        <?php if(isset($model->usuario_act_id)):
                                                            $usuario_ini = $model->usuarioAct->nombre." ".$model->usuarioAct->apellido;
                                                            ?>

                                                            <div class="col-md-4">
                                                                <?php echo $form->labelEx($model,'usuario_act_id'); ?>
                                                                <?php echo $form->textField($model,'usuario_act_id',array('size'=>6,'maxlength'=>6, 'class' => 'span-12', 'disabled'=>'disabled','value'=>$usuario_ini)); ?>
                                                            </div>

                                                        <?php endif;?>

                                                        <?php if(isset($model->fecha_act)):
                                                            $fecha_act = date("d-m-Y H:i:s",strtotime($model->fecha_act));
                                                            ?>

                                                            <div class="col-md-4">
                                                                <?php echo $form->labelEx($model,'fecha_act'); ?>
                                                                <?php echo $form->textField($model,'fecha_act',array('size'=>6, 														'maxlength'=>6, 'class' => 'span-12', 'disabled'=>'disabled','value'=>$fecha_act)); ?>
                                                            </div>

                                                        <?php endif;?>


														<?php if(isset($model->fecha_elim)):?> 
														
                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_elim'); ?>
                                                            <?php echo $form->textField($model,'fecha_elim',array('size'=>6, 														'maxlength'=>6, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>
														
														<?php endif;?>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'estatus'); ?>
                                                            <?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado'), array('prompt'=>'- - -', 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/catalogo/denominacion"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>