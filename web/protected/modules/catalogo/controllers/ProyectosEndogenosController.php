<?php

class ProyectosEndogenosController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $defaultAction = 'index';
    static $_permissionControl = array(
        'read' => 'Consultar Proyectos Endogenos',
        'write' => 'Gestiona los Proyectos Endogenos',
        'label' => 'Proyectos Endogenos'
    );

    const MODULO = "Catalogo.ProyectosEndogenos";

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
//'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {

//en esta seccion colocar los action de solo lectura o consulta
        return array(
            array('allow',
                'actions' => array(
                    'index',
                    'crear',
                    'gradosNivel',
                    'verDetalle',
                    'verDetalleGrado',
                    'obtenerGradosAsignaturas',
                    'condicionarGrado',
                    'buscarAsignaturas',
                    'actualizarDatosGeneralesPlan',
                    'buscarGradosPlan',
                    'buscarAsignaturasPlan',
                    'buscarAsignaturasDisponibles'
                ),
                'pbac' => array('read'),
            ),
            //en esta seccion colocar todos los action del modulo
            array('allow',
                'actions' => array(
                    'crearPlan',
                    'agregarAsignatura',
                    'cambiarEstatusAsignatura',
                    'modificar',
                    'ordenarAsignatura'
                ),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {

        $model = new ProyectosEndogenos('search');
        $model->unsetAttributes();  // clear any default values
        $data = $this->getRequest('ProyectosEndogenos');
        if (isset($data))
            $model->attributes = $data;

        $this->registerLog('LECTURA', self::MODULO . '.Index', 'EXITOSO', 'Entró a consultar los Proyectos Endogenos');
        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function columnaAcciones($data) {


        $columna = '<div class="action-buttons">';
        $estatus = (isset($data['estatus'])) ? $data['estatus'] : '';
        $nombre = (isset($data['nombre'])) ? $data['nombre'] : '';
        $id = (isset($data['id'])) ? $data['id'] : '';
        if (Yii::app()->user->pbac('read'))
            $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in look-data", "title" => "Consultar Datos del Proyecto Endogeno")) . '&nbsp;&nbsp;';

        if (Yii::app()->user->pbac('write')) {
            if (($estatus == 'A') || ($estatus == '')) {
                $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green edit-data", "title" => "Modificar Datos del Datos del Proyecto Endogeno")) . '&nbsp;&nbsp;';
                $columna .= CHtml::link("", "#", array("class" => "fa icon-trash red change-status", 'data-action' => 'E', 'data-id' => base64_encode($id), 'data-description' => $nombre, "title" => "Inactivar Proyecto Endogeno")) . '&nbsp;&nbsp;';
            } else if ($estatus == 'E') {
                $columna .= CHtml::link("", "", array("class" => 'fa fa-check change-status', 'data-action' => 'A', 'data-id' => base64_encode($id), 'data-description' => $nombre, 'title' => 'Activar Proyecto Endogeno'));
            }
        }
        $columna .= '</div>';


        return $columna;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ProyectosEndogenos the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = ProyectosEndogenos::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ProyectosEndogenos $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'proyectos-endogenos-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
