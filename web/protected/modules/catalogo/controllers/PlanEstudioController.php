<?php

class PlanEstudioController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Consultar Planes de Estudio',
        'write' => 'Crear Planes de Estudio',
        'label' => 'Planes de Estudio'
    );

    const MODULO = "Catalogo.PlanEstudio";

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
//'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {

//en esta seccion colocar los action de solo lectura o consulta
        return array(
            array('allow',
                'actions' => array(
                    'index',
                    'crear',
                    'gradosNivel',
                    'verDetalle',
                    'verDetalleGrado',
                    'obtenerGradosAsignaturas',
                    'condicionarGrado',
                    'buscarAsignaturas',
                    'actualizarDatosGeneralesPlan',
                    'buscarGradosPlan',
                    'buscarAsignaturasPlan',
                    'buscarAsignaturasDisponibles'
                ),
                'pbac' => array('read'),
            ),
            //en esta seccion colocar todos los action del modulo
            array('allow',
                'actions' => array(
                    'crearPlan',
                    'agregarAsignatura',
                    'cambiarEstatusAsignatura',
                    'actualizarAsignatura',
                    'modificar',
                    'ordenarAsignatura'
                ),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        $model = new Plan('search');
// $dropDownModalidad = Modalidad::model()->getModalidades();
        $dropDownCredencial = Credencial::model()->getCredenciales();
        $dropDownMencion = Mencion::model()->getMenciones();
        $dropDownFundJuridico = FundamentoJuridico::model()->getFundamentosJuridicos();

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Plan']))
            $model->attributes = $_GET['Plan'];

        $this->registerLog('LECTURA', self::MODULO . '.Index', 'EXITOSO', 'Entró a consultar los Planes de Estudios ');

        $this->render('index', array(
// 'dropDownModalidad' => $dropDownModalidad,
            'dropDownCredencial' => $dropDownCredencial,
            'dropDownMencion' => $dropDownMencion,
            'dropDownFundJuridico' => $dropDownFundJuridico,
            'model' => $model
                )
        );
    }

    public function actionCrear() {
        $url = $_SERVER['REQUEST_URI'];
        $validaUrl = '/catalogo/planEstudio/crear';
        if ($url != $validaUrl) {
            $this->redirect('../crear');
        }
        $model = new Plan('crear');
// $dropDownModalidad = Modalidad::model()->getModalidades();
        $dropDownCredencial = Credencial::model()->getCredenciales();
        $dropDownMencion = Mencion::model()->getMenciones();
        $dropDownFundJuridico = FundamentoJuridico::model()->getFundamentosJuridicos();
        $dropDownNivel = Nivel::model()->getNiveles();
        $dropDownTipoDocumento = TipoDocumento::model()->getTipoDocumentos();


        $this->render('crear', array(
// 'dropDownModalidad' => $dropDownModalidad,
            'dropDownCredencial' => $dropDownCredencial,
            'dropDownMencion' => $dropDownMencion,
            'dropDownFundJuridico' => $dropDownFundJuridico,
            'dropDownNivel' => $dropDownNivel,
            'dropDownTipoDocumento' => $dropDownTipoDocumento,
            'model' => $model
                )
        );
    }

    public function actionCrearPlan() {
        if (Yii::app()->request->isAjaxRequest) {
            $model = new Plan('crear');
            $model->attributes = $_REQUEST['Plan'];
            if ($model->validate()) {
                if ($model->save()) {
                    $this->registerLog('ESCRITURA', self::MODULO . '.CrearPlan', 'EXITOSO', 'Creó el plan de estudios :' . $model->id);
                    $mensaje = "Creación Exitosa";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensajeSuccess', 'mensaje' => $mensaje, 'id' => base64_encode($model->id), 'nivel_id' => base64_encode($model->nivel_id), 'cod_plan' => base64_encode($model->cod_plan)));
                    Yii::app()->end();
                } else {

                    $this->registerLog('ESCRITURA', self::MODULO . '.CrearPlan', 'FALLIDO', 'No se completo el proceso. Intento crear el plan de estudio con cod_plan :' . $model->cod_plan);

                    $mensaje = "Creación Fallida, intente nuevamente";

                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensajeError', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            } else {
                $this->registerLog('ESCRITURA', self::MODULO . '.CrearPlan', 'FALLIDO', 'No se completo el proceso, campos vacios. Intento crear el plan de estudio con cod_plan :' . $model->cod_plan);

                $this->renderPartial('//errorSumMsg', array('model' => $model));
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'Usted no se encuentra autorizado para realizar esta acción mediante esta vía.');
        }
    }

    public function actionObtenerGradosAsignaturas() {
        if (Yii::app()->request->isAjaxRequest) {
            $plan_id = $_REQUEST ['id'];
            $nivel_id = $_REQUEST ['nivel_id'];
            $codigo_plan = $_REQUEST['codigo_plan'];
            $nivel_id_decoded = base64_decode($nivel_id);
            $plan_id_decoded = base64_decode($plan_id);
            $codigo_plan_decoded = base64_decode($codigo_plan);

            if (is_numeric($nivel_id_decoded) && is_numeric($plan_id_decoded) && is_numeric($codigo_plan_decoded)) {
                $grados = NivelGrado:: model()->getGradosNivel($nivel_id_decoded);
                if ($grados !== array()) {
                    $dataProvider = $this->dataProviderGradosNivel($grados, 'U', $nivel_id_decoded, $plan_id_decoded);
                } else {
                    throw new CHttpException(999, 'El nivel que esta seleccionando no posee grados asociados.');
                }
                Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                $this->registerLog('ESCRITURA', self::MODULO . '.GradosNivel', 'EXITOSO', 'Seleccionó el nivel ' . $nivel_id_decoded . ' y se cargaron los grados correspondientes');
                $this->renderPartial('_gradosAsignaturas', array('dataProvider' => $dataProvider, 'codigo_plan' => $codigo_plan), false, true);
                Yii::app()->end();
            } else {
                $this->registerLog('ILEGAL', self::MODULO . '.GradosNivel', 'ERROR', 'Seleccionó el nivel ' . $nivel_id_decoded . ', no es numerico o esta vacio');
                throw new CHttpException(404, 'No se ha encontrado el Recurso que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
            }
        } else {
            $this->registerLog('ILEGAL', self::MODULO . '.GradosNivel', 'ERROR', 'No esta autorizado autorizado para realizar esta acción de manera directa, sin una petición por ajax');

            throw new CHttpException(403, 'Usted no se encuentra autorizado para realizar esta acción mediante esta vía.');
        }
    }

    public function actionVerDetalle() {
        if (array_key_exists('id', $_REQUEST)) {
            $id = $_REQUEST['id'];
            $id_decoded = base64_decode($id);
            $renderPartial = false;
            if (array_key_exists('renderPartial', $_REQUEST))
                $renderPartial = $_REQUEST['renderPartial'];
            if (is_numeric($id_decoded)) {

                $grados = $this->buscarGrados($id_decoded);
                if ($grados == null)
                    $dataProviderGrados = array();
                else
                    $dataProviderGrados = $this->dataProviderGrados($grados);
                $data = Plan::model()->getDatosPlan($id_decoded);

                if ($renderPartial) {
                    Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                    $this->renderPartial('verDetalle', array('dataProvider' => $dataProviderGrados, 'data' => $data, 'renderPartial' => $renderPartial), false, true);
                    Yii::app()->end();
                } else
                    $this->render('verDetalle', array('dataProvider' => $dataProviderGrados, 'data' => $data));
                Yii::app()->end();
            } else
                throw new CHttpException(404, 'No se ha encontrado el Plan de Estudio que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        } else
            throw new CHttpException(404, 'No se ha especificado el Plan de Estudio que desea consultar. Vuelva a la página anterior e intentelo de nuevo.');
    }

    public function actionVerDetalleGrado() {
        if (array_key_exists('id', $_REQUEST) && array_key_exists('plan_id', $_REQUEST)) {
            $id = $_REQUEST['id'];
            $id_decoded = base64_decode($id);

            $plan_id = $_REQUEST['plan_id'];
            $plan_id_decoded = base64_decode($plan_id);
            if (is_numeric($id_decoded) && is_numeric($plan_id_decoded)) {
                $asignaturas = $this->buscarGradosAsignaturas($id_decoded, $plan_id_decoded);
                $data = Grado::model()->getDatosGrado($id_decoded);
                if (!$data) {
                    throw new CHttpException(404, 'No se ha encontrado el recurso. Vuelva a la página anterior e intentelo de nuevo.');
                }
                $dataProviderAsignaturas = $this->dataProviderAsignaturas($asignaturas);
                Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                $this->renderPartial('verDetalleGrado', array('dataProvider' => $dataProviderAsignaturas, 'data' => $data), false, true);
                Yii::app()->end();
            } else
                throw new CHttpException(404, 'No se ha encontrado el Grado que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        } else
            throw new CHttpException(404, 'No se ha especificado el Grado que desea consultar. Vuelva a la página anterior e intentelo de nuevo.');
    }

    public function actionGradosNivel() {
        if (Yii::app()->request->isAjaxRequest) {
            $item = $_REQUEST ['nivel_id'];
            if (is_numeric($item)) {
                $grados = NivelGrado:: model()->getGradosNivel($item);
                if ($grados !== array()) {
                    $dataProvider = $this->dataProviderGradosNivel($grados);
                } else {
                    throw new CHttpException(999, 'El nivel que esta seleccionando no posee grados asociados.');
                }

                Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                $this->registerLog('ESCRITURA', self::MODULO . '.GradosNivel', 'EXITOSO', 'Seleccionó el nivel ' . $item . ' y se cargaron los grados correspondientes');

                $this->renderPartial('_gridGrados', array('dataProvider' => $dataProvider), false, true);
                Yii::app()->end();
            } else {
                $this->registerLog('ILEGAL', self::MODULO . '.GradosNivel', 'ERROR', 'Seleccionó el nivel ' . $item . ', no es numerico o esta vacio');

                throw new CHttpException(404, 'No se ha encontrado el Recurso que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
            }
        } else {
            $this->registerLog('ILEGAL', self::MODULO . '.GradosNivel', 'ERROR', 'No esta autorizado autorizado para realizar esta acción de manera directa, sin una petición por ajax');

            throw new CHttpException(403, 'Usted no se encuentra autorizado para realizar esta acción mediante esta vía.');
        }
    }

    public function actionCondicionarGrado() {
        if (Yii::app()->request->isAjaxRequest) {
            $grado_id = $_REQUEST ['grado_id']; // encoded
            $nivel_id = $_REQUEST ['nivel_id']; // encoded
            $plan_id = $_REQUEST ['plan_id'];  // encoded
            $grado_id_decoded = base64_decode($grado_id);
            $nivel_id_decoded = base64_decode($nivel_id);
            $plan_id_decoded = base64_decode($plan_id);

            if (is_numeric($grado_id_decoded) && is_numeric($nivel_id_decoded)) {
                $asignaturas_usadas = PlanesGradosAsignaturas::model()->getAsignaturasGradosPlanSinEstatus($grado_id_decoded, $plan_id_decoded);

                $dropDownAsignatura = $dropDownAsignaturaPre = Asignatura::model()->getAsignaturas($asignaturas_usadas);
                $dropDownGrado = NivelGrado::model()->getGrados($grado_id_decoded, $nivel_id_decoded);
                $grado = Grado::model()->getDatosGrado($grado_id_decoded);
                /* echo json_encode(array(
                  'dropDownAsignatura' => $dropDownAsignatura,
                  'grado' => $dropDownGrado
                  ));
                 */
                Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                $this->renderPartial('_formAgregarAsignaturas', array(
                    'dropDownAsignatura' => $dropDownAsignatura,
                    'dropDownAsignaturaPre' => $dropDownAsignaturaPre,
                    'dropDownGrado' => $dropDownGrado,
                    'plan_id' => $plan_id,
                    'nivel_id' => $nivel_id,
                    'grado_id' => $grado_id,
                    'grado' => $grado
                        ), false, true);
                Yii::app()->end();
            } else {
                throw new CHttpException(404, 'No se ha encontrado el Recurso que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
            }
        } else {
            throw new CHttpException(403, 'Usted no se encuentra autorizado para realizar esta acción mediante esta vía.');
        }
    }

    public function actionAgregarAsignatura() {
        if (Yii::app()->request->isAjaxRequest) {
            $asignatura_id = $this->getRequest('asignatura_id');
            $asignatura_pre_id = $this->getRequest('asignatura_pre_id');
            $avaladoMppe = $this->getRequest('avalado');
            $cod_plan = $this->getRequest('cod_plan');
            $grado_id = $this->getRequest('grado_id'); // encoded
            $hora_teorica = $this->getRequest('hora_teorica'); // encoded
            $hora_practica = $this->getRequest('hora_practica'); // encoded
            $grado_pre_id = $this->getRequest('grado_pre_id'); // encoded
            $nivel_id = $this->getRequest('nivel_id'); // encoded
            $plan_id = $this->getRequest('plan_id');  // encoded
            $prelacion = $this->getRequest('prelacion');

            $asignatura_id_decoded = base64_decode($asignatura_id);
            $asignatura_pre_id_decoded = base64_decode($asignatura_pre_id);
            $avaladoMppe_decoded = base64_decode($avaladoMppe);
            $cod_plan_decoded = base64_decode($cod_plan);
            $grado_id_decoded = base64_decode($grado_id);
            $grado_pre_id_decoded = base64_decode($grado_pre_id);
            $nivel_id_decoded = base64_decode($nivel_id);
            $plan_id_decoded = base64_decode($plan_id);
            $prelacion_decoded = base64_decode($prelacion);

            $poseePrelacion = false;
            if (is_numeric($plan_id_decoded) && is_numeric($nivel_id_decoded) && is_numeric($asignatura_id_decoded)) {
                if ($prelacion) {
                    if (is_numeric($cod_plan_decoded) && is_numeric($asignatura_pre_id_decoded) && is_numeric($grado_pre_id_decoded)) {
                        $poseePrelacion = true;
                    } else {
                        $mensaje = "Estimado Usuario, los datos suministrados no son correctos. Cierre la ventana e intente de nuevo.";
                        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    }
                }
                $pagModel = PlanesGradosAsignaturas::model()->findByAttributes(array('grado_id' => $grado_id_decoded
                    , 'plan_id' => $plan_id_decoded, 'asignatura_id' => $asignatura_id_decoded));

                if ($pagModel !== null) {
                    $model = $pagModel;
                    $model->scenario = 'agregarAsignatura';
                    $model->usuario_ini_id = Yii::app()->user->id;
                    $model->fecha_act = date('Y-m-d H:i:s');
                    $model->fecha_elim = null;
                    $model->estatus = 'A';
                } else {
                    $model = new PlanesGradosAsignaturas('agregarAsignatura');
                    $model->plan_id = $plan_id_decoded;
                    $model->asignatura_id = $asignatura_id_decoded;
                    $model->grado_id = $grado_id_decoded;
                    $model->usuario_ini_id = Yii::app()->user->id;
                    $model->fecha_ini = date('Y-m-d H:i:s');
                    $model->estatus = 'A';
                }
                $model->hora_practica = $hora_practica;
                $model->hora_teorica = $hora_teorica;

                $transaction = Yii::app()->db->beginTransaction();
                try {
                    if ($model->validate())
                        if ($model->save()) {
                            if ($poseePrelacion) {
                                $pgrado_prelacion = PlanesGradosAsignaturas::model()->with(array(
                                            'plan' => array(
                                                'joinType' => 'INNER JOIN',
                                                'condition' => "plan.cod_plan=$cod_plan_decoded",
                                            ),
                                        ))->findAll(array(
                                    'condition' => "asignatura_id = $asignatura_pre_id_decoded AND grado_id = $grado_pre_id_decoded AND t.estatus='A'",
                                    'limit' => '1'
                                ));
                                if (isset($pgrado_prelacion) && isset($pgrado_prelacion[0]['id']) && is_numeric($pgrado_prelacion[0]['id'])) {
                                    $modelPredecesor = new AsignaturaPredecesor();
                                    $modelPredecesor->pgrado_asignatura_id = $model->id;
                                    $modelPredecesor->prelacion_id = $pgrado_prelacion[0]['id'];
                                    $modelPredecesor->usuario_ini_id = Yii::app()->user->id;
                                    $modelPredecesor->fecha_ini = date('Y-m-d H:i:s');
                                    $modelPredecesor->estatus = 'A';
                                    if ($modelPredecesor->validate()) {
                                        if ($modelPredecesor->save()) {

                                            //$this->registerLog('ESCRITURA', self::MODULO . 'AgregarAsignatura', 'EXITOSO', 'Agregó la asignatura_id:' . $asignatura_id . ' al plan_id:' . $plan_id . ' en el grado_id:' . $grado_id_decoded);
                                        } else {
                                            $mensaje = 'Estimado Usuario, ocurrio un error mientras se realizaba el proceso. Cierre la ventana e intente nuevamente.';
                                            Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                                            Yii::app()->end();
                                        }
                                    } else {
                                        $this->renderPartial('//msgBox', array('class' => 'errorDialogBox', 'message' => CHtml::errorSummary($modelPredecesor),), false, true);
                                    }
                                } else {
                                    $mensaje = "Estimado Usuario, los datos suministrados no son correctos. Cierre la ventana e intente de nuevo.";
                                    Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                                    Yii::app()->end();
                                }
                            }

                            $asignaturas_ids = PlanesGradosAsignaturas::model()->getAsignaturasGradosPlan($model->grado_id, $model->plan_id);
                            $transaction->commit();
                            $this->registerLog('ESCRITURA', self::MODULO . 'AgregarAsignatura', 'EXITOSO', 'Agregó la asignatura_id:' . $asignatura_id . ' al plan_id:' . $plan_id . ' en el grado_id:' . $grado_id_decoded);
                            $mensaje = "Se agregó exitosamente la materia ";
                            Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                            echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje, 'asignaturas' => json_encode($asignaturas_ids), 'grado' => base64_encode($model->grado_id), 'plan' => base64_encode($model->plan_id)));
                            Yii::app()->end();
                        } else {
                            $mensaje = "Ocurrio un error durante el proceso, cierre la ventana e intente nuevamente.";
                            Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                            Yii::app()->end();
                        } else {
                        $this->registerLog('ESCRITURA', self::MODULO . 'AgregarAsignatura', 'NO EXITOSO', 'El usuario con el id=' . $model->usuario_ini_id . ' ha intentado agregar una asignatura al plan ' . $model->plan_id . ' en el grado' . $model->grado_id . '.  Datos no válidos.');
                        $this->renderPartial('//msgBox', array('class' => 'errorDialogBox', 'message' => CHtml::errorSummary($model),), false, true);
                    }
                } catch (Exception $ex) {
                    $transaction->rollback();
                    var_dump($ex);
                    //$this->registerLog('ESCRITURA', self::MODULO . 'AgregarAsignatura', 'ERROR', 'Ha intentado matricular la Seccion Plantel ' . $seccion_plantel_id);
                }
            } else {
                $this->registerLog('ILEGAL', self::MODULO . '.AgregarAsignatura', 'ERROR', 'El recurso solicitado no posee el formato correcto. plan_id :' . $plan_id_decoded . ' ,nivel_id:' . $nivel_id_decoded . ' ,grado_id:' . $grado_id_decoded);
                //throw new CHttpException(404, 'No se ha encontrado el Recurso que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
                $mensaje = "Estimado Usuario, los datos suministrados no son correctos. Cierre la ventana e intente de nuevo.";
                Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                Yii::app()->end();
            }
        } else {
            $this->registerLog('ILEGAL', self::MODULO . '.AgregarAsignatura', 'ERROR', 'No esta autorizado autorizado para realizar esta acción de manera directa, sin una petición por ajax');
            throw new CHttpException(403, 'Usted no se encuentra autorizado para realizar esta acción mediante esta vía.');
        }
    }

    public function actionModificar() {
        $plan_id = $this->getRequest('id');  // encoded
        $plan_id_decoded = base64_decode($plan_id);

        if (is_numeric($plan_id_decoded)) {
            $grados = $this->buscarGrados($plan_id_decoded);
            if ($grados == null)
                $dataProviderGrados = array();
            else
                $dataProviderGrados = $this->dataProviderGrados($grados);
            $data = Plan::model()->getDatosPlan($plan_id_decoded);

            $model = $this->loadModel($plan_id_decoded);
            $model->scenario = 'modificar';
// $dropDownModalidad = Modalidad::model()->getModalidades();
            $dropDownCredencial = Credencial::model()->getCredenciales();
            $dropDownMencion = Mencion::model()->getMenciones();
            $dropDownFundJuridico = FundamentoJuridico::model()->getFundamentosJuridicos();
            $dropDownNivel = NivelPlan::model()->obtenerNivelesPlan($plan_id_decoded);
            $dropDownTipoDocumento = TipoDocumento::model()->getTipoDocumentos();

            $this->render('modificar', array(
                // 'dropDownModalidad' => $dropDownModalidad,
                'dropDownCredencial' => $dropDownCredencial,
                'dropDownMencion' => $dropDownMencion,
                'dropDownFundJuridico' => $dropDownFundJuridico,
                'dropDownNivel' => $dropDownNivel,
                'dropDownTipoDocumento' => $dropDownTipoDocumento,
                'model' => $model,
                'plan_id' => $plan_id
                    )
            );
            //$this->render('verDetalle', array('dataProvider' => $dataProviderGrados, 'data' => $data));
            Yii::app()->end();
        } else {
            $this->registerLog('ILEGAL', self::MODULO . '.Modificar', 'ERROR', 'El recurso solicitado no posee el formato correcto. plan_id :' . $plan_id_decoded);
            throw new CHttpException(404, 'No se ha encontrado el Recurso que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        }
    }

    public function actionActualizarDatosGeneralesPlan() {

        if (Yii::app()->request->isAjaxRequest) {
            $plan_id = $this->getRequest('plan_id');
            $model_plan = $this->getRequest('Plan');
            $plan_id_decoded = base64_decode($plan_id);
            $usuario_id = Yii::app()->user->id;
            if (is_numeric($plan_id_decoded) && is_array($model_plan)) {
                $model = $this->loadModel($plan_id_decoded);
                $model->attributes = $model_plan;
                $model->scenario = 'modificar';
                $model->fecha_act = date('Y-m-d H:i:s');
                $model->usuario_act_id = $usuario_id;
                if ($model->validate()) {
                    if ($model->save()) {
                        $this->registerLog('ESCRITURA', self::MODULO . '.ActualizarDatosGeneralesPlan', 'EXITOSO', 'Actualizó el plan de estudios :' . $model->id);
                        $mensaje = "Actualización Exitosa";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'mensajeSuccess', 'mensaje' => $mensaje, 'id' => base64_encode($model->id), 'nivel_id' => base64_encode($model->nivel_id)));
                        Yii::app()->end();
                    } else {

                        $this->registerLog('ESCRITURA', self::MODULO . '.ActualizarDatosGeneralesPlan', 'FALLIDO', 'No se completo el proceso. Intento actualizar el plan de estudio con cod_plan :' . $model->cod_plan);

                        $mensaje = "Actualización Fallida, intente nuevamente";

                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'mensajeError', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    }
                } else {
                    $this->registerLog('ESCRITURA', self::MODULO . '.ActualizarDatosGeneralesPlan', 'FALLIDO', 'No se completo el proceso, campos vacios. Intento actualizar el plan de estudio con cod_plan :' . $model->cod_plan);

                    $this->renderPartial('//errorSumMsg', array('model' => $model));
                    Yii::app()->end();
                }
            } else {
                $this->registerLog('ESCRITURA', self::MODULO . '.ActualizarDatosGeneralesPlan', 'FALLIDO', 'No se suministraron los datos necesarios para realizar esta accion ');
                throw new CHttpException(404, 'No se ha suministrado los recursos necesarios para realizar esta acción. Vuelva a la página anterior e intentelo de nuevo.');
            }
        } else {
            throw new CHttpException(403, 'Usted no se encuentra autorizado para realizar esta acción mediante esta vía.');
        }
    }

    public function actionCambiarEstatusAsignatura() {

        $accion = $this->getPost('accion');
        $plan_id = $this->getPost('plan_id');
        $grado_id = $this->getPost('grado_id');
        $asignatura_id = $this->getPost('asignatura_id');

        if (in_array($accion, array('I', 'A'))) {

            $plan_id_decoded = base64_decode($plan_id);
            $grado_id_decoded = base64_decode($grado_id);
            $asignatura_id_decoded = base64_decode($asignatura_id);

            $planGradosAsignatura = new PlanesGradosAsignaturas();

            if (is_numeric($plan_id_decoded) && is_numeric($grado_id_decoded) && is_numeric($asignatura_id_decoded)) {

                $model = PlanesGradosAsignaturas::model()->findByAttributes(array('grado_id' => $grado_id_decoded
                    , 'plan_id' => $plan_id_decoded, 'asignatura_id' => $asignatura_id_decoded));

                if ($model) {

                    $result = $planGradosAsignatura->cambiarEstatusAsignatura($plan_id_decoded, $grado_id_decoded, $asignatura_id_decoded, $accion);

                    if ($result->isSuccess) {

                        if ($accion == 'I') {
                            $this->registerLog('ELIMINACION', self::MODULO . 'cambiarEstatusAsignatura', 'EXITOSO', 'Se ha Inactivado la asignatura ' . $model->asignatura_id . ' en el plan' . $model->plan_id . ' en el grado ' . $model->grado_id);
                        } else {
                            $this->registerLog('ESCRITURA', self::MODULO . 'cambiarEstatusAsignatura', 'EXITOSO', 'Se ha Activado la asignatura ' . $model->asignatura_id . ' en el plan' . $model->plan_id . ' en el grado ' . $model->grado_id);
                        }
                        $class = 'successDialogBox';

                        $mensaje = $result->message;
                        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                        echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje, 'plan' => $plan_id, 'grado' => $grado_id));
                        Yii::app()->end();
                    } else {
                        $mensaje = $result->message;
                        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    }
                } else {
                    throw new CHttpException(404, 'No se ha encontrado la Asignatura que ha solicitado. Cierre la ventana e intentelo de nuevo.');
                }
            } else {
                throw new CHttpException(404, 'No se ha encontrado la Asignatura que ha solicitado. Cierre la ventana e intentelo de nuevo.');
            }
        } else {

            $class = 'errorDialogBox';
            $message = 'No se ha especificado la acción a tomar sobre la asignatura, cierre la ventana e intentelo de nuevo.';

            $this->renderPartial('//msgBox', array(
                'class' => $class,
                'message' => $message,
            ));
        }
    }
    
    public function actionActualizarAsignatura() {

        $accion = $this->getPost('accion');
        $plan_id = $this->getPost('plan_id');
        $grado_id = $this->getPost('grado_id');
        $asignatura_id = $this->getPost('asignatura_id');
        $hora_practica = $this->getPost('hora_practica');
        $hora_teorica = $this->getPost('hora_teorica');
        $id = $this->getPost('id');

        if (in_array($accion, array('I', 'A'))) {

            $plan_id_decoded = base64_decode($plan_id);
            $grado_id_decoded = base64_decode($grado_id);
            $asignatura_id_decoded = base64_decode($asignatura_id);
            $id_decoded = base64_decode($id);

            $planGradosAsignatura = new PlanesGradosAsignaturas();

            if (is_numeric($plan_id_decoded) && is_numeric($grado_id_decoded) && is_numeric($asignatura_id_decoded)) {

                $model = PlanesGradosAsignaturas::model()->findByAttributes(array('grado_id' => $grado_id_decoded
                    , 'plan_id' => $plan_id_decoded, 'asignatura_id' => $asignatura_id_decoded));

                if ($model) {

                    $result = $planGradosAsignatura->actualizarEstatusAsignatura($hora_teorica, $hora_practica, $plan_id_decoded, $grado_id_decoded, $asignatura_id_decoded, $accion);
//                    var_dump($result);die();
                    if ($result->isSuccess) {

                        if ($accion == 'I') {
                            $this->registerLog('ELIMINACION', self::MODULO . 'cambiarEstatusAsignatura', 'EXITOSO', 'Se ha Inactivado la asignatura ' . $model->asignatura_id . ' en el plan' . $model->plan_id . ' en el grado ' . $model->grado_id);
                        } else {
                            $this->registerLog('ESCRITURA', self::MODULO . 'cambiarEstatusAsignatura', 'EXITOSO', 'Se ha Activado la asignatura ' . $model->asignatura_id . ' en el plan' . $model->plan_id . ' en el grado ' . $model->grado_id);
                        }
                        $class = 'successDialogBox';

                        $mensaje = $result->message;
                        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                        echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje, 'plan' => $plan_id, 'grado' => $grado_id));
                        Yii::app()->end();
                    } else {
                        $mensaje = $result->message;
                        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    }
                } else {
                    throw new CHttpException(404, 'No se ha encontrado la Asignatura que ha solicitado. Cierre la ventana e intentelo de nuevo.');
                }
            } else {
                throw new CHttpException(404, 'No se ha encontrado la Asignatura que ha solicitado. Cierre la ventana e intentelo de nuevo.');
            }
        } else {

            $class = 'errorDialogBox';
            $message = 'No se ha especificado la acción a tomar sobre la asignatura, cierre la ventana e intentelo de nuevo.';

            $this->renderPartial('//msgBox', array(
                'class' => $class,
                'message' => $message,
            ));
        }
    }

    public function actionOrdenarAsignatura() {

        $accion = $this->getPost('accion');
        $plan_id = $this->getPost('plan_id');
        $grado_id = $this->getPost('grado_id');
        $asignatura_id = $this->getPost('asignatura_id');

        if (in_array($accion, array('D', 'A'))) {

            $plan_id_decoded = base64_decode($plan_id);
            $grado_id_decoded = base64_decode($grado_id);
            $asignatura_id_decoded = base64_decode($asignatura_id);

            $planGradosAsignatura = new PlanesGradosAsignaturas();

            if (is_numeric($plan_id_decoded) && is_numeric($grado_id_decoded) && is_numeric($asignatura_id_decoded)) {

                $model = PlanesGradosAsignaturas::model()->findByAttributes(array('grado_id' => $grado_id_decoded
                    , 'plan_id' => $plan_id_decoded, 'asignatura_id' => $asignatura_id_decoded));

                if ($model) {

                    $result = $planGradosAsignatura->ordenarAsignatura($plan_id_decoded, $grado_id_decoded, $asignatura_id_decoded, $accion);

                    if ($result->isSuccess) {

                        if ($accion == 'D') {
                            $this->registerLog('ESCRITURA', self::MODULO . 'ordenarAsignatura', 'EXITOSO', 'Se ha bajado de nivel la asignatura ' . $model->asignatura_id . ' en el plan' . $model->plan_id . ' en el grado ' . $model->grado_id);
                        } else {
                            $this->registerLog('ESCRITURA', self::MODULO . 'ordenarAsignatura', 'EXITOSO', 'Se ha subido de nivel la  asignatura ' . $model->asignatura_id . ' en el plan' . $model->plan_id . ' en el grado ' . $model->grado_id);
                        }
                        $class = 'successDialogBox';

                        $mensaje = $result->message;
                        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                        echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje, 'plan' => $plan_id, 'grado' => $grado_id));
                        Yii::app()->end();
                    } else {
                        $class = 'errorDialogBox';
                        $mensaje = $result->message;
                        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    }
                } else {
                    throw new CHttpException(404, 'Recurso solicitado no encontrado. Cierre la ventana e intentelo de nuevo.');
                }
            } else {
                throw new CHttpException(404, 'Recurso solicitado no encontrado. Cierre la ventana e intentelo de nuevo.');
            }
        } else {

            $class = 'errorDialogBox';
            $message = 'No se ha especificado la acción a tomar sobre la asignatura, cierre la ventana e intentelo de nuevo.';

            $this->renderPartial('//msgBox', array(
                'class' => $class,
                'message' => $message,
            ));
        }
    }

    public function actionBuscarAsignaturas() {
        if (Yii::app()->request->isAjaxRequest) {
            $grado_id = $this->getRequest('grado_id'); // encoded
            $plan_id = $this->getRequest('plan_id');  // encoded
            $grado_id_decoded = base64_decode($grado_id);
            $plan_id_decoded = base64_decode($plan_id);

            if (is_numeric($grado_id_decoded) && is_numeric($plan_id_decoded)) {
                $asignaturas = PlanesGradosAsignaturas::model()->getAsignaturasGrados($grado_id_decoded, $plan_id_decoded);

                if ($asignaturas !== null) {
                    $asignaturas = $this->buscarGradosAsignaturas($grado_id_decoded, $plan_id_decoded);
                    $resultProvider = $this->dataProviderAsignaturas($asignaturas, true);
                    $dataProviderAsignaturas = $resultProvider[0];
                    $asignaturas_ids = $resultProvider[1];
                    Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                    $this->renderPartial('_gridAsignaturas', array('dataProvider' => $dataProviderAsignaturas, 'asignaturas' => json_encode($asignaturas_ids)), false, true);
                    Yii::app()->end();
                } else {
                    $this->registerLog('ILEGAL', self::MODULO . '.BuscarAsignaturas', 'ERROR', 'Ocurrio un error obteniendo las asignaturas y los grados del  plan_id :' . $plan_id_decoded . ' ,grado_id:' . $grado_id_decoded);
                    Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                    $this->renderPartial('_gridAsignaturas', array('dataProvider' => array(), 'asignaturas' => json_encode(array())), false, true);
                    Yii::app()->end();
                }
            } else {
                $this->registerLog('ILEGAL', self::MODULO . '.BuscarAsignaturas', 'ERROR', 'El recurso solicitado no posee el formato correcto. plan_id :' . $plan_id_decoded . ' ,grado_id:' . $grado_id_decoded);
                throw new CHttpException(404, 'No se ha encontrado el Recurso que ha solicitado. Cierre la ventana e intentelo de nuevo.');
            }
        } else {
            $this->registerLog('ILEGAL', self::MODULO . '.BuscarAsignaturas', 'ERROR', 'No esta autorizado autorizado para realizar esta acción de manera directa, sin una petición por ajax');
            throw new CHttpException(403, 'Usted no se encuentra autorizado para realizar esta acción mediante esta vía.');
        }
    }

    public function actionBuscarGradosPlan() {
        if (Yii::app()->request->isAjaxRequest) {
            $cod_plan = $this->getRequest('cod_plan'); // encoded
            $cod_plan_decoded = base64_decode($cod_plan);

            if (is_numeric($cod_plan_decoded)) {
                $grados = PlanesGradosAsignaturas::model()->getGradosPlanCodPlan($cod_plan_decoded);
                if ($grados !== null) {
                    $lista = CHtml::listData($grados, 'grado_id', 'nombre');
                    echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

                    foreach ($lista as $valor => $descripcion) {
                        echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
                    }
                } else {
                    $mensaje = "Estimado usuario, el Código de Plan que ha suministrado no posee grados asociados o no existe. ";
                    Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            } else {
                $this->registerLog('ILEGAL', self::MODULO . '.BuscarGradosPlan', 'ERROR', 'El recurso solicitado no posee el formato correcto. cod_plan :' . $cod_plan_decoded);
                $mensaje = "Estimado usuario, el Código de Plan que ha suministrado no posee el formato correcto. ";
                Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                Yii::app()->end();
            }
        } else {
            $this->registerLog('ILEGAL', self::MODULO . '.BuscarGradosPlan', 'ERROR', 'No esta autorizado autorizado para realizar esta acción de manera directa, sin una petición por ajax');
            throw new CHttpException(403, 'Usted no se encuentra autorizado para realizar esta acción mediante esta vía.');
        }
    }

    public function actionBuscarAsignaturasPlan() {
        if (Yii::app()->request->isAjaxRequest) {
            $grado_pre_id = $this->getRequest('grado_pre'); // encoded
            $cod_plan = $this->getRequest('cod_plan'); // encoded
            $grado_pre_id_decoded = base64_decode($grado_pre_id);
            $cod_plan_decoded = base64_decode($cod_plan);

            if (is_numeric($grado_pre_id_decoded) && is_numeric($cod_plan_decoded)) {
                $grados = PlanesGradosAsignaturas::model()->getAsignaturasGradosCodPlan($grado_pre_id_decoded, $cod_plan_decoded);
                if ($grados !== null) {
                    $lista = CHtml::listData($grados, 'asignatura_id', 'nombre');
                    echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

                    foreach ($lista as $valor => $descripcion) {
                        echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
                    }
                } else {
                    $mensaje = "Estimado usuario, el Grado Prelación que ha suministrado no posee asignaturas asociadas. ";
                    Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            } else {
                $this->registerLog('ILEGAL', self::MODULO . '.BuscarAsignaturasPlan', 'ERROR', 'El recurso solicitado no posee el formato correcto. grado prelación :' . $grado_pre_id_decoded);
                $mensaje = "Estimado usuario, el Grado Prelación que ha suministrado no posee el formato correcto. ";
                Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                Yii::app()->end();
            }
        } else {
            $this->registerLog('ILEGAL', self::MODULO . '.BuscarAsignaturasPlan', 'ERROR', 'No esta autorizado autorizado para realizar esta acción de manera directa, sin una petición por ajax');
            throw new CHttpException(403, 'Usted no se encuentra autorizado para realizar esta acción mediante esta vía.');
        }
    }

    public function actionBuscarAsignaturasDisponibles() {
        if (Yii::app()->request->isAjaxRequest) {
            $asignaturas = $this->getRequest('asignaturas'); // encoded
            $asignaturas_decoded = json_decode($asignaturas, true);

            $grados = Asignatura::model()->getAsignaturas($asignaturas_decoded);
            $lista = CHtml::listData($grados, 'id', 'nombre');
            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $this->registerLog('ILEGAL', self::MODULO . '.BuscarAsignaturasPlan', 'ERROR', 'No esta autorizado autorizado para realizar esta acción de manera directa, sin una petición por ajax');
            throw new CHttpException(403, 'Usted no se encuentra autorizado para realizar esta acción mediante esta vía.');
        }
    }

    public function loadModel($id) {
        $model = Plan::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'No se ha encontrado el Grado que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Plan $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'plan_estudio-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function buscarGrados($id) {
        $grados = Plan::model()->getGradosPlan($id);
        //if ($grados === null)
        //throw new CHttpException(404, 'No se ha encontrado el Plan de Estudio que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        return $grados;
    }

    public function buscarGradosAsignaturas($grado_id, $plan_id) {
        $asignaturas = PlanesGradosAsignaturas::model()->getAsignaturasGrados($grado_id, $plan_id);
        if ($asignaturas === null)
            throw new CHttpException(404, 'No se han encontrado las Asignaturas que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        return $asignaturas;
    }

    public function dataProviderGrados($grados) {

        foreach ($grados as $key => $value) {
            $botones = '';
            $cant_asignaturas = $value['cantidad_asignaturas'];
            if ($cant_asignaturas > 0) {
                $botones = "<div class='center'>" . CHtml::link("", "", array("class" => "fa icon-zoom-in blue look-data-plan",
                            'data-id' => base64_encode($value['grado_id']),
                            'data-plan_id' => base64_encode($value['plan_id']),
                            "title" => "Ver Detalle del grado")
                        ) .
                        "</div>";
            }

            $value_nombre = $value['nombre'];
            $nombre = "<div class='center'>" . $value_nombre . "</div>";

            $rawData[] = array(
                'id' => $key,
                'nombre' => $nombre,
                'boton' => $botones
            );
        }
        return new CArrayDataProvider($rawData, array(
            'pagination' => array(
                'pageSize' => 10,
            ),
                )
        );
    }

    public function dataProviderGradosNivel($grados, $tipo = 'C', $nivel_id = null, $plan_id = null) {
        $rawData = array();
        $botones = '';
        foreach ($grados as $key => $value) {
            if ($tipo != 'C') {
                $botones = "<div class='action-buttons center'>"
//                        CHtml::link("", "", array("class" => "icon-zoom-in blue look-data-grado",
//                            'data-id' => base64_encode($value['id']),
//                            "title" => "Ver Detalle del grado")
//                        )
//                        . '&nbsp;&nbsp;'
                        . CHtml::link("", "", array("class" => "fa fa-plus green edit-data-grado",
                            'data-id' => base64_encode($value['id']),
                            'data-nivel' => base64_encode($nivel_id),
                            'data-plan' => base64_encode($plan_id),
                            "title" => "Establecer Asignaturas")
                        )
                        . "</div>";
            }
            $value_nombre = $value['nombre'];
            $nombre = "<div class='center'>" . $value_nombre . "</div>";

            $rawData[] = array(
                'id' => $key,
                'nombre' => $nombre,
                'boton' => $botones
            );
        }
        return new CArrayDataProvider($rawData, array(
            'pagination' => array(
                'pageSize' => 20,
            ),
                )
        );
    }

    public function dataProviderAsignaturas($asignaturas, $edicion = false) {
        $rawData = array();
        $asignaturas_ids = array();
//        var_dump($asignaturas);
        if ($edicion) {
            $columna = '<div class="action-buttons pull-right">';
            $cantidad = count($asignaturas) - 1;
            foreach ($asignaturas as $key => $value) {

                $columna = '';
                $value_nombre = $value['nombre'];
//                $hora_teorica = '<div class="input-group bootstrap-timepicker"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a data-action="incrementHour" href="#"><i class="icon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a data-action="incrementMinute" href="#"><i class="icon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a data-action="incrementSecond" href="#"><i class="icon-chevron-up"></i></a></td></tr><tr><td><input type="text" maxlength="2" class="bootstrap-timepicker-hour" name="hour"></td> <td class="separator">:</td><td><input type="text" maxlength="2" class="bootstrap-timepicker-minute" name="minute"></td> <td class="separator">:</td><td><input type="text" maxlength="2" class="bootstrap-timepicker-second" name="second"></td></tr><tr><td><a data-action="decrementHour" href="#"><i class="icon-chevron-down"></i></a></td><td class="separator"></td><td><a data-action="decrementMinute" href="#"><i class="icon-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a data-action="decrementSecond" href="#"><i class="icon-chevron-down"></i></a></td></tr></tbody></table></div><input type="text" class="form-control" id="timepicker1"><span class="input-group-addon"><i class="icon-time bigger-110"></i></span></div>';
                
                $hora_teorica = '<div id="hora"><input id="hora_teorica' . base64_encode($value['asignatura_id']) . '" value="' . ($value['hora_teorica'] + 0) . '" type="text" class="form-control" /></div>';
                $hora_practica = '<div id="hora"><input id="hora_practica' . base64_encode($value['asignatura_id']) . '" value="' . ($value['hora_practica'] + 0) . '" type="text" class="form-control" /></div>';
                $suma = '<div id="hora">' . ($value['hora_teorica'] + $value['hora_practica']) . '</div>';
//                $hora_practica = '<div class="input-group bootstrap-timepicker"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a data-action="incrementHour" href="#"><i class="icon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a data-action="incrementMinute" href="#"><i class="icon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a data-action="incrementSecond" href="#"><i class="icon-chevron-up"></i></a></td></tr><tr><td><input type="text" maxlength="2" class="bootstrap-timepicker-hour" name="hour"></td> <td class="separator">:</td><td><input type="text" maxlength="2" class="bootstrap-timepicker-minute" name="minute"></td> <td class="separator">:</td><td><input type="text" maxlength="2" class="bootstrap-timepicker-second" name="second"></td></tr><tr><td><a data-action="decrementHour" href="#"><i class="icon-chevron-down"></i></a></td><td class="separator"></td><td><a data-action="decrementMinute" href="#"><i class="icon-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a data-action="decrementSecond" href="#"><i class="icon-chevron-down"></i></a></td></tr></tbody></table></div><input type="text" class="form-control" id="timepicker1"><span class="input-group-addon"><i class="icon-time bigger-110"></i></span></div>';
//                $hora_practica = $value['hora_practica'];
                $asignaturas_ids[] = $value['asignatura_id'];
                $id = $asignaturas[0]['id'];
                $asignatura = base64_encode($value['asignatura_id']);
                $grado = base64_encode($value['grado_id']);
                $plan = base64_encode($value['plan_id']);
                $prelacion = $value['prelacion'];
                $estatus = $value['estatus'];
                $orden = $value['orden'];
                $grado_pre = '';
                $asignatura_pre = '';
                if ($prelacion !== '' && $prelacion !== null) {
                    $prelacion_explode = explode(';', $prelacion);
                    $grado_pre = $prelacion_explode[0];
                    $asignatura_pre = $prelacion_explode[1];
                }
                if ($estatus == 'A') {
                    //$columna .= CHtml::link("", Yii::app()->createUrl("/catalogo/planEstudio/modificarAsignatura/"), array("class" => "fa icon-pencil green edit-data", "data-href" => "/catalogo/planEstudio/modificarAsignatura/", "data-asignatura" => $asignatura, "data-grado" => $grado, "data-plan" => $plan, "title" => "Modificar Asignatura")) . '&nbsp;&nbsp;';
                    if ($key == 0 AND $orden != null) {
                        if ($key != $cantidad)
                            $columna .= CHtml::link("", Yii::app()->createUrl("/catalogo/planEstudio/ordenarAsignatura/"), array("class" => "fa fa-arrow-down green change-order", "data-href" => "/catalogo/planEstudio/ordenarAsignatura/", "data-description" => $value_nombre, "data-asignatura" => $asignatura, "data-grado" => $grado, "data-plan" => $plan, "data-action" => 'D', "title" => "Bajar Asignatura")) . '&nbsp;&nbsp;';
                    } else if ($key == $cantidad AND $orden != null) {
                        $columna .= CHtml::link("", Yii::app()->createUrl("/catalogo/planEstudio/ordenarAsignatura/"), array("class" => "fa fa-arrow-up green change-order", "data-href" => "/catalogo/planEstudio/ordenarAsignatura/", "data-description" => $value_nombre, "data-asignatura" => $asignatura, "data-grado" => $grado, "data-plan" => $plan, "data-action" => 'A', "title" => "Subir Asignatura")) . '&nbsp;&nbsp;';
                    } else {
                        if ($orden != null) {
                            $columna .= CHtml::link("", Yii::app()->createUrl("/catalogo/planEstudio/ordenarAsignatura/"), array("class" => "fa fa-arrow-down green change-order", "data-href" => "/catalogo/planEstudio/ordenarAsignatura/", "data-description" => $value_nombre, "data-asignatura" => $asignatura, "data-grado" => $grado, "data-plan" => $plan, "data-action" => 'D', "title" => "Bajar Asignatura")) . '&nbsp;&nbsp;';
                            $columna .= CHtml::link("", Yii::app()->createUrl("/catalogo/planEstudio/ordenarAsignatura/"), array("class" => "fa fa-arrow-up green change-order", "data-href" => "/catalogo/planEstudio/ordenarAsignatura/", "data-description" => $value_nombre, "data-asignatura" => $asignatura, "data-grado" => $grado, "data-plan" => $plan, "data-action" => 'A', "title" => "Subir Asignatura")) . '&nbsp;&nbsp;';
                        }
                    }
                    $columna .= CHtml::link("", Yii::app()->createUrl("/catalogo/planEstudio/inactivarAsignatura/"), array("class" => "fa icon-trash red change-status", "data-href" => "/catalogo/planEstudio/inactivarAsignatura/", "data-description" => $value_nombre, "data-asignatura" => $asignatura, "data-grado" => $grado, "data-plan" => $plan, "data-action" => 'I', "title" => "Inactivar Asignatura"));
                    $columna .= '&nbsp;&nbsp;' . CHtml::link("", Yii::app()->createUrl("/catalogo/planEstudio/actualizarAsignatura/"), array("class" => "fa fa-refresh blue change-update", "data-href" => "/catalogo/planEstudio/actualizarAsignatura/", "data-description" => $value_nombre, "data-asignatura" => $asignatura, "data-grado" => $grado, "data-plan" => $plan, "data-action" => 'A', "data-id" => $id, "title" => "Actualizar Cambios"));
                } else if ($estatus == 'I') {
                    $columna .= CHtml::link("", Yii::app()->createUrl("/catalogo/planEstudio/activarAsignatura/"), array("class" => "fa fa-check blue change-status", "data-href" => "/catalogo/planEstudio/activarAsignatura/", "data-description" => $value_nombre, "data-asignatura" => $asignatura, "data-grado" => $grado, "data-plan" => $plan, "title" => "Activar Asignatura"));
                }
                

                $estatus = strtr($estatus, array("A" => "Activo", "I" => "Inactivo"));
                $rawData[] = array(
                    'id' => $key,
                    'nombre' => "<div class='center'>" . $value_nombre . '</div>',
                    'hora_teorica' => "<div class='center'>" . $hora_teorica . '</div>',
                    'hora_practica' => "<div class='center'>" . $hora_practica . '</div>',
                    'suma' => "<div class='center'>" . $suma . '</div>',
                    'acciones' => "<div class='center'>" . $columna . '</div>',
                    'grado_pre' => "<div class='center'>" . $grado_pre . '</div>',
                    'asignatura_pre' => "<div class='center'>" . $asignatura_pre . '</div>',
                    'estatus' => "<div class='center'>" . $estatus . '</div>',
                    'orden' => $orden
                );
            }
            $columna .= '</div>';
        } else {
            foreach ($asignaturas as $key => $value) {

                $value_nombre = $value['nombre'];

                $rawData[] = array(
                    'id' => $key,
                    'nombre' => $value_nombre,
                );
            }
        }
        if ($edicion)
            return array(new CArrayDataProvider($rawData, array(
                    'pagination' => array(
                        'pageSize' => 20,
                    ),
                        )
                ), $asignaturas_ids);
        else {
            return new CArrayDataProvider($rawData, array(
                'pagination' => array(
                    'pageSize' => 20,
                ),
            ));
        }
    }

    static function registrarTraza($transaccion, $accion) {
        $Utiles = new Utiles();
        $fecha = date('Y-m-d H:i:s');
        $modulo = "Planteles.PlanEstudioController." . $accion;
        $Utiles->traza($transaccion, $modulo, $fecha);
    }

    public function columnaAcciones($data) {

// $id = $data["id"];
//$estatus = (is_object($data->estatusPlantel) && isset($data->estatusPlantel->nombre)) ? $data->estatusPlantel->nombre : "";

        $columna = '<div class="action-buttons">';
        if (Yii::app()->user->pbac('read'))
            $columna .= CHtml::link("", Yii::app()->createUrl("/catalogo/planEstudio/verDetalle/id/" . base64_encode($data->id)), array("class" => "fa icon-zoom-in", "title" => "Consultar Datos del Plan")) . '&nbsp;&nbsp;';
        if (Yii::app()->user->pbac('write'))
            $columna .= CHtml::link("", Yii::app()->createUrl("/catalogo/planEstudio/modificar?id=" . base64_encode($data->id)), array("class" => "fa fa-pencil green", "title" => "Modificar Datos del Plan")) . '&nbsp;&nbsp;';
// $columna .= CHtml::link("", "/planteles/consultar/reporte/id/" . base64_encode($data->id), array("class" => "fa fa-print", "style" => "color:#555;", "title" => "Imprimir Datos del Plantel"));
        $columna .= '</div>';

        return $columna;
    }

}
