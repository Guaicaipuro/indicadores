<?php

class DenominacionPersonalController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de DenominacionPersonalController',
        'write' => 'Creación y Modificación de DenominacionPersonalController',
        'admin' => 'Administración Completa  de DenominacionPersonalController',
        'label' => 'Módulo de DenominacionPersonalController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'admin','activar'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'admin'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Lists all models.
     */
    public function actionLista()
    {
        $model=new DenominacionPersonal('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('DenominacionPersonal')){
            $model->attributes=$this->getQuery('DenominacionPersonal');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new DenominacionPersonal('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('DenominacionPersonal')){
            $model->attributes=$this->getQuery('DenominacionPersonal');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro()
    {
        $model=new DenominacionPersonal;
		$estatus = '';
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if($this->hasPost('DenominacionPersonal'))
        {
            $model->attributes=$this->getPost('DenominacionPersonal');
			$model->nombre = strtoupper($model->nombre);
            $model->nombre = trim($model->nombre);
            $model->fecha_ini = date("Y-m-d H:i:s");
            $model->usuario_ini_id = Yii::app()->user->id;
            $model->estatus='A';
            if($model->save()){
				$estatus = 'exito-registro';
				$model = new DenominacionPersonal();
               // $this->redirect(array('view','id'=>$model->id));
            }
        }

        $this->render('create',array(
            'model'=>$model,
			'estatus'=>$estatus
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id)
    {
            $idDecoded = $this->getIdDecoded($id);
            $model = $this->loadModel($idDecoded);
			$estatus = '';

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if($this->hasPost('DenominacionPersonal'))
            {
                $model->attributes=$this->getPost('DenominacionPersonal');
				$model->nombre = strtoupper($model->nombre);
                $model->nombre = trim($model->nombre);
                $model->fecha_act = date("Y-m-d H:i:s");
                $model->usuario_act_id = Yii::app()->user->id;
                if($model->save()){
                    if(Yii::app()->request->isAjaxRequest){
                        $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La actualización de los Datos se ha efectuado de forma exitosa.'));
                        Yii::app()->end();
                    }else{
						$estatus = 'exito-mod';
					}
                }
            }

            $this->render('update',array(
                    'model'=>$model,
				'estatus'=>$estatus
            ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
		$model->estatus = 'E';
		$model->fecha_elim = date("Y-m-d H:i:s");
		$model->usuario_act_id = Yii::app()->user->id;
        // Descomenta este código para habilitar la eliminación física de registros.
        if($model->update()){

			$this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La Eliminación de los Datos se ha efectuado de forma exitosa.'));

		}
        else{
            $this->renderPartial('//msgBox', array('class'=>'alertDialogBox', 'message'=>'Error 500! Ha ocurrido un error durante la Eliminación de los Datos.'));
        }



//        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//        if(!$this->hasQuery('ajax')){
//            $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
//        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return DenominacionPersonal the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=DenominacionPersonal::model()->findByPk($id);
        if($model===null){
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param DenominacionPersonal $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='denominacion-personal-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $id = base64_encode($id_encoded);
        $columna = '<div class="action-buttons">';

        if ($data->estatus == "E") {
            $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/catalogo/denominacionPersonal/consulta/id/' . $id)) . '&nbsp;&nbsp;';

            if (Yii::app()->user->group == 1) {
                $columna .= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar este Cargo", "onClick" => "VentanaDialog('$id','/catalogo/denominacionPersonal/activar','Activar Denominación','activar')")) . '&nbsp;&nbsp;';
            }
        } else {
            $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/catalogo/denominacionPersonal/consulta/id/' . $id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '/catalogo/denominacionPersonal/edicion/id/' . $id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Eliminar datos", "onClick" => "VentanaDialog('$id','/catalogo/denominacionPersonal/borrar','Inactivar Denominación','borrar')")) . '&nbsp;&nbsp;';
        }
        $columna .= '</div>';
        return $columna;
    }

	  /**
     *  funcion para devolver el esatatus segun las iniciales A y E
     * @param type $data registro del modelo
     * @return string el estatus del registro
     */
    public function estatus($data) {
        $columna='';
        $estatus = $data["estatus"];

        if ($estatus == "E") {
            $columna = "Inactivo";
        } else if ($estatus == "A") {
            $columna = "Activo";
        }
        return $columna;
    }

    /**
     * Reactiva una modalidad en particular
     * @params integer $id del modelo que se va a reactivar
     */
    public function actionActivar() {

        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $id = base64_decode($id);

            $model = $this->loadModel($id);
            if ($model) {
                $model->usuario_act_id = Yii::app()->user->id;
                $model->fecha_act = date("Y-m-d H:i:s");
                $model->estatus = "A";
                if ($model->save()) {
                    $this->registerLog(
                        "ACTIVAR", "activar", "Exitoso", "activo una DenominacionPersonal"
                    );
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Habilitado con exito.'));
                    $model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }
}