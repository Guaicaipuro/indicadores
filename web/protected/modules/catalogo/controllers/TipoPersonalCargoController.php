<?php

class TipoPersonalCargoController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de TipoPersonalCargoCargoController',
        'write' => 'Creación y Modificación de TipoPersonalCargoCargoController',
        'admin' => 'Administración Completa  de TipoPersonalCargoCargoController',
        'label' => 'Módulo de TipoPersonalCargoCargoController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'activar','admin',),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'admin'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Lists all models.
     */
    public function actionLista()
    {
        $model=new TipoPersonalCargo('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('TipoPersonalCargo')){
            $model->attributes=$this->getQuery('TipoPersonalCargo');
        }
        $dataProvider = $model->search();
        $model->unsetAttributes();  // clear any default values
        $this->render('index',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $groupId = Yii::app()->user->group;
        $model = new TipoPersonalCargoCargo('search');
        if (isset($_GET['TipoPersonalCargo']))
            $model->attributes = $_GET['TipoPersonalCargo'];
        $usuarioId = Yii::app()->user->id;
        $dataProvider = new CActiveDataProvider('TipoPersonalCargo');
        $this->render('admin', array(
            'model' => $model,
            'groupId' => $groupId,
            'usuarioId' => $usuarioId,
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {
        $idDecode = base64_decode($id);
        if (is_numeric($idDecode)) {
            $this->renderPartial('view', array(
                'model' => $this->loadModel($idDecode),
            ));
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro()
    {
        $model = new TipoPersonalCargo('registro_modificacion');
        if ($model) {

            if (isset($_POST['TipoPersonalCargo'])) {
                $model->attributes = $_POST['TipoPersonalCargo'];
//                $nombre = trim($_POST[tipoPersonal->nombre);
                //$nombre=(is_object($data->cargo) && isset($data->cargo->nombre))? $data->cargo->nombre:) ""'
//                $nombre = strtoupper($nombre);
//                $model->tipoPersonal->nombre = $nombre;
                $model->usuario_ini_id = Yii::app()->user->id;
                $model->fecha_ini = date("Y-m-d H:i:s");              
                $model->estatus = "A";

                if ($model->validate()) {
                    if ($model->save()) {
                        $this->registerLog(
                                "ESCRITURA", "registro", "Exitoso", "Se creo un Cargo"
                        );
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Exito! ya puede realizar otro registro.'));
                        $model = new TipoPersonalCargo;
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                }
            }
        } else {

            throw new CHttpException(404, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
        }

        $this->renderPartial('_form', array(
            'model' => $model,
        ));
        
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id)
    {
        $id = $_REQUEST['id'];
        $id = base64_decode($id);
        $model = $this->loadModel($id);
       if (isset($_POST['TipoPersonalCargo'])) {
           $model->attributes = $_POST['TipoPersonalCargo'];
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
           $model->setScenario('registro_modificacion');
            if ($model->save())
                if ($model->validate()){
                    if ($model->save()){
                        $this->registerLog('ESCRITURA', 'catalogo.tipoPersonal.create', 'EXITOSO', 'Se ha creado un servicio');
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Actualizado con exito.'));
                        $model = $this->loadModel($id);
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }

                  } else {
                    $this->renderPartial('//errorSumMsg', array('model' => $model));
                }
                }
         $this->renderPartial('_form', array(
            'model' => $model,
        ));
        
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id)
    {
         if (isset($_POST['id'])) {
                $id = $_POST['id'];
                $id = base64_decode($id);
                $model = $this->loadModel($id);
                if ($model) {
                    $model->usuario_act_id = Yii::app()->user->id;
                    $model->fecha_elim = date("Y-m-d H:i:s");
                    $model->estatus = "I";
                    if ($model->save()) {
                        $this->registerLog('ESCRITURA', 'catalogo.especificacionestatus.borrar', 'EXITOSO', 'Se ha eliminado un Servicio');
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Inhabilitado con exito.'));
                        $model = $this->loadModel($id);
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                } else {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');
                }
            }
    }
     /**
        * Activate a particular model.
        * If activation is successful, the browser will be redirected to the 'admin' page.
        * @param integer $id the ID of the model to be deleted
        */
        public function actionActivar($id)
        {
   
            if (isset($_POST['id'])) {
                $id = $_POST['id'];
                $id = base64_decode($id);
                $model = $this->loadModel($id);
                if ($model) {
                    $model->usuario_act_id = Yii::app()->user->id;
                    $model->fecha_elim = date("Y-m-d H:i:s");
                    $model->estatus = "A";
                    if ($model->save()) {
                        //$this->registerLog('ESCRITURA', 'catalogo.especificacionestatus.activar', 'EXITOSO', 'Se ha activado una Servicio');
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Activado con exito.'));
                        $model = $this->loadModel($id);
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                } else {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');
                }
            }
        }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return TipoPersonalCargoCargo the loaded model
     * @throws CHttpException
     */
    public function getEstatus($data) {
        $estatus = $data["estatus"];
        $columna = strtr($estatus, array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado',));
        return $columna;
    }
     public function getFechaIni($data) {
        $result = $data['fecha_ini'];
        
        $result = Utiles::transformDate($result, '-', 'y-m-d', 'd-m-y');
        
        return $result;
    }
    
     public function getFechaAct($data) {
        $result = $data['fecha_act'];
        
        $result = Utiles::transformDate($result, '-', 'y-m-d', 'd-m-y');
        
     return $result;
     
     }

    
    public function loadModel($id)
    {
        $model=TipoPersonalCargo::model()->findByPk($id);
        if($model===null){
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param TipoPersonalCargoCargo $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='tipo-personal-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {

        $id = $data["id"];
        $id = base64_encode($id);
        $estatus = $data['estatus'];
        if ($estatus == 'I') {
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "title" => "Buscar el Tipo de Personal Cargo", "onClick" => "VentanaDialog('$id','/catalogo/tipoPersonalCargo/consulta','Vista del Tipo de Personal Cargo','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-check", "title" => "Activar el Tipo de Personal Cargo", "onClick" => "VentanaDialog('$id','/catalogo/tipoPersonalCargo/activar','Activar el Tipo de Personal Cargo','activar')")) . '&nbsp;&nbsp;';
        } else {
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "title" => "Busqueda del Tipo de Personal Cargo", "onClick" => "VentanaDialog('$id','/catalogo/tipoPersonalCargo/consulta','Vista de Tipo de Personal Cargo','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green", "title" => "Modificar el Tipo de Personal Cargo", "onClick" => "VentanaDialog('$id','/catalogo/tipoPersonalCargo/edicion','Modificar el Tipo de Personal Cargo','update')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Inhabilitar este Tipo de Personal Cargo", "onClick" => "VentanaDialog('$id','/catalogo/tipoPersonalCargo/eliminacion','Inhabilitar el Tipo de Personal Cargo','borrar')")) . '&nbsp;&nbsp;';
        }
        return $columna;
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
//    public function getIdDecoded($id){
//        if(is_numeric($id)){
//            return $id;
//        }
//        else{
//            $idDecodedb64 = base64_decode($id);
//            if(is_numeric($idDecodedb64)){
//                return $idDecodedb64;
//            }
//        }
//        return null;
//    }
}