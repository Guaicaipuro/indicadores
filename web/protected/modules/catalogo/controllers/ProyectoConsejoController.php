<?php

class ProyectoConsejoController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de ProyectoConsejoController',
        'write' => 'Creación y Modificación de ProyectoConsejoController',
        'admin' => 'Administración Completa  de ProyectoConsejoController',
        'label' => 'Módulo de ProyectoConsejoController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'activacion','create'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion','create'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Lists all models.
     */
    public function actionLista()
    {
        $model=new ProyectoConsejo('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('ProyectoConsejo')){
            $model->attributes=$this->getQuery('ProyectoConsejo');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new ProyectoConsejo('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('ProyectoConsejo')){
            $model->attributes=$this->getQuery('ProyectoConsejo');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta()
    {
        if(isset($_GET['id'])){

            $id = $_GET['id'];
            $idDecoded = $this->getIdDecoded($id);
            //$idDecoded = $this->getIdDecoded($id);
            $model = $this->loadModel($idDecoded);
            $this->renderPartial('detallesProyectoConsejo',array(
                'model'=>$model,
            ));

        }
        
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */

    public function actionCreate()
    {
            $model = new ProyectoConsejo;

        //Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model, 'formType'=>'registro'
                ), FALSE, TRUE);
    }

    public function actionRegistro()
    {
        $model=new ProyectoConsejo;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        
        if($this->hasPost('ProyectoConsejo') )
        {   

            $model->attributes=$this->getPost('ProyectoConsejo');
            $model->beforeInsert();

            if($model->validate()) {

                if($model->save()){
                    $this->registerLog('ESCRITURA', 'modulo.ProyectoConsejo.registro', 'EXITOSO', 'El Registro de los datos de ProyectoConsejo se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                   // Yii::app()->user->setFlash('success', 'El proceso de registro de los datos se ha efectuado exitosamente');
                    //$this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Registro Exitoso'));
                    //$this->redirect(array('edicion','id'=>base64_encode($model->id),));
                    $mensaje = 'El proceso de registro de los datos se ha efectuado exitosamente';
                    echo json_encode(array('status'=>'success','mensaje'=>$mensaje));
                    Yii::app()->end();

                }
                else{
                    $mensaje = 'Estimado usuario ha ocurrido un errror al amacenar los datos. Por favor Intente nuevamente';
                    echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
                    Yii::app()->end();

                }
            }else{

               // var_dump('expression'); 
                //Yii::app()->user->setFlash('error', 'El proceso de registro de ...');
                $this->renderPartial('//errorSumMsg', array('model' => $model),false,TRUE);
                Yii::app()->end();
                //echo json_encode(array('mensaje'=>'error'));
            }
        }
        else{
            throw new CHttpException(403,'Estimado usuario no hemos recibido los parametros necesarios para realizar esta accion');
        }
        
        /*
        $this->render('create',array(
            'model'=>$model,
        ));
        */
        //$this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro'));   
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion()
    {
        if(isset($_GET['id'])){

            $id = $_GET['id'];
            $idDecoded = $this->getIdDecoded($id);
            $model = $this->loadModel($idDecoded);

        }
        
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if($this->hasPost('ProyectoConsejo'))
            {
                
                $id = $_POST['ProyectoConsejo']['id'];
                $idDecoded = $this->getIdDecoded($id);
                $model = $this->loadModel($idDecoded);
                $model->attributes=$this->getPost('ProyectoConsejo');
                $model->beforeUpdate();

                if($model->validate()) {

                    if($model->save()){
                        
                        $this->registerLog('ACTUALIZACION', 'modulo.ProyectoConsejo.edicion', 'EXITOSO', 'La Actualización de los datos de ProyectoConsejo se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                        $mensaje = 'El proceso de actualizacion de los datos se ha efectuado exitosamente';
                        echo json_encode(array('status'=>'success','mensaje'=>$mensaje));
                        Yii::app()->end();

                        /*if(Yii::app()->request->isAjaxRequest){
                            $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La actualización de los Datos se ha efectuado de forma exitosa.'));
                            Yii::app()->end();
                        }*/
                    }
                    else
                    {
                    $mensaje = 'Estimado usuario el proceso de actualización no fue completado';
                    echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
                    Yii::app()->end();
                    }
                }
                else
                {
                    $this->renderPartial('//errorSumMsg', array('model' => $model),false,TRUE);
                    Yii::app()->end();
                }
            }

            $this->renderPartial('update',array(
                    'model'=>$model,
            ));
    }

    /**
     * Logical Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion()
    {
        if( isset($_POST['id']) ){
            
            $id = $_POST['id'];
            $idDecoded = $this->getIdDecoded($id);
            $model = $this->loadModel($idDecoded);
        
            if($model){
                $model->beforeDelete();

                if($model->save()){

                    CategoriaConsejo::model()->updateAll(array('estatus' => 'I'), array('condition' => 'proyecto_id =' . $model->id));                    $this->registerLog('ELIMINACION', 'catalogo.ProyectoConsejo.eliminacion', 'EXITOSO', 'La Eliminación de los datos de ProyectoConsejo se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                    $criteria = new CDbCriteria();
                    $criteria->condition ='categoria_id IN (SELECT id FROM gplantel.categoria_consejo WHERE proyecto_id ='. $model->id.')';
                    ConsejoCategoria::model()->updateAll(
                        array('estatus'=>'I'),$criteria);
                    
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La inactivación del proyecto se ha efectuado de forma exitosa.'));


                    /*if(Yii::app()->request->isAjaxRequest){
                        $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La eliminación del registro se ha efectuado de forma exitosa.'));
                        Yii::app()->end();
                    }*/
                }else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
            }else {
                    throw new CHttpException(404, 'Error! Recurso no encontrado!');
                }

        //$this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
        }
    }
    
    /**
     * Activation of a particular model Logicaly Deleted.
     * If activation is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be activated
     */
    public function actionActivacion(){
        
        if( isset($_POST['id']) ){

            $id = $_POST['id'];
            $idDecoded = $this->getIdDecoded($id);
            $model = $this->loadModel($idDecoded);
            
            if($model){
                
                $model->beforeActivate();
                
                if($model->save()){
                    
                    CategoriaConsejo::model()->updateAll(array('estatus' => 'A'), array('condition' => 'proyecto_id=' . $model->id));
                    
                    $this->registerLog('ACTIVACION', 'modulo.ProyectoConsejo.activacion', 'EXITOSO', 'La Activación de los datos de ProyectoConsejo se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La activación de este Proyecto se ha efectuado de forma exitosa.'));

                    /*if(Yii::app()->request->isAjaxRequest){
                        $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La activación de este Proyecto se ha efectuado de forma exitosa.'));
                        Yii::app()->end();
                    }*/
                }else{
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
            }else {
                    throw new CHttpException(404, 'Error! Recurso no encontrado!');
                }
            
            //$this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
        }
        
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ProyectoConsejo the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if(is_numeric($id)){
            $model=ProyectoConsejo::model()->findByPk($id);
            if($model===null){
                throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
        }
        else{
            return null;
        }
    }

    /**
     * Performs the AJAX validation.
     * @param ProyectoConsejo $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='proyecto-consejo-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $id = base64_encode($id_encoded);
        $columna = '<div class="action-buttons">';
        $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", "onClick" => "consultarProyecto('$id')",/*'href' => '/catalogo/proyectoConsejo/consulta/id/'.$id*/)) . '&nbsp;&nbsp;';
        if($data->estatus=='A'){    
            $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", "onClick" => "modificarProyecto('$id')",/* 'href' => '/catalogo/proyectoConsejo/edicion/id/'.$id*/)) . '&nbsp;&nbsp;';
        
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Eliminar", "onClick" => "eliminarProyecto('$id')" ,/*'href' => '/catalogo/proyectoConsejo/eliminacion/id/'.$id*/)) . '&nbsp;&nbsp;';
        }else{
            $columna .= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar", "onClick" => "activarProyecto('$id')",/* 'href' => '/catalogo/proyectoConsejo/activacion/id/'.$id*/)) . '&nbsp;&nbsp;';
        }
        $columna .= '</div>';
        return $columna;
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }

    public function estatus($data) {
        $columna='';
        $estatus = $data["estatus"];

        if ($estatus == "I") {
            $columna = "Inactivo";
        } else if ($estatus == "A") {
            $columna = "Activo";
        }
        return $columna;
    }
}

