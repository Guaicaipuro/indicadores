<?php

class SituacionCargoController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de SituacionCargoController',
        'write' => 'Creación y Modificación de SituacionCargoController',
        'admin' => 'Administración Completa  de SituacionCargoController',
        'label' => 'Módulo de SituacionCargoController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'admin','activar'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'admin'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Lists all models.
     */
    public function actionLista()
    {
        $model=new SituacionCargo('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('SituacionCargo')){
            $model->attributes=$this->getQuery('SituacionCargo');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new SituacionCargo('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('SituacionCargo')){
            $model->attributes=$this->getQuery('SituacionCargo');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro()
    {
        $model=new SituacionCargo;
        $exito=false;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if($this->hasPost('SituacionCargo'))
        {
            $model->usuario_ini_id = Yii::app()->user->id;
            $model->fecha_ini = date("Y-m-d H:i:s");
            $model->estatus = "A";
            $model->attributes=$this->getPost('SituacionCargo');
            if ($model->validate()) {
                if ($model->save()) {
                    //$this->registerLog("ESCRITURA", "registro", "Exitoso", "Se creo un Cargo");
                    //$this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Exito! ya puede realizar otro registro.'));
                    Yii::app()->user->setFlash('exito', "El Registro de los Datos se ha efectuado de forma exitosa");
                    $exito=true;
                    $model->unsetAttributes();
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            }
        }
        $this->render('create',array(
            'model'=>$model,'exito'=>$exito
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id)
    {
        $exitoA=false;
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if($this->hasPost('SituacionCargo'))
        {
            $model->attributes=$this->getPost('SituacionCargo');
            if($model->validate() AND $model->save()){
                $exitoA=true;
                Yii::app()->user->setFlash('exito', "La actualización de los Datos se ha efectuado de forma exitosa");
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La actualización de los Datos se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }
        $this->render('update',array(
            'model'=>$model,'exitoA'=>$exitoA
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        // Descomenta este código para habilitar la eliminación física de registros.
        if($model){
            $model->fecha_elim=date('Y-m-d H:i:s');
            $model->usuario_act_id=Yii::app()->user->id;
            $model->estatus='E';
            if(!$model->update()){
                Yii::app()->user->setFlash('error', "Estimado usuario, no hemos encontrado el recurso solicitado. Por favor recargue la página e intente de nuevo.");
            }
        }
        else {
            Yii::app()->user->setFlash('error', "Estimado usuario, no hemos encontrado el recurso solicitado. Por favor recargue la página e intente de nuevo.");
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!$this->hasQuery('ajax')){
            $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
        }
    }
    /**
     * Activate a particular model.
     * If activation is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionActivar($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);

        if($model){
            $model->estatus = 'A';
            $model->fecha_act = date('Y-m-d H:i:s');
            $model->fecha_elim =null;
            $model->usuario_act_id=Yii::app()->user->id;
            if(!$model->update()){
                Yii::app()->user->setFlash('error', "Estimado usuario, no hemos encontrado el recurso solicitado. Por favor recargue la página e intente de nuevo.");
            }
        }
        else {
            Yii::app()->user->setFlash('error', "Estimado usuario, no hemos encontrado el recurso solicitado. Por favor recargue la página e intente de nuevo.");
        }
        // if AJAX request (triggered by deletion via admin grid view), giwe should not redirect the browser
        if(!$this->hasQuery('ajax')){
            $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SituacionCargo the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=SituacionCargo::model()->findByPk($id);
        if($model===null){
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $model;
    }
    public function estatus($data){
        $estatus=$data['estatus'];
        if($estatus=='A'){
            $columna='Activo';
        } else{
            $columna='Inactivo';
        }
        return $columna;

    }

    /**
     * Performs the AJAX validation.
     * @param SituacionCargo $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='situacion-cargo-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $estatus = $data["estatus"];
        $id = base64_encode($id_encoded);
        $columna = '<div class="action-buttons">';
        if($estatus == 'A'){
            $columna = '<div class="action-buttons">';
            $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/catalogo/situacionCargo/consulta/id/'.$id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '/catalogo/situacionCargo/edicion/id/'.$id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Eliminar datos", 'href' => '/catalogo/situacionCargo/eliminacion/id/'.$id)) . '&nbsp;&nbsp;';
        }
        else {
            $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/catalogo/situacionCargo/consulta/id/'.$id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-check blue", "title" => "Activar datos", 'href' => '/catalogo/situacionCargo/activar/id/'.$id)) . '&nbsp;&nbsp;';
        }
        $columna .= '</div>';
        return $columna;
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }
}