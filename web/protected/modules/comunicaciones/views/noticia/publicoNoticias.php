<?php

/* @var $this NoticiaController */
/* @var $model Noticia */

$this->breadcrumbs=array(
    'Noticias ',
);
$this->pageTitle = 'Noticias Publicadas';

?>
<div>
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/styles-iview.css'; ?>" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/iview.css'; ?>" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/skin-4/style.css'; ?>" />
<!--<div class="col-xs-12">-->
<!--    <div class="pull-right" style="padding-left:10px;">-->
<!--        <a href="--><?php //echo $this->createUrl("/comunicaciones/noticia"); ?><!--" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">-->
<!--            <i class="fa fa-plus icon-on-right"></i>-->
<!--            Noticia                        </a>-->
<!--    </div>-->
<!--</div>-->
    <div class="col-xs-12">
        <div class="space-6"></div>

        <div class="row row-fluid">
            <div class="accordion-style1 panel-group" id="accordion">

                <?php
                $entrada=false;
                foreach($datos as $key => $registro):?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="#collapse<?php echo $key; ?>" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle <?php if($entrada){ echo "collapsed";}?>">
                                        <i data-icon-show="icon-angle-right" data-icon-hide="icon-angle-down" class="bigger-110 <?php if($entrada){ echo "icon-angle-right";}else{ echo "icon-angle-down";}?>"></i>
                                        &nbsp;<?php echo $registro->titulo;?>
                                    </a>
                                </h4>
                            </div>

                            <div id="collapse<?php echo $key; ?>" class="panel-collapse <?php if($entrada){ echo "collapse ";}else if (!$entrada){ echo "collapse in"; $entrada=true;}?>" style="height: 0px;">
                                <div class="panel-body">
                                    <p style="text-align: justify; font-size:17px;font-family: 'Helvetica';" > <?php echo $registro->cuerpo;?>
                                    </p>
                                    <div class="col-xs-12">
                                        <small>

                                            <cite title="Source Title"><?php echo $registro->usuarioIni->nombre.' '.$registro->usuarioIni->apellido." ".$registro->fecha_ini; ?></cite>
                                    </small>
                                    </div>

                                </div>
                            </div>
                        </div>
                <?php endforeach;?>

                <?php $this->widget('CLinkPager',array(
                    'pages' =>$pages,
                    'header' => '',
                    'htmlOptions' => array('class' => 'pagination'),
                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                    'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                    'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                    'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',

                )) ?>

            </div>
        </div>
    </div>
</div>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/raphael-min.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.easing.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.fullscreen.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/iview.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/slider.js',CClientScript::POS_END); ?>
