<?php
$fecha=date('d-m-Y');
?>

<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">
            <!--            <div class="tab-content">-->
            <div class="tab-pane active" id="datosGenerales">
                <div class="form">

                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'noticia-form',
                        'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                    )); ?>
                    <div id="divRespuesta"></div>
                    <div id="div-result">
                        <?php
                        if($estatus=="registrado"||$estatus=="actualizado"): ?>
                            <div class="successDialogBox"><p class="note">La Noticia se ha <?=$estatus;?> con exito.</p></div>
                        <?php
                        endif;

                        if($model->hasErrors()):
                            $this->renderPartial('//errorSumMsg', array('model' => $model));


                        else:
                            ?>
                            <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                        <?php
                        endif;

                        ?>
                    </div>


                    <div id="div-datos-generales">

                        <div class="widget-box">

                            <div class="widget-header">
                                <h5>Datos Generales</h5>

                                <div class="widget-toolbar">
                                    <a data-action="collapse" href="#">
                                        <i class="icon-chevron-up"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="widget-body">
                                <div class="widget-body-inner">
                                    <div class="widget-main">
                                        <div class="widget-main form">
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="col-md-12">
                                                        <label class="col-md-12" >Título</label>
                                                        <?php echo $form->textField($model,'titulo',array('size'=>40, 'maxlength'=>40, 'required' => 'required', 'class' => 'span-12', )); ?>
                                                    </div>


                                                </div>
                                                <div class="col-md-12">
                                                    <div class="space-6"></div>
                                                    <div class="col-md-4">
                                                        <div class="col-md-6">
                                                            <label class="col-md-12" >Fecha de Clausura</label>

                                                            <div class="input-group">
                                                                     <span class="input-group-addon">
                                                                     <i class="icon-calendar bigger-100"></i>
                                                                     </span>

                                                                <?php
                                                                if ($model->fecha_de_clausura != "") {
                                                                    echo $form->textField($model,'fecha_de_clausura', array('size' => 30, 'maxlength' => 30,
                                                                        'id' => 'date-picker', 'required' => 'required', 'readonly' => 'readonly',
                                                                        'value'=>Utiles::transformDate($model->fecha_de_clausura, '-', 'Y-m-d', 'd-m-Y')));

                                                                } else {
                                                                    echo $form->textField($model,'fecha_de_clausura', array('size' => 30, 'maxlength' => 30,
                                                                        'id' => 'date-picker', 'required' => 'required', 'readonly' => 'readonly',
                                                                        'value'=>Utiles::transformDate($fechaClausuraDefecto, '-', 'Y-m-d', 'd-m-Y')));
                                                                }

                                                                ?>

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="col-md-4">
                                                        <label class="col-md-12" >Importante o Fija</label>

                                                        <?php echo $form->dropDownList($model,'importante_o_fija',array('I'=>'Importante', 'F'=>'Fija'), array( 'class' => 'span-12', "required"=>"required",)); ?>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label class="col-md-12" >Estatus</label>
                                                        <?php /*echo $form->labelEx($model,'estatus'); */?>
                                                        <?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado'), array( 'class' => 'span-12', "required"=>"required",)); ?>
                                                    </div>


                                                </div>

                                                <div class="space-6"></div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label class="col-md-12">Cuerpo</label>

                                                        <div id="cuerpo" class="wysiwyg-editor" style="border:1px solid #ccc;word-wrap:break-word;" contenteditable="true"  required="required" >
                                                        </div>
                                                        <?php
                                                        if ($model->cuerpo) {
                                                            $model->cuerpo = CHtml::decode($model->cuerpo);
                                                        }

                                                        echo $form->textArea($model, 'cuerpo', array('id' => 'cuerpo2', 'readonly' => 'true','class'=>'hide', 'hidden' => 'hidden', 'required'=>'required'));
                                                        ?>


                                                    </div>
                                                </div>



                                                <div class="space-6"></div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">

                                <div class="col-md-6">
                                    <a class="btn btn-danger" href="<?php echo $this->createUrl("/comunicaciones/noticia"); ?>" id="btnRegresar">
                                        <i class="icon-arrow-left"></i>
                                        Volver
                                    </a>
                                </div>

                                <div class="col-md-6 wizard-actions">
                                    <button id="btn_guardar" class="btn btn-primary btn-next" data-last="Finish" type="button">
                                        Guardar
                                        <i class="icon-save icon-on-right"></i>
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                </div><!-- form -->
            </div>

            <!--            <div class="tab-pane" id="otrosDatos">-->
            <!--                <div class="alertDialogBox">-->
            <!--                    <p>-->
            <!--                        Próximamente: Esta área se encuentra en Desarrollo.-->
            <!--                    </p>-->
            <!--                </div>-->
            <!--            </div>-->

        </div>



        <div id="resultDialog" class="hide"></div>

        <?php
        /**
         * Yii::app()->clientScript->registerScriptFile(
         *   Yii::app()->request->baseUrl . '/public/js/modules/NoticiaController/noticia/form.js',CClientScript::POS_END
         *);
         */
        ?>
    </div>
</div>


<!--<script src="/public/js/modules/fundamentoJuridico/jquery.ui.widget.js"></script>
<script src="/public/js/modules/fundamentoJuridico/jquery.iframe-transport.js"></script>-->
<!---------------------------------------------------------------------------------------->

<script src="/public/js/ace-elements.min.js"></script>
<script src="/public/js/bootstrap-wysiwyg.min.js"></script>
<script src="/public/js/typeahead-bs2.min.js"></script>
<script src="/public/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/public/datepicker/js/bootstrap-datepicker.js"></script>
<script src="/public/js/jquery.ui.touch-punch.min.js"></script>
<script src="/public/js/markdown/markdown.min.js"></script>
<script src="/public/js/markdown/bootstrap-markdown.min.js"></script>
<script src="/public/js/jquery.hotkeys.min.js"></script>
<script src="/public/js/bootstrap-wysiwyg.min.js"></script>
<script src="/public/js/bootbox.min.js"></script>

<?php
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/public/js/modules/comunicaciones/noticia.js',CClientScript::POS_END
);
