<?php

class NoticiaController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';
//    public $defaultAction='publicoNoticia';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de NoticiaController',
        'write' => 'Creación y Modificación de NoticiaController',
        'admin' => 'Administración Completa  de NoticiaController',
        'label' => 'Módulo de NoticiaController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'admin','publicoNoticia','activar'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'admin','publicoNoticia'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta','publicoNoticia'),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * listaNoticia.
     */
    public function actionPublicoNoticia($page=1)
    {
        list($datos, $paginator) = Noticia::model()->listaNoticias($page);
        $this->render('publicoNoticias',array('datos'=>$datos, 'pages'=>$paginator));


    }
    public function actionReglamento()
    {
        $this->layout='reglamento';
        $this->render('reglamento');
    }

    /**
     * Lists all models.
     */
    public function actionLista()
    {

        $model=new Noticia;
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('Noticia')){
            $model->attributes=$this->getQuery('Noticia');
           
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Noticia('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('Noticia')){
            $model->attributes=$this->getQuery('Noticia');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro()
    {
        $estatus="";
        $date = new DateTime();
        $date->modify('+30 day');
        $fechaClausuraDefecto = $date->format('Y-m-d');

        $model = new Noticia;

        if($this->hasPost('Noticia')){

            $model->attributes = $this->cleanInput($this->getPost('Noticia'));
            $model->usuario_ini_id =Yii::app()->user->id;
            $model->fecha_ini = date("Y-m-d H:i:s");
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            if($model->validate()){
                if($model->save()){
                    $estatus="registrado";
                    $this->redirect('edicion/id/'.base64_encode($model->id).'/estatus/'.base64_encode($estatus));
                    Yii::app()->end();
                }else{
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            }
            else{
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//errorSumMsg', array('model'=>$model));
                    Yii::app()->end();
                }
            }

        }

        $this->render('create', array(
            'model' => $model, 'fechaClausuraDefecto' => $fechaClausuraDefecto,'estatus'=>$estatus

        ));

    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id,$estatus="")
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $estatus = base64_decode($estatus);

        if($this->hasPost('Noticia')){

            $model->attributes = $this->cleanInput($this->getPost('Noticia'));
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            if($model->validate()){
                try {
                    if ($model->save()) {
                        $estatus="actualizado";
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completado la operación, comuníquelo al administrador del sistema.');
                    }
                }
                catch(Exception $e){
                    throw new CHttpException(500, $e->getMessage());
                }
            }
            else{
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//errorSumMsg', array('model'=>$model));
                    Yii::app()->end();
                }
            }
        }
        if($this->getPost('estatus')){
            $estatus="registrado";
        }

        $this->render('update',array(
            'model'=>$model
        ,   'estatus'=>$estatus
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id)
    {

        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);

            if ($model) {
                $model->usuario_act_id = Yii::app()->user->id;
                $model->usuario_elim_id= Yii::app()->user->id;
                $model->fecha_elim = date("Y-m-d H:i:s");
                $model->fecha_act = date("Y-m-d H:i:s");
                $model->estatus = 'E';
                if ($model->estatus=='E') {

                    $model->save();
                    Yii::app()->user->setFlash('eliminacionRealizada', "La Noticia a entrado en Estatus de Eliminada");
                } else {
                    if(!$this->hasQuery('ajax')){
                        $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
                    }
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }

        //-----------------------------------------------
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!$this->hasQuery('ajax')){
            $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
        }

    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Noticia the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {

        $model=Noticia::model()->findByPk($id);

        if($model===null){
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $model;
    }
    public function estatus($data) {
        $estatus = $data["estatus"];

        if ($estatus == "E") {
            $columna = "Eliminado";
        } else if ($estatus == "A") {
            $columna = "Activo";
        }else if ($estatus == "I"){
            $columna = "Inactivo";
        }
        return $columna;
    }
    public function importanteFija($data) {
        $importanteFija = $data["importante_o_fija"];

        if ($importanteFija == "I") {
            $importanteFija = "Importante";
        } else if ($importanteFija == "F") {
            $importanteFija = "Fija";
        }
        return $importanteFija;
    }


    /**
     * Performs the AJAX validation.
     * @param Noticia $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='noticia-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
//    public function getActionButtons($data) {
//        $id_encoded = $data["id"];
//        $linkEliminado=$data["estatus"];
//        $id = base64_encode($id_encoded);
//        $columna = '<div class="action-buttons">';
//        $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/comunicaciones/noticia/consulta/id/'.$id)) . '&nbsp;&nbsp;';
//        $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '/comunicaciones/noticia/edicion/id/'.$id)) . '&nbsp;&nbsp;';
//        if($linkEliminado!='E') {
//            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Eliminar datos", 'href' => '/comunicaciones/noticia/eliminacion/id/' . $id)) . '&nbsp;&nbsp;';
//        }
//        $columna .= '</div>';
//        return $columna;
//    }

    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $estatus = $data["estatus"];
        $id = base64_encode($id_encoded);
        $columna = '<div class="action-buttons">';
        if($estatus == 'A'){
            $columna = '<div class="action-buttons">';
            $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/comunicaciones/noticia/consulta/id/'.$id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '/comunicaciones/noticia/edicion/id/'.$id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Eliminar datos", 'href' => '/comunicaciones/noticia/eliminacion/id/'.$id)) . '&nbsp;&nbsp;';
        }
        else {
            $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/comunicaciones/noticia/consulta/id/'.$id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-check blue", "title" => "Activar datos", 'href' => '/comunicaciones/noticia/activar/id/'.$id)) . '&nbsp;&nbsp;';
        }
        $columna .= '</div>';
        return $columna;
    }

    /**
     * Activate a particular model.
     * If activation is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionActivar($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $model->estatus = 'A';
        ///$model->usuario_act_id=Yii::app()->user->id;
        $model->fecha_act = date('Y-m-d H:i:s');
        $model->fecha_elim =null;
        $model->usuario_elim_id=null;
        //  $model->update();
        $model->usuario_act_id=Yii::app()->user->id;
        //$model->fecha_elim = date('Y-m-d H:i:s');
        $model->update();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!$this->hasQuery('ajax')){
            $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
        }
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }
    /**
     * Encontrado en Internet por Nelson Gonzalez
     * Funcion de filtrado de entradas XSS en formularios
     *
     *   '@<script[^>]*?>.*?</script>@si',   // Elimina javascript
     *   '@<[\/\!]*?[^<>]*?>@si',            // Elimina las etiquetas HTML
     *   '@<style[^>]*?>.*?</style>@siU',    // Elimina las etiquetas de estilo
     *   '@<![\s\S]*?--[ \t\n\r]*>@'         // Elimina los comentarios multi-línea
     */
    function cleanInput($input) {

        $search = array(
            '@<script[^>]*?>.*?</script>@si',
            '@<style[^>]*?>.*?</style>@siU',
            '@<![\s\S]*?--[ \t\n\r]*>@'
        );
        $output = preg_replace($search, '', $input);
        return $output;
    }
}