<?php
//  CHtml::listData
class ModificarController extends Controller {

    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Permite ver los datos básicos del Plantel',
        'write' => 'Permite modificar los datos del Plantel',
        'admin' => 'Otorga permisos extras (solo directores de plantel)',
        'label' => 'Modificación de Planteles'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
//'postOnly + delete', // we only allow deletion via POST request
        );
    }


    public function actions() {
        return array(
            'upload' => array(
                'class' => 'xupload.actions.XUploadAction',
                'path' => Yii::app()->getBasePath() . "/../uploads",
                'publicPath' => Yii::app()->getBaseUrl() . "/uploads",
            ),
            'addTabularInputs' => array(
                'class' => 'ext.yii-playground.actions.XTabularInputAction',
                'modelName' => 'CongresoPedagogicoDocente',
                'viewName' => 'tabular/_tabularInput',
            ),
            'addTabularInputsAsTable' => array(
                'class' => 'ext.yii-playground.actions.XTabularInputAction',
                'modelName' => 'CongresoPedagogicoDocente',
                'viewName' => 'tabular/_tabularInputAsTable',
            ),
        );
    }

    protected function beforeAction($event) {
        /* $this->render("//msgBox", array('class' => 'infoDialogBox', 'message' => 'En este momento este modulo se encuentra en mantenimiento, disculpe las molestias ocasionadas.'));
          Yii::app()->end(); */
        if ($event->id == 'index') {
            if (!(Yii::app()->user->id == 1 OR Yii::app()->user->group == UserGroups::DESARROLLADOR OR Yii::app()->user->group == UserGroups::JEFE_DRCEE OR Yii::app()->user->group == UserGroups::ADMIN_DRCEE OR Yii::app()->user->group == UserGroups::ADMIN_REG_CONTROL OR Yii::app()->user->group == UserGroups::JEFE_DRCEE_ZONA OR Yii::app()->user->group == UserGroups::JEFE_ZONA)) {
                $plantel_id = $this->getRequest('id');
                $plantel_id_decoded = base64_decode($plantel_id);
                if (AutoridadPlantel::model()->esAutoridadDelPlantel($plantel_id_decoded))
                    return true;
                else {
                    $this->registerLog('LECTURA', self::MODULO . $event->id, 'ILEGAL', 'Intento entrar a un plantel en el cual no es autoridad, plantel_id : ' . $plantel_id_decoded);

                    throw new CHttpException(401, 'Estimado usuario usted no es una Autoridad de este Plantel, por lo tanto no tiene acceso a esta acción. ');
                }
            } else
                return true;
        } else
            return true;
    }

    public function accessRules() {

//en esta seccion colocar los action de solo lectura o consulta
        return array(
            array('allow',
                'actions' => array(
                    'seleccionarMunicipio',
                    'seleccionarParroquia',
                    'seleccionarLocalidad',
                    'seleccionarPoblacion',
                    'seleccionarUrbanizacion',
                    'buscarAutoridad',
                    'ViaAutoComplete',
                    'index',
                    'addTabularInputs',
                    'prueba',
                    'addTabularInputsAsTable',
                    'obtenerDatosPersona',
                    'buscarDependenciaNominal',
                    'obtenerFormDependencia'),
                'pbac' => array('read'),
            ),
            //en esta seccion colocar todos los action del modulo
            array('allow',
                'actions' => array(
                    'actualizarDatosGenerales',
                    'actualizarServicio',
                    'actualizarDatosAutoridad',
                    'agregarProyecto',
                    'agregarServicio',
                    'eliminarProyecto',
                    'eliminarServicio',
                    'getAutoridadPlantel',
                    'seleccionarMunicipio',
                    'seleccionarParroquia',
                    'seleccionarPoblacion',
                    'seleccionarUrbanizacion',
                    'seleccionarLocalidad',
                    'index',
                    'upload',
                    'agregarLogo',
                    'eliminarAutoridad',
                    'buscarCedula',
                    'agregarAutoridad',
                    'guardarNuevaAutoridad',
                    'informacionAula',
                    'informacionCongreso',
                    'registrarAula',
                    'crearAula',
                    'modificarAula',
                    'procesarCambioAula',
                    'eliminarAula',
                    'upload',
                    'guardarDatosCongreso',
                    'completarCampos',
                    'updateCongreso',
                    'eliminarCongreso',
                    'editarDocente',
                    'modificarDocentes',
                    'eliminarDocente',
                    'activarDocente',
                    'updateDocente',
                    'editarCongreso',
                    'agregarDocente',
                    'guardarDocente',
                    'guardarDependenciaNominal',
                    'actualizarGridCongresos',
                    'activarCongreso',
                    //AGREGADO POR JONATHAN HUAMAN FECHA 14-05-2015//
                    'MostrarFormModalidad',
                    'GuardarPlantelModalidad',
                    'ActualizarGridPlantelModalidad',
                    'verDatosPlantelModalidad',
                    'ModificarPlantelModalidad',
                    'MostrarDatosPlantelModalidad',
                    'ActivarModalidad',
                    'InactivarModalidad',
                ),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    const MODULO = "Planteles.ModificarController.";

    public function actionPrueba() {
        var_dump($_REQUEST);
    }

    public function actionIndex() {
        if (array_key_exists('id', $_REQUEST) && $_REQUEST['id'] !== '') {
            $id = $_REQUEST['id'];
            $plantel_id = base64_decode($id);

            if (is_numeric($plantel_id)) {
                $model = $this->loadModel($plantel_id);
                Yii::import("xupload.models.XUploadForm");
                if ($model->codigo_ner !== null && $model->codigo_ner !== '') {
                    if (Yii::app()->user->group == UserGroups::DIRECTOR) {
                        $model->setScenario('updateDatosGeneralesDirectorNer');
                    } else {
                        $model->setScenario('updateDatosGeneralesNer');
                    }
                } else {
                    if (Yii::app()->user->group == UserGroups::DIRECTOR) {
                        $model->setScenario('updateDatosGeneralesDirectorSNer');
                    } else {
                        $model->setScenario('updateDatosGeneralesSNer');
                    }
                }

                $rawData = array();
                $autoridadPlantel = new AutoridadPlantel;
                $modelPersonalPlantel = new PersonalPlantel('search');
                $proyectoEndo = new ProyectosEndogenosPlantel;
                $xupload = new XUploadForm();
                $usuario = new UserGroupsUser('nuevoUsuario');
                $congreso = new CongresoPedagogico();
                $congreso_docente = new CongresoPedagogicoDocente();
                $dependenciaNominal = new DependenciaNominalPlantel('search');
                $dependenciaNominal->plantel_id = $plantel_id;
                $existe_congreso = $congreso->findAllByAttributes(array('plantel_id' => $plantel_id));
                $personalPlantel = PersonalPlantel::model()->getIdTipoPersonalExistente($plantel_id);
                /* ARMADO DEL GRID AUTORIDADES Y EL DROPDOWLIST DEL MISMO */
                $modelPersonalPlantel->unsetAttributes();
                $modelPersonalPlantel->plantel_id = $plantel_id;
                $autoridades = $autoridadPlantel->buscarAutoridadesPlantel($plantel_id);
                $dataProviderAutoridades = $this->dataProviderAutoridades($autoridades);
                /* FIN ARMADO DEL GRID AUTORIDADES Y EL DROPDOWLIST DEL MISMO */

                /* ARMADO DEL GRID PROYECTOS ENDOGENOS Y EL DROPDOWLIST DEL MISMO */

                $proyectosEndogenosPlantel = ProyectosEndogenosPlantel::model()->getProyectosEndogenos($plantel_id);
                if ($proyectosEndogenosPlantel !== array()) {

                    $resultPE = $this->dataProviderProyectos($proyectosEndogenosPlantel);
                    $dataProviderPE = $resultPE[0];
                    $ids_proyectosEndoP = $resultPE[1];
// se guarda proyectos_endogenos_id para poder filtrar el dropdownlist
// y quitar de la lista aquellos proyectos que ya fueron asignados al plantel
                    $proyectosEndoDisp = ProyectosEndogenos::model()->getProyectosEndogenos($ids_proyectosEndoP);
                    $listProyectosEndo = CHtml::listData($proyectosEndoDisp, 'id', 'nombre');
                } else {
                    $proyectosEndoDisp = ProyectosEndogenos::model()->getProyectosEndogenos();
                    $listProyectosEndo = CHtml::listData($proyectosEndoDisp, 'id', 'nombre');
                    $dataProviderPE = array();
//$ids_proyectosEndoP = array();
                }
                /* FIN ARMADO DEL GRID PROYECTOS ENDOGENOS Y EL DROPDOWLIST DEL MISMO */

                /* ARMADO DEL GRID SERVICIOS Y EL DROPDOWLIST DEL MISMO */
                $servicios = ServicioPlantel::model()->getServiciosPlantel($plantel_id);
                if ($servicios !== array()) {
                    $resultadoDataProvider = $this->dataProviderServicios($servicios);
                    $dataProviderServ = $resultadoDataProvider[0];
                    $listaServiciosUsados = $resultadoDataProvider[1];
                    $serviciosDispo = Servicio::model()->getServicios($listaServiciosUsados);
                    $listServicios = CHtml::listData($serviciosDispo, 'id', 'nombre');
                } else {
                    $serviciosDispo = Servicio::model()->getServicios();
                    $listServicios = CHtml::listData($serviciosDispo, 'id', 'nombre');
                    $dataProviderServ = array();
                }
                /* FIN ARMADO DEL GRID SERVICIOS Y EL DROPDOWLIST DEL MISMO */


                $distrito = Distrito::model()->findAll(array('order' => 'nombre ASC'));
                /* $denominacion = Denominacion::model()->findAll(array('order' => 'nombre ASC'));
                  $estatusPlantel = EstatusPlantel::model()->findAll(array('order' => 'nombre ASC', 'condition' => 'id=1'));
                  $tipoDependencia = TipoDependencia::model()->findAll(array('order' => 'nombre ASC'));
                  $zonaEducativa = ZonaEducativa::model()->findAll(array('order' => 'nombre ASC'));
                  $clasePlantel = ClasePlantel::model()->findAll(array('order' => 'nombre ASC'));
                  $categoria = Categoria::model()->findAll(array('order' => 'nombre ASC'));
                  $condicionEstudio = CondicionEstudio::model()->findAll(array('order' => 'nombre ASC'));
                  $modalidadEstudio = Modalidad::model()->findAll(array('order' => 'nombre ASC'));
                  $genero = Genero::model()->findAll(array('order' => 'nombre ASC'));
                  $estado = Estado::model()->findAll(array('order' => 'nombre ASC'));
                  $municipio = Municipio::model()->findAll(array('order' => 'nombre ASC'));
                  $parroquia = Parroquia::model()->findAll(array('order' => 'nombre ASC'));
                  $turno = Turno::model()->findAll(array('order' => 'nombre ASC'));
                  $zona_ubicacion = ZonaUbicacion::model()->findAll(array('order' => 'nombre ASC')); */

                $cargoSelect = Cargo::model()->findAll(array('order' => 'nombre ASC'));
                /* DROPDOWNLIST */
//Yii::app()->clientScript->scriptMap['jquery-ui.css'] = false;
//Yii::app()->clientScript->scriptMap['jquery-ui.min.js'] = false;

                $modelAula = new Aula;
                
                // AGREGADO POR JONATHAN HUAMAN  FECHA: 14-05-2015 //
                $modelPlantelModalidad= new PlantelModalidad('search');
                $modelPlantelModalidad->plantel_id=$plantel_id;
                $modalidad= PlantelModalidad::model()->ObtenerDatosPlantelModalidad($plantel_id);
                // FIN DE AGREGADO //

                $this->render('_form', array(
                    'model' => $model,
                    'modelPersonalPlantel' => $modelPersonalPlantel,
                    'modelAula' => $modelAula,
                    //AGREGADO POR JONATHAN HUAMAN//
                    'modelPlantelModalidad'=>$modelPlantelModalidad,
                    'modalidad'=>$modalidad,
                    //FIN DE AGREGACION//
                    'proyectoEndo' => $proyectoEndo,
                    'usuario' => $usuario,
                    'autoridadPlantel' => $autoridadPlantel,
                    'distrito' => $distrito,
                    'personalPlantel'=>$personalPlantel,
                    /* 'denominacion' => $denominacion,
                      'tipoDependencia' => $tipoDependencia,
                      'zonaEducativa' => $zonaEducativa,
                      'estatusPlantel' => $estatusPlantel,
                      'clasePlantel' => $clasePlantel,
                      'categoria' => $categoria,
                      'condicionEstudio' => $condicionEstudio,
                      'modalidadEstudio' => $modalidadEstudio,
                      'genero' => $genero,
                      'estado' => $estado,
                      'municipio' => $municipio,
                      'parroquia' => $parroquia,
                      'turno' => $turno,
                      'zona_ubicacion' => $zona_ubicacion, */
                    'listServicios' => $listServicios,
                    'listProyectosEndo' => $listProyectosEndo,
                    'cargoSelect' => $cargoSelect,
                    'plantel_id' => $plantel_id,
                    'xupload' => $xupload,
                    'usuario' => $usuario,
                    'dataProviderPE' => $dataProviderPE,
                    'dataProviderServ' => $dataProviderServ,
                    'dataProviderAutoridades' => $dataProviderAutoridades,
                    'congreso' => $congreso,
                    'congreso_docente' => $congreso_docente,
                    'existeCongreso' => $existe_congreso,
                    'dependenciaNominal' => $dependenciaNominal,
                    //'ids_proyectosEndoP' => $ids_proyectosEndoP
                ));

                $this->registrarTraza('Entró a Modificar un Plantel', 'index');
                Yii::app()->end();
            }
            throw new CHttpException(404, 'No se ha encontrado el Plantel que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.'); // no es numerico
        } else
            if ((isset($_REQUEST['Aula'])) || (isset($_REQUEST['ajax']))) {
                //$id = $_REQUEST['id'];
                //$plantel_id = base64_decode($id);
                $model = new Aula('search');
                $model->unsetAttributes();  // clear any default values
                if (isset($_GET['Aula']['plantel_id'])) {
                    $model->attributes = $_GET['Aula'];
                    $plantel_id = $_REQUEST['Aula']['plantel_id'];
                    $model->plantel_id = $plantel_id;
                }
                if (isset($_GET['plantel_id'])) {
                    $plantel_id = $_REQUEST['plantel_id'];
                    $model->plantel_id = $plantel_id;
                }
                $this->render('_formAula', array(
                    'model' => $model,
                    'modelAula' => $model,
                    'plantel_id' => $plantel_id,
                ));
            } else {
                throw new CHttpException(404, 'No se ha especificado el Plantel que desea modificar. Vuelva a la página anterior e intentelo de nuevo.'); // esta vacio el request
            }
    }

    public function actionAgregarProyecto() {
        if (isset($_REQUEST['proyecto_endogeno_id'])) {
            $proyecto_endogeno_id = $_REQUEST['proyecto_endogeno_id'];
            //$listaProyectos = $_REQUEST['listaProyectos'];
            $plantel_id = $_REQUEST['plantel_id'];
            $listadoProyectosEndogenos = ProyectosEndogenos::model()->getProyectosEndogenos();
            $estatusMod = true;
            $rawData = array();

            /* if ($listaProyectos !== '')
              $listaProyectos = explode(',', $listaProyectos);
              $listaProyectos[] = $proyecto_endogeno_id;
              asort($listaProyectos);
             *
             */


// Guardar proyecto //
            $proyectosEndogenosPlantel = ProyectosEndogenosPlantel::model()->agregarProyectoEndogenoPlantel($proyecto_endogeno_id, $plantel_id);
// Fin Guardar proyecto //
            /* ARMADO DEL GRID PROYECTOS ENDOGENOS Y EL DROPDOWLIST DEL MISMO */
            if ($proyectosEndogenosPlantel !== array()) {

                $resultPE = $this->dataProviderProyectos($proyectosEndogenosPlantel);
                $dataProviderPE = $resultPE[0];
                $ids_proyectosEndoP = $resultPE[1];
// se guarda proyectos_endogenos_id para poder filtrar el dropdownlist
// y quitar de la lista aquellos proyectos que ya fueron asignados al plantel
                $proyectosEndoDisp = ProyectosEndogenos::model()->getProyectosEndogenos($ids_proyectosEndoP);
                $listProyectosEndo = CHtml::listData($proyectosEndoDisp, 'id', 'nombre');
            } else {
                $proyectosEndoDisp = ProyectosEndogenos::model()->getProyectosEndogenos();
                $listProyectosEndo = CHtml::listData($proyectosEndoDisp, 'id', 'nombre');
                $dataProviderPE = array();
//$ids_proyectosEndoP = array();
            }

            /* FIN ARMADO DEL GRID PROYECTOS ENDOGENOS Y EL DROPDOWLIST DEL MISMO */
            Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
            Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
            Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
            $this->renderPartial('_formEndogeno', array(
                'listProyectosEndo' => $listProyectosEndo,
                'plantel_id' => $plantel_id,
                'dataProvider' => $dataProviderPE,
                'estatusMod' => $estatusMod
//'ids_proyectosEndoP' => $listaProyectos
            ), false, true);
            Yii::app()->end();
        } else {
            throw new CHttpException(404, 'No se ha especificado el Proyecto Endogeno que desea agregar. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    public function actionAgregarLogo() {
        if (isset($_REQUEST['plantel_id']) && isset($_REQUEST['filename'])) {
            $plantel_id = $_REQUEST['plantel_id'];
//$listaProyectos = $_REQUEST['listaProyectos'];
            $filename = $_REQUEST['filename'];
            Plantel::model()->agregarLogo($plantel_id, $filename);
        }
    }

    public function actionEliminarProyecto() {
        if (isset($_REQUEST['proyecto_endogeno_id'])) {
            $proyecto_endogeno_id = $_REQUEST['proyecto_endogeno_id'];
//$listaProyectos = $_REQUEST['listaProyectos'];
            $plantel_id = $_REQUEST['plantel_id'];
            $listadoProyectosEndogenos = ProyectosEndogenos::model()->getProyectosEndogenos();
            $estatusMod = true;
            $rawData = array();
//$fila = $_REQUEST['fila'];
            /* $listaProyectos = explode(',', $listaProyectos);
              $listaProyectos = $this->eliminarProyecto($listaProyectos, $fila);
             *
             */
//$listaProyectos[] = $proyecto_endogeno_id;
//asort($listaProyectos);
// Guardar proyecto //
            $proyectosEndogenosPlantel = ProyectosEndogenosPlantel::model()->eliminarProyectoEndogenoPlantel($proyecto_endogeno_id, $plantel_id);
// Fin Guardar proyecto //
            /* ARMADO DEL GRID PROYECTOS ENDOGENOS Y EL DROPDOWLIST DEL MISMO */
            if ($proyectosEndogenosPlantel !== array()) {
                $resultPE = $this->dataProviderProyectos($proyectosEndogenosPlantel);
                $dataProviderPE = $resultPE[0];
                $ids_proyectosEndoP = $resultPE[1];
// se guarda proyectos_endogenos_id para poder filtrar el dropdownlist
// y quitar de la lista aquellos proyectos que ya fueron asignados al plantel
                $proyectosEndoDisp = ProyectosEndogenos::model()->getProyectosEndogenos($ids_proyectosEndoP);
                $listProyectosEndo = CHtml::listData($proyectosEndoDisp, 'id', 'nombre');
            } else {
                $proyectosEndoDisp = ProyectosEndogenos::model()->getProyectosEndogenos();
                $listProyectosEndo = CHtml::listData($proyectosEndoDisp, 'id', 'nombre');
                $dataProviderPE = array();
//$ids_proyectosEndoP = array();
            }

            /* FIN ARMADO DEL GRID PROYECTOS ENDOGENOS Y EL DROPDOWLIST DEL MISMO */
            Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
            Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
            Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
            $this->renderPartial('_formEndogeno', array(
                'listProyectosEndo' => $listProyectosEndo,
                'plantel_id' => $plantel_id,
                'dataProvider' => $dataProviderPE,
                'estatusMod' => $estatusMod
//'ids_proyectosEndoP' => $listaProyectos
            ), false, true);
            Yii::app()->end();
        } else {
            throw new CHttpException(404, 'No se ha especificado el Proyecto Endogeno que desea eliminar. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    public function actionBuscarCedula() {
        if (isset($_REQUEST['cedula']) && isset($_REQUEST['plantel_id'])) {
            $cedula = $_REQUEST['cedula'];
            $plantel_id = (int) $_REQUEST['plantel_id'];
            $existe = false;
            $cedulaArrayDecoded = array();
            $cedulaDecoded = "";
            if (strpos($cedula, "-")) {
                $cedulaArrayDecoded = explode("-", $cedula);
                if (count($cedulaArrayDecoded) == 2) {
                    $origen = $cedulaArrayDecoded[0];
                    $cedulaDecoded = $cedulaArrayDecoded[1];
                    if (!(is_string($origen) && strlen($origen) == 1 && is_numeric($cedulaDecoded) && strlen($cedulaDecoded) > 1 && strlen($cedulaDecoded) <= 8)) {
                        // MENSAJE DE ERROR NO POSEE EL FORMATO CORRECTO V-99999999
                        $mensaje = "La Cedula de Identidad no posee el formato correcto, Ej: V-99999999 ó E-99999999";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    }
                } else {
                    // MENSAJE DE ERROR NO POSEE EL FORMATO CORRECTO V-99999999
                    $mensaje = "La Cedula de Identidad no posee el formato correcto, Ej: V-99999999 ó E-99999999";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            } else {
                // MENSAJE DE ERROR NO POSEE EL FORMATO CORRECTO V-99999999
                $mensaje = "La Cedula de Identidad no posee el formato correcto, Ej: V-99999999 ó E-99999999";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                Yii::app()->end();
            }

            $autoridades = AutoridadPlantel::model()->buscarAutoridadesPlantel($plantel_id);

            if ($autoridades == array()) {
                /*
                 * Es la primera vez que entra en este ciclo por lo tanto solo resta buscar la cedula en la base de datos
                 * Si es distinto de un array() es porque anteriormente ya habia agregado una autoridad al plantel,
                 * en ese caso hay que validar si la cedula no esta primero en este arreglo
                 */
                $this->validarUsuario($origen, $cedulaDecoded, $autoridades, $plantel_id);
            } else {

                foreach ($autoridades as $key => $value) {
                    if ($value['cedula'] == $cedulaDecoded) {
                        $existe = true;
                        $mensaje = "Esta cedula posee un cargo asignado.";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
//$this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
                        Yii::app()->end();
                    }
                }
                if (!$existe) {
                    $this->validarUsuario($origen, $cedulaDecoded, $autoridades, $plantel_id);
                }
            }
        } else {
            throw new CHttpException(404, 'No se han especificado los datos necesarios. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    public function actionAgregarAutoridad() {
        if (isset($_REQUEST['cedula']) && isset($_REQUEST['cargo']) && isset($_REQUEST['plantel_id'])) {
            $autoridadPlantel = new AutoridadPlantel;
            $cargoModel = new Cargo;
            $rawData = array();
            $cedula = $_REQUEST['cedula'];
            $cargo = $_REQUEST['cargo'];
            $plantel_id = $_REQUEST['plantel_id'];
            $usuario_id = Yii::app()->user->id;
            $autoridades = $autoridadPlantel->buscarAutoridadesPlantel($plantel_id);
            if (strpos($cedula, "-")) {
                $cedulaArrayDecoded = explode("-", $cedula);
                if (count($cedulaArrayDecoded) == 2) {
                    $origen = $cedulaArrayDecoded[0];
                    $cedulaDecoded = $cedulaArrayDecoded[1];
                    if (!(is_string($origen) && strlen($origen) == 1 && is_numeric($cedulaDecoded) && strlen($cedulaDecoded) > 1 && strlen($cedulaDecoded) <= 8)) {
                        // MENSAJE DE ERROR NO POSEE EL FORMATO CORRECTO V-99999999
                        $mensaje = "La Cedula de Identidad no posee el formato correcto, Ej: V-99999999 ó E-99999999";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    }
                } else {
                    // MENSAJE DE ERROR NO POSEE EL FORMATO CORRECTO V-99999999
                    $mensaje = "La Cedula de Identidad no posee el formato correcto, Ej: V-99999999 ó E-99999999";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            } else {
                // MENSAJE DE ERROR NO POSEE EL FORMATO CORRECTO V-99999999
                $mensaje = "La Cedula de Identidad no posee el formato correcto, Ej: V-99999999 ó E-99999999";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                Yii::app()->end();
            }


            if ($autoridades == array()) {
                $resultadoValidacionCargo = $autoridadPlantel->validarExisteCargo($cargo, $plantel_id);
                if ($resultadoValidacionCargo != $cargo) {
                    $datosUsuario = $autoridadPlantel->buscarUsuarioId($cedulaDecoded);
                    $autoridadPlantel->agregarAutoridad($plantel_id, $cargo, $datosUsuario, $cedulaDecoded);
                } else {
                    $mensaje = "Este cargo ya esta asignado a otra persona";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            } else {
                /*
                 *  validamos que en el plantel no haya otro usuario con ese cargo
                 */
                $resultadoValidacionCargo = AutoridadPlantel ::model()->validarExisteCargo($cargo, $plantel_id);
                if ($resultadoValidacionCargo == false) {
                    /*
                     * No hay otro usuario con este cargo_id, entonces lo verificamos que no este en el array autoridades
                     */
                    foreach ($autoridades as $key => $value) {
                        if ($value['cargo_id'] == $cargo) {
                            $mensaje = "Esta cargo ya esta asignado a otro usuario.";
                            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                            echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
//$this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
                            Yii::app()->end();
                        }
                    }
                    $datosUsuario = $autoridadPlantel->buscarUsuarioId($cedulaDecoded);
                    $autoridadPlantel->agregarAutoridad($plantel_id, $cargo, $datosUsuario, $cedulaDecoded);
                } else {
                    $mensaje = "Este cargo ya esta asignado a otra persona";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            }


            /*
             * Armado del provider
             */
            $selectCargo = Cargo::model()->getCargoAutoridad($usuario_id);
            $autoridades = $autoridadPlantel->buscarAutoridadesPlantel($plantel_id);
            $dataProvider = $this->dataProviderAutoridades($autoridades);
            $cargoSelect = $cargoModel->getCargoAutoridad($usuario_id);

            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
//$this->renderPartial('_formAutoridades', array('mensaje' => $mensaje), false, true);
            $this->renderPartial('_formAutoridades', array('autoridadPlantel' => $autoridadPlantel, 'cargoSelect' => $cargoSelect, 'plantel_id' => $plantel_id, 'dataProvider' => $dataProvider));
            Yii::app()->end();
        } else {
            throw new CHttpException(404, 'No se han especificado los datos necesarios para agregar el Cargo. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    public function actionBuscarAutoridad() {

        if (Yii::app()->request->isAjaxRequest) {

            if (is_numeric($this->getQuery('usuario_id')) && is_numeric($this->getQuery('plantel_id'))) {

                $usuario_id = $this->getQuery('usuario_id');
                $plantel_id = $this->getQuery('plantel_id');

                $autoridad = new AutoridadPlantel();
                $autoridadPlantel = $autoridad->getAutoridadPlantel($plantel_id, $usuario_id);
                if ($autoridadPlantel) {

                    $this->renderPartial('_datosAutoridades', array('modelAutoridad' => $autoridadPlantel), false, true);
                } else {

                    $this->renderPartial('//msgBox', array('class' => 'alertDialogBox', 'message' => 'No se ha podido recopilar los datos de la Autoridad, intente nuevamente.'), false, true);
                }
            } else {
                throw new CHttpException(404, 'Recurso no encontrado. Datos incompletos.');
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionActualizarDatosAutoridad() {

        if (Yii::app()->request->isAjaxRequest) {

            //var_dump($this->getPost('estado_id') && is_numeric($this->getPost('estado_id')) && strlen($this->getPost('control_zona_observacion')));

            $usuario_idDecoded = (int) base64_decode($this->getPost('usuario_id'));
            $plantel_idDecoded = (int) base64_decode($this->getPost('plantel_id'));

//            $cargo_id = (int) $this->getPost('cargo');
//            $cargo_idBackup = (int) base64_decode($this->getPost('cargo_idBackup'));

            $email = $this->getPost('email');
            $emailBackup = $this->getPost('emailBackup');

            $telf_cel = (int) $this->getPost('telf_cel');
            $telf_celBackup = (int) $this->getPost('telf_celBackup');

            $telf_fijo = (int) $this->getPost('telf_fijo');
            $telf_fijoBackup = (int) $this->getPost('telf_fijoBackup');

            $validacionDatos = $this->validarActualizacionAutoridad($telf_fijo, $telf_cel, $usuario_idDecoded, $email);
            if ($validacionDatos !== null) {


                $model = $this->loadUserModel($usuario_idDecoded, 'contacto');

                $model->email = $email;
                $model->telefono = $telf_fijo;
                $model->telefono_celular = $telf_cel;
                $model->date_act = date('Y-m-d H:i:s');
                $model->user_act_id = Yii::app()->user->id;

                if ($model) {

                    if ($model->save()) {

                        $this->registerLog('ESCRITURA', self::MODULO . 'ActualizarDatosAutoridad', 'EXITOSO', 'El usuario con el id=' . Yii::app()->user->id . ''
                            . ' ha cambiado los datos de una Autoridad Plantel '
                            . 'Email :<' . $emailBackup . '> a <' . $email . '>, Teléfono Fijo :<' . $telf_fijoBackup . '> a <' . $telf_fijo . '>'
                            . ', Teléfono Celular :<' . $telf_celBackup . '> a <' . $telf_cel . '>');
                        $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => 'Los datos del usuario han sido actualizados exitosamente.'), false, true);
//                        if ($cargo_id !== $cargo_idBackup) {
//                            $autoridad [] = array(
//                                'usuario_id' => $usuario_idDecoded,
//                                'cargo' => $cargo_id,
//                                'cargo_anterior' => $cargo_idBackup
//                            );
//                            $resultadoActualizacionAutoridad = AutoridadPlantel::model()->actualizarAutoridadPlantel($plantel_idDecoded, $autoridad, true);
//                            if ($resultadoActualizacionAutoridad) {
//                                $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => 'Los datos del usuario han sido actualizados exitosamente.'), false, true);
//                            } else {
//                                $this->renderPartial('//msgBox', array('class' => 'errorDialogBox', 'message' => "No se puedo actualizar el cargo de la autoridad en el plantel"), false, true);
//                            }
//                        } else {
//                            $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => 'Los datos del usuario han sido actualizados exitosamente.'), false, true);
//                        }
                    } else {

                        $this->renderPartial('//msgBox', array('class' => 'errorDialogBox', 'message' => CHtml::errorSummary($model)), false, true);
                    }
                } else {

                    $this->renderPartial('//msgBox', array('class' => 'alertDialogBox', 'message' => 'La persona a la que desea actualizar los datos no se encuentra registrada. Recargue la página e intentelo de nuevo.'), false, true);
                }
            } else {
                $this->renderPartial('//msgBox', array('class' => 'errorDialogBox', 'message' => $validacionDatos), false, true);
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionGuardarNuevaAutoridad() {
        if (isset($_REQUEST['UserGroupsUser'])) {
            $existe = false;
            $cedula = array_key_exists('cedula', $_REQUEST['UserGroupsUser']) ? $_REQUEST['UserGroupsUser']['cedula'] : null;
            if (strpos($cedula, "-")) {
                $cedulaArrayDecoded = explode("-", $cedula);
                if (count($cedulaArrayDecoded) == 2) {
                    $origen = $cedulaArrayDecoded[0];
                    $cedulaDecoded = $cedulaArrayDecoded[1];
                    if (!(is_string($origen) && strlen($origen) == 1 && is_numeric($cedulaDecoded) && strlen($cedulaDecoded) > 1 && strlen($cedulaDecoded) <= 8)) {
                        // MENSAJE DE ERROR NO POSEE EL FORMATO CORRECTO V-99999999
                        $mensaje = "La Cedula de Identidad no posee el formato correcto, Ej: V-99999999 ó E-99999999";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'mensajeError', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    }
                } else {
                    // MENSAJE DE ERROR NO POSEE EL FORMATO CORRECTO V-99999999
                    $mensaje = "La Cedula de Identidad no posee el formato correcto, Ej: V-99999999 ó E-99999999";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensajeError', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            } else {
                // MENSAJE DE ERROR NO POSEE EL FORMATO CORRECTO V-99999999
                $mensaje = "La Cedula de Identidad no posee el formato correcto, Ej: V-99999999 ó E-99999999";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'mensajeError', 'mensaje' => $mensaje));
                Yii::app()->end();
            }

            $autoridadPlantel = new AutoridadPlantel;
            $usuario = array(
                'origen' => $origen,
                'cedula' => $cedulaDecoded,
                'username' => $_REQUEST['UserGroupsUser']['username'],
                'nombre' => $_REQUEST['UserGroupsUser']['nombre'],
                'apellido' => $_REQUEST['UserGroupsUser']['apellido'],
                'email' => $_REQUEST['UserGroupsUser']['email'],
                'telefono' => $_REQUEST['UserGroupsUser']['telefono'],
                'telefono_celular' => $_REQUEST['UserGroupsUser']['telefono_celular'],
                'cargo' => $_REQUEST['cargo'],
                'plantel_id' => $_REQUEST['plantel_id']
            );


            $validacionResult = $this->validarUsuarioNuevo($usuario);
            if ($validacionResult == NULL) {
                if ($autoridadPlantel->guardarUsuario($usuario) != array()) {
                    $this->registrarTraza('Guardó Nueva Autoridad', 'GuardarNuevaAutoridad');

                    $selectCargo = Cargo::model()->getCargoAutoridad(Yii::app()->user->id);
                    $autoridades = $autoridadPlantel->buscarAutoridadesPlantel($usuario['plantel_id']);
                    $dataProvider = $this->dataProviderAutoridades($autoridades);
                    Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;

//$this->renderPartial('_formAutoridades', array('mensaje' => $mensaje), false, true);

                    $this->renderPartial('_formAutoridades', array('autoridadPlantel' => $autoridadPlantel, 'cargoSelect' => $selectCargo, 'plantel_id' => $usuario['plantel_id'], 'dataProvider' => $dataProvider), false, true);
                    Yii::app()->end();
//$mensaje = "Usuario Registrado exitosamente, esta en la cola para su activaci&oacute;n";
//echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje));
                } else {
                    $mensaje = "Registro invalido, por favor intente nuevamente";
                    echo json_encode(array('statusCode' => 'mensajeError', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            } else {
                echo json_encode(array('statusCode' => 'mensajeError', 'mensaje' => $validacionResult));
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(404, 'No se han especificado los datos necesarios de la autoridad que desea registrar. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    public function actionEliminarAutoridad() {
        if (isset($_REQUEST['id']) && isset($_REQUEST['plantel_id'])) {
            $id = $_REQUEST['id'];
            $plantel_id = $_REQUEST['plantel_id'];
            $autoridadPlantel = new AutoridadPlantel;
            $autoridadPlantel->eliminarAutoridad($id, $plantel_id);
            $selectCargo = Cargo::model()->getCargoAutoridad(Yii::app()->user->id);
            $autoridades = $autoridadPlantel->buscarAutoridadesPlantel($plantel_id);
            $dataProviderAutoridades = $this->dataProviderAutoridades($autoridades);

            Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
            $this->renderPartial('_formAutoridades', array('autoridadPlantel' => $autoridadPlantel, 'cargoSelect' => $selectCargo, 'plantel_id' => $plantel_id, 'dataProvider' => $dataProviderAutoridades));
            Yii::app()->end();
        } else {
            throw new CHttpException(404, 'No se ha especificado la autoridad a eliminar. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    public function actionActualizarDatosGenerales() {
        if (isset($_REQUEST['Plantel'])) {
            $mensajeError = '';
            $denominacion_id = '';
            $tipo_dependencia = '';
            $ano_fundado = '';

//$model = new Plantel;
            $model = $this->loadModel($_REQUEST['plantel_id']);
            if ($model->codigo_ner !== null && $model->codigo_ner !== '') {
                if (Yii::app()->user->group == UserGroups::DIRECTOR) {
                    $model->setScenario('updateDatosGeneralesDirectorNer');
                    $denominacion_id = $model->denominacion_id;
                    $tipo_dependencia = $model->tipo_dependencia_id;
                    $ano_fundado = $model->annio_fundado;
                } else {
                    $model->setScenario('updateDatosGeneralesNer');
                }
            } else {
                if (Yii::app()->user->group == UserGroups::DIRECTOR) {
                    $model->setScenario('updateDatosGeneralesDirectorSNer');
                    $denominacion_id = $model->denominacion_id;
                    $tipo_dependencia = $model->tipo_dependencia_id;
                    $ano_fundado = $model->annio_fundado;
                } else {
                    $model->setScenario('updateDatosGeneralesSNer');
                }
            }

            if(isset($_REQUEST['Plantel']['avec'])){
                $avec = 1;
            }
            if(!isset($_REQUEST['Plantel']['avec'])){
                $avec = 0;
            }
            $model->attributes = $_REQUEST['Plantel'];
            $model->avec = $avec;
//            var_dump($_REQUEST['Plantel']);exit;
            if ($denominacion_id != null and $denominacion_id != '' AND $tipo_dependencia != null and $tipo_dependencia != '' AND $ano_fundado != null and $ano_fundado != '') {
                $model->denominacion_id = $denominacion_id;
                $model->tipo_dependencia_id = $tipo_dependencia;
                $model->annio_fundado = $ano_fundado;
            }
            $fronteriza = null;
            $ner = "";
            $indigena = null;
            $dificil_acceso = null;
            $model->no_inscrito = (isset($_REQUEST['Plantel']['no_inscrito'])) ? $_REQUEST['Plantel']['no_inscrito'] = 1 : 0;

            if (isset($_REQUEST['indigena']))
                $indigena = $_REQUEST['indigena'];

            if (isset($_REQUEST['dificil_acceso']))
                $dificil_acceso = $_REQUEST['dificil_acceso'];

            if (isset($_REQUEST['fronteriza']))
                $fronteriza = $_REQUEST['fronteriza'];

            if (isset($_REQUEST['Plantel']['cod_plantelNer']) && $_REQUEST['Plantel']['cod_plantelNer'] !== null && $_REQUEST['Plantel']['cod_plantelNer'] !== '') {
                $model->cod_plantel = $_REQUEST['Plantel']['cod_plantelNer'];
                $ner = TRUE;
            }
            if (isset($model->telefono_fijo) && $model->telefono_fijo != null)
                $model->telefono_fijo = Utiles::onlyNumericString($model->telefono_fijo);
            if (isset($model->telefono_otro) && $model->telefono_otro != null)
                $model->telefono_otro = Utiles::onlyNumericString($model->telefono_otro);

            (isset($model->cod_estadistico) && $model->cod_estadistico != '') ? $model->cod_estadistico : $model->cod_estadistico = null;


            if ($model->validate()) {
                if ($model->cod_estadistico != '') {
                    if ($model->validarCodEstadistico($model->id, $model->cod_estadistico)) {
                        $mensajeError .= "El Código Estadistico ya esta asignado a otro plantel <br>";
                    }
                }
                if ($model->validarCodPlantel($model->id, $model->cod_plantel)) {
                    $mensajeError .= "El Código de Plantel ya esta asignado a otro plantel<br>";
                }

                if ($mensajeError !== '') {
                    Yii:: app()->user->setFlash('mensajeError', $mensajeError);
                    $this->renderPartial('flashMsg', array('estatus' => "error"));
                    Yii::app()->end();
                }
                if ($model->update()) {
                    Yii::app()->cache->delete(Plantel::INDICE_IDENTIFICACION_PLANTEL.$model->id);
                    TipoUbicacionPlantel::model()->prepararActualizacionTipoUbicacion($model->id, $fronteriza, $indigena, $dificil_acceso);

                    Yii:: app()->user->setFlash('mensajeExitoso', "Actualización Exitosa");
                    $this->renderPartial('flashMsg', array('estatus' => "success"));
//$this->redirect('index', array('id' => base64_encode($plantel_id)));
//$this->redirect(array('index', 'id' => base64_encode($model->id)));
                    $this->registrarTraza('Actualizó el Plantel ' . $model->id, 'ActualizarDatosGenerales');
                    Yii::app()->end();
                } else {
                    Yii:: app()->user->setFlash('mensajeError', "Actualización Fallida, intente nuevamente");
                    $this->registrarTraza('Actualización Fallida del Plantel ' . $model->id, 'ActualizarDatosGenerales');
                    Yii::app()->end();
                }
            } else {
                $this->renderPartial('//errorSumMsg', array('model' => $model));
                Yii::app()->end();
            }
            /* $model = new Plantel;
              $model->attributes = $_REQUEST['Plantel'];
              if ($model->validate()) {
              if ($model->update()) {

              } else {

              Yii::app()->user->setFlash('mensajeError', "ACTUALIZACIÓN FALLIDA, POR FAVOR INTENTE NUEVAMENTE");
              $this->renderPartial('//flashMsgv2');
              Yii::app()->end();
              }
              } else {
              $this->renderPartial('//errorSumMsg', array('model' => $model));
              Yii::app()->end();
              }
             *
             */
        } else {
            throw new CHttpException(404, 'No se han especificado los datos necesarios del Plantel a modificar. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    public function actionGetAutoridadPlantel() {
        if (Yii::app()->request->isAjaxRequest) {
            $plantel_idDecoded = (int) ($this->getQuery('plantel_id'));
            if (is_numeric($plantel_idDecoded)) {
                $autoridadPlantel = new AutoridadPlantel;
                $modelPersonalPlantel = new PersonalPlantel('search');
                $selectCargo = Cargo::model()->getCargoAutoridad(Yii::app()->user->id);
                $autoridades = $autoridadPlantel->buscarAutoridadesPlantel($plantel_idDecoded);
                $personalPlantel = PersonalPlantel::model()->getIdTipoPersonalExistente($plantel_idDecoded);

                $dataProviderAutoridades = $this->dataProviderAutoridades($autoridades);
                $this->renderPartial('_formAutoridades', array('autoridadPlantel' => $autoridadPlantel,'personalPlantel'=>$personalPlantel, 'modelPersonalPlantel'=>$modelPersonalPlantel,'cargoSelect' => $selectCargo, 'plantel_id' => $plantel_idDecoded, 'dataProvider' => $dataProviderAutoridades));
            } else
                throw new CHttpException(403, 'No se ha encontrado el recurso solicitado. Recargue la página e intentelo de nuevo.');
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionObtenerDatosPersona() {
        if (Yii::app()->request->isAjaxRequest) {
            $docentesCongreso = $this->getPOST('CongresoPedagogicoDocente');

            $tDocumentoIdentidad = $this->getPOST('tdocumento_identidad');
            $documentoIdentidad = $this->getPOST('documento_identidad');
            $index = base64_decode($this->getPOST('index'));
            unset($docentesCongreso[$index]);
            $docentesCongresoOrdenado = array_values($docentesCongreso);
            $autoridadPlantel = '';
            $datosPersona = '';
            $nombresPersona = '';
            $apellidosPersona = '';
            if (in_array($tDocumentoIdentidad, array('V', 'E', 'P'))) {
                if (in_array($tDocumentoIdentidad, array('V', 'E'))) {
                    if (strlen((string) $documentoIdentidad) > 10) {
                        $mensaje = 'Estimado usuario el campo Documento de Identidad no puede superar los diez (10) caracteres.';
                        $title = 'Notificación de Error';
                        echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                        Yii::app()->end();
                    } else {
                        if (!is_numeric($documentoIdentidad)) {
                            $mensaje = 'Estimado usuario el campo Documento de Identidad debe poseer solo caracteres numericos.';
                            $title = 'Notificación de Error';
                            echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                            Yii::app()->end();
                        }
                    }
                }
                if (count($docentesCongresoOrdenado) > 1)
                    foreach ($docentesCongresoOrdenado as $index => $valor) {

                        if (isset($valor['tdocumento_identidad']) AND $valor['tdocumento_identidad'] == $tDocumentoIdentidad AND isset($valor['documento_identidad']) AND $valor['documento_identidad'] == $documentoIdentidad) {
                            $mensaje = 'Estimado usuario, ya agregó al Docente con Documento de Identidad ' . $tDocumentoIdentidad . '-' . $documentoIdentidad;
                            $title = 'Notificación de Error';
                            echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                            Yii::app()->end();
                        }
                    }
                $autoridadPlantel = new AutoridadPlantel();
                $datosPersona = $autoridadPlantel->busquedaSaimeMixta($tDocumentoIdentidad, $documentoIdentidad);
                if ($datosPersona) {
                    (isset($datosPersona['nombre'])) ? $nombresPersona = $datosPersona['nombre'] : $nombresPersona = '';
                    (isset($datosPersona['apellido'])) ? $apellidosPersona = $datosPersona['apellido'] : $apellidosPersona = '';
                    echo json_encode(array('statusCode' => 'SUCCESS', 'nombres' => $nombresPersona, 'apellidos' => $apellidosPersona));
                    Yii::app()->end();
                } else {
                    $mensaje = 'El Documento de Identidad ' . $tDocumentoIdentidad . '-' . $documentoIdentidad . ' no se encuentra registrada en nuestro sistema, por favor contacte al personal de soporte mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>.';
                    $title = 'Notificación de Error';
                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                    Yii::app()->end();
                }
            } else {
                $mensaje = 'Estimado usuario, se han enviado valores no permitidos en el campo Tipo de Documento de Identidad.';
                $title = 'Notificación de Error';
                echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionGuardarDatosCongreso() {
        if (Yii::app()->request->isAjaxRequest) {
            $periodo = '';
            $rollback = false;
            $usuario_id = Yii::app()->user->id;
            $docentesCongreso = $this->getPOST('CongresoPedagogicoDocente');
            $congresoPedagogico = $this->getPOST('CongresoPedagogico');
            $plantel_id = base64_decode($this->getPOST('plantel_id'));
            $modelCongresoPedadogico = new CongresoPedagogico();
            $modelCongresoDocente = new CongresoPedagogicoDocente();
            $modelCongresoPedadogico->attributes = $congresoPedagogico;

            $periodo = PeriodoEscolar ::model()->getPeriodoActivo();
            $modelCongresoPedadogico->periodo_id = $periodo['id'];
            $modelCongresoPedadogico->plantel_id = $plantel_id;
            $modelCongresoPedadogico->fecha_inicio = Utiles::transformDate($congresoPedagogico['fecha_inicio']);
            $modelCongresoPedadogico->fecha_fin = Utiles::transformDate($congresoPedagogico['fecha_fin']);
            $modelCongresoPedadogico->usuario_ini_id = $usuario_id;


            if (is_array($docentesCongreso)) {
                $docentesCongreso = array_values($docentesCongreso);
                foreach ($docentesCongreso as $index => $valor) {
                    if ((isset($valor['tdocumento_identidad']) AND isset($valor['documento_identidad'])) AND ($valor['documento_identidad'] == '' OR $valor['tdocumento_identidad'] == '' )) {
                        unset($docentesCongreso[$index]);
                    }
                }
                if (count($docentesCongreso) > 0) {
                    $docentesCongreso = array_values($docentesCongreso);
                    if ($modelCongresoPedadogico->validate()) {

                        $transaction = Yii::app()->db->beginTransaction();
                        try {

                            if ($modelCongresoPedadogico->save()) {
                                foreach ($docentesCongreso as $index => $valor) {
                                    $modelCongresoDocente = new CongresoPedagogicoDocente();
                                    $modelCongresoDocente->congreso_id = $modelCongresoPedadogico->id;
                                    $modelCongresoDocente->tdocumento_identidad = $valor['tdocumento_identidad'];
                                    $modelCongresoDocente->documento_identidad = $valor['documento_identidad'];
                                    $modelCongresoDocente->nombres = $valor['nombres'];
                                    $modelCongresoDocente->apellidos = $valor['apellidos'];
                                    $modelCongresoDocente->usuario_ini_id = $usuario_id;
                                    $modelCongresoDocente->save(false);
                                }
                            }
                            $transaction->commit();
                        } catch (Exception $ex) {
                            $rollback = true;
                            $transaction->rollback();
                            $mensaje = 'Estimado usuario, ha ocurrido un error durante el proceso de registro, intente nuevamente. Si el error persiste porfavor comuniquese mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>';
                            $title = 'Notificación de Error';
                            echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title, 'exception' => $ex->getMessage()));
                            Yii::app()->end();
                        }
                        if ($rollback == false) {
                            $mensaje = 'Estimado usuario, se ha registrado exitosamente los datos del Congreso Pedagógico. Al cerrar esta ventana se refrescara la pagína automaticamente.';
                            $title = 'Notificación de Exito';
                            echo json_encode(array('statusCode' => 'SUCCESS', 'mensaje' => $mensaje, 'title' => $title));
                            Yii::app()->end();
                        }
                    } else {
                        $this->renderPartial('//errorSumMsg', array('model' => $modelCongresoPedadogico));
                    }
                } else {
                    $mensaje = 'Estimado usuario, debe agregar por lo menos un Docente.';
                    $title = 'Notificación de Error';
                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                    Yii::app()->end();
                }
            } else {
                $mensaje = 'Estimado usuario, se han enviado valores no permitidos o no ha agregado por lo menos un Docente.';
                $title = 'Notificación de Error';
                echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    //Busqueda de la dependencia nominal antes de ser guardada.. - Meylin

    public function actionBuscarDependenciaNominal() {

        if (Yii::app()->request->isAjaxRequest) {
            $dependencia = $_REQUEST['codigo'];
            $plantelId = $_REQUEST['plantel'];
            $model = new DependenciaNominalPlantel();
            if ($dependencia AND $plantelId) {

                $busqueda_estado_plantel = DependenciaNominalPlantel::model()->extraerEstadoPlantel($plantelId);
                $busqueda_estado_dependencia = DependenciaNominalPlantel::model()->extraerEstadoDependencia($dependencia);
                $existeDependencia = DependenciaNominalPlantel::model()->validarDependencia($dependencia);
                //var_dump($busqueda_estado_dependencia);die();

                if ($existeDependencia['result'] == 1) {
                    $estado_p = $busqueda_estado_plantel[0]['estado_id'];
                    $estado_p = $busqueda_estado_plantel[0]['estado_id'];
                    $estado_d = $busqueda_estado_dependencia[0]['estado_id'];
                    //var_dump($estado);die();
                    if ($estado_p == $estado_d) {

                        $estado_id = intval($estado_d);
                        $existeEstado = DependenciaNominalPlantel::model()->validarEstadoDependencia($estado_id, $dependencia);
                        if ($existeEstado == 0) {
                            $mensaje = "Disculpe, el código que ingreso <b>no coincide con el estado del plantel</b>. ";
                            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO existe una dependencia con ese codigo en el estado
                            Yii::app()->end();
                        }


                        $datos = DependenciaNominalPlantel::model()->extraerDatosDependenciaPlantel($dependencia);
                        $yaExiste = DependenciaNominalPlantel::model()->verificarRegistroDependenciaPlantel($dependencia, $plantelId);
                        // var_dump($yaExiste);die();

                        if ($yaExiste['result'] == 1) {

                            $mensaje = "Disculpe, el código que ingreso <b>ya se encuentra registrado para este plantel</b>. ";
                            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO existe una dependencia con ese codigo en el estado
                            Yii::app()->end();
                        }

                        // var_dump($yaExiste);die();
                        //envia datos de la dependencia.
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'existe', 'nombre' => $datos['nombre'], 'id' => $datos['dependencia_nominal_id'])); //'error' => true,
                        Yii::app()->end();


                        //s echo 'Exito';
                    } else {
                        $mensaje = "Disculpe, el código que ingreso <b>no pertenece al estado del plantel</b>. ";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO CORRESPONDE AL ESTADO
                        Yii::app()->end();
                    }
                } else {
//
                    $mensaje = "Disculpe, el código que ingreso <b>no se encuentra registrado</b>. ";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje)); // NO EXISTE
                    Yii::app()->end();

                    //var_dump($existeEstado);die();
                    echo 'No existe';
                }




                Yii::app()->end();
            } else {
                $model->addError('dependencia_nominal', 'Estimado usuario, debe ingresar una Dependencia Nominal.');
                $this->renderPartial('//errorSumMsg', array('model' => $model));
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionGuardarDependenciaNominal() {
        if (Yii::app()->request->isAjaxRequest) {
            $dependenciaForm = $this->getPost('DependenciaNominalPlantel');
            $plantelId = base64_decode($this->getPost('plantel_id'));
            $model = new DependenciaNominalPlantel();
            if ($dependenciaForm AND $plantelId) {

                $model->attributes = $dependenciaForm;
                $model->usuario_ini_id = Yii::app()->user->id;
                $model->estatus = 'A';
                $model->plantel_id = $plantelId;
                if ($model->validate() AND $model->save()) {
                    echo 'Exito';
                    Yii::app()->end();
                } else {
                    $this->renderPartial('//errorSumMsg', array('model' => $model));
                    Yii::app()->end();
                }
            } else {
                $model->addError('dependencia_nominal', 'Estimado usuario, debe ingresar una Dependencia Nominal.');
                $this->renderPartial('//errorSumMsg', array('model' => $model));
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function eliminarProyecto($listaProyectos, $fila) {
        for ($i = $fila; $i < count($listaProyectos); $i++) {
            if (isset($listaProyectos[$i + 1])) {
                $listaProyectos [$i] = $listaProyectos[$i + 1];
            } else
                unset($listaProyectos[$i]);
        }

        return $listaProyectos;
    }

    public function loadAula($id) {
        $model = Aula::model()->findByPk($id);

        if ($model === null)
            throw new CHttpException(404, 'No se ha encontrado el Aula que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        return $model;
    }

    public function loadCongreso($id) {
        $model = CongresoPedagogico::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, '1 No se ha encontrado el Congreso que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        return $model;
    }


    public function loadCongresoDocente($id) {
        $model = CongresoPedagogicoDocente::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'No se han encontrado los datos del Congreso que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        return $model;
    }

    public function loadModel($id) {
        $model = Plantel::model()->findByPk($id);

        if ($model === null)
            throw new CHttpException(404, 'No se ha encontrado el Plantel que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        return $model;
    }

    public function loadModalidad($id) {
        $model = Modalidad::model()->findByPk($id);

        if ($model === null)
            throw new CHttpException(404, 'No se ha encontrado el Plantel que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        return $model;
    }

    public function loadUserModel($id, $scenario = false) {

        $model = UserGroupsUser::model()->findByPk((int) $id);

        if ($model === null || ($model->relUserGroupsGroup->level > Yii::app()->user->level))
            throw new CHttpException(403, 'El recurso solicitado no se ha encontrado o puede que su perfil de usuario no posea acceso a este recurso.');
        if ($scenario)
            $model->setScenario($scenario);
        return $model;
    }

    public function actionAgregarServicio() {
        if (isset($_REQUEST['id']) && isset($_REQUEST['calidad'])) {
            $listaServicio = array();
            $servicios = array();
            $listaServicio[] = $servicio_id = $_REQUEST['id'];
            $calidad = $_REQUEST['calidad'];
            $plantel_id = $_REQUEST['plantel_id'];
            $fecha_desde = $_REQUEST['fecha_desde'];
//$listaServicio = Yii::app()->getSession()->get('listaServicio') !== null ? Yii::app()->getSession()->get('listaServicio') : array();
//$servicios = Yii::app()->getSession()->get('servicios') !== null ? Yii::app()->getSession()->get('servicios') : array();

            $servicios = ServicioPlantel:: model()->agregarServicio($plantel_id, $servicio_id, $calidad, $fecha_desde);

            $resultadoDataProvider = $this->dataProviderServicios($servicios);
            $dataProviderServ = $resultadoDataProvider[0];
            $listaServiciosUsados = $resultadoDataProvider[1];
            $serviciosDispo = Servicio::model()->getServicios($listaServiciosUsados);
            $listServicios = CHtml::listData($serviciosDispo, 'id', 'nombre');
//Yii::app()->getSession()->add('servicios', $servicios);
            Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
            Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
            Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
            $this->renderPartial('_formServicio', array('dataProvider' => $dataProviderServ, 'listServicios' => $listServicios, 'plantel_id' => $plantel_id), false, true);
            $this->registrarTraza('Agregó un servicio al Plantel ' . $plantel_id, 'AgregarServicio');
            Yii::app()->end();
        } else {
            throw new CHttpException(404, 'No se han especificado los datos necesarios para agregar el Servicio. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    public function actionEliminarServicio() {
        if (isset($_REQUEST['id']) && isset($_REQUEST['plantel_id'])) {
            $listaServicio = array();
            $servicios = array();
            $listaServicio[] = $servicio_id = $_REQUEST['id'];
            $plantel_id = $_REQUEST['plantel_id'];

//$listaServicio = Yii::app()->getSession()->get('listaServicio') !== null ? Yii::app()->getSession()->get('listaServicio') : array();
//$servicios = Yii::app()->getSession()->get('servicios') !== null ? Yii::app()->getSession()->get('servicios') : array();

            $servicios = ServicioPlantel:: model()->eliminarServicio($plantel_id, $servicio_id);
            if ($servicios !== array()) {
                $resultadoDataProvider = $this->dataProviderServicios($servicios);
                $dataProviderServ = $resultadoDataProvider[0];
                $listaServiciosUsados = $resultadoDataProvider[1];
                $serviciosDispo = Servicio::model()->getServicios($listaServiciosUsados);
                $listServicios = CHtml::listData($serviciosDispo, 'id', 'nombre');
            } else {
                $dataProviderServ = array();
                $serviciosDispo = Servicio::model()->getServicios();
                $listServicios = CHtml::listData($serviciosDispo, 'id', 'nombre');
            }
//Yii::app()->getSession()->add('servicios', $servicios);
            Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
            Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
            Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
            $this->renderPartial('_formServicio', array('dataProvider' => $dataProviderServ, 'listServicios' => $listServicios, 'plantel_id' => $plantel_id), false, true);
            $this->registrarTraza('Eliminó un servicio al Plantel ' . $plantel_id, 'EliminarServicio');
            Yii::app()->end();
        } else {
            throw new CHttpException(404, 'No se han especificado los datos necesarios para eliminar el Servicio. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    public function actionActualizarServicio() {
        if (isset($_REQUEST['id']) && isset($_REQUEST['calidad'])) {
            $listaServicio = array();
            $servicios = array();
            $listaServicio[] = $servicio_id = $_REQUEST['id'];
            $calidad = $_REQUEST['calidad'];
            $plantel_id = $_REQUEST['plantel_id'];
            $fecha_desde = $_REQUEST['fecha_desde'];
//$listaServicio = Yii::app()->getSession()->get('listaServicio') !== null ? Yii::app()->getSession()->get('listaServicio') : array();
//$servicios = Yii::app()->getSession()->get('servicios') !== null ? Yii::app()->getSession()->get('servicios') : array();

            $servicios = ServicioPlantel:: model()->actualizarServicio($plantel_id, $servicio_id, $calidad, $fecha_desde);

            $resultadoDataProvider = $this->dataProviderServicios($servicios);
            $dataProviderServ = $resultadoDataProvider[0];
            $listaServiciosUsados = $resultadoDataProvider[1];
            $serviciosDispo = Servicio::model()->getServicios($listaServiciosUsados);
            $listServicios = CHtml::listData($serviciosDispo, 'id', 'nombre');
//Yii::app()->getSession()->add('servicios', $servicios);
            Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
            $this->renderPartial('_formServicio', array('dataProvider' => $dataProviderServ, 'listServicios' => $listServicios, 'plantel_id' => $plantel_id), false, true);
            $this->registrarTraza('Actualizó un servicio al Plantel ' . $plantel_id, 'AcstualizarServicio');
            Yii::app()->end();
        } else {
            throw new CHttpException(404, 'No se han especificado los datos necesarios para actualizar el Servicio. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    /**
     * Performs the AJAX validation.
     * @param Plantel $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'plantel-form') {
            echo CActiveForm::validate($model);
            Yii

                ::app()->end();
        }
    }

    public function dataProviderProyectos($proyectosEndogenosPlantel) {
        $listadoProyectosEndogenos = ProyectosEndogenos::model()->getProyectosEndogenos();
        foreach ($proyectosEndogenosPlantel as $key => $value) {
            $proyecto_endogeno_id = $ids_proyectosEndoP[] = $value['proyectos_endogenos_id'];
            $boton = "<div class='center'>" . CHtml::link("", "", array("class" => "icon-trash red remove-data", 'onClick' => " eliminarProyecto('$proyecto_endogeno_id')", "title" => "Eliminar Proyecto Endogeno")) .
                "</div>";
            $nombre = "<div class='center'>" . $listadoProyectosEndogenos[$value['proyectos_endogenos_id']]['nombre'] . "</div>";
            $rawData[] = array(
                'id' => $key,
                'proyectos_endogenos_id' => $value['proyectos_endogenos_id'],
                'nombre' => $nombre,
                'boton' => $boton
            );
        }
        $dataProviderPE = new CArrayDataProvider($rawData, array(
            'pagination' => array(
                'pageSize' => 5,
            )
        ));

        return array(
            $dataProviderPE, $ids_proyectosEndoP);
    }

    public function dataProviderServicios($servicios) {
        $nombres_servicios = Servicio::model()->getServicios();
        $condicion_servi = CondicionServicio::model()->getCondServ();
        $listaServiciosUsados = array();


        foreach ($servicios as $key => $value) {
            $servicio_id = $listaServiciosUsados[] = $value['servicio_id'];

            $botones = "<div class='center'>" . CHtml::link("", "", array("class" => "fa fa-pencil green",
                        'onClick' => "actualizarServicio(' $servicio_id')",
                        "title" => "Actualizar Servicio")
                ) .
                '&nbsp;&nbsp;' . CHtml::link("", "", array("class" => "icon-trash red remove-data",
                        'onClick' => "eliminarServicio(' $servicio_id')",
                        "title" => "Eliminar Servicio")
                ) .
                "</div>";
            $servicio = "<div class='center'>" . $nombres_servicios [$value['servicio_id']]['nombre'] . "</div>";
            $calidad = "<div class='center'>" . $condicion_servi [$value['calidad']]['nombre'] . "</div>";

            $rawData[] = array(
                'id' => $key,
                'servicio' => $servicio,
                'calidad' => $calidad,
                'fecha_desde' => '<center>' . $value['fecha_desde'] . '</center>',
                'boton' => $botones
            );
        }
        return array(new CArrayDataProvider($rawData, array(
                'pagination' => array(
                    'pageSize' => 5,
                ),
            )
        ),
            $listaServiciosUsados
        );
    }

    public function dataProviderAutoridades($autoridades) {
        $rawData = array();
        $boton = '';
        $usuario_id_signed = Yii::app()->user->id;
        if ($autoridades != array()) {

            foreach ($autoridades as $key => $value) {
                $boton = '';
                $id = $value['id'];
                $usuario_id = $value['usuario_id'];
                if ($usuario_id_signed != $usuario_id) {
                    $boton = "<div class='action-buttons center'>" .
                        CHtml::link("", "", array("class" => "icon-pencil green change-data", 'data-id' => $usuario_id, "title" => "Modificar Autoridad")) . '&nbsp;&nbsp;' .
                        CHtml::link("", "", array("class" => "icon-trash red remove-data", 'onClick' => "eliminarAutoridad($id)", "title" => "Eliminar Autoridad")) .
                        "</div>";
                }
                $nombre = "<div class='center'>" . $value['nombre'] . ' ' . $value['apellido'] . "</div>";
                $cedula = "<div class='center'>" . $value['cedula'] . "</div>";
                $cargo = "<div class='center'>" . $value['nombre_cargo'] . "</div>";

                $telefono_fijo = "<div class='center'>" . $value['telefono_fijo'] . "</div>";
                $telefono_celular = "<div class='center'>" . $value['telefono_celular'] . "</div>";
                $correo = "<div class='center'>" . $value['email'] . "</div>";
                $rawData [] = array(
                    'id' => $key,
                    'cargo' => $cargo,
                    'nombre' => $nombre,
                    'cedula' => $cedula,
                    'correo' => $correo,
                    'telefono_fijo' => $telefono_fijo,
                    'telefono_celular' => $telefono_celular,
                    'boton' => $boton
                );
            }
            return new CArrayDataProvider($rawData, array(
                'pagination' => array(
                    'pageSize' => 5,
                ),
            ));
        } else
            return new CArrayDataProvider($rawData, array(
                'pagination' => array(
                    'pageSize' => 5,
                ),
            ));
    }

    public function validarUsuario($origen, $cedula, $autoridades, $plantel_id) {
        $existe_usuario_autoridad = "";
        $autoridadPlantel = new AutoridadPlantel();

        $busquedaCedula = $autoridadPlantel->busquedaSaime($origen, $cedula); // valida si existe la cedula en la tabla saime
        if (!$busquedaCedula) {
            $mensaje = "Esta Cedula de Identidad no se encuentra registrada en nuestro sistema, "
                . "por favor contacte al personal de soporte mediante "
                . "<a href='mailto:soporte_gescolar@me.gob.ve'>soporte_gescolar@me.gob.ve</a>";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); // NO EXISTE EN SAIME
// $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
            Yii::app()->end();
        } else {
            $busquedaCedulaUG = $autoridadPlantel->busquedaUserGroups($origen, $cedula);
            if ($busquedaCedulaUG == null) {
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'usuario' => $busquedaCedula['cedula'] . $this->generarLetraFromCedula($cedula)));
                Yii::app()->end();
            } else {
                $tipo_dependencia = Plantel::model()->validarTipoDependencia($plantel_id);
                $existe_usuario_autoridad = Plantel::model()->validarAutoridad($tipo_dependencia, $busquedaCedulaUG, $plantel_id);
                if ($existe_usuario_autoridad != array()) {
                    // ya tiene un cargo en ese plantel
                    $mensaje = "El Usuario ya posee un cargo como Autoridad en el MPPE. <br>"
                        . " Si cree que esto puede ser una excepción comuniquelo al correo "
                        . "<a href='mailto:soporte_gescolar@me.gob.ve'>soporte_gescolar@me.gob.ve</a>";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                    //$this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
                    $this->registerLog('ILEGAL', 'Planteles.Modificar.BuscarCedula', 'NO EXITOSO', 'El Usuario ha intentado registrar una autoridad (C.I: ' . $origen . '-' . $cedula . ') que ya posee un cargo en el MPPE');
                    Yii::app()->end();
                } else {
                    $usuarioUGU = UserGroupsUser::model()->findByPk($busquedaCedulaUG);
                    $group_id_usuario = (isset($usuarioUGU) && $usuarioUGU->group_id !== null) ? $usuarioUGU->group_id : null;
                    $grupoUsuario = (isset($usuarioUGU) && is_object($usuarioUGU->relUserGroupsGroup) !== null) ? $usuarioUGU->relUserGroupsGroup->description : null;

                    if ($group_id_usuario !== null && in_array($group_id_usuario,array(UserGroups::DIRECTOR,UserGroups::FUNCIONARIO_MPPE))) {
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'successC', 'cedula' => $cedula, 'autoridades' => $autoridades)); // debe asignar un cargo
                        // $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
                        Yii::app()->end();
                    } else {
                        $mensaje = "El Usuario esta asignado al grupo <strong>'$grupoUsuario'</strong> en el sistema, debe solicitar acceso a la <strong>'Dirección de Registro y Control de Estudios y Evaluación'</strong>";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                        //$this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
                        $this->registerLog('ILEGAL', 'Planteles.Modificar.BuscarCedula', 'NO EXITOSO', 'El Usuario ha intentado registrar una autoridad (C.I: ' . $origen . '-' . $cedula . ') y dicho usuario no esta en el grupo DIRECTOR');
                        Yii::app()->end();
                    }
                }
            }
        }
    }

    public function validarActualizacionAutoridad($telf_fijo, $telf_cel, $usuario_id, $email) {
        $mensaje = "";
        if ($telf_fijo == null || $telf_fijo == '') {
            $mensaje .= "El campo Teléfono Fijo no puede estar vacio <br>";
        } elseif (strlen($telf_fijo) < 11)
            $mensaje .= "El campo Teléfono Fijo debe poseer 11 Dígitos <br>";

        if ($telf_cel == null || $telf_cel == '') {
            $mensaje .= "El campo Teléfono Celular no puede estar vacio <br>";
        } elseif (strlen($telf_cel) < 11)
            $mensaje .= "El campo Teléfono Celular debe poseer 11 Dígitos <br>";

        if ($email == null || $email == '') {
            $mensaje .= "El campo Correo Eletrónico no puede estar vacio <br>";
        } else
            if (!(filter_var($email, FILTER_VALIDATE_EMAIL))) {
                $mensaje .= "El campo Correo Eletrónico no posee el formato correcto <br>";
            } else
                if ($email == AutoridadPlantel::model()->validarUniqueEmail($email, $usuario_id)) {
                    $mensaje .= "Este Correo Eletrónico ya esta registrado <br>";
                }

//        if ($cargo_id == null || $cargo_id == '') {
//            $mensaje .= "El campo Cargo no puede estar vacio <br>";
//        } elseif ($cargo_id == AutoridadPlantel::model()->validarExisteCargo($cargo_id, $plantel_id, $usuario_id)) {
//            $mensaje .= "Este Cargo ya esta asignado. <br>";
//        }

        if ($mensaje == "") {
            return null;
        } else
            return $mensaje;
    }

    public function validarUsuarioNuevo($usuario) {
        $mensaje = "";
        $cod_area_movil = array('0416', '0426', '0412', '0414', '0424');
        /*
         * Validar Cedula
         */
        if ($usuario['cedula'] == null || $usuario['cedula'] == '') {
            $mensaje .= "El campo Cedula no puede estar vacio <br>";
        }
        if ($usuario['nombre'] == null || $usuario['nombre'] == '') {
            $mensaje .= "El campo Nombre no puede estar vacio <br>";
        }
        if ($usuario['apellido'] == null || $usuario['apellido'] == '') {
            $mensaje .= "El campo Apellido no puede estar vacio <br>";
        }
        if ($usuario['telefono'] != null && $usuario['telefono'] != '')
            if (strlen($usuario['telefono']) < 11) {
                $mensaje .= "El campo Telefono Fijo debe poseer 11 Dígitos <br>";
            } else if (substr($usuario['telefono'], 0, 2) != '02') {
                $mensaje .= "El campo Telefono Fijo no posee el formato correcto <br>";
            }

        if ($usuario['telefono_celular'] == null || $usuario['telefono_celular'] == '') {
            $mensaje .= "El campo Telefono Celular no puede estar vacio <br>";
        } elseif (strlen($usuario['telefono_celular']) < 11) {
            $mensaje .= "El campo Telefono Celular debe poseer 11 Dígitos <br>";
        } else if (!in_array(substr($usuario['telefono_celular'], 0, 4), $cod_area_movil, false)) {
            $mensaje .= "El campo Telefono Celular no posee el formato correcto <br> ";
        }
        if ($usuario['email'] == null || $usuario['email'] == '') {
            $mensaje .= "El campo Email no puede estar vacio <br>";
        } elseif (!(filter_var($usuario['email'], FILTER_VALIDATE_EMAIL))) {
            $mensaje .= "El campo Email no posee el formato correcto <br>";
        } elseif ($usuario['email'] == AutoridadPlantel::model()->validarUniqueEmail($usuario['email'])) {
            $mensaje .= "Este email ya esta registrado <br>";
        }

        if ($usuario['cargo'] == null || $usuario['cargo'] == '') {
            $mensaje .= "El campo Cargo no puede estar vacio <br>";
        } elseif ($usuario['cargo'] == AutoridadPlantel::model()->validarExisteCargo($usuario['cargo'], $usuario['plantel_id'])) {
            $mensaje .= "Este cargo ya fue asignado. <br>";
        }

        if ($usuario['cedula'] == AutoridadPlantel::model()->validarUniqueUsuario($usuario['cedula'])) {
            $mensaje .= "Este Usuario esta registrado. <br>";
        }

        if ($mensaje == "") {

            return null;
        } else
            return $mensaje;
    }

    public function actionSeleccionarMunicipio() {

        if (array_key_exists('estado_id', $_REQUEST))
            $item = $_REQUEST ['estado_id'];
        elseif (array_key_exists('estado_id', $_REQUEST['Plantel']))
            $item = $_REQUEST['Plantel']['estado_id'];


        if ($item == '' || $item == NULL) {
            $lista = array('empty' => '-SELECCIONE-');

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            //$lista = Municipio::model()->findAll(array('condition' => 'estado_id =' . $item, 'order' => 'nombre ASC'));
            $lista = CMunicipio::getData('estado_id', "{$item}");
            $lista = CHtml::listData($lista, 'id', 'nombre');
            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    public function actionSeleccionarParroquia() {
        if (array_key_exists('municipio_id', $_REQUEST))
            $item = $_REQUEST ['municipio_id'];
        elseif (array_key_exists('municipio_id', $_REQUEST['Plantel']))
            $item = $_REQUEST['Plantel']['municipio_id'];

        if ($item == '' || $item == NULL) {
            $lista = array('empty' => '-SELECCIONE-');

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            //$lista = Parroquia::model()->findAll(array('condition' => 'municipio_id =' . $item, 'order' => 'nombre ASC'));
            $lista = CParroquia::getData('municipio_id', "{$item}");
            $lista = CHtml::listData($lista, 'id', 'nombre');
            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);


            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    /*     * *********************ALEXIS EDITANDO *********************************************** */

    public function actionSeleccionarPoblacion() {
        if (array_key_exists('parroquia_id', $_REQUEST))
            $item = $_REQUEST ['parroquia_id'];
        elseif (array_key_exists('parroquia_id', $_REQUEST['Plantel']))
            $item = $_REQUEST['Plantel']['parroquia_id'];

        if ($item == '' || $item == NULL) {
            $lista = array('empty' => '-SELECCIONE-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $lista = Plantel::model()->obtenerPoblacion($item);
            $lista = CHtml::listData($lista, 'id', 'nombre');
            //$data = CJSON::encode(Plantel::model()->obtenerPoblacion($item)); echo "$data";

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    public function actionSeleccionarUrbanizacion() {
        if (array_key_exists('parroquia_id', $_REQUEST))
            $item = $_REQUEST ['parroquia_id'];
        elseif (array_key_exists('parroquia_id', $_REQUEST['Plantel']))
            $item = $_REQUEST['Plantel']['parroquia_id'];
        //$item=$_REQUEST['parroquia_id'];

        if ($item == '' || $item == NULL) {
            $lista = array('empty' => '-SELECCIONE-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $lista = Plantel::model()->obtenerUrbanizacion($item);
            $lista = CHtml::listData($lista, 'id', 'nombre');
            //$data = CJSON::encode(Plantel::model()->obtenerPoblacion($item)); echo "$data";

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    /* ESTE CODIGO ES DESARROLLADO POR ENRIQUEX00 */

    public function actionInformacionAula($id) {
        $this->renderPartial('informacionAula', array(
            'model' => $this->loadAula($id),
        ));
    }

    public function actionRegistrarAula($id) {
        $model = new Aula;
        $loadModelPlantel = $this->loadModel($id);
        $condicionInfraestructura = CondicionInfraestructura::model()->findAll(array('order' => 'id ASC'));
        $tipoAula = TipoAula::model()->findAll(array('order' => 'id ASC'));
        $this->renderPartial('_formRegistrarAula', array(
            'model' => $model,
            'modelPlantel' => $loadModelPlantel,
            'modalidad_id' => $this->loadModalidad($loadModelPlantel['modalidad_id']),
            'condicionInfraestructura' => $condicionInfraestructura,
            'tipoAula' => $tipoAula,
        ), FALSE, TRUE);
    }

    public function actionCrearAula() {
        $model = new Aula;

        if (isset($_POST['Aula'])) {
            $id = $_POST['Aula']['plantel_id'];
            $loadModelPlantel = $this->loadModel($id);
            $condicionInfraestructura = CondicionInfraestructura::model()->findAll(array('order' => 'id ASC'));
            $tipoAula = TipoAula::model()->findAll(array('order' => 'id ASC'));

            $model->attributes = $_POST['Aula'];
            $model->usuario_act_id = Yii::app()->user->id;
            $model->usuario_ini_id = Yii::app()->user->id;
            $model->fecha_ini = date("Y-m-d H:i:s");
            $model->fecha_act = date("Y-m-d H:i:s");
            $model->estatus = 'A';

            $capacidad = $model->capacidad;
            if ($capacidad > 50) {
                $model->capacidad = 50;
            }

            /* VALIDO LOS SCRIPTS O ETIQUETAS */
            $model->nombre = str_replace('<', '< ', $model->nombre);
            $model->observacion = str_replace('<', '< ', $model->observacion);

            $nombre = $model->nombre;
            $nombre = trim(strtoupper($nombre));
            $model->nombre = $nombre;

            $observacion = $model->observacion;
            $observacion = trim(strtoupper($observacion));
            $model->observacion = $observacion;

            /* VALIDO QUE EL PLANTEL NO SE REPITAN LOS AULAS */


            if ($model->validate()) {
                $resultado = $model->validarAula($model->plantel_id, $model->nombre);
                if ($resultado == null) {
                    if ($model->save()) {
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Registro Exitoso.'));
                        $this->registrarTraza('Registro un aula en el plantel' . $id, 'CrearAula');
                    }
                } else {
                    $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'Este aula ya esta asignado a este plantel.'));
                }
            } else { // si ingresa datos erroneos muestra mensaje de error.
                $this->renderPartial('//errorSumMsg', array('model' => $model));
                //Yii::app()->end();
            }
        }
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_formRegistrarAula', array(
            'model' => $model,
            'modelPlantel' => $loadModelPlantel,
            'modalidad_id' => $this->loadModalidad($loadModelPlantel['modalidad_id']),
            'condicionInfraestructura' => $condicionInfraestructura,
            'tipoAula' => $tipoAula,
        ));
    }

    public function actionModificarAula($id) {
        $model = $this->loadAula($id);
        $loadModelPlantel = $this->loadModel($model['plantel_id']);
        $condicionInfraestructura = CondicionInfraestructura::model()->findAll(array('order' => 'id ASC'));
        $tipoAula = TipoAula::model()->findAll(array('order' => 'id ASC'));
        $this->renderPartial('_formRegistrarAula', array(
            'model' => $model,
            'modelPlantel' => $loadModelPlantel,
            'modalidad_id' => $this->loadModalidad($loadModelPlantel['modalidad_id']),
            'condicionInfraestructura' => $condicionInfraestructura,
            'tipoAula' => $tipoAula,
        ), FALSE, TRUE);
    }

    public function actionProcesarCambioAula() {

        $model = new Aula;

        if (isset($_POST['Aula'])) {
            $id = $_POST['Aula']['plantel_id'];
            $aula_id = $_POST['Aula']['id'];
            $loadModelPlantel = $this->loadModel($id);
            $model = Aula::model()->findByPk($aula_id);
            $condicionInfraestructura = CondicionInfraestructura::model()->findAll(array('order' => 'id ASC'));
            $tipoAula = TipoAula::model()->findAll(array('order' => 'id ASC'));
            $model->attributes = $_POST['Aula'];
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");

            $capacidad = $model->capacidad;
            if ($capacidad > 50) {
                $model->capacidad = 50;
            }

            /* VALIDO LOS SCRIPTS O ETIQUETAS */
            $model->nombre = str_replace('<', '< ', $model->nombre);
            $model->observacion = str_replace('<', '< ', $model->observacion);

            $nombre = $model->nombre;
            $nombre = trim(strtoupper($nombre));
            $model->nombre = $nombre;

            $observacion = $model->observacion;
            $observacion = trim(strtoupper($observacion));
            $model->observacion = $observacion;

            if ($model->validate()) {

                $resultado = $model->validarAula($model->plantel_id, $model->nombre);
                if ($resultado == null) {
                    if ($model->save()) {

                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Actualizado con exito.'));
                        $this->registrarTraza('Modifico el Aula' . $aula_id . ' al plantel ' . $id, 'ProcesarCambioAula');
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                } else {
                    $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'Este aula ya esta asignado a este plantel.'));
                }
            } else { // si ingresa datos erroneos muestra mensaje de error.
                $this->renderPartial('//errorSumMsg', array('model' => $model));
                //Yii::app()->end();
            }
        } else {
            Yii::app()->user->setFlash('error', "No se ha podido completar la última operación, ID invalido.");
        }

        $this->renderPartial('_formRegistrarAula', array(
            'model' => $model,
            'modelPlantel' => $loadModelPlantel,
            'modalidad_id' => $this->loadModalidad($loadModelPlantel['modalidad_id']),
            'condicionInfraestructura' => $condicionInfraestructura,
            'tipoAula' => $tipoAula,
        ));
    }

    public function actionEliminarAula($id) {
        //$model = $this->loadModel($id);
        $model = Aula::model()->findByPk($id);
        $model->fecha_elim = date("Y-m-d H:i:s");
        $model->estatus = 'E';

        if ($model->validate()) {

            if ($model->save()) {

                $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Eliminado con exito.'));
                $this->registrarTraza('Elimino el Aula ' . $id, 'EliminarAula');
            } else {
                throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
            }
        }
    }

    public function actionObtenerFormDependencia() {
        if (Yii::app()->request->isAjaxRequest) {
            $plantel_id = base64_decode($this->getQuery('plantel_id'));
            if (is_numeric($plantel_id)) {
                $modelDependencia = new DependenciaNominalPlantel();
                $modelDependencia->plantel_id = $plantel_id;
                $this->renderPartial('_formDependenciaNominal', array('modelDependencia' => $modelDependencia, 'plantel_id' => $plantel_id), false, true);
                Yii::app()->end();
            } else {
                throw new CHttpException(404, 'Recurso no encontrado. Datos incompletos.');
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function columnaAcciones($data)
        /*
         * Botones del accion (crear, consultar)
         */ {
        $id = $data["id"];
        $estatus = $data["estatus"];
        if (($estatus == 'A') || ($estatus == '')) {
            $columna = CHtml::link("", "#", array("class" => "fa fa-search", "onClick" => "consultarAula($id,'')", "title" => "Consultar este aula")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-pencil green", "onClick" => "modificarAula($id)", "title" => "Modificar aula")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-times red remove-data", "onClick" => "eliminarAula($id)", "title" => "Eliminar aula")) . '&nbsp;&nbsp;';
        } else if ($estatus == 'E') {
            $columna = CHtml::link("", "#", array("class" => "fa fa-search", "onClick" => "consultarAula($id,'')", "title" => "Consultar este aula")) . '&nbsp;&nbsp;';
        }

        return $columna;
    }




    public function columnaEstatus($data) {
        $estatus = $data['estatus'];
        if (($estatus == 'A') || ($estatus == '')) {
            return 'Activo';
        } else if ($estatus == 'E') {
            return 'Inactivo';
        }
        return $estatus;
    }

    /* FIN DEL CODIGO DESARROLLADO POR ENRIQUEX00 */
    /*
   *      CODIGO DE CONGRESO PEDAGOGICO --------
   */

    public function columnaAccionesD($data) //RENDERIZA LA COLUMNA DE ACCIONES EN EL GRIDVIEW docente-grid 
        /*
         * Botones del accion (eliminar, editar inscritos)
        */
    {
        $id = $data["id"];
        $estatus = $data["estatus"];
        if (($estatus == 'A') || ($estatus == '')) {
            $columna  = CHtml::link("", "#", array("class" => "fa fa-pencil green", "onClick" => "editarDocente($id)", "title" => "Modificar docente")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-times red remove-data", "onClick" => "eliminarDocente($id)", "title" => "Eliminar docente")) . '&nbsp;&nbsp;';
        } else if ($estatus == 'E') {
            $columna = CHtml::link("", "#", array("class" => "fa fa-check green", "onClick" => "activarDocente($id)", "title" => "Activar este docente")) . '&nbsp;&nbsp;';
        }
        return $columna;
    }

    public function actionAgregarDocente($id){ //CARGA UN MODELO VACIO Y RENDERIZA LA VENTANA EMERGENTE CON EL FORMULARIO PARA AGREGAR NUEVO DOCENTE EN CONGRESO PEDAGOGICO
        $model=new CongresoPedagogicoDocente;
        $congreso = $id;

        $this->renderPartial('agregarDocentes', array(
            'model' => $model,
            'congreso' => $congreso,
        ));
    }

    public function actionGuardarDocente(){ //VALIDA Y GUARDA LOS DATOS DEL NUEVO DOCENTE EN EL CONGRESO PEDAGOGICO SELECCIONADO

        $model=new CongresoPedagogicoDocente;

        if($_POST['CongresoPedagogicoDocente']['nombres'] == null || $_POST['CongresoPedagogicoDocente']['nombres'] == ''){
            $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'Error! no puede guardar en el sistema un registro vacio.'));
            Yii::app()->end();
        }

        if (isset($_POST['CongresoPedagogicoDocente'])) {
            $model->attributes=$_POST['CongresoPedagogicoDocente'];
            $model->documento_identidad = $_POST['documento_identidad'];
            $model->usuario_ini_id = Yii::app()->user->id ;
            $model->fecha_ini = date('d-m-Y H:i:s');
            
            if ($model->validate()) {
                if ($model->save()) {
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Guardado con exito.'));
                    $this->registrarTraza('Guardo el Docente ' . $model->id, 'GuardarDocente');
                    Yii::app()->end();
                } else {
                    $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'Error! no se ha completa la operación comuniquelo al administrador del sistema.'));
                    Yii::app()->end();
                }
            }
            else {
                $this->renderPartial("//errorSumMsg", array('model'=>$model));
                Yii::app()->end();
            }
        }



    }


    public function actionUpdateDocente(){ // VALIDA Y GUARDA LOS CAMBIOS REALIZADOS EN EL REGISTRO DEL DOCENTE SELECCIONADO
        $id = $_POST['CongresoPedagogicoDocente']['id'];

        $model=$this->loadCongresoDocente($id);

        if($_POST['CongresoPedagogicoDocente']['nombres'] == null || $_POST['CongresoPedagogicoDocente']['nombres'] == ''){
            $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'Error! no puede guardar en el sistema un registro vacio.'));
            Yii::app()->end();
        }

        if (isset($_POST['CongresoPedagogicoDocente'])) {
            $model->attributes=$_POST['CongresoPedagogicoDocente'];
            $model->documento_identidad = $_POST['documento_identidad'];
            if ($model->validate()) {
                if ($model->update()) {
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Editado con exito.'));
                    $this->registrarTraza('Edito el Docente ' . $id, 'EditarDocentes');
                    Yii::app()->end();
                } else {
                    $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'Error! no se ha completa la operación comuniquelo al administrador del sistema.'));
                    Yii::app()->end();
                }
            }
            else {
                $this->renderPartial("//errorSumMsg", array('model'=>$model));
                Yii::app()->end();
            }
        }

    }

     public function actionModificarDocentes($id) { //ENVIA A LA VENTANA DONDE SE LISTAN LOS DOCENTES INSCRITOS EN EL CONGRESO

        $this->render('modificarDocentes', array(
            'model' => $this->loadCongreso(base64_decode($id)),
        ));
    }
    public function actionEditarDocente($id) { //RENDERIZA LA VENTANA EMERGENTE PARA EDITAR LOS DATOS DEL DOCENTE INSCRITO

        $this->renderPartial('editarDocentes', array(
            'model' => $this->loadCongresoDocente($id),
        ));

    }

    public function actionEliminarDocente($id) { //CAMBIA EL ESTATUS DEL DOCENTE INSCRITO DE "ACTIVO" A "ELIMINADO"

        $model = CongresoPedagogicoDocente::model()->findByPk($id);
        $model->fecha_elim = date("Y-m-d H:i:s");
        $model->estatus = 'E';

        if ($model->validate()) {

            if ($model->save()) {

                $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Eliminado con exito.'));
                $this->registrarTraza('Elimino el Congreso ' . $id, 'EliminarDocente');
            } else {
                throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
            }
        }
    }



    public function actionActivarDocente($id) { //CAMBIA EL ESTATUS DEL DOCENTE INSCRITO DE "ELIMINADO" A "ACTIVO"
        //$model = $this->loadModel($id);
        $model = CongresoPedagogicoDocente::model()->findByPk($id);
        $model->fecha_elim = date("Y-m-d H:i:s");
        $model->estatus = 'A';

        if ($model->validate()) {

            if ($model->save()) {

                $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Activación exitosa.'));
                $this->registrarTraza('Activó el Congreso ' . $id, 'ActivarCongreso');
            } else {
                throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
            }
        }
    }


    public function actionCompletarCampos(){ //ACCION QUE AUTOCOMPLETA LOS CAMPOS DE LOS FORMULARIOS PARA AGREGAR/EDITAR DOCENTES EN CONGRESO PEDAGOGICO
        $cedula = $_REQUEST['cedula'];
        $tdocumento = $_REQUEST['tipo_documento'];
        $congreso =  $_REQUEST['congreso_id'];

        if ($cedula == null || $cedula == ''){
            echo json_encode(array('status'=>'vacio',));
        }else{

        $datos = CongresoPedagogicoDocente::model()->datosInscritos($cedula, $tdocumento);
        $inscrito = CongresoPedagogicoDocente::model()->docentesInscritosVal($congreso, $cedula);
        if($datos['primer_nombre'] == null && $datos['primer_apellido'] == null){

            echo json_encode(array('status'=>'error',));

        }else{
            if ($datos['cedula'] == $inscrito['documento_identidad']){

                echo json_encode(array('status'=>'mensaje',));
            }else{
                $datos['primer_nombre'] = strtoupper($datos['primer_nombre']);
                $datos['segundo_nombre'] = strtoupper($datos['segundo_nombre']);
                $datos['primer_apellido'] = strtoupper($datos['primer_apellido']);
                $datos['segundo_apellido'] = strtoupper($datos['segundo_apellido']);
                echo json_encode(array('status'=>'success','tipo_documento'=> $datos['cedula'],'primer_nombre'=> $datos['primer_nombre'],'segundo_nombre'=> $datos['segundo_nombre'],
                'primer_apellido'=> $datos['primer_apellido'],'segundo_apellido'=> $datos['segundo_apellido'],
                'tipo_documento'=> $datos['origen']));

            }
         
        }
        }
    }




    public function columnaAccionesCp($data) //RENDERIZA LA COLUMNA DE ACCIONES EN EL GRIDVIEW cong-ped-grid
        /*
         * Botones del accion (consultar, editar, eliminar, editar inscritos)
        */
    {
        $id = $data["id"];
        $estatus = $data["estatus"];
        if (($estatus == 'A') || ($estatus == '')) {
            $columna = CHtml::link("", "#", array("class" => "fa fa-search", "onClick" => "consultarCongresoP($id)", "title" => "Consultar este congreso")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-pencil green", "onClick" => "editarCongresoP($id)", "title" => "Modificar este congreso")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "/planteles/modificar/modificarDocentes/id/".base64_encode($id), array("class" => "fa fa-user green ", /*"onClick" => "modificarDocentes($id)",*/ "title" => "Modificar docentes de este congreso")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-times red remove-data", "onClick" => "eliminarCongreso($id)", "title" => "Eliminar congreso")) . '&nbsp;&nbsp;';
        } else if ($estatus == 'E') {
            $columna = CHtml::link("", "#", array("class" => "fa fa-search", "onClick" => "consultarCongresoP($id)", "title" => "Consultar este congreso")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-check green", "onClick" => "activarCongreso($id)", "title" => "Activar este congreso")) . '&nbsp;&nbsp;';
        }
        return $columna;
    }

    public function actionInformacionCongreso($id) { //RENDERIZA LA VENTANA EMERGENTE PARA MOSTRAR LA INFORMACION DEL CONGRESO
        $this->renderPartial('informacionCongreso', array(
            'model' => $this->loadCongreso($id),
        ));
    }


    public function actionUpdateCongreso(){ //GUARDA LOS CAMBIOS REALIZADOS EN LA INFORMACION DEL CONGRESO
        $id = $_POST['CongresoPedagogico']['id'];
        $model=$this->loadCongreso($id);
        if (isset($_POST['CongresoPedagogico'])) {
            if ($model->validate()) {
                $model->attributes=$_POST['CongresoPedagogico'];
                if ($model->save()) {
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Editado con exito.'));
                    $this->registrarTraza('Edito el Congreso ' . $id, 'EditarCongreso');
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            }
        }


    }
    public function actionEditarCongreso($id) { //RENDERIZA LA VENTANA EMERGENTE PARA EDITAR CONGRESO
        $this->renderPartial('editarCongreso', array(
            'model' => $this->loadCongreso($id),
        ));
    }

    public function actionEliminarCongreso($id) { //CAMBIAR ESTATUS DE CONGRESO DE "ACTIVO" A "ELIMINADO"
        //$model = $this->loadModel($id);
        $model = CongresoPedagogico::model()->findByPk($id);
        $model->fecha_elim = date("Y-m-d H:i:s");
        $model->estatus = 'E';
        if ($model->validate()) {
            if ($model->save()) {
                $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Eliminado con exito.'));
                $this->registrarTraza('Elimino el Congreso ' . $id, 'EliminarCongreso');
            } else {
                throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
            }
        }
    }



    public function actionActivarCongreso($id) { //CAMBIAR ESTATUS DE CONGRESO DE "ELIMINADO" A "ACTIVO"
        //$model = $this->loadModel($id);
        $model = CongresoPedagogico::model()->findByPk($id);
        $model->fecha_elim = date("Y-m-d H:i:s");
        $model->estatus = 'A';

        if ($model->validate()) {

            if ($model->save()) {

                $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Activación exitosa.'));
                $this->registrarTraza('Activó el Congreso ' . $id, 'ActivarCongreso');
            } else {
                throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
            }
        }
    }

    /*
    *   FIN CONGRESO PEDAGOGICO ----------
    */








    public function actionSeleccionarTipoVia() {
        $item = $_REQUEST['Plantel']['parroquia_id'];


        if ($item == '' || $item == NULL) {
            $lista = array('empty' => '-SELECCIONE-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $lista = Plantel::model()->obtenerTipoVia($item);
            $lista = CHtml::listData($lista, 'id', 'nombre');
            //$data = CJSON::encode(Plantel::model()->obtenerPoblacion($item)); echo "$data";

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    public function actionViaAutoComplete() {
        $id = $_GET["id"];


        $_GET['term'] = strtoupper($_GET['term']);
        $res = array();

        if (isset($_GET['term']) && !empty($id)) {

            $res = Plantel::obtenerVia($id, $_GET['term']);
        }

        echo CJSON::encode($res);
    }

    /*     * ******************************************************************************************************* */
    /* static function generarLetra() {
      //Se define una cadena de caractares. Te recomiendo que uses esta.
      $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      //Obtenemos la longitud de la cadena de caracteres
      $longitudCadena = strlen($cadena);

      //Se define la variable que va a contener la contraseña
      $pass = "";
      //Se define la longitud de la contraseña, en mi caso 10, pero puedes poner la longitud que quieras
      $longitudPass = 1;

      //Creamos la contraseña
      for ($i = 1; $i <= $longitudPass; $i++) {
      //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
      $pos = rand(0, $longitudCadena - 1);

      //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
      $pass .= substr($cadena, $pos, 1);
      }
      return $pass;
      }
     *
     */

    public static function generarLetraFromCedula($cedula) {

        if (is_numeric($cedula)) {
            $numero = $cedula;
        } else {
            $numero = substr($cedula, 2);
        }

        $letra = substr("TRWAGMYFPDXBNJZSQVHLCKE", strtr($numero, "XYZ", "012") % 23, 1);

        return $letra;
    }

    static function registrarTraza($transaccion, $accion) {
        $Utiles = new Utiles();
        $fecha = date('Y-m-d H:i:s');
        $modulo = "Planteles.ModificarController." . $accion;
        $Utiles->traza($transaccion, $modulo, $fecha);
    }

// Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */

    public function actionActualizarGridCongresos(){
        if($this->hasRequest('ajax') AND $this->hasRequest('plantel_id')){
            $model = new CongresoPedagogico();
            $congreso_docente = new CongresoPedagogicoDocente();
            $plantel_id = $this->getQuery('plantel_id');
            $model->plantel_id = base64_decode($plantel_id);
            $this->render(
                '_formCongresoPedagogico',
                array(
                    'congreso'=>$model,
                    'congreso_docente'=>$congreso_docente,
                    'plantel_id'=>$plantel_id
                )
            );
        }
        else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionUpload($id) 
    {
        $upload_handler = new UploadHandler(null, true, null, date('YmdHis') . 'MP', "/public/uploads/LogoPlanteles/");
    }
    public function getNacionalidad($data)
    {
        $tdocumento_identidad = $data["tdocumento_identidad"];

        if($tdocumento_identidad==Constantes::NAC_VENEZOLANO)
        {
            return Constantes::DESC_NAC_VENEZOLANO;
        }
        if($tdocumento_identidad==Constantes::NAC_EXTRANJERO)
        {
            return Constantes::DESC_NAC_EXTRANJERO;
        }
        if($tdocumento_identidad==Constantes::NAC_PASAPORTE)
        {
            return Constantes::DESC_NAC_PASAPORTE;
        }

        return Constantes::DESC_NAC_OTRO;


    } // FIN DE LA FUNCION PARA OBTENER LA NACIONALIDAD

    public function estatus($data)
    {
        $estatus = $data["estatus"];
        $columna="";

        if($estatus==Constantes::ESTATUS_INACTIVO)
        {
            $columna=Constantes::DESC_ESTATUS_INACT;
        }
        else if($estatus==Constantes::ESTATUS_ACTIVO)
        {
            $columna=Constantes::DESC_ESTATUS_ACT;
        }
        return $columna;
    }
    
    public function columnaAccionesModalidad($data)
    {
        $id = $data["id"];
        $columna = CHtml::link("", "", array("class" => "fa fa-search verDatosModalidad", "data" => base64_encode($id), "title" => "ver Datos")) . '&nbsp;&nbsp;';
        if($data->estatus=='A')
        { 
           if(in_array(Yii::app()->user->group, array(UserGroups::ADMIN_0,  UserGroups::JEFE_DRCEE,  UserGroups::ADMIN_REG_CONTROL) ))
                {
            $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green EditarDatosModalidad", "data" => base64_encode($id), "title" => "Editar Datos")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("data" => base64_encode($id), "class" => "fa fa-trash-o red remove-data InactivarDatosModalidad", "style" => "color:#555;", "title" => "Inactivar Datos"));
                }
        }
        else
        {
            if(in_array(Yii::app()->user->group, array(UserGroups::ADMIN_0,  UserGroups::JEFE_DRCEE,  UserGroups::ADMIN_REG_CONTROL) ))
            {
            $columna .= CHtml::link("", "", array("data"=>  base64_encode($id), "class" => "fa icon-ok green remove-data activarDatosModalidad", "style" => "color:#555;", "title" => "Activar Datos"));
            }
        }
         return $columna;
    }
    
    public function actionMostrarFormModalidad() {
        $plantel_id= $_REQUEST['plantel_id'];
        $modelPlantelModalidad=new PlantelModalidad();
        $modalidad= PlantelModalidad::model()->ObtenerPlantelModalidad( base64_decode($plantel_id) );
        if($modalidad=='' || $modalidad==array())
        {
            $mensaje="Estimado Usuario, No Existen Modalidades Disponibles. Comuniquese con el Administrador del Sistema";
            echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
        }
        else
        {
            if(is_numeric(base64_decode($plantel_id))==false)
            {
                $mensaje="Ocurrio Un error con el Código de Plantel, recargue e intente Nuevamente.";
                echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
            }
            else{
                        if($_GET)
                        {
                            $this->renderPartial('_formPlantelModalidad',array('modelPlantelModalidad'=>$modelPlantelModalidad,'plantel_id'=>$plantel_id,'modalidad'=>$modalidad),false,true);
                        }
                }
        }
    }
    
    public function actionGuardarPlantelModalidad()
    {
        if($_POST)
        {
            $modelPlantelModalidad=new PlantelModalidad();
            $mensaje='';
            $_POST['PlantelModalidad']['plantel_id']= base64_decode($_POST['PlantelModalidad']['plantel_id']);
            if(is_numeric($_POST['PlantelModalidad']['plantel_id']) == false)
            {
                $mensaje="Error al Traer Código del Plantel, Comuniquese con el Administrador del Sistema.";
                echo json_encode(array('statusCode'=>'error','mensaje'=>$mensaje));
            }else
            {
                $modelPlantelModalidad->attributes= $_POST['PlantelModalidad'];
                $modelPlantelModalidad->estatus='A';
                $modelPlantelModalidad->usuario_ini_id = Yii::app()->user->id;
                $modelPlantelModalidad->fecha_ini = date("Y-m-d H:i:s");
                
                if( $modelPlantelModalidad->validate() && $modelPlantelModalidad->save() )
                {
                    $mensaje='Registro Satisfactorio de la Modalidad';
                    echo json_encode(array('status'=>'success','mensaje'=>$mensaje));
                }else
                {
                    $this->renderPartial('//errorSumMsg', array('model' => $modelPlantelModalidad));
                }
            }
        }
    }
    
    public function actionActualizarGridPlantelModalidad()
    {
        if($this->hasRequest('ajax') AND $this->hasRequest('id') )
        {
            //print_r($_REQUEST);
            
            $plantel_id = $_REQUEST['id'];
            if(isset($_REQUEST['PlantelModalidad']['modalidad_id']) || isset($_REQUEST['PlantelModalidad']['estatus']))
            {
            $modalidad_id= $_REQUEST['PlantelModalidad']['modalidad_id'];
            $estatus= $_REQUEST['PlantelModalidad']['estatus'];
            }
            
            $modelPlantelModalidad = new PlantelModalidad('search');
            $modelPlantelModalidad->plantel_id = base64_decode($plantel_id);
            
            if(isset($_REQUEST['PlantelModalidad']['modalidad_id']) || isset($_REQUEST['PlantelModalidad']['estatus']))
            {
            $modelPlantelModalidad->modalidad_id=$modalidad_id;
            $modelPlantelModalidad->estatus= $estatus;
            }
            $modalidad= PlantelModalidad::model()->ObtenerDatosPlantelModalidad( base64_decode($plantel_id) );
            
            $this->render(
                '_modalidad',
                array(
                    'modelPlantelModalidad'=>$modelPlantelModalidad,
                    'plantel_id'=>$plantel_id,
                    'modalidad'=>$modalidad,
                    'codPlantel'=>$plantel_id,
                     )
                        );
        }
        else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }
    
    public function actionVerDatosPlantelModalidad()
     {
        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        if (!is_numeric($id_decod)) 
        {
            $mensaje = 'No se ha encontrado el registro de Modalidad que ha solicitado para consultar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
                    $model = PlantelModalidad::model()->findByPk($id_decod);
                    if ($model != null) 
                    {
                        $this->renderPartial('detallesModalidad', array('model' => $model), false, true);
                    } else {
                        $mensaje = 'No se ha encontrado el registro de Modalidad que ha solicitado para consultar. Recargue la página e intentelo de nuevo. </br>';
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                    }
               }
     }
     
     public function actionMostrarDatosPlantelModalidad() {

        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $plantel_id= $_REQUEST['plantel_id'];
        $mensaje = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro de Modalidad que ha solicitado para modificar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $modelPlantelModalidad = PlantelModalidad::model()->findByPk($id_decod);
            $modelPlantelModalidad->id = base64_encode($modelPlantelModalidad->id);
            $modalidad= PlantelModalidad::model()->ObtenerPlantelModalidad( base64_decode($plantel_id) );
            
            if($modalidad=='' || $modalidad==array())
            {
                $mensaje="Estimado Usuario, No Existen más Modalidades para Editar. Comuniquese con el Administrador del Sistema";
                echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
            }
            else
            {
            array_push($modalidad,array( 'id'=>$modelPlantelModalidad->modalidad_id,'nombre' => $modelPlantelModalidad->modalidad->nombre ) );

            if ($modelPlantelModalidad != null) {
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                //$this->renderPartial('_form', array('model' => $model), false, true);
                $this->renderPartial('_formPlantelModalidad',array('modelPlantelModalidad'=>$modelPlantelModalidad,'plantel_id'=>$plantel_id,'modalidad'=>$modalidad),false,true);
            } else {
                $mensaje = 'No se ha encontrado el registro de Modalidad que ha solicitado para modificar. Recargue la página e intentelo de nuevo. </br>';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
            
            }
        }
    }
    
    public function actionModificarPlantelModalidad()
    {
        if($_POST['PlantelModalidad'])
        {
            $mensaje='';
            $_POST['PlantelModalidad']['plantel_id']= base64_decode($_POST['PlantelModalidad']['plantel_id']);
            $_POST['PlantelModalidad']['id']= base64_decode($_POST['PlantelModalidad']['id'] );
            if( is_numeric($_POST['PlantelModalidad']['plantel_id']) == false || is_numeric($_POST['PlantelModalidad']['id'])==false)
            {
                $mensaje="Error al Traer Código del Plantel o Código de Modalidad, Comuniquese con el Administrador del Sistema.";
                echo json_encode(array('statusCode'=>'error','mensaje'=>$mensaje));
            }else
            {
                $modelPlantelModalidad= PlantelModalidad::model()->findByPk($_POST['PlantelModalidad']['id']);
                $usuario_id = Yii::app()->user->id;
                $estatus = 'A';
                $fecha = date('Y-m-d H:i:s');
                $modelPlantelModalidad->modalidad_id = $_POST['PlantelModalidad']['modalidad_id'];
                $modelPlantelModalidad->estatus = $estatus;
                $modelPlantelModalidad->usuario_act_id = $usuario_id;
                $modelPlantelModalidad->fecha_act = $fecha;
                
                if( $modelPlantelModalidad->validate() && $modelPlantelModalidad->save() )
                {
                    $mensaje='Registro Satisfactorio de la Modalidad';
                    echo json_encode(array('status'=>'success','mensaje'=>$mensaje));
                }else
                {
                    $this->renderPartial('//errorSumMsg', array('model' => $modelPlantelModalidad));
                }
            }
        }
    }
    
    public function actionActivarModalidad() {
        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        $mensaje_exitoso = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro de la Modalidad que ha solicitado para activar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $modelPlantelModalidad = PlantelModalidad::model()->findByPk($id_decod);
            if ($modelPlantelModalidad != null) {
                $usuario_id = Yii::app()->user->id;
                $estatus = 'A'; //Estatus inactivado
                $fecha = date('Y-m-d H:i:s');
                $modelPlantelModalidad->estatus = $estatus;
                $modelPlantelModalidad->usuario_ini_id = $usuario_id;
                $modelPlantelModalidad->fecha_ini = $fecha;
                $modelPlantelModalidad->fecha_elim = null;
                $modelPlantelModalidad->usuario_act_id = $usuario_id;
                $modelPlantelModalidad->fecha_act = null;
                if ($modelPlantelModalidad->save()) {
                    $this->registerLog('ACTIVAR', 'planteles.modificar.ActivarModalidad', 'EXITOSO', 'Permite activar un registro de Modalidad asociado a un Plantel.');
                    $mensaje_exitoso = 'Activación exitosa de la Modalidad';
                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                } else { // error que no guardo
                    $mensaje.= 'No se pudo activar el registro de la Modalidad asociado al Plantel, Por favor intente nuevamente <br>';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                }
            } else {
                $mensaje = 'Por favor seleccione un registro del área de atención para activar';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }
    
    public function actionInactivarModalidad() {
        
        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        $mensaje_exitoso = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro de Modalidad que ha solicitado para inactivar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $modelPlantelModalidad = PlantelModalidad::model()->findByPk($id_decod);
            if ($modelPlantelModalidad != null) {
                $usuario_id = Yii::app()->user->id;
                $estatus = 'I'; //Estatus inactivado
                $fecha = date('Y-m-d H:i:s');
                $modelPlantelModalidad->estatus = $estatus;
                $modelPlantelModalidad->usuario_act_id = $usuario_id;
                $modelPlantelModalidad->fecha_elim = $fecha;
                //   var_dump($model);
                if ($modelPlantelModalidad->save()) {
                    $this->registerLog('INACTIVAR', 'planteles.Modificar.InactivarModalidad', 'EXITOSO', 'Permite inactivar un registro de Modalidad del Plantel');
                    $mensaje_exitoso = 'Inactivación exitosa de la Modalidad';
                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                } else { // error que no guardo
                    $mensaje.= 'No se pudo inactivar el registro de Modalidad';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                }
            } else {
                $mensaje = 'Por favor seleccione un registro de Modalidad para inactivar';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }

}
