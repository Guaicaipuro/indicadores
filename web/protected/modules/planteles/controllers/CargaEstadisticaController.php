<?php

class CargaEstadisticaController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';
    public $layout = '//layouts/column2';
    CONST PERIODO_ID = 15;
    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de CargaEstadisticaController',
        'write' => 'Creación y Modificación de CargaEstadisticaController',
        'admin' => 'Administración Completa  de CargaEstadisticaController',
        'label' => 'Módulo de CargaEstadisticaController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('index'),
                'pbac' => array('write'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    public function actionIndex()
    {
        $this->breadcrumbs = array(
            'Planteles' => array('consultar/'),
            'Carga de Estadisticas'
        );
        $i=0;
        $nuevo = true;
        $modelError= array();
        $models= array();
        $valid=true;
        if($this->hasQuery('id') and $this->hasQuery('nivel_id')){
            $id=$this->getIdDecoded($this->getQuery('id'));
            $nivel_id=$this->getIdDecoded($this->getQuery('nivel_id'));
            $usuario_id=Yii::app()->user->id;
            $fecha = date('Y-m-d H:i:s');
            if($this->hasPost('CargaEstadistica')){
                $modelos=$this->getPost('CargaEstadistica');


                $transaction = Yii::app()->db->beginTransaction();
                try {
                    foreach($modelos as $i=>$model)
                    {
                        $cargaEstadistica = new CargaEstadistica();
                        $cargaEstadistica->attributes=$model;
                        $cargaEstadistica->id=$model['id'];
                        if(is_null($cargaEstadistica->id) OR $cargaEstadistica->id==''){

                            $cargaEstadistica->id=null;
                            $cargaEstadistica->plantel_id=$id;
                            $cargaEstadistica->usuario_ini_id=$usuario_id;
                            $cargaEstadistica->fecha_ini=$fecha;
                            $cargaEstadistica->nivel_id=$nivel_id;
                            $cargaEstadistica->periodo_id=self::PERIODO_ID;

                        }
                        else {
                            $cargaEstadistica = $this->loadModel($cargaEstadistica->id);
                            $cargaEstadistica->attributes=$model;
                            $cargaEstadistica->usuario_act_id=$usuario_id;
                            $cargaEstadistica->fecha_act=$fecha;
                            $cargaEstadistica->nivel_id=$nivel_id;
                            $cargaEstadistica->periodo_id=self::PERIODO_ID;
                        }
                        $models[]=$cargaEstadistica;
                        if(!($cargaEstadistica->validate() AND $cargaEstadistica->save())){
                            $nuevo = false;
                            $modelError[]=$cargaEstadistica;
                            $valid = false;
                            //$transaction->rollback();
                            //break;
                        }
                    }

                    if($valid){
                        $transaction->commit();
                        $models = $this->loadModels($id,$nivel_id);
                        Yii:: app()->user->setFlash('exito', 'Registro Exitoso');
                    }
                    else {
                        $transaction->rollback();
                        Yii:: app()->user->setFlash('error', 'Actualización Fallida, intente nuevamente');
                    }

                } catch (Exception $ex) {
                    $valid = false;
                    Yii:: app()->user->setFlash('error', 'Actualización Fallida, intente nuevamente');
                    $transaction->rollback();
                }
            }

            else {
                $models = $this->loadModels($id,$nivel_id);
            }

            if(!$models){
                if($nivel_id==1){
                    $nuevo = true;
                    while($i++<3) {
                        $models[]=CargaEstadistica::model();
                    }
                }
                else {
                    $models[]=CargaEstadistica::model();
                }
            }
            else{
                $nuevo=false;
            }


            $dataPlantel = Plantel::obtenerDatosIdentificacion($id);
            //$tipoAtencion = TipoAtencion::obtenerTipoAtencion();

            if($dataPlantel){
                if($nivel_id==1){
                    $view='formularioInicial';
                }
                else {
                    $view='formularioPMT';
                    //$view='formularioInicial';
                }
                $this->render($view,array(
                        'models'=>$models,
                        'modelError'=>$modelError,
                        'nivel_id'=>$nivel_id,
                        'id'=>$id,
                        'nuevo'=>$nuevo,
                        'datosPlantel'=>$dataPlantel,
                    )
                );
            }
            else {
                throw new CHttpException(403,'Estimado usuario, no hemos podido encontrar los datos solicitados.');
            }

        }
        else {
            throw new CHttpException(404,'The requested page does not exist.');
        }

    }
    /**
     * Lists all models.
     */
    public function actionLista()
    {
        $model=new CargaEstadistica('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('CargaEstadistica')){
            $model->attributes=$this->getQuery('CargaEstadistica');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new CargaEstadistica('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('CargaEstadistica')){
            $model->attributes=$this->getQuery('CargaEstadistica');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro()
    {
        $model=new CargaEstadistica;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if($this->hasPost('CargaEstadistica'))
        {
            $model->attributes=$this->getPost('CargaEstadistica');
            $model->beforeInsert();
            if($model->save()){
                $this->registerLog('ESCRITURA', 'modulo.CargaEstadistica.registro', 'EXITOSO', 'El Registro de los datos de CargaEstadistica se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                Yii::app()->user->setFlash('success', 'El proceso de registro de los datos se ha efectuado exitosamente');
                $this->redirect(array('edicion','id'=>base64_encode($model->id),));
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if($this->hasPost('CargaEstadistica'))
        {
            $model->attributes=$this->getPost('CargaEstadistica');
            $model->beforeUpdate();
            if($model->save()){
                $this->registerLog('ACTUALIZACION', 'modulo.CargaEstadistica.edicion', 'EXITOSO', 'La Actualización de los datos de CargaEstadistica se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La actualización de los Datos se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Logical Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);

        if($model){
            $model->beforeDelete();
            if($model->save()){
                $this->registerLog('ELIMINACION', 'modulo.CargaEstadistica.eliminacion', 'EXITOSO', 'La Eliminación de los datos de CargaEstadistica se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La eliminación del registro se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }

        $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
    }

    /**
     * Activation of a particular model Logicaly Deleted.
     * If activation is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be activated
     */
    public function actionActivacion($id){
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        if($model){
            $model->beforeActivate();
            if($model->save()){
                $this->registerLog('ACTIVACION', 'modulo.CargaEstadistica.activacion', 'EXITOSO', 'La Activación de los datos de CargaEstadistica se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La activación de este registro se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }
        $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return CargaEstadistica the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if(is_numeric($id)){
            $model=CargaEstadistica::model()->findByPk($id);
            if($model===null){
                throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
        }
        else{
            return null;
        }
    }
    public function loadModels($id,$nivel_id)
    {
        if(is_numeric($id) AND is_numeric($nivel_id)){
            $models=CargaEstadistica::model()->findAllByAttributes(array('plantel_id' =>$id, 'nivel_id' => $nivel_id));
            return $models;
        }
        else{
            return null;
        }
    }

    public function loadModelPlantel($id)
    {
        if(is_numeric($id)){
            $model=Plantel::model()->findByPk($id);
            if($model===null){
                throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
        }
        else{
            return null;
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CargaEstadistica $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='carga-estadistica-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $id = base64_encode($id_encoded);
        $columna = '<div class="action-buttons">';
        $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/planteles/cargaEstadistica/consulta/id/'.$id)) . '&nbsp;&nbsp;';
        $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '/planteles/cargaEstadistica/edicion/id/'.$id)) . '&nbsp;&nbsp;';
        if($data->estatus=='A'){
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Eliminar", 'href' => '/planteles/cargaEstadistica/eliminacion/id/'.$id)) . '&nbsp;&nbsp;';
        }else{
            $columna .= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar", 'href' => '/planteles/cargaEstadistica/activacion/id/'.$id)) . '&nbsp;&nbsp;';
        }
        $columna .= '</div>';
        return $columna;
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }
}
