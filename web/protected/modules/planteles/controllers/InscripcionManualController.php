<?php

class InscripcionManualController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    const MODULO = "Planteles.InscripcionManual";

    public $defaultAction = 'index';
    static $_permissionControl = array(
        'read' => 'Consulta de Inscripcion Manual',
        'write' => 'Asignación de Inscripcion Manual',
        'admin' => 'Asignación y Eliminación de Inscripcion Manual',
        'label' => ' Inscripcion Manual'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
//'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'autoComplete', 'obtenerDatosPersona', 'buscarNombrePlantel', 'columnaEstatusInscripcion', 'view'),
                'pbac' => array('read', 'write', 'admin'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'modificar', 'validarEstudiante'),
                'pbac' => array('write', 'admin'),
            ),
//            array('allow',
//                'actions' => array('index','modificar'),
//                'pbac' => array('admin'),
//            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex($id) {

        $plantel_id = base64_decode($id);
        $model = new InscripcionManual('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['InscripcionManual']))
            $model->attributes = $_GET['InscripcionManual'];

        $model->plantel_id = $plantel_id;

        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function actionView() {

        $id = '';
        $plantel_id = '';
        $nombre = '';
        if (isset($_GET['id'])) {
            $id = base64_decode($_GET['id']);
            $plantel_id = base64_decode($_GET['plantel']);
        }
        if (is_numeric($id) && is_numeric($plantel_id) && $id != '' && $plantel_id != '') {

            $model = InscripcionManual::model()->findByPk($id);
            if ($model != null) {
                $estatus_inscripcion = $model['estatus_inscripcion'];

                switch ($estatus_inscripcion) {
                    case 'V':
                        $nombre = 'Falta Verificar';
                        break;
                    case 'M':
                        $nombre = 'Matriculado';
                        break;
                    case 'P':
                        $nombre = 'Pendiente por Validar';
                        break;
                    case 'L':
                        $nombre = 'Listo para Matricular';
                        break;
                }
            } else {
                $model = new InscripcionManual;
            }

            $afinidad = Afinidad::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A'"));
            $grado = Grado::model()->findAll(array('order' => 'consecutivo ASC', 'condition' => "estatus='A'"));
            $seccion = Seccion::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A'"));
            $turno = Turno::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A'"));
            $estado = CEstado::getData(array('order' => 'nombre ASC'));
            $this->render('view', array(
                'model' => $model,
                'id' => $id,
                'plantel_id' => $plantel_id,
                'afinidad' => $afinidad,
                'estado' => $estado,
                'grado' => $grado,
                'seccion' => $seccion,
                'turno' => $turno,
                'nombre' => $nombre
            ));
        } else {
            throw new CHttpException(404, "Error, por favor recargue la página he intente nuevamente ya que no se encontro el registro que se requiere ver.");
        }
    }

    public function actionModificar() {

        $id = '';
        $plantel_id = '';
        $mensaje = '';
        $mensaje_error = '';
        $mensaje_exitoso = '';
        $msj = '';
        $div = '';
        $usuario_id = Yii::app()->user->id;
        $estatus = 'A';
        $fecha = date('Y-m-d H:i:s');
        $ip = Yii::app()->request->userHostAddress;
        $username = Yii::app()->user->name;
        $periodo_id = 14;
        $seccion_plantel_id = false;

        if (isset($_GET['id'])) {
            $id = base64_decode($_GET['id']);
            $plantel_id = base64_decode($_GET['plantel']);
        } elseif (isset($_REQUEST['InscripcionManual'])) {
            $id = base64_decode($_REQUEST['InscripcionManual']['id']);
            $plantel_id = base64_decode($_REQUEST['InscripcionManual']['plantel_id']);
        }
        if (is_numeric($id) && is_numeric($plantel_id) && $id != '' && $plantel_id != '') {

            $model = InscripcionManual::model()->findByPk($id);
            if ($this->hasRequest('InscripcionManual')) {
                $model->attributes = $this->getRequest('InscripcionManual');
                $cod_plantel = $model->cod_plantel = isset($_REQUEST['InscripcionManual']['cod_plantel']) ? $_REQUEST['InscripcionManual']['cod_plantel'] : '';
                $plantel = $model->plantel = base64_decode($_REQUEST['InscripcionManual']['plantel']);
                $plantel_id = $model->plantel_id = base64_decode($_REQUEST['InscripcionManual']['plantel_id']);
                $origen = $model->origen = isset($_REQUEST['InscripcionManual']['origen']) ? $_REQUEST['InscripcionManual']['origen'] : '';
                $documento_identidad = $model->documento_identidad = isset($_REQUEST['InscripcionManual']['documento_identidad']) ? trim($_REQUEST['InscripcionManual']['documento_identidad']) : '';
                $model->sexo = isset($_REQUEST['InscripcionManual']['sexo']) ? $_REQUEST['InscripcionManual']['sexo'] : '';
                $nombres = $model->nombres = isset($_REQUEST['InscripcionManual']['nombres']) ? trim($_REQUEST['InscripcionManual']['nombres']) : '';
                $apellidos = $model->apellidos = isset($_REQUEST['InscripcionManual']['apellidos']) ? trim($_REQUEST['InscripcionManual']['apellidos']) : '';
                $fecha_nacimiento = $model->fecha_nacimiento = isset($_REQUEST['InscripcionManual']['fecha_nacimiento']) ? $_REQUEST['InscripcionManual']['fecha_nacimiento'] : '';
                $model->afinidad_id = isset($_REQUEST['InscripcionManual']['afinidad_id']) ? $_REQUEST['InscripcionManual']['afinidad_id'] : null;
                $seccion = $model->seccion_id = isset($_REQUEST['InscripcionManual']['seccion_id']) ? $_REQUEST['InscripcionManual']['seccion_id'] : null;
                $grado = $model->grado_id = isset($_REQUEST['InscripcionManual']['grado_id']) ? $_REQUEST['InscripcionManual']['grado_id'] : null;
                $turno = $model->turno_id = isset($_REQUEST['InscripcionManual']['turno_id']) ? $_REQUEST['InscripcionManual']['turno_id'] : null;
                $model->estado_id = isset($_REQUEST['InscripcionManual']['estado_id']) ? $_REQUEST['InscripcionManual']['estado_id'] : null;
                $repitiente =  $model->repitiente_est = isset($_REQUEST['InscripcionManual']['repitiente_est'])? $_REQUEST['InscripcionManual']['repitiente_est']: 0;
                $estudiante_id = $model->estudiante_id;

                if ($model->validate()) {
//Valido documento de identidad del estudiante.
                    if ($origen == 'C') {//Es una cedula escolar.
                        if (strlen($documento_identidad) < 11 || strlen($documento_identidad) > 11) {
                            $mensaje .='<li>La <b>Cédula Escolar</b> debe contener un largo de 11 caracteres maximos, por favor revise los datos ingresados en el campo <b>Documento Identidad</b>.</li>';
                        }
                        if (!is_numeric($documento_identidad)) {
                            $mensaje .='<li>La <b>Cédula Escolar</b> debe contener solo números, por favor revise los datos ingresados en el campo <b>Documento Identidad</b>.</li>';
                        }
                    } elseif ($origen == 'V') {//Es una cedula venezolana.
                        if (strlen($documento_identidad) < 8 || strlen($documento_identidad) > 8) {
                            $mensaje .='<li>La <b>Cédula Venezolana</b> debe contener un largo de 8 caracteres maximos, por favor revise los datos ingresados en el campo <b>Documento Identidad</b>.</li>';
                        }
                        if (!is_numeric($documento_identidad)) {
                            $mensaje .='<li>La <b>Cédula Venezolana</b> debe contener solo números, por favor revise los datos ingresados en el campo <b>Documento Identidad</b>.</li>';
                        }
                    } elseif ($origen == 'E') {//Es una cedula extranjera.
                        if (strlen($documento_identidad) < 8 || strlen($documento_identidad) > 8) {
                            $mensaje .='<li>La <b>Cédula Extranjera</b> debe contener un largo de 8 caracteres maximos, por favor revise los datos ingresados en el campo <b>Documento Identidad</b>.</li>';
                        }
                        if (!is_numeric($documento_identidad)) {
                            $mensaje .='<li>La <b>Cédula Extranjera</b> debe contener solo números, por favor revise los datos ingresados en el campo <b>Documento Identidad</b>.</li>';
                        }
                    } elseif ($origen == 'P') {//Es un pasaporte.
                        if (strlen($documento_identidad) < 20 || strlen($documento_identidad) > 20) {
                            $mensaje .='<li>El <b>Pasaporte</b> debe contener un largo de 20 caracteres maximos, por favor revise los datos ingresados en el campo <b>Documento Identidad</b>.</li>';
                        }
                    }
//Fin
//Valido Código del Plantel
                    if ($cod_plantel != '') {
                        $variable = 1;
                        $validarCodPlantel = Plantel::model()->validarCodPlantel($plantel_id, $cod_plantel, $variable);

                        if ($validarCodPlantel == array() || $validarCodPlantel == false) {
                            $mensaje .="<li>El <b>Código del Plantel</b> no es valido ya que el plantel <b>'$plantel'</b> no esta registrado, por favor revise los datos ingresados.</li>";
                        } else {
                            $plantel_id = $model->plantel_id = $validarCodPlantel['plantel_id'];
                            $plantel = $model->plantel = $validarCodPlantel['nombre'];
                        }
                    } else {
                        $mensaje .='<li>Código del Plantel no puede ser nulo.</li>';
                    }
//Fin
//Valido que el grado y la sección corresponda al plantel.
                    if ($grado != null && $seccion != null && $turno != null) {
                        $seccion_plantel_id = SeccionPlantel::model()->validarSeccionGrado($plantel_id, $seccion, $grado, $turno);
                        if ($seccion_plantel_id == false) {
                            $mensaje .="<li>El <b>Grado</b>, <b>Sección</b> o <b>Turno</b> no es valido debido al plan de estudio que imparte el plantel <b>'$plantel'</b> o verifique si la sección plantel existe, por favor revise los datos ingresados.</li>";
                        }
                    } else {
                        $mensaje .='<li>Grado, Sección o Turno no puede ser nulo.</li>';
                    }
//Fin

                    if ($mensaje != '') {
                        $div = '<div class="errorSumary">'
                                . ' <p>Por favor corrija los siguientes errores:</p>'
                                . ' <ul>'
                                . $mensaje
                                . ' </ul></div>';
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $div));
                        Yii::app()->end();
                    }

//Valido si el estudiante esta matriculado en el periodo escolar actual.
                    $existeEstudiante = Estudiante::model()->existeEstudiante($origen, $documento_identidad);
                    if ($existeEstudiante != array()) {//Indica si el estudiante existe en la tabla matricula.estudiante, se verifica si esta matriculado. (Bachillerato)
                        if ($estudiante_id != $existeEstudiante[0] && $estudiante_id != 0 && $estudiante_id != null) {
                            $model->estudiante_id = $existeEstudiante[0];
                            $estudiante_id = $model->estudiante_id;
                        }
                        $resultadoNoMatriculado = Estudiante::model()->estaMatriculado($existeEstudiante, $periodo_id); //Me devuelve los estudiantes que no estan matriculados en el periodo actual.
                        if (isset($resultadoNoMatriculado[0]) && $resultadoNoMatriculado != array()) {
                            $verificoNivel = SeccionPlantel::model()->verificarNivel($seccion_plantel_id); //Si me retorna true es porque pertenece a primaria o inicial.
                            if ($verificoNivel != false && $verificoNivel > 2) {//Indica que no es un estudiante de inicial.
                                $resultadoBachillerato = Estudiante::model()->validarBachillerato($existeEstudiante, $periodo_id, $grado, $repitiente);
                                if ($resultadoBachillerato == array()) {
                                    $mensaje .='<li>El Estudiante <b>' . $nombres . ' ' . $apellidos . '</b> existe en el sistema, pero el grado en el que desea matricular a dicho estudiante no es correcto porque se verificó y esta persona no tiene registro de que curso el grado anterior en el periodo escolar correspondiente, por favor verifique los <b>Datos Académicos</b> que ingresó y guarde los datos correctos, si el error persiste verifique en el módulo de <b>Estudiante</b> el <b>Historial del Estudiante</b> y compruebe si el estudiante fue inscrito en el periodo escolar 2012-2013, si no fue inscrito registre la escolaridad que le falta al estudiante si una vez realizado estos pasos no puede matricular contacte al administrador del sistema, enviando un correo <b>soporte_gescolar@me.gob.ve</b> especificando el inconveniente que esta presentando.</li>';
                                } else {
                                    $model->estatus_inscripcion = 'P'; //P->Significa que el estudiante esta pendiente por verificar.
                                }
                            } elseif ($verificoNivel <= 2) {
                                $resultadoInicialPrimaria = Estudiante::model()->buscarInicialPrimaria($existeEstudiante, $verificoNivel);
                                if ($resultadoInicialPrimaria == array()) {
                                    $mensaje .='<li>El Estudiante <b>' . $nombres . ' ' . $apellidos . '</b> existe en el sistema, pero no cumple con la edad para estar en inicial o primaria, por favor verifique la fecha de nacimiento en el modulo de estudiante y guarde los datos correctos, esto se debe a que los rangos adecuados son de <b>1er Maternal Hasta 1er Grado (2006-01-01 al 2014-12-31) y si es de 2do grado a 6to Grado (1999-01-01 al 2007-12-31)</b>, si el error persiste contacte al administrador del sistema, enviando un correo <b>soporte_gescolar@me.gob.ve</b> especificando el inconveniente que esta presentando.</li>';
                                } else {
                                    $origen_rep = $model->origen_rep = isset($_REQUEST['InscripcionManual']['origen_rep']) ? $_REQUEST['InscripcionManual']['origen_rep'] : '';
                                    $documento_identidad_rep = $model->documento_identidad_rep = isset($_REQUEST['InscripcionManual']['documento_identidad_rep']) ? trim($_REQUEST['InscripcionManual']['documento_identidad_rep']) : '';
                                    $model->nombres_rep = isset($_REQUEST['InscripcionManual']['nombres_rep']) ? trim($_REQUEST['InscripcionManual']['nombres_rep']) : '';
                                    $model->apellidos_rep = isset($_REQUEST['InscripcionManual']['apellidos_rep']) ? trim($_REQUEST['InscripcionManual']['apellidos_rep']) : '';
                                    $afinidad_id = $model->afinidad_id = isset($_REQUEST['InscripcionManual']['afinidad_id']) ? trim($_REQUEST['InscripcionManual']['afinidad_id']) : null;
//    $resultadoInicialPrimaria = Estudiante::model()->buscarInicialPrimaria($estudiantesNoMatriculado);
                                    if ($origen_rep == '') {
                                        $mensaje .='<li>Origen del Representante no puede ser nulo.</li>';
                                    }
                                    if ($documento_identidad_rep == '') {
                                        $mensaje .='<li>Documento Identidad del Representante no puede ser nulo.</li>';
                                    }
                                    if ($afinidad_id == null) {
                                        $mensaje .='<li>Afinidad no puede ser nulo.</li>';
                                    }
                                    if ($documento_identidad_rep != '' && $origen_rep != '' && $afinidad_id != null) {
                                        $model->estatus_inscripcion = 'P'; //P->Significa que el estudiante esta pendiente por verificar.
                                    }
                                }
//$mensaje .='<li>El Estudiante ' . $nombres . ' ' . $apellidos . ' existe en el sistema, pero el nivel de estudio es de educación primaria o inicial, por favor verifique los <b>Datos Académicos</b> que ingresó y guarde los datos correctos, debido a que este estudiante debe ser de educación media, si el error persiste contacte al administrador del sistema, enviando un correo <b>soporte_gescolar@me.gob.ve</b> especificando el inconveniente que esta presentando.</li>';
                            }
                        } else {//Sino es porque ya esta matriculado.
                            $model->estatus_inscripcion = 'M'; //M->Significa que el estudiante esta matriculado.
                            $msj = ', se acota que el estudiante ya esta matriculado en el periodo <b>2014</b>.';
                        }
                    } elseif ($existeEstudiante == array()) {//Indica si el estudiante no existe en la tabla matricula.estudiante, luego se verifica si es de primaria o inicial.
                        $verificoNivel = SeccionPlantel::model()->verificarNivel($seccion_plantel_id); //Si me retorna true es porque pertenece a primaria o inicial.
                        if ($verificoNivel != false && $verificoNivel <= 2) {
                            if ($fecha_nacimiento != '') {
                                $fecha_nacimiento = date("Y-m-d", strtotime($fecha_nacimiento));
                                //  $calcularAdulto = Estudiante::model()->calcularEdad($fecha_nacimiento);
                                if ($verificoNivel == 1) {
                                    $edad_permitida = 8;
                                    $anio_actual = date('Y');
                                    $anio_final = $anio_actual - 1;
                                    $anio_inicial = $anio_final - $edad_permitida;
                                } elseif ($verificoNivel == 2) {
                                    $edad_permitida = 8;
                                    $anio_actual = date('Y');
                                    $anio_final = $anio_actual - $edad_permitida;
                                    $anio_inicial = $anio_final - $edad_permitida;
                                }
                                if (($fecha_nacimiento < $anio_inicial . '-01-01') || ($fecha_nacimiento > $anio_final . '-12-31')) {
                                    $mensaje .='<li>El Estudiante <b>' . $nombres . ' ' . $apellidos . '</b> no existe en el sistema y no cumple con la edad adecuada para estar en inicial o primaria, por favor verifique la fecha de nacimiento que ingreso y guarde los datos correctos, esto se debe a que los rangos de edad son de <b>1er Maternal Hasta 1er Grado (2006-01-01 al 2014-12-31) y si es de 2do grado a 6to Grado (1999-01-01 al 2007-12-31)</b>, si el error persiste contacte al administrador del sistema, enviando un correo <b>soporte_gescolar@me.gob.ve</b> especificando el inconveniente que esta presentando.</li>';
                                }
                            } else {
                                $mensaje .='<li>Fecha Nacimiento no puede ser nulo.</li>';
                            }
                            $origen_rep = $model->origen_rep = isset($_REQUEST['InscripcionManual']['origen_rep']) ? $_REQUEST['InscripcionManual']['origen_rep'] : '';
                            $documento_identidad_rep = $model->documento_identidad_rep = isset($_REQUEST['InscripcionManual']['documento_identidad_rep']) ? trim($_REQUEST['InscripcionManual']['documento_identidad_rep']) : '';
                            $model->nombres_rep = isset($_REQUEST['InscripcionManual']['nombres_rep']) ? trim($_REQUEST['InscripcionManual']['nombres_rep']) : '';
                            $model->apellidos_rep = isset($_REQUEST['InscripcionManual']['apellidos_rep']) ? trim($_REQUEST['InscripcionManual']['apellidos_rep']) : '';
                            $afinidad_id = $model->afinidad_id = isset($_REQUEST['InscripcionManual']['afinidad_id']) ? trim($_REQUEST['InscripcionManual']['afinidad_id']) : null;
                            if ($origen_rep == '') {
                                $mensaje .='<li>Origen del Representante no puede ser nulo.</li>';
                            }
                            if ($documento_identidad_rep == '') {
                                $mensaje .='<li>Documento Identidad del Representante no puede ser nulo.</li>';
                            }
                            if ($afinidad_id == null) {
                                $mensaje .='<li>Afinidad no puede ser nulo.</li>';
                            }
                            if ($documento_identidad_rep != '' && $origen_rep != '' && $afinidad_id != null) {
                                $model->estatus_inscripcion = 'P'; //P->Significa que el estudiante esta pendiente por verificar.
                            }
                        } else {
                            $mensaje .='<li>El Estudiante <b>' . $nombres . ' ' . $apellidos . '</b> no existe en el sistema y según los datos suministrados en <b>Datos Académicos</b> el nivel de estudio es de educación media y para que este estudiante este en educación media debe estar registrado en sistema, por favor verifique los <b>Datos Académicos</b> que ingresó y guarde los datos correctos, debido a que los estudiantes que no existen en sistema debe ser de educación primaria o inicial, si el error persiste contacte al administrador del sistema, enviando un correo <b>soporte_gescolar@me.gob.ve</b> especificando el inconveniente que esta presentando.</li>';
                        }
                    }
//Fin

                    if ($mensaje != '') {
                        $div = '<div class="errorSumary">'
                                . ' <p>Por favor corrija los siguientes errores:</p>'
                                . ' <ul>'
                                . $mensaje
                                . ' </ul></div>';
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $div));
                        Yii::app()->end();
                    } else {
                        $transaction = Yii::app()->db->beginTransaction();
                        try {
                            $model->usuario_act_id = $usuario_id;
                            $model->fecha_act = $fecha;
                            $model->estatus = $estatus;
                            $model->seccion_plantel_id = $seccion_plantel_id;
                            $model->periodo_id = $periodo_id;

                            if ($model->save()) {
                                $this->registerLog('ACTUALIZACION', self::MODULO . 'modificar', 'EXITOSO', 'Permite modificar el registro número: ' . $model->id . ' que fue seleccionado para la matriculación manual de los estudiantes que no se pudieron inscribir en el proceso normal de matriculación.', $ip, $usuario_id, $username, $fecha);
                                $mensaje_exitoso = 'Modificación exitosa de los datos del estudiante: ' . $nombres . ' ' . $apellidos . $msj;
                                echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                                $transaction->commit();
                                Yii::app()->end();
                            } else {
                                $mensaje_error = 'No se pudo guardar los datos modificados del estudiante: ' . $nombres . ' ' . $apellidos . ', Por favor intente nuevamente <br>';
                                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));
                            }
                        } catch (Exception $ex) {
//                            var_dump($ex);
//                            die();
                            $transaction->rollback();
                            $this->registerLog('ESCRITURA', self::MODULO . 'modificar', 'FALLIDO', "Ocurrió un error, No se pudo guardar los datos modificados del estudiante: $nombres $apellidos .  Error: {$ex->getMessage()}.", $ip, $usuario_id, $username, $fecha);
                            $mensaje_error = 'Ocurrio un error, No se pudo guardar los datos modificados del estudiante: ' . $nombres . ' ' . $apellidos . ', Por favor recargue la página he intente nuevamente';
                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));
                        }
                    }
                } else {
                    $error = CHtml::errorSummary($model);
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $error));
                    Yii::app()->end();
                }
            } else {

                $afinidad = Afinidad::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A'"));
                $grado = Grado::model()->findAll(array('order' => 'consecutivo ASC', 'condition' => "estatus='A'"));
                $seccion = Seccion::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A'"));
                $turno = Turno::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A'"));
                $estado = CEstado::getData(array('order' => 'nombre ASC'));
                $this->render('_form', array(
                    'model' => $model,
                    'id' => $id,
                    'plantel_id' => $plantel_id,
                    'afinidad' => $afinidad,
                    'estado' => $estado,
                    'grado' => $grado,
                    'seccion' => $seccion,
                    'turno' => $turno
                ));
            }
        } else {
            throw new CHttpException(404, "Error, por favor recargue la página he intente nuevamente ya que no se encontro el registro que se requiere modificar.");
        }
    }

    public function actionValidarEstudiante() {
        $mensaje = '';
        $mensaje_exitoso = '';
        $div = '';
        $title = '';
        $id = null;
        $plantel_id = null;
        $usuario_id = Yii::app()->user->id;
        $fecha = date('Y-m-d H:i:s');
        $ip = Yii::app()->request->userHostAddress;
        $username = Yii::app()->user->name;
        $periodo_id = 14;
        $ruta = yii::app()->basePath;
        $ruta = $ruta . '/yiic';
        $control = 0;
        $inscripcion_regular_array[] = 1;
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '' && $_REQUEST['id'] != null && isset($_REQUEST['plantel_id'])) {
            $id = base64_decode($_REQUEST['id']);
            //    $plantel_id = base64_decode($_REQUEST['plantel_id']);
            $model = InscripcionManual::model()->findByPk($id);

            if ($model != null) {
                $nombres = isset($model['nombres']) ? $model['nombres'] : '';
                $apellidos = isset($model['apellidos']) ? $model['apellidos'] : '';
                $origen = isset($model['origen']) ? $model['origen'] : '';
                $documento_identidad = isset($model['documento_identidad']) ? $model['documento_identidad'] : '';
                $estatus_inscripcion = isset($model['estatus_inscripcion']) ? $model['estatus_inscripcion'] : '';
                $seccion_plantel_id = isset($model['seccion_plantel_id']) ? $model['seccion_plantel_id'] : null;
                $estudiante_id = isset($model['estudiante_id']) ? $model['estudiante_id'] : null;
                $grado = isset($model['grado_id']) ? $model['grado_id'] : null;
                $plantel_id = isset($model['plantel_id']) ? $model['plantel_id'] : null;
                $repitiente = isset($model['repitiente_est'])?$model['repitiente_est']:0;

                if ($estatus_inscripcion == 'P') {
//Valido si el estudiante esta matriculado en el periodo escolar actual.
                    $existeEstudiante = Estudiante::model()->existeEstudiante($origen, $documento_identidad);
                    if ($existeEstudiante != array()) {//Indica si el estudiante existe en la tabla matricula.estudiante, se verifica si esta matriculado. (Bachillerato)
                        $resultadoNoMatriculado = Estudiante::model()->estaMatriculado($existeEstudiante, $periodo_id); //Me devuelve los estudiantes que no estan matriculados en el periodo actual.
                        if (isset($resultadoNoMatriculado[0]) && $resultadoNoMatriculado != array()) {
                            $verificoNivel = SeccionPlantel::model()->verificarNivel($seccion_plantel_id); //Si me retorna true es porque pertenece a primaria o inicial.
                            if ($verificoNivel != false && $verificoNivel > 2) {//Indica que no es un estudiante de inicial.
                                $resultadoBachillerato = Estudiante::model()->validarBachillerato($existeEstudiante, $periodo_id, $grado, $repitiente);
                                if ($resultadoBachillerato != array()) {
                                    $control = 1; //Se puede matricular el estudiante.
                                    $model->estatus_inscripcion = 'L'; //L->Significa que el estudiante esta listo para ser matriculado.
                                }
                            } elseif ($verificoNivel <= 2) {
                                $resultadoInicialPrimaria = Estudiante::model()->validarInicialPrimaria($existeEstudiante);
                                if ($resultadoInicialPrimaria != array()) {
                                    $control = 1; //Se puede matricular el estudiante.
                                    $model->estatus_inscripcion = 'L'; //L->Significa que el estudiante esta listo para ser matriculado.
                                }
                            }
                        }
                    } elseif ($existeEstudiante == array()) {//Indica si el estudiante no existe en la tabla matricula.estudiante, luego se verifica si es de primaria o inicial.
                        $verificoNivel = SeccionPlantel::model()->verificarNivel($seccion_plantel_id); //Si me retorna true es porque pertenece a primaria o inicial.
                        if ($verificoNivel != false && $verificoNivel <= 2) {
                            $control = 2; //Se tiene que registrar el estudiante.
                            $model->estatus_inscripcion = 'L'; //L->Significa que el estudiante esta listo para ser matriculado.
                        }
//Fin
                    }
                } else {
                    $mensaje .='<li>El Estatus Inscripción es el incorrecto por favor presione <i class="icon-pencil green"></i> y ingrese los datos del estudiante correspondientes, si el error persiste contacte al administrador del sistema, enviando un correo <b>soporte_gescolar@me.gob.ve</b> especificando el inconveniente que esta presentando.</li>';
                }

                if ($mensaje != '') {
                    $title = 'Notificación de Error';
                    $div = '<div class="errorSumary">'
                            . ' <p>Por favor corrija los siguientes errores:</p>'
                            . ' <ul>'
                            . $mensaje
                            . ' </ul></div>';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $div, 'title' => $title));
                    Yii::app()->end();
                } else {
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $model->usuario_act_id = $usuario_id;
                        $model->fecha_act = $fecha;
                        if ($model->save()) {
                            $this->registerLog('ACTUALIZACION', self::MODULO . 'validarEstudiante', 'EXITOSO', 'Permite validar el registro número: ' . $model->id . ' que fue seleccionado para agregarle el estatus L->Listo para ser matriculado.', $ip, $usuario_id, $username, $fecha);


//Transformo un array a [] con toPgArray
                            $estudiante[] = (int) $estudiante_id;
                            $estudiantes = $this->to_pg_array($estudiante);
                            $inscripcion_regular = $this->to_pg_array($inscripcion_regular_array);
//
// Fin
                            try {
//Matricular en MatriculacionCommand
                                if ($control == 1) { //Se puede matricular el estudiante.
                                    $resultadoMatriculacion = Estudiante::model()->inscribirEstudiantes($estudiantes, $plantel_id, $seccion_plantel_id, $periodo_id, self::MODULO . 'validarEstudiante', $inscripcion_regular);
                                    $this->registerLog('ESCRITURA', self::MODULO . 'validarEstudiante', 'EXITOSO', 'Ha matriculado los estudiantes ' . $estudiantes . ' que faltaban matricular en el periodo escolar ' . $periodo_id . ' en la Seccion Plantel ' . $seccion_plantel_id . ' del plantel ' . $plantel_id, $ip, $usuario_id, $username, $fecha);
//                                echo "nohup /usr/bin/php $ruta Matriculacion InscripcionManual --estudiantes=$estudiantes --seccion_plantel_id=$seccion_plantel_id --plantel_id=$plantel_id --periodo_id=$periodo_id --usuario_id=$usuario_id --ip=$ip --username=$username --inscripcion_regular=$inscripcion_regular 1>/dev/null & echo $!";
//                                die();
//                                $resultado_comando = shell_exec("nohup /usr/bin/php $ruta Matriculacion InscripcionManual --estudiantes=$estudiantes --seccion_plantel_id=$seccion_plantel_id --plantel_id=$plantel_id --periodo_id=$periodo_id --usuario_id=$usuario_id --ip=$ip --username=$username --inscripcion_regular=$inscripcion_regular 1>/dev/null & echo $!");
                                    $estatus_inscripcion = 'M'; //M->Significa que el estudiante esta matriculado.
                                    $update = InscripcionManual::model()->updateEstatus($id, $estatus_inscripcion, $usuario_id, $fecha);
                                    $transaction->commit();
                                }
                                if ($control == 2) { //Se tiene que registrar el estudiante.
                                    $registrarEstudiante = Estudiante::model()->registrarEstudiante($model);
                                    $this->registerLog('ESCRITURA', self::MODULO . 'validarEstudiante', 'EXITOSO', 'Se registro y matriculo los estudiantes ' . $estudiantes . ' que no existian en el sistema y faltaban por matricular en el periodo escolar ' . $periodo_id . ' en la Seccion Plantel ' . $seccion_plantel_id . ' del plantel ' . $plantel_id, $ip, $usuario_id, $username, $fecha);
                                    $estatus_inscripcion = 'M'; //M->Significa que el estudiante esta matriculado.
                                    $update = InscripcionManual::model()->updateEstatus($id, $estatus_inscripcion, $usuario_id, $fecha);
                                    $transaction->commit();
                                }
//  Utiles::enviarCorreo('mari.lac.mor@gmail.com', 'Notificación de la validación de entrega de seriales a las zonas educativas', $resultado_comando);
//Fin
                                if($control > 0){
                                    $title = 'Notificación de Exitosa';
                                    $mensaje_exitoso = 'Validación de los datos del estudiante: ' . $nombres . ' ' . $apellidos . ' y matriculación exitosa en el periodo escolar ' . $periodo_id;
                                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso, 'title' => $title));
                                    Yii::app()->end();
                                }else{
                                    $transaction->rollback();//Si entra aqui verificar si el estudiante es de bachillerato revisar la funcion (validarBachillerato) porque esta retornando vacio o si es de inicial-primaria revisar la funcion validarInicialPrimaria porque esta retornando vacio.
                                    $this->registerLog('ESCRITURA', self::MODULO . 'validarEstudiante', 'FALLIDO', "Ocurrió un error, No se pudo matricular al estudiante:  ' . $nombres . ' ' . $apellidos . ' debido a que la validación no se realizó con exito, Por favor intente nuevamente.", $ip, $usuario_id, $username, $fecha);
                                    $title = 'Notificación de Error';
                                    $mensaje = 'No se pudo matricular al estudiante:  ' . $nombres . ' ' . $apellidos . ' debido a que la validación no se realizó con exito, Por favor intente nuevamente.<br>';
                                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje, 'title' => $title));
                                    Yii::app()->end();
                                }
                            } catch (Exception $e) {
//                                var_dump($e);
//                                die();
                                $transaction->rollback();
                                $this->registerLog('ESCRITURA', self::MODULO . 'validarEstudiante', 'FALLIDO', "Ocurrió un error, No se pudo matricular el estudiante: $nombres $apellidos .  Error: {$e->getMessage()}.", $ip, $usuario_id, $username, $fecha);
                                $title = 'Notificación de Error';
                                $mensaje = 'Ocurrio un error, No se pudo matricular el estudiante: ' . $nombres . ' ' . $apellidos . '.';
                                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje, 'title' => $title));
                                Yii::app()->end();
                            }
                        } else {
                            $title = 'Notificación de Error';
                            $mensaje = 'No se pudo modificar el estatus de inscripción del estudiante: ' . $nombres . ' ' . $apellidos . ', Por favor intente nuevamente.<br>';
                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje, 'title' => $title));
                            Yii::app()->end();
                        }
                    } catch (Exception $ex) {
//                        var_dump($ex);
//                        die();
                        $transaction->rollback();
                        $this->registerLog('ESCRITURA', self::MODULO . 'validarEstudiante', 'FALLIDO', "Ocurrió un error, No se pudo modificar el estatus de inscripción del estudiante: $nombres $apellidos .  Error: {$ex->getMessage()}.", $ip, $usuario_id, $username, $fecha);
                        $title = 'Notificación de Error';
                        $mensaje = 'Ocurrio un error, No se pudo modificar el estatus de inscripción del estudiante: ' . $nombres . ' ' . $apellidos . ', Por favor recargue la página he intente nuevamente.';
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje, 'title' => $title));
                        Yii::app()->end();
                    }
                }
            } else {
                $mensaje .='No se encontraron los datos del estudiante y el plantel al que pertenece el mismo, Por favor recargue la pagina, si el error persiste contacte al administrador del sistema, enviando un correo <b>soporte_gescolar@me.gob.ve</b> especificando el inconveniente que esta presentando.';
                $title = 'Notificación de Error';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje, 'title' => $title));
                Yii::app()->end();
            }
        } else {
            $mensaje .='No se encuentra el plantel al que pertenece el estudiante, Por favor recargue la pagina, si el error persiste contacte al administrador del sistema, enviando un correo <b>soporte_gescolar@me.gob.ve</b> especificando el inconveniente que esta presentando.';
            $title = 'Notificación de Error';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje, 'title' => $title));
            Yii::app()->end();
        }
    }

    public function actionObtenerDatosPersona() {
        if (Yii::app()->request->isAjaxRequest) {
            $tDocumentoIdentidad = $this->getQuery('tdocumento_identidad');
            $documentoIdentidad = $this->getQuery('documento_identidad');
            $datosPersona = '';
            $nombresPersona = '';
            $apellidosPersona = '';
            $fecha_nacimientoFormat = '';
            if (in_array($tDocumentoIdentidad, array('V', 'E', 'P'))) {
                if (in_array($tDocumentoIdentidad, array('V', 'E'))) {
                    if (strlen((string) $documentoIdentidad) > 10) {
                        $mensaje = 'Estimado usuario el campo Documento de Identidad no puede superar los diez (10) caracteres.';
                        $title = 'Notificación de Error';
                        echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                        Yii::app()->end();
                    } else {
                        if (!is_numeric($documentoIdentidad)) {
                            $mensaje = 'Estimado usuario el campo Documento de Identidad debe poseer solo caracteres numericos.';
                            $title = 'Notificación de Error';
                            echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                            Yii::app()->end();
                        }
                    }
                }

                $datosPersona = AutoridadPlantel::model()->busquedaSaimeMixta($tDocumentoIdentidad, $documentoIdentidad);
                if ($datosPersona) {
                    (isset($datosPersona['nombre'])) ? $nombresPersona = $datosPersona['nombre'] : $nombresPersona = '';
                    (isset($datosPersona['apellido'])) ? $apellidosPersona = $datosPersona['apellido'] : $apellidosPersona = '';
                    (isset($datosPersona['sexo'])) ? $sexoPersona = $datosPersona['sexo'] : $sexoPersona = '';
                    (isset($datosPersona['fecha_nacimiento'])) ? $fecha_nacimiento = $datosPersona['fecha_nacimiento'] : $fecha_nacimiento = '';
                    $fecha_nacimientoFormat = date("d-m-Y", strtotime($fecha_nacimiento));
                    $calcularEdad = Estudiante::model()->calcularEdad($fecha_nacimiento);
                    if ($calcularEdad >= 12) {
                        echo json_encode(array('statusCode' => 'SUCCESS', 'nombres' => $nombresPersona, 'apellidos' => $apellidosPersona, 'fecha_nacimiento' => $fecha_nacimientoFormat, 'sexo' => $sexoPersona));
                        Yii::app()->end();
                    } else {
                        $mensaje = 'La persona ' . $nombresPersona . ' ' . $apellidosPersona . ' no puede ser representante del estudiante que desea guardar porque debe tener 12 años minimo.';
                        $title = 'Notificación de Error';
                        echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                        Yii::app()->end();
                    }
                } else {
                    $mensaje = 'El Documento de Identidad ' . $tDocumentoIdentidad . '-' . $documentoIdentidad . ' no se encuentra registrado en nuestro sistema, por favor contacte al personal de soporte.';
                    $title = 'Notificación de Error';
                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                    Yii::app()->end();
                }
            } else {
                $mensaje = 'Estimado usuario, se han enviado valores no permitidos en el campo Tipo de Documento de Identidad.';
                $title = 'Notificación de Error';
                echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionAutoComplete() {

        $_GET['term'] = strtoupper($_GET['term']);
        $res = array();

        if (isset($_GET['term'])) {
            $res = InscripcionManual::obtenerNombresPlantel($_GET['term']);
        }

        echo CJSON::encode($res);
    }

    public function actionBuscarNombrePlantel() {
        $variable = 1;
        $nombrePlantel = '';
        if (isset($_REQUEST['codigo'])) {
            $cod_plantel = $_REQUEST['codigo'];
            $nombrePlantel = InscripcionManual::obtenerNombresPlantel($cod_plantel, $variable);
        }
        echo json_encode(array('statusCode' => 'success', 'nombre' => $nombrePlantel['nombre'], 'estado' => $nombrePlantel['estado']));
        Yii::app()->end();
    }

    public function columnaSexo($data) {
        $sexo = $data['sexo'];
        if (($sexo == 'F')) {
            return 'FEMENINO';
        } else if ($sexo == 'M') {
            return 'MASCULINO';
        }
    }

    public function columnaEstatusInscripcion($data) {
        $estatus = $data->estatus_inscripcion;
        $nombre = '';
        switch ($estatus) {
            case 'V':
                $nombre = 'Falta Verificar';
                break;
            case 'M':
                $nombre = 'Matriculado';
                break;
            case 'P':
                $nombre = 'Pendiente por Validar';
                break;
            case 'L':
                $nombre = 'Listo para Matricular';
                break;
        }
        return $nombre;
    }

    public function columnaAcciones($data) {

        $id = $data->id;
        $plantel_id = $data->plantel_id;
        $estatus = $data->estatus_inscripcion;
        $columna = '<div class="left" class="action-buttons">';
        $columna .= CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;</span>", "/planteles/inscripcionManual/view/id/" . base64_encode($id) . "/plantel/" . base64_encode($plantel_id), array("class" => "fa fa-search-plus blue", "title" => "Ver Datos del Estudiantes"));
        if ($estatus == 'V') {
            $columna .= CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;</span>", "/planteles/inscripcionManual/modificar/id/" . base64_encode($id) . "/plantel/" . base64_encode($plantel_id), array("class" => "fa fa-pencil green", "title" => "Modificar Datos de Estudiantes"));
        }
        if ($estatus == 'P') {
            $columna .= CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;</span>", "", array("class" => "fa fa-check-square-o red validarEstudiante", "id" => base64_encode($id), "plantel_id" => base64_encode($plantel_id), "title" => "Validar Datos de Estudiantes"));
        }
        $columna .= '</div>';

        return $columna;
    }

    public function to_pg_array($set) {
        settype($set, 'array'); // can be called with a scalar or array
        $result = array();
        foreach ($set as $t) {
            if (is_array($t)) {
                $result[] = to_pg_array($t);
            } else {
                $t = str_replace('"', '\\"', $t); // escape double quote
                if (!is_numeric($t)) // quote only non-numeric values
                    $t = '"' . $t . '"';
                $result[] = $t;
            }
        }
        return '{' . implode(",", $result) . '}'; // format
    }

}
