<?php

class DocenteController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';
    CONST PERIODO_ESCOLAR_ID=15;

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de DocenteController',
        'write' => 'Creación y Modificación de DocenteController',
        'admin' => 'Administración Completa  de DocenteController',
        'label' => 'Módulo de DocenteController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion','liberarDocente','CambiarEstatusAsignacionDocente'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion','asignaturaDocente','obtenerDatosPersona','guardarAsignaturaDocente','liberarDocente','CambiarEstatusAsignacionDocente'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta','obtenerDatosPersona'),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Lists all models.
     */
    public function actionLista()
    {

        if($this->hasQuery('id') AND $this->hasQuery('plantel')) {
            $idDecoded = $this->getIdDecoded($this->getQuery('id'));
            $plantelIdDecoded = $this->getIdDecoded($this->getQuery('plantel'));
            $model = new AsignaturaDocente('search');
            $model->unsetAttributes();  // clear any default values
            //$periodo_actual = PeriodoEscolar::model()->getPeriodoActivo();
            //$model->periodo_id=$periodo_actual['id'];
            $model->periodo_id=self::PERIODO_ESCOLAR_ID;
            $model->seccion_plantel_id=$idDecoded;
            $model->estatus='A';
            $dataPlantel = Plantel::model()->obtenerDatosIdentificacion($plantelIdDecoded);
            $dataSeccion = SeccionPlantel::model()->cargarDetallesSeccion($idDecoded, $plantelIdDecoded);
            $estatus_asig_doc=CierreAsignacionDocente::model()->getDatos($idDecoded,self::PERIODO_ESCOLAR_ID);
            $estatus_asig_doc=$estatus_asig_doc['estatus'];
            $this->render('index', array(
                    'model' => $model,
                    'seccion_plantel_id' => $this->getQuery('id'),
                    'plantel_id' => $this->getQuery('plantel'),
                    'datosPlantel' => $dataPlantel,
                    'dataSeccion' => $dataSeccion,
                    'estatus_asig_doc' => $estatus_asig_doc,
                )
            );
        }
        else {
            throw new CHttpException(403, 'Estimado Usuario no se han recibido los parametros necesarios para reealizar esta acción.');
        }
    }
    public function actionCambiarEstatusAsignacionDocente() {

        $accion = $this->getPost('accion');
        $seccion_plantel_id = $this->getPost('seccion_plantel_id');
        $seccion_plantel_id_decoded = base64_decode($seccion_plantel_id);
        $usuario_id = Yii::app()->user->id;
        if (in_array($accion, array('E', 'A')) AND is_numeric($seccion_plantel_id_decoded)) {
            $model = new CierreAsignacionDocente();
            $periodo_escolar = self::PERIODO_ESCOLAR_ID;
            $resultado_consulta = $model->getDatos($seccion_plantel_id_decoded, $periodo_escolar);
            if ($resultado_consulta !== array() AND $resultado_consulta != false) {
                $model = $model->findByPk($resultado_consulta['id']);
                $model->estatus = $accion;
                $model->fecha_act = date('Y-m-d H:s:i');
                $model->usuario_act_id = $usuario_id;
                if ($accion == 'E') {
                    $model->fecha_elim = date('Y-m-d H:s:i');
                } else {
                    $model->fecha_elim = NULL;
                }
            } else {
                $model->seccion_plantel_id = $seccion_plantel_id_decoded;
                $model->periodo_id = $periodo_escolar;
                $model->estatus = 'A';
                $model->fecha_ini = date('Y-m-d H:s:i');
                $model->usuario_ini_id = $usuario_id;
            }
            if($model->validate()){
                if ($model->save()) {
                    $accion_mensaje = '';
                    if ($accion == 'A') {
                        $accion_mensaje = 'Cerrado';
                        $this->registerLog('ELIMINACION', 'planteles.seccionPlantel15.cambiarEstatusAsignacionDocente', 'EXITOSO', 'Se ha Inactivado el Proceso de Asignación de Docentes para la seccion_plantel_id' . $model->seccion_plantel_id . ' y en el periodo ' . $model->periodo_id);
                    } else {
                        $accion_mensaje = 'Aperturado';
                        $this->registerLog('ESCRITURA', 'planteles.seccionPlantel15.cambiarEstatusAsignacionDocente', 'EXITOSO', 'Se ha Activado el Proceso de Asignación de Docentes para la seccion_plantel_id' . $model->seccion_plantel_id . ' y en el periodo ' . $model->periodo_id);
                    }
                    $respuesta['statusCode'] = 'success';
                    $respuesta['mensaje'] = 'Estimado Usuario, se ha ' . $accion_mensaje . ' el Proceso de Asignación de Docentes satisfactoriamente. Al cerrar esta ventana se recargará la página automaticamente para actualizar la información.';
                    echo json_encode($respuesta);
                    Yii::app()->end();
                } else {
                    $class = 'errorDialogBox';
                    $message = '500 Error: No se ha podido completar la operación, comuniquelo al administrador del sistema para su corrección.';
                    $this->renderPartial('//msgBox', array(
                        'class' => $class,
                        'message' => $message,
                    ));
                }
            }
            else {
                $this->renderPartial('//errorSumMsg', array(
                    'model' => $model,
                ));
            }

        } else {

            $class = 'errorDialogBox';
            $message = 'No se ha especificado la acción a tomar sobre el plantel en cuanto al proceso de matriculación, recargue la página e intentelo de nuevo.';

            $this->renderPartial('//msgBox', array(
                'class' => $class,
                'message' => $message,
            ));
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Docente('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('Docente')){
            $model->attributes=$this->getQuery('Docente');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {

        $idDecoded = $this->getIdDecoded($id);
        $model = new AsignaturaDocente('search');
        //$periodo_actual = PeriodoEscolar::model()->getPeriodoActivo();
        //$model->periodo_id=$periodo_actual['id'];
        $model->periodo_id=self::PERIODO_ESCOLAR_ID;
        $model->seccion_plantel_id=$idDecoded;
        $this->render('view',array(
            'model'=>$model,
        ));
    }




    public function actionLiberarDocente($id){
        if ($this->hasRequest('id')) {
            $id = $this->getRequest('id');
            $id_decode = base64_decode($id);
            $model = $this->loadModel($id_decode);
            //var_dump($model); die();
            $model->usuario_act_id=Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            $model->fecha_elim=date('Y-m-d H:i:s');
            $model->personal_plantel_id="";
            $model->estatus = "A";
            if ($model->save()) {
                $this->registerLog('ESCRITURA', 'planteles.docente.borrar', 'EXITOSO', 'Se ha desvinculado un docente');
                $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Desvinculada la Asignatura '.$model->asignatura->nombre));
                //$model = $this->loadModel($id_decode);
            } else {
                throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
            }
        }
    }
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro()
    {
        if($this->hasQuery('id')) {
            $plantel_id = $this->getIdDecoded($this->getQuery('id'));
            $model=new Docente;
            $modelDocentePlantel=new DocentePlantel();
            $valido = true;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if($this->hasPost('Docente'))
            {
                $model->attributes=$this->getPost('Docente');
                $model->fecha_ini=date('Y-m-d H:i:s');
                $model->usuario_ini_id=Yii::app()->user->id;
                if($model->validate()){
                    $existeDocente = $model->findByAttributes(array('documento_identidad'=>$model->documento_identidad,'tdocumento_identidad'=>$model->tdocumento_identidad));
                    if($existeDocente!= array()){
                        $model=$existeDocente;

                        $existeDocentePlantel= $modelDocentePlantel->findByAttributes(array('docente_id'=>$model->id,'plantel_id'=>$plantel_id));

                        if($existeDocentePlantel!= array()){
                            $model->addError('documento_identidad','Ya esta registrado un Docente en este Plantel con la misma Identificación');
                            $model->addError('tdocumento_identidad','');
                            $valido = false;
                        }
                        else
                        {
                            $modelDocentePlantel->docente_id=$model->id;
                            $modelDocentePlantel->plantel_id=$plantel_id;
                            $modelDocentePlantel->estatus='A';
                            $modelDocentePlantel->fecha_ini=date('Y-m-d H:i:s');
                            $modelDocentePlantel->usuario_ini_id=Yii::app()->user->id;
                        }
                    }
                    if($valido && $model->save()){
                        if($existeDocente==array()){
                            $modelDocentePlantel->docente_id=$model->id;
                            $modelDocentePlantel->plantel_id=$plantel_id;
                            $modelDocentePlantel->estatus='A';
                            $modelDocentePlantel->fecha_ini=date('Y-m-d H:i:s');
                            $modelDocentePlantel->usuario_ini_id=Yii::app()->user->id;
                        }
                        if($modelDocentePlantel->save())
                            $this->redirect(array('consulta','id'=>base64_encode($model->id),'plantel_id'=>$this->getQuery('id')));
                    }
                }
            }
            $this->render('create',array(
                'model'=>$model,
                'plantel_id'=>$this->getQuery('id')
            ));
        }
        else {
            throw new CHttpException(403, 'Estimado Usuario no se han recibido los parametros necesarios para reealizar esta acción.');
        }
    }


    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if($this->hasPost('Docente'))
        {
            $model->attributes=$this->getPost('Docente');
            $model->fecha_act=date('Y-m-d H:i:s');
            $model->usuario_act_id=Yii::app()->user->id;
            if($model->save()){
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La actualización de los Datos se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $model->fecha_act=date('Y-m-d H:i:s');
        $model->fecha_elim=date('Y-m-d H:i:s');
        $model->usuario_act_id=Yii::app()->user->id;
        $model->estatus = 'E';
        // Descomenta este código para habilitar la eliminación física de registros.
        //$model->update();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!$this->hasQuery('ajax')){
            $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
        }
    }
    public function actionAsignaturaDocente()
    {
        if($this->hasQuery('id')) {
            $idDecoded = $this->getIdDecoded($this->getQuery('id'));
            $model = $this->loadModel($idDecoded);
            $this->renderPartial('_formAsignaturaDocente', array('model'=>$model),false,true);
        }
        else {
            throw new CHttpException(403, 'Estimado Usuario no se han recibido los parametros necesarios para reealizar esta acción.');
        }
    }
    public function actionObtenerDatosPersona(){
        if (Yii::app()->request->isAjaxRequest AND $this->hasQuery('tdocumento_identidad') AND $this->hasQuery('documento_identidad') AND $this->hasQuery('plantel_id')) {
            $modelDocente = new AsignaturaDocente();
            $modelDocente->tdocumento_identidad= $tdocumento_identidad = $this->getQuery('tdocumento_identidad');
            $modelDocente->documento_identidad= $documento_identidad = $this->getQuery('documento_identidad');
            $plantel_id = $this->getIdDecoded($this->getQuery('plantel_id'));
            $autoridadPlantel = '';
            $datosPersona = '';
            $nombresPersona = '';
            $apellidosPersona='';
            $fechaNacimientoPersona='';
            if(in_array($modelDocente->tdocumento_identidad, array('V','E','P'))){
                if(in_array($modelDocente->tdocumento_identidad, array('V','E'))){
                    if(strlen((string)$modelDocente->documento_identidad)>10){
                        $mensaje = 'Estimado usuario el campo Documento de Identidad no puede superar los diez (10) caracteres.';
                        $title='Notificación de Error';
                        echo json_encode(array('statusCode'=>'ERROR','mensaje'=>$mensaje,'title'=>$title));
                        Yii::app()->end();
                    }
                    else {
                        if(!is_numeric($modelDocente->documento_identidad)){
                            $mensaje = 'Estimado usuario el campo Documento de Identidad debe poseer solo caracteres numericos.';
                            $title='Notificación de Error';
                            echo json_encode(array('statusCode'=>'ERROR','mensaje'=>$mensaje,'title'=>$title));
                            Yii::app()->end();
                        }
                    }
                }
                $personalPlantel = new PersonalPlantel();
                $datosPersona = $personalPlantel->existePersonalPlantel($modelDocente->documento_identidad, $modelDocente->tdocumento_identidad,$plantel_id,self::PERIODO_ESCOLAR_ID);
                if($datosPersona){
                    (isset($datosPersona['nombre']))? $nombresPersona=$datosPersona['nombre']:$nombresPersona='';
                    (isset($datosPersona['apellido']))? $apellidosPersona=$datosPersona['apellido']:$apellidosPersona='';
                    echo json_encode(array('statusCode'=>'SUCCESS','nombres'=>$nombresPersona,'apellidos'=>$apellidosPersona));
                    Yii::app()->end();
                }
                else {
                    $mensaje = 'El Documento de Identidad '.$modelDocente->tdocumento_identidad.'-'.$modelDocente->documento_identidad.' no se encuentra registrado en este plantel como Personal Docente o Administrativo.';
                    $title='Notificación de Error';
                    echo json_encode(array('statusCode'=>'ERROR','mensaje'=>$mensaje,'title'=>$title));
                    Yii::app()->end();
                }
            }
            else {
                $mensaje = 'Estimado usuario, se han enviado valores no permitidos en el campo Tipo de Documento de Identidad.';
                $title='Notificación de Error';
                echo json_encode(array('statusCode'=>'ERROR','mensaje'=>$mensaje,'title'=>$title));
                Yii::app()->end();
            }
        }
        else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionGuardarAsignaturaDocente()
    {
        if($this->hasQuery('AsignaturaDocente') AND $this->hasQuery('plantel')) {
            $usuario_id = Yii::app()->user->id;
            $model= new AsignaturaDocente('asignaturaDocente');
            $form=$this->getQuery('AsignaturaDocente');
            $model->unsetAttributes();
            $model->attributes = $form;
            $plantel_id = $this->getIdDecoded($this->getQuery('plantel'));
            $model->seccion_plantel_id = $seccion_plantel_id = $this->getIdDecoded($model->seccion_plantel_id);
            $model->periodo_id =$periodo_id = $this->getIdDecoded($model->periodo_id);
            (isset($form['id']) AND $form != '' AND $form != null) ? $id = $this->getIdDecoded($form['id']):$id = null;
            $rollback = false;
            $existeDocente = false;
            $existeDocentePlantel = false;
            $registrar = false;
            $transaction = Yii::app()->db->beginTransaction();

            try {
                if ($model->validate()) {
                    $docente = new Personal('asignaturaDocente');
                    $autoridadPlantel = new AutoridadPlantel();
                    $docentePlantel = new PersonalPlantel();
                    $docente->tdocumento_identidad = $model->tdocumento_identidad;
                    $docente->documento_identidad = $model->documento_identidad;
                    $docente->nombres = $model->nombres;
                    $docente->apellidos = $model->apellidos;
                    $docente->fecha_nacimiento = $model->fecha_nacimiento;

                    $datosPersona = $autoridadPlantel->busquedaSaimeMixta($model->tdocumento_identidad, $model->documento_identidad);
                    if ($datosPersona) {
                        $existeDocente = $docente->existeDocente($model->documento_identidad, $model->tdocumento_identidad);

                        if (!$existeDocente) {
                            $personal = new Personal();
                            $personal->tdocumento_identidad = $datosPersona['origen'];
                            $personal->documento_identidad = $datosPersona['cedula'];
                            $personal->nombres = $datosPersona['nombre'];
                            $personal->apellidos = $datosPersona['apellido'];
                            $personal->fecha_nacimiento = $datosPersona['fecha_nacimiento'];
                            $personal->sexo = $datosPersona['sexo'];
                            $personal->plantel_id=$plantel_id;
                            $personal->especificacion_estatus_id = 2;
                            $personal->fecha_ini = date('Y-m-d H:i:s');
                            $personal->usuario_ini_id = $usuario_id;
                            $personal->estatus = 'A';

                            if (!$personal->save(false)) {
                                $transaction->rollback();
                                $class = 'errorDialogBox';
                                $message = 'No se ha podido completar la operación, comuniquelo al administrador del sistema para su corrección.';
                                $this->renderPartial('//msgBox', array(
                                        'class' => $class,
                                        'message' => $message,
                                    )
                                );
                                Yii::app()->end();
                            }
                            else {
                                $docentePlantel->personal_id = $personal->id;
                                $registrar = true;
                            }
                        } else {
                            $docentePlantel->personal_id = $existeDocente;
                            $personal_plantel_id = $existeDocentePlantel= $docentePlantel->existeDocentePlantel($existeDocente,$plantel_id,$periodo_id);
                            if(!$existeDocentePlantel){
                                $registrar = true;
                            }
                        }
                        if($registrar){
                            $docentePlantel->usuario_ini_id = $usuario_id;
                            $docentePlantel->estatus='A';
                            $docentePlantel->fecha_ini = date('Y-m-d H:i:s');
                            $docentePlantel->plantel_id = $plantel_id;
                            $docentePlantel->periodo_id = $periodo_id;
                            $docentePlantel->tipo_personal_id = 4;
                            if(!$docentePlantel->save(false)){
                                $transaction->rollback();
                                $class = 'errorDialogBox';
                                $message = 'No se ha podido completar la operación, comuniquelo al administrador del sistema para su corrección.';
                                $this->renderPartial('//msgBox', array(
                                        'class' => $class,
                                        'message' => $message,
                                    )
                                );
                                Yii::app()->end();
                            }
                            else {
                                $personal_plantel_id = $docentePlantel->id;
                            }
                        }
                        $model = $this->loadModel($id);
                        $model->personal_plantel_id = $personal_plantel_id;
                        $model->fecha_act = date('Y-m-d H:i:s');
                        $model->usuario_act_id = $usuario_id;
                        if(!$model->save(false)){
                            $transaction->rollback();
                            $class = 'errorDialogBox';
                            $message = 'No se ha podido completar la operación, comuniquelo al administrador del sistema para su corrección.';
                            $this->renderPartial('//msgBox', array(
                                    'class' => $class,
                                    'message' => $message,
                                )
                            );
                            Yii::app()->end();
                        }

                    } else {
                        $model->addError('documento_identidad', 'El documento de identidad ' . $model->tdocumento_identidad . '-' . $model->documento_identidad . ' no se encuentra registrada en nuestro sistema, por favor contacte al personal de soporte mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>.');
                        $this->renderPartial('//errorSumMsg', array('model' => $model));
                        Yii::app()->end();
                    }
                } else {
                    $this->renderPartial('//errorSumMsg', array('model' => $model));
                    Yii::app()->end();
                }
                if ($rollback == false) {
                    $transaction->commit();
                    $message = 'Estimado usuario, se ha actualizado exitosamente la asignatura. Cierre la ventana para poder continuar actualizando las demas asignaturas.';
                    $class = 'successDialogBox';
                    $this->renderPartial('//msgBox', array(
                            'class' => $class,
                            'message' => $message,
                        )
                    );
                    Yii::app()->end();
                }
            }
            catch (Exception $e) {
                $transaction->rollback();
                $message = 'Estimado usuario, ha ocurrido un error durante el proceso de registro, intente nuevamente. Si el error persiste porfavor comuniquese mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>';
                $class = 'errorDialogBox';
                $this->renderPartial('//msgBox', array(
                        'class' => $class,
                        'message' => $message,
                    )
                );
                Yii::app()->end();
            }
        }
        else {
            throw new CHttpException(403, 'Estimado Usuario no se han recibido los parametros necesarios para reealizar esta acción.');
        }
    }
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return AsignaturaDocente the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=AsignaturaDocente::model()->findByPk($id);
        if($model===null){
            throw new CHttpException(404,'Estimado Usuario, no se ha encontrado el recurso solicitado.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Docente $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='docente-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */

    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $id = base64_encode($id_encoded);
        $informacion=$data->asignatura->nombre;
        $persona_plantel_id = $data['personal_plantel_id'];
        $resultado=CierreAsignacionDocente::model()->getDatos($data["seccion_plantel_id"],self::PERIODO_ESCOLAR_ID);
        $estatus_asig_doc=$resultado['estatus'];

        $columna = '<div class="action-buttons "><div class="pull-left"> ';
        if(Yii::app()->user->pbac('write') OR  Yii::app()->user->pbac('admin')){
            if($estatus_asig_doc==false OR  $estatus_asig_doc==null OR $estatus_asig_doc=='E'){
                $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green edit-docente", "title" => "Asignar Docente",'data-id'=>$id, 'href' => '/planteles/docente/edicion/id/'.$id));
                if($persona_plantel_id>0){
                    $columna .= CHtml::link("", "#", array("class" => "fa icon-trash red", "title" => "Liberar Asignatura", "onClick" => "VentanaDialog('$id','/planteles/docente/liberarDocente','$informacion','liberarDocente','liberarDocente')")) . '&nbsp;&nbsp;';        }
            }
        }
        $columna .= '</div></div>';

        return $columna;
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }
}