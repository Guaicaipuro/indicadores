<?php

class ConsejoEducativoController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de ConsejoEducativoController',
        'write' => 'Creación y Modificación de ConsejoEducativoController',
        'admin' => 'Administración Completa  de ConsejoEducativoController',
        'label' => 'Módulo de ConsejoEducativoController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 
                    'consulta',  
                    'ObtenerMunicipios',
                    'ObtenerParroquias',
                    'ObtenerPoblacion',
                    'ObtenerUrbanizacion',
                    'ObtenerTipoVia',
                    'VerificarPlantel',
                    'VerReporte',
                    'PdfGeneral',
                    'ViaAutoComplete'
                ),
                'pbac' => array('admin'),// Division de comunidades educativas
            ),
            array('allow',
                'actions' => array('lista', 
                    'consulta', 
                    'registro', 
                    'edicion',
                    'ObtenerMunicipios',
                    'ObtenerParroquias',
                    'ObtenerPoblacion',
                    'ObtenerUrbanizacion',
                    'ObtenerTipoVia',
                    'VerificarPlantel',
                    'VerReporte',
                    'ViaAutoComplete'
                    ),
                'pbac' => array('write'),// Director de plantel
            ),
            array('allow',
                'actions' => array('lista', 
                    'consulta',  
                    'ObtenerMunicipios',
                    'ObtenerParroquias',
                    'ObtenerPoblacion',
                    'ObtenerUrbanizacion',
                    'ObtenerTipoVia',
                    'VerificarPlantel',
                    'VerReporte',
                    'PdfGeneral',
                    'ViaAutoComplete'
                    ),
                'pbac' => array('read'),// Jefe de zona
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Lists all models.
     */
    public function actionLista()
    {
        $estado_id='';
        $model=new ConsejoEducativo('search');
        $model->unsetAttributes();  // clear any default values
        $modelPlantel = new Plantel;
        $modelPlantel->unsetAttributes();
        if($this->hasQuery('ConsejoEducativo')){
            $model->attributes=$this->getQuery('ConsejoEducativo');
            if(isset($_REQUEST['Plantel'])){
                $modelPlantel->attributes = $_REQUEST['Plantel'];

                if(isset($_REQUEST['Plantel']['poligonal'] ) ){
                    $modelPlantel->latitud=$_REQUEST['Plantel']['poligonal'];
                }
            }
        }
        if(!Yii::app()->user->pbac('planteles.consejoEducativo.admin')){
            $modelPlantel->estado_id = Yii::app()->user->estado;
            $condicionEstado = 'disabled';
            $_REQUEST['Plantel']['estado_id'] = $modelPlantel->estado_id;
            $municipios =  CHtml::listData( Municipio::model()->findAll(array('condition'=>"estado_id=:id", 'params'=>array(':id'=>$modelPlantel->estado_id))),'id','nombre');
        }else{
            $condicionEstado = '';
            $municipios = array();
        }

        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'modelPlantel'=>$modelPlantel,
            'dataProvider'=>$dataProvider,
            'condicionEstado'=>$condicionEstado,
            'municipios' => $municipios,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new ConsejoEducativo('search');
        $model->unsetAttributes();  // clear any default values
        $modelPlantel = new Plantel;
        $modelPlantel->unsetAttributes();

        if($this->hasQuery('ConsejoEducativo')){
            $model->attributes=$this->getQuery('ConsejoEducativo');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'modelPlantel'=>$modelPlantel,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {
        /* SI $id = 1 entonces la accion consulta se está solicitando desde planteles
        y se utiliza es el id del plantel, de lo contrario se utiliza el id del consejo educativo */
        if($id==1 and isset($_GET['p_id']) and !empty($_GET['p_id']) ){

            $plantel_id = $_GET['p_id'];
            $plantel_id =  $this->getIdDecoded($plantel_id);
            $model  = ConsejoEducativo::model()->findByAttributes(array(),'plantel_id=:plantel_id',array(':plantel_id'=>$plantel_id));
           
        }else{
            $idDecoded = $this->getIdDecoded($id);
            $model = $this->loadModel($idDecoded);
        }

        /* SE CARGA EL MODELO DE TAQUILLA*/
        $taquilla_id = $model->taquilla_id;
        $modelTaquilla=TaquillaConsejo::model()->findByPk($taquilla_id);

        /* BUSQUEDA DE LOS PROYECTOS Y CATEGORIAS QUE POSEE EL CONSEJO EDUCATIVO*/
        $datosProyectos = $model->proyectosAndCategorias();
        $datosProyectos = CHtml::listData($datosProyectos,'nombre','nombre_categoria');
        
        $periodoActivo = Yii::app()->session->get('periodoEscolarActual');
        $matriculaEstudiante = ConsejoEducativo::model()->matriculaEstudiante( $model->plantel_id,$periodoActivo['id']);

        $this->render('view',array(
            'model'=>$model,
            'modelTaquilla'=>$modelTaquilla,
            'datosProyectos'=>$datosProyectos,
            'matriculaEstudiante'=>$matriculaEstudiante

        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro()
    {
        $model=new ConsejoEducativo;
        $modelTaquilla = new TaquillaConsejo;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if($this->hasPost('ConsejoEducativo') || $this->hasPost('TaquillaConsejo'))
        {

            $modelTaquilla->attributes=$this->getPost('TaquillaConsejo');
            $modelTaquilla->beforeInsert();

            /* SE INICIA LA TRANSACCION*/
            $transaction = Yii::app()->db->beginTransaction();

            try{
                
                if($modelTaquilla->validate()){
                    
                    if($modelTaquilla->save()){

                        $taquilla_id = $modelTaquilla->id; // SE OBTIENE EL ID DE LA TAQUILLA REGISTRADA
                        $model->attributes=$this->getPost('ConsejoEducativo');// SE SETEAN LOS ATRIBUTOS

                        /* SE OBTIENE EL ID DEL PLANTEL CODIFICADO Y SE DESCODIFICA Y SE ASIGNA 
                        AL MODELO DE CONSEJO EDUCATIVO*/
                        $plantel_id = $_POST['ConsejoEducativo']['plantel_id'];
                        $model->plantel_id = $this->getIdDecoded($plantel_id);

                        $model->taquilla_id = $taquilla_id;
                        $model->beforeInsert();
                        

                        if($model->validate()){


                            if($model->save()){

                                    $consejo_id = $model->id;

                                    
                                    /* SI EXISTE EL POST DE CATEGORIAS, ES QUE EL CONSEJO EDUCATIVO POSEE
                                    1 O MÁS PROYECTOS*/
                                    if($this->hasPost('Categorias')){

                                        /* SE ALMACENA EN UNA VARIABLE TODOS LOS PROYECTOS SELECCIONADOS POR
                                        EL CONSEJO EDUCATIVO*/
                                        $categoria_post = $this->getPost('Categorias');

                                        /* $categoria_post  ES UN ARRAY EN DONDE LA LLAVE  EL ID DEL PROYECTO
                                        Y EL VALOR ES UN ARRAY QUE CONTIENE LAS CATEGORIAS DE ESE PROYECTO*/
                        

                                        foreach ($categoria_post as $proyecto_id => $categoria_array) {

                                            foreach ($categoria_array as $key => $categoria_id) {
                                                
                                                /* SE CREA UN NUEVO MODELO DE CONSEJOCATEGORIA PARA CADA UNA DE LAS CATEGORIAS
                                                SELECCIONADAS POR EL CONSEJO EDUCATIVO */
                                                $modelConsejoCategoria = new ConsejoCategoria;

                                                $modelConsejoCategoria->consejo_id = $consejo_id;
                                                $modelConsejoCategoria->categoria_id = $categoria_id;
                                                $modelConsejoCategoria->beforeInsert();
                                                
                                                if($modelConsejoCategoria->validate()){

                                                    if($modelConsejoCategoria->save()){

                                                        $this->registerLog('ESCRITURA', 'ConsejoCategoria.registro', 'EXITOSO', 'El Registro de los datos de la ConsejoCategoria se ha efectuado exitósamente. Data-> '.json_encode($modelConsejoCategoria->attributes));

                                                    }// ENDIF CONSEJOCATEGORIA->SAVE
                                                    else{
                                        
                                                        $mensaje = 'Estimado usuario ocurrió un error y no se pudieron almacenar los datos. Por favor recargue la página e intente nuevamente';
                                                        echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
                                                        Yii::app()->end();
                                                    }
                                                }// ENDIF CONSEJOCATEGORIA->VALIDATE
                                                else{
                                    
                                                    $mensaje = 'Estimado usuario ocurrió un error y no se pudieron almacenar los datos. Por favor recargue la página e intente nuevamente';
                                                    echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
                                                    Yii::app()->end();
                                                }
                                            }                                            
                                        }
                                        

                                    }
                                    $ruta = Yii::app()->request->baseUrl;
                                    $id = base64_encode($model->id);
                                    $transaction->commit(); // FIN DE LA TRANSACCION
                                    $this->registerLog('ESCRITURA', 'TaquillaConsejo.registro', 'EXITOSO', 'El Registro de los datos de la TaquillaConsejo se ha efectuado exitósamente. Data-> '.json_encode($modelTaquilla->attributes));
                                    $this->registerLog('ESCRITURA', 'planteles.ConsejoEducativo.registro', 'EXITOSO', 'El Registro de los datos de ConsejoEducativo se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                                    Yii::app()->user->setFlash('success', 'El proceso de registro de los datos se ha efectuado exitosamente');
                                    //$this->redirect('/planteles/consejoEducativo/edicion/id/'.base64_encode($model->id));
                                    //$this->redirect(array('edicion','id'=>base64_encode($model->id),'aux'=>2));
                                    $mensaje = 'El proceso de registro de los datos se ha efectuado exitosamente';
                                    echo json_encode(array('status'=>'success','mensaje'=>$mensaje,'url'=>$ruta,'id'=>$id));
                                    Yii::app()->end();

                            }// ENDIF CONSEJO EDUCATIVO SAVE
                            // SI OCURRE UN ERROR AL GUARDAR EL MODELO DE CONSEJO EDUCATIVO SE ENVIA UN MENSAJE DE ERROR
                            else{
          
                                $mensaje = 'Estimado usuario ocurrió un error y no se pudieron almacenar los datos, recargue la página e intente nuevamente';
                                echo json_encode(array('status'=>'error','mensaje'=>$mensaje,));
                                Yii::app()->end();
                            }

                        }//ENDIF CONSEJO EDUVATIVO VALIDATE
                        // SI HAY ERRORES AL AMACENAR EL MODELO DE CONSEJOEDUCATIVO, SE ENVIAN LOS ERRORES AL FORMULARIO
                        else{

                            $this->renderPartial('//errorSumMsg', array('model' => $model),false,TRUE);
                            Yii::app()->end();
                        }
                        // SI  EXISTEN ERRORES AL GUARDAR EL MODELO DE TAQUILLA SE ENVIA UN MENSAJE DE ERROR 
                    }// ENDIF TAQUILLA SAVE
                    else{

                            $mensaje = 'Estimado usuario ocurrió un error y no se pudieron almacenar los datos, Por favor recargue la página e intente nuevamente';
                            echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
                            Yii::app()->end();
                        }
                }// FIN TAQUILLA VALIDATE
                // SI HAY ERRORES EN EL MODELO DE TAQUILLA, SE ENVIAN LOS ERRORES AL FORMULARIO
                else{

                    $this->renderPartial('//errorSumMsg', array('model' => $modelTaquilla),false,TRUE);
                    Yii::app()->end();
                }
            }// SI NO SE EJECUTA EL TRY ENTRA EN EL CATCH 
            catch (Exception $ex)
            {
                
                $transaction->rollback();
                $this->registerLog('ESCRITURA', 'planteles.ConsejoEducativo.registro', 'FALLIDO', "Ocurrió un error cuando intento registrar los datos del consejo educativo con el Error: {$ex->getMessage()}.");
                $mensaje = 'Estimado usuario ocurrió un error y no se pudieron almacenar los datos, Por favor recargue la página e intente nuevamente.';
                echo json_encode(array('status' => 'error', 'mensaje' => $mensaje));
                Yii::app()->end();
            }
        }

        if(isset($_GET['p_id']) and !empty($_GET['p_id']) ){

            $id = $_GET['p_id'];
            $plantel_id = $this->getIdDecoded($id);
            $modelPlantel = Plantel::model()->findByPk($plantel_id);
            

        }else if(!empty($model->plantel_id) and $model->plantel_id !=''){
            
            $modelPlantel = Plantel::model()->findByPk($model->plantel_id);

        }
        $periodoActivo = Yii::app()->session->get('periodoEscolarActual');
        $matriculaEstudiante = ConsejoEducativo::model()->matriculaEstudiante( $modelPlantel->id,$periodoActivo['id']);
        $bancos = CHtml::listData(Banco::model()->findAll(array('order' => 'nombre ASC','condition' => "estatus='A'")), 'id', 'nombre');
        $estados = CHtml::listData(CEstado::getData(),'id','nombre');
        $proyectos = CHtml::listData(ProyectoConsejo::model()->findAll(array('order' => 'nombre ASC','condition' => "estatus='A'")), 'id', 'nombre');
        $categorias = CHtml::listData(CategoriaConsejo::model()->findAll(array('order' => 'nombre ASC','condition' => "estatus='A'")), 'id', 'nombre','proyecto_id');

        $this->render('create',array(
            'model'=>$model,
            'modelTaquilla'=>$modelTaquilla,
            'modelPlantel'=>$modelPlantel,
            'proyectos'=>$proyectos,
            'bancos'=>$bancos,
            'estados'=>$estados,
            'categorias'=>$categorias,
            'matriculaEstudiante'=>$matriculaEstudiante
        ),false,true);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id)
    {
            $idDecoded = $this->getIdDecoded($id);
            $model = $this->loadModel($idDecoded);
            $taquilla_id = $model->taquilla_id;
            $modelTaquilla=TaquillaConsejo::model()->findByPk($taquilla_id);




            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);
            /*
            if($this->hasPost('ConsejoEducativo') || $this->hasPost('TaquillaConsejo'))
            {

                $model->attributes=$this->getPost('ConsejoEducativo');
                $model->beforeUpdate();
                var_dump($model);
                var_dump($modelTaquilla);die();
                if($model->save()){
                    $this->registerLog('ACTUALIZACION', 'ConsejoEducativo.edicion', 'EXITOSO', 'La Actualización de los datos de ConsejoEducativo se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                    if(Yii::app()->request->isAjaxRequest){
                        $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La actualización de los Datos se ha efectuado de forma exitosa.'));
                        Yii::app()->end();
                    }
                }
            }
            */

        if($this->hasPost('ConsejoEducativo') || $this->hasPost('TaquillaConsejo'))
        {
            /* SE SETEAN LOS ATRIBUTOS DEL MODELO DE TAQUILLA*/
            $modelTaquilla->attributes=$this->getPost('TaquillaConsejo');
            $modelTaquilla->beforeInsert();

            /* SE INICIA LA TRANSACCION*/
            $transaction = Yii::app()->db->beginTransaction();

            try{
                
                if($modelTaquilla->validate()){
                    
                    if($modelTaquilla->save()){

                        $taquilla_id = $modelTaquilla->id;
                        $model->attributes=$this->getPost('ConsejoEducativo');

                        $plantel_id = $_POST['ConsejoEducativo']['plantel_id'];
                        $model->plantel_id = $this->getIdDecoded($plantel_id);

                        $model->taquilla_id = $taquilla_id;
                        $model->beforeInsert();

                        if($model->validate()){

                            if($model->save()){

                                /* SE OBTIENE EL ID DEL CONSEJO EDUCATIVO*/
                                $consejo_id = $model->id;

                                /* 
                                    SE COLOCAN EN ESTATUS='I' TODOS LAS CATEGORIAS QUE POSEA EL
                                    CONSEJO EDUCATIVO EN LA TABLA MUCHOS A MUCHOS DE 
                                    CONSEJOCATEGORIA
                                */
                                ConsejoCategoria::model()->updateAll(array('estatus' => 'I'), array('condition' => 'consejo_id=' . $consejo_id));


                                if($this->hasPost('Categorias')){

                                    /* 
                                        SE ALMACENA EN UNA VARIABLE TODOS LOS PROYECTOS SELECCIONADOS POR
                                        EL CONSEJO EDUCATIVO
                                    */
                                    $categoria_post = $this->getPost('Categorias');


                                    foreach ($categoria_post as $proyecto_id => $categoria_array) {
                                        
                                        /* SE RECORRE TODO EL ARRAY DE LAS CATEGORIAS EN DONDE EL VALUE ES EL ID DE LA CATEGORIA*/
                                        foreach ($categoria_array as $key => $categoria_id) {

                                            /* VERIFICO SI YA EXISTE LA RELACION ENTRE EL CONSEJO QUE SE ESTA ACTUALIZANDO Y LA CATEGORIA*/
                                            if($consejoCategoria_id = $model->existeCategoria($consejo_id,$categoria_id)){

                                                
                                                $modelConsejoCategoria = ConsejoCategoria::model()->findByPk($consejoCategoria_id['id']);

                                                $modelConsejoCategoria->beforeActivate();

                                                if($modelConsejoCategoria->validate()){

                                                    if($modelConsejoCategoria->save()){

                                                        $this->registerLog('ACTUALIZACION', 'ConsejoCategoria.edicion', 'EXITOSO', 'La Actualización de los datos del ConsejoCategoria se ha efectuado exitósamente. Data-> '.json_encode($modelConsejoCategoria->attributes));
                                                    }
                                                    else{
                                            
                                                        $mensaje = 'Estimado usuario ocurrió un error y no se pudieron actualizar los datos. Por favor recargue la página e intente nuevamente';
                                                        echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
                                                        Yii::app()->end();
                                                    }
                                                }
                                                else{
                                        
                                                        $mensaje = 'Estimado usuario ocurrió un error y no se pudieron actualizar los datos. Por favor recargue la página e intente nuevamente';
                                                        echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
                                                        Yii::app()->end();
                                                }
                                            }
                                            else{//SI NO SE ENCUENTRA LA RELACION CONSEJO CON CATEGORIA ENTONCES ES UN NUEVO REGISTRO

                                                $modelConsejoCategoria = new ConsejoCategoria;
                                                $modelConsejoCategoria->consejo_id = $consejo_id;
                                                $modelConsejoCategoria->categoria_id = $categoria_id;
                                                $modelConsejoCategoria->beforeInsert();
                                                
                                                if($modelConsejoCategoria->validate()){

                                                    if($modelConsejoCategoria->save()){

                                                        $this->registerLog('ESCRITURA', 'ConsejoCategoria.registro', 'EXITOSO', 'El Registro de los datos de la ConsejoCategoria se ha efectuado exitósamente. Data-> '.json_encode($modelConsejoCategoria->attributes));

                                                    }
                                                    else{
                                        
                                                        $mensaje = 'Estimado usuario ocurrió un error y no se pudieron almacenar los datos. Por favor recargue la página e intente nuevamente';
                                                        echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
                                                        Yii::app()->end();
                                                    }
                                                }
                                                else{
                                    
                                                    $mensaje = 'Estimado usuario ocurrió un error y no se pudieron almacenar los datos. Por favor recargue la página e intente nuevamente';
                                                    echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
                                                    Yii::app()->end();
                                                }                                            
                                            }
                                        }
                                    }       
                                }
                                
                                $transaction->commit();
                                $this->registerLog('ACTUALIZACION', 'TaquillaConsejo.edicion', 'EXITOSO', 'La Actualización de los datos de la TaquillaConsejo se ha efectuado exitósamente. Data-> '.json_encode($modelTaquilla->attributes));
                                $this->registerLog('ACTUALIZACION', 'ConsejoEducativo.edicion', 'EXITOSO', 'La Actualización de los datos de ConsejoEducativo se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                                $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La actualización de los Datos se ha efectuado de forma exitosa.'));
                                Yii::app()->end();

                            }// ENDIF MODEL->SAVE
                            // SI OCURRE UN ERROR AL GUARDAR EL MODELO DE CONSEJO EDUCATIVO SE ENVIA UN MENSAJE DE ERROR
                            else{
          
                                $mensaje = 'Estimado usuario ocurrió un error y no se pudieron almacenar los datos, recargue la página e intente nuevamente';
                                echo json_encode(array('status'=>'error','mensaje'=>$mensaje,));
                                Yii::app()->end();
                            }

                        }// ENDIF MODEL->VALIDATE
                        // SI HAY ERRORES AL AMACENAR EL MODELO DE CONSEJOEDUCATIVO, SE ENVIAN LOS ERRORES AL FORMULARIO
                        else{

                            $this->renderPartial('//errorSumMsg', array('model' => $model),false,TRUE);
                            Yii::app()->end();
                        }
                        // SI  EXISTEN ERRORES AL GUARDAR EL MODELO DE TAQUILLA SE ENVIA UN MENSAJE DE ERROR 
                    }// ENDIF MODELTAQUILLA->SAVE
                    else{

                            $mensaje = 'Estimado usuario ocurrió un error y no se pudieron almacenar los datos, Por favor recargue la página e intente nuevamente';
                            echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
                            Yii::app()->end();
                        }
                }// ENDIF MODELTAQUILLA->VALIDATE
                // SI HAY ERRORES EN EL MODELO DE TAQUILLA, SE ENVIAN LOS ERRORES AL FORMULARIO
                else{

                    $this->renderPartial('//errorSumMsg', array('model' => $modelTaquilla),false,TRUE);
                    Yii::app()->end();
                }
            }
            catch (Exception $ex)
            {
                
                $transaction->rollback();
                $this->registerLog('ESCRITURA', 'planteles.ConsejoEducativo.registro', 'FALLIDO', "Ocurrió un error cuando intento registrar los datos del consejo educativo con el Error: {$ex->getMessage()}.");
                $mensaje = 'Estimado usuario ocurrió un error y no se pudieron almacenar los datos, Por favor recargue la página e intente nuevamente.';
                echo json_encode(array('status' => 'error', 'mensaje' => $mensaje));
                Yii::app()->end();
            }
        }
            $bancos = CHtml::listData(Banco::model()->findAll(array('order' => 'nombre ASC','condition' => "estatus='A'")), 'id', 'nombre');
            $estados = CHtml::listData(CEstado::getData(),'id','nombre');
            $proyectos = CHtml::listData(ProyectoConsejo::model()->findAll(array('order' => 'nombre ASC','condition' => "estatus='A'")), 'id', 'nombre');

            $categorias = CHtml::listData(CategoriaConsejo::model()->findAll(array('order' => 'nombre ASC','condition' => "estatus='A'")), 'id', 'nombre','proyecto_id');
            $categorias_seleccionadas = CHtml::listData($model->categoriaSeleccionada($model->id),'id','id');
            
            $municipios =  CHtml::listData( Municipio::model()->findAll(array('condition'=>"estado_id=:id", 'params'=>array(':id'=>$modelTaquilla->estado_id))),'id','nombre');
            $parroquias =  CHtml::listData( Parroquia::model()->findAll(array('condition'=>"municipio_id=:id", 'params'=>array(':id'=>$modelTaquilla->municipio_id))),'id','nombre');
            $poblacion = CHtml::listData(ConsejoEducativo::model()->obtenerPoblacion($modelTaquilla->parroquia_id), 'id', 'nombre');
            $urbanizacion = CHtml::listData(ConsejoEducativo::model()->obtenerUrbanizacion($modelTaquilla->parroquia_id), 'id', 'nombre');
            $tipoVia = CHtml::listData(ConsejoEducativo::model()->obtenerTipoVia(), 'id', 'nombre');


            if(isset($_REQUEST['p_id']) and !empty($_REQUEST['p_id']) ){

                $id = $_GET['p_id'];
                $plantel_id = $this->getIdDecoded($id);
                $modelPlantel = Plantel::model()->findByPk($plantel_id);

            }else if(!empty($model->plantel_id) and $model->plantel_id !=''){

                $modelPlantel = Plantel::model()->findByPk($model->plantel_id);
            }

            $periodoActivo = Yii::app()->session->get('periodoEscolarActual');
            $matriculaEstudiante = ConsejoEducativo::model()->matriculaEstudiante( $modelPlantel->id,$periodoActivo['id']);


                $this->render('update',array(
                    'model'=>$model,
                    'modelPlantel'=>$modelPlantel,
                    'bancos'=>$bancos,
                    'estados'=>$estados,
                    'proyectos'=>$proyectos,
                    'categorias'=>$categorias,
                    'modelTaquilla'=>$modelTaquilla,
                    'categorias_seleccionadas'=>$categorias_seleccionadas,
                    'municipios'=>$municipios,
                    'parroquias'=>$parroquias,
                    'poblacion'=>$poblacion,
                    'urbanizacion'=>$urbanizacion,
                    'tipoVia'=>$tipoVia,
                    'matriculaEstudiante'=>$matriculaEstudiante
                ),false,true);

    }

    public function actionVerReporte($id)
    {
        /* SI $id = 1 entonces la accion verReporte se está solicitando desde planteles
        y se utiliza es el id del plantel, de lo contrario se utiliza el id del consejo educativo */
        if($id==1 and isset($_GET['p_id']) and !empty($_GET['p_id']) ){

            $plantel_id = $_GET['p_id'];
            $plantel_id =  $this->getIdDecoded($plantel_id);
            $model  = ConsejoEducativo::model()->findByAttributes(array(),'plantel_id=:plantel_id',array(':plantel_id'=>$plantel_id));
           
        }else{
            $idDecoded = $this->getIdDecoded($id);
            $model = $this->loadModel($idDecoded);
        }

        // PROYECTOS Y CATEGORIAS DEL CONSEJO EDUCATIVO
        $datosProyectos = $model->proyectosAndCategorias();
        $datosProyectos = CHtml::listData($datosProyectos,'nombre','nombre_categoria');
        
        $mktime = mktime(0, 0, 0, date("m")  , date("d"), date("Y")-1);
        $hoy = date('Y-m-d',$mktime); // $hoy es un año anterior al dia de hoy

         /*header del pdf*/
        $header = $this->renderPartial('verReporte_header',array(),true);

        /* body del pdf*/
        $body = $this->renderPartial('verReporte',array(
            'model'=>$model,
            'datosProyectos'=>$datosProyectos,
            'hoy' => $hoy,
        ),true);


        $modulo = 'planteles.ConsejoEducativo.reporte';
        $ip = Yii::app()->request->userHostAddress;
        $username = Yii::app()->user->name;
        $usuario_id = Yii::app()->user->id;
        $fecha_actual = date('d-m-y');

        $name = 'Consejo Educativo';// Nombre del pdf
        $mpdf = new mpdf('', 'LEGAL', 0, '', 15, 15, 35, 50); // Creo un Nuevo Archivo PDF
        $mpdf->setHTMLFooter('<br>' . '<p style="text-align:center;"> {PAGENO} / {nb}</p> <p style="text-align:right;">'.$fecha_actual.'</p>'); //Footer del PDF
        $mpdf->SetFont('sans-serif');
        $mpdf->SetHTMLHeader($header);
        $mpdf->WriteHTML($body); //Escritura del Codigo HTML y PHP
        $mpdf->Output($name . '.pdf', 'I'); //SALIDA DEL PDF, I->Mostrar en Página Web, D-> Descarga Directa
        $this->registerLog('REPORTES', $modulo, 'EXITOSO', 'Generación de un Reporte de los Datos del Consejo educativo', $ip, $usuario_id, $username);        
    }

    /* FUNCION PARA GENERAR EL PDF DE LA BUSQUEDA FILTRADA DEL CONSEJO EDUCATIVO*/
    public function actionPdfGeneral(){

        if(isset(Yii::app()->session['dataProvider']) 
            AND !empty(Yii::app()->session['dataProvider']))
        {
            /*$dataProvider contiene los datos de la ultima busqueda del consejo educativo*/
            $dataProvider = Yii::app()->session['dataProvider']->getData();

            $contador=count($dataProvider);

            $mktime = mktime(0, 0, 0, date("m")  , date("d"), date("Y")-1);
            $hoy = date('Y-m-d',$mktime); // $hoy es un año anterior al dia de hoy

            $actualizado = 0;
            $no_actualizado = 0;

            for ($i=0; $i <$contador ; $i++) { 

                if($dataProvider[$i]['fecha_registro'] >= $hoy){
                    
                    $actualizado++;
                }else if($dataProvider[$i]['fecha_registro'] < $hoy){
                     
                    $no_actualizado++;
                }             
            }


            $header = $this->renderPartial('_pdfGeneral_header',
                array(),true);

            $modulo = 'planteles.ConsejoEducativo.pdfGeneral';
            $ip = Yii::app()->request->userHostAddress;
            $username = Yii::app()->user->name;
            $usuario_id = Yii::app()->user->id;
            $fecha_actual = date('d-m-y');

            $name = 'Búsqueda de Consejos Educativos';
            $mpdf = new mpdf('', 'LEGAL-L', 0, '', 15, 15, 55, 30); // Creo un Nuevo Archivo PDF
            $mpdf->setHTMLFooter('<br>' . '<p style="text-align:center;"> {PAGENO} / {nb}</p> <p style="text-align:right;">'.$fecha_actual.'</p>'); //Footer del PDF
            $mpdf->SetFont('sans-serif');
            $mpdf->SetHTMLHeader($header);


            $body = $this->renderPartial('_pdfGeneral',
                array(
                    'contador'=>$contador,
                    'actualizado'=>$actualizado,
                    'no_actualizado'=>$no_actualizado,
                ),true
            );

            $body.= $this->renderPartial('_pdfGeneral_table_header',
                array(),true
            );

            if($contador>0){
                for ($i=0; $i <$contador ; $i++) { 
                    $modelPlantel = Plantel::model()->findByPk($dataProvider[$i]['plantel_id']);
                    $body.= $this->renderPartial('_pdfGeneral_table',
                        array(
                            'dataProvider'=>$dataProvider[$i],
                            'hoy'=>$hoy,
                            'modelPlantel'=>$modelPlantel
                        ),true
                    );
                }
            }else{
                $body.='<tr><td colspan="7" style="font-weight: bold;">No se encontraron resultados</td></tr>';
            }

            $body .='</table>';
            $mpdf->WriteHTML($body);

            $mpdf->Output($name . '.pdf', 'D'); //SALIDA DEL PDF, I->Mostrar en Página Web, D-> Descarga Directa
            $this->registerLog('REPORTES', $modulo, 'EXITOSO', 'Generación de un Reporte de la Búsquedas de Consejos Educativos', $ip, $usuario_id, $username);               

        }else{
            throw new CHttpException(404,'The requested page does not exist.');
        }


    }
        
    /**
     * Logical Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        
        if($model){
            $model->beforeDelete();
            if($model->save()){
                $this->registerLog('ELIMINACION', 'planteles.ConsejoEducativo.eliminacion', 'EXITOSO', 'La Eliminación de los datos de ConsejoEducativo se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La eliminación del registro se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }

        $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
    }
    
    /**
     * Activation of a particular model Logicaly Deleted.
     * If activation is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be activated
     */
    public function actionActivacion($id){
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        if($model){
            $model->beforeActivate();
            if($model->save()){
                $this->registerLog('ACTIVACION', 'planteles.ConsejoEducativo.activacion', 'EXITOSO', 'La Activación de los datos de ConsejoEducativo se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La activación de este registro se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }
        $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ConsejoEducativo the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if(is_numeric($id)){
            $model=ConsejoEducativo::model()->findByPk($id);
            if($model===null){
                throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
        }
        else{
            return null;
        }
    }

    /**
     * Performs the AJAX validation.
     * @param ConsejoEducativo $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='consejo-educativo-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $id = base64_encode($id_encoded);
        $columna = '<div class="action-buttons">';
        $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/planteles/consejoEducativo/consulta/id/'.$id)) . '&nbsp;&nbsp;';
        if($data->estatus=='A'){
            //$columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '/planteles/consejoEducativo/edicion/id/'.$id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-file orange", "title" => "Reporte",'target'=>"_blank", 'href' => '/planteles/consejoEducativo/VerReporte/id/'.$id)) . '&nbsp;&nbsp;';
            //$columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Eliminar", 'href' => '/planteles/consejoEducativo/eliminacion/id/'.$id)) . '&nbsp;&nbsp;';
        }else{
            //$columna .= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar", 'href' => '/planteles/consejoEducativo/activacion/id/'.$id)) . '&nbsp;&nbsp;';
        }
        $columna .= '</div>';
        return $columna;
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }

    /*****************Funcion para obtener los municipios de un estado especifico **********************/
    public function actionObtenerMunicipios(){
        
        if(isset($_REQUEST['id'])){
            $id = $_REQUEST['id'];
        }
        else if(isset($_REQUEST['Plantel']['estado_id'])){
            $id = $_REQUEST['Plantel']['estado_id'];
        }
        if(isset($id) and !empty($id)){
            $resultado= Municipio::model()->findAll(array('condition'=>"estado_id=:id", 'params'=>array(':id'=>$id)));
            $lista = CHtml::listData($resultado, 'id', 'nombre');

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('--SELECCIONE--'), true);

            foreach ($lista as $valor => $descripcion) {

                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }

    }
    /***************************************************************************************************/

    /*****************Funcion para obtener las parroquias de un municipio especifico **********************/
    public function actionObtenerParroquias(){

        if(isset($_REQUEST['id'])){
            $id = $_REQUEST['id'];
        }
        else if(isset($_REQUEST['Plantel']['estado_id'])){
            $id = $_REQUEST['Plantel']['municipio_id'];
        }

        if(isset($id) and !empty($id)){
            $resultado= Parroquia::model()->findAll(array('condition'=>"municipio_id=:id", 'params'=>array(':id'=>$id)));
            $lista = CHtml::listData($resultado, 'id', 'nombre');

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('--SELECCIONE--'), true);

            foreach ($lista as $valor => $descripcion) {

                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }

    }
    /***************************************************************************************************/

    /*****************Funcion para obtener la poblacion de una parroquia especifica **********************/
    public function actionObtenerPoblacion() {
        
            $parroquia = $_REQUEST['id'];

            $lista = ConsejoEducativo::model()->obtenerPoblacion($parroquia);
            $lista = CHtml::listData($lista, 'id', 'nombre');   

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    /***************************************************************************************************/

    /*****************Funcion para obtener la urbanización de una población especifica **********************/
    public function actionObtenerUrbanizacion() {
        
            $parroquia = $_REQUEST['id'];

            $lista = ConsejoEducativo::model()->obtenerUrbanizacion($parroquia);
            $lista = CHtml::listData($lista, 'id', 'nombre');   

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    /***************************************************************************************************/
 
 /*****************Funcion para obtener el tipo de via **********************/
    public function actionObtenerTipoVia() {
        


            $lista = ConsejoEducativo::model()->obtenerTipoVia();
            $lista = CHtml::listData($lista, 'id', 'nombre');   

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    /******************************************************************************************/

    public function actionViaAutoComplete() {
        $id = $_GET["id"];

        $_GET['term'] = strtoupper($_GET['term']);
        $res = array();

        if (isset($_GET['term']) && !empty($id)) {
            $res = Plantel::obtenerVia($id, $_GET['term']);
        }

        echo CJSON::encode($res);
    }


    /* FUNCION PARA VERIFICAR SI EL PLANTEL YA POSEE UN CONSEJO EDUCATIVO*/
    public function actionVerificarPlantel()
    {

        if(isset($_POST['plantel']) and !empty($_POST['plantel']))
        {

            $plantel_id = $_POST['plantel'];
            $plantel_id = $this->getIdDecoded($plantel_id);

            if($respuesta = ConsejoEducativo::model()->plantelHasConsejo($plantel_id) )
            {

                $consejo_id = base64_encode($respuesta['id']);

                $mensaje = 'Este plantel ya posee un Consejo Educativo. ¿ Desea actualizar los datos del Consejo Educativo ?';
                echo json_encode(array('status'=>'success','url'=>$consejo_id));
                Yii::app()->end();
            }
            else{
                echo json_encode(array('status'=>'error'));
                Yii::app()->end();
            }
        }
    }

    public function columnaEstatus($data) {
        $estatus = $data['estatus'];
        if (($estatus == 'A') || ($estatus == '')) {
            $columna = 'Activo';
        } else if ($estatus == 'I') {
            $columna ='Inactivo';
        }
        return $columna;
    }

    /* FUNCION QUE SE ACTIVA DESDE EL CGRIDVIEW DE LA LISTA DE CONSEJOS EDUCATIVOS 
    PARA DETERMINAR SI SE EL CONSEJO EDUCATIVO SE ENCUENTRA ACTUALIZADO*/
    public function columnaActualizado($data) {
        // SE OBTIENE LA ULTIMA FECHA DE REGISTRO EN LA TAQUILLA
        $fecha_registro = $data['fecha_registro'];

        /*SE OBTIENE EL AÑO ANTETIOR AL DIA ACTUAL
            EJ: SI la fecha actual es 27-04-2015
            $hoy = 27-04-2014
        */
        $mktime = mktime(0, 0, 0, date("m")  , date("d"), date("Y")-1);
        $hoy = date('Y-m-d',$mktime);

        if($fecha_registro >= $hoy){
            /*
                SI LA FECHA DE REGISTRO ES MAYOR O IGUAL EL AÑO ANTETIOR AL DIA ACTUAL
                ENTONCES EL CONSEJO ESTA ACTUALIZADO
            */
            $columna = 'Actualizado';
        }else if($fecha_registro < $hoy){
            $columna = 'No Actualizado';
        }
        return $columna;
    }

}
