<?php

class EscolaridadController extends Controller
{
    const MODULO = "Planteles.Escolaridad";

    public $defaultAction = 'registro';
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'write' => 'Registrar Escolaridad',
        'label' => 'Registro de Escolaridad periodo 2014 en adelante'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('registro','buscarEstudiante'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionRegistro()
    {
        $modelSeccion = new SeccionPlantel();
        $modelPlantel = new Plantel();
        $escolaridadForm = new EscolaridadForm('regEscolaridad');
        if($this->hasPost('EscolaridadForm')){
            $escolaridadForm->attributes=$form=$this->getPost('EscolaridadForm');
            if($this->hasPost('inscripcion_regular')){
                $escolaridadForm->inscripcion_regular=$this->getPost('inscripcion_regular',0);
            }
            if($this->hasPost('repitiente')){
                $escolaridadForm->repitiente=$this->getPost('repitiente',0);
            }
            if($this->hasPost('materia_pendiente')){
                $escolaridadForm->materia_pendiente=$this->getPost('materia_pendiente',0);
            }
            if($this->hasPost('doble_inscripcion')){
                $escolaridadForm->doble_inscripcion=$this->getPost('doble_inscripcion',0);
            }
            if($this->hasPost('observaciones')){
                $escolaridadForm->observacion=$this->getPost('observaciones',0);
            }
            if($escolaridadForm->validate()){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $estudiantes[] = $escolaridadForm->estudiante_id;
                    $inscripcion_regular_array[] = (int) $escolaridadForm->inscripcion_regular;
                    $doble_inscripcion_array[] = (int) $escolaridadForm->doble_inscripcion;
                    $materia_pendiente_array [] = (int)$escolaridadForm->materia_pendiente;
                    $observacion_array [] = $escolaridadForm->observacion;
                    $repitiente_array [] = (int) $escolaridadForm->repitiente;

                    $estudiantes_pg_array = $this->to_pg_array($estudiantes);
                    $doble_inscripcion_pg_array = $this->to_pg_array($doble_inscripcion_array);
                    $inscripcion_regular_pg_array = $this->to_pg_array($inscripcion_regular_array);
                    $materia_pendiente_pg_array = $this->to_pg_array($materia_pendiente_array);
                    $observacion_pg_array = $this->to_pg_array($observacion_array);
                    $repitiente_pg_array = $this->to_pg_array($repitiente_array);

                    $modulo = self::MODULO . '.Registro';
                    $resultadoInscripcion = Estudiante::model()->inscribirEstudiantes($estudiantes_pg_array, $escolaridadForm->plantel_id, $escolaridadForm->seccion_plantel_id, $escolaridadForm->periodo_id, $modulo, $inscripcion_regular_pg_array, $doble_inscripcion_pg_array, $repitiente_pg_array, $materia_pendiente_pg_array, $observacion_pg_array);

                    $transaction->commit();
                    $this->registerLog('ESCRITURA', self::MODULO . 'Registro', 'EXITOSO', 'Ha creado la escolaridad para el siguiente caso :'.json_encode($escolaridadForm));
                    Yii::app()->user->setFlash('success', 'Estimado usuario, se ha registrado existamente la escolaridad para el estudiante <strong>'.$escolaridadForm->nombres.' '.$escolaridadForm->apellidos.'</strong>');
                } catch (Exception $ex) {
                    $transaction->rollback();
                    $this->registerLog('ESCRITURA', self::MODULO . 'Registro', 'FALLIDO', 'Ha fallado el registro de la escolaridad para el siguiente caso :'.json_encode($escolaridadForm));
                    Yii::app()->user->setFlash('error', 'Estimado usuario, se ha ocurrido un error durante el registro de la escolaridad para el estudiante <strong>'.$escolaridadForm->nombres.' '.$escolaridadForm->apellidos.'</strong>');
                }
                $this->redirect('/planteles/escolaridad/registro/id/'.base64_encode($escolaridadForm->seccion_plantel_id).'/plantel/'.base64_encode($escolaridadForm->plantel_id).'/periodo/'.base64_encode($escolaridadForm->periodo_id));
            }
            else
            {
                $existeSeccion = $modelSeccion->existeSeccionPlantel($escolaridadForm->seccion_plantel_id, $escolaridadForm->plantel_id);
                if ($existeSeccion != 0) {
                    $datosSeccion = $modelSeccion->cargarDetallesSeccion($escolaridadForm->seccion_plantel_id, $escolaridadForm->plantel_id);
                    $datosPlantel = $modelPlantel->obtenerDatosIdentificacion($escolaridadForm->plantel_id);
                    $this->render('index', array('datosSeccion' => $datosSeccion, 'datosPlantel' => $datosPlantel, 'seccion_plantel_id' => base64_encode($escolaridadForm->seccion_plantel_id), 'periodo_id' => base64_encode($escolaridadForm->periodo_id), 'plantel_id' => base64_encode($escolaridadForm->plantel_id), 'escolaridadForm' => $escolaridadForm));
                }
                else {
                    throw new CHttpException(403, 'Estimado usuario, se han suministrado datos invalidos. Intente nuevamente.');
                }
            }
        }
        else {
            if($this->hasQuery('id') AND $this->hasQuery('plantel') AND $this->hasQuery('periodo')) {
                $escolaridadForm->seccion_plantel_id=$seccion_plantel_id = $this->getIdDecoded($this->getQuery('id'));
                $escolaridadForm->plantel_id=$plantel_id = $this->getIdDecoded($this->getQuery('plantel'));
                $escolaridadForm->periodo_id=$periodo_id = $this->getIdDecoded($this->getQuery('periodo'));

                $existeSeccion = $modelSeccion->existeSeccionPlantel($seccion_plantel_id, $plantel_id);
                if ($existeSeccion != 0) {
                    $datosSeccion = $modelSeccion->cargarDetallesSeccion($seccion_plantel_id, $plantel_id);
                    $datosPlantel = $modelPlantel->obtenerDatosIdentificacion($plantel_id);
                    $this->render('index', array('datosSeccion' => $datosSeccion, 'datosPlantel' => $datosPlantel, 'seccion_plantel_id' => base64_encode($seccion_plantel_id), 'periodo_id' => base64_encode($periodo_id), 'plantel_id' => base64_encode($plantel_id), 'escolaridadForm' => $escolaridadForm));
                }
                else {
                    throw new CHttpException(403, 'Estimado usuario, se han suministrado datos invalidos. Intente nuevamente.');
                }
            }
            else {

                throw new CHttpException(403, 'Estimado usuario, se han suministrado datos invalidos. Intente nuevamente.');
            }

        }

    }

    public function actionBuscarEstudiante() {
        $documento_identidad = $this->getQuery('documento_identidad');
        $tdocumento_identidad = $this->getQuery('tdocumento_identidad');
        $periodo_id = $this->getQuery('periodo_id');
        $modelEstudiante = new Estudiante();
        $estaMatriculado = false;
        $busquedaCedula = null;
        $estudiante = array();
        if ($documento_identidad AND $tdocumento_identidad AND $periodo_id) {
            if (in_array($tdocumento_identidad, array('V', 'E', 'P','C'))) {
                $estudiantes = new Estudiante;
                if($tdocumento_identidad=='C'){
                    $validaCedula = Estudiante::model()->findByAttributes(array('cedula_escolar' => $documento_identidad));
                }
                else {
                    $validaCedula = Estudiante::model()->findByAttributes(array('documento_identidad' => $documento_identidad, 'tdocumento_identidad' => $tdocumento_identidad));
                }
                $estudiante_id = (is_object($validaCedula) AND isset($validaCedula->id)) ? $validaCedula->id : null;
                if (!is_null($estudiante_id)) {
                    $estudiante[]=$estudiante_id;
                    $estaMatriculado = $modelEstudiante->estaMatriculado($estudiante,$periodo_id,true);
                    if($estaMatriculado==$estudiante_id){
                        $mensaje = "Estimado usuario, el estudiante que desea escolarizar se encuentra inscrito en este periodo. Por favor revise el <strong>Historico de Estudiante</strong>";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                    }
                    else {
                        $datosEstudiante = $modelEstudiante->getDatosEstudiante($estudiante_id,true);
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'SUCCESS', 'datosEstudiante'=>$datosEstudiante));
                    }
                } else {
                    $mensaje = "Estimado usuario, el estudiante que desea escolarizar no se encuentra registrado en el sistema. Verifique que dicho estudiante tenga actualizado el <strong>Tipo de Documento de Identidad</strong>.";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                }
            } else {
                $mensaje = "Estimado usuario, seleccione un Tipo de Documento valido.";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
            }
        } else {
            $mensaje = "ERROR!!!!! No ha ingresado los parámetros necesarios para cumplir con la respuesta a su petición.";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje)); //
        }
        Yii::app()->end();
    }
    public function to_pg_array($set) {
        settype($set, 'array'); // can be called with a scalar or array
        $result = array();
        foreach ($set as $t) {
            if (is_array($t)) {
                $result[] = to_pg_array($t);
            } else {
                $t = str_replace('"', '\\"', $t); // escape double quote
                if (!is_numeric($t)) // quote only non-numeric values
                    $t = '"' . $t . '"';
                $result[] = $t;
            }
        }
        return '{' . implode(",", $result) . '}'; // format
    }
    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }
}