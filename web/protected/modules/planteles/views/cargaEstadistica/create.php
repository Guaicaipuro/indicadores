<?php
/* @var $this CargaEstadisticaController */
/* @var $model CargaEstadistica */

$this->pageTitle = 'Registro de Carga Estadisticas';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Carga Estadisticas'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>