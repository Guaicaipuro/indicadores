<?php
/* @var $this CargaEstadisticaController */
/* @var $model CargaEstadistica */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'carga-estadistica-form',
                                'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-result">
                        <?php
           if($model->hasErrors()):
               $this->renderPartial('//errorSumMsg', array('model' => $model));
           elseif(Yii::app()->user->hasFlash('success')):               $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => Yii::app()->user->getFlash('success')));
           else:
                ?>
                        <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                <?php
                   endif;
               ?>
                        </div>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'plantel_id'); ?>
                                                            <?php echo $form->textField($model,'plantel_id', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'periodo_id'); ?>
                                                            <?php echo $form->textField($model,'periodo_id', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'tipo_atencion_id'); ?>
                                                            <?php echo $form->textField($model,'tipo_atencion_id', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'mat_inicial_m'); ?>
                                                            <?php echo $form->textField($model,'mat_inicial_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'mat_incorporacion_m'); ?>
                                                            <?php echo $form->textField($model,'mat_incorporacion_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'mat_abandono_m'); ?>
                                                            <?php echo $form->textField($model,'mat_abandono_m', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'mat_final_m'); ?>
                                                            <?php echo $form->textField($model,'mat_final_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'mat_inicial_f'); ?>
                                                            <?php echo $form->textField($model,'mat_inicial_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'mat_incorporacion_f'); ?>
                                                            <?php echo $form->textField($model,'mat_incorporacion_f', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'mat_abandono_f'); ?>
                                                            <?php echo $form->textField($model,'mat_abandono_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'mat_final_f'); ?>
                                                            <?php echo $form->textField($model,'mat_final_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pre_inicial_m'); ?>
                                                            <?php echo $form->textField($model,'pre_inicial_m', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pre_incorporacion_m'); ?>
                                                            <?php echo $form->textField($model,'pre_incorporacion_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pre_abandono_m'); ?>
                                                            <?php echo $form->textField($model,'pre_abandono_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pre_final_m'); ?>
                                                            <?php echo $form->textField($model,'pre_final_m', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pre_inicial_f'); ?>
                                                            <?php echo $form->textField($model,'pre_inicial_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pre_incorporacion_f'); ?>
                                                            <?php echo $form->textField($model,'pre_incorporacion_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pre_abandono_f'); ?>
                                                            <?php echo $form->textField($model,'pre_abandono_f', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pre_final_f'); ?>
                                                            <?php echo $form->textField($model,'pre_final_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pri_inicial_m'); ?>
                                                            <?php echo $form->textField($model,'pri_inicial_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pri_incorporacion_m'); ?>
                                                            <?php echo $form->textField($model,'pri_incorporacion_m', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pri_abandono_m'); ?>
                                                            <?php echo $form->textField($model,'pri_abandono_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pri_final_m'); ?>
                                                            <?php echo $form->textField($model,'pri_final_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pri_inicial_f'); ?>
                                                            <?php echo $form->textField($model,'pri_inicial_f', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pri_incorporacion_f'); ?>
                                                            <?php echo $form->textField($model,'pri_incorporacion_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pri_abandono_f'); ?>
                                                            <?php echo $form->textField($model,'pri_abandono_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pri_final_f'); ?>
                                                            <?php echo $form->textField($model,'pri_final_f', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'meg_inicial_m'); ?>
                                                            <?php echo $form->textField($model,'meg_inicial_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'meg_incorporacion_m'); ?>
                                                            <?php echo $form->textField($model,'meg_incorporacion_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'meg_abandono_m'); ?>
                                                            <?php echo $form->textField($model,'meg_abandono_m', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'meg_final_m'); ?>
                                                            <?php echo $form->textField($model,'meg_final_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'meg_inicial_f'); ?>
                                                            <?php echo $form->textField($model,'meg_inicial_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'meg_incorporacion_f'); ?>
                                                            <?php echo $form->textField($model,'meg_incorporacion_f', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'meg_abandono_f'); ?>
                                                            <?php echo $form->textField($model,'meg_abandono_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'meg_final_f'); ?>
                                                            <?php echo $form->textField($model,'meg_final_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'met_inicial_m'); ?>
                                                            <?php echo $form->textField($model,'met_inicial_m', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'met_incorporacion_m'); ?>
                                                            <?php echo $form->textField($model,'met_incorporacion_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'met_abandono_m'); ?>
                                                            <?php echo $form->textField($model,'met_abandono_m', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'met_final_m'); ?>
                                                            <?php echo $form->textField($model,'met_final_m', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'met_inicial_f'); ?>
                                                            <?php echo $form->textField($model,'met_inicial_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'met_incorporacion_f'); ?>
                                                            <?php echo $form->textField($model,'met_incorporacion_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'met_abandono_f'); ?>
                                                            <?php echo $form->textField($model,'met_abandono_f', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'met_final_f'); ?>
                                                            <?php echo $form->textField($model,'met_final_f', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_elim'); ?>
                                                            <?php echo $form->textField($model,'fecha_elim',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'estatus'); ?>
                                                            <?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/cargaEstadistica"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                    <div class="col-md-6 wizard-actions">
                                        <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                            Guardar
                                            <i class="icon-save icon-on-right"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <div id="resultDialog" class="hide"></div>

        <?php
            /**
             * Yii::app()->clientScript->registerScriptFile(
             *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/CargaEstadisticaController/carga-estadistica/form.js',CClientScript::POS_END
             *);
             */
        ?>
    </div>
</div>
