<?php
/**
 * Created by PhpStorm.
 * User: isalaz01
 * Date: 18/08/15
 * Time: 10:24 AM
 */
?>
<?php if (Yii::app()->user->hasFlash('exito')): ?>
    <div class="successDialogBox">
        <p><?php echo Yii::app()->user->getFlash('exito'); ?></p>
    </div>
<?php endif; ?>
<?php if (Yii::app()->user->hasFlash('error')): ?>
    <div class="errorDialogBox">
        <p><?php echo Yii::app()->user->getFlash('error'); ?></p>
    </div>
<?php endif; ?>
<div class = "widget-box collapsed">

    <div class = "widget-header">
        <h5>Identificación del Plantel <?php echo '"' . $datosPlantel['nom_plantel'] . '"'; ?></h5>

        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-down"></i>
            </a>
        </div>

    </div>

    <div class = "widget-body">
        <div style = "display: none;" class = "widget-body-inner">
            <div class = "widget-main">

                <div class="row row-fluid">
                    <?php $this->renderPartial('../matricula/_informacionPlantel', array('datosPlantel' => $datosPlantel)); ?>
                </div>
            </div>
        </div>
    </div>

</div>
<div class = "widget-box">

    <div class = "widget-header">
        <h5>Formulario de Carga de Estadistica </h5>

        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class = "widget-body">
        <div  class = "widget-body-inner">
            <div class = "widget-main">
                <div class="row">
                    <?php
                    if($modelError){
                        foreach($modelError as $i => $item){
                            if($modelError){
                                $this->renderPartial('//errorSumMsg',array('model'=>$modelError),false,true);
                            }
                            break;
                        }
                    }
                    ?>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php echo CHtml::beginForm(); ?>
                        <?php foreach($models as $i=>$model){ ?>
                            <?php echo CHtml::activeHiddenField($model,"[$i]id",array('value'=>$model->id)); ?>
                            <?php echo CHtml::activeHiddenField($model,"[$i]tipo_atencion_id",array('value'=>($model->tipo_atencion_id)?$model->tipo_atencion_id:99)); ?>
                            <div class="table-responsive">
                                <table class="report table table-striped table-bordered table-hover" >
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            <strong>VARIABLES</strong>
                                        </th>
                                        <th class="center">
                                            <strong>SEXO</strong>
                                        </th>
                                        <th class="center">
                                            <strong>TOTAL</strong>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody >

                                    <tr style="vertical-align:middle">
                                        <td style="vertical-align:middle" rowspan="2"  class="center">
                                            MATRÍCULA INICIAL (MI)
                                        </td>
                                        <td  style="vertical-align:middle" class="center">
                                            MASCULINO
                                        </td>
                                        <td  style="vertical-align:middle"  class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]pmt_inicial_m",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->pmt_inicial_m)); ?>
                                        </td>
                                    </tr>
                                    <tr style="vertical-align:middle">
                                        <td style="vertical-align:middle" class="center">
                                            FEMENINO
                                        </td>
                                        <td  style="vertical-align:middle"  class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]pmt_inicial_f",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->pmt_inicial_f)); ?>
                                        </td>
                                    </tr>
                                    <tr style="vertical-align:middle">
                                        <td style="vertical-align:middle" rowspan="2" class="center">
                                            INCORPORACIÓN (I)
                                        </td>
                                        <td  style="vertical-align:middle" class="center">
                                            MASCULINO
                                        </td>
                                        <td   style="vertical-align:middle" class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]pmt_incorporacion_m",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->pmt_incorporacion_m)); ?>
                                        </td>
                                    </tr>
                                    <tr style="vertical-align:middle">
                                        <td  style="vertical-align:middle" class="center">
                                            FEMENINO
                                        </td>
                                        <td   style="vertical-align:middle" class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]pmt_incorporacion_f",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->pmt_incorporacion_f)); ?>
                                        </td>
                                    </tr>
                                    <tr style="vertical-align:middle">
                                        <td style="vertical-align:middle" rowspan="2" class="center">
                                            ABANDONO (A)
                                        </td>
                                        <td style="vertical-align:middle" class="center">
                                            MASCULINO
                                        </td>
                                        <td  style="vertical-align:middle" class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]pmt_abandono_m",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->pmt_abandono_m)); ?>
                                        </td>
                                    </tr>
                                    <tr style="vertical-align:middle">
                                        <td style="vertical-align:middle" class="center">
                                            FEMENINO
                                        </td>
                                        <td  style="vertical-align:middle" class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]pmt_abandono_f",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->pmt_abandono_f)); ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <div class="col-md-6 wizard-actions pull-right">
                                <button type="submit" data-last="Finish" class="btn btn-primary btn-next">
                                    <?php echo ($nuevo)?'Guardar':'Actualizar';?>
                                    <i class="icon-save icon-on-right"></i>
                                </button>
                            </div>
                        </div>
                        <?php echo CHtml::endForm(); ?>
                    </div>
                </div>
                <div class="row"><div class="space-12"></div></div>
            </div>
        </div>
    </div>

</div>
<script type="application/javascript">
    $(document).ready(function(){
        $('.cargaEstadistica').unbind('keyup');
        $('.cargaEstadistica').bind('keyup', function() {
            keyNum(this,false,false);
        });
    });

</script>