<?php
/* @var $this CargaEstadisticaController */
/* @var $model CargaEstadistica */

$this->pageTitle = 'Actualización de Datos de Carga Estadisticas';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Carga Estadisticas'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>