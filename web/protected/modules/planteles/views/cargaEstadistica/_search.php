<?php
/* @var $this CargaEstadisticaController */
/* @var $model CargaEstadistica */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'plantel_id'); ?>
		<?php echo $form->textField($model,'plantel_id', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'periodo_id'); ?>
		<?php echo $form->textField($model,'periodo_id', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_atencion_id'); ?>
		<?php echo $form->textField($model,'tipo_atencion_id', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mat_inicial_m'); ?>
		<?php echo $form->textField($model,'mat_inicial_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mat_incorporacion_m'); ?>
		<?php echo $form->textField($model,'mat_incorporacion_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mat_abandono_m'); ?>
		<?php echo $form->textField($model,'mat_abandono_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mat_final_m'); ?>
		<?php echo $form->textField($model,'mat_final_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mat_inicial_f'); ?>
		<?php echo $form->textField($model,'mat_inicial_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mat_incorporacion_f'); ?>
		<?php echo $form->textField($model,'mat_incorporacion_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mat_abandono_f'); ?>
		<?php echo $form->textField($model,'mat_abandono_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mat_final_f'); ?>
		<?php echo $form->textField($model,'mat_final_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pre_inicial_m'); ?>
		<?php echo $form->textField($model,'pre_inicial_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pre_incorporacion_m'); ?>
		<?php echo $form->textField($model,'pre_incorporacion_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pre_abandono_m'); ?>
		<?php echo $form->textField($model,'pre_abandono_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pre_final_m'); ?>
		<?php echo $form->textField($model,'pre_final_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pre_inicial_f'); ?>
		<?php echo $form->textField($model,'pre_inicial_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pre_incorporacion_f'); ?>
		<?php echo $form->textField($model,'pre_incorporacion_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pre_abandono_f'); ?>
		<?php echo $form->textField($model,'pre_abandono_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pre_final_f'); ?>
		<?php echo $form->textField($model,'pre_final_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pri_inicial_m'); ?>
		<?php echo $form->textField($model,'pri_inicial_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pri_incorporacion_m'); ?>
		<?php echo $form->textField($model,'pri_incorporacion_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pri_abandono_m'); ?>
		<?php echo $form->textField($model,'pri_abandono_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pri_final_m'); ?>
		<?php echo $form->textField($model,'pri_final_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pri_inicial_f'); ?>
		<?php echo $form->textField($model,'pri_inicial_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pri_incorporacion_f'); ?>
		<?php echo $form->textField($model,'pri_incorporacion_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pri_abandono_f'); ?>
		<?php echo $form->textField($model,'pri_abandono_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pri_final_f'); ?>
		<?php echo $form->textField($model,'pri_final_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'meg_inicial_m'); ?>
		<?php echo $form->textField($model,'meg_inicial_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'meg_incorporacion_m'); ?>
		<?php echo $form->textField($model,'meg_incorporacion_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'meg_abandono_m'); ?>
		<?php echo $form->textField($model,'meg_abandono_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'meg_final_m'); ?>
		<?php echo $form->textField($model,'meg_final_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'meg_inicial_f'); ?>
		<?php echo $form->textField($model,'meg_inicial_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'meg_incorporacion_f'); ?>
		<?php echo $form->textField($model,'meg_incorporacion_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'meg_abandono_f'); ?>
		<?php echo $form->textField($model,'meg_abandono_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'meg_final_f'); ?>
		<?php echo $form->textField($model,'meg_final_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'met_inicial_m'); ?>
		<?php echo $form->textField($model,'met_inicial_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'met_incorporacion_m'); ?>
		<?php echo $form->textField($model,'met_incorporacion_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'met_abandono_m'); ?>
		<?php echo $form->textField($model,'met_abandono_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'met_final_m'); ?>
		<?php echo $form->textField($model,'met_final_m', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'met_inicial_f'); ?>
		<?php echo $form->textField($model,'met_inicial_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'met_incorporacion_f'); ?>
		<?php echo $form->textField($model,'met_incorporacion_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'met_abandono_f'); ?>
		<?php echo $form->textField($model,'met_abandono_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'met_final_f'); ?>
		<?php echo $form->textField($model,'met_final_f', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuario_ini_id'); ?>
		<?php echo $form->textField($model,'usuario_ini_id', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_ini'); ?>
		<?php echo $form->textField($model,'fecha_ini',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuario_act_id'); ?>
		<?php echo $form->textField($model,'usuario_act_id', array('class' => 'span-12',)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_act'); ?>
		<?php echo $form->textField($model,'fecha_act',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_elim'); ?>
		<?php echo $form->textField($model,'fecha_elim',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estatus'); ?>
		<?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->