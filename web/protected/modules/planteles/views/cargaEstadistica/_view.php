<?php
/* @var $this CargaEstadisticaController */
/* @var $model CargaEstadistica */

$this->breadcrumbs=array(
	'Carga Estadisticas'=>array('lista'),
);
?>

<div class="tabbable">

    <ul class="nav nav-tabs">
        <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="datosGenerales">
            <div class="form">

        <div id="div-datos-generales">

            <div class="widget-box">

                <div class="widget-header">
                    <h5>Datos Generales</h5>

                    <div class="widget-toolbar">
                        <a data-action="collapse" href="#">
                            <i class="icon-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-body-inner">
                        <div class="widget-main">
                            <div class="widget-main form">

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'plantel_id'); ?>
                                            <?php echo $form->textField($model,'plantel_id', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'periodo_id'); ?>
                                            <?php echo $form->textField($model,'periodo_id', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'tipo_atencion_id'); ?>
                                            <?php echo $form->textField($model,'tipo_atencion_id', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'mat_inicial_m'); ?>
                                            <?php echo $form->textField($model,'mat_inicial_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'mat_incorporacion_m'); ?>
                                            <?php echo $form->textField($model,'mat_incorporacion_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'mat_abandono_m'); ?>
                                            <?php echo $form->textField($model,'mat_abandono_m', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'mat_final_m'); ?>
                                            <?php echo $form->textField($model,'mat_final_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'mat_inicial_f'); ?>
                                            <?php echo $form->textField($model,'mat_inicial_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'mat_incorporacion_f'); ?>
                                            <?php echo $form->textField($model,'mat_incorporacion_f', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'mat_abandono_f'); ?>
                                            <?php echo $form->textField($model,'mat_abandono_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'mat_final_f'); ?>
                                            <?php echo $form->textField($model,'mat_final_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pre_inicial_m'); ?>
                                            <?php echo $form->textField($model,'pre_inicial_m', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pre_incorporacion_m'); ?>
                                            <?php echo $form->textField($model,'pre_incorporacion_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pre_abandono_m'); ?>
                                            <?php echo $form->textField($model,'pre_abandono_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pre_final_m'); ?>
                                            <?php echo $form->textField($model,'pre_final_m', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pre_inicial_f'); ?>
                                            <?php echo $form->textField($model,'pre_inicial_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pre_incorporacion_f'); ?>
                                            <?php echo $form->textField($model,'pre_incorporacion_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pre_abandono_f'); ?>
                                            <?php echo $form->textField($model,'pre_abandono_f', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pre_final_f'); ?>
                                            <?php echo $form->textField($model,'pre_final_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pri_inicial_m'); ?>
                                            <?php echo $form->textField($model,'pri_inicial_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pri_incorporacion_m'); ?>
                                            <?php echo $form->textField($model,'pri_incorporacion_m', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pri_abandono_m'); ?>
                                            <?php echo $form->textField($model,'pri_abandono_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pri_final_m'); ?>
                                            <?php echo $form->textField($model,'pri_final_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pri_inicial_f'); ?>
                                            <?php echo $form->textField($model,'pri_inicial_f', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pri_incorporacion_f'); ?>
                                            <?php echo $form->textField($model,'pri_incorporacion_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pri_abandono_f'); ?>
                                            <?php echo $form->textField($model,'pri_abandono_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pri_final_f'); ?>
                                            <?php echo $form->textField($model,'pri_final_f', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'meg_inicial_m'); ?>
                                            <?php echo $form->textField($model,'meg_inicial_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'meg_incorporacion_m'); ?>
                                            <?php echo $form->textField($model,'meg_incorporacion_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'meg_abandono_m'); ?>
                                            <?php echo $form->textField($model,'meg_abandono_m', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'meg_final_m'); ?>
                                            <?php echo $form->textField($model,'meg_final_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'meg_inicial_f'); ?>
                                            <?php echo $form->textField($model,'meg_inicial_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'meg_incorporacion_f'); ?>
                                            <?php echo $form->textField($model,'meg_incorporacion_f', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'meg_abandono_f'); ?>
                                            <?php echo $form->textField($model,'meg_abandono_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'meg_final_f'); ?>
                                            <?php echo $form->textField($model,'meg_final_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'met_inicial_m'); ?>
                                            <?php echo $form->textField($model,'met_inicial_m', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'met_incorporacion_m'); ?>
                                            <?php echo $form->textField($model,'met_incorporacion_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'met_abandono_m'); ?>
                                            <?php echo $form->textField($model,'met_abandono_m', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'met_final_m'); ?>
                                            <?php echo $form->textField($model,'met_final_m', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'met_inicial_f'); ?>
                                            <?php echo $form->textField($model,'met_inicial_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'met_incorporacion_f'); ?>
                                            <?php echo $form->textField($model,'met_incorporacion_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'met_abandono_f'); ?>
                                            <?php echo $form->textField($model,'met_abandono_f', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'met_final_f'); ?>
                                            <?php echo $form->textField($model,'met_final_f', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'usuario_ini_id'); ?>
                                            <?php echo $form->textField($model,'usuario_ini_id', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'fecha_ini'); ?>
                                            <?php echo $form->textField($model,'fecha_ini',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'usuario_act_id'); ?>
                                            <?php echo $form->textField($model,'usuario_act_id', array('class' => 'span-12',)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'fecha_act'); ?>
                                            <?php echo $form->textField($model,'fecha_act',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'fecha_elim'); ?>
                                            <?php echo $form->textField($model,'fecha_elim',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'estatus'); ?>
                                            <?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div><!-- form -->
        </div>
    </div>
</div>
