<?php
/**
 * Created by PhpStorm.
 * User: isalaz01
 * Date: 05/08/15
 * Time: 01:40 PM
 */
?>
<?php if (Yii::app()->user->hasFlash('exito')): ?>
    <div class="successDialogBox">
        <p><?php echo Yii::app()->user->getFlash('exito'); ?></p>
    </div>
<?php endif; ?>
<?php if (Yii::app()->user->hasFlash('error')): ?>
    <div class="errorDialogBox">
        <p><?php echo Yii::app()->user->getFlash('error'); ?></p>
    </div>
<?php endif; ?>
<div class = "widget-box collapsed">

    <div class = "widget-header">
        <h5>Identificación del Plantel <?php echo '"' . $datosPlantel['nom_plantel'] . '"'; ?></h5>

        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-down"></i>
            </a>
        </div>

    </div>

    <div class = "widget-body">
        <div style = "display: none;" class = "widget-body-inner">
            <div class = "widget-main">

                <div class="row row-fluid">
                    <?php $this->renderPartial('../matricula/_informacionPlantel', array('datosPlantel' => $datosPlantel)); ?>
                </div>
            </div>
        </div>
    </div>

</div>
<div class = "widget-box">

    <div class = "widget-header">
        <h5>Formulario de Carga de Estadistica </h5>

        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class = "widget-body">
        <div  class = "widget-body-inner">
            <div class = "widget-main">
                <div class="row">
                    <?php
                    if($modelError){
                        foreach($modelError as $i => $item){
                            if($modelError){
                                $this->renderPartial('//errorSumMsg',array('model'=>$modelError),false,true);
                            }
                            break;
                        }
                    }
                    ?>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php echo CHtml::beginForm(); ?>
                        <div class="table-responsive">
                            <table class="report table table-striped table-bordered table-hover" >
                                <thead>
                                <tr>
                                    <th colspan="8" class="center">
                                        <strong>MATRÍCULA FINAL POR ETAPA Y VARIABLE SEGÚN TIPO DE ATENCIÓN Y SEXO</strong>
                                    </th>
                                </tr>
                                <tr >
                                    <th rowspan="2" class="center" >
                                        TIPO DE ATENCIÓN
                                    </th>
                                    <th rowspan="2" class="center">
                                        SEXO
                                    </th>
                                    <th colspan="3" class="center">
                                        ETAPA MATERNAL Y VARIABLE
                                    </th>
                                    <th  colspan="3" class="center">
                                        ETAPA PREESCOLAR Y VARIABLE
                                    </th>
                                </tr>
                                <tr>
                                    <th  class="center">
                                        MATRÍCULA INICIAL (MI)
                                    </th>
                                    <th class="center">
                                        INCORPORACIÓN (I)
                                    </th>
                                    <th  class="center">
                                        ABANDONO (A)
                                    </th>
                                    <th  class="center">
                                        MATRÍCULA INICIAL (MI)
                                    </th>
                                    <th class="center">
                                        INCORPORACIÓN (I)
                                    </th>
                                    <th  class="center">
                                        ABANDONO (A)
                                    </th>
                                </tr>
                                </thead>
                                <tbody >

                                <?php foreach($models as $i=>$model){ ?>
                                    <?php echo CHtml::activeHiddenField($model,"[$i]id",array('value'=>$model->id)); ?>
                                    <tr style="vertical-align:middle">
                                        <th style="vertical-align:middle" rowspan="3" class="center" >
                                            <?php
                                            switch($i+1){
                                                case 1: echo "ATENCIÓN INSTITUCIONAL"; break;
                                                case 2: echo "ATENCIÓN EN ESPACIOS DE FAMILIA Y COMUNIDAD"; break;
                                                case 3: echo "ATENCIÓNI EN SIMONCITO FAMILIAR Y COMUNITARIO"; break;
                                            }
                                            ?>
                                            <?php echo CHtml::activeHiddenField($model,"[$i]tipo_atencion_id",array('value'=>($model->tipo_atencion_id)?$model->tipo_atencion_id:$i+1)); ?>
                                        </th>
                                    </tr>
                                    <tr style="vertical-align:middle">
                                        <td  style="vertical-align:middle" class="center">
                                            MASCULINO
                                        </td>
                                        <td  class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]mat_inicial_m",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required', 'value'=>$model->mat_inicial_m)); ?>
                                        </td>
                                        <td class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]mat_incorporacion_m",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->mat_incorporacion_m)); ?>
                                        </td>
                                        <td  class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]mat_abandono_m",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->mat_abandono_m)); ?>
                                        </td>
                                        <!--<td class="center">
                                            <strong><span style="float: none;" id="CargaEstadisticaTotal_<?php /*echo $i;*/?>_mat_final_m"</span></strong>
                                        </td>-->
                                        <td  class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]pre_inicial_m",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->pre_inicial_m)); ?>
                                        </td>
                                        <td class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]pre_incorporacion_m",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->pre_incorporacion_m)); ?>
                                        </td>
                                        <td  class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]pre_abandono_m",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->pre_abandono_m)); ?>
                                        </td>

                                        <!--<td class="center">
                                            <strong><span style="float: none;" id="CargaEstadisticaTotal_<?php /*echo $i;*/?>_mat_final_f"</span></strong>
                                        </td>-->
                                    </tr>
                                    <tr style="vertical-align:middle">
                                        <td style="vertical-align:middle" class="center">
                                            FEMENINO
                                        </td>
                                        <td  class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]mat_inicial_f",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->mat_inicial_f)); ?>
                                        </td>
                                        <td class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]mat_incorporacion_f",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->mat_incorporacion_f)); ?>
                                        </td>
                                        <td  class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]mat_abandono_f",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->mat_abandono_f)); ?>
                                        </td>
                                        <!--<td class="center">
                                            <strong><span style="float: none;" id="CargaEstadisticaTotal_<?php /*echo $i;*/?>_pre_final_f"</span></strong>
                                        </td>-->
                                        <td  class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]pre_inicial_f",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->pre_inicial_f)); ?>
                                        </td>
                                        <td class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]pre_incorporacion_f",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->pre_incorporacion_f)); ?>
                                        </td>
                                        <td  class="center">
                                            <?php echo CHtml::activeNumberField($model,"[$i]pre_abandono_f",array('class'=>'col-xs-12 center cargaEstadistica','min'=>0,'max'=>9999,'required'=>'required','value'=>$model->pre_abandono_f)); ?>
                                        </td>
                                        <!-- <td class="center">
                                            <strong><span style="float: none;" id="CargaEstadisticaTotal_<?php /*echo $i;*/?>_pre_final_f"</span></strong>
                                        </td>-->
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-6 wizard-actions pull-right">
                                <button type="submit" data-last="Finish" class="btn btn-primary btn-next">
                                    <?php echo ($nuevo)?'Guardar':'Actualizar';?>
                                    <i class="icon-save icon-on-right"></i>
                                </button>
                            </div>
                        </div>
                        <?php echo CHtml::endForm(); ?>
                    </div>
                </div>
                <div class="row"><div class="space-12"></div></div>
            </div>
        </div>
    </div>

</div>
<script type="application/javascript">
    $(document).ready(function(){
        $('.cargaEstadistica').unbind('keyup');
        $('.cargaEstadistica').bind('keyup', function() {
            keyNum(this,false,false);
        });
    });

</script>