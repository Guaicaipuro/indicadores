<?php

/* @var $this CargaEstadisticaController */
/* @var $model CargaEstadistica */

$this->breadcrumbs=array(
    'Catálogo' => array('/catalogo'),
    'Carga Estadisticas'=>array('lista'),
    'Administración',
);
$this->pageTitle = 'Administración de Carga Estadisticas';

?>
    <div class="widget-box">
        <div class="widget-header">
            <h5>Lista de Carga Estadisticas</h5>

            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div style="display:block;" class="widget-body-inner">
                <div class="widget-main">

                    <div class="row space-6"></div>
                    <div>
                        <div id="resultadoOperacion">
                            <div class="infoDialogBox">
                                <p>
                                    En este módulo podrá registrar y/o actualizar los datos de Carga Estadisticas.
                                </p>
                            </div>
                        </div>

                        <div class="pull-right" style="padding-left:10px;">
                            <a href="<?php echo $this->createUrl("/planteles/cargaEstadistica/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                                <i class="fa fa-plus icon-on-right"></i>
                                Registrar Nuevo Carga Estadisticas                        </a>
                        </div>


                        <div class="row space-20"></div>

                    </div>

                    <?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'carga-estadistica-grid',
                        'dataProvider'=>$dataProvider,
                        'filter'=>$model,
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'summaryText' => 'Mostrando {start}-{end} de {count}',
                        'pager' => array(
                            'header' => '',
                            'htmlOptions' => array('class' => 'pagination'),
                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                        ),
                        'afterAjaxUpdate' => "
                function(){
                    
                }",
                        'columns'=>array(
                            array(
                                'header' => '<center>Id</center>',
                                'name' => 'id',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[id]', $model->id, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Plantel_id</center>',
                                'name' => 'plantel_id',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[plantel_id]', $model->plantel_id, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Periodo_id</center>',
                                'name' => 'periodo_id',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[periodo_id]', $model->periodo_id, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Tipo_atencion_id</center>',
                                'name' => 'tipo_atencion_id',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[tipo_atencion_id]', $model->tipo_atencion_id, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Mat_inicial_m</center>',
                                'name' => 'mat_inicial_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[mat_inicial_m]', $model->mat_inicial_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Mat_incorporacion_m</center>',
                                'name' => 'mat_incorporacion_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[mat_incorporacion_m]', $model->mat_incorporacion_m, array('title' => '',)),
                            ),
                            /*
                            array(
                                'header' => '<center>mat_abandono_m</center>',
                                'name' => 'mat_abandono_m',
                                'htmlOptions' => array(),
                                            'filter' => CHtml::textField('CargaEstadistica[mat_abandono_m]', $model->mat_abandono_m, array('title' => '',)),
                                        ),
                            array(
                                'header' => '<center>Mat_final_m</center>',
                                'name' => 'mat_final_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[mat_final_m]', $model->mat_final_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Mat_inicial_f</center>',
                                'name' => 'mat_inicial_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[mat_inicial_f]', $model->mat_inicial_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Mat_incorporacion_f</center>',
                                'name' => 'mat_incorporacion_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[mat_incorporacion_f]', $model->mat_incorporacion_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Mat_abandono_f</center>',
                                'name' => 'mat_abandono_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[mat_abandono_f]', $model->mat_abandono_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Mat_final_f</center>',
                                'name' => 'mat_final_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[mat_final_f]', $model->mat_final_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pre_inicial_m</center>',
                                'name' => 'pre_inicial_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pre_inicial_m]', $model->pre_inicial_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pre_incorporacion_m</center>',
                                'name' => 'pre_incorporacion_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pre_incorporacion_m]', $model->pre_incorporacion_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pre_abandono_m</center>',
                                'name' => 'pre_abandono_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pre_abandono_m]', $model->pre_abandono_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pre_final_m</center>',
                                'name' => 'pre_final_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pre_final_m]', $model->pre_final_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pre_inicial_f</center>',
                                'name' => 'pre_inicial_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pre_inicial_f]', $model->pre_inicial_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pre_incorporacion_f</center>',
                                'name' => 'pre_incorporacion_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pre_incorporacion_f]', $model->pre_incorporacion_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pre_abandono_f</center>',
                                'name' => 'pre_abandono_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pre_abandono_f]', $model->pre_abandono_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pre_final_f</center>',
                                'name' => 'pre_final_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pre_final_f]', $model->pre_final_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pri_inicial_m</center>',
                                'name' => 'pri_inicial_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pri_inicial_m]', $model->pri_inicial_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pri_incorporacion_m</center>',
                                'name' => 'pri_incorporacion_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pri_incorporacion_m]', $model->pri_incorporacion_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pri_abandono_m</center>',
                                'name' => 'pri_abandono_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pri_abandono_m]', $model->pri_abandono_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pri_final_m</center>',
                                'name' => 'pri_final_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pri_final_m]', $model->pri_final_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pri_inicial_f</center>',
                                'name' => 'pri_inicial_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pri_inicial_f]', $model->pri_inicial_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pri_incorporacion_f</center>',
                                'name' => 'pri_incorporacion_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pri_incorporacion_f]', $model->pri_incorporacion_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pri_abandono_f</center>',
                                'name' => 'pri_abandono_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pri_abandono_f]', $model->pri_abandono_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Pri_final_f</center>',
                                'name' => 'pri_final_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[pri_final_f]', $model->pri_final_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Meg_inicial_m</center>',
                                'name' => 'meg_inicial_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[meg_inicial_m]', $model->meg_inicial_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Meg_incorporacion_m</center>',
                                'name' => 'meg_incorporacion_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[meg_incorporacion_m]', $model->meg_incorporacion_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Meg_abandono_m</center>',
                                'name' => 'meg_abandono_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[meg_abandono_m]', $model->meg_abandono_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Meg_final_m</center>',
                                'name' => 'meg_final_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[meg_final_m]', $model->meg_final_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Meg_inicial_f</center>',
                                'name' => 'meg_inicial_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[meg_inicial_f]', $model->meg_inicial_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Meg_incorporacion_f</center>',
                                'name' => 'meg_incorporacion_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[meg_incorporacion_f]', $model->meg_incorporacion_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Meg_abandono_f</center>',
                                'name' => 'meg_abandono_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[meg_abandono_f]', $model->meg_abandono_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Meg_final_f</center>',
                                'name' => 'meg_final_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[meg_final_f]', $model->meg_final_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Met_inicial_m</center>',
                                'name' => 'met_inicial_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[met_inicial_m]', $model->met_inicial_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Met_incorporacion_m</center>',
                                'name' => 'met_incorporacion_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[met_incorporacion_m]', $model->met_incorporacion_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Met_abandono_m</center>',
                                'name' => 'met_abandono_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[met_abandono_m]', $model->met_abandono_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Met_final_m</center>',
                                'name' => 'met_final_m',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[met_final_m]', $model->met_final_m, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Met_inicial_f</center>',
                                'name' => 'met_inicial_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[met_inicial_f]', $model->met_inicial_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Met_incorporacion_f</center>',
                                'name' => 'met_incorporacion_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[met_incorporacion_f]', $model->met_incorporacion_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Met_abandono_f</center>',
                                'name' => 'met_abandono_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[met_abandono_f]', $model->met_abandono_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Met_final_f</center>',
                                'name' => 'met_final_f',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[met_final_f]', $model->met_final_f, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Usuario_ini_id</center>',
                                'name' => 'usuario_ini_id',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Fecha_ini</center>',
                                'name' => 'fecha_ini',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[fecha_ini]', $model->fecha_ini, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Usuario_act_id</center>',
                                'name' => 'usuario_act_id',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Fecha_act</center>',
                                'name' => 'fecha_act',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[fecha_act]', $model->fecha_act, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Fecha_elim</center>',
                                'name' => 'fecha_elim',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[fecha_elim]', $model->fecha_elim, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Estatus</center>',
                                'name' => 'estatus',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CargaEstadistica[estatus]', $model->estatus, array('title' => '',)),
                            ),
                            */
                            array(
                                'type' => 'raw',
                                'header' => '<center>Acción</center>',
                                'value' => array($this, 'getActionButtons'),
                                'htmlOptions' => array('nowrap'=>'nowrap'),
                            ),
                        ),
                    )); ?>
                </div>
            </div>
        </div>
    </div>

<?php

/**
 * Yii::app()->clientScript->registerScriptFile(
 *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/CargaEstadisticaController/carga-estadistica/admin.js', CClientScript::POS_END
 *);
 */
?>