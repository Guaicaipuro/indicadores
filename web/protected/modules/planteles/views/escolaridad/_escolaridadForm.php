<?php
/**
 * Created by PhpStorm.
 * User: isalaz01
 * Date: 05/06/15
 * Time: 09:46 AM
 */

/* @var $form CActiveForm */
?>
    <div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'escolaridad-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
        ));
        ?>
        <?php echo $form->hiddenField($escolaridadForm, 'periodo_id',array('readOnly' => 'readOnly','value' => $escolaridadForm->periodo_id));?>
        <?php echo $form->hiddenField($escolaridadForm, 'estudiante_id',array('readOnly' => 'readOnly'));?>
        <?php echo $form->hiddenField($escolaridadForm, 'seccion_plantel_id',array('readOnly' => 'readOnly','value' => $escolaridadForm->seccion_plantel_id));?>
        <?php echo $form->hiddenField($escolaridadForm, 'plantel_id',array('readOnly' => 'readOnly','value' => $escolaridadForm->plantel_id));?>
        <?php
        if ($form->errorSummary($escolaridadForm)):
            ?>
            <div id ="div-result-message" class="errorDialogBox" >
                <?php echo $form->errorSummary($escolaridadForm); ?>
            </div>
        <?php
        endif;
        ?>



        <div id="1eraFila" class="col-md-12">
            <div class="col-md-3" >
                <div class="col-md-12" >

                    <?php echo $form->labelEx($escolaridadForm,'tdocumento_identidad',array('class'=>'col-md-12'));?>
                    <?php echo $form->dropDownList($escolaridadForm, 'tdocumento_identidad', array(
                        'C'=>'Cédula Escolar',
                        'V' => 'Venezolana',
                        'E' => 'Extranjera',
                        'P' => 'Pasaporte',
                    ), array('required' => 'required' , 'class' => 'span-12'));
                    ?>
                </div>
            </div>

            <div class="col-md-3" >
                <div class="col-md-12" >
                    <?php  echo $form->labelEx($escolaridadForm,'documento_identidad',array('class'=>'col-md-12')); ?>
                    <?php echo $form->textField($escolaridadForm, 'documento_identidad',array('maxlength' => 20, 'placeholder' => 'Ejm: 12345678','class' => 'span-12'));?>
                </div>
            </div>
            <div class="col-md-3" >
                <div class="col-md-12" >
                    <?php  echo $form->labelEx($escolaridadForm,'nombres',array('class'=>'col-md-12','class' => 'col-md-12')); ?>
                    <?php echo $form->textField($escolaridadForm, 'nombres',array('readOnly' => 'readOnly','class' => 'span-12'));?>
                </div>
            </div>
            <div class="col-md-3" >
                <div class="col-md-12" >
                    <?php  echo $form->labelEx($escolaridadForm,'apellidos',array('class'=>'col-md-12','class' => 'col-md-12')); ?>
                    <?php echo $form->textField($escolaridadForm, 'apellidos',array('readOnly' => 'readOnly','class' => 'span-12' ));?>
                </div>
            </div>
        </div>
        <div class = "col-md-12"><div class = "space-6"></div></div>
        <hr>
        <div class = "col-md-12"><div class = "space-6"></div></div>

        <div class = "col-md-12">
            <?php $this->renderPartial('_formEscolaridad',array('escolaridadForm'=>$escolaridadForm)); ?>
        </div>
        <div class = "col-md-12"><div class = "space-6"></div></div>
        <hr>

    </div>
    <div class="row">
        <div class="col-xs-6 pull-right">
            <button class="btn btn-primary btn-next pull-right" title="Guardar Datos de Escolaridad" data-last="Finish" type="submit">
                Guardar
                <i class="icon-save icon-on-right"></i>
            </button>
        </div>
    </div>

<?php $this->endWidget(); ?>
    <div id="dialog_error" class="hide"></div>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/plantel/escolaridad-form.js',CClientScript::POS_END); ?>