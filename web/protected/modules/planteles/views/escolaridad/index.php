<?php
/* @var $this EscolaridadController */

$this->breadcrumbs=array(
    'Planteles'=> array('/planteles/'),
    'Secciones'=> array('/planteles/seccionPlantel/admin/id/'.$plantel_id),
    'Escolaridad'
);
?>
<div class = "widget-box collapsed">

    <div class = "widget-header">
        <h5>Identificación del Plantel <?php echo '"' . $datosPlantel['nom_plantel'] . '"'; ?></h5>

        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-down"></i>
            </a>
        </div>

    </div>

    <div class = "widget-body">
        <div style = "display: none;" class = "widget-body-inner">
            <div class = "widget-main">

                <div class="row row-fluid">
                    <?php $this->renderPartial('_informacionPlantel', array('datosPlantel' => $datosPlantel)); ?>
                </div>
            </div>
        </div>
    </div>

</div>
<div class = "widget-box collapsed">

    <div class = "widget-header">
        <h5>Datos de la Sección</h5>

        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-down"></i>
            </a>
        </div>

    </div>
    <div class = "widget-body">
        <div style = "display: none;" class = "widget-body-inner">
            <div class = "widget-main">

                <div class="row row-fluid">
                    <?php
                    $this->renderPartial('_informacionSeccion', array('datosSeccion' => $datosSeccion)); ?>
                </div>
            </div>
        </div>
    </div>

</div>
<div class = "widget-box">
    <div class = "widget-header">
        <h5>Datos del Estudiante</h5>
        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-down"></i>
            </a>
        </div>
    </div>
    <div class = "widget-body">
        <div style = "display: block;" class = "widget-body-inner">
            <div class = "widget-main">

                <div class="row row-fluid">
                    <?php
                    if(Yii::app()->user->hasFlash('error')):?>
                        <div class="errorDialogBox">
                            <p> <?php echo Yii::app()->user->getFlash('error'); ?></p>
                        </div>
                    <?php endif; ?>
                    <?php
                    if(Yii::app()->user->hasFlash('success')):?>
                        <div class="successDialogBox">
                            <p> <?php echo Yii::app()->user->getFlash('success'); ?></p>
                        </div>
                    <?php endif; ?>
                    <?php $this->renderPartial('_escolaridadForm', array('escolaridadForm' => $escolaridadForm,'periodo_id'=>$periodo_id)); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class = "col-md-12"><div class = "space-6"></div></div>
<div class="row">
    <div class="col-xs-6">
        <a id="btnRegresar" class="btn btn-danger" href="/planteles/seccionPlantel/admin/id/<?php echo $plantel_id;?>">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>
</div>
