<?php
/**
 * Created by PhpStorm.
 * User: isalaz01
 * Date: 05/12/14
 * Time: 03:13 PM
 */
?>
<div class="col-xs-12">
    <div class="row-fluid">
        <div class="form">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'docente-form',
                'htmlOptions' => array('data-form-type'=>'multipart/form-data',), // for inset effect
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation'=>false,
            )); ?>
            <div id="div-result-docente">
                <?php
                if($model->hasErrors()):
                    $this->renderPartial('//errorSumMsg', array('model' => $model));
                else:
                    ?>
                    <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                <?php
                endif;
                ?>
            </div>
            <div id="div-datos-generales">

                <div class="widget-box">

                    <div class="widget-header">
                        <h5>Datos Generales</h5>

                        <div class="widget-toolbar">
                            <a data-action="collapse" href="#">
                                <i class="icon-chevron-up"></i>
                            </a>
                        </div>
                    </div>

                    <div class="widget-body">
                        <div class="widget-body-inner">
                            <div class="widget-main">
                                <div class="widget-main form">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <?php

                                                $tdocumento_identificacion = array(
                                                    array('id'=>'V',
                                                        'nombre'=>'Venezolana'),
                                                    array('id'=>'E',
                                                        'nombre'=>'Extranjera'),
                                                    array('id'=>'P',
                                                        'nombre'=>'Pasaporte'),
                                                );
                                                (is_object($model->personalPlantel) AND is_object($model->personalPlantel->personal) AND isset($model->personalPlantel->personal->tdocumento_identidad))?$tdocumento_identidad =$model->personalPlantel->personal->tdocumento_identidad:$tdocumento_identidad='V';
                                                (is_object($model->personalPlantel) AND is_object($model->personalPlantel->personal) AND isset($model->personalPlantel->personal->documento_identidad))?$documento_identidad =$model->personalPlantel->personal->documento_identidad:$documento_identidad='';
                                                (is_object($model->personalPlantel) AND is_object($model->personalPlantel->personal) AND isset($model->personalPlantel->personal->nombres))?$nombres =$model->personalPlantel->personal->nombres:$nombres='';
                                                (is_object($model->personalPlantel) AND is_object($model->personalPlantel->personal) AND isset($model->personalPlantel->personal->apellidos))?$apellidos =$model->personalPlantel->personal->apellidos:$apellidos='';
                                                ?>
                                                <?php echo $form->labelEx($model,'tdocumento_identidad'); ?>
                                                <?php echo $form->dropDownList($model,'tdocumento_identidad',CHtml::listData($tdocumento_identificacion, 'id', 'nombre'),array('class'=>'span-12','value'=>$tdocumento_identidad)); ?>
                                            </div>
                                            <div class="col-md-3">
                                                <?php echo $form->labelEx($model,'documento_identidad'); ?>
                                                <?php echo $form->textField($model,'documento_identidad',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', "required"=>"required",'value'=>$documento_identidad)); ?>
                                            </div>
                                            <div class="col-md-3">
                                                <?php echo $form->labelEx($model,'nombres'); ?>
                                                <?php echo $form->textField($model,'nombres',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', "readOnly"=>"readOnly",'value'=>$nombres)); ?>
                                            </div>
                                            <div class="col-md-3">
                                                <?php echo $form->labelEx($model,'apellidos'); ?>
                                                <?php echo $form->textField($model,'apellidos',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', "readOnly"=>"readOnly",'value'=>$apellidos)); ?>
                                            </div>
                                        </div>
                                        <?php echo $form->hiddenField($model,'seccion_plantel_id',array('value'=>base64_encode($model->seccion_plantel_id))); ?>
                                        <?php echo $form->hiddenField($model,'periodo_id',array('value'=>base64_encode($model->periodo_id))); ?>
                                        <?php echo $form->hiddenField($model,'id',array('value'=>base64_encode($model->id))); ?>
                                        <div class="space-6"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">

                            <div class="col-md-6 wizard-actions pull-right">
                                <button class="btn btn-primary btn-next" id="btnGuardarAsigDocente" data-last="Finish" type="submit">
                                    Guardar
                                    <i class="icon-save icon-on-right"></i>
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>

<div id="resultDialog" class="hide"></div>
<div id="dialog_error" class="hide"></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#btnGuardarAsigDocente').unbind('click');
        $('#btnGuardarAsigDocente').bind('click', function (e) {
            e.preventDefault();
            var divResult='div-result-docente';
            var urlDir = '/planteles/docente/guardarAsignaturaDocente';
            var datos;
            var loadingEfect=true;
            var showResult=true;
            var method='GET';
            var plantel_id=$("#plantel_id").val();
            var callback=function(){
                $('#div-result-docente').removeClass('hide');
                $('#docente-grid').yiiGridView('update', {
                    data: $(this).serialize()
                });
            }
            var responseFormat='html';
            $('#div-result-docente').addClass('hide');
            $('#div-result-docente').html('');
            datos=$('#docente-form').serialize()+'&plantel='+plantel_id;

            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method,callback);

        });
        $('#AsignaturaDocente_documento_identidad').unbind('keyup');
        $('#AsignaturaDocente_documento_identidad').bind('keyup', function () {
            makeUpper(this);
            keyAlphaNum(this, false, false);
        });
        $('#AsignaturaDocente_documento_identidad').unbind('blur');
        $('#AsignaturaDocente_documento_identidad').bind('blur', function () {
            clearField(this);
        });

        $('#AsignaturaDocente_documento_identidad').unbind('change');
        $('#AsignaturaDocente_documento_identidad').bind('change', function() {
            //clearField(this);
            var documento_identidad = $(this).val();

            //alert($(this).val());
            var id_nombres="AsignaturaDocente_nombres";
            var id_apellidos="AsignaturaDocente_apellidos";
            var id_fecha_nacimiento="AsignaturaDocente_fecha_nacimiento";
            var id_valido="AsignaturaDocente_valido";
            var tdocumento_identidad = $('#AsignaturaDocente_tdocumento_identidad').val();
            var plantel_id=$("#plantel_id").val();
            var title;
            var mensaje;
            var divResult='';
            var urlDir = '/planteles/docente/obtenerDatosPersona';
            var datos;
            var loadingEfect=false;
            var showResult=false;
            var method='GET';
            var responseFormat='json';
            var beforeSendCallback=function(){};
            var successCallback;
            var errorCallback=function(){};
            $('#'+id_valido).val(false);
            $('#div-result-docente').addClass('hide');
            $('#div-result-docente').html('');

            if(documento_identidad !=null && documento_identidad != '' ){
                if(tdocumento_identidad !=null && tdocumento_identidad != '' ){
                    datos={'tdocumento_identidad':tdocumento_identidad,'documento_identidad':documento_identidad,'plantel_id':plantel_id};
                    successCallback=function(response){
                        if(response.statusCode =='SUCCESS'){
                            $('#'+id_apellidos).val(response.apellidos);
                            $('#'+id_nombres).val(response.nombres);
                            $('#'+id_fecha_nacimiento).val(response.fecha_nacimiento);
                            $('#'+id_valido).val(true);
                        }
                        if(response.statusCode =='ERROR'){
                            $('#div-result-docente').removeClass('hide');
                            displayDialogBox('div-result-docente','error',response.mensaje);
                            $('#'+id_apellidos).val('');
                            $('#'+id_nombres).val('');
                            $('#'+id_fecha_nacimiento).val('');
                            $(this).val('');
                        }
                    };
                    executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
                }
                else {
                    title = 'Notificación de Error';
                    mensaje='Estimado usuario debe seleccionar un Tipo de Documento de Identidad Valido.';
                    $('#div-result-docente').removeClass('hide');
                    displayDialogBox('div-result-docente','error',mensaje);
                    $('#'+id_apellidos).val('');
                    $('#'+id_nombres).val('');
                    $('#'+id_fecha_nacimiento).val('');
                    $(this).val('');
                }

            }
            else {
                $('#'+id_apellidos).val('');
                $('#'+id_nombres).val('');
                $('#'+id_fecha_nacimiento).val('');
                $(this).val('');
            }

        });
    });

</script>