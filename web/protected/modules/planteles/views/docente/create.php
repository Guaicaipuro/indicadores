<?php
/* @var $this DocenteController */
/* @var $model Docente */

$this->breadcrumbs=array(
    'Planteles'=>array('/planteles/consultar/'),
    'Docentes'=>array('/planteles/docente/lista/id/'.$plantel_id),
    'Registro'
);
?>
<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>