<div class="row">
    <div class="col-md-12">
        <div id ="estudiantesIns">
        </div>
        <div class="tabbable">
            <?php echo $lapsos; ?>
            <br>
        </div>
        <div class="tabbable  tabs-left">

            <ul id="asig" class="nav nav-tabs">
                <?php print_r($materias); ?>
            </ul>
            <div class="tab-content">
                <div id="respuestaRegistroAsistenciaClases"  class="hide">
                    <p></p>
                </div>
                <?php
                //var_dump(SeccionPlantel::model()->existeEstudiantesInscriptosEnSeccion($seccion_plantel_id));

                $this->widget(
                    'zii.widgets.grid.CGridView', array(
                        'id' => 'estudiantes-asignatura-media-regular-grid',
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'dataProvider' => CalificacionAsignaturaEstudiante::model()->AlumnosParaCalificar($seccion_plantel_id,$lapsoPred,$asignatura_id_pred,$periodo),
                        'summaryText' => false,
                        'afterAjaxUpdate'=>"function(){ $('.indeca').mask('20.0');

                                        var m = 0;
                                        m = $('.calificado').length;
                                        if(m>0){
                                        $('#guardarCalificacion').addClass('hide');
                                        }else{
                                        $('#guardarCalificacion').removeClass('hide');
                                        }

                                        $('.indeca').bind('change, blur, keyup', function() {
                                            //alert(parseFloat($(this).val()));
                                            if (parseFloat($(this).val()) > 20.0) {

                                                $(this).val('20.0');
                                            }
                                            else if ($(this).val() == '20._') {

                                                $(this).val('20.0');
                                            }
                                            else if ($(this).val() == '00.0') {

                                                $(this).val('01.0');
                                            }
                                            else if ($(this).val() == '__') {

                                                $(this).val('');
                                            }

                                        });
                                        $('.indeas').mask('00');
                                        $('.indeas').bind('change, blur, keyup', function() {
        $(this).css('color','#858585');
        if ($(this).val() == '__') {

            $(this).val('');
        }

    });
                                        }",
                        'columns' => array(
                            array(
                                'header' => '<center>D.Identidad</center>',
                                'type' => 'raw',
                                'value' => array($this, 'columnaAccionesDocumentoIdentidad'),
                                'htmlOptions' => array('width' => '20%')
                            ),
                            array(
                                'name' => 'fecha_nacimiento',
                                'header' => '<center>Edad</center>',
                                'value' => array($this, 'columnaEdad'),
                                'type'=>'html',
                                'htmlOptions' => array('width' => '5%'),
                            ),
                            array(
                                'name' => 'nomape',
                                'header' => '<center>Nombre y Apellido</center>'
                            ),
                            array(
                                'type' => 'raw',
                                'header' => '<center>Asistencia</center>',
                                'htmlOptions'=>array('nowrap'=>'nowrap'),
                                'value' => array($this, 'columnaAccionesAsistencia'),
                                'htmlOptions' => array('width' => '5%'),
                            ),
//                                    array(
//                                        'type' => 'raw',
//                                        'header' => '<center><b>Acciones</b></center>',
//                                        'htmlOptions'=>array('nowrap'=>'nowrap'),
//                                        'value' => array($this, 'columnaAcciones'),
//                                        'htmlOptions' => array('width' => '5%'),
//                                    ),
                        ),
                        'pager' => array(
                            'header' => '',
                            'htmlOptions' => array('class' => 'pagination'),
                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                        ),
                    )
                );
                ?>
                <input type="hidden" name="lapso" id="lapso" value="<?php echo $lapsoPred;?>"/>

                <input type="hidden" name="periodo" id="periodo" value="<?php echo $periodo;?>"/>
                <input type="hidden" name="seccion_plantel_id" id="seccion_plantel_id" value="<?php echo $seccion_plantel_id;?>"/>
                <input type='hidden' name='asignatura' id="asignatura" value='<?php echo $asignatura_id_pred; ?>'/>
                <div class="row-fluid wizard-actions">

                    <button class="btn btn-primary btn-next" id="guardarAsistencias" data-last="Finish ">
                        Guardar
                        <i class=" icon-save"></i>
                    </button>

                </div>

            </div>


        </div>
    </div>
</div>