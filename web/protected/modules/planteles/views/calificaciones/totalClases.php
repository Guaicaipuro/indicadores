<?php
$this->breadcrumbs = array(
    'Planteles' => array('/planteles'),
    'Secciones' => array('/planteles/seccionPlantel/admin/id/' . base64_encode($plantel_id)),
    'Total de Clases'
);
Yii::app()->getSession()->add('plnatel_id',$plantel_id);
?>
<div class = "widget-box collapsed">

    <div class = "widget-header">
        <h5>Identificación del Plantel <?php echo '"' . $datosPlantel['nom_plantel'] . '"'; ?></h5>

        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-down"></i>
            </a>
        </div>

    </div>

    <div class = "widget-body">
        <div style = "display: none;" class = "widget-body-inner">
            <div class = "widget-main">

                <div class="row row-fluid">
                    <?php $this->renderPartial('_informacionPlantel', array('datosPlantel' => $datosPlantel)); ?>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="widget-box collapsed">
    <div class = "widget-header">
        <h5>Datos de la Sección</h5>
        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-down"></i>
            </a>
        </div>
    </div>
    <div class = "widget-body">
        <div style = "display:block;" class = "widget-body-inner">
            <div class = "widget-main">
                <div class="row row-fluid">
                    <div id="msgAlerta">
                    </div>

                    <div class = "col-lg-12"><div class = "space-6"></div></div>
                    <div id="gridEstudiantes" class="col-md-12" >
                        <div class="widget-main form">

                            <div class="row">

                                <div class="col-md-11" id ="detallesSec">

                                    <?php $this->renderPartial('_informacionSeccion', array('datosSeccion' => $datosSeccionInfo)); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<?php $this->renderPartial('_total_clases',
    array('datosSeccion' => $datosSeccionInfo,
        'lapso'=>$lapsoPred,
        'lapsoButtons'=>$lapsos,
        'seccion_plantel_id'=>$seccion_plantel_id
    )); ?>
<div class="widget-box">
    <div class = "widget-header">
        <h5>Asistencias <?php echo $datosSeccion['grado'] . ' Sección "' . $datosSeccion['seccion'] . '"'; ?></h5>
        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class = "widget-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'asistencia-clases-form',
            'enableAjaxValidation' => false,
        ));
        ?>
        <div style = "display:block;" class = "widget-body-inner">
            <div class = "widget-main">

                <div id="msgAlertaAsistencia">
                </div>
                <?php $this->renderPartial('_asignaturas_clases',
                    array('seccion_plantel_id'=>$seccion_plantel_id,
                        'lapsoPred'=>$lapsoPred,
                        'asignatura_id_pred'=>$asignatura_id_pred,
                        'periodo'=>$periodo,
                        'materias'=>$materias,
                        'lapsos'=>$lapsosMateria,
                        'plantel_id'=>$plantel_id,
                        'seccion_id'=>$datosSeccion['id'])
                ); ?>

                <?php
                // echo CHtml::hiddenField('plantel_id', $plantel_id);
                ?>

                <div class="space-6"></div>


            </div>
        </div>
        <?php
        $this->endWidget();
        ?>

    </div>
</div>

<hr>

<div class="col-md-12">
    <div class="col-md-6">
        <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("/planteles/seccionPlantel/admin/id/" . base64_encode($plantel_id)); ?>" class="btn btn-danger">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>



</div>

<div id="dialogPantalla"></div>
<div id="dialogo_confirmacion" class="hide"><p></p></div>
<div class ="hide" id="incluir_Estudiante" ></div>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/pnotify.custom.min.css" media="screen, projection" />
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/plantel/calificaciones.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/pnotify.custom.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js', CClientScript::POS_END);
?>

<script>
    $(document).ready(function() {

    });
</script>


