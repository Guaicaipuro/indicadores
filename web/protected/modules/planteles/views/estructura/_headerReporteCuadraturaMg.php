<?php
/*
 * DATOS DEL PLANTEL
 */

// Cabecera del documento de cuadratura

?>



<img src="<?php echo yii::app()->basePath . '/../public/images/barra_n.png'; ?>" />
<br /><br/>
<table style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;">

    <?php 

    if (isset($secciones) AND $secciones != array()) {
    $seccion_cuarto=0;
    $seccion_quinto=0;
    $w=0;
    foreach ($secciones as $key => $value) {
        if($w==0){
            $año = isset($value['año']) ? $value['año'] : '';
            $w++;
        }
        $añotemp = isset($value['año']) ? $value['año'] : '';
        if($año==$añotemp){
            $seccion_cuarto = isset($value['secciones']) ? $value['secciones'] : '';
        }
        if($año!=$añotemp){
            $seccion_quinto = isset($value['secciones']) ? $value['secciones'] : '';
        }
    }
    }
    ?>

    <tr>
        <td colspan="3" align="center" style="background:#E5E5E5; padding:3px;">
            <b>CUADRATURA MEDIA GENERAL</b>
        </td>
    </tr>

    <tr >
        <td>
            <b>C&oacute;digo del plantel:</b>
            <?php echo $model->cod_plantel; ?></td>
        <td colspan="2">
            <b>Plantel:</b>
            <?php echo $model->nombre; ?>
        </td>
    </tr>
    <tr>
        <td width="200px">
            <b>Secciones por Año</b>
            <?php ?>
        </td>

        <td width="250px">
            <b>Cuarto año:</b>
            <?php echo $seccion_cuarto; ?>
        </td>

        <td width="200px">
            <b>Quinto año:</b>
            <?php echo $seccion_quinto; ?>
        </td>
   
    </tr>
    
    <tr>
    </table>
