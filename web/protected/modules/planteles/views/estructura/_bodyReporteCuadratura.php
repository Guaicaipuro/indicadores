<style type="text/css">

    .personal table, .personal th, .personal td {
        border: 1px solid grey;
        padding-bottom: 0px;
        margin-bottom: 5px;
    }
    .personal_datos table {
        border-collapse: collapse;
        border-spacing: 10px;
        text-align:center;

    }
</style>

<?php
if (isset($cuadratura) AND $cuadratura != array()) {
    ?>
    <div class >
        <?php
        $c = 0;
        $w = 0;
        $materiaSpan =0;
        $añoSpan =0;
        $cuarto=array();
        $quinto=array();
        $materiacol=array();
        $contador=0;
        $posicion=0;
        $sa=0;
        $primero=0;
        foreach ($cuadratura as $key => $value) {
            if($primero==0){
                $materia = isset($value['materia']) ? $value['materia'] : '';
                $año = isset($value['año']) ? $value['año'] : '';
                $primero ++;
            }
            $materiaTemp = isset($value['materia']) ? $value['materia'] : '';
            $añoTemp = isset($value['año']) ? $value['año'] : '';
            if($materiaTemp == $materia AND $añoTemp == $año){
                $contador++;
                $año=$añoTemp;
                $materia=$materiaTemp;
            }
            if($materiaTemp == $materia AND $añoTemp != $año){
                $cuarto[$posicion] = $contador;
                $contador = 1;
                $año=$añoTemp;
                $materia=$materiaTemp;
            }
            if($materiaTemp != $materia AND $añoTemp == $año){
                $quinto[$posicion] = $contador;
                $contador = 1;
                $materiacol[$posicion] = $cuarto[$posicion] + $quinto[$posicion];
                $posicion++;
                $año=$añoTemp;
                $materia=$materiaTemp;
            }
            if($materiaTemp != $materia AND $añoTemp != $año){
                $quinto[$posicion] = $contador;
                $contador = 1;
                $materiacol[$posicion] = $cuarto[$posicion] + $quinto[$posicion];
                $posicion++;
                $materia=$materiaTemp;
                $año=$añoTemp;
            }
        }
        $quinto[$posicion] = $contador;
        $materiacol[$posicion] = $cuarto[$posicion] + $quinto[$posicion];
        $posicion=0;



        foreach ($cuadratura as $key => $value) {
        if($c==0){
            $materia = isset($value['materia']) ? $value['materia'] : '';
            $año = isset($value['año']) ? $value['año'] : '';
            ?>
            <table class="personal" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-collapse: collapse;
       border-spacing: 0px;
       text-align:center; border: 1px solid grey; ">
                <tr style="border: 1px solid grey;">
                    <th width="20%" style=" border: 1px solid grey;"  rowspan="2"  class="center">
                        Materia / Año
                    </th>
                    <th width="10%" style=" border: 1px solid grey;"  rowspan="2"  class="center">
                        Documento de Identidad
                    </th>
                    <!--      <th width="80px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
                              Cédula Escolar
                          </th> -->
                    <th width="30%" style=" border: 1px solid grey;"  rowspan="2"  class="center">
                        Apellidos y Nombres
                    </th>
                    <th width="10%" style=" border: 1px solid grey;" rowspan="2"  class="center">
                        Hrs
                    </th>
                    <th width="10%" style=" border: 1px solid grey;" colspan="3" class="center">
                        Hrs/A
                    </th>
                    <th width="10%" style=" border: 1px solid grey;" colspan="3" class="center">
                        P/A
                    </th>
                    <th width="10%" style=" border: 1px solid grey;" colspan="3" class="center">
                        Total
                    </th>
            </table>
        <?php }
        else {
            $materiaTemp = isset($value['materia']) ? $value['materia'] : '';
            $añoTemp = isset($value['año']) ? $value['año'] : '';
            if($año!=$añoTemp){
                $año = $añoTemp;
                $añoSpan=0;
            }
            if($materia!=$materiaTemp){
                $materia = $materiaTemp;
                echo "</table></div>";
                $materiaSpan =0;
                $w =0;
                $posicion++;
                ?>
                <br>

                <table class="personal" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-collapse: collapse;
       border-spacing: 0px;
       text-align:center; border: 1px solid grey; ">
                    <tr style="border: 1px solid grey;">
                        <th width="20%" style=" border: 1px solid grey;"  rowspan="2"  class="center">
                            Materia / Año
                        </th>
                        <th width="10%" style=" border: 1px solid grey;"  rowspan="2"  class="center">
                            Documento de Identidad
                        </th>
                        <!--      <th width="80px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
                                  Cédula Escolar
                              </th> -->
                        <th width="30%" style=" border: 1px solid grey;"  rowspan="2"  class="center">
                            Apellidos y Nombres
                        </th>
                        <th width="10%" style=" border: 1px solid grey;" rowspan="2"  class="center">
                            Hrs
                        </th>
                        <th width="10%" style=" border: 1px solid grey;" colspan="3" class="center">
                            Hrs/A
                        </th>
                        <th width="10%" style=" border: 1px solid grey;" colspan="3" class="center">
                            P/A
                        </th>
                        <th width="10%" style=" border: 1px solid grey;" colspan="3" class="center">
                            Total
                        </th>
                </table>
            <?php
            }
        }

        if($w==0){ ?>
        <table class="personal_datos" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-collapse: collapse;
           border-spacing: 0px;
           text-align:center; border: 1px solid grey; ">
        <?php }
        ?>


            <?php
            //    $nacionalidad = isset($value['tdocumento_identidad']) ? $value['tdocumento_identidad'] : '';
            $documento_identidad = isset($value['documento_identidad']) ? $value['documento_identidad'] : '';
            //$tdocumento_identidad = isset($value['tdocumento_identidad']) ? $value['tdocumento_identidad'] : '';
            // $cedula_escolar = isset($value['cedula_escolar']) ? $value['cedula_escolar'] : '';
            // $serial = isset($value['serial']) ? $value['serial'] : '';
            $nombres = isset($value['nombres']) ? $value['nombres'] : '';
            $apellidos = isset($value['apellidos']) ? $value['apellidos'] : '';
            $hrs = isset($value['horas']) ? $value['horas'] : '';
            //     $sexo = isset($value['sexo']) ? $value['sexo'] : '';
            //  $ciudad_nacimiento = isset($value['ciudad_nacimiento']) ? $value['ciudad_nacimiento'] : '';
            //      $fecha_nacimiento = (isset($value['fecha_nacimiento'])) ? $value['fecha_nacimiento'] : '';
            // $observacion = isset($value['observacion']) ? $value['observacion'] : '';
            //     $fecha_array = explode('-', $fecha_nacimiento);
            //     $anio = isset($fecha_array[0]) ? $fecha_array[0] : '';
            //    $mes = isset($fecha_array[1]) ? $fecha_array[1] : '';
            //     $dia = isset($fecha_array[2]) ? $fecha_array[2] : '';
            ?>



            <tr>
                <?php
                if($materiaSpan == 0){ ?>
                    <td rowspan= "<?php echo $materiacol[$posicion]; ?>" width="13%" align="center" style=" border: 1px solid grey;">
                        <?php echo $materia; ?>
                    </td>
                    <?php
                    $materiaSpan++;
                } ?>
                <?php
                if($añoSpan == 0){
                    if($sa== 0){?>
                        <td rowspan= "<?php echo $cuarto[$posicion]; ?>"  width="7%"  align="center" style=" border: 1px solid grey;">
                            <?php echo $año; ?>
                        </td>
                        <?php $sa++;
                    }
                    else
                        if($sa!=0){?>
                            <td rowspan= "<?php echo $quinto[$posicion]; ?>"  width="7%"  align="center" style=" border: 1px solid grey;">
                                <?php echo $año; ?>
                            </td>
                            <?php $sa--;
                        }
                } ?>

                <td width="10%"  align="center">
                    <?php echo $documento_identidad; ?>
                </td>

                <td  width="30%"  align="center">
                    <?php echo $apellidos.' '.$nombres; ?>
                </td>

                <td width="10%"  align="center">
                    <?php echo $hrs?>
                </td>
                <td width="10%"  align="center">
                    <?php echo 'Hrs asignadas' ?>
                </td>
                <td  width="10%" align="center">
                    <?php echo 'Por asignar' ?>
                </td>
                <td  width="10%" align="center">
                    <?php echo 'Total' ?>
                </td>
            </tr>
            <?php
            $c++;
            $w++;
            }
            echo "</table></div>";
            ?>

    </div>
<?php }
?>

       