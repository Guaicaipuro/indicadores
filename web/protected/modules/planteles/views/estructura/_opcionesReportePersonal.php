<?php
$columna = '<div class="btn-group dropup">
                        <button style="height:34px;" class="btn dropdown-toggle" data-toggle="dropdown">
                            Reportes de personal
                            <span class="icon-caret-up icon-on-right"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-yellow pull-right">';

$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Administrativo </span>", Yii::app()->createUrl("/planteles/estructura/reporteEmpleados/plantel/" . ($plantel_id)."/periodo/".$periodo."/tipo_personal/".base64_encode(Constantes::ADMINISTRATIVO)), array("class" => "fa fa-file-pdf-o ", "title" => "Descargar reporte del personal Administrativo")) . '</li>';
$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Docente </span>", Yii::app()->createUrl("/planteles/estructura/reporteEmpleados/plantel/" . ($plantel_id)."/periodo/".$periodo."/tipo_personal/".base64_encode(Constantes::DOCENTE)), array("class" => "fa fa-file-pdf-o ", "title" => "Descargar reporte del personal Docente")) . '</li>';
$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Docente por Seccion</span>", Yii::app()->createUrl("/planteles/estructura/docentesPorSeccion/plantel/" . ($plantel_id)."/periodo/".$periodo."/tipo_personal/".base64_encode(Constantes::DOCENTE)), array("class" => "fa fa-file-pdf-o ", "title" => "Descargar reporte del personal Docente por Seccion")) . '</li>';
$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Obrero </span>", Yii::app()->createUrl("/planteles/estructura/reporteEmpleados/plantel/" . ($plantel_id)."/periodo/".$periodo."/tipo_personal/".base64_encode(Constantes::OBRERO)), array("class" => "fa fa-file-pdf-o ", "title" => "Descargar reporte del personal Obrero")) . '</li>';



$columna .= '</ul></div>';
echo $columna;
?>