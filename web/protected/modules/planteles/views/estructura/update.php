<?php
/* @var $this EstructuraController */
/* @var $model PersonalPlantel */

$this->pageTitle = 'Registro de Personal Plantel';

$this->breadcrumbs=array(
    'Planteles'=>array('/planteles/'),
    'Estructura del Plantel'=>array('/planteles/estructura/lista/id/'.base64_encode($plantel_id)),
    'Actualización'
);
?>

<?php $this->renderPartial('_form', array(
                                            'formType'=>'edicion',
                                            'modelPersonal'=>$modelPersonal,
                                            'modelPersonalPlantel'=>$modelPersonalPlantel,
                                            'modelPlantel'=>$modelPlantel,
                                            'modelFuncionPersonal'=>$modelFuncionPersonal,
                                            'modelPersonalPlantelEspacios'=>$modelPersonalPlantelEspacios,
                                            'modelEspecificacionEstatus'=>$modelEspecificacionEstatus,
                                            'modelEstatusDocente'=>$modelEstatusDocente,
                                            'modelPersonalEstudio'=>$modelPersonalEstudio,
                                            'lista_tdocumento_identificacion'=>$lista_tdocumento_identificacion,
                                            'lista_tipo_personal'=>$lista_tipo_personal,
                                            'lista_grado_instruccion'=>$lista_grado_instruccion,
                                            'lista_estatus_docente'=>$lista_estatus_docente,
                                            'lista_especificacion_estatus'=>$lista_especificacion_estatus,
                                            'lista_especialidad'=>$lista_especialidad, 
                                            'lista_funcion'=>$lista_funcion,
                                            'lista_denominacion'=>$lista_denominacion,
                                            'lista_situacion_cargo'=>$lista_situacion_cargo,
                                            'lista_tiempo_dedicacion'=>$lista_tiempo_dedicacion,
                                            'plantel_id'=>$plantel_id,
                                            'display_div_atributo_espec'=>$display_div_atributo_espec,
                                            'display_div_seccion_dependencia'=>$display_div_seccion_dependencia,
                                            'display_div_atributo_docente'=>$display_div_atributo_docente,
                                            'display_div_atributo_denom_espec'=>$display_div_atributo_denom_espec,
                                            'etiqueta_dependencia'=>$etiqueta_dependencia,
                                            'mensaje'=>$mensaje,
                                            'indicador'=>$indicador,
                                            'modelTipoDependenciaPersonal'=> $modelTipoDependenciaPersonal,
                                            'modelPersonaPlantelDependencia'=> $modelPersonaPlantelDependencia,
                                            'tipoDependenciaPersonal'=> $tipoDependenciaPersonal,
    
    
                                          )
                            ); ?>   
        
       