
<div id="dialogPantallaEsp" class="hide"></div>


<?php 

echo CHtml::scriptFile('/public/js/modules/plantel/estructura-plantel/espacios.js');




Yii::app()->session["sess_esp_plantel_id"]=$modelPlantel->id;
        

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#personal-plantel-espacios-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

//Yii::app()->clientScript->registerCoreScript('yiiactiveform');

?>







<div class="widget-box">
 <div class="widget-header">
         <h5> Gesti&oacute;n de los espacios en los que labora el personal: <b> <?php echo  $modelPersonal->nombres." ". $modelPersonal->apellidos; ?> </b>  en el plantel: <b> <?php echo $modelPlantel->nombre; ?> </b> </h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
               
                <div class="row space-6"></div>
                
                 
                 
                 <div class="row">
                     <div class="row col-sm-8" id="resultadoOperacionEsp"> </div>
                     
                     
                     
                 </div>
                 <div class="col-md-12"> <div class="space-6"> </div> </div>
                
                 <div class="row">
                 
                 
                                            
            

                
                <div>
                    
                    
                    
                    
                        
                                <div class="pull-right" style="padding-left:10px;">
                                    <a  type="submit" onclick="VentanaDialog('','/planteles/estructura/registroPersonalPlantelEspacios','Registro de los Espacios en los que labora el personal','registroPersonalPlantelEspacios','')" data-last="Finish" class="btn btn-success btn-next btn-sm">
                                        <i class="fa fa-plus icon-on-right"></i>
                                        Registrar Espacios en el que labora el personal
                                    </a>

                                </div>

                            <?php
                           
                        ?>

                        <div class="row space-20"></div>
							
		</div><!-- search-form -->
                
                </div> <!-- class-row -->
                
               <div>

                                            
                                            <?php 
                                            
                                            
                                            
                                              
                                            
                                            $this->widget('zii.widgets.grid.CGridView', array(
                                            'id'=>'personal-plantel-espacios-grid',                                            
                                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                            'dataProvider'=>$modelPersonalPlantelEspacios->search_espacios($modelPersonalPlantel->id),
                                            'filter'=>$modelPersonalPlantelEspacios,
                                            'summaryText'=> '<br/><h5> Cantidad Total de Registros: {count} / Mostrando: del {start} al {end} </h5>', 
                                            
                                            'pager' => array(
                                                            'header' => "",
                                                            'htmlOptions' => array('class' => 'pagination'),
                                                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                                            ),
                                                
                                            'columns'=>array(
                                             array('name'=>'area_comun_plantel_id',                                                    
                                                   'header' => '<center>  Espacio </center>',
                                                   'filter' => false,
                                                   'value' => array($this, 'getNombreEspacio'),
                                                   //'value' =>'$data->Funcion->nombre',
                                                    /*'filter'=>CHtml::activeHiddenField($modelPersonalPlantelEspacios, 'area_comun_plantel_id',array('id'=>"area_comun_plantel_id",                                                                                                          
                                                                                                           'readonly'=>'readonly'

                                                                                                            )), */
                                                ), 
                                                
                                                
                                                 array('name'=>'cantidad',                                                    
                                                   'header' => '<center>  Nº de Espacios Asignados </center>',
                                                   'filter' => false,
                                                  
                                                   //'value' =>'$data->Funcion->nombre',
                                                    /*'filter'=>CHtml::activeHiddenField($modelPersonalPlantelEspacios, 'area_comun_plantel_id',array('id'=>"area_comun_plantel_id",                                                                                                          
                                                                                                           'readonly'=>'readonly'

                                                                                                            )), */
                                                ), 
                                                
                                                
                                                 array(
                                                        'header' => '<center>Estatus</center>',
                                                        'name' => 'estatus',
                                                        'value' => array($this, 'estatus'),
                                                        'filter' => false,
                                                     
                                                         /*'filter'=>CHtml::activeHiddenField($modelPersonalPlantelEspacios, 'estatus',array('id'=>"estatus",                                                                                                          
                                                                                                           'readonly'=>'readonly'

                                                                                                            )),*/
                                                        //'filter'=>array('A'=>'Activo','E'=>'Eliminado'),


                                                    ),
                                            
                                                
                                                array(                                                    
                                                        'type' => 'raw',
                                                        'header'=>'Acciones',        
                                                        'value'=>array($this,'columnaAccionesPlantelPersonalEspacios'), 
                                                     ),

                                                
                                            
                                                
                                                
                                            ),
                                            ));  ?>


                                        </div>
                
                
                
                
            </div>
        </div>
    </div>
    
    <div>
        
        <?php
        
         echo CHtml::hiddenField('_esp_personal_plantel_id', $modelPersonalPlantel->id , array());
        
        ?>
        
    </div>

<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/estructura/lista/id/".base64_encode($plantel_id)); ?>" id="btnRegresar">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
        </div>
    </div>







 
