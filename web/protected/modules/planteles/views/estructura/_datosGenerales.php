<div class="form">

                            <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'personal-form',
                                'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect _aux
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code. dropd
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                                'enableClientValidation' => true,
                                'clientOptions' => array(
                                                            //  'validateOnSubmit' => true,
                                                            'validateOnType' => true,
                                                            'validateOnChange' => true
                                                            ),
                                
                                
                            )); ?>

    
                      
                            <div id="div-result">
                                <?php
                                
                                // echo $form->errorSummary(array($modelPersonal,$modelSolicitudes)); 
                                                    
                                 if(strlen($mensaje)>0)                                          
                                 {
                                     $redireccion="lista";
                                     $id=$modelPersonalPlantel->plantel_id;
                                                                                                               
                                     Yii::app()->user->setFlash('mensajeExitoso',$mensaje);
                                     $this->renderPartial('//flashMsg');
                                     
                                     if( $modelPersonalPlantel->tipo_personal_id==Constantes::OBRERO )
                                     
                                     //if( $modelPersonalPlantel->tipo_personal_id==Constantes::ADMINISTRATIVO || $modelPersonalPlantel->tipo_personal_id==Constantes::OBRERO   )
                                     {
                                        $redireccion="edicion";
                                        $id=$modelPersonalPlantel->id;
                                     }
                                     
                                     
                                     // agregado por la pestaña de estudios superiores, comentar si piden que vuelvan a la version anterior
                                     if( $modelPersonalPlantel->tipo_personal_id!=Constantes::OBRERO )
                                     
                                     //if( $modelPersonalPlantel->tipo_personal_id==Constantes::ADMINISTRATIVO || $modelPersonalPlantel->tipo_personal_id==Constantes::OBRERO   )
                                     {
                                        $redireccion="edicion";
                                        $id=$modelPersonalPlantel->id;
                                     }
                                     
                                                                       
                                     $this->renderPartial('_z_aux',array("id"=> $id, "redireccion"=> $redireccion ));
                                    
                                     
                                     
                                 }
                                
                                
                                if($modelPersonal->hasErrors() || $modelPersonalPlantel->hasErrors() ):
                                    $this->renderPartial('//errorSumMsg', array('model' => array($modelPersonal,$modelPersonalPlantel)));
                                else:
                                    ?>
                                    <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                                <?php
                                endif;
                                ?>
                            </div>

                            <div id="div-datos-generales">

                                <div class="widget-box">

                                    <div class="widget-header">
                                        <h5> Datos Generales Del Personal </h5>

                                        <div class="widget-toolbar">
                                            <a data-action="collapse" href="#">
                                                <i class="icon-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-body-inner">
                                            <div class="widget-main">
                                                <div class="widget-main form">
                                                    <div class="row">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-3">

                                                                    <?php echo $form->labelEx($modelPersonal,'tdocumento_identidad'); ?>
                                                                    <?php echo $form->dropDownList($modelPersonal,'tdocumento_identidad',CHtml::listData($lista_tdocumento_identificacion, 'id', 'nombre'),array('class'=>'span-12',)); ?>
                                                                     <div class="row col-md-12" ><?php echo $form->error($modelPersonal, 'tdocumento_identidad'); ?> </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonal,'documento_identidad'); ?>
                                                                    <?php echo $form->textField($modelPersonal,'documento_identidad',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', "required"=>"required",'onChange'=>"buscaPersonal(this.value)",'placeholder'=>"Formato Numérico, Ejemplo: 12345")); ?>
                                                                    <div class="row col-md-12" ><?php echo $form->error($modelPersonal, 'documento_identidad'); ?> </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonal,'nombres'); ?>
                                                                    <?php echo $form->textField($modelPersonal,'nombres',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', "required"=>"required",)); ?>
                                                                                                                                        
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonal,'apellidos'); ?>
                                                                    <?php echo $form->textField($modelPersonal,'apellidos',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', "required"=>"required",)); ?>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="space-6"></div>

                                                            <div class="col-md-12">
                                                                
                                                                  <div class="col-md-3">                                                        

                                                                      <?php echo $form->labelEx($modelPersonal,'sexo'); ?>
                                                                      <?php echo $form->textField($modelPersonal, 'sexo', array('size'=>60, 'maxlength'=>150, 'class' => 'span-12','readonly' => 'readonly') ); ?>  
                                                                      <div class="row col-md-12" ><?php //echo $form->error($modelPersonal, 'fecha_nacimiento'); ?> </div>
                                                                  </div>


                                                                  <div class="col-md-3">
                                                                      <?php echo $form->labelEx($modelPersonal,'fecha_nacimiento'); ?>
                                                                      <?php echo $form->textField($modelPersonal,'fecha_nacimiento',array('size'=>60, 'maxlength'=>150, 'class' => 'span-12','readonly' => 'readonly') ); ?>
                                                                      <div class="row col-md-12" ><?php //echo $form->error($modelPersonal, 'fecha_nacimiento'); ?> </div>
                                                                  </div>                                                      
                                                                
                                                                  <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonal,'telefono_celular'); ?>
                                                                    <?php echo $form->textField($modelPersonal,'telefono_celular',array('size'=>60, 'maxlength'=>11, 'class' => 'span-12','placeholder'=>"Formato Numérico, Ejemplo: 04161234567")); ?>
                                                                    <div class="row col-md-12" ><?php echo $form->error($modelPersonal, 'telefono_celular'); ?> </div>
                                                                  </div>
                                                                
                                                                <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonal,'telefono_fijo'); ?>
                                                                    <?php echo $form->textField($modelPersonal,'telefono_fijo',array('size'=>60, 'maxlength'=>11, 'class' => 'span-12','placeholder'=>"Formato Numérico, Ejemplo: 02121234567")); ?>
                                                                    <div class="row col-md-12" ><?php echo $form->error($modelPersonal, 'telefono_fijo'); ?> </div>
                                                                </div>
                                                                

                                                            </div>
                                                            <div class="space-6"></div>
                                                            <div class="col-md-12">
                                                                
                                                                <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonalPlantel,'tipo_personal_id'); ?>
                                                                    <?php echo $form->dropDownList($modelPersonalPlantel, 'tipo_personal_id', CHtml::listData($lista_tipo_personal, 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                                    <div class="row col-md-12" ><?php echo $form->error($modelPersonalPlantel, 'tipo_personal_id'); ?> </div>
                                                                </div>
                                                                
                                                                 <div class="col-md-3">
                                                                    
                                                                    <?php echo $form->labelEx($modelPersonal,'correo'); ?>
                                                                    <?php echo  CHtml::image(Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('userGroups.img')) . "/info.png", "Info", array('class'=>'info-button', 'title'=>"Campo Requerido para Docente y Administrativo","height"=>"22px"));  ?> 
                                                                    
                                                                    <?php echo $form->textField($modelPersonal,'correo',array('size'=>60, 'maxlength'=>150, 'class' => 'span-12','placeholder'=>"Formato: correo@dominio.com")); ?>
                                                                    <div class="row col-md-12" ><?php echo $form->error($modelPersonal, 'correo'); ?> </div>
                                                                </div>
                                                                
                                                                
                                                                <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonal,'grado_instruccion_id'); ?>
                                                                    <?php echo $form->dropDownList($modelPersonal, 'grado_instruccion_id', CHtml::listData($lista_grado_instruccion, 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                                    <div class="row col-md-12" ><?php echo $form->error($modelPersonal, 'grado_instruccion_id'); ?> </div>
                                                                  </div>
                                                                
                                                                <div class="col-md-3" id="div_atributo_espec" style= '<?php //echo $display_div_atributo_espec; ?>' >
                                                                    <?php echo $form->labelEx($modelPersonalPlantel,'especialidad_id'); ?> 
                                                                    <?php echo $form->dropDownList($modelPersonalPlantel, 'especialidad_id', CHtml::listData($lista_especialidad, 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', )); ?>
                                                                </div>
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                                                            
                                                            </div>
                                                            
                                                            <div class="space-6"></div>
                                                                                                            
                                                            <div class="col-md-12">
                                                                
                                                                <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonalPlantel,'funcion_id'); ?> 
                                                                    <?php echo $form->dropDownList($modelPersonalPlantel, 'funcion_id', CHtml::listData($lista_funcion, 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', )); ?>
                                                                </div> 
                                                                
                                                                <div class="col-md-3" id="div_atributo_docente" style= '<?php //echo $display_div_atributo_docente; ?>' >
                                                                    <?php echo $form->labelEx($modelPersonalPlantel,'denominacion_id'); ?> <!-- <span class="required">*</span> -->
                                                                    <?php echo $form->dropDownList($modelPersonalPlantel, 'denominacion_id', CHtml::listData($lista_denominacion, 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', )); ?>
                                                                </div>
                                                                 
                                                                                                                                                                                                 
                                                                 <div class="col-md-3" id="div_atributo_denom_espec" style= '<?php echo $display_div_atributo_denom_espec; ?>' >
                                                                   <?php echo $form->labelEx($modelPersonalPlantel,'denominacion_especificacion'); ?> <span class="required">*</span>
                                                                   <?php echo $form->textField($modelPersonalPlantel,'denominacion_especificacion',array('size'=>60, 'maxlength'=>150, 'class' => 'span-12','placeholder'=>"Por favor indique. Ej. ABCDE")); ?>
                                                                 </div>
                                                                
                                                                 <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonalPlantel,'situacion_cargo_id'); ?>
                                                                    <?php echo $form->dropDownList($modelPersonalPlantel, 'situacion_cargo_id', CHtml::listData($lista_situacion_cargo, 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                                     <div class="row col-md-12" ><?php echo $form->error($modelPersonalPlantel, 'situacion_cargo_id'); ?> </div>
                                                                </div>
                                                                

                                                             </div>
                                                             <div class="space-6"></div>
                                                            
                                                             <div class="col-md-12">
                                                                 
                                                                <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonalPlantel,'tiempo_dedicacion_id'); ?>
                                                                    <?php echo $form->dropDownList($modelPersonalPlantel, 'tiempo_dedicacion_id', CHtml::listData($lista_tiempo_dedicacion, 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                                     <div class="row col-md-12" >  <?php echo $form->error($modelPersonalPlantel, 'tiempo_dedicacion_id'); ?> </div>
                                                                </div>                                                                                                                                                
                                                                
                                                                                                                                
                                                                 <div class="col-md-3">
                                                                    
                                                                    <?php echo $form->labelEx($modelPersonalPlantel,'horas_asignadas'); ?>
                                                                    <?php echo  CHtml::image(Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('userGroups.img')) . "/info.png", "Info", array('class'=>'info', 'title'=>"Horas asignadas por cada personal para ser trabajadas (máximo 50)","height"=>"20px","height"=>"22px"));  ?> 
                                                                    
                                                                    <?php echo $form->textField($modelPersonalPlantel,'horas_asignadas',array('size'=>60, 'maxlength'=>150, 'class' => 'span-12','placeholder'=>"Formato Decimal. Ejemplo: 00.00")); ?>
                                                                    <div class="row col-md-12" ><?php echo $form->error($modelPersonalPlantel, 'horas_asignadas'); ?> </div>
                                                                </div>                                                    
                                                                
                                                                 <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelEstatusDocente,'id'); ?>
                                                                    <?php echo $form->dropDownList($modelEstatusDocente, 'id', CHtml::listData($lista_estatus_docente, 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                                     <div class="row col-md-12" ><?php //echo $form->error($modelEstatusDocente, 'id'); ?> </div>
                                                                </div>
                                                                
                                                                <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonal,'especificacion_estatus_id'); ?>
                                                                    <?php echo $form->dropDownList($modelPersonal, 'especificacion_estatus_id', CHtml::listData($lista_especificacion_estatus, 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                                     <div class="row col-md-12" ><?php echo $form->error($modelPersonal, 'especificacion_estatus_id'); ?> </div>
                                                                </div>                                                                               
                                                               
                                                                 
                                                                 
                                                                 
                                                                 
                                                                
                                                                
                                                                                                                   
                                                                

                                                             </div>

                                                            
                                                            <div class="space-6"></div>
                                                            
                                                            <div class="col-md-12">
                                                                
                                                                <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonalPlantel,'cobra'); ?>
                                                                    <?php echo $form->radioButtonList($modelPersonalPlantel, 'cobra', array('S'=>'SI', 'N'=>'NO'),array( 'separator' => '&nbsp;&nbsp;&nbsp;' )); ?>
                                                                    <div class="row col-md-12" ><?php echo $form->error($modelPersonalPlantel, 'cobra'); ?> </div>
                                                                </div>
                                                                
                                                                <div class="col-md-3">
                                                                    <?php echo $form->labelEx($modelPersonalPlantel,'labora'); ?>
                                                                    <?php echo $form->radioButtonList($modelPersonalPlantel, 'labora', array('S'=>'SI', 'N'=>'NO'),array( 'separator' => '&nbsp;&nbsp;&nbsp;' ) ); ?>
                                                                                                                                      
                                                                     <div class="row col-md-12" ><?php echo $form->error($modelPersonalPlantel, 'labora'); ?> </div>
                                                                </div>
                                                                
                                                                <div id="div_seccion_dependencia" class="col-md-6" style= '<?php echo $display_div_seccion_dependencia; ?>' >
                                                                  
                                                                    <!-div class="row col-md-12">   
                                                                        
                                                                        <div class="col-md-6 tipoDependenciaPersonal ">
                                                                                <?php //echo $form->labelEx($modelTipoDependenciaPersonal,'nombre'); ?>
                                                                                <?php echo CHtml::label('Tipo de Dependencia <span class="required">*</span>',''); ?><br>
                                                                                <?php echo $form->dropDownList($modelTipoDependenciaPersonal,'id',CHtml::listData($tipoDependenciaPersonal,'id','nombre'),array('prompt'=>'- - -', 'class' => 'span-12')); ?>
                                                                        </div>
                                                                        <div class="col-md-6 tipoDependenciaPersonal ">
                                                                                <?php   echo CHtml::label('Descripcion <span class="required">*</span>','',array('id'=>'labelDescripcion'));  ?><br>
                                                                                <?php   if($formType=='edicion') { $condicion = array('size'=>60, 'maxlength'=>50, 'class' => 'span-12');  } else $condicion = array('size'=>60, 'maxlength'=>50, 'class' => 'span-12','readonly'=>'readonly');
                                                                                        echo $form->textField($modelPersonaPlantelDependencia,'descripcion',$condicion);
                                                                                        if($formType=='edicion')
                                                                                        echo  CHtml::hiddenField('control','edicion');
                                                                                        else
                                                                                        echo CHtml::hiddenField('control','registro');
                                                                                        ?>
                                                                                        
                                                                        </div>                  
                                                                           <?php  /*   ?>
                                                                            <div  class="col-md-6">
                                                                                <label id="etiqueta_dependencia" > <?php  echo $etiqueta_dependencia;  ?>  </label>                                                                               
                                                                                
                                                                            </div>

                                                                            <div class="col-md-6">
                                                                                <?php echo $form->labelEx($modelPersonalPlantel,'plantel_id_dep_pers'); ?> <span class="required">*</span>
                                                                                <?php echo $form->textField($modelPersonalPlantel,'plantel_id_dep_pers',array('size'=>60, 'maxlength'=>50, 'class' => 'span-12',)); ?>
                                                                                <div class="row col-md-12" ><?php echo $form->error($modelPersonalPlantel, 'plantel_id_dep_pers'); ?> </div>
                                                                            </div>
                                                                            <?php */ ?>
                                                                     <!-/div> <!-- fin class row col-md-12  -->
                                                                    
                                                                    
                                                                    
                                                                </div>
                                                                
                                                                        
                                                                
                                                            </div>
                                                            
                                                            
                                                            <div class="space-6"></div>


                                                             <div class="col-md-12">
                                                                   
                                                                 
                                                                 <div class="col-md-3">                                                      
                                                                    <?php echo $form->hiddenField($modelPersonalPlantel, 'pers_cond_nom_id', array('size'=>60, 'maxlength'=>150, 'class' => 'span-12','readonly' => 'readonly')); ?>
                                                                     <?php echo CHtml::hiddenField("tp_personal_docente", Constantes::DOCENTE); ?>                                                                     
                                                                     <?php echo CHtml::hiddenField("denom_doc_aula", Constantes::PERS_DENOM_DOC_AULA); ?>
                                                                     <?php //echo CHtml::hiddenField("denom_doc_coord", Constantes::PERS_DENOM_DOC_COORD); ?>
                                                                     <?php //echo CHtml::hiddenField("denom_doc_direc", Constantes::PERS_DENOM_DOC_DIREC); ?>
                                                                     <?php //echo CHtml::hiddenField("denom_doc_sup", Constantes::PERS_DENOM_DOC_SUPERV); ?>
                                                                     <?php //echo CHtml::hiddenField("denom_doc_otro", Constantes::PERS_DENOM_DOC_OTRO); ?>
                                                                     <?php echo CHtml::hiddenField('cod_plantel', $modelPlantel->cod_plantel ); ?>
                                                                     
                                                                 </div>
                                                                 
                                                                 <div class="col-md-3">                                                                                                                   
                                                                     
                                                                 </div>
                                                                 <div class="col-md-3">                                                                                                                   
                                                                     
                                                                 </div>
                                                                 <div class="col-md-3">                                                                                                                   
                                                                     
                                                                 </div>
                                                                 
                                                                 
                                                            </div>
                                                           

                                                        </div>





                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/estructura/lista/id/".base64_encode($plantel_id)); ?>" id="btnRegresar">
                                                <i class="icon-arrow-left"></i>
                                                Volver
                                            </a>
                                        </div>
                                        <div class="col-md-6 wizard-actions">
                                            <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                                Guardar
                                                <i class="icon-save icon-on-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div> 
                                
                                
                                
                                           

                
     
                            </div>
    
    
                            <div id="div-detalles-auditoria">                        
                                
                                    <?php if($formType=="edicion"):  ?>


                                        <div class="widget-box collapsed">

                                          <div class="widget-header">
                                              <h5>Detalles auditor&iacute;a</h5>

                                              <div class="widget-toolbar">
                                                  <a  href="#" data-action="collapse">
                                                      <i class="icon-chevron-up"></i>
                                                  </a>
                                              </div>
                                          </div>

                                          <div id="idenOtros" class="widget-body" >
                                              <div class="widget-body-inner" >
                                                  <div class="widget-main form">     

                                                        <div class="row">


                                                                 <div class="col-md-12" style="margin: 0px;line-height: 155%;">

                                                                          <div class="col-md-4">
                                                                               <?php if(($modelPersonal->usuario_ini_id!=NULL||$modelPersonal->usuario_ini_id!="")): ?>
                                                                              <label class="col-md-12"> <b>Creado por: </b></label>                      
                                                                              <label class="col-md-12"> 
                                                                                  <?php 


                                                                                  echo CHtml::encode($modelPersonal->usuarioIni->nombre." ".$modelPersonal->usuarioIni->apellido);
                                                                                  ?>  </label>                           
                                                                               <div class="row col-md-12" >  </div>  
                                                                                <?php endif; ?>
                                                                          </div>

                                                                          <div class="col-md-4">
                                                                              <?php if(($modelPersonal->usuario_act_id!=NULL||$modelPersonal->usuario_act_id!="")): ?>
                                                                              <label class="col-md-12"><b>Modificado por: </b> </label>                      
                                                                              <label class="col-md-12"> 
                                                                                  <?php 
                                                                                        echo CHtml::encode($modelPersonal->usuarioAct->nombre." ".$modelPersonal->usuarioAct->apellido);
                                                                                    ?>  
                                                                              </label>                           
                                                                               <div class="row col-md-12" >  </div> 
                                                                               <?php endif; ?>
                                                                          </div>


                                                                          <div class="col-md-4">
                                                                              <?php if((($modelPersonal->usuario_act_id!=NULL||$modelPersonal->usuario_act_id!=""))&&($modelPersonal->estatus=="I")): ?>
                                                                              <label class="col-md-12"> <b>Eliminado por:</b> </label>                      
                                                                              <label class="col-md-12"> 
                                                                              <?php 
                                                                                echo CHtml::encode($modelPersonal->usuarioAct->nombre." ".$modelPersonal->usuarioAct->apellido);
                                                                              ?>  </label>                           
                                                                               <div class="row col-md-12" >  </div>  
                                                                               <?php endif; ?>
                                                                          </div>

                                                                  </div> 

                                                                 <div class="col-md-12"> <div class="space-6"> </div> </div> <!-- salto de linea -->


                                                                  <div class="col-md-12" style="margin: 0px;line-height: 155%;">

                                                                          <div class="col-md-4">
                                                                              <?php if(($modelPersonal->usuario_ini_id!=NULL||$modelPersonal->usuario_ini_id!="")): ?>

                                                                              <label class="col-md-12"> <b>Fecha de creación: </b></label>                      
                                                                              <label class="col-md-12"> 
                                                                                  <?php

                                                                                   echo CHtml::encode(date("d-m-Y H:i:s",strtotime($modelPersonal->fecha_ini)));

                                                                                  ?>  </label>                           
                                                                               <div class="row col-md-12" >  </div>  

                                                                                <?php endif; ?>
                                                                          </div>

                                                                          <div class="col-md-4">
                                                                               <?php if(($modelPersonal->usuario_act_id!=NULL||$modelPersonal->usuario_act_id!="")): ?>
                                                                              <label class="col-md-12"><b>Fecha de Modificación: </b> </label>                      
                                                                              <label class="col-md-12"> 
                                                                                  <?php 
                                                                                       echo CHtml::encode(date("d-m-Y H:i:s",strtotime($modelPersonal->fecha_act)));
                                                                              ?>  </label>                           
                                                                               <div class="row col-md-12" >  </div>  
                                                                               <?php endif; ?>
                                                                          </div>


                                                                          <div class="col-md-4">
                                                                              <?php if((($modelPersonal->usuario_act_id!=NULL||$modelPersonal->usuario_act_id!=""))&&($modelPersonal->estatus=="I")): ?>
                                                                              <label class="col-md-12"> <b>Fecha de Eliminaci&oacute;n:</b> </label>                      
                                                                              <label class="col-md-12"> <?php 
                                                                              echo CHtml::encode(date("d-m-Y H:i:s",strtotime($modelPersonal->fecha_act)));

                                                                              ?>  </label>                           
                                                                               <div class="row col-md-12" >  </div>  
                                                                                <?php endif; ?>
                                                                          </div>





                                                                  </div> 

                                                                <div class="col-md-12"> <div class="space-6"> </div> </div> <!-- salto de linea -->




                                                                    </div> <!--div row -->



                                                  </div>
                                              </div>
                                          </div>
                                      </div>


                                    <?php endif; ?>

                            </div>
                            <?php $this->endWidget(); ?>
                        </div><!-- form -->

                        <div id="dialog_error" class="hide"></div>
                        
                       
                        
 <?php  
 Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/plantel/estructura-plantel/_datosGenerales.js',CClientScript::POS_END ); 
 Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js', CClientScript::POS_END);
 ?>