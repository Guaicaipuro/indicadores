<style type="text/css">

    .personal table, .personal th, .personal td {
        border: 1px solid grey;
        padding-bottom: 0px;
        margin-bottom: 5px;
    }
    .personal_datos table {
        border-collapse: collapse;
        border-spacing: 10px;
        text-align:center;

    }
</style>

<?php
if (isset($docentes_seccion) AND $docentes_seccion != array()) {
    ?>
    <div class >
     <?php
        $c = 0;
        foreach ($docentes_seccion as $key => $value) {
        if($c==0){ 
            $grado = isset($value['grado']) ? $value['grado'] : '';
            $seccion = isset($value['seccion']) ? $value['seccion'] : '';
            ?>
<table   style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;">
    <tr>
        <th colspan="14" align="center" style="background:#E5E5E5; padding:3px;">
            DOCENTES DE <?php echo $grado; ?> SECCION <?php echo $seccion; ?>
        </th>
    </tr>
</table>
<table class="personal" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-collapse: collapse;
       border-spacing: 0px;
       text-align:center; border: 1px solid grey; ">
    <tr style="border: 1px solid grey;">
        <th width="28px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
            Nac.
        </th>
        <th width="76px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
            Documento de Identidad
        </th>
  <!--      <th width="80px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
            Cédula Escolar
        </th> -->
        <th width="135px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
            Apellidos
        </th>
        <th width="135px" style=" border: 1px solid grey;" rowspan="2"  class="center">
            Nombres
        </th>
        <th width="110px" style=" border: 1px solid grey;" rowspan="2"  class="center">
            Correo
        </th>
        <th width="60px" style=" border: 1px solid grey;" colspan="3" class="center">
            Fecha de Nac.
        </th>

    </tr>
    <tr style=" border: 1px solid grey;" style=" border: 1px solid grey;">>
        <th width="20px" style=" border: 1px solid grey;"  class="center">
            Día
        </th>
        <th width="20px" style=" border: 1px solid grey;"  class="center">
            Mes
        </th>
        <th width="20px" style=" border: 1px solid grey;"  class="center">
            Año
        </th>
    </tr>
</table>
        <?php }
        else {
            $gradoTemp = isset($value['grado']) ? $value['grado'] : '';
            $seccionTemp = isset($value['seccion']) ? $value['seccion'] : ''; 
             if($grado!=$gradoTemp OR $seccion!=$seccionTemp){
                $grado = $gradoTemp;
                $seccion=$seccionTemp;
                echo "</table></div>";
                ?>
                <br>
                <table   style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;">
    <tr>
        <th colspan="14" align="center" style="background:#E5E5E5; padding:3px; marging-top:30px;">
            DOCENTES DE <?php echo $grado; ?> SECCION <?php echo $seccion; ?>
        </th>
    </tr>
</table>
<table class="personal" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-collapse: collapse;
       border-spacing: 0px;
       text-align:center; border: 1px solid grey; ">
    <tr style="border: 1px solid grey;">
        <th width="28px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
            Nac.
        </th>
        <th width="76px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
            Documento de Identidad
        </th>
  <!--      <th width="80px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
            Cédula Escolar
        </th> -->
        <th width="135px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
            Apellidos
        </th>
        <th width="135px" style=" border: 1px solid grey;" rowspan="2"  class="center">
            Nombres
        </th>
        <th width="110px" style=" border: 1px solid grey;" rowspan="2"  class="center">
            Correo
        </th>
        <th width="60px" style=" border: 1px solid grey;" colspan="3" class="center">
            Fecha de Nac.
        </th>

    </tr>
    <tr style=" border: 1px solid grey;" style=" border: 1px solid grey;">>
        <th width="20px" style=" border: 1px solid grey;"  class="center">
            Día
        </th>
        <th width="20px" style=" border: 1px solid grey;"  class="center">
            Mes
        </th>
        <th width="20px" style=" border: 1px solid grey;"  class="center">
            Año
        </th>
    </tr>
</table>
                <?php
            }
        }
        ?> 
        <table class="personal_datos" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-collapse: collapse;
           border-spacing: 0px;
           text-align:center; border: 1px solid grey; ">
<?php
            $nacionalidad = isset($value['tdocumento_identidad']) ? $value['tdocumento_identidad'] : '';
            $documento_identidad = isset($value['documento_identidad']) ? $value['documento_identidad'] : '';
            //$tdocumento_identidad = isset($value['tdocumento_identidad']) ? $value['tdocumento_identidad'] : '';
           // $cedula_escolar = isset($value['cedula_escolar']) ? $value['cedula_escolar'] : '';
           // $serial = isset($value['serial']) ? $value['serial'] : '';
            $nombres = isset($value['nombres']) ? $value['nombres'] : '';
            $apellidos = isset($value['apellidos']) ? $value['apellidos'] : '';
            $correo = isset($value['correo']) ? $value['correo'] : '';
            //     $sexo = isset($value['sexo']) ? $value['sexo'] : '';
          //  $ciudad_nacimiento = isset($value['ciudad_nacimiento']) ? $value['ciudad_nacimiento'] : '';
            $fecha_nacimiento = (isset($value['fecha_nacimiento'])) ? $value['fecha_nacimiento'] : '';
           // $observacion = isset($value['observacion']) ? $value['observacion'] : '';
            $fecha_array = explode('-', $fecha_nacimiento);
            $anio = isset($fecha_array[0]) ? $fecha_array[0] : '';
            $mes = isset($fecha_array[1]) ? $fecha_array[1] : '';
            $dia = isset($fecha_array[2]) ? $fecha_array[2] : '';
        ?>

         
        
        <tr>


                <td  width="28px" align="center">
                    <?php echo $nacionalidad; ?>
                </td>
                <td  width="110px"  align="center">
                    <?php echo $documento_identidad; ?>
                </td>

                <td  width="135px"  align="center">
                    <?php echo $apellidos; ?>
                </td>
                <td  width="135px"  align="center">
                    <?php echo $nombres; ?>
                </td>
                <td  width="110px"  align="center">
                    <?php echo $correo; ?>
                    <?php if ($correo==''){
                        echo '---------------';
                    }?>
                </td>
                <td width="30px"  align="center">
                    <?php echo $dia; ?>
                </td>
                <td width="30px"  align="center">
                    <?php echo $mes; ?>
                </td>
                <td  width="30px" align="center">
                    <?php echo $anio; ?>
                </td>
        </tr>
        <?php 
        $c++;
        }
        echo "</table></div>";
     ?>   

    </div>
     <?php }
?>
       