<?php
/*
 * DATOS DEL PLANTEL
 */

$personal = isset($tipoPersonal['nombre']) ? $tipoPersonal['nombre'] : '';
?>
<img src="<?php echo yii::app()->basePath . '/../public/images/barra_n.png'; ?>" />
<br /><br/>
<table style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;">

    <tr>
        <td colspan="3" align="center" style="background:#E5E5E5; padding:3px;">
            <b>DATOS DEL PLANTEL</b>
        </td>
    </tr>

    <tr >
        <td>
            <b>C&oacute;digo del plantel:</b>
            <?php echo $model->cod_plantel; ?></td>
        <td colspan="2">
            <b>Nombre del Plantel:</b>
            <?php echo $model->nombre; ?>
        </td>
    </tr>
    <tr>
        <td width="200px">
            <b>C&oacute;digo Estad&iacute;stico:</b>
            <?php echo $model->cod_estadistico; ?>
        </td>

        <td width="250px">
            <b>Dirección:</b>
            <?php echo $model->direccion; ?>
        </td>

<!--        <td width="200px">
            <b>Año Escolar:</b>
            <?php echo $periodo_escolar; ?>
        </td>
    -->
    </tr>
    <tr>
        <td>
            <b>Municipio:</b>
            <?php echo $model->municipio->nombre; ?>
        </td>
        <td>
            <b>Zona Educativa:</b>
            <?php echo $model->zonaEducativa->nombre; ?>
        </td>


    </tr>
    <tr>
    </table>
<table   style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;">
    <tr>
        <th colspan="14" align="center" style="background:#E5E5E5; padding:3px;">
            PERSONAL <?php echo $personal; ?> DEL PLANTEL
        </th>
    </tr>
</table>
<table class="personal" style="font-size:8px; margin-bottom:5px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-collapse: collapse;
       border-spacing: 0px;
       text-align:center; border: 1px solid grey; ">
    <tr style="border: 1px solid grey;">
        <th width="28px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
            Nac.
        </th>
        <th width="76px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
            Documento de Identidad
        </th>
  <!--      <th width="80px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
            Cédula Escolar
        </th> -->
        <th width="100px" style=" border: 1px solid grey;"  rowspan="2"  class="center">
            Apellidos
        </th>
        <th width="100px" style=" border: 1px solid grey;" rowspan="2"  class="center">
            Nombres
        </th>
        <th width="100px" style=" border: 1px solid grey;" rowspan="2"  class="center">
            Correo
        </th>
        </th>
        <th width="120px" style=" border: 1px solid grey;" rowspan="2"  class="center">
            Condición Nómina
        </th>
        <th width="100px" style=" border: 1px solid grey;" rowspan="2"  class="center">
            Estatus
        </th>
        <th width="80px" style=" border: 1px solid grey;" colspan="3" class="center">
            Fecha de Nac.
        </th>

    </tr>
    <tr style=" border: 1px solid grey;" style=" border: 1px solid grey;">>
        <th width="30px" style=" border: 1px solid grey;"  class="center">
            Día
        </th>
        <th width="30px" style=" border: 1px solid grey;"  class="center">
            Mes
        </th>
        <th width="30px" style=" border: 1px solid grey;"  class="center">
            Año
        </th>
    </tr>
</table>