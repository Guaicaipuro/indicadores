
<?php

/* @var $this EstructuraController  resultadoOperacion   */
/* @var $model PersonalPlantel */

$this->breadcrumbs=array(
    'Planteles'=>array('/planteles/'),
    'Estructura del Plantel',
);
$this->pageTitle = 'Estructura del Plantel';
Yii::app()->clientScript->registerCoreScript('yiiactiveform');
echo CHtml::scriptFile('/public/js/modules/plantel/estructura-plantel/admin.js');


?>
<div>
                
    <?php $this->renderPartial('_viewPlantel', array('modelPlantel' => $modelPlantel)); ?>
                
</div>
<div class="widget-box">
    <div class="widget-header">
        <h5>Estructura del Plantel</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                
                <div id="dialogPantallaAdmin" class="hide">  </div>
                
                <div>
                    <div id="resultadoOperacionAdmin">
                        <div class="infoDialogBox">
                            <p>
                                Antes de Registrar el Personal Obrero debe ir a el Modulo de Área Común del Plantel
                            </p>
                            
                        </div>
                    </div>
                    <div class="pull-left" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/planteles/areaComunPlantel/lista/id/".$plantel_id); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Áreas Comunes del Plantel </a>
                    </div>

                    <?php if(Yii::app()->user->group==UserGroups::ROOT): ?> <!-- si el usuario es administrador se muestran los botones de reportes -->
                        <div class="pull-left" style="padding-left:5px;">

                                <?php $this->renderPartial('_opcionesReportePersonal', array('plantel_id' => $plantel_id,'periodo'=>base64_encode($periodo_id))); ?>
                        </div>
                        <div class="pull-left" style="padding-left:5px;">

                                <?php $this->renderPartial('_opcionesReporteCuadratura', array('plantel_id' => $plantel_id,'periodo'=>base64_encode($periodo_id))); ?>
                        </div>
                                <!-- las vistas render se encuentran dentro del modulo planteles/estructura -->
                    <?php endif; ?>


                    <div class="pull-right" style="padding-left:10px;">
                     
                       <?php if ($estatus_proceso_registro == Constantes::ESTATUS_PROCESO_ACTIVO): ?>
                        
                        <a href="<?php echo $this->createUrl("/planteles/estructura/registro/id/".$plantel_id); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Personal</a>
                        
                        <?php endif; ?>
                    </div>


                    <div class="row space-20"></div>

                </div>
                <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>

                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'personal-plantel-grid',
                    'dataProvider'=>$model->search(base64_decode($plantel_id)),
                    'filter'=>$model,
                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                    'summaryText'=> '<br/><h5> Cantidad Total de Registros: {count} / Mostrando: del {start} al {end} </h5>', 
                    'pager' => array(
                        'header' => '',
                        'htmlOptions' => array('class' => 'pagination'),
                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                    ),
                    'afterAjaxUpdate' => "
                function(){

                }",
                    'columns'=>array(
                        /* array(
                             'header' => '<center>id</center>',
                             'name' => 'id',
                             'htmlOptions' => array(),
                             //'filter' => CHtml::textField('PersonalPlantel[id]', $model->id, array('title' => '',)),
                         ),*/




                        array(
                            'header' => '<center>Nombres</center>',
                            'name' => 'nombres',
                            //'value' => '(isset($data) AND is_object($data->personal) AND isset($data->personal->nombres))?$data->personal->nombres:""',
                            //'htmlOptions' => array('id'=>'PersonalPlantel_nombres'),
                            'filter' => CHtml::textField('PersonalPlantel[nombres]', '', array('title' => '',)),
                        ),
                        array(
                            'header' => '<center>Apellidos</center>',
                            'name' => 'apellidos',
                            //'value' => '(isset($data) AND is_object($data->personal) AND isset($data->personal->apellidos))?$data->personal->apellidos:""',
                            //'htmlOptions' => array('id'=>'PersonalPlantel_apellidos'),

                            'filter' => CHtml::textField('PersonalPlantel[apellidos]', '', array('title' => '',)),
                        ),
                        array(
                            'header' => '<center>Tipo de Documento</center>',
                            'name' => 'tdocumento_identidad',
                            //'htmlOptions' => array('id'=>'PersonalPlantel_tdocumento_identidad'),
                            
                             'value' => array($this, 'getNacionalidad'),
                            
                            'filter' => array("V"=>"VENEZOLANO", "E"=>"EXTRANJERO","P"=>"PASAPORTE"),
                        ),
                        array(
                            'header' => '<center>Documento de Identidad</center>',
                            'name' => 'documento_identidad',
                            //'htmlOptions' => array('id'=>'PersonalPlantel_documento_identidad'),
                            //'value' => '(isset($data) AND is_object($data->personal) AND isset($data->personal->documento_identidad))?$data->personal->documento_identidad:""',
                            'filter' => CHtml::textField('PersonalPlantel[documento_identidad]', '', array('title' => '',)),
                        ),
                        
                        array(
                            'header' => '<center>Tipo de Personal</center>',
                            'name' => 'tipo_personal_id',
                            //'htmlOptions' => array('id'=>'tipo_personal_id', 'class'=>'TipoPersonal'),
                            'value' => '$data->tipo_personal',
                            'filter'=>CHtml::listData(TipoPersonal::model()->findAll(array('order'=>'nombre ASC')),'id','nombre'),
                        ),
                        array(
                            'header' => '<center>Periodo Escolar</center>',
                            'name' => 'periodo_id',
                            'value' => '$data->periodo',                            
                            'filter'=>CHtml::listData(PeriodoEscolar::model()->findAll(array('order'=>'id DESC')),'id','periodo'),
                        ),
                        
                        
                        
                        
                        
                        
                        array(
                            'header' => '<center>Estatus </center>',
                            'name' => 'estatus',
                            //'htmlOptions' => array('id'=>'PersonalPlantel_tdocumento_identidad'),
                            
                             'value' => array($this, 'estatus'),
                            
                            'filter' => array("A"=>"ACTIVO", "I"=>"INACTIVO",),
                        ),
                       
                        array(
                            'type' => 'raw',
                            'header' => '<center>Acción</center>',
                            'value' => array($this, 'getActionButtons' ),
                            'htmlOptions' => array('nowrap'=>'nowrap'),
                        ),
                    ),
                )); ?>
            </div>
            
        </div>
    </div>
</div>

<div class="space-10"><div class="row"></div></div>
<div class="row">
    <div class="col-md-6">
        <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/"); ?>" id="btnRegresar">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
        
        <?php $this->renderPartial('/_accionesSobrePlantel', array('plantel_id' => $modelPlantel->id)); ?>
        
        
               
        <?php if ($estatus_proceso_registro == Constantes::ESTATUS_PROCESO_ACTIVO) 
              { ?>
           
                <a id="btnCerrarProcesoRegistroPersonal" style="height:42px;" class="btn btn-warning change-status">
                    <i class="fa fa-lock"></i>
                    Cerrar Proceso de Registro de Personal
                </a>
            <?php
             } 
             else 
             {
                if (Yii::app()->user->pbac('admin')) 
                {
                    ?> <a id="btnApeturarProcesoRegistroPersonal"style="height:42px;" class="btn btn-warning change-status">
                            <i class="fa fa-unlock"></i>
                            Aperturar Proceso de Registro de Personal
                       </a>
                <?php
                }
             }
        
        ?>
        
        <div>
            <?php echo CHtml::hiddenField("admin_plantel_id",$plantel_id);  ?>
            <?php echo CHtml::hiddenField("admin_periodo_id",$periodo_id);  ?>
            
            
        </div>
        
        
    </div>
</div>