<?php
/* @var $this ConsultarController */
/* @var $model Plantel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'plantel-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performamAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'cod_estadistico'); ?>
		<?php echo $form->textField($model,'cod_estadistico'); ?>
		<?php echo $form->error($model,'cod_estadistico'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cod_plantel'); ?>
		<?php echo $form->textField($model,'cod_plantel',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'cod_plantel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'planta_fisica_id'); ?>
		<?php echo $form->textField($model,'planta_fisica_id'); ?>
		<?php echo $form->error($model,'planta_fisica_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'denominacion_id'); ?>
		<?php echo $form->textField($model,'denominacion_id'); ?>
		<?php echo $form->error($model,'denominacion_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_dependencia_id'); ?>
		<?php echo $form->textField($model,'tipo_dependencia_id'); ?>
		<?php echo $form->error($model,'tipo_dependencia_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado_id'); ?>
		<?php echo $form->textField($model,'estado_id'); ?>
		<?php echo $form->error($model,'estado_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'municipio_id'); ?>
		<?php echo $form->textField($model,'municipio_id'); ?>
		<?php echo $form->error($model,'municipio_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parroquia_id'); ?>
		<?php echo $form->textField($model,'parroquia_id'); ?>
		<?php echo $form->error($model,'parroquia_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'localidad_id'); ?>
		<?php echo $form->textField($model,'localidad_id'); ?>
		<?php echo $form->error($model,'localidad_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textArea($model,'direccion',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'distrito_id'); ?>
		<?php echo $form->textField($model,'distrito_id'); ?>
		<?php echo $form->error($model,'distrito_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'zona_educativa_id'); ?>
		<?php echo $form->textField($model,'zona_educativa_id'); ?>
		<?php echo $form->error($model,'zona_educativa_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modalidad_id'); ?>
		<?php echo $form->textField($model,'modalidad_id'); ?>
		<?php echo $form->error($model,'modalidad_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nivel_id'); ?>
		<?php echo $form->textField($model,'nivel_id'); ?>
		<?php echo $form->error($model,'nivel_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'condicion_estudio_id'); ?>
		<?php echo $form->textField($model,'condicion_estudio_id'); ?>
		<?php echo $form->error($model,'condicion_estudio_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'correo'); ?>
		<?php echo $form->textField($model,'correo',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'correo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono_fijo'); ?>
		<?php echo $form->textField($model,'telefono_fijo',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'telefono_fijo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono_otro'); ?>
		<?php echo $form->textField($model,'telefono_otro',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'telefono_otro'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'director_actual_id'); ?>
		<?php echo $form->textField($model,'director_actual_id'); ?>
		<?php echo $form->error($model,'director_actual_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'director_supl_actual_id'); ?>
		<?php echo $form->textField($model,'director_supl_actual_id'); ?>
		<?php echo $form->error($model,'director_supl_actual_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subdirector_actual_id'); ?>
		<?php echo $form->textField($model,'subdirector_actual_id'); ?>
		<?php echo $form->error($model,'subdirector_actual_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subdirector_supl_actual_id'); ?>
		<?php echo $form->textField($model,'subdirector_supl_actual_id'); ?>
		<?php echo $form->error($model,'subdirector_supl_actual_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coordinador_actual_id'); ?>
		<?php echo $form->textField($model,'coordinador_actual_id'); ?>
		<?php echo $form->error($model,'coordinador_actual_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coordinador_supl_actual_id'); ?>
		<?php echo $form->textField($model,'coordinador_supl_actual_id'); ?>
		<?php echo $form->error($model,'coordinador_supl_actual_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'clase_plantel_id'); ?>
		<?php echo $form->textField($model,'clase_plantel_id'); ?>
		<?php echo $form->error($model,'clase_plantel_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'condicion_infra_id'); ?>
		<?php echo $form->textField($model,'condicion_infra_id'); ?>
		<?php echo $form->error($model,'condicion_infra_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'categoria_id'); ?>
		<?php echo $form->textField($model,'categoria_id'); ?>
		<?php echo $form->error($model,'categoria_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'regimen_id'); ?>
		<?php echo $form->textField($model,'regimen_id'); ?>
		<?php echo $form->error($model,'regimen_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'posee_electricidad'); ?>
		<?php echo $form->checkBox($model,'posee_electricidad'); ?>
		<?php echo $form->error($model,'posee_electricidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'posee_edificacion'); ?>
		<?php echo $form->checkBox($model,'posee_edificacion'); ?>
		<?php echo $form->error($model,'posee_edificacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'logo'); ?>
		<?php echo $form->textField($model,'logo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'logo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'observacion'); ?>
		<?php echo $form->textArea($model,'observacion',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'observacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'es_tecnica'); ?>
		<?php echo $form->checkBox($model,'es_tecnica'); ?>
		<?php echo $form->error($model,'es_tecnica'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'especialidad_tec_id'); ?>
		<?php echo $form->textField($model,'especialidad_tec_id'); ?>
		<?php echo $form->error($model,'especialidad_tec_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario_ini_id'); ?>
		<?php echo $form->textField($model,'usuario_ini_id'); ?>
		<?php echo $form->error($model,'usuario_ini_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_ini'); ?>
		<?php echo $form->textField($model,'fecha_ini'); ?>
		<?php echo $form->error($model,'fecha_ini'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario_act_id'); ?>
		<?php echo $form->textField($model,'usuario_act_id'); ?>
		<?php echo $form->error($model,'usuario_act_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_act'); ?>
		<?php echo $form->textField($model,'fecha_act'); ?>
		<?php echo $form->error($model,'fecha_act'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_elim'); ?>
		<?php echo $form->textField($model,'fecha_elim'); ?>
		<?php echo $form->error($model,'fecha_elim'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estatus'); ?>
		<?php echo $form->textField($model,'estatus',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'estatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estatus_plantel_id'); ?>
		<?php echo $form->textField($model,'estatus_plantel_id'); ?>
		<?php echo $form->error($model,'estatus_plantel_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'latitud'); ?>
		<?php echo $form->textField($model,'latitud'); ?>
		<?php echo $form->error($model,'latitud'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'longitud'); ?>
		<?php echo $form->textField($model,'longitud'); ?>
		<?php echo $form->error($model,'longitud'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'annio_fundado'); ?>
		<?php echo $form->textField($model,'annio_fundado'); ?>
		<?php echo $form->error($model,'annio_fundado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'turno_id'); ?>
		<?php echo $form->textField($model,'turno_id'); ?>
		<?php echo $form->error($model,'turno_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'genero_id'); ?>
		<?php echo $form->textField($model,'genero_id'); ?>
		<?php echo $form->error($model,'genero_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_ubicacion_id'); ?>
		<?php echo $form->textField($model,'tipo_ubicacion_id'); ?>
		<?php echo $form->error($model,'tipo_ubicacion_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->