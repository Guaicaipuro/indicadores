<?php
/* @var $this PlantelController */
/* @var $model Plantel */
/* @var $form CActiveForm */
$this->breadcrumbs = array(
    'Planteles' => array('index'),
    'Consulta de Plantel',
);
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'plantel-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        //  'validateOnSubmit' => true,
        'validateOnType' => true,
        'validateOnChange' => true),
        ));
?>
<?php //echo $form->errorSummary($model);  ?>
<?php
$groupId = Yii::app()->user->group;
$usuarioId = Yii::app()->user->id;
$view = '_viewDatosGenerales';
/* IDENTIFICACION DE GRUPOS DE USUARIO PARA MOSTRAR LA BUSQUEDA AVANZADA */
if (($groupId == UserGroups::ADMIN_0) || ($groupId == UserGroups::JEFE_DRCEE) || ($groupId == 18) || ($groupId == 24) || ($groupId == UserGroups::ADMIN_2) || ($groupId == UserGroups::MISION_RIBAS_NAC) || ($groupId == UserGroups::MISION_RIBAS_REG) || ($groupId == UserGroups::ADMIN_REG_CONTROL)) {
    $view = '_viewDatosGenerales';
}/* JEFE DE REGISTRO Y CONTROL DE EVALUACION DE ESTUDIO */
if ($groupId == UserGroups::CCEE_PLANTEL) {
    $view = '_viewDatosGCoordinador';
}/* COORDINADOR DE CONTROL DE ESTUDIO Y EVALUACION DE PLANTEL */
if ($groupId == UserGroups::JEFE_ZONA) {
    $view = '_viewDatosGCoordinador';
}/* COORDINADOR DE ZONA EDUCATIVA */
if (($groupId == 29) || ($groupId == 42)) {
    $view = '_viewDatosGDirector';
}/* DIRECTOR DEL PLANTEL */
if ($groupId == 30) {
    $view = '_viewDatosGSecretaria';
}/* SECRETARIA DEL PLANTEL */

$plantelId = base64_decode($_GET['id']);
?>

<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos generales</a></li>
        <li><a href="#modalidad" data-toggle="tab">Modalidad</a></li>
        <li><a href="#desarrollo" data-toggle="tab">Desarrollo Endógeno</a></li>
        <li><a href="#servicio" data-toggle="tab">Servicios</a></li>
        <li><a href="#autoridades" data-toggle="tab">Autoridades</a></li>
        <!-- <li><a href="#otros" data-toggle="tab">Otros</a></li>-->
        <?php
        /*COORDINADOR DE ZONA, DIRECTOR Y ROOT (ADMIN)*/
        if ($groupId == UserGroups::ADMIN_0 || $groupId == UserGroups::COORD_ZONA || $groupId == UserGroups::DIRECTOR) {
            ?>
            <li><a href="#aula" data-toggle="tab">Aula</a></li>
            <?php
        }
        ?>
    </ul>

    <div class="tab-content">

        <div class="tab-pane" id="modalidad">
        <?php
            $this->renderPartial('_viewModalidad',array('modelPlantelModalidad'=>$modelPlantelModalidad,'modalidad'=>$modalidad ));
        ?>
        </div>
        
        <div class="tab-pane" id="desarrollo">
            <?php $this->renderPartial('_viewDesarrolloEndogeno', array('model' => $model, 'plantel_id' => $plantelId)); ?>
        </div>


        <div class="tab-pane" id="servicio">
            <?php $this->renderPartial('_viewServicio', array('model' => $model, 'plantel_id' => $plantelId)); ?>
        </div>


        <div class="tab-pane" id="autoridades">
            <?php $this->renderPartial('_viewAutoridad', array('model' => $model, 'plantel_id' => $plantelId)); ?>
        </div>
        <!--<div class="tab-pane" id="otros">Otros</div>-->
        <div class="tab-pane" id="aula">
            <?php
            $this->renderPartial('_formAula', array('plantel_id' => $model->id, 'model' => $modelAula, /* , 'dataProvider' => $dataProviderAula */));
            ?>
        </div>


        <div class="tab-pane active" id="datosGenerales">
            <?php
            if($view){
                $this->renderPartial($view, array('model' => $model, 'plantel_id' => $plantelId));
            }
            ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>

    <hr>

    <div class="col-xs-6">
        <a class="btn btn-danger" href="/planteles/consultar/index" id="btnRegresar">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
         <?php $this->renderPartial('/_accionesSobrePlantel', array('plantel_id' => $plantelId)) ?>
    </div>

    <div class="col-xs-6" align="right">
        <a class="btn btn-primary pull-right" href="/planteles/consultar/reporte?id=<?php echo $_GET['id']; ?>" id="btnImprimir">
            Imprimir
            <i class="fa fa-print"></i>
        </a>
    </div>
</div>