<div class="tab-pane active" id="modalidad">
    <div class="widget-box">
        <div class="widget-header">
            <h5>Modalidad</h5>
        </div>
        <div class="widget-body">
            <div style="display:block;" class="widget-body-inner">
                <div class="widget-main">
                    <div>
                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'plantel-modalidad-grid',
                        'dataProvider' => $modelPlantelModalidad->search(),
                        //'filter' => $modelPlantelModalidad,
                        //'ajaxUrl' => '/planteles/modificar/actualizarGridPlantelModalidad/id/'.$codPlantel,
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'summaryText' => 'Mostrando {start}-{end} de {count}',
                                 'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination'),
                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                            ),
                        //'afterAjaxUpdate' => "",
                        'columns' => array(
                            array(
                                'header'=>'<center>Modalidad</center>',
                                'name'=>'modalidad_id',
                                'value'=>'$data->modalidad->nombre',
                                 ),
                            array(
                                'header'=>'<center>Estatus</center>',
                                'name'=>'estatus',
                                'value'=>'$data->estatus=="I" ? "INACTIVO" :"ACTIVO" ',
                                    ),
                        ),
                    ));
                       ?> 
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
   