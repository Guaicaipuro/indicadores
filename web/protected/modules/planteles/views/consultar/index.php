<?php
$this->breadcrumbs = array(
    'Planteles',
);

if (Yii::app()->user->pbac('planteles.consultar.read') || Yii::app()->user->pbac('planteles.consultar.admin')) {
    ?>

    <?php
    /* IDENTIFICACION DE GRUPOS DE USUARIO PARA MOSTRAR LA BUSQUEDA AVANZADA */
//$search = NULL;
    if (Yii::app()->user->pbac('planteles.consultar.admin')
    ) {
        $search = '_search'; // TODOS
    }
    else {
        if (Yii::app()->user->pbac('planteles.consultar.read')){
            $search = '_searchEstado';
        }
    }

    if (isset($search)) { ?>
        <div class="widget-box collapsed">
            <div class="widget-header">
                <h5>B&uacute;squeda Avanzada de Planteles</h5>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="icon-chevron-down"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div style="display: none;" class="widget-body-inner">
                    <div class="widget-main">
                        <?php echo CHtml::link('', '#', array('class' => 'search-button')); ?>
                        <div class="search-form" style="display:block">
                            <?php
                            $this->renderPartial($search, array(
                                'model' => $model,
                                'estadoId' => $estadoId,
                                'nivel'=>$nivel,
                            ));
                            ?>
                        </div><!-- search-form -->
                    </div>
                </div>
            </div>

        </div>
        <?php
    }
}
?>

<div class="widget-box">

    <?php
    Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	$('.search-form form').submit(function(){
		$('#plantel-grid').yiiGridView('update', {
			data: $(this).serialize()
		});
		return false;
	});
	");
    ?>

    <div class="widget-header">
        <h5>Planteles</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
                <div>
                    <?php
                    if ((Yii::app()->user->pbac('planteles.crear.write'))) {
                        ?>
                        <div class="pull-right" style="padding-left:10px;">
                            <a href="<?php echo Yii::app()->baseUrl . '/planteles/crear/' ?>" data-last="Finish" class="btn btn-success btn-next btn-sm">
                                <i class="fa fa-plus icon-on-right"></i>
                                Registrar nuevo plantel
                            </a>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="col-lg-12"><div class="space-6"></div></div>

                    <?php
                    /* SI EL USUARIO ES COORDINADOR DE CONTROL DE ESTUDIO NO BUSCAR POR NOMBRE DE PLANTEL Y LA SECRETARIA */
                    $nombrePlantel = array(
                        'header' => '<center>Nombre del plantel</center>',
                        'name' => 'nombre',
                    );

                    $codigoPlantel = array(
                        'header' => '<center>Código del plantel</center>',
                        'name' => 'cod_plantel',
                        'filter' => CHtml::textField('Plantel[cod_plantel]', null),
                        //'filterHtmlOptions' => array('style' => 'display:none'),
                    );
                    $codigoEstadistico = array(
                        'header' => '<center>Código estadístico</center>',
                        'name' => 'cod_estadistico',
                        'filter' => CHtml::textField('Plantel[cod_estadistico]', null, array('maxlength' => 10)),
                    );
                    $tipoDependencia = array(
                        'header' => '<center>Tipo de dependencia</center>',
                        'name' => 'tipo_dependencia_id',
                        'value' => '(is_object($data->tipoDependencia) && isset($data->tipoDependencia->nombre))? $data->tipoDependencia->nombre: ""',
                        'filter' => CHtml::listData(
                            CTipoDependencia::getData(), 'id', 'nombre'
                        ),
                    );

                    /* EN CASO DE QUE SEA EL COORDINADOR DE ZONA groupsId = (25)
                     * NO PERMITA REALIZAR BUSQUEDAS DE OTRAS ZONAS (ESTADOS)
                     */
                    if (!Yii::app()->user->pbac('planteles.consultar.admin')){
                        $estado = array(
                            'header' => '<center>Estado</center>',
                            'name' => 'estado_id',
                            'value' => '(is_object($data->estado) && isset($data->estado->nombre))? $data->estado->nombre : ""',
                            'filter' => false,
                        );
                    } else {
                        $estado = array(
                            'header' => '<center>Estado</center>',
                            'name' => 'estado_id',
                            'value' => '(is_object($data->estado) && isset($data->estado->nombre))? $data->estado->nombre: ""',
                            'filter' => CHtml::listData(
                                CEstado::getData(), 'id', 'nombre'
                            ),
                        );
                    }
                    $denominacion = array(
                        'header' => '<center>Denominación</center>',
                        'name' => 'denominacion_id',
                        'value' => '(is_object($data->denominacion) && isset($data->denominacion->nombre))? $data->denominacion->nombre : ""',
                        'filter' => CHtml::listData(
                            CDenominacion::getData(), 'id', 'nombre'
                        ),
                    );

                    $estatusPlantel = array(
                        'header' => '<center title="Estatus del Nivel">Estatus</center>',
                        'name' => 'estatus_plantel_id',
                        'value' => '(is_object($data->estatusPlantel) && isset($data->estatusPlantel->nombre))? $data->estatusPlantel->nombre: ""',
                        'filter' => CHtml::listData(
                            CEstatusPlantel::getData(), 'id', 'nombre'
                        ),
                    );

                    //AGREGADO POR JONATHAN HUAMAN//
                    $modalidadPlantel = array(
                        //'type' => 'raw',
                        'name' => 'modalidad_id',
                        'filter' => CHtml::listData($modalidad,'id','nombre'),
                        'header' => '<center title="Modalidad">Modalidad</center>',
                        'value'  => array($this,'obtenerModalidad'),
                        'htmlOptions' => array('class' => 'modalidad','style'=>'text-align: center;'),
                    );
                    $nivelPlantel = array(
                        'type' => 'raw',
                        'name' => 'nivel_id',
                        'filter' => CHtml::listData($nivel,'id','nombre'),
                        'header' => '<center title="Nivel">Nivel</center>',
                        'value'  => array($this,'obtenerNivel'),
                        'htmlOptions' => array('class' => 'nivel','style'=>'text-align: center;'),
                    );

                    //FIN DE AGREGACION//                              
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'id' => 'plantel-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'pager' => array('pageSize' => 1),
                        'summaryText' => false,
                        'afterAjaxUpdate' => "function(){
                                $(function(){
                                            $('.dropdown-menu > li > a.trigger').on('click',function(e){
                                                var current=$(this).next();
                                                var grandparent=$(this).parent().parent();
                                                if($(this).hasClass('left-caret')||$(this).hasClass('right-caret'))
                                                $(this).toggleClass('right-caret left-caret');
                                                grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
                                                grandparent.find('.sub-menu:visible').not(current).hide();
                                                current.toggle();
                                                e.stopPropagation();
                                            });
                                            $('.dropdown-menu > li > a:not(.trigger)').on('click',function(){
                                                var root=$(this).closest('.dropdown');
                                                root.find('.left-caret').toggleClass('right-caret left-caret');
                                                root.find('.sub-menu:visible').hide();
                                            });
                                        });
                             $('#Plantel_cod_estadistico').bind('keyup blur', function() {
                                 keyNum(this, false);
                             });
                             $('#Plantel_cod_estadistico_view').bind('keyup blur', function() {
                                 keyNum(this, false);
                             });

                             $('#Plantel_telefono_fijo').bind('keyup blur', function() {
                                 keyNum(this, false);
                             });

                             $('#Plantel_telefono_otro').bind('keyup blur', function() {
                                 keyNum(this, false);
                             });

                             $('#Plantel_nfax').bind('keyup blur', function() {
                                 keyNum(this, false);
                             });

                             $('#Plantel_cod_plantel').bind('keyup blur', function() {
                                 keyAlphaNum(this, false);
                             });

                             $('#Plantel_cod_plantel_view').bind('keyup blur', function() {
                                 keyAlphaNum(this, false);
                             });

                             $('#cod_plantelNer').bind('keyup blur', function() {
                                 keyAlphaNum(this, false);
                             });
                             
                             $(\"select[name='Plantel[modalidad_id]']\").change(function(){
                                 //colocarModalidad();
                             });
                             
                             $(\"select[name='Plantel[nivel_id]']\").change(function(){
                                 //colocarNivel();
                             });
                             
                             //$('#Plantel_modalidad_id').change(function(){ colocarModalidad();  });
                         }",
                        'columns' => array(
                            $codigoPlantel,
                            //$codigoEstadistico,
                            $modalidadPlantel,
                            $nivelPlantel,
                            $nombrePlantel,
                            $tipoDependencia,
                            $estado,
                            $denominacion,
                            $estatusPlantel,
                            array(
                                'type' => 'raw',
                                'header' => '<center>Acciones</center>',
                                'value' => array($this, 'columnaAcciones'),
                                'htmlOptions' => array('nowrap' => 'nowrap'),
                            ),
                            /*array(
                                'name'=>'fecha_ini',
                                'value'=> array($this,'formatearFecha') ,
                            ),*/
                        ),
                        'pager' => array(
                            'header' => '',
                            'htmlOptions' => array('class' => 'pagination'),
                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>

<div id="dialogPantalla" class="hide"></div>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/plantel/consultarPlantel.js', CClientScript::POS_END);
?>
<style>
    .dropdown-menu>li
    {	position:relative;
        -webkit-user-select: none; /* Chrome/Safari */
        -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* IE10+ */
        /* Rules below not implemented in browsers yet */
        -o-user-select: none;
        user-select: none;
        cursor:pointer;
    }
    .dropdown-menu .sub-menu {
        left: -95%;
        position: absolute;
        top: 0;
        display:none;
        margin-top: -1px;
        border-top-left-radius:0;
        border-bottom-left-radius:0;
        border-right-color:#fff;
        box-shadow:none;
    }
    .right-caret:before,.left-caret:before
    {	content:"";
        border-bottom: 5px solid transparent;
        border-top: 5px solid transparent;
        display: inline-block;
        height: 0;
        vertical-align: middle;
        width: 0;
        margin-right:5px;
    }
    .right-caret:before
    {	border-right: 5px solid #ffaf46;
    }
    .left-caret:before
    {	border-left: 5px solid #ffaf46;
    }
</style>
<script>
    $(document).on('ready',function(){

        $(function(){
            $('.dropdown-menu > li > a.trigger').on('click',function(e){
                var current=$(this).next();
                var grandparent=$(this).parent().parent();
                if($(this).hasClass('left-caret')||$(this).hasClass('right-caret'))
                    $(this).toggleClass('right-caret left-caret');
                grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
                grandparent.find('.sub-menu:visible').not(current).hide();
                current.toggle();
                e.stopPropagation();
            });
            $('.dropdown-menu > li > a:not(.trigger)').on('click',function(){
                var root=$(this).closest('.dropdown');
                root.find('.left-caret').toggleClass('right-caret left-caret');
                root.find('.sub-menu:visible').hide();
            });
        });
        $("select[name='Plantel[modalidad_id]']").change(function(){
            //colocarModalidad();
        });
        $("select[name='Plantel[nivel_id]']").change(function(){
            //colocarNivel();
        });
        /*$('#Plantel_modalidad_id').change(function(){
         colocarModalidad();
         //refrescarGrid();
         });*/
    });
    function colocarModalidad()
    {
        var nombreModalidad= $("select[name='Plantel[modalidad_id]'] option:selected").text();
        nombreModalidad= nombreModalidad.replace('-Seleccione-','');
        setTimeout(function(){ $('.modalidad').html('');  $('.modalidad').html(nombreModalidad); }, 5000);
    }
    function colocarNivel()
    {
        var nombreNivel= $("select[name='Plantel[nivel_id]'] option:selected").text();
        nombreNivel= nombreNivel.replace('-Seleccione-','');
        setTimeout(function(){ $('.nivel').html('');  $('.nivel').html(nombreNivel); }, 5000);
    }
</script>
