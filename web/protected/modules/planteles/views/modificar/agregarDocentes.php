<?php
/* @var $this ModificarController */
/* @var $data CongresoPedagogicoDocente */
/* @autor Pedro Chacon
    Creacion 10/04/2015
    Ventana emergente con formulario para agregar docentes a un Congreso Pedagogico existente*/
?>

<!-- ALERTAS DE ERRORES EN EL FORMULARIO -->
<div class="view" align="center">
    <div  id="alerta" class="alertDialogBox hide">
                            <p class="note">
                            EL DOCUMENTO DE IDENTIDAD NO SE ENCUENTRA REGISTRADO EN LA BASE DE DATOS
                            </p>
                        </div>
    <div  id="alerta2" class="alertDialogBox hide">
                            <p class="note">
                            EL DOCENTE YA SE ENCUENTRA REGISTRADO EN ESTE CONGRESO
                            </p>
                        </div>
    <div  id="alerta3" class="alertDialogBox hide">
                            <p class="note">
                            DEBE INTRODUCIR UN NUMERO DE DOCUMENTO DE IDENTIDAD OBLIGATORIAMENTE
                            </p>
                        </div>





<div class="widget-box" id="congreso_datos_box" >
                            <div class="widget-header">
                                <h5>Datos Generales</h5>
                                <div class="widget-toolbar">
                                    
                                </div>
                            </div>


                            <!-- FORMULARIO PARA AGREGAR DOCENTES -->
                            <div class="widget-body">
                                <div class="widget-body-inner" style="display: block;">
                                    <div class="widget-main">
                                        <div class="row">
                                            <div class="form">
                                                <form id="docente-agregar-form">
                                                <?php
                                                $form = $this->beginWidget('CActiveForm', array(
                                                    'id' => 'docente-agregar-form',
                                                    'enableAjaxValidation' => false,
                                                    
                                                ));
                                                ?>

                                                <div class="row">
                                                    <div class="col-md-12">
            									
                                                        <?php echo $form->textField($model, 'congreso_id', array('class' => 'hide', 'value' => $congreso)); ?> <!-- CAMPO OCULTO PARA ASOCIAR EL REGISTRO AL CONGRESO ACTUAL -->

                                                         <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'tdocumento_identidad', array("class" => "col-md-12")); ?>
                                                            <?php echo $form->dropDownList($model, 'tdocumento_identidad', array('V' => 'Venezolano', 'E' => 'Extranjero'), array("class" => "col-md-12"));?>
                                                            <?php //echo $form->textField($model, 'tdocumento_identidad', array('class' => 'span-7', 'readOnly'=>'readOnly')); ?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'documento_identidad', array("class" => "col-md-12")); ?>
                                                            <?php echo $form->textField($model, 'documento_identidad', array('class' => 'span-7', 'required' => 'required', 'name'=>'documento_identidad', 'maxlength'=> '9')); ?>
                                                        </div>
                                                       
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6">

                                                            <?php echo $form->labelEx($model, 'nombres', array("class" => "col-md-12")); ?> <!-- CAMPO AUTOCOMPLETABLE -->
                                                            <?php echo $form->textField($model, 'nombres', array('class' => 'span-7','readOnly'=>'readOnly', 'style' => 'text-transform:uppercase;')); ?>

                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'apellidos', array("class" => "col-md-12")); ?> <!-- CAMPO AUTOCOMPLETABLE -->
                                                            <?php echo $form->textField($model, 'apellidos', array('class' => 'span-7','readOnly'=>'readOnly', 'style' => 'text-transform:uppercase;')); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <?php $this->endWidget(); ?>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                    
                                </div>
</div>
                    
</div>
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>

<script type="text/javascript">

    $(document).ready(function(){ //FUNCION QUE PERMITE SOLO EL USO DE CARACTERES NUMERICOS EN EL CAMPO #Documento_identidad
        $('#documento_identidad').bind('keyup', function() {
            keyNum(this, true);
        });
        $('#documento_identidad').bind('blur', function() {
            clearField(this);
        });
    });

    $( "#documento_identidad" ).blur(function() {
       completarCampos(); 


    });

    function completarCampos() //FUNCION QUE ENVIA LOS DATOS INGRESADOS, Y AUTOCOMPLETA O MUESTRA EL ERROR CORRESPONDIENTE
{
    var data = {cedula: $('#documento_identidad').val(),
                tipo_documento: $('#CongresoPedagogicoDocente_tdocumento_identidad').val(),
                congreso_id: $('#CongresoPedagogicoDocente_congreso_id').val()};
    var divResult='';
    var urlDir = '/planteles/modificar/completarCampos';
    var loadingEfect=true;
    var showResult=true;
    var method='POST';
    var responseFormat='html';


    var beforeSendCallback=function(){};
    var successCallback;
    
    var errorCallback=function(){};
    
    //executeFormatedAjax(divResult, urlDir, data, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback)
    
    successCallback=function(datahtml, resp2, datos) {

                            
                                var response = jQuery.parseJSON(datos.responseText);
                                

                                 if(response.status =='success'){ //SI LOS DATOS SON CORRECTOS AUTOCOMPLETA LOS NOMBRES Y APELLIDOS CORRESPONDIENTES

                                    $('#CongresoPedagogicoDocente_nombres').val(response.primer_nombre+' '+response.segundo_nombre);
                                    $('#CongresoPedagogicoDocente_apellidos').val(response.primer_apellido+' '+response.segundo_apellido);

                                   

                                    $('#CongresoPedagogicoDocente_tdocumento_identidad').val(response.tipo_documento);
                                    $('#CongresoPedagogicoDocente_documento_identidad').val(response.documento_identidad);

                                    $('#alerta').addClass('hide');
                                    $('#alerta2').addClass('hide');
                        
                                }
                                 if(response.status =='error'){ // SI LA CEDULA NO EXISTE LIMPIA LOS CAMPOS Y MUESTRA UN MENSAJE DE ERROR
                                    $('#alerta').removeClass('hide');
                                    $('#CongresoPedagogicoDocente_documento_identidad').val('');
                                    $('#CongresoPedagogicoDocente_nombres').val('');
                                    $('#CongresoPedagogicoDocente_apellidos').val('');

                                    $('#alerta2').addClass('hide');


                                }
                                if(response.status =='mensaje'){ // SI LA CEDULA YA ESTA INSCRITA EN OTRO REGISTRO LIMPIA LOS CAMPOS Y MUESTRA EL ERROR CORRESPONDIENTE
                                    $('#alerta2').removeClass('hide');
                                    $('#CongresoPedagogicoDocente_documento_identidad').val('');
                                    $('#CongresoPedagogicoDocente_nombres').val('');
                                    $('#CongresoPedagogicoDocente_apellidos').val('');


                                    $('#alerta').addClass('hide');


                                }


                                
                                if(response.status =='vacio'){ // SI LA CEDULA YA ESTA INSCRITA EN OTRO REGISTRO LIMPIA LOS CAMPOS Y MUESTRA EL ERROR CORRESPONDIENTE
                                    $('#alerta3').removeClass('hide');
                                    $('#CongresoPedagogicoDocente_documento_identidad').val('');
                                    $('#CongresoPedagogicoDocente_nombres').val('');
                                    $('#CongresoPedagogicoDocente_apellidos').val('');


                                    $('#alerta').addClass('hide');
                                    $('#alerta2').addClass('hide');


                                }

                    
                                
                             
                };

    executeFormatedAjax(divResult, urlDir, data, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
    //$('#proyecto-consejo-form').reset(); 
}
</script>
