<?php
/**
 * Created by PhpStorm.
 * User: ignacio
 * Date: 11/09/14
 * Time: 9:09
 */

/* Editado por @usuario Pedro Chacon
    Fecha: 26/03/2015
    GridView cong-ped-grid, botones (mostrar/ocultar formularios de registro de Congreso Pedagogico y docentes*/
?>

<div class="widget-header">
    <h5>Lista de Congresos</h5>
</div>
<div class="row">
<?php
//GRID VIEW CON LA LISTA DE TODOS LOS CONGRESOS PEDAGOGICOS REGISTRADOS EN EL PLANTEL (ACTIVOS Y NO ACTIVOS)
$this->widget('zii.widgets.grid.CGridView',array(
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
    'id'=>'cong-ped-grid',
    'dataProvider'=>$congreso->searchCongresoPlantel($plantel_id),
    'ajaxUrl' => '/planteles/modificar/actualizarGridCongresos/id/'.base64_encode($plantel_id),
    'columns'=>array(
        array(
            'name'=>'campo_conocimiento_id',
            'value'=>'$data->campoConocimiento->nombre',
            'type'=>'raw',
            'filter'=>false
        ),
        array(
            'name'=>'linea_conocimiento',
            'type'=>'raw',
            'filter'=>false
        ),
        array(
            'name'=>'fecha_inicio',
            'type'=>'raw',
            'filter'=>false
        ),
        array(
            'name'=>'fecha_fin',
            'type'=>'raw',
            'filter'=>false
        ),
        array(
            'type' => 'raw',
            'header' => '<center>Acciones</center>',
            'value' => array($this, 'columnaAccionesCp'),
            'htmlOptions' => array('nowrap' => 'nowrap'),
        ),
    ),
    'pager' => array(
        'header' => '',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
    ),
)); ?>

</div>
<div class="row">
    <div class="col-md-6 wizard-actions pull-right"> <!-- BOTON PARA MOSTRAR EL FORMULARIO DE REGISTRO -->
        <button type="button" data-last="Finish" id="btnMostrarFormulario" class="btn btn-primary">
            Registrar Congreso Pedagogico
            <i class="icon-plus icon-on-right"></i>
        </button>
    </div>

</div>

<div id="registro" class="hide">
    <div class="widget-box" id="congreso_pedagogico_box">
        <div class="widget-header">
            <h5>Congreso Pedagógico</h5>
            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-body-inner" style="display: block;">
                <div class="widget-main">
                    <div class="row">
                        <div id="resultadoCongreso" class="hide"></div>
                        <div class="widget-box" id="personal_docente_box" >
                            <div class="widget-header">
                                <h5>Personal Docente</h5>

                                <div class="widget-toolbar">
                                    <a data-action="collapse" href="#">
                                        <i class="icon-chevron-up"></i>
                                    </a>
                                </div>

                            </div>
                            <div class="widget-body">
                                <div class="widget-body-inner" style="display: block;">
                                    <div class="widget-main">
                                        <div class="row">
                                            <div class="form">
                                                <form id="congreso-form">
                                                    <?php $this->widget('ext.yii-playground.widgets.tabularinput.XTabularInput',array(
                                                        'models'=>$congreso_docente,
                                                        /*'containerTagName'=>'div',
                                                        'headerTagName'=>'div',
                                                        'header'=>'
                                                        <div class="row">
                                                            <div class="col-md-3">'.CHtml::activeLabelEX($congreso_docente,'tdocumento_identidad').'</div>
                                                            <div class="col-md-3">'.CHtml::activeLabelEX($congreso_docente,'documento_identidad').'</div>
                                                            <div class="col-md-3">'.CHtml::activeLabelEX($congreso_docente,'nombres').'</div>
                                                            <div class="col-md-3"></div>
                                                        </div>
                                                    ',
                                                        'inputContainerTagName'=>'div ',
                                                        'inputTagName'=>'div class="row"',*/
                                                        //'inputTagName'=>'div class="row"',
                                                        'inputView'=>'tabular/_tabularInput',
                                                        'inputUrl'=>$this->createUrl('modificar/addTabularInputs'),
                                                        'removeTemplate'=>'<div class="col-md-2">{link}</div></div>',
                                                        'removeLabel'=>'Eliminar Docente',
                                                        'removeHtmlOptions'=>array('class'=>'btn btn-sm btn-danger', 'style'=>'margin-top:20.5px'),
                                                        'addLabel'=>'Nuevo Docente',
                                                        'addTemplate'=>'<div class="row">{link}</div>',
                                                        'addHtmlOptions'=>array('class'=>'btn btn-sm btn-success full-width'),
                                                        'successScript'=>"
                                                        $('.data-doc-ident').unbind('keyup');
                                                        $('.data-doc-ident').bind('keyup', function() {
                                                            keyAlphaNum(this,false,false);
                                                            clearField(this);
                                                            makeUpper(this);
                                                        });
                                                        $('.data-doc-ident').unbind('blur');
                                                        $('.data-doc-ident').bind(' blur', function() {
                                                            var documento_identidad = $(this).val();
                                                            var index_input = base64_decode($(this).attr('data-id'));
                                                            var id_nombres;
                                                            var id_apellidos;
                                                            var id_documento_identidad;
                                                            var id_tdocumento_identidad;
                                                            var tdocumento_identidad;
                                                            var title;
                                                            var mensaje;
                                                            var divResult='';
                                                            var urlDir = '/planteles/modificar/obtenerDatosPersona';
                                                            var datos;
                                                            var loadingEfect=false;
                                                            var showResult=false;
                                                            var method='POST';
                                                            var responseFormat='json';
                                                            var beforeSendCallback=function(){};
                                                            var successCallback;
                                                            var errorCallback=function(){};

                                                            if(documento_identidad !=null && documento_identidad != '' ){
                                                                id_nombres='CongresoPedagogicoDocente_'+index_input+'_nombres';
                                                                id_apellidos='CongresoPedagogicoDocente_'+index_input+'_apellidos';
                                                                id_tdocumento_identidad='CongresoPedagogicoDocente_'+index_input+'_tdocumento_identidad';
                                                                id_documento_identidad='CongresoPedagogicoDocente_'+index_input+'_documento_identidad';
                                                                tdocumento_identidad = $('#'+id_tdocumento_identidad).val();
                                                                if(tdocumento_identidad !=null && tdocumento_identidad != '' ){
                                                                    datos=$('#congreso-form').serialize()+'&documento_identidad='+documento_identidad+'&tdocumento_identidad='+tdocumento_identidad+'&index='+base64_encode(index_input);
                                                                    successCallback=function(response){
                                                                        if(response.statusCode =='SUCCESS'){
                                                                            $('#'+id_apellidos).val(response.apellidos);
                                                                            $('#'+id_nombres).val(response.nombres);
                                                                        }
                                                                        if(response.statusCode =='ERROR'){
                                                                            $('#'+id_apellidos).val('');
                                                                            $('#'+id_nombres).val('');
                                                                            $('#'+id_documento_identidad).val('');
                                                                            dialogo_error(response.mensaje,response.title);
                                                                        }
                                                                    };
                                                                    executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
                                                                }
                                                                else {
                                                                    title = 'Notificación de Error';
                                                                    mensaje='Estimado usuario debe seleccionar un Tipo de Documento de Identidad Valido.';
                                                                    dialogo_error(mensaje,title);
                                                                }

                                                            }

                                                        });"



                                                    ));?>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row"><div class="space-6"></div></div>

                        <div class="widget-box" id="congreso_datos_box" >
                            <div class="widget-header">
                                <h5>Datos Generales</h5>
                                <div class="widget-toolbar">
                                    <a data-action="collapse" href="#">
                                        <i class="icon-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-body-inner" style="display: block;">
                                    <div class="widget-main">
                                        <div class="row">
                                            <div class="form">
                                                <?php
                                                $form = $this->beginWidget('CActiveForm', array(
                                                    'id' => 'congreso-datos-form',
                                                    'enableAjaxValidation' => false,
                                                ));
                                                ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($congreso, 'campo_conocimiento_id', array("class" => "col-md-12")); ?>
                                                            <?php echo $form->dropDownList($congreso, 'campo_conocimiento_id', CHtml::listData(CCampoConocimiento::getData('estatus','A'), 'id', 'nombre'), array('empty' => '- - -', 'class' => 'span-7', 'required' => 'required')); ?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($congreso, 'linea_conocimiento', array("class" => "col-md-12")); ?>
                                                            <?php echo $form->textField($congreso, 'linea_conocimiento', array('class' => 'col-md-12')); ?>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($congreso, 'fecha_inicio', array("class" => "col-md-12")); ?>
                                                            <?php echo $form->textField($congreso, 'fecha_inicio', array('class' => 'span-7','readOnly'=>'readOnly')); ?>

                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($congreso, 'fecha_fin', array("class" => "col-md-12")); ?>
                                                            <?php echo $form->textField($congreso, 'fecha_fin', array('class' => 'span-7','readOnly'=>'readOnly')); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <?php echo $form->labelEx($congreso, 'observaciones', array("class" => "col-md-12")); ?>
                                                        <?php echo $form->textField($congreso, 'observaciones', array('size' => 6, 'maxlength' => 100, 'class' => 'span-7')); ?>
                                                    </div>
                                                </div>
                                                <?php $this->endWidget(); ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="row"><div class="space-6"></div></div>
    <div class="row">
        <div class="col-md-6 wizard-actions pull-right"> <!-- BOTON PARA OCULTAR EL FORMULARIO DE REGISTRO -->
            <button type="button" data-last="Finish" id="btnOcultarFormulario" class="btn btn-danger">
                Ocultar
                <i class="icon-remove icon-on-right"></i>
            </button>
            <button type="submit" data-last="Finish" id="btnGuardarCongreso" class="btn btn-primary btn-next">
                Guardar
                <i class="icon-save icon-on-right"></i>
            </button>
        </div>

    </div>
</div>
<div id="dialog_error" class="hide">
</div>


<div id="dialogPantallaConsultar" class="hide"></div>
<div id="dialogPantallaEditar" class="hide"></div>

<div id="dialogPantallaEliminar" class="hide">
    <div class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Está seguro de eliminar este Congreso?
        </p>
    </div>
</div>
<div id="dialogPantallaActivar" class="hide">
    <div class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Está seguro de activar este Congreso?
        </p>
    </div>
</div>


<script type="text/javascript">

    $( "#btnMostrarFormulario" ).click(function() {
        $("#registro").removeClass('hide');
    });

    $( "#btnOcultarFormulario" ).click(function() {
        $("#registro").addClass('hide');
    });
</script>





