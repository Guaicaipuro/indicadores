<?php
/**
 * User: pedro
 * Date: 06/04/15
 * Vista con la lista de todos los docentes registrados en elcongtreso pedagogico seleccionado y botones de acciones 
 */
?>
<?php
    $plantel_id = ($model->plantel_id);
$this->breadcrumbs = array(
    'Planteles' => array('consultar/'),
    'Modificar Datos' => array('modificar/index?id='.base64_encode($plantel_id)),
    'Congreso Pedagógico - Docentes'
);
?>
<div id="respEdicion" class="hide">
</div>

<div class="row">
    <div class="col-md-6 wizard-actions pull-right"> <!-- BOTON QUE MUESTRA EL FORMULARIO PARA AGREGAR UN NUEVO DOCENTE -->
        <button type="button" data-last="Finish" id="btnMostrarFormulario" class="btn btn-primary">
            Agregar Docente al Congreso Pedagógico
            <i class="icon-plus icon-on-right"></i>
        </button>
    </div>

</div>

<div class="widget-box" id="docentes_box" >
<div class="widget-header">
    <h5>Lista de Docentes Inscritos</h5>
</div>
<div class="widget-body">
    <div class="widget-body-inner" style="display: block;">
<div class="row">
<?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'prueba',
                'enableAjaxValidation' => false,                                                   
            ));
            echo $form->textField($model, 'id', array('class' => 'hide'));
            $this->endWidget();
                                                ?>


<?php
//var_dump(CongresoPedagogicoDocente::model()->searchDocentesInscritos($model->id)); die();
$this->widget('zii.widgets.grid.CGridView',array(
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
    'id'=>'docente-grid',
    'dataProvider'=>CongresoPedagogicoDocente::model()->searchDocentesInscritos($model->id),
   // 'ajaxUrl' => '/planteles/modificar/actualizarGridCongresos/id/'.base64_encode($id),
    'columns'=>array(
        array(
            'name'=>'documento_identidad',
            'type'=>'raw',
            'filter'=>false
        ),
        array(
            'name'=>'nombres',
            'type'=>'raw',
            'filter'=>false
        ),
        array(
            'name'=>'apellidos',
            'type'=>'raw',
            'filter'=>false
        ),

        array(
            'type' => 'raw',
            'header' => '<center>Acciones</center>',
            'value' => array($this, 'columnaAccionesD'),
            'htmlOptions' => array('nowrap' => 'nowrap'),
        ),
    ),
    'pager' => array(
        'header' => '',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
    ),
)); ?>

</div>
</div>
</div>
</div>




<div id="dialogPantallaEliminarDocente" class="hide">
    <div class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Está seguro de eliminar este Docente?
        </p>
    </div>
</div>
<div id="dialogPantallaActivar" class="hide">
    <div class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Está seguro de activar este Docente?
        </p>
    </div>
</div>
<div id="dialogPantallaEditar" class="hide"></div>

<div id="dialogPantallaAgregar" class="hide"></div>

<div id="dialog_error" class="hide">
</div>

<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/plantel/congreso_pedagogico.js', CClientScript::POS_END); ?>



<script type="text/javascript">

    $( "#btnMostrarFormulario" ).click(function() { //FUNCION QUE LLAMA A LA FUNCION QUE RENDERIZA LA VENTANA EMERGENTE PARA AGREGAR NUEVO DOCENTE
        congreso = $("#CongresoPedagogico_id").val();
        agregarDocente(congreso);
    });

    function agregarDocente(id) { // FUNCION QUE RENDERIZA LA VENTANA EMERGENTE Y ENVIA LOS DATOS INGRESADOS AL CONTROLADOR PARA VALIDAR Y GUARDAR LOS DATOS

    direccion = '/planteles/modificar/agregarDocente';
    title = 'Congreso Pedagogico - Agregar Docente';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantallaAgregar").html(result);
            var dialog = $("#dialogPantallaAgregar").removeClass('hide').dialog({
                modal: true,
                width: '1000px',
                //height: '800px',
                draggable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>" + title + "</h4></div>",
                title_html: true,
                buttons: [
                        {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGridDocente();
                            $(this).dialog("close");}
                        },
                        {

                        html: "<i class='icon-save-left bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        "type": "submit",
                        click: function() {
                            var divResult = "respEdicion";
                            var urlDir = "/planteles/modificar/guardarDocente";
                            var datos = $('#docente-agregar-form').serialize();
                            var loadingEfect = true;
                            var showResult = true;
                            var method = "POST";
                            var responseFormat = "html";
                            var successCallback;
                            $('#'+divResult).html('');
                            $('#'+divResult).addClass('hide');

                            successCallback = function(){
                                $('#'+divResult).removeClass('hide');
                                refrescarGridDocente();
                            }
                            executeAjaxRequest(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback);


                            $(this).dialog("close");
                        }

                        }

                ]
            });

        }
    });
    Loading.hide();
}
function refrescarGridDocente() { //FUNCION QUE RECARGA EL GRIDVIEW DOCENTES-GRID DESPUES DE CADA ACCION
    var data = {
        plantel_id: $("#plantel_id").val()
    };
    $('#docente-grid').yiiGridView('update', {
        data:data
    });
}

</script>