<div class="tab-pane active" id="modalidad">


    <div class="widget-box">
        <div class="widget-header">
            <h5>Modalidad</h5>

            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div style="display:block;" class="widget-body-inner">
                <div class="widget-main">

                    <div class="row space-6"></div>
                    <div>
                        <div id="resultadoOperacion">
                            <div id="msj_exitoso" class="successDialogBox hide">
                            <p>

                            </p>
                            </div>
                            <div id="div-success" class="">
                                <p>
                                </p>
                            </div>
                            <div class="infoDialogBox">
                                <p>
                                    En esta área podrá registrar, consultar y/o actualizar sus Modalidades.
                                </p>
                            </div>
                        </div>
                        <?php if(in_array(Yii::app()->user->group, array(UserGroups::ADMIN_0,  UserGroups::JEFE_DRCEE,  UserGroups::ADMIN_REG_CONTROL) )){ ?>
                        <div class="pull-right" style="padding-left:10px;">
                            <a type="button" id='btnRegistrarNuevaModalidad' data-last="Finish"
                               class="btn btn-success btn-next btn-sm">
                                <i class="fa fa-plus icon-on-right"></i>
                                Registrar Nuevo Modalidad</a>
                        </div>
                        <?php } ?>
                        <div class="row space-20"></div>
                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'plantel-modalidad-grid',
                        'dataProvider' => $modelPlantelModalidad->search(),
                        'filter' => $modelPlantelModalidad,
                        'ajaxUrl' => '/planteles/modificar/actualizarGridPlantelModalidad/id/'.$codPlantel,
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'summaryText' => 'Mostrando {start}-{end} de {count}',
                                 'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination'),
                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                            ),
                        'afterAjaxUpdate' => "function(){
                            
                                            //FUNCION QUE PERMITE VISUALIZAR LOS DATOS DE MODALIDAD
                                            $('.verDatosModalidad').unbind('click');
                                            $('.verDatosModalidad').on('click', function(e) {
                                                e.preventDefault();
                                                var registro_id;
                                                registro_id = $(this).attr('data');
                                                Loading.show();
                                                consultarModalidad(registro_id);
                                            });
                                            
                                            //FUNCION QUE PERMITE EDITAR LOS DATOS DEL AREA DE ATENCION
                                            $('.EditarDatosModalidad').unbind('click');
                                            $('.EditarDatosModalidad').on('click', function(e) {
                                                    e.preventDefault();
                                                    var registro_id;
                                                    registro_id = $(this).attr('data');
                                                    modificarModalidad(registro_id);
                                                    });
                                            
                                            //FUNCION QUE PERMITE INACTIVAR EL REGISTRO DE MODALIDAD.
                                            $('.InactivarDatosModalidad').unbind('click');
                                            $('.InactivarDatosModalidad').on('click', function(e) {
                                                e.preventDefault();
                                                var registro_id;
                                                registro_id = $(this).attr('data');
                                                inactivarModalidad(registro_id);
                                            });
                                            //FUNCION QUE PERMITE ACTIVAR EL REGISTRO DE MODALIDAD DE RESPECTIVO DE PLANTEL.
                                            $('.activarDatosModalidad').unbind('click');
                                            $('.activarDatosModalidad').on('click', function(e) {
                                                e.preventDefault();
                                                var registro_id;
                                                registro_id = $(this).attr('data');
                                                activarModalidad(registro_id);
                                            });
                                                    
                                                }",
                        'columns' => array(
                            array(
                                'header'=>'<center>Modalidad</center>',
                                'name'=>'modalidad_id',
                                'value'=>'$data->modalidad->nombre',
                                'filter'=>CHtml::listData($modalidad,'id','nombre'),
                            ),
                            array(
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            'filter' => array(
                                'A' => 'Activo',
                                'I' => 'Inactivo'
                            ),
                            'value' => array($this, 'estatus'),
                            ),
                            array('type' => 'raw',
                                        'header' => '<center>Acciones</center>',
                                        'value' => array($this, 'columnaAccionesModalidad'),
                                 ),
                        ),
                    ));
                       ?> 
                        <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input id="codPlantel" type="hidden" value="<?php echo $codPlantel; ?>">
    <div id="formModalidad" class="hide">
    </div>
    <div id="dialogPantalla" class="hide">
    </div> 
    <div id="dialog_inactivacion" class="hide">
    <div id="pregunta_inactivar" class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas Seguro(a) que Desea Inactivar este Registro de Modalidad?
        </p>
    </div>
    <div id="msj_error_inactivar" class="errorDialogBox hide">
        <p>
        </p>
    </div>
    </div>
    <div id="dialog_activacion" class="hide">
    <div id="pregunta_activar" class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas Seguro(a) que Desea Activar este Registro de Modalidad?
        </p>
    </div>
    <div id="msj_error_activar" class="errorDialogBox hide">
        <p>
        </p>
    </div>
    </div>
    <div id="dialog_error" class="hide"></div>
</div>



<script>
    $(document).on('ready',function(){
        
       //REGISTRO DEL AREA DE ATENCION ASOCIADO AL ESTUDIANTE EN CUESTION. 
       $('#btnRegistrarNuevaModalidad').on('click',function(e){
           data= { plantel_id: $('#codPlantel').val() };
        $.ajax({
            url:'/planteles/modificar/mostrarFormModalidad',
            data: data,
            dataType: 'html',
            type: 'get',
            success: function(resp, resp2, resp3) { 
            try {
                                            var json = jQuery.parseJSON(resp3.responseText);
                                            if (json.status == "error") {
                                                var mensaje = '<b>'+json.mensaje+'</b>';
                                                var title = 'Áreas de Atención';
                                                verDialogo(mensaje, title);
                                            }
                                        }
                                        catch (e) {
                                            
                                            
                                            //VENTANA MODAL QUE MUESTRA EL FORMULARIO PARA REGISTRAR LA RESPECTIVA AREA DE ATENCION    
                                            var dialogRegistrar = $("#formModalidad").removeClass('hide').dialog({
                                                modal: true,
                                                width: '650px',
                                                draggable: false,
                                                resizable: false,
                                                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Agregar Nueva Modalidad</h4></div>",
                                                title_html: true,
                                                buttons: [
                                                    {
                                                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                                                        "class": "btn btn-danger btn-xs",
                                                        click: function() {
                                                            $('#PlantelModalidad_modalidad_id').prop('selectedIndex',0);
                                                            $('#div-result').addClass('hide');
                                                            $('#div-success').addClass('hide');
                                                            $(this).dialog("close");
                                                        }
                                                    },
                                                    {
                                                        html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                                                        "class": "btn btn-primary btn-xs",
                                                        click: function() {
                                                                            //FUNCION QUE INVOCA EL REGISTRO DEL ESTUDIANTE CON SU AREA DE ATENCION.
                                                                            registrarPlantelModalidad();
                                                                          }
                                                    }
                                                ]
                                            });
                                            $("#msj_exitoso").addClass('hide');
                                            $("#msj_exitoso p").html('');
                                            $("#msj_error").addClass('hide');
                                            $("#msj_error p").html('');
                                            $("#formModalidad").html(resp);
                                            $("html, body").animate({scrollTop: 0}, "fast");

                                                document.onkeypress=function(tecla)
                                                {
                                                        if(tecla.keyCode==13)
                                                          {
                                                            $("#plantel-modalidad-form").submit(); 
                                                            return false;
                                                          }
                                                }
                                                $("#plantel-modalidad-form").submit(function( e ) {
						e.preventDefault();
						//FUNCION QUE INVOCA EL REGISTRO DEL ESTUDIANTE CON SU AREA DE ATENCION.
                                                registrarPlantelModalidad();
						});
                                        }
            
            }
            
            });
        });
        //Función que Permite Desplegar una Ventana para Ver el Registro del área de Atención.
        $(".verDatosModalidad").unbind('click');
        $(".verDatosModalidad").on('click', function(e) {
            e.preventDefault();
            var registro_id;
            registro_id = $(this).attr("data");
            Loading.show();
            consultarModalidad(registro_id);
        });
        //Funcion Que Permite Editar los Registros  del área de Atención.
        $(".EditarDatosModalidad").unbind('click');
        $(".EditarDatosModalidad").on('click', function(e) {
                                                    e.preventDefault();
                                                    var registro_id;
                                                    registro_id = $(this).attr("data");
                                                    modificarModalidad(registro_id);
                                                    });
        //FUNCION QUE PERMITE INACTIVAR EL REGISTRO DE AREA DE ATENCION DE RESPECTIVO ESTUDIANTE.
        $(".InactivarDatosModalidad").unbind('click');
        $(".InactivarDatosModalidad").on('click', function(e) {
            e.preventDefault();
            var registro_id;
            registro_id = $(this).attr("data");
            inactivarModalidad(registro_id);
        });
        //FUNCION QUE PERMITE ACTIVAR EL REGISTRO DE AREA DE ATENCION DE RESPECTIVO ESTUDIANTE.
        $(".activarDatosModalidad").unbind('click');
        $(".activarDatosModalidad").on('click', function(e) {
            e.preventDefault();
            var registro_id;
            registro_id = $(this).attr("data");
            activarModalidad(registro_id);
        });
    });
    
    function consultarModalidad(registro_id) {
           // alert(registro_id);
            var data = {
                id: registro_id
            };

            $.ajax({
                url: "/planteles/modificar/VerDatosPlantelModalidad",
                data: data,
                dataType: 'html',
                type: 'get',
                success: function(resp)
                {

                    
                    try {
                        var json = jQuery.parseJSON(resp3.responseText);

                        if (json.statusCode == "error") {
                          //  alert('error');
                            $("#msj_error_consulta").removeClass('hide');
                            $("#msj_error_consulta p").html(json.mensaje);
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                    catch (e) {
                       // alert('catch');
                        $("#msj_error_consulta").addClass('hide');
                        $("#msj_error_consulta p").html('');
                        $("#dialogPantalla").html(resp);
                        //$("html, body").animate({scrollTop: 0}, "fast");
                        
                        var dialog_consultar = $("#dialogPantalla").removeClass('hide').dialog({
                        modal: true,
                        width: '800px',
                        draggable: false,
                        resizable: false,
                        //position: 'top',
                        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='icon-list bigger-110'></i>&nbsp; Detalles del Registro de Modalidad</h4></div>",
                        title_html: true,
                        buttons: [
                            {
                                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                                "class": "btn btn-danger btn-xs",
                                click: function() {
                                    dialog_consultar.dialog("close");
                                }
                            }
                        ]
                    });
                    }
                }
            });
            Loading.hide();
        }
                function refrescarGridModalidad() {
                                            $('#plantel-modalidad-grid').yiiGridView('update', {
                                                data: $(this).serialize()
                                            });
                                     }
                                     
                function activarModalidad(registro_id) {

            $("#pregunta_activar").removeClass('hide');
            $("#msj_error_activar").addClass('hide');
            $("#msj_error_activar p").html('');

            var dialogActivar = $("#dialog_activacion").removeClass('hide').dialog({
                modal: true,
                width: '600px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Registro de Modalidad</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                        "class": "btn btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "<i class='fa icon-ok bigger-110'></i>&nbsp; Activar",
                        "class": "btn btn-success btn-xs",
                        click: function() {
                            var data = {
                                id: registro_id
                            }

                            $.ajax({
                                url: "/planteles/modificar/activarModalidad",
                                data: data,
                                dataType: 'html',
                                type: 'get',
                                success: function(resp, resp2, resp3) {

                                    try {
                                        var json = jQuery.parseJSON(resp3.responseText);

                                        if (json.statusCode == "success") {

                                            refrescarGridModalidad();
                                            dialogActivar.dialog('close');
                                            $("#msj_error_activar").addClass('hide');
                                            $("#div-success,#div-error").hide();
                                            $("#msj_error_activar p").html('');
                                            $("#msj_exitoso").removeClass('hide');
                                            $("#msj_exitoso p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }

                                        if (json.statusCode == "error") {

                                            $("#pregunta_activar").addClass('hide');
                                            $("#div-success,#div-error").hide();
                                            $("#msj_exitoso").addClass('hide');
                                            $("#msj_exitoso p").html('');
                                            $("#msj_error_activar").removeClass('hide');
                                            $("#msj_error_activar p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }
                                    }
                                    catch (e) {

                                    }
                                }
                            })
                        }
                    }
                ]
            });
            //$("#dialog_activacion").show();
        }
        
        function inactivarModalidad(registro_id) {
            $("#pregunta_inactivar").removeClass('hide');
            $("#msj_error_inactivar").addClass('hide');
            $("#msj_error_inactivar p").html('');

            var dialogInactivar = $("#dialog_inactivacion").removeClass('hide').dialog({
                modal: true,
                width: '600px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Inactivar Registro de Modalidad</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                        "class": "btn btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            var data = {
                                id: registro_id
                            };
                            $.ajax({
                                url: "/planteles/modificar/inactivarModalidad",
                                data: data,
                                dataType: 'html',
                                type: 'post',
                                success: function(resp, resp2, resp3) {

                                    try {
                                        var json = jQuery.parseJSON(resp3.responseText);

                                        if (json.statusCode == "success") {

                                            refrescarGridModalidad();
                                            dialogInactivar.dialog('close');
                                            $("#msj_error_inactivar").addClass('hide');
                                            $("#div-success,#div-error").hide();
                                            $("#msj_error_inactivar p").html('');
                                            $("#msj_exitoso").removeClass('hide');
                                            $("#msj_exitoso p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }

                                        if (json.statusCode == "error") {

                                            $("#pregunta_inactivar").addClass('hide');
                                            $("#div-success,#div-error").hide();
                                            $("#msj_exitoso").addClass('hide');
                                            $("#msj_exitoso p").html('');
                                            $("#msj_error_inactivar").removeClass('hide');
                                            $("#msj_error_inactivar p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }
                                    }
                                    catch (e) {

                                    }
                                }
                            })
                        }
                    }
                ]
            });
            //$("#dialog_inactivacion").show();
        }
        
         function registrarPlantelModalidad()
        {
            var divResult= '';
            var urlDir   = '/planteles/modificar/guardarPlantelModalidad';
            var datos    = $("#plantel-modalidad-form").serialize();
            var loadingEfect= true;
            var showResult= true;
            var method="POST";
            var responseFormat ="html";
            var beforeSendCallback= function(){};
            var  successCallback=function(datahtml, resp2, resp3) {
            $('*').scrollTop(100);
                            try {
                                var response = jQuery.parseJSON(resp3.responseText);
                                    if(response.status =='success'){
                                        $("#formModalidad").addClass('hide').dialog('close');
                                        divResult='div-success';
                                        displayDialogBox(divResult, response.status, response.mensaje,true,true);
                                        $('#PlantelModalidad_modalidad_id').prop('selectedIndex',0);
                                        refrescarGridModalidad();
                                                                   }
                                } catch (e) {
                                    divResult='div-error';
                                    displayHtmlInDivId(divResult, datahtml,true);
                                }
                };
            var errorCallback  = function(){};
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);                                 
            
            }
            
            function modificarPlantelModalidad()
        {
            var divResult= 'msj_exitoso';
            var urlDir   = '/planteles/modificar/modificarPlantelModalidad';
            var datos    = $("#modificar-plantel-modalidad-form").serialize();
            var loadingEfect= true;
            var showResult= true;
            var method="POST";
            var responseFormat ="html";
            var beforeSendCallback= function(){};
            var  successCallback=function(datahtml, resp2, resp3) {
            $('*').scrollTop(100);
                            try {
                                var response = jQuery.parseJSON(resp3.responseText);
                                    if(response.status =='success'){
                                        $("#formModalidad").addClass('hide').dialog('close');
                                        divResult='div-success';
                                        displayDialogBox(divResult, response.status, response.mensaje,true,true);
                                        $('#PlantelModalidad_modalidad_id').prop('selectedIndex',0);
                                        refrescarGridModalidad();
                                                                   }
                                } catch (e) {
                                    
                                                                                divResult='div-error';
                                                                                displayHtmlInDivId(divResult, datahtml,true);
                                }
                };
            var errorCallback  = function(){};
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);                                 
            function refrescarGridModalidad() {
                                            $('#plantel-modalidad-grid').yiiGridView('update', {
                                                data: $(this).serialize()
                                            });
                                     }
            }
            function modificarModalidad(registro_id) 
        {
                                var data = {
                                            id: registro_id,
                                            plantel_id: $('#codPlantel').val(),
                                            };
                                $.ajax({
                                    url: "/planteles/modificar/mostrarDatosPlantelModalidad",
                                    data: data,
                                    dataType: 'html',
                                    type: 'get',
                                    success: function(resp, resp2, resp3) {

                                        


                                        try {
                                            var json = jQuery.parseJSON(resp3.responseText);

                                            if (json.status == "error") {
                                                var mensaje = '<b>'+json.mensaje+'</b>';
                                                var title = 'Modalidad';
                                                verDialogo(mensaje, title);
                                            }
                                        }
                                        catch (e) {
                                            
                                            
                                            var dialogRegistrar = $("#formModalidad").removeClass('hide').dialog({
                                            modal: true,
                                            width: '650px',
                                            draggable: false,
                                            resizable: false,
                                            title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Modificar Modalidad</h4></div>",
                                            title_html: true,
                                            buttons: [
                                                {
                                                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                                                    "class": "btn btn-danger btn-xs",
                                                    click: function() {
                                                        $(this).dialog("close");
                                                    }
                                                },
                                                {
                                                    html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                                                    "class": "btn btn-primary btn-xs",
                                                    click: function() {
                                                                       modificarPlantelModalidad(); 
                                                                      }
                                                }
                                            ],
                                        });

                                            $("#msj_exitoso").addClass('hide');
                                            $("#msj_exitoso p").html('');
                                            $("#msj_error").addClass('hide');
                                            $("#msj_error p").html('');
                                            $("#formModalidad").html(resp);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                            
                                            document.onkeypress=function(tecla)
                                                {
                                                        if(tecla.keyCode==13)
                                                          {
                                                            $("#modificar-plantel-modalidad-form").submit(); 
                                                            return false;
                                                          }
                                                }
                                                $("#modificar-plantel-modalidad-form").submit(function( e ) {
						e.preventDefault();
						//FUNCION QUE INVOCA EL REGISTRO DEL ESTUDIANTE CON SU AREA DE ATENCION.
                                                modificarPlantelModalidad(); 
						});
                                        }

                                    }
                                })
        }
        
        function verDialogo(message, title, style, clickCallback, reload, buttonsExtra) {
        if (!style)
            style = 'info';

        var buttonsDialog = [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if (clickCallback) {
                        clickCallback();
                    }
                    if (reload) {
                        window.location.reload();
                    }
                }
            }
        ];

        if (buttonsExtra)
            buttonsDialog.push(buttonsExtra);

        displayDialogBox('dialog_error', style, message);

        var dialog = $("#dialog_error").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: buttonsDialog
        });
    }
</script>
