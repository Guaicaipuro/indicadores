<?php
/**
 * Created by PhpStorm.
 * User: ignacio
 * Date: 24/09/14
 * Time: 11:44
 */
?>
<div class="col-xs-12">
    <div class="row-fluid">
        <div class="tabbable">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'dependencia-nominal-form',
                            'htmlOptions' => array('data-form-type'=>'registro',), // for inset effect
                            // Please note: When you enable ajax validation, make sure the corresponding
                            // controller action is handling ajax validation correctly.
                            // There is a call to performAjaxValidation() commented in generated controller code.
                            // See class documentation of CActiveForm for details on this.
                            'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-result">
                            <?php
                            if($modelDependencia->hasErrors()){
                                $this->renderPartial('//errorSumMsg', array('model' => $modelDependencia));
                            }
                            else{
                                if (Yii::app()->user->hasFlash('exito')){ ?>
                                    <div class="successDialogBox">
                                        <p><?php echo Yii::app()->user->getFlash('exito'); ?></p>
                                    </div>
                                <?php
                                }
                                else {
                                    ?>
                            <div class="infoDialogBox" id="infoDependencia"><p class="note">Debe verificar la Dependencia Nominal ingresando el <b><i>Código</i></b> y presionando <b><i>'Buscar'</i></b></p></div>
                                <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="row">
                            <!--<div class="col-md-12">
                                <div class="col-md-12">
                                    <?php //echo $form->labelEx($modelDependencia,'dependencia_nominal'); ?>
                                    <?php //echo $form->textField($modelDependencia,'dependencia_nominal',array('size'=>25, 'maxlength'=>25, 'class' => 'span-12', "required"=>"required",)); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row"><div class="space-6"></div></div>
                        <?php // $this->endWidget(); ?>
                    </div><!-- form -->
                <div class="form">
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'busquedaDependencia-form',
                            'htmlOptions' => array('data-form-type'=>'registro',), // for inset effect
                            // Please note: When you enable ajax validation, make sure the corresponding
                            // controller action is handling ajax validation correctly.
                            // There is a call to performAjaxValidation() commented in generated controller code.
                            // See class documentation of CActiveForm for details on this.
                            'enableAjaxValidation'=>true,
                        )); ?>
                            <div id="1eraFila" class="col-md-12">
                              <div class="col-md-12" >
                            <?php echo $form->labelEx($modelDependencia,'dependencia_nominal', array("class" => "col-md-12"));?>      
                            <?php echo $form->textField($modelDependencia,'dependencia_nominal', array('class' => 'span-8', 'maxlength' => 8, 'id' => "dependencia_nominal", 'required' => 'required', 'style' => 'text-transform:uppercase;')); ?>
                                            <?php echo '<input type="hidden" id="plantel_id" value='.$plantel_id.' name="plantel_id"/>'; ?>
                            <button  id = "btnBuscarDependencia" onclick="obtenerCodigoDependencia()" class="btn btn-info btn-xs" type="button" style="padding-top: 2px; padding-bottom: 2px;" >
                                <i class="icon-search"></i>
                                Buscar
                            </button>
                        </div>
                        <div id="datosDependencia" class='hide' >
                         <div class="col-md-12" >
                              <hr>
                                      <?php echo CHtml::label('Nombre de la Dependencia Nominal Encontrada', '', array("class" => "col-md-12"));?>
                            <?php echo  $form->textField($modelDependencia, 'estatus', array('class' => 'col-md-12', 'id' => "nom_dependencia", 'readOnly' => true)); ?> 
                        </div>
                    </div>
                        <?php $this->endWidget(); ?> 
                    </div>
                    <div>&nbsp;</div>
                    </div>
                    
                                       <?php $this->endWidget(); ?> 
                        </div>
                    </div>
                  </div>
            </div>
<?php
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/public/js/modules/plantel/dependencia_nominal.js',CClientScript::POS_END
);
?>
</div>
        <script>
    $(document).ready(function() {

 $('#dependencia_nominal').focus();

        $("#dependencia_nominal").bind('keyup blur', function() {
            keyNum(this, true, true);
          //  makeUpper(this);
        });

//        $("#dependencia_nominal").bind('blur', function()  {
//
//         //   evt.preventDefault();
//            var valorDependencia = $("#dependencia_nominal").val();
//
//            if (valorDependencia.length > 0) {
//                var codigo = valorDependencia;
////alert(codigo);
//                validarDependencia(codigo);
//            }
//
//        });
        
         });
             </script>