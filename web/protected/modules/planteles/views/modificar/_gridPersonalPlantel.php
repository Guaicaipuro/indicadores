<?php
/**
 * Created by PhpStorm.
 * User: isalaz01
 * Date: 25/03/".base64_encode(15)."
 * Time: 08:59 AM
 */
/* @var $this EstructuraController  resultadoOperacion   */
/* @var $modelPersonalPlantel PersonalPlantel */
?>
<div id="autor" class="widget-box">
    <div class="widget-header">
        <h5>Personal Del Plantel</h5>
        <div class="widget-toolbar">
            <a  href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div id="personalPlantel" class="widget-body" >
        <div class="widget-body-inner" >
            <div class="widget-main form">
                <?php
                if(count($personalPlantel)>0){


                    $columna = '<div class="btn-group dropup">
                        <button style="height:34px;" class="btn dropdown-toggle" data-toggle="dropdown">
                            Reportes de personal
                            <span class="icon-caret-up icon-on-right"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-yellow pull-right">';

                    if(in_array(Constantes::ADMINISTRATIVO,$personalPlantel))
                        $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Administrativo </span>", Yii::app()->createUrl("/planteles/estructura/reporteEmpleados/plantel/" . base64_encode($plantel_id)."/periodo/".base64_encode(15)."/tipo_personal/".base64_encode(Constantes::ADMINISTRATIVO)), array("class" => "fa fa-file-pdf-o ", "title" => "Descargar reporte del personal Administrativo")) . '</li>';

                    if(in_array(Constantes::DOCENTE,$personalPlantel))
                        $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Docente </span>", Yii::app()->createUrl("/planteles/estructura/reporteEmpleados/plantel/" . base64_encode($plantel_id)."/periodo/".base64_encode(15)."/tipo_personal/".base64_encode(Constantes::DOCENTE)), array("class" => "fa fa-file-pdf-o ", "title" => "Descargar reporte del personal Docente")) . '</li>';
                    //$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Docente por Seccion</span>", Yii::app()->createUrl("/planteles/estructura/docentesPorSeccion/plantel/" . base64_encode($plantel_id)."/periodo/".base64_encode(15)."/tipo_personal/".base64_encode(Constantes::DOCENTE)), array("class" => "fa fa-file-pdf-o ", "title" => "Descargar reporte del personal Docente por Seccion")) . '</li>';
                    if(in_array(Constantes::OBRERO,$personalPlantel))
                        $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Obrero </span>", Yii::app()->createUrl("/planteles/estructura/reporteEmpleados/plantel/" . base64_encode($plantel_id)."/periodo/".base64_encode(15)."/tipo_personal/".base64_encode(Constantes::OBRERO)), array("class" => "fa fa-file-pdf-o ", "title" => "Descargar reporte del personal Obrero")) . '</li>';



                    $columna .= '</ul></div>';
                    echo $columna;
                }
                ?>
                <div class="row">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'personal-plantel-grid',
                        'dataProvider'=>$modelPersonalPlantel->search($plantel_id),
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'summaryText'=> '<br/><h5> Cantidad Total de Registros: {count} / Mostrando: del {start} al {end} </h5>',
                        'pager' => array(
                            'header' => '',
                            'htmlOptions' => array('class' => 'pagination'),
                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                        ),
                        'afterAjaxUpdate' => "
                function(){

                }",
                        'columns'=>array(
                            array(
                                'header' => '<center>Nombres</center>',
                                'name' => 'nombres',

                                'filter' => false,
                                //'value' => '(isset($data) AND is_object($data->personal) AND isset($data->personal->nombres))?$data->personal->nombres:""',
                                //'htmlOptions' => array('id'=>'PersonalPlantel_nombres'),
                            ),
                            array(
                                'header' => '<center>Apellidos</center>',
                                'name' => 'apellidos',

                                'filter' => false,
                                //'value' => '(isset($data) AND is_object($data->personal) AND isset($data->personal->apellidos))?$data->personal->apellidos:""',
                                //'htmlOptions' => array('id'=>'PersonalPlantel_apellidos'),

                            ),
                            array(
                                'header' => '<center>Tipo de Documento</center>',
                                'name' => 'tdocumento_identidad',
                                //'htmlOptions' => array('id'=>'PersonalPlantel_tdocumento_identidad'),

                                'value' => array($this, 'getNacionalidad'),

                                'filter' => false,
                            ),
                            array(
                                'header' => '<center>Documento de Identidad</center>',
                                'name' => 'documento_identidad',

                                'filter' => false,
                                //'htmlOptions' => array('id'=>'PersonalPlantel_documento_identidad'),
                                //'value' => '(isset($data) AND is_object($data->personal) AND isset($data->personal->documento_identidad))?$data->personal->documento_identidad:""',
                            ),

                            array(
                                'header' => '<center>Tipo de Personal</center>',
                                'name' => 'tipo_personal_id',
                                //'htmlOptions' => array('id'=>'tipo_personal_id', 'class'=>'TipoPersonal'),
                                'value' => '$data->tipo_personal',

                                'filter' => false,
                            ),
                            array(
                                'header' => '<center>Periodo Escolar</center>',
                                'name' => 'periodo_id',
                                'value' => '$data->periodo',

                                'filter' => false,
                            ),

                            array(
                                'header' => '<center>Estatus </center>',
                                'name' => 'estatus',
                                //'htmlOptions' => array('id'=>'PersonalPlantel_tdocumento_identidad'),

                                'value' => array($this, 'estatus'),

                                'filter' => false,
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>