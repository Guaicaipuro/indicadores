<?php
/* @var $this ModificarController */
/* @var $data CongresoPedagogico */
?>
<?php 
$datos = CongresoPedagogicoDocente::model()->docentesInscritos($model->id);
?>
<div class="view" align="center">
    
    <table style="width:800px;" class="table table-striped table-bordered table-hover">
        <thead>
                    
            <tr>
                <th class="center">
                    <b>Campo de Conocimiento</b>
                </th>
                <th class="center">
                    <b>Linea de Conocimiento</b>
                </th>
                <th class="center">
                    <b>Fecha de Inicio</b>
                </th>
                <th class="center">
                    <b>Fecha de Fin</b>
                </th>
                <th class="center">
                    <b>Observaciones</b>
                </th>
            </tr>

        </thead>
        <tbody>
            <tr>
                <td class="center">
                    <?php
                        if(isset($model->campo_conocimiento_id))
                        {
                            echo $model->campoConocimiento->nombre;
                        }
                    ?>
                </td>
                <td class="center">
                    <?php echo $model->linea_conocimiento;?>
                </td>

                <td class="center">
                    <?php echo $model->fecha_inicio;?>
                </td>

                <td class="center">
                    <?php echo $model->fecha_fin;?>
                </td>

                <td class="center">
                    <?php
                        echo $model->observaciones;
                    ?>
                </td>
            </tr>
        </tbody>
        </table>
</div>

<div class="view" align="center">
    <b>Docentes Inscritos</b>
</div>

<div class="view" align="center">
    
    <table style="width:700px;" class="table table-striped table-bordered table-hover">
        <thead>
                    
            <tr>
                <th width="30%" class="center">
                    <b>Documento de Identidad</b>
                </th>
                <th width="70%" class="center">
                    <b>Nombres y Apellidos</b>
                </th>
            </tr>

        </thead>
        <tbody>
            <?php foreach ($datos as $key => $value) {
                $documento_identidad = isset($value['documento_identidad']) ? $value['documento_identidad'] : '';
                $nombres = isset($value['nombres']) ? $value['nombres'] : '';
                $apellidos = isset($value['apellidos']) ? $value['apellidos'] : '';?>
            <tr>
                <td width="30%" class="center">
                    <?php
                        
                        echo $documento_identidad;
                    ?>
                </td>
                <td width="70%" class="center">
                    <?php
                        echo $nombres. ' '. $apellidos;
                    ?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
        
    </table>
</div>