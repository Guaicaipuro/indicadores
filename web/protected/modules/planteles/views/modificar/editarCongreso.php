<?php
/* @var $this ModificarController */
/* @var $data CongresoPedagogico */
/* @autor Pedro Chacon
    Creado 25/03/2015
    Ventana emergente con formulario para editar la informacion del Congreso Pedagogico seleccionado*/ 
?>
<div class="view" align="center">
<div class="widget-box" id="congreso_datos_box" >
                            <div class="widget-header">
                                <h5>Datos Generales</h5>
                                <div class="widget-toolbar">
                                    <a data-action="collapse" href="#">
                                        <i class="icon-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
<div class="widget-body">

                                <!-- FORMULARIO DE EDICION DE DATOS -->
                                <div class="widget-body-inner" style="display: block;">
                                    <div class="widget-main">
                                        <div class="row">
                                            <div class="form">
                                                <?php
                                                $form = $this->beginWidget('CActiveForm', array(
                                                    'id' => 'congreso-editar-form',
                                                    'enableAjaxValidation' => false,
                                                ));
                                                ?>
                                                <div class="row">
                                                    <div class="col-md-12">
            									

                                                    	<?php echo $form->textField($model, 'id', array('class' => 'hide')); ?> <!--  CAMPO OCULTO CON EL ID DEL REGISTRO-->
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'campo_conocimiento_id', array("class" => "col-md-12")); ?>
                                                            <?php echo $form->dropDownList($model, 'campo_conocimiento_id', CHtml::listData(CCampoConocimiento::getData('estatus','A'), 'id', 'nombre'), array('empty' => '- - -', 'class' => 'span-7', 'required' => 'required')); ?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'linea_conocimiento', array("class" => "col-md-12")); ?>
                                                            <?php echo $form->textField($model, 'linea_conocimiento', array('id' => 'lineaC', 'class' => 'col-md-12', 'onkeydown'=>"return validarLetras(event)")); ?>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'fecha_inicio', array("class" => "col-md-12")); ?> <!-- CAMPO DE SOLO LECTURA (SOLO EN EDITAR)-->
                                                            <?php echo $form->textField($model, 'fecha_inicio', array('class' => 'span-7','readOnly'=>'readOnly')); ?>

                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'fecha_fin', array("class" => "col-md-12")); ?> <!-- CAMPO DE SOLO LECTURA (SOLO EN EDITAR) -->
                                                            <?php echo $form->textField($model, 'fecha_fin', array('class' => 'span-7','readOnly'=>'readOnly')); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <?php echo $form->labelEx($model, 'observaciones', array("class" => "col-md-12")); ?>
                                                        <?php echo $form->textField($model, 'observaciones', array('size' => 6, 'maxlength' => 100, 'class' => 'span-7')); ?>
                                                    </div>
                                                </div>
                                                <?php $this->endWidget(); ?>
                                            </div>

                                        </div>
                                    </div>
                                    
                                </div>
</div>
                    
</div>

<script type="text/javascript">

    $("#lineaC").bind('keyup blur', function() { // FUNCION QUE PERMITE SOLO EL USO DE CARACTERES ALFABETICOS EN EL CAMPO #lineaC (LINEA DE CONOCIMIENTO)
        makeUpper(this);
    });

    function validarLetras(e) { // 1
    //e.toUpperCase();
    
   
    tecla = (document.all) ? e.keyCode : e.which; // LISTA DE CARACTERES Y COMBINACONES NO ALFABETICAS PERMITIDAS EN EL CAMPO
    if (tecla==8) return true; // backspace
        if (tecla==32) return true; // espacio
        if (e.ctrlKey && tecla==86) { return true;} //Ctrl v
        if (e.ctrlKey && tecla==67) { return true;} //Ctrl c
        
 
        patron = /[A-Z]/; //patron
        te = String.fromCharCode(tecla);
        return patron.test(te); // prueba de patron
    }   

</script>