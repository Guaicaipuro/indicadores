<?php
/* @var $this EstructuraController */
/* @var $model PersonalPlantel */
/* @var $form CActiveForm */


?>



<div class="widget-box collapsed">

    <div class="widget-header">
        <h5>Nombre del Plantel:  "<?php echo $plantel_nombre= $model->plantel->nombre;?>"</h5>

        <div class="widget-toolbar">
            <a  href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div id="idenPlantel" class="widget-body" >
        <div class="widget-body-inner" >
            <div class="widget-main form" style="width:300px;height:300px">                      

                <div class="row">
                    <table >
                        <tr>
                            <td rowspan="4" >
                                <p style="width: 180px" align= "center">
                                    <img id="tumbnailLogo" style="width:140px;height:140px;" class="img-thumbnail" alt="..." src ="<?php echo (empty($modelPlantel->logo))? Yii::app()->baseUrl . '/public/images/indice.svg' : Yii::app()->baseUrl . '/public/uploads/LogoPlanteles/thumbnail/'.$model->plantel->logo; ?>">
                                </p>
                            </td>
                        </tr>

                        <tr class="col-md-12">

                            <td colspan="3" style="vertical-align: top; width: 50px">
                                <?php
                                if ($model->plantel->codigo_ner != '') {
                                    ?>
                                    <label for="Plantel_cod_ner"><b>C&oacute;digo NER</b></label>
                                    <label class="span-7">
                                        <input type="text" value="<?php echo $model->plantel->codigo_ner; ?>" disabled="disbled">
                                    </label>
                                    <?php
                                }
                                ?>
                            </td>

                        </tr>
<div class="col-md-10"></div>
                        <tr class="col-md-2">
                            <td style="vertical-align: top; width: 50px">

                                <label for="Plantel_cod_plantel" class="col-md-9 " style="height:25px ">C&oacute;digo del Plantel</label>
                                <label class="span-7">
                                    <input type="text" value="<?php echo $model->plantel->cod_plantel; ?>" class="span-7" disabled="disbled">
                                </label>

                            </td>

                            <td style="vertical-align: top; width: 50px">

                                <label for="Plantel_cod_estadistico" class="col-md-9" style="height:25px ">C&oacute;digo Estad&iacute;stico</label>
                                <label class="span-7">
                                    <input type="text" value="<?php echo $model->plantel->cod_estadistico; ?>" class="span-7" disabled="disbled">
                                </label>

                            </td>

                            <td style="vertical-align: top; width: 50px">

                                <label for="Pantel_denominacion_id" class="col-md-9" style="height:25px ">Denominaci&oacute;n</label>
                                    
                                    <?php
                                    if(empty($model->plantel->denominacion_id))
                                    {
                                        $denominacion = '';
                                    }
                                    else
                                    {
                                        $denominacion = $model->plantel->denominacion->nombre;
                                    }
                                    ?>
                                <label class="span-7">    
                                    <input type="text" value="<?php echo $denominacion; ?>" class="span-7" disabled="disbled">
                                </label>

                            </td>
                        </tr>

                        <tr class="col-md-11">
                            <td style="vertical-align: top; width: 50px">

                                <label for="Plantel_nombre" class="col-md-9" style="height:25px ">Nombre del Plantel</label>
                                <label class="span-7">
                                    <input type="text" value="<?php echo $model->plantel->nombre; ?>" class="span-7" readonly="readonly">
                                </label>
                            </td>

                            <td style="vertical-align: top; width: 50px">

                                <label for="Plantel_zona_educativa_id" class="col-md-9" style="height:25px ">Zona Educativa</label>
                                    
                                    <?php
                                    if(empty($model->plantel->zona_educativa_id))
                                    {
                                        $zonaEducativa = '';
                                    }
                                    else
                                    {
                                        $zonaEducativa = $model->plantel->zonaEducativa->nombre;
                                    }
                                    ?>
                                <label class="span-7">    
                                    <input type="text" value="<?php echo $zonaEducativa; ?>" class="span-7" readonly="readonly">
                                </label>

                            </td>

                            <td style="vertical-align: top; width: 50px">
                                
                                <label for="Plantel_tipo_dependencia_id" class="col-md-9" style="height:25px ">Tipo de Dependencia</label>
                                    
                                    <?php
                                    if(empty($model->plantel->tipo_dependencia_id))
                                    {
                                        $tipoDependencia = '';
                                    }
                                    else
                                    {
                                        $tipoDependencia = $model->plantel->tipoDependencia->nombre;
                                    }
                                    ?>
                                <label class="span-7">    
                                    <input type="text" value="<?php echo $tipoDependencia; ?>" class="span-7" disabled="disabled">
                                </label>
                            </td>

                        </tr>
                        <tr class="col-md-11">
                            <td style="vertical-align: top; width: 50px">

                                <label for="Plantel_distrito_id" class="col-md-9" style="height:25px ">Distrito</label> 
                                    <?php
                                    /* PROBLEMA CON LA TABLA DISTRITO MIENTRAS NO TENGA NADA ESTARA ASI, MODIFICADO POR IGNACIO */
                                    // $distrito = Distrito::model()->findAll('id ='.$model->distrito_id);
                                    //echo $distrito[0]['nombre'];
                                    ?>
                                <label class="span-7">
                                    <input type="text" value="<?php #echo $model->distrito_id; ?>" class="span-7" disabled="disabled">
                                </label>
                            </td>                                     

                            <td style="vertical-align: top; width: 50px">

                                <label for="Plantel_estatus_plantel_id" class="col-md-9" style="height:25px ">Estatus</label>
                                    <?php
                                    if(empty($model->plantel->estatus_plantel_id))
                                    {
                                        $estatusPlantel = '';
                                    }
                                    else
                                    {
                                        $estatusPlantel = $model->plantel->estatusPlantel->nombre;
                                    }
                                    ?>
                                <label class="span-7">
                                    <input type="text" value="<?php echo $estatusPlantel; ?>" class="span-7" disabled="disabled">
                                </label>
                            </td>

                            <td style="vertical-align: top; width: 50px">

                                <label for="Plantel_annio_fundado" class="col-md-9" style="height:25px ">A&ntilde;o de fundaci&oacute;n</label>
                                <label class="span-7">
                                    <input type="text" value="<?php echo $model->plantel->annio_fundado; ?>" class="span-7" disabled="disabled">
                                </label>
                            </td>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>