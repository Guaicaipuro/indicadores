<?php ?>

<!--<div id="resultado">
    <div class="infoDialogBox">
        <p>
            Debe Ingresar los Datos Generales del Plantel, los campos marcados con <span class="required">*</span> son requeridos.
        </p>
    </div>
</div>-->
<?php
if ($compararFinalExistoso == 0) {
    ?>
    <div id="result-solicitudExito" >
        <div id ="solicitudRechazada" class="alertDialogBox" style="display:block">
            <p>
                Lo sentimos, actualmente no posee estudiante a quien solicitar título.
            </p>

        </div>
        <?php
    } else {
        ?>
        <div id ="solicitudExitosa" class="successDialogBox" style="display:block">
            <p>
                Estimado Usuario, el proceso de solicitud de título se ha realizado exitosamente.
            </p>
        </div>

        <div id="mostrarSolicitudTitulo" class="widget-box">

            <div class="widget-header">
                <h5>Título Solicitado </h5>
                <div class="widget-toolbar">
                    <a  href="#" data-action="collapse">
                        <i class="icon-chevron-down"></i>
                    </a>
                </div>
            </div>

            <div id="servicio" class="widget-body" >
                <div class="widget-body-inner" >
                    <div class="widget-main form">

                        <div class="row">
                            <div class="col-md-12" id ="solicitud-titulo">
                                <div id="scrolltable" style='border: 0px;background: #fff; overflow:auto;padding-right: 0px; padding-top: 0px; padding-left: 0px; padding-bottom: 0px;border-right: #DDDDDD 0px solid; border-top: #DDDDDD 0px solid;border-left: #DDDDDD 0px solid; border-bottom: #DDDDDD 0px solid;scrollbar-arrow-color : #999999; scrollbar-face-color : #666666;scrollbar-track-color :#3333333 ;height:400px; left: 28%; top: 300; width: 100%'>
                                    <?php
                                    //var_dump($dataProvider);
                                    //        die();
                                    if (isset($dataSolicitud)) {
                                        $this->widget(
                                                'zii.widgets.grid.CGridView', array(
                                            'id' => 'solicitudTitulo-grid',
                                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                            'dataProvider' => $dataSolicitud,
                                            'summaryText' => false,
                                            'columns' => array(
                                                //       array(
                                                //                    "name" => "boton",
//                    "type" => "raw",
//                    'header' => '<div class="center">' . CHtml::checkBox('selec_todoEst', false, array('id' => 'selec_todoEst', 'title' => 'Seleccionar todos','class' => 'tooltipMatricula')) . "</div>"
//            ),
                                                array(
                                                    'name' => 'documento_identidad',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Documento de Identidad</b></center>',
                                                    'value' => array($this, 'columnaDocumentoIdentidad'),
                                                ),
                                                //                array(
//                    'name' => 'serial',
//                    'type' => 'raw',
//                    'header' => '<center><b>serial</b></center>'
//                ),
                                                array(
                                                    'name' => 'nombreApellido',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Nombres y Apellidos</b></center>'
                                                ),
                                                array(
                                                    'name' => 'nombre_seccion',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Sección</b></center>'
                                                ),
                                                //                array(
//                    'name' => 'nombre_seccion',
//                    'type' => 'raw',
//                    'header' => '<center><b> Sección </b></center>'
//                ),
                                                array(
                                                    'name' => 'grado',
                                                    'type' => 'raw',
                                                    'header' => '<center><b> Grado</b></center>'
                                                ),
                                                array(
                                                    'name' => 'nombre_mencion',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Mención</b></center>'
                                                ),
                                            ),
                                            'pager' => array(
                                                'header' => '',
                                                'htmlOptions' => array('class' => 'pagination'),
                                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                            ),
                                                )
                                        );
                                    }
                                    ?>
                                </div>
                            </div>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="col-md-2">
        <div class="col-md-2">
            <div class="col-md-6">
                <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("/planteles/Titulo/indexTitulo/id/ " . base64_encode($plantel_id)); ?>" class="btn btn-danger">
                    <i class="icon-arrow-left"></i>
                    Volver
                </a>
            </div>
        </div>

    </div>

