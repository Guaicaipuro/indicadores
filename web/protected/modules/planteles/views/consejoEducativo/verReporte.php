<?php 
/****** DATOS DEL PLANTEL**************/

if(is_object($model->plantel)){

    $plantel = array();
    if(isset($model->plantel->cod_plantel)){
        $plantel['cod_plantel'] = $model->plantel->cod_plantel;
    }else{
        $plantel['cod_plantel'] ='';
    }

    if(isset($model->plantel->nombre)){
        $plantel['nombre'] = $model->plantel->nombre;
    }else{
        $plantel['nombre'] = '';
    }

    if(is_object($model->plantel->estado) AND isset($model->plantel->estado->nombre)){
        $plantel['estado'] = $model->plantel->estado->nombre;
    }else{
        $plantel['estado'] ='';
    }
    
    if(is_object($model->plantel->municipio) AND isset($model->plantel->municipio->nombre)){
        $plantel['municipio'] = $model->plantel->municipio->nombre;
    }else{
        $plantel['municipio'] ='';
    }

    if(is_object($model->plantel->parroquia) AND isset($model->plantel->parroquia->nombre)){
        $plantel['parroquia'] = $model->plantel->parroquia->nombre;
    }else{
         $plantel['parroquia'] ='';
    }

    if(isset($model->plantel->direccion)){
        $plantel['direccion'] = $model->plantel->direccion;
    }else{
        $plantel['direccion'] = '';
    }

    if(is_object($model->plantel->tipoDependencia) && isset($model->plantel->tipoDependencia->nombre))
    {
        $plantel['dependencia'] = strtoupper($model->plantel->tipoDependencia->nombre);
    }else{
        $plantel['dependencia'] = '';
    }

    if(is_object($model->plantel->zonaUbicacion) && isset($model->plantel->zonaUbicacion->nombre))
    {
        $plantel['zonaUbicacion'] = strtoupper($model->plantel->zonaUbicacion->nombre);
    }else{
        $plantel['zonaUbicacion'] = '';
    }

    if(is_object($model->plantel->modalidad) && isset($model->plantel->modalidad->nombre))
    {
        $plantel['modalidad'] = $model->plantel->modalidad->nombre;
    }else{
        $plantel['modalidad'] = '';
    }

    if(isset($model->plantel->correo)){
        $plantel['correo'] = $model->plantel->correo;
    }else{
        $plantel['correo'] = '';
    }
/*************DATOS DEL DIRECTOR********************/
   /*
    if(is_object($model->plantel->directorActual)){

        $directorActual = array();
        if(isset($model->plantel->directorActual->cedula)){
            $directorActual['cedula'] = $model->plantel->directorActual->cedula;
        }else{
            $directorActual['cedula'] = '';
        }

        if(isset($model->plantel->directorActual->nombre)){
            $directorActual['nombre'] = $model->plantel->directorActual->nombre;
        }else{
            $directorActual['nombre'] = '';
        }

        if(isset($model->plantel->directorActual->apellido)){
            $directorActual['apellido'] = $model->plantel->directorActual->apellido;
        }else{
            $directorActual['apellido'] = '';
        }

        if(isset($model->plantel->directorActual->telefono)){
            $directorActual['telefono'] = $model->plantel->directorActual->telefono;
        }else{
            $directorActual['telefono'] = '';
        }
        
        if(isset($model->plantel->directorActual->correo)){
            $directorActual['correo'] = $model->plantel->directorActual->correo;
        }else{
            $directorActual['correo'] = '';
        }        
    }*/


/* $data son los datos que se quieren mostrar del director  */

$data = array('cedula','nombre','apellido','email','telefono');
$directorActual = datosDirector($model,$data);
}
/* $data son los datos que se quieren mostrar del director  */
/* datosDirector ES UNA FUNCION PARA OBTENER LOS DATOS DEL DIRECTOR DEL PLANTEL*/
function datosDirector($model,$data){

    $directorActual = array();
    if(is_object($model->plantel->directorActual))
    {

        foreach ($data as $key => $value) {
            if(isset($model->plantel->directorActual->$value)){

                $directorActual[$value] = $model->plantel->directorActual->$value;
            }else{
                $directorActual[$value] = '';
            }
        }
    }else{
        foreach ($data as $key => $value) {
            $directorActual[$value] = '';
        }
    }
    return $directorActual;
}

?>

<table style="font-size:12en; font-family: Arial; width:800px;" cellpadding="6" cellspacing="0">
    <tr>
        <td colspan="5" align="center" style="background: #8BBBCC; padding:3px; font-family: Arial;" >
            <b>DATOS DEL PLANTEL</b>
        </td>
    </tr>

    <tr>
        <td colspan="5">
        </td>
    </tr>

    <tr>
        <td style="font-family: Arial;" colspan="2" width="30%">
            <b>Código del plantel:</b>
            <?php echo $plantel['cod_plantel']; ?>
        </td>
        <td style="font-family: Arial;" colspan="3" width="70%">
            <b>Nombre del Plantel:</b>
            <?php echo $plantel['nombre']; ?>
        </td>
    </tr>
    
    <tr>
        <td style="font-family: Arial;" colspan="2" width="30%">
            <b>Estado:</b>
            <?php echo $plantel['estado']; ?>
        </td>

        <td style="font-family: Arial;" colspan="1" width="35%">
            <b>Municipio:</b>
            <?php echo $plantel['municipio']; ?>
        </td>

        <td style="font-family: Arial;" colspan="2" width="35%" >
            <b>Parroquia:</b>
            <?php echo $plantel['parroquia']; ?>
        </td>
    </tr>
    
    <tr>
        <td style="font-family: Arial;" colspan="5" >
            <b>Dirección:</b>
            <?php echo $plantel['direccion']; ?>
        </td>

    </tr>
    
    <tr>
        <td style="font-family: Arial;" colspan="2" >
            <b>Dependencia:</b>
            <?php echo $plantel['dependencia']; ?>
        </td>
        <td style="font-family: Arial;" colspan="3">
            <b>Zona de Ubicación:</b>
            <?php echo $plantel['zonaUbicacion']; ?>
        </td>
 
    </tr>
    <tr>

        <td style="font-family: Arial;" colspan="2">
            <b>Modalidad:</b>
            <?php echo $plantel['modalidad']; ?>
        </td> 
        <td style="font-family: Arial;" colspan="3" >
            <b>Correo del Plantel:</b>
            <?php echo $plantel['correo']; ?>
        </td>
    </tr>

</table>

<br>

<table style="font-size:12en; font-family: Arial; width:800px;" cellpadding="6" cellspacing="0">
    <tr>
        <td colspan="5" align="center" style="background: #8BBBCC; padding:3px; font-family: Arial;" >
            <b>DATOS DEL DIRECTOR DEL PLANTEL</b>
        </td>
    </tr>

    <tr>
        <td colspan="5">
            <br>
        </td>
    </tr>

    <?php if(!empty($directorActual['cedula'])){ ?>
    <tr>
        <td style="font-family: Arial; text-align: center;" width="10%">
            <b>Cédula</b><hr>
        </td>
        <td style="font-family: Arial; text-align: center" width="30%">
            <b>Nombres</b><hr>
        </td>
        <td style="font-family: Arial; text-align: center" width="30%">
            <b>Apellidos</b><hr>
        </td> 
        <td style="font-family: Arial; text-align: center" width="20%">
            <b>Correo</b><hr>
        </td>
        <td style="font-family: Arial; text-align: center" width="10%">
            <b>Teléfono</b><hr>
        </td> 

    </tr>

    <tr>

        <td style="font-family: Arial; text-align:center;" >
            <?php echo $directorActual['cedula']; ?><hr>
        </td>

        <td style="font-family: Arial; text-align:center;" >
            <?php echo $directorActual['nombre']; ?><hr>
        </td>

        <td style="font-family: Arial; text-align:center;" >
            <?php echo $directorActual['apellido']; ?><hr>
        </td>

        <td style="font-family: Arial; text-align:center;" >
            <?php echo $directorActual['email']; ?><hr>
        </td>

        <td style="font-family: Arial; text-align:center;" >
            <?php echo $directorActual['telefono']; ?><hr>
        </td>
    </tr>

<?php }else{ ?>

        <tr>
        <td colspan="5" style="font-style: italic;">
            No se encuentran registrados los datos del director del plantel
        </td>
    </tr>

<?php }// fin del else ?>

</table>

<br>

<table style="font-size:12en; font-family: Arial; width:800px;" cellpadding="6" cellspacing="0">

    <tr>
        <td colspan="3" align="center" style="background: #8BBBCC; padding:3px; font-family: Arial;" >
            <b>DATOS DEL CONSEJO EDUCATIVO</b>
        </td>
    </tr>

    <tr rowspan='2'>
        <td colspan="3">
            <br>
        </td>
    </tr>

    <tr>
        <td style="font-family: Arial;" width="30%">
            <b>RIF: </b>
            <?php echo $model->rif; ?>
        </td>
        
        <td style="font-family: Arial;" width="30%">
            <b> Última Fecha de Registro en Taquilla: </b>
            <?php echo $model->fecha_registro; ?>
        </td>
        <td style="font-family: Arial;" width="40%">
            <b>Actualizado: </b>
            <?php if($model->fecha_registro>= $hoy){
                    echo 'Actualizado';
                    }else{
                        echo 'No Actualizado';
                        } ?>
        </td>        
    </tr>

    <tr>
        <td style="font-family: Arial;" width="30%">
            <b>Banco: </b>
            <?php echo $model->banco->nombre; ?>
        </td>    
        <td style="font-family: Arial;" width="40%">
            <b>Número de Cuenta: </b>
            <?php echo $model->numero_cuenta; ?>
        </td>
        
        <td style="font-family: Arial;" width="30%">
            <b>Posee Comite de Economía: </b>
            <?php echo $model->comite_economia; ?>
        </td>
       
    </tr>
    
    <tr>
        
        <td style="font-family: Arial;">
            <b>Pertenece a un Circuito Educativo: </b>
            <?php echo $model->circuito_educativo; ?>
        </td> 

        <td style="font-family: Arial;" colspan="2">
            <b>Proyecto Educativo Integral Comunitario: </b>
            <?php echo $model->peic; ?>
        </td>
    </tr>    
</table>

<br>

<table style="font-size:12en; font-family: Arial; width:800px;" cellpadding="6" cellspacing="0">
    <tr>
        <td colspan="3" align="center" style="background: #8BBBCC; padding:3px; font-family: Arial;" >
            <b>PROYECTOS DEL CONSEJO EDUCATIVO</b>
        </td>
    </tr>

    <tr rowspan='2'>
        <td colspan="3">
        <br>
        </td>
    </tr>

<?php if(isset($datosProyectos) and !empty($datosProyectos)){ ?>


    <tr>
        <td style="font-family: Arial; text-align: center;" colspan="1" width="20%">
            <b><i>PROYECTOS</i></b><hr>
        </td>
        <td style="font-family: Arial; text-align: center;" colspan="2" width="80%">
            <b><i>CATEGORÍAS</i></b><hr>
        </td>            
    </tr>
    <?php 
        /*SE REALIZA UN FOREACH DE $DATOSPROYECTOS QUE ES UN ARRAY QUE POSEE 
        COMO CLAVE LOS NOMBRES DE LOS PROYECTOS Y COMO VALOR UN STRING CON LOS NOMBRES
        DE LAS CATEGORIAS ASOCIADAS CON ESE PROYECTO SEPARADAS POR PUNTO Y COMA*/ 
    ?>
    <?php foreach ($datosProyectos as $nombre_proyecto => $nombre_categoria) {?>
        <tr>
            <td style="font-family: Arial;" colspan="1" width="25%">
                <i><?php echo $nombre_proyecto?> </i>
            </td>
            <td style="font-family: Arial;" colspan="2" width="75%">
                <?php echo $nombre_categoria; ?>
            </td>            
        </tr>
    <?php } // endforeach $datosProyectos ?>

<?php }// endif 
        else{ ?>

                <tr>
                    <td style="font-family: Arial;" colspan="3">
                        <i>El Consejo Educativo no posee proyectos  </i>
                    </td>           
                </tr>        
<?php } //endelse?>
</table>