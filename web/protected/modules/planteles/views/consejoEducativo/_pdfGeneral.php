<style>
table#principal {
	width: 100%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th{
	background-color: #8BBBCC;
}
td {
    padding: 10px;
}

</style>

<table width="20%" align="right" >
	<tr>
		<td width="20%" style="font-weight: bold;">
			Consejos Actualizados
		</td>	
		<td width="10%" style="font-weight: bold; text-align: center;">
			<?php echo  $actualizado; ?>
		</td>
	</tr>
	<tr>
		<td width="20%" style="font-weight: bold">
			Consejos No actualizados
		</td>
		<td width="10%" style="font-weight: bold; text-align: center;" >
			<?php echo  $no_actualizado; ?>
		</td>
	</tr>
	<tr>
		<td width="20%" style="font-weight: bold;">
			Total Resultados
		</td>
		<td width="10%" style="font-weight: bold; text-align: center;">
			<?php echo  $contador; ?>
		</td>		
	</tr>	
</table>
<br>
<br>
<table align="center" id="principal">