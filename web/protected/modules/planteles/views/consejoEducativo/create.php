<?php
/* @var $this ConsejoEducativoController */
/* @var $model ConsejoEducativo */

$this->pageTitle = 'Registro de Consejo Educativos';
      $this->breadcrumbs=array(
        'Planteles' => array('/planteles/consultar'),
	'Registro de Consejo Educativo',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro',
	'modelTaquilla'=>$modelTaquilla,
	'modelPlantel'=>$modelPlantel,
	'bancos'=>$bancos,'estados'=>$estados,
	'proyectos'=>$proyectos,
	'categorias'=>$categorias,
	'matriculaEstudiante'=>$matriculaEstudiante
	)); ?>