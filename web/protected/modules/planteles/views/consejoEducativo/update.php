<?php
/* @var $this ConsejoEducativoController */
/* @var $model ConsejoEducativo */

$this->pageTitle = 'Actualización de Datos de Consejo Educativos';
      $this->breadcrumbs=array(
        'Planteles' => array('/planteles/consultar'),
	'Actualización de Consejo Educativo',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion',
	'modelTaquilla'=>$modelTaquilla,
	'modelPlantel'=>$modelPlantel,
	'bancos'=>$bancos,
	'estados'=>$estados,
	'proyectos'=>$proyectos,
	'categorias'=>$categorias,
	'categorias_seleccionadas'=>$categorias_seleccionadas,
	'municipios'=>$municipios,'parroquias'=>$parroquias,
	'poblacion'=>$poblacion,
	'urbanizacion'=>$urbanizacion,
	'tipoVia'=>$tipoVia,
	'matriculaEstudiante'=>$matriculaEstudiante
	)); 
?>