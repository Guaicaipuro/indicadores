<?php

/* @var $this ConsejoEducativoController */
/* @var $model ConsejoEducativo */

$this->breadcrumbs=array(
    'Consejos Educativos'=>array('lista'),
    'Administración',
);
$this->pageTitle = 'Administración de Consejos Educativos';

?>

    <?php 
        $search='_search';
    ?>

        <div class="widget-box collapsed">

            <div class="widget-header">
                <h5>Búsqueda Avanzada de Consejos Educativos</h5>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="icon-chevron-down"></i>
                    </a>
                </div>

            </div>

            <div class="widget-body">
                <div style="display: none;" class="widget-body-inner">
                    <div class="widget-main">

                        <?php echo CHtml::link('', '#', array('class' => 'search-button')); ?>
                        <div class="search-form" style="display:block">
                            <?php
                            $this->renderPartial($search, array(
                                'model' => $model,
                                'modelPlantel' =>$modelPlantel,
                                'municipios' => $municipios,
                                'condicionEstado'=>$condicionEstado,
                            ));
                            ?>
                        </div><!-- search-form -->
                    </div>
                </div>
            </div>
        </div>

<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Consejos Educativos</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="col-md-12">
                    <div class="col-md-4">
                        <?php 
                            if(Yii::app()->user->pbac('planteles.consejoEducativo.admin') || Yii::app()->user->pbac('planteles.consejoEducativo.read'))
                            {
                                echo CHtml::link(" Exportar Búsqueda", '/planteles/consejoEducativo/PdfGeneral', array("class" => "btn btn-primary fa fa-file-pdf-o  blue", "title" => "Exportar Búsqueda"));
                            }
                        ?> 
                    </div>                    
                    <div class="col-md-8"></div>
                </div>
   


                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                    </div>
                </div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'consejo-educativo-grid',
    'dataProvider'=>$dataProvider,
        'filter'=> $model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
                    
                    $('.cod_plantel').on('keyup keydown',function(tecla){
                        makeUpper(this);
                    });

                    $('.plantel_nombre').on('keyup keydown',function(tecla){
                        keyTextDash(this, true, true);
                        makeUpper(this);
                    });

                    $.mask.definitions['H'] = '[J|j|g|G]';
                    $('.ConsejoEducativo_rif').mask('H-999999999');

                    $('.ConsejoEducativo_rif').blur(function(){
                        this.value= this.value.toUpperCase();
                    });                     
                }",
    'columns'=>array(
        /*
        array(
            'header' => '<center>Id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('ConsejoEducativo[id]', $model->id, array('title' => '',)),
        ),
        */
        array(
            'header' => '<center>Entidad Federal</center>',
            'value' => '(is_object($data->plantel->estado) && isset($data->plantel->estado->nombre))? $data->plantel->estado->nombre: ""',
            'htmlOptions' => array(),
            'filter' => CHtml::dropDownList("Plantel[estado_id]",$modelPlantel->estado_id,CHtml::listData(CEstado::getData(), 'id', 'nombre'),array('id'=>'Plantel_estado_id2','empty' => array('' => ''),'title'=>'Estados',$condicionEstado=>$condicionEstado)),
            //'filter' => CHtml::textField('Plantel[estado_id]', $modelPlantel->estado_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Nombre Plantel</center>',
            'value' => ' $data->plantel->nombre',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Plantel[nombre]', $modelPlantel->nombre, array('title' => 'Nombre del Plantel','value'=>$modelPlantel->nombre,'class'=>'plantel_nombre')),
        ),          
        array(
            'header' => '<center>Código DEA</center>',
            'value' => ' $data->plantel->cod_plantel',
            'htmlOptions' => array('style'=>'text-align:center;'),
            'filter' => CHtml::textField('Plantel[cod_plantel]', $modelPlantel->cod_plantel, array('title' => 'Código del Plantel','value'=>$modelPlantel->cod_plantel,'class'=>'cod_plantel')),
        ),      
        array(
            'header' => '<center>RIF del Consejo Educativo</center>',
            'name' => 'rif',
            'htmlOptions' => array('style'=>'text-align:center;'),
            'filter' => CHtml::textField('ConsejoEducativo[rif]', $model->rif, array('title' => 'RIF del Consejo Educativo','class'=>'ConsejoEducativo_rif')),
        ),
        array(
            'header' => '<center>Forma Parte de un Circuito Educativo Comunitario</center>',
            'name' => 'circuito_educativo',
            'htmlOptions' => array('style'=>'text-align:center;'),
            'filter' => array('SI' => 'SI', 'NO' => 'NO'),
            'filterHtmlOptions'=>array('style'=>'text-align: center;','title'=>'Forma Parte de un Circuito Educativo Comunitario'),
        ),        
        
        array(
            'header' => '<center>Posee Poligonal definida y Mapeo de Georeferenciación</center>',
            'value' => '(is_object($data->plantel) && isset($data->plantel->longitud) && $data->plantel->longitud!=0  && isset($data->plantel->latitud) && $data->plantel->latitud!=0)?"SI":"NO"',
            'htmlOptions' => array('style'=>'text-align:center;'),
            'filter' => CHtml::dropDownList("Plantel[poligonal]",$modelPlantel->latitud,array('SI'=>'SI','NO'=>'NO'),array('id'=>'Plantel_poligonal','empty' => array('' => ''),'title'=>'Estados')),

        ),
        array(
            'header' => '<center>Actualizado</center>',
            'name' => 'fecha_registro',
            'htmlOptions' => array(),
            'filter' => array('A' => 'Actualizado', 'N' => 'No Actualizado'),
            'value'=> array($this,'columnaActualizado'), 
            //'filter' => CHtml::textField('ConsejoEducativo[fecha_registro]', $model->fecha_registro, array('title' => 'Fecha de último registro',)),
        ),
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[estatus]', $model->estatus, array('title' => '',)),
            'filter' => array('A' => 'Activo', 'I' => 'Inactivo'),
            'value'=> array($this,'columnaEstatus'),
        ),
        array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
    ),
)); ?>
            </div>
        </div>
    </div>
</div>

<?php
    /**
     * Yii::app()->clientScript->registerScriptFile(
     *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/ConsejoEducativoController/consejo-educativo/admin.js', CClientScript::POS_END
     *);
     */
?>
<?php

    Yii::app()->clientScript->registerScriptFile(
        Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js',CClientScript::POS_END
    );
             
?>   
<script type="text/javascript">
    
$(document).ready(function(){

    $('#consejo-educativo-search').submit(function(e){

        e.preventDefault();
        refrescarGrid();
    });

    $('.cod_plantel').on('keyup keydown',function(tecla){
        makeUpper(this);
    });

    $('.plantel_nombre').on('keyup keydown',function(tecla){
        keyTextDash(this, true, true);
        makeUpper(this);
    });

    $.mask.definitions['H'] = '[J|j|g|G]';
    $(".ConsejoEducativo_rif").mask("H-999999999");
    $('#ConsejoEducativo_numero_cuenta').mask('99999999999999999999');

    $(".ConsejoEducativo_rif").blur(function(){
        this.value= this.value.toUpperCase();
    });
    
    $('#cedulaIdentidadDirector').bind('keyup keydown blur', function() {
        keyNum(this, true);
    });
});

function refrescarGrid(){
    
    $('#consejo-educativo-grid').yiiGridView('update', {
        data: $('#consejo-educativo-search').serialize()
    });
}

</script>