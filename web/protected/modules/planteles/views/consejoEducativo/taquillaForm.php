<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'taquilla-consejo-form',
        'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>

    <div id="div-taquilla-unica">

        <div class="widget-box">

            <div class="widget-header">
                <h5> Ubicación de Taquilla Única</h5>

                <div class="widget-toolbar">
                    <a data-action="collapse" href="#">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-body-inner">
                    <div class="widget-main">
                        <div class="widget-main form">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="col-md-4">
                                        <?php echo $form->labelEx($model,'estado_id'); ?>
                                        <?php echo $form->dropDownList($model,'estado_id',$estados, array('prompt'=>'--SELECCIONE--', 'class' => 'span-12', "required"=>"required",'title'=>'Seleccione un Estado')); ?>
                                    </div>

                                    <?php if($formType=='edicion'){ ?>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'municipio_id'); ?>
                                            <?php echo $form->dropDownList($model,'municipio_id', $municipios, array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",'title'=>'Seleccione un Municipio')); ?>
                                        </div>


                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'parroquia_id'); ?>
                                            <?php echo $form->dropDownList($model,'parroquia_id',$parroquias, array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",'title'=>'Seleccione una Parroquia')); ?>
                                        </div>

                                    <?php }else if($formType=='registro'){ ?>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'municipio_id'); ?>
                                            <?php echo $form->dropDownList($model,'municipio_id', array(), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",'title'=>'Seleccione un Municipio')); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'parroquia_id'); ?>
                                            <?php echo $form->dropDownList($model,'parroquia_id',array(), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",'title'=>'Seleccione una Parroquia')); ?>
                                        </div>

                                    <?php } ?>



                                </div>

                                <div class="space-6"></div>

                                <div class="col-md-12">

                                    <?php if($formType=='edicion'){ ?>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'poblacion_id'); ?>
                                            <?php echo $form->dropDownList($model,'poblacion_id',$poblacion, array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",'title'=>'Seleccione una Población')); ?>
                                        </div>


                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'urbanizacion_id'); ?>
                                            <?php echo $form->dropDownList($model,'urbanizacion_id',$urbanizacion, array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",'title'=>'Seleccione una Urbanización')); ?>
                                        </div>


                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'tipo_via_id'); ?>
                                            <?php echo $form->dropDownList($model, 'tipo_via_id', $tipoVia, array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",'title'=>'Seleccione un tipo de via')); ?>
                                        </div>


                                    <?php }else if($formType=='registro'){ ?>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'poblacion_id'); ?>
                                            <?php echo $form->dropDownList($model,'poblacion_id',array(), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",'title'=>'Seleccione una Población')); ?>
                                        </div>


                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'urbanizacion_id'); ?>
                                            <?php echo $form->dropDownList($model,'urbanizacion_id',array(), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",'title'=>'Seleccione una Urbanización')); ?>
                                        </div>


                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'tipo_via_id'); ?>
                                            <?php echo $form->dropDownList($model, 'tipo_via_id', array(), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",'title'=>'Seleccione una tipo de via')); ?>
                                        </div>

                                    <?php } ?>

                                </div>

                                <div class="space-6"></div>

                                <div class="col-md-12">

                                    <div class="col-md-4 autocomplete-w1">
                                        <?php echo $form->labelEx($model,'via'); ?>
                                        <?php echo $form->textField($model, 'via', array('size' => 6, 'maxlength' => 70, 'placeholder' => 'Introduzca nombre de la via', 'class' => 'span-12', 'id' => 'query', 'onkeyup' => 'makeUpper("#query");','required'=>'required','title'=>'Nombre de la via')); ?>
                                        <div id="log" style="height: 200px; width: 300px; overflow: auto;" class="ui-widget-content" hidden="hidden"></div>
                                    </div>


                                    <div class="col-md-8">
                                        <?php echo $form->labelEx($model,'direccion'); ?>
                                        <?php echo $form->textArea($model,'direccion',array('rows'=>4, 'maxlength'=>200, 'class' => 'span-12',"required"=>"required",'title'=>'Direccion de la Taquilla Única' )); ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
