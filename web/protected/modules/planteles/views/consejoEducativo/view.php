<?php
/* @var $this ConsejoEducativoController */
/* @var $model ConsejoEducativo */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
	'Consejo Educativos'=>array('lista'),
);
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGeneralesPlantel">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'consejo-educativo-plantel-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-datos-generales-plantel">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales del Plantel</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            Código DEA
                                                            <?php echo CHtml::textField('codigo_plantel',(is_object($model->plantel) && isset($model->plantel->cod_plantel))? $model->plantel->cod_plantel: "",array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            Plantel
                                                            <?php echo CHtml::textField('plantel_nombre',(is_object($model->plantel) && isset($model->plantel->nombre))? $model->plantel->nombre: "", array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            Estado
                                                            <?php echo CHtml::textField('estado_nombre',(is_object($model->plantel->estado) && isset($model->plantel->estado->nombre))? $model->plantel->estado->nombre: "", array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>



                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">


                                                        <div class="col-md-4">
                                                            Municipio
                                                            <?php echo CHtml::textField('municipio_nombre',(is_object($model->plantel->municipio) && isset($model->plantel->municipio->nombre))? $model->plantel->municipio->nombre: "", array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Parroquia
                                                            <?php echo CHtml::textField('parroquia_nombre',(is_object($model->plantel->parroquia) && isset($model->plantel->parroquia->nombre))? $model->plantel->parroquia->nombre: "", array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Dirección
                                                            <?php echo CHtml::textArea('direccion',(is_object($model->plantel) && isset($model->plantel->direccion))? $model->plantel->direccion: "", array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>
                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            Dependencia
                                                            <?php echo CHtml::textField('dependencia_nombre',(is_object($model->plantel->tipoDependencia) && isset($model->plantel->tipoDependencia->nombre))? $model->plantel->tipoDependencia->nombre: "", array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Zona de ubicación
                                                            <?php echo CHtml::textField('zona_nombre',(is_object($model->plantel->zonaUbicacion) && isset($model->plantel->zonaUbicacion->nombre))? $model->plantel->zonaUbicacion->nombre: "", array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Modalidad
                                                            <?php echo CHtml::textField('modalidad_nombre',(is_object($model->plantel->modalidad) && isset($model->plantel->modalidad->nombre))? $model->plantel->modalidad->nombre: "", array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>
                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            Correo del Plantel
                                                            <?php echo CHtml::textField('plantel_correo',(is_object($model->plantel) && isset($model->plantel->correo))? $model->plantel->correo: "", array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Cédula del Director
                                                            <?php echo CHtml::textField('director_cedula',(is_object($model->plantel->directorActual) && isset($model->plantel->directorActual->cedula))? $model->plantel->directorActual->cedula: "", array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Nombres y Apellidos del Director
                                                            <?php echo CHtml::textField('director_nombre',(is_object($model->plantel->directorActual) && isset($model->plantel->directorActual->apellido) && isset($model->plantel->directorActual->nombre) )? $model->plantel->directorActual->nombre.' '.$model->plantel->directorActual->apellido: "", array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>                                                  
                                                    </div>


                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            Télefono del Director
                                                            <?php echo CHtml::textField('director_telefono',(is_object($model->plantel->directorActual) && isset($model->plantel->directorActual->telefono))? $model->plantel->directorActual->telefono: "", array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>
                                                    
                                                        <div class="col-md-4">

                                                            <table class="table">
                                                                <caption style="text-align:center;">Matricula de Estudiantes</caption>
                                                                <tr>
                                                                    <th style="text-align:center;">
                                                                        Genero
                                                                    </th>
                                                                    <th style="text-align:center;">
                                                                        Cantidad
                                                                    </th>
                                                                </tr>                                                               
                                                                <tr>
                                                                    <td>
                                                                        Masculino
                                                                    </td>
                                                                    <td style="text-align:center;">
                                                                        <?php 
                                                                            if($matriculaEstudiante["total"]==0) {echo 0;}
                                                                            else{ echo $matriculaEstudiante["m"];} 
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Femenino
                                                                    </td>
                                                                    <td style="text-align:center;">
                                                                        <?php 
                                                                            if($matriculaEstudiante["total"]==0) {echo 0;}
                                                                            else{ echo $matriculaEstudiante["f"];} 
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Sin Identificar
                                                                    </td>
                                                                    <td style="text-align:center;">
                                                                        <?php 
                                                                            if($matriculaEstudiante["total"]==0) {echo 0;}
                                                                            else{ echo $matriculaEstudiante["sin_sexo"];} 
                                                                        ?>
                                                                    </td>
                                                                </tr>                                                                
                                                                <tr class="danger">
                                                                    <td>
                                                                        Total alumnos
                                                                    </td>
                                                                    <td style="text-align:center;">
                                                                        <?php echo $matriculaEstudiante["total"]; ?>
                                                                    </td>
                                                                </tr>                                                                                                                               
                                                            </table>
                                                        </div>
                                                    
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'consejo-educativo-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales del Consejo Educativo</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            RIF
                                                            <?php echo $form->textField($model,'rif',array('size'=>11, 'maxlength'=>11, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            Última Fecha de Registro en Taquilla
                                                            <?php echo $form->textField($model,'fecha_registro', array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            Banco
                                                            <?php echo CHtml::textField('banco',$model->banco->nombre, array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>



                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">


                                                        <div class="col-md-4">
                                                            Número de cuenta
                                                            <?php echo $form->textField($model,'numero_cuenta', array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Posee Comite de Ecónomia Escolar
                                                            <?php echo $form->textField($model,'comite_economia',array('size'=>2, 'maxlength'=>2, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Forma Parte de un Circuito Educativo Comunitario
                                                            <?php echo $form->textField($model,'circuito_educativo',array('size'=>2, 'maxlength'=>2, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>



                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                            Proyecto Educativo Integral Comunitario (PEIC)
                                                            <?php echo $form->textArea($model,'peic',array('rows'=>4,'cols'=>50, 'maxlength'=>160, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        

                                                  </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane active" id="datosTaquillaUnica">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'taquilla-unica-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-taquilla-unica">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Ubicación de Taquilla Única</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            Estado
                                                            <?php echo CHtml::textField('Taquilla[estado]',$modelTaquilla->estado->nombre,array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            Municipio
                                                            <?php echo CHtml::textField('Taquilla[estado]',$modelTaquilla->municipio->nombre,array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            Parroquia
                                                            <?php echo CHtml::textField('Taquilla[parroquia]',$modelTaquilla->parroquia->nombre,array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>



                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">


                                                        <div class="col-md-4">
                                                            Población
                                                            <?php echo CHtml::textField('Taquilla[poblacion]',$modelTaquilla->poblacion->nombre,array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Urbanización
                                                            <?php echo CHtml::textField('Taquilla[urbanizacion]',$modelTaquilla->urbanizacion->nombre,array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Tipo Vía
                                                            <?php echo CHtml::textField('Taquilla[tipo_via]',$modelTaquilla->tipoVia->nb_tipo_via,array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>



                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            Via
                                                            <?php echo CHtml::textField('Taquilla[via]',$modelTaquilla->via,array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Dirección
                                                            <?php echo CHtml::textArea('Taquilla[direccion]',$modelTaquilla->direccion,array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                  </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>
                <?php if (!empty($datosProyectos)) { ?>
                <div class="tab-pane active" id="datosProyectosConsejo">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'proyectos-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-proyectos-consejo">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Proyectos del Consejo Educativo</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">

                                            <?php /*SE REALIZA UN FOREACH DE $DATOSPROYECTOS QUE ES UN ARRAY QUE POSEE 
                                            COMO CLAVE LOS NOMBRES DE LOS PROYECTOS Y COMO VALOR UN STRING CON LOS NOMBRES
                                            DE LAS CATEGORIAS ASOCIADAS CON ESE PROYECTO SEPARADAS POR PUNTO Y COMA*/ ?>

                                            <?php foreach ($datosProyectos as $nombre_proyecto => $nombre_categoria) {?>
                                            <?php /* SE IMPRIME EL NOMBRE DEL PROYECTO*/ ?>
                                            <div class="row"><i><?php echo $nombre_proyecto;?></i>
                                            <div class="space-6"></div>

                                                <ul>
                                                    <?php /* SE OBTIENE TODOS LOS NOMBRES DE LAS CATEGORIAS ASOCIADAS AL PROYECTO
                                                    Y SE IMPRIMEN EN FORMA DE LISTA */ ?>
                                                    <?php $categoria =explode(';', $nombre_categoria); 

                                                        foreach ($categoria as $key => $nombre) {
                                                            echo '<li>'.$nombre;
                                                        }

                                                    ?>
                                                </ul>

                                            </div>
                                            <hr>
                                            <div class="space-6"></div>
                                                                                            
                                            <?php } ?>
                                            
                                                 

                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>
                <?php } ?>  

                <div class="row">

                    <div class="col-md-6">
                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/consejoEducativo"); ?>" id="btnRegresar">
                            <i class="icon-arrow-left"></i>
                                Volver
                        </a>

                        <?php $this->renderPartial('/_accionesSobrePlantel', array('plantel_id' => $model->plantel_id)) ?>                

                    </div>
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>