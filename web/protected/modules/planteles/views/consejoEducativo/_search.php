<?php
/* @var $this ConsejoEducativoController */
/* @var $model ConsejoEducativo */
/* @var $form CActiveForm */
?>
<div class="widget-main form">



<?php $form=$this->beginWidget('CActiveForm', array(
	//'action'=>Yii::app()->createUrl($this->route),
	//'method'=>'get',
	'id'=>'consejo-educativo-search',
)); ?>


	<div class="row col-md-12">
		<div class="col-md-2"><b>C&oacute;digo plantel</b></div>
                <div class="col-md-4">
                    <?php echo $form->textField($modelPlantel,'cod_plantel',array('size'=>10,'maxlength'=>10,'class'=>'span-7 cod_plantel')); ?>
                </div>


		<div class="col-md-2"><b>C&oacute;digo estad&iacute;stico</b></div>
                <div class="col-md-4">
                    <?php echo $form->textField($modelPlantel,'cod_estadistico',array('class'=>'span-7')); ?>
                </div>
	</div>

	<div class="row col-md-12">

		<div class="col-md-2"><b>Denominaci&oacute;n</b></div>
                <div class="col-md-4">
                    <?php
                    $groupId = Yii::app()->user->group;

                    if($groupId == UserGroups::MISION_RIBAS_NAC)
                    {
                        echo '<select id="Plantel_denominacion_id" name="Plantel[denominacion_id]" class="span-7">
                                    <option value="9">MISIÓN RIBAS</option>
                                </select>';
                    }
                    else
                    {
                        echo $form->dropDownList($modelPlantel, 'denominacion_id',
                                CHtml::listData(CDenominacion::getData(),'id', 'nombre'),
                                array('empty' => '-Seleccione-','class' => 'span-7')
                        );
                    }
                    ?>
                </div>

		<div class="col-md-2"><b>Nombre del plantel</b></div>
                <div class="col-md-4">
                    <?php echo $form->textField($modelPlantel,'nombre',array('size'=>60,'maxlength'=>150,'class'=>'span-7 plantel_nombre')); ?>
                </div>

	</div>

	<div class="row col-md-12">

		<div class="col-md-2"><b>Estado</b></div>
                <div class="col-md-4">
                    <?php
                    echo $form->dropDownList(
                        $modelPlantel, 'estado_id', CHtml::listData(CEstado::getData(), 'id', 'nombre'), array(
                        'ajax' => array(
                            'type' => 'GET',
                            'update' => '#municipio',
                            'url' => 'consejoEducativo/ObtenerMunicipios',
                        ),
                        'empty' => array('' => '-Seleccione-'), 'class' => 'span-7',$condicionEstado=>$condicionEstado)
                    );
                    ?>
                </div>


		<div class="col-md-2"><b>Municipio</b></div>
                <div class="col-md-4">
                    <?php
                    echo $form->dropDownList($modelPlantel, 'municipio_id', $municipios, array(
                        'empty' => '-Seleccione-',
                        'id' => 'municipio',
                        'class' => 'span-9',
                        'ajax' => array(
                            'type' => 'GET',
                            'update' => '#parroquia',
                            'url' => 'consejoEducativo/ObtenerParroquias',
                        ),
                        'empty' => array('' => '-Seleccione-'), 'class' => 'span-7')
                    );
                    ?>
                </div>

	</div>

	<div class="row col-md-12">

		<div class="col-md-2"><b>Parroquia</b></div>
                <div class="col-md-4">
                    <?php
                    echo $form->dropDownList($modelPlantel, 'parroquia_id', array(), array(
                        'empty' => '-Seleccione-',
                        'id' => 'parroquia',
                        'class' => 'span-9',
                        	/*'ajax' => array(
                            'type' => 'GET',
                            'update' => '#localidad',
                            'url' => CController::createUrl('seleccionarLocalidad'),
                        ),*/
                        'empty' => array('' => '-Seleccione-'), 'class' => 'span-7')
                    );
                    ?>
                </div>

		<div class="col-md-2"><b>Modalidad</b></div>
                <div class="col-md-4">
                    <?php
                        echo $form->dropDownList($modelPlantel, 'modalidad_id', 
                            CHtml::listData(CModalidad::getData(), 'id', 'nombre'),
                            array('empty' => '-Seleccione-','class' => 'span-7')
                        );
                    ?>
                </div>

	</div>

	<div class="row col-md-12">

		<div class="col-md-2"><b>Estatus del plantel</b></div>
                <div class="col-md-4">
                    <?php
                        echo $form->dropDownList($modelPlantel, 'estatus_plantel_id', 
                            CHtml::listData(CEstatusPlantel::getData(), 'id', 'nombre'),
                            array('empty' => '-Seleccione-','class' => 'span-7')
                        );
                    ?>
                </div>

		<div class="col-md-2"><b>Tipo de dependencia</b></div>
                <div class="col-md-4">
                    <?php
                        echo $form->dropDownList($modelPlantel, 'tipo_dependencia_id', 
                           CHtml::listData(CTipoDependencia::getData(), 'id', 'nombre'),
                           array('empty' => '-Seleccione-','class' => 'span-7')
                        );
                    ?>
                </div>
    </div>
    <div class="row col-md-12">
		<div class="col-md-2"><b>Cédula del Director</b></div>
                <div class="col-md-4">
                    <input type="text" name="Plantel[cedula_director]" class="span-7" id="cedulaIdentidadDirector" maxlength="10">
                </div>
        <div class="col-md-2"><b>Posee Poligonal definida</b></div>
        <div class="col-md-4">
            <select name="Plantel[poligonal]" id="Plantel_poligonal" class="span-7">
                <option value="">-Seleccione-</option>
                <option value="SI">SI</option>
                <option value="NO">NO</option>
            </select>
        </div>    

	</div>

	<div class="row col-md-12">
		
		<div class="col-md-2"><?php echo $form->label($model,'rif',array('style'=>'font-weight: bold'));?></div>
		
		<div class="col-md-4">
			<?php echo $form->textField($model,'rif',array('size'=>11, 'maxlength'=>11, 'class' => 'span-7 ConsejoEducativo_rif',)); ?>
		</div>

		<div class="col-md-2"><b>Actualizado</b></div>
		<div class="col-md-4">
            <?php echo $form->dropDownList($model, 'fecha_registro', array('A' => 'Actualizado', 'N' => 'No Actualizado'), array('prompt'=>'- - -', 'class' => 'span-12', )); ?>

		</div>
	</div>

	<div class="row col-md-12">
		
		<div class="col-md-2"><?php echo $form->label($model,'banco_id',array('style'=>'font-weight: bold')); ?></div>
		
		<div class="col-md-4">
			<?php echo $form->dropDownList($model, 'banco_id', CHtml::listData(Banco::model()->findAll(array('limit'=>50)), 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', )); ?>
		</div>
		
		<div class="col-md-2"><?php echo $form->label($model,'numero_cuenta',array('style'=>'font-weight: bold')); ?></div>
		
		<div class="col-md-4">
			<?php echo $form->textField($model,'numero_cuenta', array('class' => 'span-7',)); ?>
		</div>
	</div>

	<div class="row col-md-12">
		
		<div class="col-md-2"><?php echo $form->label($model,'comite_economia',array('style'=>'font-weight: bold')); ?></div>
		
		<div class="col-md-4">
            <?php echo $form->dropDownList($model, 'comite_economia', array('SI' => 'SI', 'NO' => 'NO'), array('prompt'=>'- - -', 'class' => 'span-12', )); ?>
        </div>
		
		<div class="col-md-2"><?php echo $form->label($model,'circuito_educativo',array('style'=>'font-weight: bold')); ?></div>
		
		<div class="col-md-4">
            <?php echo $form->dropDownList($model, 'circuito_educativo', array('SI' => 'SI', 'NO' => 'NO'), array('prompt'=>'- - -', 'class' => 'span-12', )); ?>
        </div>
	</div>


	<div class="row">
            <div class=" pull-right">
	            <div class="col-md-12" style="margin-right: 35px;">
	                <button class="btn btn-primary btn-next btn-sm" data-last="Finish" type="submit" id="buscarConsejo">
	                    Buscar
	                    <i class="fa fa-search icon-on-right"></i>
	                </button>
	            </div>
            </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->