<?php

/* @var $this ConsejoEducativoController */
/* @var $model ConsejoEducativo */

$this->breadcrumbs=array(
	'Consejo Educativos'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Consejo Educativos';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Consejo Educativos</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Consejo Educativos.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/planteles/consejoEducativo/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Consejo Educativos                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'consejo-educativo-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[id]', $model->id, array('title' => '',)),
        ),
        array(
            'header' => '<center>rif</center>',
            'name' => 'rif',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[rif]', $model->rif, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_registro</center>',
            'name' => 'fecha_registro',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[fecha_registro]', $model->fecha_registro, array('title' => '',)),
        ),
        array(
            'header' => '<center>comite_economia</center>',
            'name' => 'comite_economia',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[comite_economia]', $model->comite_economia, array('title' => '',)),
        ),
        array(
            'header' => '<center>circuito_educativo</center>',
            'name' => 'circuito_educativo',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[circuito_educativo]', $model->circuito_educativo, array('title' => '',)),
        ),
        array(
            'header' => '<center>peic</center>',
            'name' => 'peic',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[peic]', $model->peic, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>taquilla_id</center>',
            'name' => 'taquilla_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[taquilla_id]', $model->taquilla_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>banco_id</center>',
            'name' => 'banco_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[banco_id]', $model->banco_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>plantel_id</center>',
            'name' => 'plantel_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[plantel_id]', $model->plantel_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_ini_id</center>',
            'name' => 'usuario_ini_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_ini</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_act_id</center>',
            'name' => 'usuario_act_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_act</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[fecha_act]', $model->fecha_act, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_elim</center>',
            'name' => 'fecha_elim',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[fecha_elim]', $model->fecha_elim, array('title' => '',)),
        ),
        array(
            'header' => '<center>estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[estatus]', $model->estatus, array('title' => '',)),
        ),
        array(
            'header' => '<center>numero_cuenta</center>',
            'name' => 'numero_cuenta',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('ConsejoEducativo[numero_cuenta]', $model->numero_cuenta, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>