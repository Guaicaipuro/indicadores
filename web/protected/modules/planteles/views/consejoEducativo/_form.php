<?php
/* @var $this ConsejoEducativoController */
/* @var $model ConsejoEducativo */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">

                <div id="div-result">
                    <?php
                    if($model->hasErrors()):
                        $this->renderPartial('//errorSumMsg', array('model' => $model));
                    elseif(Yii::app()->user->hasFlash('success')):               $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => Yii::app()->user->getFlash('success')));
                    else:
                        ?>
                        <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>

                    <?php
                    endif;
                    ?>
                </div>

                <div id='dialogPantalla' class="hide ">
                    <div class="alertDialogBox">
                        <p class="note">Este plantel ya posee un Consejo Educativo.
                            ¿ Desea actualizar los datos del Consejo Educativo ? </p>
                    </div>

                </div>
                <div class="tab-pane active" id="datosGeneralesPlantel">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'consejo-educativo-plantel-form',
                            'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                            // Please note: When you enable ajax validation, make sure the corresponding
                            // controller action is handling ajax validation correctly.
                            // There is a call to performAjaxValidation() commented in generated controller code.
                            // See class documentation of CActiveForm for details on this.
                            'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-datos-generales-plantel">

                            <div class="widget-box collapsed">

                                <div class="widget-header">
                                    <h5>Datos Generales del Plantel</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-down"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            Código DEA
                                                            <?php echo CHtml::textField('codigo_plantel',(isset($modelPlantel->cod_plantel))? $modelPlantel->cod_plantel: "",array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Código DEA del Plantel')); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            Plantel
                                                            <?php echo CHtml::textField('plantel_nombre',(isset($modelPlantel->nombre))? $modelPlantel->nombre: "", array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Nombre del Plantel')); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            Estado
                                                            <?php echo CHtml::textField('estado_nombre',(is_object($modelPlantel->estado) && isset($modelPlantel->estado->nombre))? $modelPlantel->estado->nombre: "", array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Estado de ubicación del Plantel')); ?>
                                                        </div>



                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">


                                                        <div class="col-md-4">
                                                            Municipio
                                                            <?php echo CHtml::textField('municipio_nombre',(is_object($modelPlantel->municipio) && isset($modelPlantel->municipio->nombre))? $modelPlantel->municipio->nombre: "", array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Municipio de ubicación del Plantel')); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Parroquia
                                                            <?php echo CHtml::textField('parroquia_nombre',(is_object($modelPlantel->parroquia) && isset($modelPlantel->parroquia->nombre))? $modelPlantel->parroquia->nombre: "", array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Parroquia de ubicación del Plantel')); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Dirección
                                                            <?php echo CHtml::textArea('direccion',(isset($modelPlantel->direccion))? $modelPlantel->direccion: "", array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Dirección del Plantel')); ?>
                                                        </div>
                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            Dependencia
                                                            <?php echo CHtml::textField('dependencia_nombre',(is_object($modelPlantel->tipoDependencia) && isset($modelPlantel->tipoDependencia->nombre))? $modelPlantel->tipoDependencia->nombre: "", array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Dependencia del Plantel')); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Zona de ubicación
                                                            <?php echo CHtml::textField('zona_nombre',(is_object($modelPlantel->zonaUbicacion) && isset($modelPlantel->zonaUbicacion->nombre))? $modelPlantel->zonaUbicacion->nombre: "", array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Zona de ubicación del Plantel')); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Modalidad
                                                            <?php echo CHtml::textField('modalidad_nombre',(is_object($modelPlantel->modalidad) && isset($modelPlantel->modalidad->nombre))? $modelPlantel->modalidad->nombre: "", array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Modalidad de estudio del Plantel')); ?>
                                                        </div>
                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            Correo del Plantel
                                                            <?php echo CHtml::textField('plantel_correo',(isset($modelPlantel->correo))? $modelPlantel->correo: "", array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Correo del Plantel')); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Cédula del Director
                                                            <?php echo CHtml::textField('director_cedula',(is_object($modelPlantel->directorActual) && isset($modelPlantel->directorActual->cedula))? $modelPlantel->directorActual->cedula: "", array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Cédula del director del Plantel')); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            Nombres y Apellidos del Director
                                                            <?php echo CHtml::textField('director_nombre',(is_object($modelPlantel->directorActual) && isset($modelPlantel->directorActual->apellido) && isset($modelPlantel->directorActual->nombre) )? $modelPlantel->directorActual->nombre.' '.$modelPlantel->directorActual->apellido: "", array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Nombres y Apellidos del director del Plantel')); ?>
                                                        </div>
                                                    </div>


                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            Télefono del Director
                                                            <?php echo CHtml::textField('director_telefono',(is_object($modelPlantel->directorActual) && isset($modelPlantel->directorActual->telefono))? $modelPlantel->directorActual->telefono: "", array('class' => 'span-12', 'readonly'=>'readonly','title'=>'Teléfono del director del Plantel')); ?>
                                                        </div>

                                                        <div class="col-md-4">

                                                            <table class="table">
                                                                <caption style="text-align:center;">Matricula de Estudiantes</caption>
                                                                <tr>
                                                                    <th style="text-align:center;">
                                                                        Genero
                                                                    </th>
                                                                    <th style="text-align:center;">
                                                                        Cantidad
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Masculino
                                                                    </td>
                                                                    <td style="text-align:center;">
                                                                        <?php
                                                                        if($matriculaEstudiante["total"]==0) {echo 0;}
                                                                        else{ echo $matriculaEstudiante["m"];}
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Femenino
                                                                    </td>
                                                                    <td style="text-align:center;">
                                                                        <?php
                                                                        if($matriculaEstudiante["total"]==0) {echo 0;}
                                                                        else{ echo $matriculaEstudiante["f"];}
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Sin Identificar
                                                                    </td>
                                                                    <td style="text-align:center;">
                                                                        <?php
                                                                        if($matriculaEstudiante["total"]==0) {echo 0;}
                                                                        else{ echo $matriculaEstudiante["sin_sexo"];}
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <tr class="danger">
                                                                    <td>
                                                                        Total alumnos
                                                                    </td>
                                                                    <td style="text-align:center;">
                                                                        <?php echo $matriculaEstudiante["total"]; ?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                    <div class="form">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'consejo-educativo-form',
                            'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                            // Please note: When you enable ajax validation, make sure the corresponding
                            // controller action is handling ajax validation correctly.
                            // There is a call to performAjaxValidation() commented in generated controller code.
                            // See class documentation of CActiveForm for details on this.
                            'enableAjaxValidation'=>false,
                        )); ?>



                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales del Consejo Educativo</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'rif'); ?>
                                                            <?php echo $form->textField($model,'rif',array('size'=>11, 'maxlength'=>11, 'class' => 'span-12', "required"=>"required",'placeholder'=>'J-999999999','title'=>'RIF del Consejo Educativo','style'=>'text-transform: uppercase;')); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_registro'); ?>
                                                            <?php echo $form->textField($model,'fecha_registro', array('class' => 'span-12',"required"=>"required" ,'readonly'=>'readonly','title'=>'Última fecha de registro en Taquilla Única')); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'banco_id'); ?>
                                                            <?php echo $form->dropDownList($model, 'banco_id', $bancos, array('prompt'=>'- SELECCIONE -', 'class' => 'span-12', "required"=>"required",'title'=>'Seleccione un Banco')); ?>
                                                        </div>
                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'numero_cuenta'); ?>
                                                            <?php echo $form->textField($model,'numero_cuenta', array('class' => 'span-12','placeholder'=>'99999999999999999999 ',"required"=>"required",'title'=>'Número de Cuenta del Banco')); ?>

                                                        </div>

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'circuito_educativo'); ?>
                                                            <?php echo $form->radioButtonList($model,'circuito_educativo',array('SI'=>'SI', 'NO'=>'NO'),array("required"=>"required",'title'=>'¿Pertenece a un Circuito Educativo ?','labelOptions'=>array('style'=>'display:inline'), 'separator'=>"\t",)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'comite_economia'); ?>
                                                            <?php echo $form->radioButtonList($model,'comite_economia',array('SI'=>'SI', 'NO'=>'NO'),array("required"=>"required",'title'=>'¿Posee Circuito Educativo ?','labelOptions'=>array('style'=>'display:inline'), 'separator'=>"\t",)); ?>
                                                        </div>
                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="peic">
                                                            <?php echo $form->labelEx($model,'peic'); ?>
                                                            <?php echo $form->textArea($model,'peic',array('cols'=>50,'rows'=>4, 'maxlength'=>160, 'class' => 'span-12','placeholder'=>'Descripción del Proyecto Educativo Integral Comunitario','title'=>'Descripción del Proyecto Educativo Integral Comunitario','style'=>'text-transform: uppercase;' )); ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <?php if($formType == 'registro') {?>

                                                            <div class="col-md-4">
                                                                <?php echo $form->textField($model,'plantel_id',array('size'=>11, 'maxlength'=>11, 'class' => 'span-12 hide', "required"=>"required",'placeholder'=>'','readonly'=>'readonly',   'value'=>$_GET['p_id'])); ?>
                                                            </div>

                                                        <?php }else if($formType == 'edicion') {?>

                                                            <div class="col-md-4">
                                                                <?php echo $form->textField($model,'plantel_id',array('size'=>11, 'maxlength'=>11, 'class' => 'span-12 hide', "required"=>"required",'placeholder'=>'','readonly'=>'readonly',   'value'=>base64_encode($model->plantel_id))); ?>
                                                            </div>

                                                        <?php } ?>

                                                        <?php if(isset($model->id)){ ?>
                                                            <div class="col-md-4 ">
                                                                <?php echo $form->textField($model,'id',array('class' => 'span-12 hide','value'=>base64_encode($model->id))); ?>
                                                            </div>
                                                        <?php } ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>

                    <?php if($formType=='edicion'){
                        $this->renderPartial('taquillaForm',(array('model'=>$modelTaquilla, 'formType'=>$formType,
                            'estados'=>$estados,
                            'municipios'=>$municipios,
                            'parroquias'=>$parroquias,
                            'poblacion'=>$poblacion,
                            'urbanizacion'=>$urbanizacion,
                            'tipoVia'=>$tipoVia,)));
                    }else{
                        ?>
                        <?php $this->renderPartial('taquillaForm',(array('model'=>$modelTaquilla, 'formType'=>$formType,'estados'=>$estados))); }?>



                    <?php // echo $form->radioButtonList($key,$value,array('SI'=>'SI', 'NO'=>'NO'),array("required"=>"required",'labelOptions'=>array('style'=>'display:inline'), 'separator'=>"",)); ?>
                    <div class="form">

                        <?php /* $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'proyecto-consejo-form',
                                //'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); */?>
                        <div id="div-proyecto-consejo">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5> Proyectos del Consejo Educativo</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">

                                                    <?php  foreach ($proyectos as $key => $nombre) {
                                                        ?>

                                                        <div class="row">

                                                            <b><i><?php echo $nombre; ?></b></i>

                                                            <?php //echo CHtml::radioButtonList('Proyectos['.$key.']','Proyectos['.$key.']',array('SI'=>'SI', 'NO'=>'NO'),array('labelOptions'=>array('style'=>'display:inline'), 'separator'=>"\t",'class'=>'clase-proyectos')); ?>

                                                            <div class="space-6"></div>
                                                            <div  class=' categorias-<?php echo "Proyectos[".$key."]"; ?>A'>

                                                                <?php
                                                                $lista_categorias = array();
                                                                $selected = array();

                                                                // Se recorre la lista completa de categorias activas
                                                                foreach ($categorias as $proyecto_id => $valor) {


                                                                    //Si los id de los proyectos coinciden se guarda en la variable lista_categorias
                                                                    // que representa las categorias de un proyecto especifico
                                                                    if($proyecto_id==$key){

                                                                        $lista_categorias = $valor;

                                                                        if($formType=='edicion'){


                                                                            foreach ($lista_categorias as $iden => $lista_valor) {

                                                                                if(in_array($iden, $categorias_seleccionadas)){

                                                                                    $selected[] = $iden;
                                                                                }

                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                // Se muestran las categorias por cada proyecto en forma de checkbox
                                                                echo CHtml::checkBoxList("Categorias[$key][]", $selected,$lista_categorias,array(
                                                                    "style" => "width: 10px;",
                                                                    "class"=> "checkbox-$key"
                                                                ));?>
                                                            </div>
                                                            <div class="space-6"></div>
                                                            <div class="space-6"></div>
                                                        </div>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php /* $this->endWidget(); */?>

                </div>
            </div>

            <div class="space-6"></div>
            <div class="row">

                <div class="col-md-6">
                    <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/"); ?>" id="btnRegresar">
                        <i class="icon-arrow-left"></i>
                        Volver
                    </a>
                    <?php if($formType=='edicion'){ ?>
                        <div class="btn-group dropup">
                            <button style="height:42px;" class="btn dropdown-toggle" data-toggle="dropdown">
                                Acciones
                                <span class="icon-caret-up icon-on-right"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-yellow pull-right">
                                <?php
                                $columna ='';
                                if( Yii::app()->user->pbac('consejoEducativo.consulta.read') or  Yii::app()->user->pbac('consejoEducativo.consulta.admin')){

                                    $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Consultar Consejo Educativo</span>", Yii::app()->createUrl("/planteles/consejoEducativo/consulta/id/". base64_encode($model->id)), array("class" => "fa fa-search-plus blue", "title" => "Consultar Consejo Educativo")) . '</li>';
                                    $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Imprimir Datos del Consejo Educativo</span>", Yii::app()->createUrl("/planteles/consejoEducativo/VerReporte/id/" . base64_encode($model->id)), array("class" => "fa icon-file orange", "title" => "Imprimir datos del Consejo Educativo","target"=>"_blank")) . '</li>';
                                }
                                echo $columna;
                                ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>

                <div class="col-md-6 wizard-actions">
                    <button  id='button-form' class="btn btn-primary btn-next" data-last="Finish" type="submit" >
                        Guardar
                        <i class="icon-save icon-on-right"></i>
                    </button>
                </div>

            </div>
        </div><!-- form -->

    </div>
</div>

<div id="resultDialog" class="hide"></div>

<?php
/**
 * Yii::app()->clientScript->registerScriptFile(
 *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/ConsejoEducativoController/consejo-educativo/form.js',CClientScript::POS_END
 *);
 */
?>
<?php

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/public/js/modules/plantel/consejoEducativo/form.js',CClientScript::POS_END
);

?>
<?php

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js',CClientScript::POS_END
);

?>

