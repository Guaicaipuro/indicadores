<?php
$this->breadcrumbs = array(
    'Estudiantes por Inscribir' => array('/planteles/inscripcionManual/index/id/' . base64_encode($plantel_id)),
    'Datos de Estudiantes',
);
$model->fecha_nacimiento = date("d-m-Y", strtotime($model->fecha_nacimiento));
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Ver Datos Generales</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">

                    <div class = "widget-box">
                        <div class = "widget-header" style="border-width: thin;">
                            <h5>Ver Datos</h5>
                            <div class="widget-toolbar">
                                <a data-action="collapse" href="#">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class = "widget-body">
                            <div style = "display: block;" class = "widget-body-inner">
                                <div class = "widget-main">
                                    <?php
                                    $form = $this->beginWidget('CActiveForm', array(
                                        'id' => 'inscripcionManual-form',
                                        'enableAjaxValidation' => false,
                                    ));
                                    ?>

                                    <div class="row">

                                        <h5 style="color: #c43030">Datos del Plantel</h5>
                                        <hr>

                                        <div class="col-md-12">
                                            <input name="InscripcionManual[id]" type="hidden" value="<?php echo base64_encode($id); ?>">
                                            <div id="cod_plantel" class="col-md-4">
                                                <?php echo CHtml::label('Código del Plantel', ''); ?>
                                                <div class="autocomplete-w1">
                                                    <?php echo $form->textField($model, 'cod_plantel', array('maxlength' => 15, 'placeholder' => 'Introduzca el código del plantel', 'class' => 'span-12', 'disabled' => true, 'required' => 'required', 'id' => 'query', 'onkeyup' => 'makeUpper("#query");')); ?>
                                                    <div id="log" style="height: 200px; width: 300px; overflow: auto;" class="ui-widget-content"
                                                         hidden="hidden"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo CHtml::label('Plantel', ''); ?>
                                                <?php echo $form->textField($model, 'plantel', array('maxlength' => 150, 'class' => 'span-12', 'disabled' => true, 'required')); ?>
                                                <input name="InscripcionManual[plantel_id]" type="hidden" value="<?php echo base64_encode($plantel_id); ?>">
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo CHtml::label('Estado', ''); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'estado_id', CHtml::listData($estado, 'id', 'nombre'), array(
                                                    'prompt' => '-SELECCIONE-',
                                                    'maxlength' => 2,
                                                    'class' => 'span-12', 'disabled' => true,
                                                    'disabled' => true));
                                                ?>
                                            </div>

                                        </div>

                                        <div class = "col-md-12"><div class = "space-6"></div></div>

                                        <h5 style="color: #c43030">Datos de Estudiantes</h5>
                                        <hr>

                                        <div class = "col-md-12">

                                            <div class="col-md-4">
                                                <?php echo CHtml::label('Origen', ''); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'origen', array('V' => 'Venezolano', 'E' => 'Extranjero', 'P' => 'Pasaporte', 'C' => 'Cédula Escolar'), array('prompt' => '-SELECCIONE-', 'size' => 1, 'maxlength' => 1, 'class' => 'span-12', 'disabled' => true, 'required'));
                                                ?>
                                            </div>

                                            <div class="col-md-4">
                                                <?php echo CHtml::label('Documento Identidad', ''); ?>
                                                <?php echo $form->textField($model, 'documento_identidad', array('size' => 20, 'maxlength' => 20, 'class' => 'span-12', 'disabled' => true, 'required')); ?>
                                            </div>

                                            <div class="col-md-4">
                                                <?php echo CHtml::label('Sexo', ''); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'sexo', array('F' => 'Femenino', 'M' => 'Masculino'), array('prompt' => '-SELECCIONE-', 'size' => 1, 'maxlength' => 1, 'class' => 'span-12', 'disabled' => true, 'required'));
                                                ?>
                                            </div>

                                        </div>

                                        <div class = "col-md-12">

                                            <div class="col-md-4" >
                                                <?php echo CHtml::label('Nombres', ''); ?>
                                                <?php echo $form->textField($model, 'nombres', array('size' => 120, 'maxlength' => 120, 'class' => 'span-12', 'disabled' => true, 'required')); ?>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo CHtml::label('Apellidos', ''); ?>
                                                <?php echo $form->textField($model, 'apellidos', array('size' => 120, 'maxlength' => 120, 'class' => 'span-12', 'disabled' => true, 'required')); ?>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo CHtml::label('Fecha Nacimiento', ''); ?>
                                                <?php echo $form->textField($model, 'fecha_nacimiento', array('maxlength' => 10, 'class' => 'span-12', 'disabled' => true, 'required')); ?>
                                            </div>

                                        </div>

                                        <div class = "col-md-12"><div class = "space-6"></div></div>

                                        <h5 style="color: #c43030">Datos Académicos</h5>
                                        <hr>

                                        <div class="col-md-12">

                                            <div class="col-md-4" >
                                                <?php echo CHtml::label('Grado', ''); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'grado_id', CHtml::listData($grado, 'id', 'nombre'), array(
                                                    'prompt' => '-SELECCIONE-',
                                                    'maxlength' => 2,
                                                    'class' => 'span-12',
                                                    'disabled' => true));
                                                ?>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo CHtml::label('Sección', ''); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'seccion_id', CHtml::listData($seccion, 'id', 'nombre'), array(
                                                    'prompt' => '-SELECCIONE-',
                                                    'maxlength' => 2,
                                                    'class' => 'span-12',
                                                    'disabled' => true));
                                                ?>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo CHtml::label('Turno', ''); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'turno_id', CHtml::listData($turno, 'id', 'nombre'), array(
                                                    'prompt' => '-SELECCIONE-',
                                                    'maxlength' => 2,
                                                    'class' => 'span-12',
                                                    'disabled' => true));
                                                ?>
                                            </div>

                                        </div>

                                        <div class = "col-md-12"><div class = "space-6"></div></div>
                                        
                                        <div class="col-md-12">
                                            <div class="col-md-4"> 
                                              <?php echo CHtml::label('Estudiante Repitiente', ''); ?>
                                              <?php echo CHtml::textField('repitiente_est', (isset($model['repitiente_est']) && $model['repitiente_est'] == 0)? 'NO':'SI', array('maxlength' => 10, 'class' => 'span-12', 'disabled' => true, 'required')); ?>
                                            </div>
                                            <div class = "col-md-4"><div class = "space-6"></div></div>
                                            <div class = "col-md-4"><div class = "space-6"></div></div>
                                        </div>
                                        
                                        <div class = "col-md-12"><div class = "space-6"></div></div>

                                        <h5 style="color: #c43030">Datos del Representante</h5>
                                        <hr>

                                        <div class = "col-md-12">

                                            <div class="col-md-4">
                                                <?php echo CHtml::label('Origen', ''); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'origen_rep', array('V' => 'Venezolano', 'E' => 'Extranjero', 'P' => 'Pasaporte'), array('prompt' => '-SELECCIONE-', 'size' => 1, 'maxlength' => 1, 'class' => 'span-12 data-identidad', 'disabled' => true, 'required'));
                                                ?>
                                            </div>

                                            <div class="col-md-4">
                                                <?php echo CHtml::label('Documento Identidad', ''); ?>
                                                <?php echo $form->textField($model, 'documento_identidad_rep', array('size' => 20, 'maxlength' => 20, 'class' => 'span-12 data-identidad', 'disabled' => true, 'required')); ?>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo CHtml::label('Nombres', ''); ?>
                                                <?php echo $form->textField($model, 'nombres_rep', array('size' => 120, 'maxlength' => 120, 'class' => 'span-12', 'disabled' => true, 'required')); ?>
                                            </div>

                                        </div>

                                        <div class = "col-md-12">

                                            <div class="col-md-4" >
                                                <?php echo CHtml::label('Apellidos', ''); ?>
                                                <?php echo $form->textField($model, 'apellidos_rep', array('size' => 120, 'maxlength' => 120, 'class' => 'span-12', 'disabled' => true, 'required')); ?>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo CHtml::label('Afinidad', ''); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'afinidad_id', CHtml::listData($afinidad, 'id', 'nombre'), array(
                                                    'prompt' => '-SELECCIONE-',
                                                    'maxlength' => 2,
                                                    'class' => 'span-12',
                                                    'disabled' => true));
                                                ?>
                                            </div>

                                        </div>

                                        <div class = "col-md-12"><div class = "space-6"></div></div>


                                        <h5 style="color: #c43030">Estatus de Matriculación</h5>
                                        <hr>
                                        <div class="col-md-12">
                                            <div class="col-md-4" >
                                                <?php echo CHtml::label('Estatus', ''); ?>
                                                <?php echo CHtml::textField('', (isset($nombre) && $nombre != '') ? $nombre : "", array('maxlength' => 150, 'class' => 'span-12', 'disabled' => true, 'required')); ?>
                                            </div>
                                        </div>

                                        <div class = "col-md-12"><div class = "space-6"></div></div>

                                    </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <br>

            <div id="botones" class="row">

                <div class="col-md-6 pull-left">
                    <a id="btnRegresar" href="<?php echo Yii::app()->createUrl('/planteles/inscripcionManual/index/id/' . base64_encode($plantel_id)); ?>" class="btn btn-danger">
                        <i class="icon-arrow-left"></i>
                        Volver
                    </a>
                </div>
            </div>

        </div>
    </div>