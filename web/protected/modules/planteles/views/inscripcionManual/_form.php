<?php
$this->breadcrumbs = array(
    'Estudiantes por Matricular' => array('/planteles/inscripcionManual/index/id/' . base64_encode($plantel_id)),
    'Datos de Estudiantes',
);
$model->fecha_nacimiento = date("d-m-Y", strtotime($model->fecha_nacimiento));
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Modificar Datos Generales</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class = "widget-box">

                        <div id="info" class="infoDialogBox">
                            <p class="note">
                                <b>Por Favor Ingrese los datos correspondientes, Los campos marcados con <b><span class="required">*</span></b> son requeridos.</b>
                                <br>En el siguiente formulario los datos a editar son los siguientes:<br>
                            <ul>
                                <li>Datos del Plantel <i class="icon-hand-right"></i> <b>Solo el Código del Plantel.</b></li>
                                <li>Datos de Estudiantes.</li>
                                <li>Datos Académicos.</li>
                                <li>Datos del Representante <i class="icon-hand-right"></i> <b>Se requiere cuando es un estudiante de inicial que no este registrado en sistema.</b></li>
                            </ul>
                            </p>
                        </div>

                        <div id="error" class="errorDialogBox hide">
                            <p></p>
                        </div>
                        <div id="exitoso" class="successDialogBox hide">
                            <p></p>
                        </div>

                        <div class = "widget-header" style="border-width: thin;">

                            <h5>Modificar Datos</h5>
                            <div class="widget-toolbar">
                                <a data-action="collapse" href="#">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </div>

                        </div>

                        <div class = "widget-body">
                            <div style = "display: block;" class = "widget-body-inner">
                                <div class = "widget-main">
                                    <?php
                                    $form = $this->beginWidget('CActiveForm', array(
                                        'id' => 'inscripcionManual-form',
                                        'enableAjaxValidation' => false,
                                    ));
                                    ?>

                                    <div class="row">

                                        <h5 style="color: #c43030">Datos del Plantel</h5>
                                        <hr>

                                        <div class="col-md-12">
                                            <input name="InscripcionManual[id]" type="hidden" value="<?php echo base64_encode($id); ?>">
                                            <div id="cod_plantel" class="col-md-4">
                                                <?php echo $form->labelEx($model, 'cod_plantel'); ?>
                                                <div class="autocomplete-w1">
                                                    <?php echo $form->textField($model, 'cod_plantel', array('maxlength' => 15, 'placeholder' => 'Introduzca el código del plantel', 'class' => 'span-12', 'required' => 'required', 'id' => 'query', 'onkeyup' => 'makeUpper("#query");')); ?>
                                                    <div id="log" style="height: 200px; width: 300px; overflow: auto;" class="ui-widget-content"
                                                         hidden="hidden"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo $form->labelEx($model, 'plantel'); ?>
                                                <?php echo $form->textField($model, 'plantel', array('maxlength' => 150, 'class' => 'span-12', 'required', 'readonly' => true)); ?>
                                                <input name="InscripcionManual[plantel_id]" type="hidden" value="<?php echo base64_encode($plantel_id); ?>">
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo $form->labelEx($model, 'estado_id'); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'estado_id', CHtml::listData($estado, 'id', 'nombre'), array(
                                                    'prompt' => '-SELECCIONE-',
                                                    'maxlength' => 2,
                                                    'class' => 'span-12',
                                                    'disabled' => true));
                                                ?>
                                            </div>

                                        </div>

                                        <div class = "col-md-12"><div class = "space-6"></div></div>

                                        <h5 style="color: #c43030">Datos de Estudiantes</h5>
                                        <hr>

                                        <div class = "col-md-12">

                                            <div class="col-md-4">
                                                <?php echo $form->labelEx($model, 'origen'); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'origen', array('V' => 'Venezolano', 'E' => 'Extranjero', 'P' => 'Pasaporte', 'C' => 'Cédula Escolar'), array('prompt' => '-SELECCIONE-', 'size' => 1, 'maxlength' => 1, 'class' => 'span-12', 'required'));
                                                ?>
                                            </div>

                                            <div class="col-md-4">
                                                <?php echo $form->labelEx($model, 'documento_identidad'); ?>
                                                <?php echo $form->textField($model, 'documento_identidad', array('size' => 20, 'maxlength' => 20, 'class' => 'span-12', 'required')); ?>
                                            </div>

                                            <div class="col-md-4">
                                                <?php echo $form->labelEx($model, 'sexo'); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'sexo', array('F' => 'Femenino', 'M' => 'Masculino'), array('prompt' => '-SELECCIONE-', 'size' => 1, 'maxlength' => 1, 'class' => 'span-12', 'required'));
                                                ?>
                                            </div>

                                        </div>

                                        <div class = "col-md-12">

                                            <div class="col-md-4" >
                                                <?php echo $form->labelEx($model, 'nombres'); ?>
                                                <?php echo $form->textField($model, 'nombres', array('size' => 120, 'maxlength' => 120, 'class' => 'span-12', 'required')); ?>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo $form->labelEx($model, 'apellidos'); ?>
                                                <?php echo $form->textField($model, 'apellidos', array('size' => 120, 'maxlength' => 120, 'class' => 'span-12', 'required')); ?>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo $form->labelEx($model, 'fecha_nacimiento'); ?>
                                                <?php echo $form->textField($model, 'fecha_nacimiento', array('maxlength' => 10, 'class' => 'span-12', 'required')); ?>
                                            </div>

                                        </div>

                                        <div class = "col-md-12"><div class = "space-6"></div></div>

                                        <h5 style="color: #c43030">Datos Académicos</h5>
                                        <hr>

                                        <div class="col-md-12">

                                            <div class="col-md-4" >
                                                <?php echo $form->labelEx($model, 'grado_id'); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'grado_id', CHtml::listData($grado, 'id', 'nombre'), array(
                                                    'prompt' => '-SELECCIONE-',
                                                    'maxlength' => 2,
                                                    'class' => 'span-12'));
                                                ?>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo $form->labelEx($model, 'seccion_id'); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'seccion_id', CHtml::listData($seccion, 'id', 'nombre'), array(
                                                    'prompt' => '-SELECCIONE-',
                                                    'maxlength' => 2,
                                                    'class' => 'span-12'));
                                                ?>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo $form->labelEx($model, 'turno_id'); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'turno_id', CHtml::listData($turno, 'id', 'nombre'), array(
                                                    'prompt' => '-SELECCIONE-',
                                                    'maxlength' => 2,
                                                    'class' => 'span-12'));
                                                ?>
                                            </div>

                                        </div>
                                        
                                        <div class = "col-md-12"><div class = "space-6"></div></div>
                                        
                                        <div class="col-md-12">
                                            <div  class="col-md-12">
                                            <?php echo CHtml::label('Estudiante Repitiente', '', array('class' => 'col-md-2')); ?>
                                            <div class="col-md-1">
                                                <?php echo CHtml::checkBox('InscripcionManual[repitiente_est]',false, array('class' => 'span-7', 'id'=>'repitiente_est')); ?>
                                            </div>
                                          </div>
                                        </div>

                                        <div class = "col-md-12"><div class = "space-6"></div></div>

                                        <h5 style="color: #c43030">Datos del Representante</h5>
                                        <hr>

                                        <div class = "col-md-12">

                                            <div class="col-md-4">
                                                <?php echo $form->labelEx($model, 'origen_rep'); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'origen_rep', array('V' => 'Venezolano', 'E' => 'Extranjero', 'P' => 'Pasaporte'), array('prompt' => '-SELECCIONE-', 'size' => 1, 'maxlength' => 1, 'class' => 'span-12 data-identidad', 'required'));
                                                ?>
                                            </div>

                                            <div class="col-md-4">
                                                <?php echo $form->labelEx($model, 'documento_identidad_rep'); ?>
                                                <?php echo $form->textField($model, 'documento_identidad_rep', array('size' => 20, 'maxlength' => 20, 'class' => 'span-12 data-identidad', 'required')); ?>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo $form->labelEx($model, 'nombres_rep'); ?>
                                                <?php echo $form->textField($model, 'nombres_rep', array('size' => 120, 'maxlength' => 120, 'class' => 'span-12', 'required')); ?>
                                            </div>

                                        </div>

                                        <div class = "col-md-12">

                                            <div class="col-md-4" >
                                                <?php echo $form->labelEx($model, 'apellidos_rep'); ?>
                                                <?php echo $form->textField($model, 'apellidos_rep', array('size' => 120, 'maxlength' => 120, 'class' => 'span-12', 'required')); ?>
                                            </div>

                                            <div class="col-md-4" >
                                                <?php echo $form->labelEx($model, 'afinidad_id'); ?>
                                                <?php
                                                echo $form->dropDownList($model, 'afinidad_id', CHtml::listData($afinidad, 'id', 'nombre'), array(
                                                    'prompt' => '-SELECCIONE-',
                                                    'maxlength' => 2,
                                                    'class' => 'span-12'));
                                                ?>
                                            </div>

                                        </div>

                                        <div class = "col-md-12"><div class = "space-6"></div></div>

                                    </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <br>

            <div id="botones" class="row">

                <div class="col-md-6 pull-left">
                    <a id="btnRegresar" href="<?php echo Yii::app()->createUrl('/planteles/inscripcionManual/index/id/' . base64_encode($plantel_id)); ?>" class="btn btn-danger">
                        <i class="icon-arrow-left"></i>
                        Volver
                    </a>
                </div>

                <div id="btnGuardar" class="col-md-6 wizard-actions">
                    <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                        Guardar
                        <i class="icon-save"></i>
                    </button>
                </div>

            </div>

        </div>
    </div>

    <div id="dialog_error" class="hide">
    </div>
    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
    <script>
        $(document).ready(function() {

            $("#repitiente_est").unbind('click');
                $("#repitiente_est").click(function() {
                   var select = $('#repitiente_est').is(':checked')? true:false;
                   if(select){
                       $('#repitiente_est').val(1);
                   }else{
                       $('#repitiente_est').val(0);
                   }
               });

            function log(message) {
                var divResult = '';
                var nombre;
                var estado;
                var urlDir = '/planteles/inscripcionManual/buscarNombrePlantel';
                var datos = {codigo: message};
                var loadingEfect = false;
                var showResult = false;
                var method = 'POST';
                var responseFormat = 'json';
                var beforeSendCallback = function() {
                };
                var successCallback;
                var errorCallback = function() {
                };


                $("<div>").text(message).prependTo("#log");
                $("#log").scrollTop(0);
                successCallback = function(response) {

                    if (response.statusCode == 'success') {
                        nombre = response.nombre;
                        estado = response.estado;
                        $('#InscripcionManual_plantel').val(nombre);
                        $('#InscripcionManual_estado_id').val(estado);
                    }
                };
                executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
            }

            $("#query").autocomplete({
                source: "/planteles/inscripcionManual/autoComplete",
                minLength: 1,
                select: function(event, ui) {
                    $('#query').val(ui.item.value);
                    log(ui.item.value);
                }
            });


            $('#btnGuardar').unbind('click');
            $('#btnGuardar').click(function() {
                $('#InscripcionManual_estado_id').removeAttr('disabled', 'disabled');
                var title;
                var mensaje;
                var divResult = '';
                var urlDir = '/planteles/inscripcionManual/modificar';
                var datos = $('#inscripcionManual-form').serialize();
                var loadingEfect = false;
                var showResult = false;
                var method = 'POST';
                var responseFormat = 'json';
                var beforeSendCallback = function() {
                    $('#InscripcionManual_estado_id').removeAttr('disabled', 'disabled');
                };
                var successCallback;
                var errorCallback = function() {
                };
                successCallback = function(response) {
                    Loading.show();
                    if (response.statusCode == 'success') {
                        mensaje = response.mensaje;
                        $('#error').addClass('hide');
                        $('#error p').html('');
                        $('#exitoso').removeClass('hide');
                        $('#exitoso p').html(mensaje);
                        $('#info').addClass('hide');
                        $('#InscripcionManual_estado_id').attr('disabled', 'disabled');
                        scrollUp();
                        Loading.hide();
                    }
                    if (response.statusCode == 'error') {
                        Loading.hide();
                        mensaje = response.mensaje;
                        $('#exitoso').addClass('hide');
                        $('#exitoso p').html('');
                        $('#error').removeClass('hide');
                        $('#error p').html(mensaje);
                        $('#info').addClass('hide');
                        $('#InscripcionManual_estado_id').attr('disabled', 'disabled');
                        scrollUp();
                        Loading.hide();
                    }
                };
                Loading.show();
                executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);


            });



            $('.data-identidad').unbind('blur');
            $('.data-identidad').bind('blur', function() {
                var tdocumento_identidad = $('#InscripcionManual_origen_rep').val();
                var documento_identidad = $('#InscripcionManual_documento_identidad_rep').val();
                var title;
                var mensaje;
                var divResult = '';
                var urlDir = '/planteles/inscripcionManual/obtenerDatosPersona';
                var datos;
                var loadingEfect = false;
                var showResult = false;
                var method = 'GET';
                var responseFormat = 'json';
                var beforeSendCallback = function() {
                };
                var successCallback;
                var errorCallback = function() {
                };
                if (documento_identidad != null && documento_identidad != '') {
                    if (tdocumento_identidad != null && tdocumento_identidad != '' && tdocumento_identidad != 'C') {
                        datos = {
                            documento_identidad: documento_identidad,
                            tdocumento_identidad: tdocumento_identidad,
                        }
                        successCallback = function(response) {
                            Loading.show();
                            if (response.statusCode == 'SUCCESS') {
                                $('#InscripcionManual_nombres_rep').val(response.nombres);
                                $('#InscripcionManual_apellidos_rep').val(response.apellidos);
                                Loading.hide();
                            }
                            if (response.statusCode == 'ERROR') {
                                Loading.hide();
                                $('#InscripcionManual_origen_rep').val('');
                                $('#InscripcionManual_documento_identidad_rep').val('');
                                $('#InscripcionManual_nombres_rep').val('');
                                $('#InscripcionManual_apellidos_rep').val('');
                                dialogo_error(response.mensaje, response.title);
                                Loading.hide();
                            }
                        };
                        Loading.show();
                        executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
                    }
                }
            });


        });

        function dialogo_error(mensaje, title, redireccionar) {
            displayDialogBox('dialog_error', 'info', mensaje);
            //$("#dialog_error p").html(mensaje);
            var dialog = $("#dialog_error").removeClass('hide').dialog({
                modal: true,
                width: '450px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                        "class": "btn btn-xs",
                        click: function() {
                            $(this).dialog("close");
                            if (redireccionar) {
                                window.location.reload();
                            }
                        }
                    }
                ]
            });
        }
    </script>

