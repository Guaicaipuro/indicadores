<?php
$this->breadcrumbs = array(
    'Planteles' => array('/planteles/consultar'),
    'Datos de Estudiantes',
);
?>

<div class="tab-pane active" id="inscripcion">

    <div id="info_index" class="infoDialogBox">
        <p class="note">
            <b>Por Favor siga los siguientes pasos:</b>
            <br>
        <ul>
            <li><b>Ingrese en </b> <i class="icon-pencil green"></i> <b>y modifiqué los datos que esten erróneos o que hagan falta llenar.</b></li>
            <li><b>Presione </b> <i class="icon-check red"></i> <b> y luego en continuar si esta seguro que todos los datos estan correctos para ser matriculados.</b></li>
        </ul>
        </p>
        <p>
            <b>Nota:</b> Si luego de presionar <i class="icon-check red"></i> el <b>Estatus Inscripción</b> muestra "Listo para Matricular" contacte al administrador del sistema enviando un correo a <b>soporte_gescolar@me.gob.ve</b>
        </p>
    </div>


    <div class="widget-box">

        <div class = "widget-header">
            <h5>Estudiantes por Matricular</h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-up"></i>
                </a>
            </div>

        </div>
        <div id="_formS" class="widget-body">
            <div style="display:block;" class="widget-body-inner">
                <div class="widget-main form">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            $this->widget('zii.widgets.grid.CGridView', array(
                                'id' => 'inscripcion-grid',
                                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                'summaryText' => 'Mostrando {start}-{end} de {count}',
                                'dataProvider' => $model->search(),
                                'filter' => $model,
                                'afterAjaxUpdate' => "function(){
                                        $('.validarEstudiante').unbind('click');
                                        $('.validarEstudiante').click(function() {
                                                    var id = $(this).attr('id');
                                                    var plantel_id = $(this).attr('plantel_id');
                                                    validarEstudiante(id, plantel_id);
                                         });
                                }",
                                'columns' => array(
                                    array(
                                        'header' => '<center>Nombres</center>',
                                        'name' => 'nombres',
                                        'filter' => CHtml::textField('InscripcionManual[nombres]', '', array('maxlength' => 120, 'id' => 'nombres')),
                                    ),
                                    array(
                                        'header' => '<center>Apellidos</center>',
                                        'name' => 'apellidos',
                                        'filter' => CHtml::textField('InscripcionManual[apellidos]', '', array('maxlength' => 120, 'id' => 'apellidos')),
                                    ),
                                    array(
                                        'header' => '<center>Documento Identidad</center>',
                                        'name' => 'documento_identidad',
                                        'filter' => CHtml::textField('InscripcionManual[documento_identidad]', '', array('maxlength' => 20, 'id' => 'documento_identidad')),
                                    ),
                                    array(
                                        'header' => '<center>Sexo</center>',
                                        'type' => 'raw',
                                        'value' => array($this, 'columnaSexo'),
                                        'name' => 'sexo',
                                        'filter' => array(
                                            'F' => 'FEMENINO',
                                            'M' => 'MASCULINO'),
                                        'htmlOptions' => array('nowrap' => 'nowrap'),
                                    ),
                                    array(
                                        'header' => '<center>Código del Plantel</center>',
                                        'name' => 'cod_plantel',
                                        'filter' => CHtml::textField('InscripcionManual[cod_plantel]', '', array('maxlength' => 20, 'id' => 'cod_plantel')),
                                    ),
                                    array(
                                        'header' => '<center>Nombre del Plantel</center>',
                                        'name' => 'plantel',
                                        'filter' => CHtml::textField('InscripcionManual[plantel]', '', array('maxlength' => 150, 'id' => 'plantel')),
                                    ),
                                    array(
                                        'type' => 'raw',
                                        'header' => '<center>Estatus Inscripción</center>',
                                        'name' => 'estatus_inscripcion',
                                        //    'filter' => CHtml::textField('InscripcionManual[estatus_inscripcion]', '', array('maxlength' => 1, 'id' => 'estatus_inscripcion')),
                                        'value' => array($this, 'columnaEstatusInscripcion'),
                                        'filter' => array(
                                            'V' => 'Falta Verificar',
                                            'M' => 'Matriculado',
                                            'P' => 'Pendiente por Validar',
                                            'L' => 'Listo para Matricular'),
                                        'htmlOptions' => array('nowrap' => 'nowrap'),
                                    ),
                                    array(
                                        'type' => 'raw',
                                        'header' => '<center>Acciones</center>',
                                        'value' => array($this, 'columnaAcciones'),
                                        'htmlOptions' => array('nowrap' => 'nowrap'),
                                    ),
                                ),
                                'pager' => array(
                                    'header' => '',
                                    'htmlOptions' => array('class' => 'pagination'),
                                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                    'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                    'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                    'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                ),
                            ));
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<br>

<div class="col-md-12">
    <div class="col-md-12">
        <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("planteles/consultar"); ?>" class="btn btn-danger">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>
</div>
<div id='div' class="hide"></div>
<div class="panel panel-warning hide" id="dialog_verificar">
    <div class="panel-heading">
        <h2 class="panel-title center grey" style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
            <b>
                &iquest;Estas seguro(a) que desea validar los datos de este estudiante?
                Si continua esta indicando que los datos son correctos y ya se puede matricular dicho estudiante.
            </b>
        </h2>
    </div>
</div>

<div class="col-lg-12"><div class="space-6"></div></div>

<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<script>
    $(document).ready(function() {

        $('.validarEstudiante').unbind('click');
        $('.validarEstudiante').click(function() {
            var id = $(this).attr('id');
            var plantel_id = $(this).attr('plantel_id');
            validarEstudiante(id, plantel_id);
        });
    });
    function validarEstudiante(id, plantel_id) {
        var mensaje = '';
        var title = '';
        var div = 'div';
        var tipo = '';
        var redireccionar = false;
        var divResult = '';
        var urlDir = '/planteles/inscripcionManual/validarEstudiante';
        var datos = {
            id: id,
            plantel_id: plantel_id
        }
        var loadingEfect = false;
        var showResult = false;
        var method = 'POST';
        var responseFormat = 'json';
        var beforeSendCallback = function() {
        };
        var successCallback;
        var errorCallback = function() {
        };
        var dialog = $("#dialog_verificar").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa icon-info-sign'></i> Información </h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-chevron-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-xs btn-danger",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "&nbsp; Continuar <i class='icon-chevron-right'></i>",
                    "class": "btn btn-xs btn-primary",
                    click: function() {
                        $(this).dialog("close");
                        successCallback = function(response) {

                            if (response.statusCode == 'success') {
                                title = response.title;
                                mensaje = response.mensaje;
                                tipo = 'success';
                                redireccionar = true;
                                dialogo(mensaje, title, div, tipo, redireccionar);
                            }
                            if (response.statusCode == 'error') {
                                title = response.title;
                                mensaje = response.mensaje;
                                tipo = 'error';
                                dialogo(mensaje, title, div, tipo, redireccionar);
                            }
                        };
                        executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
                    }
                }
            ]
        });
    }

    function dialogo(mensaje, title, div, tipo, redireccionar) {
        displayDialogBox(div, tipo, mensaje);
        var dialog = $("#" + div).removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
            title_html: true,
            close: function() {
                $(this).dialog("close");
                window.location.reload();
            },
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                        if (redireccionar) {
                            window.location.reload();
                        }
                    }
                }
            ]
        });
    }

</script>

