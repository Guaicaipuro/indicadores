<?php
/*
 * DATOS DEL PLANTEL
 */
$cod_plantel = isset($datosPlantel['cod_plantel']) ? $datosPlantel['cod_plantel'] : '';
$cod_estadistico = isset($datosPlantel['cod_estadistico']) ? $datosPlantel['cod_estadistico'] : '';
$nombre_plantel = isset($datosPlantel['nom_plantel']) ? $datosPlantel['nom_plantel'] : '';
$municipio = isset($datosPlantel['municipio']) ? $datosPlantel['municipio'] : '';
$direccion = isset($datosPlantel['direccion']) ? $datosPlantel['direccion'] : '';
$zona_educativa = isset($datosPlantel['zona_educativa']) ? $datosPlantel['zona_educativa'] : '';
$telefono_fijo = (isset($datosPlantel['telefono_fijo']) AND $datosPlantel['telefono_fijo'] > 0) ? $datosPlantel['telefono_fijo'] : '';
$telefono_otro = (isset($datosPlantel['telefono_otro']) AND $datosPlantel['telefono_otro'] > 0 ) ? $datosPlantel['telefono_otro'] : '';
$denominacion = isset($datosPlantel['denominacion']) ? $datosPlantel['denominacion'] : '';

/*
 * DATOS DE LA SECCION
 */

$plan_estudio = '';
$cod_plan = '';
$mencion = '';
$seccion = '';
$grado =  '';
$cant_estudiantes = '';

/*
 * DATOS DEL PERIODO
 *
 *
 */
$periodo_escolar = (isset($periodo['periodo'])) ? $periodo['periodo'] : '';
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<br /><br/>
<table style="font-size:9.5px; font-family: Arial; width:800px;">

    <tr>
        <td colspan="3" align="center" style="background:#E5E5E5; padding:3px; font-family: Arial;" >
            <b>DATOS DEL PLANTEL</b>
        </td>
    </tr>

    <tr>
        <td style="font-family: Arial;">
            <b>C&oacute;digo del plantel:</b>
            <?php echo $cod_plantel; ?></td>
        <td colspan="2" style="font-family: Arial;">
            <b>Nombre del Plantel:</b>
            <?php echo $nombre_plantel; ?>
        </td>
    </tr>
    <tr>
        <td style="font-family: Arial;">
            <b>C&oacute;digo Estad&iacute;stico:</b>
            <?php echo $cod_estadistico; ?>
        </td>

        <td style="font-family: Arial;" colspan="2">
            <b>Dirección:</b>
            <?php echo $direccion; ?>
        </td>

    </tr>
    <tr>
        <td style="font-family: Arial;" >
            <b>Denominación:</b>
            <?php echo $denominacion; ?>
        </td>
        <td style="font-family: Arial;" >
            <b>Municipio:</b>
            <?php echo $municipio; ?>
        </td>
        <td style="font-family: Arial;" >
            <b>Zona Educativa:</b>
            <?php echo $zona_educativa; ?>
        </td>
    </tr>

</table>
<table   style="font-size:9.5px; font-family: Arial; width:800px;">
    <tr>
        <th colspan="14" align="center" style=" font-family: Arial; background:#E5E5E5; padding:3px;">
            ESTUDIANTES DE ESTUDIANTES POR SECCIÓN
        </th>
    </tr>
    <tr>
        <th style="width:20%;">
            Cantidad
        </th>
        <th style="width:20%;">
            Grado
        </th>
        <th style="width:20%;">
            Plan
        </th>
          <th style="width:20%;">
            Credencial
        </th>
          <th style="width:20%;">
            Mención
        </th>
    </tr>
    <tr>
        <?php
        foreach ($estadisticas_estudiante as $valor):?>
    <tr>
        <td align="center">
            <?php echo $valor['cantidad'];?>
        </td >
        <td align="center">
            <?php echo $valor['grado'];?>
        </td>
          <td align="center">
            <?php echo $valor['plan'];?>
        </td>
          <td align="center">
            <?php echo $valor['credencial'];?>
        </td>
            <td align="center">
            <?php echo $valor['mencion'];?>
        </td>
    </tr>
       <?php  endforeach;?>
</table>