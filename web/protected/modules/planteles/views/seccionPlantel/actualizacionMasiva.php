<?php
$this->breadcrumbs = array(
    'Consultar Planteles' => array('consultar/'),
    'Secciones por Plantel' => array("/planteles/seccionPlantel/admin/id/". base64_encode($plantel_id)),
    'Actualización Masiva de Cedula de Estudiantes'
                          );
?>
<div><?php $this->widget('ext.loading.LoadingWidget'); //Dibujo que muestra un loading ?></div>
<div class="widget-box">
    <div class = "widget-header">
        <h5>Identificación del Plantel <?php echo '"' . $datosPlantel['nom_plantel'] . '"'; ?></h5>
        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class = "widget-body">
        <div style = "display:none;" class = "widget-body-inner">
            <div class = "widget-main">
                <div class="row row-fluid">
                    <div id="msgAlerta">
                    </div>
                    <div class = "col-lg-12"><div class = "space-6"></div></div>
                    <div id="gridEstudiantes" class="col-md-12" >
                        <div class="widget-main form">
                            <div class="row">
                                <div class="col-md-11" id ="detallesPlantel">
                                    <?php $this->renderPartial('_informacionPlantel', array('plantel_id' => $plantel_id,'datosPlantel' => $datosPlantel)); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="widget-box">
    <div class = "widget-header">
        <h5>Datos de la Sección</h5>
        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class = "widget-body">
        <div style = "display:none;" class = "widget-body-inner">
            <div class = "widget-main">
                <div class="row row-fluid">
                    <div id="msgAlerta">
                    </div>
                    <div class = "col-lg-12"><div class = "space-6"></div></div>
                    <div id="gridEstudiantes" class="col-md-12" >
                        <div class="widget-main form">
                            <div class="row">
                                <div class="col-md-11" id ="detallesSec">
                                    <?php $this->renderPartial('_informacionSeccion', array('datosSeccion' => $datosSeccion)); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$periodo = (isset($periodo_escolar) AND isset($periodo_escolar['periodo']))? $periodo_escolar['periodo'] : '';
?>
<div class="widget-box">
    <div class = "widget-header">
        <h5><?php echo $model->grado->nombre . ' Sección "' . $model->seccion->nombre. '" Periodo Escolar '.$periodo; ?></h5>
        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class = "widget-body">
        <div class = "widget-main">
            <div class="row row-fluid">
                <div id="msgAlerta">
                </div>
                <div class="row col-md-12">
                    <?php
                    echo CHtml::hiddenField('plantel_id', $plantel_id);
                    ?>
                    <div class="col-md-12">
                        <?php if(Yii::app()->user->hasFlash('mensajeExito')){ ?>
                        <div class="successDialogBox">
                            <p>
                                <?php echo Yii::app()->user->getFlash('mensajeExito'); ?>
                            </p>
                        </div>
                        <?php } ?>
                        
                        <div id="msj_exitoso" class="successDialogBox hide">
                            <p>
                            </p>
                        </div>
                        <div id="msj_error_inactivar" class="errorDialogBox hide">
                            <p>
                            </p>
                        </div>
                        
                        <div class="col-md-7">
                            <div class="row col-md-12">
                                <h5><strong>Lista de Estudiantes</strong></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class = "col-lg-12"><div class = "space-6"></div></div>
                <div id="gridEstudiantes" class="col-md-12" >
                    <div class="widget-main form"><?php if($totalInscritos['count']!=0 ){?>
                        <i><b>Total de estudiantes Inscritos: </b></i>&nbsp;<?php echo ' ' . $totalInscritos['count'] . '  '; ?><br>
                            <i><b>Total de Estudiantes con Cedula Escolar: </b></i>&nbsp;<?php echo ' ' . $totalConCedulaEscolar['count'] . '  '; ?>
                            <div class="row">
                                <div class="col-md-12" id ="estudiantesInscritos">
                                <?php if (isset($dataProvider) && $totalConCedulaEscolar['count']!=0) { ?>
                                    <br>
                                    <div class="total hide"><?php echo $totalInscritos['count']; ?></div>
                                    <div class="row col-md-12">
                                        <form id="formulario" name="actualizacionMasiva" method="post">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                                <th><center>N°</center></th>
                                                <th><center>Documento de Identidad</center></th>
                                                <th><center>Nombres y Apellidos</center></th>
                                                <th><center>Actualización</center></th>
                                                <th><center>Nombres y Apellidos (Verificación) </center></th>
                                        </thead>
                                        <?php $i=1; foreach($dataProvider as $data){ ?>
                                                <tr>
                                                        <td>
                                                                <center>
                                                                    <?php echo $i; ?>
                                                                </center>
                                                        </td>
                                    
                                                        <td><div>
                                                            <center>
                                                                <?php 
                                                                        if($data['documento_identidad']!=null && $data['tdocumento_identidad']!=null && $data['cedula_escolar']==null)
                                                                            echo  $data['tdocumento_identidad'].'-'.$data['documento_identidad'];
                                                                        if($data['cedula_escolar']!=null && $data['documento_identidad']==null && $data['tdocumento_identidad']==null)
                                                                            echo 'C.E: '.$data['cedula_escolar'];                                                    
                                                                        if($data['cedula_escolar']!=null && $data['documento_identidad']!=null && $data['tdocumento_identidad']!=null)
                                                                            echo 'C.E: '.$data['cedula_escolar'];
                                                                    ?>
                                                            </center>
                                                            </div>
                                                        </td>
                                                        
                                                        <td>
                                                            <div>
                                                            <?php echo $data['nomape']; ?>
                                                            </div>
                                                        </td>
                                    
                                                        <td>
                                                            <center>
                                                                    <?php
                                                                        if($data['documento_identidad']!=null && $data['tdocumento_identidad']!=null && $data['cedula_escolar']==null ){
                                                                                   echo CHtml::textField($data['id'],'No Aplica',array('readonly'=>'readonly')); }
                                                                        if($data['cedula_escolar']!=null && $data['documento_identidad']==null && $data['tdocumento_identidad']==null ){
                                                                                   echo CHtml::textField($data['id'],'',array('class'=>'documento_identidad','id'=>$data['id'],'maxlength'=>10,'placeholder'=>'V0000000 ó E00000000')); }
                                                                        if( $data['cedula_escolar']!=null && $data['documento_identidad']!=null && $data['tdocumento_identidad']!=null )
                                                                                   echo CHtml::textField($data['id'],'Actualizado',array('class'=>'documento_identidad','readonly'=>'readonly'));  
                                                                     ?>
                                                            </center>
                                                        </td>
                                                        
                                                        <td>
                                                            <center>
                                                                    <?php 
                                                                            echo CHtml::textField('Visualizacion','',array('readonly'=>'readonly','class'=>'nomape','size'=>35,'id'=>$data['id'])); 
                                                                            if($totalInscritos['count']==$i)
                                                                            {
                                                                            //echo CHtml::hiddenField('Visualizacion','',array('readonly'=>'readonly')); 
                                                                            } 
                                                                        ?>
                                                            </center>
                                                        </td>
                                                </tr>
                                        <?php $i++; } ?>
                                    </table>
                                <?php        
                                    if ($totalConCedulaEscolar['count']!=0 )
                                    {    
                                        echo    '<div class="col-md-6 wizard-actions">
                                                <button id="Guardar" class="btn btn-primary btn-next" data-last="Finish" type="submit" >
                                                Actualizar
                                                <i class="icon-save icon-on-right"></i>
                                                </button>
                                                </div>';
                                    }
                                    
                                    }else
                                    { 
                                            echo "<br>";
                                            echo '<div class="infoDialogBox">
                                                          <p>
                                                              Esta sección no tiene ningún estudiante con Cédula Escolar para Actualizar
                                                          </p>
                                                  </div>';
                                    }
                                    ?>
                                    </div>
                                
                                </div>
                            </div>
                        <?php  }else{ echo '<div class="infoDialogBox">
                                                          <p>
                                                             Esta sección no tiene ningún estudiante con Cédula Escolar para Actualizar
                                                          </p>
                                                      </div>';
                        }?>              
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="col-md-12">
    <div class="col-md-6">
        <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("/planteles/seccionPlantel/admin/id/" . base64_encode($plantel_id)); ?>" class="btn btn-danger">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
        <?php $this->renderPartial('/_accionesSobrePlantel', array('plantel_id' => $plantel_id)) ?>
    </div>
    <div class = "col-md-6 pull-right wizard-actions " style = "padding-left:10px;">
        <a  href="<?php echo Yii::app()->createUrl("/planteles/matricula/reporte/id/" . base64_encode($plantel_id) . '/seccion/' . base64_encode($seccionId)); ?>"  id="reporteMatricula"   class = "btn btn-primary btn-next btn-sm ">
            Imprimir Matricula
            <i class="fa fa-file-pdf-o icon-on-right"></i>
        </a>
    </div>
</div>
<div id="dialogPantalla"></div>
<div class ="hide" id="incluir_Estudiante" ></div>
<div id="dialog_error" class="hide"></div>
<div id="Confirmacion" class="hide">
    <div class="alertDialogBox"><p>¿Esta Seguro de que desea Actualizar los Registros Asociados?</p></div>
</div>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/plantel/matricula.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/pnotify.custom.min.js', CClientScript::POS_END);
?>
<script type="text/javascript">
    $(document).ready(function(){
        
        function verDialogo(message, title, style, clickCallback, reload, buttonsExtra)
        {
        if (!style)
            style = 'info';

        var buttonsDialog = [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if (clickCallback) {
                        clickCallback();
                    }
                    if (reload) {
                        window.location.reload();
                    }
                }
            }
        ];

        if (buttonsExtra)
            buttonsDialog.push(buttonsExtra);

        displayDialogBox('dialog_error', style, message);

        var dialog = $("#dialog_error").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: buttonsDialog
        });
        }
    
        $("#reporteMatricula").unbind('click');
        $("#reporteMatricula").bind('click', function(e) {
            mostrarNotificacion();
        });
        $('.documento_identidad').on('keydown',function(tecla){  
            if($(this).val()=='')
            {
                   if(tecla.keyCode!=86 && tecla.keyCode!=69)
                   {
                      return false;
                   }
            }
            else
            {
            		if( (tecla.keyCode<48 || tecla.keyCode>57) && (tecla.keyCode<96 || tecla.keyCode>105) && tecla.keyCode!=8 && tecla.keyCode!=9)
            		{
            			return false;
            		}
                        if($(this).val().length==2 && tecla.keyCode==8)
                        {
                            this.value='';
                        }
                        if($(this).val().length==1 && tecla.keyCode!=8)
            		{
            			this.value=this.value+'-';
            			//alert('solo la segunda vez deberia aparecer');
            		}
            }
            this.value=this.value.toUpperCase(); 
                                                               });
                                                               
        $('.documento_identidad').on('blur',function(){
            var id   = $(this).attr('id');
            var valor= $(this).val();
            var data = $(this).val().split('-');
            var validacion=false;
            
            if($(this).val()!='' && $(this).attr('readonly')!='readonly' ){
            $('.documento_identidad').each(function(){
                if( $(this).attr('id')!=id )
                {
                    if( $(this).val()==valor )
                    {
                        var mensaje = 'Estimado Usuario, el documento Ingresado se encuentra duplicado, por favor Ingrese Otro documento nuevamente';
                        var title = 'Documento de Identidad Repetido';
                        validacion=true;
                        verDialogo(mensaje, title);
                        $('.documento_identidad').filter('#'+id).val('');
                        //$(this).val('');
                    }
                }
                                                     });
                                 }
            if($(this).val()!='' && this.value.length>2 && validacion!=true && $(this).attr('readonly')!='readonly' ){
            var documento = { documento_identidad: data[1], tdocumento_identidad: data[0] };
            Loading.show();
            $.ajax({
                     url : '../../buscarCedula',
                     data:  documento,
                     type: 'post',
                     success: function(respuesta)
                     {
                                    var json= jQuery.parseJSON(respuesta);
                                    if(json.estatus=='success')
                                    {
                                        Loading.hide();
                                        $( ".nomape" ).filter('#'+id).val(json.nombres+' '+json.apellidos);
                                    }
                                    if(json.estatus=='error')
                                    {
                                        Loading.hide();
                                        var mensaje = json.mensaje;
                                        var title = json.title;  
                                        verDialogo(mensaje, title);
                                        $( ".nomape,.documento_identidad" ).filter('#'+id).val('');
                                    }
                     }
                    });
                }
                else
                {
                    if($(this).attr('readonly')!='readonly')
                    $( ".nomape,.documento_identidad" ).filter('#'+id).val('');
                }
        });
        // Advertencia antes de Guardar.
        $('#Guardar').on('click',function(e){
            e.preventDefault();
            //alert($('.total').text());
                                            var datos= $('#formulario').serialize();
                         var dialogo=$('#Confirmacion').removeClass('hide').dialog({
                                                
                                                modal: true,
                                                width: '600px',
                                                draggable: false,
                                                resizable: false,
                                                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i>Confirmacion de Actualizacion</h4></div>",
                                                title_html: true,
                                                buttons: [
                                                            {
                                                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                                                                "class": "btn btn-xs",
                                                                click: function() {
                                                                    $(this).dialog("close");
                                                                }
                                                            },
                                                            {
                                                                html: "<i class='fa fa-spinner bigger-110'></i>&nbsp; Actualizar",
                                                                "class": "btn btn-danger btn-xs",
                                                                click: function() {
                                                                    $.ajax({
                                                                        url: "../../cargaActualizacionMasiva",
                                                                        data: datos,
                                                                        dataType: 'html',
                                                                        type: 'post',
                                                                        success: function(json) {
                                                                            try {
                                                                                var json = jQuery.parseJSON(json);

                                                                                if (json.statusCode == "exito") {

                                                                                    //alert('HOla mundo');
                                                                                    location.reload();
                                                                                    dialogo.dialog('close');
                                                                                    $("#msj_error_inactivar").addClass('hide');
                                                                                    $("#msj_error_inactivar p").html('');
                                                                                    $("#msj_exitoso").removeClass('hide');
                                                                                    $("#msj_exitoso p").html(json.mensaje);
                                                                                    $("html, body").animate({scrollTop: 0}, "fast");
                                                                                }

                                                                                if (json.statusCode == "error") {

                                                                                    $("#pregunta_inactivar").addClass('hide');
                                                                                    $("#msj_exitoso").addClass('hide');
                                                                                    $("#msj_exitoso p").html('');
                                                                                    $("#msj_error_inactivar").removeClass('hide');
                                                                                    $("#msj_error_inactivar p").html(json.mensaje);
                                                                                    $("html, body").animate({scrollTop: 0}, "fast");
                                                                                }
                                                                            }
                                                                            catch (e) {

                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        ]
                                            });
                                            //return false;
                                            }); 
        
    });
</script>                   