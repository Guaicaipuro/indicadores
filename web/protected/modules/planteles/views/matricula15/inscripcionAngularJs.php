<?php
/* @var $this Matricula15Controller */
$this->breadcrumbs = array(
    'Planteles' => array('/planteles'),
    'Secciones' => array('/planteles/seccionPlantel15/admin/id/' . $plantel_id),
    'Inscripción'
);


?>
    <div ng-app="Matricula">
    <div ng-controller="MatriculaController" ng-init="inicializar()">
    <div class = "widget-box collapsed">

        <div class = "widget-header">
            <h5>Identificación del Plantel <?php echo '"' . $dataPlantel['nom_plantel'] . '"'; ?></h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: none;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row row-fluid">
                        <?php $this->renderPartial('_informacionPlantel', array('datosPlantel' => $dataPlantel)); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="widget-box collapsed" >
        <div class = "widget-header">
            <h5>Inscripción Inicial <?php echo $dataSeccion['grado'] . ' Sección "' . $dataSeccion['seccion'] . '" Periodo Escolar '.$periodo_escolar; ?></h5>
            <div class = "widget-toolbar">
                <a id="wBoxInscripcion" href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>
        </div>

        <div class = "widget-body">
            <div style = "display:block;" class = "widget-body-inner">
                <div class = "widget-main">
                    <div class="row row-fluid">
                        <div id="loadingWidget" class=" center hide row-fluid ui-corner-all" style="padding: 0 .7em;">
                            <div class="loadingContent">
                                <p><span><strong> {{msgLoadingInit}} </strong></span></p>
                                <p>
                                    <img alt="" src="/public/images/ajax-loader.gif" />
                                </p>
                            </div>
                        </div>
                        <div id="msgAlerta"></div>

                        <div class="contenidoInscripcion hide ">
                            <div class="row col-md-12">
                                <?php
                                echo CHtml::hiddenField('plantel_id', $plantel_id);
                                echo CHtml::hiddenField('seccion_plantel_id', $seccion_plantel_id);
                                echo CHtml::hiddenField('periodo_id', $periodo_escolar_id);
                                echo CHtml::hiddenField('grado_id', $dataSeccion['id']);
                                ?>
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="row  col-md-12 pull-right">
                                            <?php
                                            $nivelesHabilitados []=1; // INICIAL
                                            $nivelesHabilitados []=2; // PRIMARIA
                                            if(in_array((isset($dataSeccion['nivel_id'])?$dataSeccion['nivel_id']:''),$nivelesHabilitados)):
                                                ?>
                                                <div class = "pull-right wizard-actions" style = "padding-left:10px;">
                                                    <button id="btnRegistroNuevo" ng-click="dialogEstudianteNuevo()" data-last = "Finish" class = "btn btn-success btn-next btn-sm tooltipMatricula" title="Registre rápidamente un nuevo estudiante para su inscripción">
                                                        Registrar Nuevo Estudiante
                                                        <i class = "fa fa-plus icon-on-right"></i>
                                                    </button>
                                                </div>
                                            <?php endif; ?>
                                            <div class = "pull-right wizard-actions" style = "padding-left:10px;">
                                                <button id="btnIncluirEst" ng-click="dialogEstudianteExistente()" data-last = "Finish" title="Buscar un estudiante ya registrado que no aparezca en el listado de 'Estudiantes Disponibles para la Inscripción'." class = "btn btn-success btn-next btn-sm tooltipMatricula">
                                                    Buscar Estudiante Existente
                                                    <i class="fa fa-search-plus icon-on-right"></i>
                                                </button>
                                            </div>
                                            <div style="padding-left:10px;" class="pull-right wizard-actions">
                                                <button id="linkDialogAyuda" title="Si tiene alguna duda sobre el proceso de matriculación presione aquí" class="btn btn-primary btn-next btn-sm tooltipMatricula">
                                                    Ayuda
                                                    <i class="fa fa-question icon-on-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class = "col-lg-12"></div>
                            <div class="col-md-12">

                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <h5><strong>Estudiantes disponibles para la Inscripción</strong></h5>
                                        <div class="row col-md-12">
                                            <!--<div class="gridStyle" ng-grid="gridSinMatricular"></div>-->
                                            <span><strong>Estudiantes Seleccionados:</strong></span> {{estudSelecSinMatricular.length}}
                                            <table class="table-inscripcion" tr-ng-grid="" selected-items="estudSelecSinMatricular" order-by="defaultOrder" locale="{{myLocale}}" items="estudiantesSinMatricular" fields="['documento_identidad','nom_completo','edad']" selection-mode="MultiRow" page-items="25">
                                                <thead>
                                                <tr>
                                                    <th field-name="documento_identidad" display-name="Documento de Identidad" display-format="documentoIdentidadFilter:gridItem" display-align="center"></th>
                                                    <th field-name="nom_completo" display-name="Nombres y Apellidos" display-align="center"></th>
                                                    <!--<th field-name="fecha_nacimiento" display-name="Fecha de Nacimiento" display-format="trNgGridTranslateFilter:gridOptions.locale" display-align="center"></th>-->

                                                    <th field-name="edad" display-name="Edad" display-format="ageFilter:gridItem" display-align="center"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td ng-dblclick="pushEstudiantePreInscrito(gridItem)" field-name="documento_identidad" ></td>
                                                    <td ng-dblclick="pushEstudiantePreInscrito(gridItem)" field-name="nom_completo"></td>
                                                    <!-- <td ng-dblclick="pushEstudiantePreInscrito(gridItem)" field-name="fecha_nacimiento"></td>-->
                                                    <td style="width: 10%" ng-dblclick="pushEstudiantePreInscrito(gridItem)" field-name="edad"></td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td>
                                                        <div class="space-6"><div class="row"></div></div>
                                                        <div class=" col-md-12">
                                                            <span class="hide" tr-ng-grid-global-filter=""></span>
                                                            <div class="col-md-11">
                                                                <span class="form-group pull-left" tr-ng-grid-pager=""></span>
                                                            </div>
                                                            <div class="col-md-1">
                                                                    <span class="form-group pull-right">
                                                                        <button class="btn btn-success btn-sm tooltipMatricula " data-placement="top" ng-click="pushEstudiantePreInscrito()" ng-class="{'disabled':!estudSelecSinMatricular.length}"><i class="icon-arrow-right"></i></button>
                                                                    </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <h5><strong>Estudiantes Pre-Inscritos</strong></h5>
                                        <div class="row col-md-12">
                                            <span><strong>Estudiantes Seleccionados:</strong></span> {{estudSelecPreInscritos.length}}
                                            <table class="table-inscripcion" tr-ng-grid="" selected-items="estudSelecPreInscritos" order-by="defaultOrder" locale="{{myLocale}}" items="estudiantesPreInscritos" fields="['documento_identidad','nom_completo','edad']" selection-mode="MultiRow" page-items="25">
                                                <thead>
                                                <tr ng-dblClick="pushEstudianteSinMatricular()">
                                                    <th field-name="documento_identidad" display-name="Documento de Identidad" display-format="documentoIdentidadFilter:gridItem" display-align="center"></th>
                                                    <th field-name="nom_completo" display-name="Nombres y Apellidos" display-align="center"></th>
                                                    <!--<th field-name="fecha_nacimiento" display-name="Fecha de Nacimiento" display-format="trNgGridTranslateFilter:gridOptions.locale" display-align="center"></th>-->
                                                    <th field-name="edad" display-name="Edad" display-format="ageFilter:gridItem" display-align="center"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td ng-dblclick="pushEstudianteSinMatricular(gridItem)" field-name="documento_identidad"></td>
                                                    <td ng-dblclick="pushEstudianteSinMatricular(gridItem)" field-name="nom_completo"></td>
                                                    <!--<td ng-dblclick="pushEstudianteSinMatricular(gridItem)" field-name="fecha_nacimiento"></td>-->
                                                    <td style="width: 10%" ng-dblclick="pushEstudianteSinMatricular(gridItem)" field-name="edad"></td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td>
                                                        <div class="space-6"><div class="row"></div></div>
                                                        <div class=" col-md-12">
                                                            <span class="hide" tr-ng-grid-global-filter=""></span>
                                                            <div class="col-md-1">
                                                                        <span class="form-group pull-left">
                                                                            <button class="btn btn-success btn-sm tooltipMatricula " data-placement="top" ng-click="pushEstudianteSinMatricular()" ng-class="{'disabled':!estudSelecPreInscritos.length}"><i class="icon-arrow-left"></i></button>
                                                                        </span>
                                                            </div>
                                                            <div class="col-md-11">
                                                                <span class="form-group pull-right" tr-ng-grid-pager=""></span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--<div id="gridEstudiantes" class="col-md-12" >
                            <?php /*$this->renderPartial('_gridEstudiantes', array('inscritos' => $inscritos, 'dataProviderIns' => $dataProviderIns, 'dataProviderPen' => $dataProviderPen)); */?>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row"><div class="space-14"></div></div>
    <div class="col-md-12">
        <div class="col-md-6">
            <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("/planteles/seccionPlantel15/admin/id/" . ($plantel_id)); ?>" class="btn btn-danger">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
            <?php $this->renderPartial('/_accionesSobrePlantel', array('plantel_id' => $plantel_id)) ?>
        </div>
        <div class="col-md-6 wizard-actions">

            <button id="btnGuardarInscripcion" ng-click="preInscripcion()" data-last="Finish" class="btn btn-primary btn-next">
                Siguiente
                <i class="icon-arrow-right icon-on-right"></i>
            </button>
        </div>
    </div>
    <div class ="hide" id="incluir_Estudiante">
        <?php
        $tdocumento_identidad = array(
            array('id' => 'V', 'descripcion' => 'Venezolana'),
            array('id' => 'E', 'descripcion' => 'Extranjero (Nacionalizado)'),
            array('id' => 'C', 'descripcion' => 'Cédula Extranjera'),
            array('id' => 'P', 'descripcion' => 'Pasaporte'),
            array('id' => 'O', 'descripcion' => 'Otra'),
        );
        $this->renderPartial('_formBusquedaEstudiante', array('model' => $model,
            'plantel_id' => $plantel_id,
            'individual' => false,
            'tdocumento_identidad' => $tdocumento_identidad,
            'seccion_plantel_id' => $seccion_plantel_id));
        ?>
    </div>
    <div class ="hide" id="incluir_Estudiante_Nuevo">
        <?php
        $this->renderPartial('nuevoRegistro',
            array(
                'mEstudiante' => $model,
                'mDatosAnt' => $mDatosAnt,
                'cedulaEscolar' => $cedulaEscolar,
                'model' => $modelRepresentante,
                'modelAfinidad' => $modelAfinidad,
                'key' => $key
            ));
        ?>
    </div>
    <div class="hide" id="dialog_peticion_activa">
        <div class="alertDialogBox">
            <p style="text-align: justify">
                Estimado usuario, en este momento se esta ejecutando una petición. Espere que dicha operación culmine e intente nuevamente.</p>
        </div>
    </div>
    <div id="dialogPantalla"></div>
    <div id="confirm" class="hide center">

        <div class="alert alert-info ">
            <p> ¿ Esta seguro que son todos los estudiantes que desea inscribir ?  </p>
            <p> En caso contrario presione el botón Cancelar y verifique. </p>
        </div>

    </div>
    <div id="dialogoAyuda" class="hide" >
        <div class="alert alert-info ">
            <p> <b>1.-</b> El botón <b style="color: #00be67">"Registrar Nuevo Estudiante"</b> permite efectuar el registro rápido de un nuevo estudiante para su inscripción en esta sección. Cuando se culmine el proceso de registro del nuevo estudiante el mismo sera agregado al listado de <b style="color: #222a2d">"Estudiantes Pre-Inscritos"</b> en esta sección.</p>
        </div>
        <div class="alert alert-info ">
            <p> <b>2.-</b> El botón <b style="color: #00be67">"Buscar Estudiante"</b> permite efectuar una búsqueda de los estudiantes de dos formas, la primera efectua una busqueda de los estudiantes inscritos en el periodo anterior. La segunda, si tilda la opción <b>"Búsqueda Completa"</b> se realiza esta actividad en todos los registros de estudiantes a nivel nacional. Cuando se culmine el proceso de búsqueda y selección del estudiante el mismo sera agregado al listado de <b style="color: #222a2d">"Estudiantes Pre-Inscritos"</b> en esta sección.</p>
        </div>
        <div class="alert alert-info ">
            <p>
                <b>3.-</b> El listado de <b style="color: #222a2d">"Estudiantes disponibles para la Inscripción"</b> contendrá a todos aquellos estudiantes que fueron inscritos en este plantel, en el periodo anterior, dependiendo también del último grado en el que fue inscrito el estudiante.
            </p>
        </div>
        <div class="alert alert-info ">
            <p>
                <b>4.-</b> El listado de <b style="color: #222a2d">"Estudiantes Pre-Inscritos"</b> contendrá la lista de estudiantes seleccionados para ser inscritos en esta sección. Se debe tomar en cuenta que los estudiantes que hayan sido seleccionados mediante las opciones <b style="color: #00be67">"Registrar Nuevo Estudiante"</b> y <b style="color: #00be67">"Buscar Estudiante"</b> no se podrán regresar al listado de <b style="color: #222a2d">"Estudiantes disponibles para la Inscripción"</b>.
            </p>
        </div>
        <div class="alert alert-info ">
            <p>
                <b>5.-</b> Para pasar al Siguiente paso debe hacer click en el botón <b>"Siguiente"</b> en donde podrá efectuar la Caracterización de la Inscripción.
            </p>
        </div>
    </div>
    </div>
    </div>

<?php ?>
    <script type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/pnotify.custom.min.css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/trNgGrid.min.css" media="screen, projection" />
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/plantel/matricula15.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/pnotify.custom.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/angular.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/resource.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/trNgGrid.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/angular/planteles/controllers/MatriculaController.js', CClientScript::POS_END);


