<?php
/**
 * Created by PhpStorm.
 * User: isalaz01
 * Date: 18/02/15
 * Time: 11:58 AM
 */
?>
<div class = "widget-box">
    <div class = "widget-header" style="border-width: thin">
        <h5>Búsqueda del estudiante</h5>
        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div style="margin-right: "
    <div class = "widget-body">
        <div style = "display: block;" class = "widget-body-inner">
            <div class = "widget-main"  style="margin-left:10px;">
                <div class="row">
                    <div id="respuestaBuscar" class="hide errorDialogBox" ><p></p> </div>
                    <div id="busquedaVacia" class="hide alertDialogBox" ><p></p> </div>
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'estudiante-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnType' => true,
                                'validateOnChange' => true),
                        )
                    );
                    echo CHtml::hiddenField('individual', $individual);
                    ?>
                    <div id="1eraFila" class="row">
                        <?php echo $form->labelEx($model, 'tdocumento_identidad', array('class' => 'col-md-2')); ?>
                        <div class="col-md-4" >
                            <?php echo $form->dropDownList($model, 'tdocumento_identidad', CHtml::listData($tdocumento_identidad, 'id', 'descripcion'), array('empty' => '-Seleccione-', 'class' => 'span-7','ng-model'=>'estudiante.tdocumento_identidad')); ?>
                            <?php echo $form->error($model, 'tdocumento_identidad'); ?>
                        </div>
                        <?php echo $form->labelEx($model, 'documento_identidad', array('class' => 'col-md-2')); ?>
                        <div class="col-md-4" >
                            <?php echo $form->textField($model, 'documento_identidad', array('class' => 'span-7','ng-model'=>'estudiante.documento_identidad', 'ng-trim'=>true, 'value'=>'{{estudiante.documento_identidad | uppercase}}')); ?>
                            <?php echo $form->error($model, 'documento_identidad'); ?>
                        </div>
                    </div>
                    <div id="2daFila" class="row">
                        <?php echo $form->labelEx($model, 'nombres', array('class' => 'col-md-2')); ?>
                        <div class="col-md-4" >
                            <?php echo $form->textField($model, 'nombres', array('class' => 'span-7','ng-model'=>'estudiante.nombres', 'ng-trim'=>true, 'value'=>'{{estudiante.nombres | uppercase}}')); ?>
                            <?php echo $form->error($model, 'nombres'); ?>
                        </div>
                        <?php echo $form->labelEx($model, 'apellidos', array('class' => 'col-md-2')); ?>
                        <div class="col-md-4" >
                            <?php echo $form->textField($model, 'apellidos', array('class' => 'span-7','ng-model'=>'estudiante.apellidos', 'ng-trim'=>true, 'value'=>'{{estudiante.apellidos | uppercase}}')); ?>
                            <?php echo $form->error($model, 'apellidos'); ?>
                        </div>
                    </div>
                    <div id="3eraFila" class="row">
                        <?php echo $form->labelEx($model, 'cedula_escolar', array('class' => 'col-md-2')); ?>
                        <div class="col-md-4">
                            <?php echo $form->textField($model, 'cedula_escolar', array('class' => 'span-7','ng-model'=>'estudiante.cedula_escolar', 'ng-trim'=>true, 'value'=>'{{estudiante.cedula_escolar | uppercase}}')); ?>
                            <?php echo $form->error($model, 'cedula_escolar'); ?>
                        </div>
                        <?php echo $form->labelEx($model, 'Cédula representante', array('class' => 'col-md-2')); ?>
                        <div class="col-md-4" >
                            <?php echo $form->textField($model, 'cirepresentante', array('class' => 'span-7','ng-model'=>'estudiante.ci_representante', 'ng-trim'=>true, 'value'=>'{{estudiante.ci_representante | uppercase}}')); ?>
                            <?php echo $form->error($model, 'cirepresentante'); ?>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                    <div>

                        <a id="btnBuscar" ng-click="getEstudiante()" class="pull-right btn btn-primary btn-sm" style="margin-top:-30px;margin-right:2%">
                            Buscar
                            <i class="icon-search icon-on-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="space-6"><div class="row"></div></div>
<div id="loadingWidgetEstudDialog" class=" center hide row-fluid ui-corner-all" style="padding: 0 .7em;">
    <div class="loadingContent">
        <p><span><strong> {{msgLoadingEstud}} </strong></span></p>

        <p>
            <img alt="" src="/public/images/ajax-loader.gif" />
        </p>
    </div>
</div>

<div class="row hide" id="respuestaBusqueda2">
    </div>
<div class="row hide" id="respuestaBusqueda">
    <div class="row col-md-12 hide " id="gridEstudBusqueda">
        <table class="table-inscripcion" tr-ng-grid="" locale="{{myLocale}}" items="estudiantesBusqueda" fields="['documento_identidad','nombres','apellidos','cod_plantel','plantel_nombre']" page-items="5">
            <thead>
            <tr>
                <th field-name="identificacionEstud" display-name="Documento de Identidad" enable-filtering="false" display-align="center"></th>
                <th field-name="nombresEstud" display-name="Nombres" enable-filtering="false" display-align="center"></th>
                <th field-name="apellidosEstud" display-name="Apellidos" enable-filtering="false" display-align="center"></th>
                <th field-name="codPlantelActual" display-name="Código del Plantel" enable-filtering="false" display-align="center"></th>
                <th field-name="nombrePlantelActual" display-name="Nombre del Plantel" enable-filtering="false" display-align="center"></th>
                <th>
                    <div class="tr-ng-title center">
                        Acciones
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <div class="center">
                        <a class="fa fa-plus green add-estud" title="Agregar Estudiante" ng-click="pushEstudiantePreInscrito(gridItem.datos_inscripcion,1)">
                        </a>
                    </div>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td>
                    <div class="space-6"><div class="row"></div></div>
                    <div class=" col-md-12">
                        <span class="hide" tr-ng-grid-global-filter=""></span>
                        <div class="col-md-11">
                            <span class="form-group pull-left" tr-ng-grid-pager=""></span>
                        </div>
                    </div>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>