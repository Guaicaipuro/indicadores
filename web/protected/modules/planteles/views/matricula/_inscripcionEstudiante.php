<div id="summary" class="hide">


</div><div class = "widget-box collapsed">

    <div class = "widget-header" style="border-width: thin">
        <h5>Datos del Estudiante </h5>

        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-down"></i>
            </a>
        </div>

    </div>
    <div class = "widget-body">
        <div style = "display: block;" class = "widget-body-inner">
            <div class = "widget-main"  style="margin-left:10px;">
                <div class="row">
                    <?php
                    $nombres = (is_object($estudiante->estudiante) AND isset($estudiante->estudiante->nombres)) ? $estudiante->estudiante->nombres : null;
                    $apellidos = (is_object($estudiante->estudiante) AND isset($estudiante->estudiante->apellidos)) ? $estudiante->estudiante->apellidos : null;
                    $documento_identidad = (is_object($estudiante->estudiante) AND isset($estudiante->estudiante->documento_identidad)) ? $estudiante->estudiante->documento_identidad : null;
                    $tDocumento_identidad = (is_object($estudiante->estudiante) AND isset($estudiante->estudiante->tdocumento_identidad)) ? $estudiante->estudiante->tdocumento_identidad : null;
                    if (!is_null($tDocumento_identidad) AND ! is_null($documento_identidad)) {
                        $identificacion = $tDocumento_identidad . '-' . $documento_identidad;
                    } else {
                        $identificacion = $documento_identidad;
                    }
                    $fecha_nacimiento = (is_object($estudiante->estudiante) AND isset($estudiante->estudiante->fecha_nacimiento)) ? $estudiante->estudiante->fecha_nacimiento : null;
                    $cedula_escolar = (is_object($estudiante->estudiante) AND isset($estudiante->estudiante->cedula_escolar)) ? $estudiante->estudiante->cedula_escolar : null;
                    $grado_actual = (is_object($estudiante->estudiante) AND isset($estudiante->estudiante->grado_actual)) ? $estudiante->estudiante->grado_actual : null;
                    $inscripcion_id = (is_object($estudiante) AND isset($estudiante->id)) ? $estudiante->id : null;

                    echo CHtml::hiddenField('inscripcion_id', $inscripcion_id);
                    ?>
                    <div id="1eraFila" class="col-md-12">
                        <div class="col-md-4" >
                            <?php echo CHtml::label('<strong>Nombres</strong>', '', array("class" => "col-md-12",)); ?>
                            <?php echo CHtml::textField('', $nombres, array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                        </div>

                        <div class="col-md-4" >
                            <?php echo CHtml::label('<strong>Apellidos</strong>', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('', $apellidos, array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                        </div>

                        <div class="col-md-4" >
                            <?php echo CHtml::label('<strong>Fecha de Nacimiento</strong>', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('', $fecha_nacimiento, array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                        </div>

                    </div>

                    <div class = "col-md-12"><div class = "space-6"></div></div>

                    <div id="2daFila" class="col-md-12">
                        <div class="col-md-4" >
                            <?php echo CHtml::label('<strong>Documento de Identidad</strong>', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('', $identificacion, array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                        </div>

                        <div class="col-md-4" >
                            <?php echo CHtml::label('<strong>Cédula Escolar</strong>', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('', $cedula_escolar, array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                        </div>

                        <!--                        <div class="col-md-4" >
                        <?php echo CHtml::label('<strong>Grado Actual</strong>', '', array("class" => "col-md-12")); ?>
                        <?php echo CHtml::textField('', $grado_actual, array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                                                </div>-->
                    </div>
                    <div class = "col-md-12"><div class = "space-6"></div></div>



                </div>
            </div>
        </div>

    </div>

</div>
<div  id="datosEscolaridad">
    <?php
    $this->renderPartial('_modEscolaridadEstudiante', array(
        'estudiante' => $estudiante,
            ), FALSE, TRUE);
    ?>
</div>



