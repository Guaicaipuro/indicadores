<?php
/* @var $this SeccionController */
/* @var $model Seccion */
/* @var $form CActiveForm */
?>
<script>
    $(document).ready(function() {
        /*$("#gradoId").change(function(event) {
         event.preventDefault();
         var grado_id = $(this).val();
         var plantel_id = $("#plantel_id").val();
         var plan_id = $("#planId").val();
         var nivel_id = $("#nivelId").val();
         var divResultMsgError = 'msgPersonalDocente';
         var divResultMsgSuccess = 'resultPersonalDocente';
         var divWidget = 'wBoxPersonalDocente';
         var url ='/planteles/seccionPlantel/obtenerAsignaturasCrear/';
         var datos = {
         plantel_id:base64_encode(plan_id),
         grado_id:base64_encode(grado_id),
         plan_id:base64_encode(plan_id),
         nivel_id:base64_encode(nivel_id)
         };
         var loadingEfect=false;
         var showResult=false;
         var method='GET';
         var responseFormat='html';
         var errorCallback=function(){};
         var beforeSendCallback=function(){};
         var successCallback;
         beforeSendCallback = function(){
         $("#"+divResultMsgError).addClass('hide');
         $("#"+divResultMsgError).html('');
         };
         successCallback=function(response,statusCode,dom){
         try{
         var responseJson = jQuery.parseJSON(dom.responseText);
         if(responseJson.statusCode =='ERROR-A'){
         $("#"+divResultMsgError).removeClass('hide');
         $("#"+divWidget).addClass('hide');
         $("#"+divResultMsgSuccess).html('');
         displayDialogBox(divResultMsgError,'error',responseJson.mensaje)
         }
         }
         catch(e){
         $("#"+divResultMsgError).addClass('hide');
         $("#"+divResultMsgError).html('');
         $("#"+divWidget).removeClass('hide');
         $("#"+divResultMsgSuccess).html('');
         displayHtmlInDivId(divResultMsgSuccess,response,true);
         }
         };
         // LIMPIO LOS POSIBLES MSG'S DE ERROR ANTES DE BUSCAR LAS ASIGNATURAS
         executeFormatedAjax(divResultMsgSuccess, url, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
         });*/
        $('#capacidadAlumnos').bind('keyup blur', function() {
            if ($("#capacidadAlumnos").val() > 100)
            {
                $("#capacidadAlumnos").val('100');
            }
            keyNum(this, false);// true acepta la ñ y para que sea español
        });

        $("#resultadoSeccionRegistrar").click(function() {
            document.getElementById("resultadoSeccionRegistrar").style.display = "none";
            document.getElementById("resultadoRegistrar").style.display = "block";
        });

        function noENTER(evt)
        {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text"))
            {
                return false;
            }
        }
        document.onkeypress = noENTER;
    });
</script>
<div class="col-md-12">
    <div class="form" id="_formRegistrarSeccion">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'seccion-plantel-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'clientOptions' => array(
                //  'validateOnSubmit' => true,
                'validateOnType' => true,
                'validateOnChange' => true),
        ));
        ?>
        <div id="autor" class="widget-box ">

            <div id="resultadoSeccionRegistrar">
            </div>

            <div id="seccion_error" class="errorDialogBox" style="display: none">
                <p>
                </p>
            </div>

            <div id="resultadoRegistrar" class="infoDialogBox">
                <p>
                    Por Favor Ingrese los datos correspondientes para registrar una sección, Los campos marcados con <span class="required">*</span> son requeridos.
                </p>
            </div>

            <div id ="guardoRegistro" class="successDialogBox" style="display: none">
                <p>
                    Registro Exitoso
                </p>
            </div>

            <div class="widget-header">
                <h5>Registrar Sección</h5>
                <div class="widget-toolbar">
                    <a  href="#" data-action="collapse">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>
        </div>
        <div id="registrarSeccion" class="widget-body" >
            <div class="widget-body-inner" >
                <div class="widget-main form">
                    <div style="padding: 20px" class="row">
                        <div  id="divNombre" class="col-md-4" >
                            <?php echo $form->labelEx($model, 'seccion_id', array("class" => "col-md-12")); ?>
                            <?php echo $form->dropDownList($model, 'seccion_id', CHtml::listData($seccion, 'id', 'nombre'), array('empty' => '- SELECCIONE -', 'class' => 'span-7', 'id' => 'seccionId')); ?>
                        </div>
                        <div  id="divNivel_id" class="col-md-4">
                            <?php echo $form->labelEx($model, 'modalidad_id', array("class" => "col-md-12")); ?>
                            <?php
                            echo $form->dropDownList(
                                    $model, 'modalidad_id', CHtml::listData($modalidad, 'modalidad_id', 'nombre'), array(
                                'empty' => array('' => '- SELECCIONE -'),
                                'class' => 'span-7',
                                'id' => 'modalidadId',
                                'onChange' => 'mostrarNivel()'
                                    )
                            );
                            ?>
                        </div>
                        <div  id="divNivel_id" class="col-md-4">
                            <?php echo $form->labelEx($model, 'nivel_id', array("class" => "col-md-12")); ?>
                            <?php //echo $form->dropDownList($model, 'nivel_id', CHtml::listData($nivel, 'nivel_id', 'nombre'), array('empty' => '- SELECCIONE -', 'class' => 'span-7', 'id' => 'nivelId')); ?>
                            <?php
                            echo $form->dropDownList(
                                    $model, 'nivel_id', $nivel, array(
                                'empty' => array('' => '- SELECCIONE -'),
                                'class' => 'span-7',
                                'id' => 'nivelId',
                                'onChange' => 'mostrarPlan()'
                                    )
                            );
                            ?>
                        </div>
                        
                        <div class="col-md-12"> </div>
                        
                        <div  id="divPlan_id" class="col-md-4" >
                            <?php echo $form->labelEx($model, 'plan_id', array("class" => "col-md-12")); ?>
                            <?php //echo $form->dropDownList($model, 'plan_id', array(), array('empty' => '- SELECCIONE -', 'class' => 'span-7', 'id' => 'planId')); ?>
                            <?php
                            //var_dump($model); die();
                            echo $form->dropDownList(
                                    $model, 'plan_id', $plan, array(
                                'empty' => array('' => '- SELECCIONE -'),
                                'class' => 'span-7',
                                'id' => 'planId',
                                'onChange' => 'mostrarGrado()'
                                    )
                            );
                            ?>
                        </div>
                        <div  id="divGrado_id" class="col-md-4" >
                            <?php echo $form->labelEx($model, 'grado_id', array("class" => "col-md-12")); ?>
                            <?php echo $form->dropDownList($model, 'grado_id', $grado, array('empty' => '- SELECCIONE -', 'class' => 'span-7', 'id' => 'gradoId')); ?>
                        </div>
                        <div  id="divCapacidad" class="col-md-4" >
                            <?php echo $form->labelEx($model, 'capacidad', array("class" => "col-md-12", "title" => "Cantidad de estudiantes para esta sección")); ?>
                            <?php echo $form->textField($model, 'capacidad', array('class' => 'span-7', 'id' => 'capacidadAlumnos', 'placeholder' => 'Cantidad de estudiantes')); ?>
                        </div>

                        <div class="col-md-12"> </div>
                        
                        <div  id="divTurno_id" class="col-md-4" >
                            <?php echo $form->labelEx($model, 'turno_id', array("class" => "col-md-12")); ?>
                            <?php echo $form->dropDownList($model, 'turno_id', CHtml::listData($turno, 'id', 'nombre'), array('empty' => '- SELECCIONE -', 'class' => 'span-7', 'id' => 'turnoId')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>

    <div class="row"><div class="space-6"></div></div>
    <div class="row hide" id="msgPersonalDocente"></div>
    <div id="wBoxPersonalDocente" class="row hide">
        <div class="widget-header">
            <h5>Personal Docente</h5>
        </div>
        <div id="personal_docente" class="widget-body" >
            <div class="widget-body-inner" >
                <div class="widget-main form">
                    <div class="row" id="resultPersonalDocente">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<!--  <div id="botones" class="row">

      <div class="col-md-6">
          <a id="btnRegresar" href=" // echo Yii::app()->createUrl("planteles/consultar/"); " class="btn btn-danger">
              <i class="icon-arrow-left"></i>
              Volver
          </a>
      </div>
  </div> -->



</div><!-- form -->

<div id="css_js">
    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/plantel/seccionPlantel15.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/plantel/registrar_seccion.js', CClientScript::POS_END);
    ?>
</div>






