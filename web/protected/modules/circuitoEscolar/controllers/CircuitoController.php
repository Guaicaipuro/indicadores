<?php


class CircuitoController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de CircuitoController',
        'write' => 'Creación y Modificación de CircuitoController',
        'admin' => 'Administración Completa  de CircuitoController',
        'label' => 'Módulo de CircuitoController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
       // en este array colocar solo los action de consulta
       return array(
           array('allow',
               'actions' => array(
                    'lista', 
                    'consulta', 
                    'registro', 
                    'edicion', 
                    'eliminacion', 
                    'activacion',
                    'buscarMunicipios',
                    'BuscarParroquias',
                    'VerificarEstadoMunicipioParroquia',
                    'vistaCircuito','modificarTipoPlantel',
                    'MostrarFormCircuitoPlantel',
                    'VerificarCodigoPlantel',
                    'GuardarCircuitoPlantel',
                    'reporteTotalCiruitos',
                    'reporteCircuitoIndividual'
                    ),
               'pbac' => array('admin'),
           ),
           array('allow',
               'actions' => array(
                    'lista', 
                    'consulta', 
                    'registro', 
                    'edicion',
                    'buscarMunicipios',
                    'BuscarParroquias',
                    'VerificarEstadoMunicipioParroquia',
                    'vistaCircuito',
                    'modificarTipoPlantel',
                    'MostrarFormCircuitoPlantel',
                    'VerificarCodigoPlantel',
                    'GuardarCircuitoPlantel',
                    'reporteTotalCiruitos',
                    'reporteCircuitoIndividual'
                    ),
               'pbac' => array('write'),
           ),
           array('allow',
               'actions' => array(
                    'lista',
                    'consulta',
                    'vistaCircuito',
                    'reporteTotalCiruitos',
                    'reporteCircuitoIndividual'
                    ),
               'pbac' => array('read'),
           ),
           // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
           array('deny', // deny all users
               'users' => array('*'),
           ),
       );
   }

    public function actionVistaCircuito($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $modelCircuitoPlantel = new CircuitoPlantel();
        $modelCircuitoPlantel->circuito_id = $model->id;
        $modelCircuitoPlantel->estatus = 'A';
        $modelCircuitoPlantel->plantel_integral = 'S';
        $dataProvider_integral = $modelCircuitoPlantel->search();
        $modelCircuitoPlantel->plantel_integral = 'N';
        $dataProvider_no_integral = $modelCircuitoPlantel->search();

        /* Condicion para habilitar o deshabilitar el boton de exportar circuito*/
        if($dataProvider_integral->totalItemCount <= 0
            && $dataProvider_no_integral->totalItemCount <=0)
        {
            $disabled = 'disabled';
        }else{
            $disabled = '';
        }

        $this->render('vistaCircuito',array(
            'model'=>$model,
            'dataProvider_integral'=> $dataProvider_integral,
            'dataProvider_no_integral'=> $dataProvider_no_integral,
            'disabled' => $disabled,
        ));        
    }

    /**
     * Lists all models.
     */
    public function actionLista()
    {
        $model=new Circuito('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('Circuito')){
            $model->attributes=$this->getQuery('Circuito');
        }

        if(Yii::app()->user->pbac('circuitoEscolar.circuito.admin') OR Yii::app()->user->group==UserGroups::JEFE_DRCEE){
            $condicionEstado = '';
        }else{

            $model->estado_id = Yii::app()->user->estado;
            $condicionEstado = 'disabled';

        }
        if(isset($model->estado_id) && !empty($model->estado_id))
        {
            $municipios =  CHtml::listData( Municipio::model()->findAll(array('condition'=>"estado_id=:id", 'params'=>array(':id'=>$model->estado_id))),'id','nombre');
            
            if(isset($model->municipio_id) && !empty($model->municipio_id))
            {
                $parroquias =  CHtml::listData( Parroquia::model()->findAll(array('condition'=>"municipio_id=:id", 'params'=>array(':id'=>$model->municipio_id))),'id','nombre');
            }else{
                $parroquias = array();
                $model->parroquia_id = '';
            } 
        }else{
            $municipios = array();
            $model->municipio_id = '';
            $parroquias = array();
            $model->parroquia_id = '';
        }     
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
            'condicionEstado'=>$condicionEstado,
            'municipios' => $municipios,
            'parroquias' => $parroquias,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Circuito('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('Circuito')){
            $model->attributes=$this->getQuery('Circuito');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }



    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro()
    {
        $model=new Circuito;
        //$usuarioId= Yii::app()->user->id;
        if(!Yii::app()->user->pbac('circuitoEscolar.circuito.admin'))
        {
           // $usuarioEstadoId = Yii::app()->user->estado;
           $model->estado_id= Yii::app()->user->estado;
        }

        if($this->hasPost('Circuito'))
        {
            $model->attributes=$this->getPost('Circuito');
            $model->beforeInsert();
            $model->setScenario('registroEdicionCircuito');
            if($model->save()){
                $this->registerLog('ESCRITURA', 'modulo.Circuito.registro', 'EXITOSO', 'El Registro de los datos de Circuito se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                Yii::app()->user->setFlash('success', 'El proceso de registro de los datos se ha efectuado exitosamente');
                $model=new Circuito;
                // $usuarioId= Yii::app()->user->id;
                if(!Yii::app()->user->pbac('circuitoEscolar.circuito.admin'))
                {
                   // $usuarioEstadoId = Yii::app()->user->estado;
                   $model->estado_id=Yii::app()->user->estado;
                }
            }
        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id)
    {
            $idDecoded = $this->getIdDecoded($id);
            $model = $this->loadModel($idDecoded);         
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);
            $Municipios = Municipio::model()->findAll(array('condition' => 'estado_id =' . $model->estado_id, 'order' => 'nombre ASC'));
            $Parroquias = Parroquia::model()->findAll(array('condition' => 'municipio_id =' . $model->municipio_id, 'order' => 'nombre ASC'));
            if($this->hasPost('Circuito'))
            {
                $model->attributes=$this->getPost('Circuito');
                if(!Yii::app()->user->pbac('circuitoEscolar.circuito.admin'))
                {
                   $model->estado_id= Yii::app()->user->estado;
                }                  
                $model->beforeUpdate();
                $model->setScenario('registroEdicionCircuito');
                if($model->save()){
                    $this->registerLog('ACTUALIZACION', 'modulo.Circuito.edicion', 'EXITOSO', 'La Actualización de los datos de Circuito se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                    Yii::app()->user->setFlash('success', 'El proceso de Actualización de los datos se ha efectuado exitosamente');
                    $Municipios = Municipio::model()->findAll(array('condition' => 'estado_id =' . $model->estado_id, 'order' => 'nombre ASC'));
                    $Parroquias = Parroquia::model()->findAll(array('condition' => 'municipio_id =' . $model->municipio_id, 'order' => 'nombre ASC'));
                }
            }

            $this->render('update',array(
                    'model'=>$model,
                    'Municipios'=>$Municipios,
                    'Parroquias'=>$Parroquias,
            ));
    }

    /**
     * Logical Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        
        if($model){
            $model->beforeDelete();
            if($model->save()){
                $this->registerLog('ELIMINACION', 'modulo.Circuito.eliminacion', 'EXITOSO', 'La Eliminación de los datos de Circuito se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La eliminación del registro se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }

        $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
    }
    
    /**
     * Activation of a particular model Logicaly Deleted.
     * If activation is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be activated
     */
    public function actionActivacion($id){
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        if($model){
            $model->beforeActivate();
            if($model->save()){
                $this->registerLog('ACTIVACION', 'modulo.Circuito.activacion', 'EXITOSO', 'La Activación de los datos de Circuito se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La activación de este registro se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }
        $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Circuito the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if(is_numeric($id)){
            $model=Circuito::model()->findByPk($id);
            if($model===null){
                throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
        }
        else{
            return null;
        }
    }

    /**
     * Performs the AJAX validation.
     * @param Circuito $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='circuito-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $id = base64_encode($id_encoded);
        $columna = '<div class="action-buttons">';
        //$columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'onclick'=>"consultarCircuito('$id')",/*'href' => '/circuitoEscolar/circuito/consulta/id/'.$id*/)) . '&nbsp;&nbsp;';
        $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/circuitoEscolar/circuito/vistaCircuito/id/'.$id)) . '&nbsp;&nbsp;';
        
        if(Yii::app()->user->pbac('circuitoEscolar.circuito.admin') || Yii::app()->user->pbac('circuitoEscolar.circuito.write')){
            $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '/circuitoEscolar/circuito/edicion/id/'.$id)) . '&nbsp;&nbsp;';
        }
        /*
        if($data->estatus=='A'){
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Eliminar", 'href' => '/circuitoEscolar/circuito/eliminacion/id/'.$id)) . '&nbsp;&nbsp;';
        }else{
            $columna .= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar", 'href' => '/circuitoEscolar/circuito/activacion/id/'.$id)) . '&nbsp;&nbsp;';
        }
        */
        $columna .= '</div>';
        return $columna;
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }

    public function actionBuscarMunicipios()
    {
        $item = $_REQUEST['estado_id'];
        if ($item == '' || $item == NULL) {
            $lista = array('empty' => '-Seleccione-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $lista = Municipio::model()->findAll(array('condition' => 'estado_id =' . $item, 'order' => 'nombre ASC'));
            $lista = CHtml::listData($lista, 'id', 'nombre');

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-Seleccione-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    public function actionBuscarParroquias() {
        $item = $_REQUEST['municipio_id'];

        if ($item == '' || $item == NULL) {
            $lista = array('empty' => '-Seleccione-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $lista = Parroquia::model()->findAll(array('condition' => 'municipio_id =' . $item, 'order' => 'nombre ASC'));
            $lista = CHtml::listData($lista, 'id', 'nombre');

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-Seleccione-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }
    public function actionVerificarEstadoMunicipioParroquia()
    {
       if($_POST)
       {
          $formType= $_POST['formType'];

          if($formType=='registro')
                $model=new Circuito();
          if($formType=='edicion')
          {
            $id=base64_decode($_POST['Circuito']['id']);
            $model=Circuito::model()->findByPk($id);
          }

          $model->attributes=$_POST['Circuito'];
          $model->setScenario('registroEdicionCircuito');

          //VALIDANDO FORMULARIO
          if($model->validate())
            {

                if($formType=='registro')
                {
                          if($model->tipo_circuito=='M')
                          {
                            $verificarCircuito =  Circuito::model()->findAll(array('condition'=>'estado_id = '.$model->estado_id.' AND municipio_id = '.$model->municipio_id .' AND parroquia_id is null'));
                          }
                          else{
                            $verificarCircuito =  Circuito::model()->findAll(array('condition'=>'estado_id = '.$model->estado_id.' AND  municipio_id = '.$model->municipio_id. ' AND parroquia_id = '.$model->parroquia_id));
                          }
                }
                if($formType=='edicion')
                {
                    $id= $_POST['Circuito']['id'];
                    $id = base64_decode($id);
                          if($model->tipo_circuito=='M')
                          {
                            $verificarCircuito =  Circuito::model()->findAll(array('condition'=>'estado_id = '.$model->estado_id.' AND municipio_id = '.$model->municipio_id.' AND parroquia_id is null AND id!= '.$id));
                          }
                          else{
                            $verificarCircuito =  Circuito::model()->findAll(array('condition'=>'estado_id = '.$model->estado_id.' AND  municipio_id = '.$model->municipio_id. ' AND parroquia_id = '.$model->parroquia_id .' AND id!='.$id ));
                          }
                }

                  //SI ESTA VACIO O ES UN ARRAY INDICA QUE NO EXISTEN REGISTROS CON LOS PARAMETROS INDICADOS.         
                  if($verificarCircuito==array() || empty($verificarCircuito) )
                  {
                        
                        echo json_encode(array('estatus'=>'success','mensaje'=>''));
                  }
                  //SE ENCONTRO UN REGISTRO EN B.D, MANDAR ERROR.
                  else
                  {
                        $mensaje= "Estimado Usuario, El Circuito a registrar ya se encuentra en Sistema, por favor intente nuevamente";
                        echo json_encode(array('estatus'=>'error','mensaje'=>$mensaje));
                  }
            }
            //SI OCURRIO ERROR DEVUELVO EL MENSAJE.
            else
            {
                $this->renderPartial('//errorSumMsg',array('model'=>$model),false,true);
                Yii::app()->end();
            }
       }
    }

    public function actionModificarTipoPlantel($ctrl)
    {
        if(isset($_POST["Integral"]) and !empty($_POST["Integral"]))
        {
            $planteles_a_modificar = $_POST["Integral"];
            $planteles_a_modificar = explode ( '&', $planteles_a_modificar);
            foreach ($planteles_a_modificar as $key => $value)
            {
                list($id,)= explode('=', $value);
                $id = str_replace ( '%3D' , '=' , $id);
                $id =$this->getIdDecoded($id);
                if(is_numeric($id)){
                    $model=CircuitoPlantel::model()->findByPk($id);
                    if($model===null){
                        throw new CHttpException(404,'The requested page does not exist.');
                    }
                }               
                if($ctrl == 1){
                    $model->plantel_integral = 'S';
                }elseif ($ctrl == 2) {
                    $model->plantel_integral = 'N';
                }else{
                    throw new CHttpException(404,'The requested page does not exist.');
                }
                $model->beforeUpdate();
                if($model->save()){
                    $this->registerLog('ACTUALIZACION', 'modulo.CircuitoPlantel.edicion', 'EXITOSO', 'La Actualización de los datos de CircuitoPlantel se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                }else{
                    
                    $mensaje = 'Estimado usuario ocurrió un error y no se pudieron almacenar los datos. Por favor recargue la página e intente nuevamente';
                    echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
                    Yii::app()->end();                
                }
            }

            $mensaje = 'Se han modificado los datos de forma exitosa';
            echo json_encode(array('status'=>'success','mensaje'=>$mensaje));
            Yii::app()->end();          
        }        
    }

    public function actionMostrarFormCircuitoPlantel()
    {
        if($_POST){
                            $circuitoId=base64_decode($_POST['circuito_id']);
                            $model = new CircuitoPlantel();
                            $model->circuito_id=$circuitoId;
                            $this->renderPartial('_circuitoPlantel', array('model' => $model));
                  }
    }

    public function actionVerificarCodigoPlantel()
    {
        if($_POST){
                            $codigoDea=$_POST['codigo_dea'];
                            $circuitoId=base64_decode($_POST['circuito_id']);

                            if(is_numeric($circuitoId)==true)
                                $circuito= Circuito::model()->find("id=".$circuitoId);

                            $Plantel= Plantel::model()->find("cod_plantel = '$codigoDea' ");

                            //print_r($Plantel); die();

                            //print_r($circuito); die();

                            if( $Plantel!=array() )
                            {
                                $circuitoPlantel= CircuitoPlantel::model()->find('plantel_id ='.$Plantel->id);

                               // print_r($circuitoPlantel); die();

                               if($circuitoPlantel==array())
                                { 
                                                if($circuito->parroquia_id=='')
                                                {
                                                    if($circuito->estado_id==$Plantel->estado_id AND $circuito->municipio_id==$Plantel->municipio_id)
                                                    {
                                                        $estatus='success';
                                                        $mensaje='';
                                                        $plantel=$Plantel->nombre;
                                                        $plantelId=base64_encode($Plantel->id);
                                                    }
                                                    else
                                                    {
                                                        $plantelId='';
                                                        $plantel='';
                                                        $estatus='error';
                                                        $mensaje='Estimado Usuario, El Plantel Ingresado no pertenece al Estado y Municipio del Circuito en Cuestion,verifique e Intente Nuevamente';
                                                    }
                                                }
                                                else
                                                {
                                                    if($circuito->estado_id==$Plantel->estado_id AND $circuito->municipio_id==$Plantel->municipio_id AND $circuito->parroquia_id==$Plantel->parroquia_id)
                                                       {
                                                        $estatus='success';
                                                        $mensaje='';
                                                        $plantel=$Plantel->nombre;
                                                        $plantelId=base64_encode($Plantel->id);
                                                       } 
                                                    else
                                                    {
                                                        $plantelId='';
                                                        $plantel='';
                                                        $estatus='error';
                                                        $mensaje='Estimado Usuario, El Plantel Ingresado no pertenece al Estado,Municipio y Parroquia del Circuito en Cuestion,verifique e Intente Nuevamente';
                                                    }

                                                }

                                }
                                else
                                {
                                                        $plantelId='';
                                                        $plantel='';
                                                        $estatus='error';
                                                        $mensaje='Estimado Usuario, El Plantel Ingresado se Encuentra registrado en el Cicruito: "'.strtoupper($circuitoPlantel->circuito->nombre_circuito).'", verifique e Intente Nuevamente';
                                }


                            }
                            else
                            {
                                $plantelId='';
                                $estatus='error';
                                $mensaje='Estimado Usuario, El Codigo de Plantel Ingresado no se encuentra registrado en Sistema, Verifique e Intente Nuevamente.';
                                $plantel='';
                            }

                            echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje,'plantel'=>$plantel,'plantelId'=>$plantelId));
                  }
    }

    public function actionGuardarCircuitoPlantel()
    {
        if($_POST)
        {

            $model= new CircuitoPlantel();
            $model->attributes=$this->getPost('CircuitoPlantel');
            $codigoPlantel=$model->plantel_id;
            $model->circuito_id=base64_decode($model->circuito_id);
            $model->beforeInsert();

            if($codigoPlantel!='' || !empty($codigoPlantel))
            {
                $plantel= Plantel::model()->find("cod_plantel= '$model->plantel_id' ");
                $model->plantel_id= $plantel->id;
                
                if($model->validate() )
                {
                    if($model->save())
                    echo json_encode(array('estatus'=>'success','mensaje'=>'Registro Guardado Satisfactoriamente.'));
                    else
                    echo json_encode(array('estatus'=>'error','mensaje'=>'Error al Guardar el Plantel en el Circuito, recargue e Intente Nuevamente.'));
                }
                else
                {
                    $this->renderPartial('//errorSumMsg',array('model'=>$model),false,true);
                    Yii::app()->end();
                }
            }
            else
            {
                    echo json_encode(array('estatus'=>'error','mensaje'=>'Error, debe Ingresar un Codigo de Plantel'));
            }
        }
    }
    
    public function actionReporteTotalCiruitos()
    {
        $headers = array(
            'id',
            'Estado',
            'Municipio',
            'Parroquia',
            'Nombre Circuito',
            'Tipo circuito',
            'Código Plantel',
            'Nombre Plantel',
            'Tipo Plantel',
            'Nombre del Director',
            'Numero de teléfono',
            'Correo',
        );
        
        $colDef = array(
            'id'=>array(),
            'estado'=>array(),
            'municipio'=>array(),
            'parroquia'=>array(),
            'nombre_circuito' => array(),
            'tipo_circuito'=> array(),
            'cod_plantel'=> array(),
            'plantel' => array(),
            'tipo_plantel' => array(),
            'nombre_director' => array(),
            'telefono_director' => array(),
            'correo_director' => array(),
        );   

        $model = new Circuito();
        $dataReport = $model->reporteCircuitoGeneral();

        $fileName = 'Circuitos_' . date('Y-m-d') . '.csv';
        
        CsvExport::export(
            $dataReport, // a CActiveRecord array OR any CModel array
            $colDef, 
            $headers, 
            true, 
            $fileName,
            ';',
            false
        );
    }

    public function actionReporteCircuitoIndividual($id)
    {

        

        $id = $this->getIdDecoded($id);
        $model = $this->loadModel($id);

        if($model->tipo_circuito=='M')
        {
            $headers = array(
                'id',
                'Estado',
                'Municipio',
                'Nombre Circuito',
                'Tipo circuito',
                'Código Plantel',
                'Nombre Plantel',
                'Tipo Plantel',
                'Nombre del Director',
                'Numero de teléfono',
                'Correo',
            );
            $colDef = array(
                'id'=>array(),
                'estado'=>array(),
                'municipio'=>array(),
                'nombre_circuito' => array(),
                'tipo_circuito'=> array(),
                'cod_plantel'=> array(),
                'plantel' => array(),
                'tipo_plantel' => array(),
                'nombre_director' => array(),
                'telefono_director' => array(),
                'correo_director' => array(),
            );            
        }elseif($model->tipo_circuito=='P')
        {

            $headers = array(
                'id',
                'Estado',
                'Municipio',
                'Parroquia',
                'Nombre Circuito',
                'Tipo circuito',
                'Nombre Plantel',
                'Tipo Plantel',
                'Nombre del Director',
                'Numero de teléfono',
                'Correo',
            );
            $colDef = array(
                'id'=>array(),
                'estado'=>array(),
                'municipio'=>array(),
                'parroquia'=>array(),
                'nombre_circuito' => array(),
                'tipo_circuito'=> array(),
                'plantel' => array(),
                'tipo_plantel' => array(),
                'nombre_director' => array(),
                'telefono_director' => array(),
                'correo_director' => array(),
            );             
        }        

        $dataReport = $model->reporteCircuitoIndividual();
       
        $fileName = 'Circuitos_' . date('Y-m-d') . '.csv';
        
        CsvExport::export(
            $dataReport, // a CActiveRecord array OR any CModel array
            $colDef, 
            $headers, 
            true, 
            $fileName,
            ';',
            false
        );
    }    

        public function obtenerLink($data) 
        {
                           if (is_object($data->plantel) && isset($data->plantel->cod_plantel) )
                           {

                            //$link='<a href='.Yii::app()->createUrl("/planteles/consultar/informacion/?id=" . base64_encode($data->id)).'>'.$data->plantel->cod_plantel.'</a>';
                            $columna = CHtml::link( $data->plantel->cod_plantel, Yii::app()->createUrl("/planteles/consultar/informacion/?id=" . base64_encode($data->plantel_id)), array("target" => "_blank"));
                            return $columna;
                           }
                            
                           else
                            return '';
        }
}
