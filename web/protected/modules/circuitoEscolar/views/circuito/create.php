<?php
/* @var $this CircuitoController */
/* @var $model Circuito */

$this->pageTitle = 'Registro de Circuitos';
      $this->breadcrumbs=array(
	'Circuitos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/circuitoEscolar/circuito/circuito.js', CClientScript::POS_END);
?>
