<?php

/* @var $this CircuitoController */
/* @var $model Circuito */

$this->breadcrumbs=array(
	'Circuitos'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Circuitos';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Circuitos</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="dialogPantalla"></div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Circuitos.
                            </p>
                        </div>
                    </div>

                    <div class="pull-left" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/circuitoEscolar/circuito/reporteTotalCiruitos"); ?>" id='exportar' data-last="Finish" class="btn btn-primary btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Exportar Totales                        
                        </a>
                    </div>
                    <?php if ( Yii::app()->user->pbac('circuitoEscolar.circuito.admin') ): ?>
                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/circuitoEscolar/circuito/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Circuito
                        </a>
                    </div>
                <?php endif; ?>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'circuito-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                        $('#Circuito_nombre_circuito').on('keyup keydown',function(){
                            keyAlphaNum(this,true);
                            makeUpper(this);
                        });
                }",
	'columns'=>array(
        /*
        array(
            'header' => '<center>Id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Circuito[id]', $model->id, array('title' => '',)),
        ),
        */
        array(
            'header' => '<center>Nombre del Circuito</center>',
            'name' => 'nombre_circuito',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Circuito[nombre_circuito]', $model->nombre_circuito, array('title' => '',)),
        ),
        array(
            'header' => '<center>Estado</center>',
            'name' => 'estado_id',
            'value' => '(is_object($data->estado) && isset($data->estado->nombre))? $data->estado->nombre: ""',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[estado_id]', $model->estado_id, array('title' => '',)),
            'filter' => CHtml::dropDownList('Circuito[estado_id]',$model->estado_id,CHtml::listData(CEstado::getData(), 'id', 'nombre'),array($condicionEstado=>$condicionEstado,'empty' => array('' => ''),'title'=>'Estados')),
        ),
        array(
            'header' => '<center>Municipio</center>',
            'name' => 'municipio_id',
            'value' => '(is_object($data->municipio) && isset($data->municipio->nombre))? $data->municipio->nombre: ""',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[municipio_id]', $model->municipio_id, array('title' => '',)),
            //'filter' => CHtml::listData(CMunicipio::getData(),'id','nombre'),
            'filter' => CHtml::dropDownList('Circuito[municipio_id]',$model->municipio_id,$municipios,array('empty' => array('' => ''),'title'=>'Municipios')),        
        ),
        array(
            'header' => '<center>Parroquia</center>',
            'name' => 'parroquia_id',
            'value' => '(is_object($data->parroquia) && isset($data->parroquia->nombre))? $data->parroquia->nombre: ""',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[parroquia_id]', $model->parroquia_id, array('title' => '',)),
            'filter' => CHtml::dropDownList('Circuito[parroquia_id]',$model->parroquia_id,$parroquias,array('empty' => array('' => ''),'title'=>'Parroquia')),        
        ),
        array(
            'header' => '<center>Tipo de Circuito</center>',
            'name' => 'tipo_circuito',
            'value' => '($data->tipo_circuito == "M")? "Municipal": "Parroquial"',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[tipo_circuito]', $model->tipo_circuito, array('title' => '',)),
            'filter' => CHtml::dropDownList('Circuito[tipo_circuito]',$model->tipo_circuito,array('M'=>'Municipal','P'=>'Parroquial'),array('empty' => array('' => ''),'title'=>'Parroquia')),        
        ),
		/*
        array(
            'header' => '<center>usuario_ini_id</center>',
            'name' => 'usuario_ini_id',
            'htmlOptions' => array(),
                        'filter' => CHtml::textField('Circuito[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
                    ),
        array(
            'header' => '<center>Fecha_ini</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Circuito[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),
        array(
            'header' => '<center>Usuario_act_id</center>',
            'name' => 'usuario_act_id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Circuito[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Fecha_act</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Circuito[fecha_act]', $model->fecha_act, array('title' => '',)),
        ),
        array(
            'header' => '<center>Fecha_elim</center>',
            'name' => 'fecha_elim',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Circuito[fecha_elim]', $model->fecha_elim, array('title' => '',)),
        ),
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Circuito[estatus]', $model->estatus, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>

<?php
    /**
     * Yii::app()->clientScript->registerScriptFile(
     *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/CircuitoController/circuito/admin.js', CClientScript::POS_END
     *);
     */
?>

<script type="text/javascript">

$(document).ready(function(){

    $('#Circuito_nombre_circuito').on('keyup keydown',function(){
        keyAlphaNum(this,true);
        makeUpper(this);
    });
});
</script>