<?php
/* @var $this CircuitoController */
/* @var $model Circuito */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'circuito-form',
                                'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>


                        

                        <div id="div-result">
                        <?php
                                
    if(Yii::app()->user->hasFlash('success')){

    $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => Yii::app()->user->getFlash('success')));
           
    }
               ?>
               
                        </div>
                        <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                        
                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <?php 
                                                        $model->id=base64_encode($model->id);
                                                        echo $form->hiddenField($model,'id',array('readonly'=>'readonly')); ?>

                                                        <div class="col-md-4">
                                                            <?php  echo CHtml::label('Nombre del Circuito <span class="required">*</span>',''); ?>
                                                            <?php echo $form->textField($model,'nombre_circuito',array('size'=>30, 'maxlength'=>30, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php 
                                                            if(!Yii::app()->user->pbac('circuitoEscolar.circuito.admin'))
                                                            {
                                                                $condicion='disabled';
                                                                echo "<input type='hidden' name='Circuito[estado_id]' value=".$model->estado_id.">";
                                                            }
                                                            
                                                            else
                                                            $condicion='';
                                                            echo CHtml::label('Estado <span class="required">*</span>','');
                                                            ?>
                                                            <?php 
                                                            echo $form->dropDownList($model, 'estado_id', CHtml::listData(Estado::model()->findAll(array('limit'=>50)), 'id', 'nombre'), array('prompt'=>'-Seleccione-', 'class' => 'span-12', "required"=>"required", $condicion => $condicion )); 
                                                            ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <?php echo CHtml::label('Tipo de Circuito <span class="required">*</span>',''); ?>
                                                            <?php echo $form->dropDownList($model,'tipo_circuito',array('M'=>'Municipal','P'=>'Parroquial'),array('empty'=>'-Seleccione-','size'=>1, 'maxlength'=>1, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>
                                                        

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">
                                                    <?php 
                                                            if($formType=='registro')
                                                            {
                                                                $municipio='hide';
                                                                $parroquia='hide';
                                                            }
                                                            else
                                                                {
                                                                 $municipio='';
                                                                 $parroquia='';
                                                                }
                                                            if($formType=='edicion')
                                                            {
                                                                if($model->municipio_id=='')
                                                                    $municipio='hide';
                                                                else
                                                                    $municipio='';
                                                                if($model->parroquia_id=='')
                                                                    $parroquia='hide';
                                                                else
                                                                    $parroquia='';
                                                            }

                                                     ?>
                                                        <div class="col-md-4 municipio <?php echo $municipio; ?>">
                                                            <?php echo CHtml::label('Municipio <span class="required">*</span>','');
                                                                    if($formType=='registro')
                                                                    {
                                                                        $listaMunicipios=array();
                                                                    }
                                                                    else
                                                                    {
                                                                        $listaMunicipios=CHtml::listData($Municipios,'id','nombre');
                                                                    }
                                                             ?>
                                                            <?php echo $form->dropDownList($model, 'municipio_id',$listaMunicipios, array('prompt'=>'-Seleccione-', 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>
                                                        <div class="col-md-4 parroquia <?php echo $parroquia; ?>">
                                                            <?php echo CHtml::label('Parroquia <span class="required">*</span>','');
                                                                if($formType=='registro')
                                                                    {
                                                                        $listaParroquias=array();
                                                                    }
                                                                    else
                                                                    {
                                                                        $listaParroquias=CHtml::listData($Parroquias,'id','nombre');
                                                                    }
                                                             ?>
                                                            <?php echo $form->dropDownList($model, 'parroquia_id', $listaParroquias, array('prompt'=>'-Seleccione-', 'class' => 'span-12', )); ?>
                                                        </div>


                   

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">



                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/circuitoEscolar/circuito"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                    <div class="col-md-6 wizard-actions">
                                        <button id='btnRegistrarCircuito' class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                            Guardar
                                            <i class="icon-save icon-on-right"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <div id="resultDialog" class="hide"></div>

        <div id="dialog_error" class="hide">
            
        </div>
        <?php
            /**
             * Yii::app()->clientScript->registerScriptFile(
             *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/CircuitoController/circuito/form.js',CClientScript::POS_END
             *);
             */
        ?>
    </div>
</div>
