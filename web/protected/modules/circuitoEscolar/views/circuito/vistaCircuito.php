<?php

/* @var $this CircuitoController */
/* @var $model CircuitoPlantel */

$this->breadcrumbs=array(
    'Circuitos'=>array('lista'),
    'Planteles del Circuito Escolar',
);
$this->pageTitle = 'Planteles del Circuito Escolar';

?>
<div>
    <div id="resultadoOperacion">
        <div class="infoDialogBox">
            <p>
                Planteles que pertenecen al Circuito Escolar.
            </p>
        </div>
    </div>
    <div id="dialogPantalla"></div>
</div>
<div class="row space-12"></div>

<div class="pull-left" style="padding-left:10px;margin-bottom :20px;">
    <a href="<?php echo $this->createUrl("/circuitoEscolar/circuito/reporteCircuitoIndividual/id/".base64_encode($model->id)); ?>" id='exportar' data-last="Finish" class="btn btn-primary btn-next btn-sm <?php echo $disabled ?>">
        <i class="fa fa-plus icon-on-right"></i>
            Exportar Datos del Circuito                        
    </a>
</div>
<?php if ( Yii::app()->user->pbac('circuitoEscolar.circuito.admin') || Yii::app()->user->pbac('circuitoEscolar.circuito.write') ): ?>
<div class="pull-right" style="padding-left:10px;margin-bottom :20px;">
    <a href="#" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
    <i class="fa fa-plus icon-on-right"></i>
        Registrar Plantel en el Circuito</a>
</div>
<?php endif; ?>

<table class="table table-striped table-bordered" id="table">
    <tr class="danger">
        <th style="text-align:center;">
            Nombre
        </th>
        <th style="text-align:center;">
            Estado
        </th>
        <th style="text-align:center;">
            Municipio
        </th>
        <?php if($model->tipo_circuito=='P'): ?>
            <th style="text-align:center;">
                Parroquia
            </th>
        <?php endif; ?>
    </tr>
    <tr>
        <td style="text-align:center;">
            <?php echo $model->nombre_circuito ?>
        </td>
        <td style="text-align:center;">
            <?php echo $model->estado->nombre ?>
        </td>
        <td style="text-align:center;">
            <?php echo $model->municipio->nombre ?>
        </td>
        <?php if($model->tipo_circuito=='P'): ?>
            <td style="text-align:center;">
                <?php echo $model->parroquia->nombre ?>
            </td>
        <?php endif; ?>
    </tr>
</table>
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<input id="circuito" type="hidden" value="<?php echo base64_encode($model->id); ?>">
<div id="dialogoCircuitoPlantel" class="hide"></div>

<?php if ( Yii::app()->user->pbac('circuitoEscolar.circuito.admin') || Yii::app()->user->pbac('circuitoEscolar.circuito.write') ): ?>
<div class="pull-left" style="padding-left:10px;">
    <a class="btn btn-primary disabled" href="#resultadoOperacion" id="pasarIntegral" onclick="confirmarAccion(1)">
        Cambiar Plantel(es) a tipo Integral
        <i class="icon-arrow-right"></i>
    </a>
</div>
<div class="pull-right" style="padding-right:10px;margin-bottom :20px;">
    <a class="pull-right btn btn-warning disabled" href="#resultadoOperacion" id="pasarRegular" onclick="confirmarAccion(2)">
        <i class="icon-arrow-left"></i>
            Cambiar Plantel(es) a tipo Regular
    </a>
</div>

<?php endif; ?>

<div class="col-md-12">
    <div class="col-md-6">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'circuito-plantel-no-integral-grid',
            'dataProvider'=>$dataProvider_no_integral,
                //'filter'=>$model,
                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                'summaryText' => '',
                'afterAjaxUpdate' => "
                        function(){

                            $('#todos_selected_regular').on('change',function(){
                                    if(this.checked){
                                        $('.to_integral').prop('checked',true);
                                        selector = '.to_integral';
                                        div = '#pasarIntegral';
                                        verificarCheckboxes(selector,div);            
                                    }else{
                                        $('.to_integral').prop('checked',false);
                                        $('#pasarIntegral').addClass('disabled');
                                    }
                                });

                                $('#todos_selected_integral').on('change',function(){
                                    if(this.checked){
                                        $('.to_regular').prop('checked',true);
                                        selector = '.to_regular';
                                        div = '#pasarRegular';
                                        verificarCheckboxes(selector,div);            
                                    }else{
                                        $('.to_regular').prop('checked',false);
                                        $('#pasarRegular').addClass('disabled');
                                    }
                                });

                                $('.to_regular').on('change',function(){
                                    
                                    selector = '.to_regular';
                                    div = '#pasarRegular';
                                    verificarCheckboxes(selector,div);        
                                });

                                $('.to_integral').on('change',function(){
                                    selector = '.to_integral';
                                    div = '#pasarIntegral';
                                    verificarCheckboxes(selector,div);
                                }); 
                        }",
            'columns'=>array(
                
                array(
                    'header' => CHtml::CheckBox('todos_selected_regular', false,array ( )),
                    //'name' => 'id',
                    //'value'=> CHtml::checkBox("to_integral", false,array ( )),
                    'type'=>'raw',
                    'value' => 'CHtml::checkBox(base64_encode($data->id),false,array("class"=>"to_integral","value"=>base64_encode($data->id)))',
                    'htmlOptions' => array(),
                ),

                array(
                    'type'=>'raw',
                    'header' => '<center>Código Plantel</center>',
                    //'name' => 'circuito_id',
                    //'value' => '(is_object($data->plantel) && isset($data->plantel->cod_plantel) )? "<a href=".Yii::app()->createUrl("."/planteles/consultar/informacion/?id=" . base64_encode($data->id))." target="."_blank >".$data->plantel->cod_plantel."</a>" : ""',
                    //'htmlOptions' => array(),
                    'value'  => array($this,'obtenerLink'),
                    //'value' => ($this,'obtenerLink'),
                ),
                array(
                    'header' => '<center>Nombre del Plantel</center>',
                    'name' => 'plantel_id',
                    'value'=> '(is_object($data->plantel) && isset($data->plantel->nombre) )? $data->plantel->nombre: ""',
                    'htmlOptions' => array(),
                ),
                array(
                    'header' => '<center>Tipo de Plantel</center>',
                    'name' => 'plantel_integral',
                    'value' => '($data->plantel_integral == "S")? "Integral": "Regular"',
                    'htmlOptions' => array(),
                ),
                /*
                array(
                    'header' => '<center>Estado</center>',
                    'name' => 'plantel_integral',
                    'value'=> '(is_object($data->plantel) && isset($data->plantel->estado->nombre) )? $data->plantel->estado->nombre: ""',
                    'htmlOptions' => array(),
                ),
                array(
                    'header' => '<center>Municipio</center>',
                    'name' => 'plantel_integral',
                    'value'=> '(is_object($data->plantel) && isset($data->plantel->municipio->nombre) )? $data->plantel->municipio->nombre: ""',
                    'htmlOptions' => array(),
                ),
                array(
                    'header' => '<center>Parroquia</center>',
                    'name' => 'plantel_integral',
                    'value'=> '(is_object($data->plantel) && isset($data->plantel->parroquia->nombre) )? $data->plantel->parroquia->nombre: ""',
                    'htmlOptions' => array(),
                ),*/                               
            ),
        )); ?>
    </div>
    <div class="col-md-6">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'circuito-plantel-integral-grid',
            'dataProvider'=>$dataProvider_integral,
                //'filter'=>$model,
                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                'summaryText' => '',
                'afterAjaxUpdate' => "
                        function(){

                        $('#todos_selected_regular').on('change',function(){
                                if(this.checked){
                                    $('.to_integral').prop('checked',true);
                                    selector = '.to_integral';
                                    div = '#pasarIntegral';
                                    verificarCheckboxes(selector,div);            
                                }else{
                                    $('.to_integral').prop('checked',false);
                                    $('#pasarIntegral').addClass('disabled');
                                }
                            });

                            $('#todos_selected_integral').on('change',function(){
                                if(this.checked){
                                    $('.to_regular').prop('checked',true);
                                    selector = '.to_regular';
                                    div = '#pasarRegular';
                                    verificarCheckboxes(selector,div);            
                                }else{
                                    $('.to_regular').prop('checked',false);
                                    $('#pasarRegular').addClass('disabled');
                                }
                            });

                            $('.to_regular').on('change',function(){
                                
                                selector = '.to_regular';
                                div = '#pasarRegular';
                                verificarCheckboxes(selector,div);        
                            });

                            $('.to_integral').on('change',function(){
                                selector = '.to_integral';
                                div = '#pasarIntegral';
                                verificarCheckboxes(selector,div);
                            }); 
                        }",
            'columns'=>array(

                array(
                    'header' => CHtml::CheckBox('todos_selected_integral', false,array ( )),
                    //'name' => 'id',
                    //'value'=> CHtml::checkBox("to_integral", false,array ( )),
                    'type'=>'raw',
                    'value' => 'CHtml::checkBox(base64_encode($data->id),false,array("class"=>"to_regular","value"=>base64_encode($data->id)))',
                    'htmlOptions' => array(),
                ),

                array(
                    'type'=>'raw',
                    'header' => '<center>Código Plantel</center>',
                    //'name' => 'circuito_id',
                     'value'  => array($this,'obtenerLink'),
                    //'htmlOptions' => array(),
                ),
                array(
                    'header' => '<center>Nombre del Plantel</center>',
                    'name' => 'plantel_id',
                    'value'=> '(is_object($data->plantel) && isset($data->plantel->nombre) )? $data->plantel->nombre: ""',
                    'htmlOptions' => array(),
                ),
                array(
                    'header' => '<center>Tipo de Plantel</center>',
                    'name' => 'plantel_integral',
                    'value' => '($data->plantel_integral == "S")? "Integral": "Regular"',
                    'htmlOptions' => array(),
                ),
               
            ),
        )); ?>
    </div>    
</div>

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-danger" href="<?php echo $this->createUrl("/circuitoEscolar/circuito"); ?>" id="btnRegresar">
            <i class="icon-arrow-left"></i>
                Volver
        </a>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/circuitoEscolar/circuito/vistaCircuito.js', CClientScript::POS_END);
?>

<script type="text/javascript">
    
$(document).ready(function(){

    $('#todos_selected_regular').on('change',function(){
        if(this.checked){
            $('.to_integral').prop('checked',true);
            selector = '.to_integral';
            div = '#pasarIntegral';
            verificarCheckboxes(selector,div);            
        }else{
            $('.to_integral').prop('checked',false);
            $('#pasarIntegral').addClass('disabled');
        }
    });

    $('#todos_selected_integral').on('change',function(){
        if(this.checked){
            $('.to_regular').prop('checked',true);
            selector = '.to_regular';
            div = '#pasarRegular';
            verificarCheckboxes(selector,div);            
        }else{
            $('.to_regular').prop('checked',false);
            $('#pasarRegular').addClass('disabled');
        }
    });

    $('.to_regular').on('change',function(){
        
        selector = '.to_regular';
        div = '#pasarRegular';
        verificarCheckboxes(selector,div);        
    });

    $('.to_integral').on('change',function(){
        selector = '.to_integral';
        div = '#pasarIntegral';
        verificarCheckboxes(selector,div);
    }); 
});

function verificarCheckboxes(selector, div)
{
    datos = $(selector).serialize();
    if (datos == '')
    {
        $(div).addClass('disabled');
    }else{
        $(div).removeClass('disabled');
    }
}

function confirmarAccion(ctrl){
    
    switch(ctrl){
        case 1:
            datos = $('.to_integral').serialize();
            break;
        case 2:
            datos = $('.to_regular').serialize();
            break;
        default:
            return false;
    }
    if (datos == '') {
        return false;
    }
    var title = 'Modificar Tipo de Plantel'
    if(ctrl == 1)var contenido = '<div class="alertDialogBox"><p class="bolder center grey"> ¿Desea modificar plantel(es) a tipo integral? </p></div>'
    if(ctrl == 2)var contenido = '<div class="alertDialogBox"><p class="bolder center grey"> ¿Desea modificar plantel(es) a tipo regular? </p></div>'
    $("#dialogPantalla").html(contenido);

    var dialog = $("#dialogPantalla").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
        title_html: true,

        buttons: [
        {
            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
            "class": "btn btn-xs",

            click: function() {
                $(this).dialog("close");
            }
        },
        {
            html: "<i class='icon-check bigger-110'></i>&nbsp; Modificar",
            "class": "btn btn-success btn-xs",
            
            click: function(){
                modificarTipoPlantel(ctrl);
                $(this).dialog("close");
            }
        }
        ],
    });
}


function modificarTipoPlantel(ctrl){
    if(ctrl==1)
    {
        var datos = $('.to_integral').serialize();
    }else if(ctrl==2)
    {
        var datos = $('.to_regular').serialize();  
    }

    var data = {Integral:datos};
    var urlDir = '../../modificarTipoPlantel/ctrl/'+ctrl;
    var divResult='resultadoOperacion';
    var loadingEfect=true;
    var showResult=true;
    var method='POST';
    var responseFormat='html';

    var beforeSendCallback=function(){
        $('#'+divResult).html('');
    };

    var successCallback;
    var errorCallback=function(){};

    successCallback=function(datahtml, resp2, resp3) {
        try {

            var response = jQuery.parseJSON(resp3.responseText);

            if(response.status =='success' || response.status =='error'){

                if(response.status =='success') 
                {
                    refrescarGrid();
                    if(ctrl==1)
                    {
                        $('#pasarIntegral').addClass('disabled');
                    }else if(ctrl==2)
                    {
                        $('#pasarRegular').addClass('disabled');
                    }
                }

                displayDialogBox(divResult, response.status, response.mensaje,true,true);
            }
        }catch(e){
            
            displayHtmlInDivId(divResult, datahtml,true);
        }
    };

    executeFormatedAjax(divResult, urlDir, data, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);
}
function refrescarGrid(){
    
    $('#circuito-plantel-no-integral-grid').yiiGridView('update', {
    data: $(this).serialize()
    });
    $('#circuito-plantel-integral-grid').yiiGridView('update', {
    data: $(this).serialize()
    });
}
</script>