<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">

                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'circuito-plantel-form',
                            ));
                        ?>
                        <div id="msj_error" class="errorDialogBox hide">
                            <p>

                            </p>
                        </div>

            <div id="div-result"></div>
            <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                           <?php 
                                            $model->circuito_id=base64_encode($model->circuito_id);
                                            echo $form->hiddenField($model,'circuito_id');
                                            //echo CHtml::hiddenField('plantelId','');
                                            echo CHtml::label('Código DEA del Plantel <span class="required">*</span>',''); 
                                            echo $form->textField($model,'plantel_id',array('maxlength'=>10));
                                                            ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <?php 
                                            echo CHtml::label('Nombre del Plantel <span class="required">*</span>','');
                                            echo CHtml::textField('nombre_plantel','',array('readonly'=>'readonly'));                 
                                                             ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <?php 
                                            echo CHtml::label('Tipo de Plantel <span class="required">*</span>','');
                                            echo $form->dropDownList($model,'plantel_integral',array('N'=>'Regular','S'=>'Integral'),array('empty'=>'-Seleccione-','class' => 'span-12','required'=>'required'));
                                                             ?>       
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>
            </div>
        </div>
        <div id="resultDialog" class="hide"></div>
        <div id="dialog_error" class="hide"></div>
    </div>
</div>