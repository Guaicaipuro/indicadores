<?php

/* @var $this CircuitoController */
/* @var $model Circuito */

$this->breadcrumbs=array(
	'Circuitos'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Circuitos';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Circuitos</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Circuitos.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/circuitoEscolar/circuito/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Circuitos                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'circuito-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[id]', $model->id, array('title' => '',)),
        ),
        array(
            'header' => '<center>nombre_circuito</center>',
            'name' => 'nombre_circuito',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[nombre_circuito]', $model->nombre_circuito, array('title' => '',)),
        ),
        array(
            'header' => '<center>estado_id</center>',
            'name' => 'estado_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[estado_id]', $model->estado_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>municipio_id</center>',
            'name' => 'municipio_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[municipio_id]', $model->municipio_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>parroquia_id</center>',
            'name' => 'parroquia_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[parroquia_id]', $model->parroquia_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>tipo_circuito</center>',
            'name' => 'tipo_circuito',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[tipo_circuito]', $model->tipo_circuito, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>usuario_ini_id</center>',
            'name' => 'usuario_ini_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_ini</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_act_id</center>',
            'name' => 'usuario_act_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_act</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[fecha_act]', $model->fecha_act, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_elim</center>',
            'name' => 'fecha_elim',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[fecha_elim]', $model->fecha_elim, array('title' => '',)),
        ),
        array(
            'header' => '<center>estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Circuito[estatus]', $model->estatus, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>