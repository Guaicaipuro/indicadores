<?php
/* @var $this CircuitoController */
/* @var $model Circuito */

$this->pageTitle = 'Actualización de Datos de Circuitos';
      $this->breadcrumbs=array(
	'Circuitos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model,'Municipios'=>$Municipios,'Parroquias'=>$Parroquias, 'formType'=>'edicion')); ?>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/circuitoEscolar/circuito/circuito.js', CClientScript::POS_END);
?>
