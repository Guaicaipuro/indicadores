<?php

/* @var $this ConsultaController */
/* @var $model Saime */
?>

<div class="col-xs-12">
    <div class="row row-fluid">
        <div class="widget-box">
            <div class="widget-header">
                <h5>Búsqueda Saime</h5>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="widget-body-inner">
                    <div class="widget-main form" >
                        <div class="form" id="_form">
                            <div class="row">
                                <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'saime-form',
                                        'action'=>Yii::app()->createUrl('/saime/consulta/buscar'),
                                        'enableClientValidation'=>true,
                                        'focus'=>array($model,'cedula'),
                                        'clientOptions' => array(
                                            'validateOnSubmit' => true,
                                        ),
                                        'enableAjaxValidation'=>false,
                                    )
                                ); ?>
                                <?php echo $form->errorSummary($model); ?>
                                <div class="col-md-3">
                                    <?php echo $form->labelEx($model,'origen',array('class','col-md-12','style'=>'font-weight:bold')); ?>
                                    <?php echo $form->dropDownList($model, 'origen', array('V' => 'Venezolana', 'E' => 'Extranjera'), array('class' => 'col-md-12')); ?>

                                </div>
                                <div class="col-md-4">
                                    <?php echo $form->labelEx($model,'cedula',array('class'=>'col-md-12','style'=>'font-weight:bold')); ?>
                                    <?php echo $form->textField($model,'cedula', array('class' => 'col-md-12', 'maxlength'=> '8')); ?>
                                    <?php echo $form->error($model, 'cedula'); ?>
                                </div>
                                <div class="col-md-3">
                                    <div class="col-md-6 wizard-actions">
                                        <button class="btn btn-primary btn-sm" style="margin-top: 22px" data-last="Finish" type="submit">
                                            Buscar
                                            <i class="icon-search icon-on-right"></i>
                                        </button>
                                    </div>
                                </div>
                                <?php $this->endWidget(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#Saime_cedula').bind('keyup', function() {
            keyNum(this, true);
        });
        $('#Saime_cedula').bind('blur', function() {
            clearField(this);
        });
    });
</script>