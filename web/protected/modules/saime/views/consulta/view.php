<?php
/* @var $this ConsultaController */
/* @var $model Saime */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
    'Saime'=>array('lista'),

);

//-------Seccion de extraccion y modelado de los datos del arreglo------
if ($model != false ){
$origen = isset($model['origen']) ? $model['origen'] : '';
if($origen == 'V'){
    $origen = 'VENEZOLANO';
}else $origen = 'EXTRANJERO';
$cedula = isset($model['cedula']) ? $model['cedula'] : '';
$pais_origen = isset($model['pais_origen']) ? $model['pais_origen'] : '';
$nacionalidad = isset($model['nacionalidad']) ? $model['nacionalidad'] : '';
$primer_nombre = isset($model['primer_nombre']) ? $model['primer_nombre'] : '';
$primer_nombre = strtoupper($primer_nombre);
$segundo_nombre = isset($model['segundo_nombre']) ? $model['segundo_nombre'] : '';
$segundo_nombre = strtoupper($segundo_nombre);
$primer_apellido = isset($model['primer_apellido']) ? $model['primer_apellido'] : '';
$primer_apellido = strtoupper($primer_apellido);
$segundo_apellido = isset($model['segundo_apellido']) ? $model['segundo_apellido'] : '';
$segundo_apellido = strtoupper($segundo_apellido);
$fecha_nacimiento = isset($model['fecha_nacimiento']) ? $model['fecha_nacimiento'] : '';
$sexo = isset($model['sexo']) ? $model['sexo'] : '';
if ($sexo== 'M'){
    $sexo = 'MASCULINO';
}else $sexo = 'FEMENINO';
$fecha_registro = isset($model['fecha_registro']) ? $model['fecha_registro'] : '';
$fecha_ult_actualizacion = isset($model['fecha_ult_actualizacion']) ? $model['fecha_ult_actualizacion'] : '';
}else throw new CHttpException(404,'El Documento de Identidad introducido no existe en el registro');
//--------Fin extraccion---------


?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'saime-form',
                            'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                            // Please note: When you enable ajax validation, make sure the corresponding
                            // controller action is handling ajax validation correctly.
                            // There is a call to performAjaxValidation() commented in generated controller code.
                            // See class documentation of CActiveForm for details on this.
                            'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Vista de Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <label class="col-md-12" for="grupo" style="font-weight: bold">Origen</label>
                                                            <?php echo $origen;//Chtml::texField( 'origen', '$origen' ); ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="col-md-12" for="grupo" style="font-weight: bold">Nacionalidad</label>
                                                            <?php echo $nacionalidad;//Chtml::texField( 'origen', '$origen' ); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <label class="col-md-12" for="grupo" style="font-weight: bold">Pais Origen</label>
                                                            <?php echo $pais_origen;//Chtml::texField( 'origen', '$origen' ); ?>
                                                        </div>
                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">
                                                        <div class="col-md-4">
                                                            <label class="col-md-12" for="grupo" style="font-weight: bold">Documento de Identidad</label>
                                                            <?php echo $cedula;//Chtml::texField( 'origen', '$origen' ); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <label class="col-md-12" for="grupo" style="font-weight: bold">Primer Nombre</label>
                                                            <?php echo $primer_nombre;//Chtml::texField( 'origen', '$origen' ); ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="col-md-12" for="grupo" style="font-weight: bold">Segundo Nombre</label>
                                                            <?php echo $segundo_nombre;//Chtml::texField( 'origen', '$origen' ); ?>
                                                        </div>
                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <label class="col-md-12" for="grupo" style="font-weight: bold">Fecha de Nacimiento</label>
                                                            <?php echo $fecha_nacimiento;//Chtml::texField( 'origen', '$origen' ); ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="col-md-12" for="grupo" style="font-weight: bold">Primer Apellido</label>
                                                            <?php echo $primer_apellido;//Chtml::texField( 'origen', '$origen' ); ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="col-md-12" for="grupo" style="font-weight: bold">Segundo Apellido</label>
                                                            <?php echo $segundo_apellido;//Chtml::texField( 'origen', '$origen' ); ?>
                                                        </div>
                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <label class="col-md-12" for="grupo" style="font-weight: bold">Sexo</label>
                                                            <?php echo $sexo;//Chtml::texField( 'origen', '$origen' ); ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="col-md-12" for="grupo" style="font-weight: bold">Fecha de Registro</label>
                                                            <?php echo $fecha_registro;//Chtml::texField( 'origen', '$origen' ); ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="col-md-12" for="grupo" style="font-weight: bold">Última Actualización</label>
                                                            <?php echo $fecha_ult_actualizacion;//Chtml::texField( 'origen', '$origen' ); ?>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/saime/consulta"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>