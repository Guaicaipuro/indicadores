<?php
/* @var $this ConsultaController */
/* @var $model Saime */

$this->breadcrumbs=array(
	'Saimes'=>array('lista'),
);
?>

<div class="tabbable">

    <ul class="nav nav-tabs">
        <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="datosGenerales">
            <div class="form">

        <div id="div-datos-generales">

            <div class="widget-box">

                <div class="widget-header">
                    <h5>Datos Generales</h5>

                    <div class="widget-toolbar">
                        <a data-action="collapse" href="#">
                            <i class="icon-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-body-inner">
                        <div class="widget-main">
                            <div class="widget-main form">

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'origen'); ?>
                                            <?php echo $form->textField($model,'origen',array('size'=>1, 'maxlength'=>1, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'cedula'); ?>
                                            <?php echo $form->textField($model,'cedula', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'pais_origen'); ?>
                                            <?php echo $form->textField($model,'pais_origen',array('size'=>3, 'maxlength'=>3, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'nacionalidad'); ?>
                                            <?php echo $form->textField($model,'nacionalidad',array('size'=>3, 'maxlength'=>3, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'primer_nombre'); ?>
                                            <?php echo $form->textField($model,'primer_nombre',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'segundo_nombre'); ?>
                                            <?php echo $form->textField($model,'segundo_nombre',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', )); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'primer_apellido'); ?>
                                            <?php echo $form->textField($model,'primer_apellido',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'segundo_apellido'); ?>
                                            <?php echo $form->textField($model,'segundo_apellido',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', )); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
                                            <?php echo $form->textField($model,'fecha_nacimiento', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'sexo'); ?>
                                            <?php echo $form->dropDownList($model, 'sexo', array('F'=>'Femenino', 'M'=>'Masculino',), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'fecha_registro'); ?>
                                            <?php echo $form->textField($model,'fecha_registro', array('class' => 'span-12',"required"=>"required",)); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php// echo $form->labelEx($model,'naturalizado'); ?>
                                            <?php// echo $form->textField($model,'naturalizado', array('class' => 'span-12',)); ?>
                                        </div> 

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'fecha_ult_actualizacion'); ?>
                                            <?php echo $form->textField($model,'fecha_ult_actualizacion', array('class' => 'span-12',)); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div><!-- form -->
        </div>
    </div>
</div>
