<?php
/* @var $this ConsultaController */
/* @var $model Saime */

$this->pageTitle = 'Actualización de Datos de Saimes';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Saimes'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>