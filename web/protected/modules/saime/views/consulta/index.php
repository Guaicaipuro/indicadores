<?php

/* @var $this ConsultaController */
/* @var $model Saime */

$this->breadcrumbs=array(
	'Saimes'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Saimes';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Saimes</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Saimes.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/saime/consulta/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Saimes                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'saime-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>origen</center>',
            'name' => 'origen',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[origen]', $model->origen, array('title' => '',)),
        ),
        array(
            'header' => '<center>cedula</center>',
            'name' => 'cedula',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[cedula]', $model->cedula, array('title' => '',)),
        ),
        array(
            'header' => '<center>pais_origen</center>',
            'name' => 'pais_origen',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[pais_origen]', $model->pais_origen, array('title' => '',)),
        ),
        array(
            'header' => '<center>nacionalidad</center>',
            'name' => 'nacionalidad',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[nacionalidad]', $model->nacionalidad, array('title' => '',)),
        ),
        array(
            'header' => '<center>primer_nombre</center>',
            'name' => 'primer_nombre',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[primer_nombre]', $model->primer_nombre, array('title' => '',)),
        ),
        array(
            'header' => '<center>segundo_nombre</center>',
            'name' => 'segundo_nombre',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[segundo_nombre]', $model->segundo_nombre, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>primer_apellido</center>',
            'name' => 'primer_apellido',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[primer_apellido]', $model->primer_apellido, array('title' => '',)),
        ),
        array(
            'header' => '<center>segundo_apellido</center>',
            'name' => 'segundo_apellido',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[segundo_apellido]', $model->segundo_apellido, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_nacimiento</center>',
            'name' => 'fecha_nacimiento',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[fecha_nacimiento]', $model->fecha_nacimiento, array('title' => '',)),
        ),
        array(
            'header' => '<center>naturalizado</center>',
            'name' => 'naturalizado',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[naturalizado]', $model->naturalizado, array('title' => '',)),
        ),
        array(
            'header' => '<center>sexo</center>',
            'name' => 'sexo',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[sexo]', $model->sexo, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_registro</center>',
            'name' => 'fecha_registro',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[fecha_registro]', $model->fecha_registro, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_ult_actualizacion</center>',
            'name' => 'fecha_ult_actualizacion',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[fecha_ult_actualizacion]', $model->fecha_ult_actualizacion, array('title' => '',)),
        ),
        array(
            'header' => '<center>id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Saime[id]', $model->id, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>