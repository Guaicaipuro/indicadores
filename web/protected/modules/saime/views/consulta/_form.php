<?php
/* @var $this ConsultaController */
/* @var $model Saime */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">


                    <div class="form">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'saime-form',
                                'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-result">
                        <?php
           if($model->hasErrors()):
               $this->renderPartial('//errorSumMsg', array('model' => $model));
           elseif(Yii::app()->user->hasFlash('success')):               $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => Yii::app()->user->getFlash('success')));
           else:
                ?>
                        <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                <?php
                   endif;
               ?>
                        </div>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'origen'); ?>
                                                            <?php echo $form->textField($model,'origen',array('size'=>1, 'maxlength'=>1, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'cedula'); ?>
                                                            <?php echo $form->textField($model,'cedula', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'pais_origen'); ?>
                                                            <?php echo $form->textField($model,'pais_origen',array('size'=>3, 'maxlength'=>3, 'class' => 'span-12', )); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'nacionalidad'); ?>
                                                            <?php echo $form->textField($model,'nacionalidad',array('size'=>3, 'maxlength'=>3, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'primer_nombre'); ?>
                                                            <?php echo $form->textField($model,'primer_nombre',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'segundo_nombre'); ?>
                                                            <?php echo $form->textField($model,'segundo_nombre',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', )); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'primer_apellido'); ?>
                                                            <?php echo $form->textField($model,'primer_apellido',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'segundo_apellido'); ?>
                                                            <?php echo $form->textField($model,'segundo_apellido',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
                                                            <?php echo $form->textField($model,'fecha_nacimiento', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'naturalizado'); ?>
                                                            <?php echo $form->textField($model,'naturalizado', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'sexo'); ?>
                                                            <?php echo $form->dropDownList($model, 'sexo', array('F'=>'Femenino', 'M'=>'Masculino',), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_registro'); ?>
                                                            <?php echo $form->textField($model,'fecha_registro', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_ult_actualizacion'); ?>
                                                            <?php echo $form->textField($model,'fecha_ult_actualizacion', array('class' => 'span-12',)); ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/saime/consulta"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                    <div class="col-md-6 wizard-actions">
                                        <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                            Guardar
                                            <i class="icon-save icon-on-right"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <div id="resultDialog" class="hide"></div>

        <?php
            /**
             * Yii::app()->clientScript->registerScriptFile(
             *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/ConsultaController/saime/form.js',CClientScript::POS_END
             *);
             */
        ?>
    </div>
</div>
