<?php
/* @var $this ConsultaController */
/* @var $model Saime */

$this->pageTitle = 'Registro de Saimes';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Saimes'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>