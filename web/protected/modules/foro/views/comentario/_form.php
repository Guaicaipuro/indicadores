<?php
/* @var $this ComentarioController */
/* @var $model Comentario */
/* @var $form CActiveForm */

/**
 * Divs necesarios para vista de edicion.
 */
if($formType == 'edicion'){ ?>
<div class="col-xs-12">
    <div class="row-fluid">
        <div class="tabbable">
<?php } ?>
            <div class="tab-pane active" id="datosGenerales">
                <div class="form">

                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'comentario-form',
                        'htmlOptions' => array('data-form-type'=>$formType,),// for inset effect
                        'action'=>$formType == 'registro' ? Yii::app()->createUrl('//foro/comentario/registro') : '',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                    ));

                    // Si no esta seteada la variable $tema
                    if(!isset($tema)) {
                        // Si por GET llega la variable tema setear la variable $tema
                        if (isset($_GET['tema'])) $tema = base64_decode($_GET['tema']);
                    }

                    // Condicional para setear la variable $disable.
                    if( Yii::app()->user->id === $model->usuario_ini_id || Yii::app()->user->pbac('foro.comentario.admin') || $formType == 'registro' )
                        $disable = false;
                    else
                        $disable = true;
                    ?>

                    <div id="div-datos-generales">

                        <div class="widget-box">

                            <div class="widget-header">
                                <h5>
                                    <?php echo $formType == 'registro' ? 'Añadir un nuevo comentario' : 'Editar comentario'; ?>
                                </h5>

                                <div class="widget-toolbar">
                                    <a data-action="collapse" href="#">
                                        <i class="icon-chevron-up"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="widget-body">
                                <div class="widget-body-inner">
                                    <div class="widget-main">
                                        <div class="widget-main form">
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div id="row">

                                                        <?php if($model->estatus === 'I' && $formType == 'edicion'){ ?>
                                                            <div id="resultadoOperacion">
                                                                <div class="alertDialogBox">
                                                                    <p>
                                                                        Este comentario se encuentra Inactivo.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        <?php }

                                                        if(Yii::app()->user->hasFlash('success'))
                                                            $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => Yii::app()->user->getFlash('success')));
                                                        elseif($model->hasErrors())
                                                            $this->renderPartial('//errorSumMsg', array('model' => $model));
                                                        else { ?>
                                                            <div class="infoDialogBox">
                                                                <p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p>
                                                                <p>&bull; El asunto debe contener más de 6 car&aacute;cteres</p>
                                                                <p>&bull; El comentario debe contener más de 10 car&aacute;cteres</p>
                                                                <?php
                                                                if(!Yii::app()->user->pbac('foro.comentario.admin')) {
                                                                    if ($model->estatus == 'I' || $formType == 'registro') {
                                                                        ?>
                                                                        <p><b>Recuerda que tu comentario debe ser activado por un administrador.</b></p>
                                                                    <?php
                                                                    }
                                                                }?>
                                                            </div>
                                                        <?php }?>

                                                    </div>

                                                    <?php echo $form->hiddenField($model, 'tema_id',  array('value' => $tema)); ?>

                                                    <div class="row">
                                                        <label>Usuario:</label>
                                                        <span>
                                                            <?php echo $formType == 'registro' ? Yii::app()->user->nombre : $this->usuario($model); ?>
                                                        </span>
                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="row">
                                                            <?php echo $form->labelEx($model,'titulo'); ?>
                                                            <?php echo $form->textField($model,'titulo',array('size'=>145, 'minlength'=>6, 'maxlength'=>150, 'class' => 'span-12', "required"=>"required", 'class' => 'Comentario_asunto', 'autocomplete' => 'off')); ?>
                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="row">
                                                        <?php echo $form->labelEx($model,'contenido'); ?>
                                                        <?php echo $form->textArea($model,'contenido',array('rows'=>6, 'minlength'=>10, 'cols'=>12, 'class' => 'span-12', "required"=>"required",)); ?>
                                                    </div>

                                                    <hr>

                                                    <div class="row">
                                                        <?php if($formType == 'edicion'){ ?>
                                                            <div class="col-md-3">
                                                                <a class="btn btn-danger" href="<?php echo $this->createUrl("/foro/comentario"); ?>" id="btnRegresar">
                                                                    <i class="icon-arrow-left"></i>
                                                                    Volver
                                                                </a>
                                                            </div>

                                                            <div class="col-md-3 wizard-actions">
                                                                <a class="btn btn-danger" href="<?php echo $this->createUrl("/foro/tema/consulta/id/".base64_encode($model->tema_id)); ?>" id="btnRegresar">
                                                                    Ver Tema
                                                                    <i class="icon-arrow-right"></i>
                                                                </a>
                                                            </div>

                                                        <?php }

                                                        if(!$disable){ ?>
                                                            <div class="col-md-6 wizard-actions">
                                                                <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                                                    <?php echo $formType == 'registro' ? 'Comentar' : 'Guardar'; ?>
                                                                    <i class="icon-save icon-on-right"></i>
                                                                </button>
                                                            </div>
                                                        <?php } ?>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                </div><!-- form -->
            </div>
            <?php if($formType == 'edicion'){ ?>
        </div>
    </div>
</div>
    <?php } /**
             * Fin Divs para vista de edicion.
             */

    Yii::app()->clientScript->registerScriptFile(
        Yii::app()->request->baseUrl . '/public/js/modules/foro/comentario/form.js',CClientScript::POS_END
    );
    ?>

