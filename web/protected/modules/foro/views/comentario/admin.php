<?php

/* @var $this ComentarioController */
/* @var $model Comentario */

$this->breadcrumbs=array(
	'Comentarios',
	'Administración',
);
$this->pageTitle = 'Administración de Comentarios';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Comentarios</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Comentarios.
                            </p>
                        </div>
                    </div>

                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'comentario-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array(
                'class' => 'pagination',
            ),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
                    
                }",
    	'columns'=>array(
        array(
            'header' => '<center>Asunto del Comentario</center>',
            'name' => 'titulo',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Comentario[titulo]', $model->titulo, array('title' => '',)),
        ),
        array(
            'header' => '<center>Tema</center>',
            'name' => 'tema.titulo',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Comentario[tema_titulo]', $model->tema_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Usuario</center>',
            'name' => 'usuarioIni',
            'value' => array($this, 'usuario'),
            'htmlOptions' => array('width'=>'200px', 'style'=>'text-align: center'),
            'filter' => CHtml::textField('Comentario[usuario]', $model->usuario_ini_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'value' => array($this, 'estatus'),
            'filter' => array('' => 'Todos', 'A' => 'Activo', 'I' => 'Inactivo'),
            'htmlOptions' => array('width'=>'10%', 'style'=>'text-align: center'),
        ),
        array(
            'header' => '<center>Creado</center>',
            'name' => 'fecha',
            'value' => 'date("d-m-Y H:i", strtotime($data->fecha_ini))',
            'htmlOptions' => array('width'=>'60px', 'style'=>'text-align: center'),
            'filter' => CHtml::hiddenField('Comentario[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),
		array(
             'type' => 'raw',
              'header' => '<center>Acción</center>',
              'value' => array($this, 'getActionButtons'),
              'htmlOptions' => array('nowrap'=>'nowrap', 'width'=>'40px'),
        ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>
