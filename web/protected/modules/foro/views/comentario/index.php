<?php

/* @var $this ComentarioController */
/* @var $model Comentario */

$this->breadcrumbs=array(
	'Comentarios'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Comentarios';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Comentarios</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>

                <div>

                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Comentarios.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/foro/comentario/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Comentarios
                        </a>
                    </div>

                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'comentario-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Comentario[id]', $model->id, array('title' => '',)),
        ),
        array(
            'header' => '<center>tema_id</center>',
            'name' => 'tema_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Comentario[tema_id]', $model->tema_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>titulo</center>',
            'name' => 'titulo',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Comentario[titulo]', $model->titulo, array('title' => '',)),
        ),
        array(
            'header' => '<center>contenido</center>',
            'name' => 'contenido',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Comentario[contenido]', $model->contenido, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_ini_id</center>',
            'name' => 'usuario_ini_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Comentario[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
        ),
		array(
            'type' => 'raw',
            'header' => '<center>Acción</center>',
            'value' => array($this, 'getActionButtons'),
            'htmlOptions' => array('nowrap'=>'nowrap'),
        ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>