<?php
/* @var $this ComentarioController */
/* @var $model Comentario */

$this->pageTitle = 'Registro de Comentarios';
      $this->breadcrumbs=array(
	'Comentarios'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>