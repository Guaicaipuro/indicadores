<?php
/* @var $this ComentarioController */
/* @var $model Comentario */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
	'Comentarios'=>array('lista'),
);
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Comentario</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'comentario-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Comentario</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">

                                                    <?php if($model->estatus === 'I'): ?>
                                                    <div id="resultadoOperacion">
                                                        <div class="alertDialogBox">
                                                            <p>
                                                                Este comentario se encuentra Inactivo.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>

                                                    <div class="col-md-12">

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Creado:</label>
                                                             <span>
                                                                 <?php echo date('H:i | d-m-Y', strtotime($model->fecha_ini)); ?>
                                                             </span>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label>Usuario:</label>
                                                            <span>
                                                                 <?php echo $this->usuario($model); ?>
                                                            </span>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Tema</label>
                                                                <?php echo $form->textField($model->tema,'titulo', array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                            </div>
                                                        </div>

                                                        <div class="space-6"></div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Asunto</label>
                                                                <?php echo $form->textField($model,'titulo',array('size'=>60, 'maxlength'=>150, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                            </div>
                                                        </div>

                                                        <div class="space-6"></div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Comentario</label>
                                                                <?php echo $form->textArea($model,'contenido',array('rows'=>6, 'cols'=>12, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                            </div>
                                                        </div>

                                                        <div class="space-6"></div>

                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_act'); ?>
                                                            <?php echo $form->textField($model,'fecha_act', array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'estatus'); ?>
                                                            <?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado'), array('prompt'=>'- - -', 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="col-md-4">
                                            <a class="btn btn-danger" href="<?php echo $this->createUrl("/foro/comentario"); ?>" id="btnRegresar">
                                                <i class="icon-arrow-left"></i>
                                                Volver
                                            </a>
                                        </div>

                                        <div class="col-md-4 wizard-actions">
                                            <?php if($model->estatus === 'I' && Yii::app()->user->pbac('foro.comentario.admin')){ ?>
                                                <a class="btn btn-primary btn-next" href="<?php echo $this->createUrl("/foro/comentario/activacion/id/".base64_encode($model->id)); ?>">
                                                    Activar Comentario
                                                    <i class="icon-save icon-on-right"></i>
                                                </a>
                                            <?php } ?>
                                        </div>

                                        <div class="col-md-4">
                                            <a class="btn btn-danger pull-right" href="<?php echo $this->createUrl("/foro/tema/consulta/id/".base64_encode($model->tema_id)); ?>" id="btnTema">
                                                Ver Tema
                                                <i class="icon-arrow-right"></i>
                                            </a>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>
            </div>
        </div>

    </div>
</div>