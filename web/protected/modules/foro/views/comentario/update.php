<?php
/* @var $this ComentarioController */
/* @var $model Comentario */

$this->pageTitle = 'Actualización de Datos de Comentarios';
      $this->breadcrumbs=array(
	'Comentarios'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>