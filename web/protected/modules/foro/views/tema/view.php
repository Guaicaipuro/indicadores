<?php
/* @var $this TemaController */
/* @var $model Tema */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
	'Temas'=>array('../foro'),
);
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Tema</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'tema-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <?php
                        // Se guardan los errores de la variable de sesion 'error'.
                        $errors = Yii::app()->getSession()->get('error');
                        // Se verifica que este seteada la variable $errors.
                        if(isset($errors)){ ?>
                            <div class="errorDialogBox"><p class="note">
                                <p>Tu comentario no pudo ser registrado, porque:</p>
                                <?php
                                // Se muestran todos los errores.
                                foreach($errors as $error)
                                    echo '<p>'.$error[0].'</p>';
                                ?>
                            </div>
                            <?php Yii::app()->getSession()->remove('error');

                        }elseif(Yii::app()->user->hasFlash('success')){
                            $message = Yii::app()->user->getFlash('success');
                            if(!Yii::app()->user->pbac('foro.tema.admin')) $message .= '<p><b>Recuerda que tu comentario debe ser activado por un administrador.</b></p>';
                            $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => $message ));
                        }
                        ?>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Tema</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="row">

                                                        <div class="col-md-12">
                                                          <label>Creado:</label>
                                                          <span>
                                                              <?php echo date('H:i | d-m-Y', strtotime($model->fecha_ini)); ?>
                                                          </span>
                                                        </div>
                                                        <div class="col-md-12">
                                                          <label>Usuario:</label>
                                                          <span>
                                                              <?php echo $this->usuario($model); ?>
                                                          </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label>Título</label>
                                                            <?php echo $form->textField($model,'titulo',array('size'=>60, 'maxlength'=>150, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>
                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label>Contenido</label>
                                                            <?php echo $form->textArea($model,'contenido',array('rows'=>6, 'cols'=>12, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>
                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">
                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model, 'fecha_act'); ?>
                                                            <?php echo $form->textField($model, 'fecha_act',  array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'estatus'); ?>
                                                            <?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado'), array('prompt'=>'- - -', 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="space-6"></div>
                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/foro/tema"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>
                                </div>

                                <hr>

                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>
        <!--------------- LISTADO DE COMENTARIOS ---------------->

            <div class="widget-box">
                <div class="widget-header">
                    <h5>Comentarios</h5>

                    <div class="widget-toolbar">
                        <a data-action="collapse" href="#">
                            <i class="icon-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-body-inner">
                        <div class="widget-main">
                            <div class="widget-main form">

                           <?php
                                 // Variable que se utiliza para saber si hay comentarios activos.
                                 $auxiliar = false;
                              foreach($model->comentarios as $comentario){
                                  // Esconder comentarios a los usuarios sin permisos de Administrador, si estan Inactivos.
                                  if( $comentario->estatus === 'A' || Yii::app()->user->pbac('foro.tema.admin') ){
                                      // Si algun comentario esta activo y el usuario no es Administrador setear como 'true' la variable $auxiliar.
                                      if( $comentario->estatus === 'A' && !Yii::app()->user->pbac('foro.tema.admin') )
                                          $auxiliar = true;
                                      ?>
                                      <div class="row">
                                          <?php
                                          // Muestra boton para activar comentario si esta Inactivo y es Administrador el usuario.
                                          if($comentario->estatus === 'I' && Yii::app()->user->pbac('foro.tema.admin')):?>
                                              <div class="alertDialogBox"><p class="note">Debes activar este comentario para que los usuarios puedan verlo.</p></div>
                                              <div class="col-md-6 wizard-actions pull-right">
                                                  <a class="btn btn-primary btn-next" href="<?php echo $this->createUrl("/foro/comentario/activacion/id/".base64_encode($comentario->id)."/returnUrl/".base64_encode(Yii::app()->request->requestUri)); ?>">
                                                      Activar Comentario
                                                      <i class="icon-save icon-on-right"></i>
                                                  </a>
                                              </div>
                                          <?php endif; ?>
                                          <div class="row">
                                              <div class="col-md-12">
                                                  <label>Creado:</label>
                                                 <span>
                                                     <?php echo date('H:i | d-m-Y', strtotime($comentario->fecha_ini)); ?>
                                                 </span>
                                              </div>

                                              <div class="col-md-12">
                                                  <label>Usuario:</label>
                                                 <span>
                                                     <?php echo $this->usuario($comentario); ?>
                                                 </span>
                                              </div>
                                          </div>

                                          <div class="row">
                                              <div class="col-md-12">
                                                  <label>Titulo</label>
                                                  <?php echo $form->textField($comentario,'titulo',array('size'=>60, 'maxlength'=>150, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                              </div>
                                          </div>

                                          <div class="space-6"></div>

                                          <div class="row">
                                              <div class="col-md-12">
                                                  <label>Contenido</label>
                                                  <?php echo $form->textArea($comentario,'contenido',array('rows'=>6, 'cols'=>12, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                              </div>
                                          </div>

                                      </div>
                                      <hr>
                                  <?php  } // End if
                              } // End foreach

                           /**
                            * Casos en los que se muestra el mensaje:
                            * 1.- Si $auxiliar es true y el usuario no tiene permisos de Administrador...
                            * ...($auxiliar solo es true si hay uno o mas comentarios Activos).
                            * 2.- Si el usuario tiene permisos de Administrador y hay comentarios (sin importar su estatus).
                            */
                           if( ( !$auxiliar && !Yii::app()->user->pbac('foro.tema.admin') ) || ( Yii::app()->user->pbac('foro.tema.admin') && (count($model->comentarios) < 1) ) ){ ?>
                                  <div class="alertDialogBox"><p class="note">No hay comentarios acerca de este tema a&uacute;n.</p></div>
                           <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        <!--------------- FIN LISTADO DE COMENTARIOS ---------------->

            <!---------------- FORMULARIO COMENTARIO -------------------->

                <?php
                   $modelC = Comentario::model();
                   $this->renderPartial('../comentario/_form', array('model'=>$modelC, 'formType'=>'registro', 'tema'=>$model->id));
                ?>

            <!--------------- FIN FORMULARIO COMENTARIO ------------------>

            </div>
        </div>
    </div>
</div>
