<?php

/* @var $this TemaController */
/* @var $model Tema */

$this->breadcrumbs=array(
	'Temas',
	'Administración',
);
$this->pageTitle = 'Administración de Temas';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Temas</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Temas.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/foro/tema/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Temas
                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tema-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
                    
                }",
	'columns'=>array(
        array(
            'header' => '<center>Tema</center>',
            'name' => 'titulo',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Tema[titulo]', $model->titulo, array('title' => '',)),
        ),
        array(
            'header' => '<center>Usuario</center>',
            'name' => 'usuarioIni.username',
            'value' => array($this, 'usuario'),
            'htmlOptions' => array('width'=>'200px', 'style'=>'text-align: center'),
            'filter' => CHtml::textField('Comentario[usuario]', $model->usuario_ini_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'value' => array($this, 'estatus'),
            'filter' => array('' => 'Todos', 'A' => 'Activo', 'I' => 'Inactivo'),
            'htmlOptions' => array('width'=>'10%', 'style'=>'text-align: center'),
        ),
        array(
            'header' => '<center>Actualizado</center>',
            'name' => 'fecha_ac', // <- Este valor esta malo a proposito para que no haga la busqueda.
            'value' => 'date("d-m-Y H:i", strtotime($data->fecha_act))',
            'htmlOptions' => array('width'=>'60px', 'style'=>'text-align: center'),
            'filter' => CHtml::hiddenField('Tema[fecha_act]', $model->fecha_act, array('title' => '',)),
        ),
		array(
            'type' => 'raw',
            'header' => '<center>Acción</center>',
            'value' => array($this, 'getActionButtons'),
            'htmlOptions' => array('nowrap'=>'nowrap', 'width'=>'60px'),
        ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>
