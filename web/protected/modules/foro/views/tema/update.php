<?php
/* @var $this TemaController */
/* @var $model Tema */

$this->pageTitle = 'Actualización de Datos de Temas';
    $this->breadcrumbs=array(
        'Temas'=>array('../foro'),
        'Actualización',
    );
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion', 'tema'=>$_GET['id'])); ?>