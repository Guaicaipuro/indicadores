<?php
/* @var $this TemaController */
/* @var $model Tema */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">
        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'tema-form',
                                'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        ));

                        // Condicional para setear la variable $disable.
                        if( Yii::app()->user->id === $model->usuario_ini_id || Yii::app()->user->pbac('foro.tema.admin') || $formType == 'registro' )
                            $disable = false;
                        else
                            $disable = true;
                        ?>

                        <div id="div-result">
                           <?php
                           if($model->hasErrors())
                               $this->renderPartial('//errorSumMsg', array('model' => $model));
                           elseif(Yii::app()->user->hasFlash('success'))
                               $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => Yii::app()->user->getFlash('success')));
                           else{ ?>
                               <div class="infoDialogBox">
                             <?php if($disable){?>
                                 <p class="note">No puedes actualizar la información de este tema.</p>
                                 <p>&bull; El tema no fue publicado por ti.</p>
                                 <p>&bull; No tienes permisos de Administración.</p>
                             <?php }else{?>
                                   <p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p>
                                   <p>&bull; El titulo debe contener más de 6 car&aacute;cteres</p>
                                   <p>&bull; El contenido debe contener más de 10 car&aacute;cteres</p>
                              <?php } ?>
                               </div>
                               <?php
                           } ?>
                        </div>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Usuario:</label>
                                                        <span>
                                                             <?php echo $formType == 'registro' ? Yii::app()->user->nombre : $this->usuario($model); ?>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <?php echo $form->labelEx($model,'titulo'); ?>
                                                        <?php echo $form->textField($model,'titulo',array('size'=>60, 'pattern' => '.{6,}', 'maxlength'=>150, 'class' => 'span-12', "required"=>"required", "disabled" => $disable ? 'disabled' : '', )); ?>
                                                    </div>
                                                </div>

                                                <div class="space-6"></div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <?php echo $form->labelEx($model,'contenido'); ?>
                                                        <?php echo $form->textArea($model,'contenido',array('rows'=>6 ,'cols'=>12, 'class' => 'span-12', "required"=>"required",  "disabled" => $disable ? 'disabled' : '',)); ?>
                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="row">

                                                        <div class="col-md-6">
                                                            <a class="btn btn-danger" href="<?php echo $this->createUrl("/foro"); ?>" id="btnRegresar">
                                                                <i class="icon-arrow-left"></i>
                                                                Volver
                                                            </a>
                                                        </div>

                                                    <?php if( Yii::app()->user->id === $model->usuario_ini_id || Yii::app()->user->pbac('foro.tema.admin') || $formType == 'registro' ){?>
                                                        <div class="col-md-6 wizard-actions">
                                                            <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                                                Guardar
                                                                <i class="icon-save icon-on-right"></i>
                                                            </button>
                                                        </div>
                                                    <?php } ?>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>
            </div>
        </div>

        <div id="resultDialog" class="hide"></div>

        <?php
        Yii::app()->clientScript->registerScriptFile(
            Yii::app()->request->baseUrl . '/public/js/modules/foro/tema/form.js',CClientScript::POS_END
        );
        ?>

    </div>
</div>
