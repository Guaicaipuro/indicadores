<?php
/* @var $this TemaController */
/* @var $model Tema */

$this->pageTitle = 'Registro de Temas';
      $this->breadcrumbs=array(
	'Temas'=>array('../foro'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>