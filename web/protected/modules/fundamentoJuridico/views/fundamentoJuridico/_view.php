<?php
/* @var $this FundamentoJuridicoController */
/* @var $data FundamentoJuridico */
?>

<div class="view">

    <div class="tabbable">

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#datosGenerales">Datos Generales</a></li>
        </ul>

        <div class="tab-content">

            <div id="datosGenerales" class="tab-pane active">


            <div class="widget-main form">
                <div class="row">
                    <div class="col-md-12">
                        <label class="col-md-12" ><b>Nombre:</b> <?php echo CHtml::encode($model->nombre); ?></label>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="col-md-12" ><b>Fecha de Emisión:</b> <?php
                        if($model->fecha_emision){
                        echo CHtml::encode(date("d-m-Y", strtotime($model->fecha_emision))); 
                        }else{
                         echo "----------";   
                        }
                        ?></label>

                    </div>
                    <div class="col-md-6">
                        <label class="col-md-12" ><b>Tipo de Fundamento Jurídico:</b> <?php echo CHtml::encode($model->tipoFundamento->nombre); ?></label>

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label class="col-md-12">
                            <b>Descripción:</b>
                        </label>

                        <?php if (isset($model->descripcion)) { ?>
                            <div class="col-md-12"  >
                                        <div style="border:1px solid #ccc; min-height:100px;">
                                        <?php echo $model->descripcion; ?>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>
                        <div class="col-md-12"  >
                                <div class="col-md-12" style="border:1px solid #ccc; min-height:100px;">
                                    <?php echo "No posee descripción"; ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <?php
                        if ($modelArchivo->search($model->id)) {
                            $this->widget('zii.widgets.grid.CGridView', array(
                                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                'id' => 'archivo-fundamento-grid',
                                'dataProvider' => $modelArchivo->search($model->id),
                                'summaryText' => false,
                                'pager' => array(
                                    'header' => '',
                                    'htmlOptions' => array('class' => 'pagination'),
                                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                    'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                    'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                    'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                ),
                                'columns' => array(
                                    array('class' => 'CLinkColumn',
                'header' => '<center>Nombre del Archivo</center>',
                'labelExpression' => '$data->nombre',
                'urlExpression' => '"/fundamentoJuridico/fundamentoJuridico/descargar?id=".base64_encode($data->ruta)'
            ),
                                ),
                            ));
                        }
                        ?>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <label class="col-md-12" ><b>Creado por:</b> <?php echo CHtml::encode($model->usuarioIni->nombre . " " . $model->usuarioIni->apellido); ?></label>


                    </div>
                    <div class="col-md-4">
                        <label class="col-md-12" ><b>Fecha de Creación:</b> <?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime($model->fecha_ini))); ?></label>

                    </div>
                    <div class="col-md-4">
                        <label class="col-md-12" ><b>Estatus:</b>
                            <?php
                            if ($model->estatus == "A") {
                                echo "Activo";
                            } else if ($model->estatus == "E") {
                                echo "Inactivo";
                            }
                            ?>	
                        </label>
                    </div>

                </div>

                <div class="row">

                    <?php if ($model->usuarioAct) { ?>
                        <div class="col-md-4">
                            <label class="col-md-12" ><b>Modificado por:</b> <?php echo CHtml::encode($model->usuarioAct->nombre . " " . $model->usuarioAct->apellido); ?></label>

                                </div>
                        <?php } ?>
                        <div class="col-md-4">
                            <?php if ($model->fecha_act) { ?>
                                <label class="col-md-12"><b>Fecha de Actualización:</b> <?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime($model->fecha_act))); ?></label>

                            <?php } ?>
                    </div>
                    <?php if ($model->estatus == "E") { ?>
                    <div class="col-md-4">
                            <label class="col-md-12"><b>Inactivado el:</b> <?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime($model->fecha_elim))); ?></label>

                                </div>
                        <?php } ?>
                </div>
                <hr>
                <div class="row-actions">
                    <a class="btn btn-danger" href="/fundamentoJuridico/fundamentoJuridico">

                        <i class="icon-arrow-left bigger-110"></i>
                        Volver
                    </a>

                </div>

            </div>
     
        </div>
    </div>
</div>

</div>