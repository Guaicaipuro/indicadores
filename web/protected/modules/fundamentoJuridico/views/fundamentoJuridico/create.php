<?php
/* @var $this FundamentoJuridicoController */
/* @var $model FundamentoJuridico */

$this->breadcrumbs=array(
	'Fundamentos Jurídicos' => array('index'),
    'Agregar',
);

/*
this->menu=array(

	array('label'=>'List FundamentoJuridico', 'url'=>array('index')),
	array('label'=>'Manage FundamentoJuridico', 'url'=>array('admin')),
);
*/
?>

<?php
$this->renderPartial('_form', array('model' => $model, 'modelArchivo' => $modelArchivo, 'key' => $key, 'msj' => $msj, 'llave' => $llave, 'subtitulo' => "Nuevo Fundamento Juridico"));
//$this->renderPartial('_archivo', array('model'=>$model,'key'=>$key,'msj'=>$msj,'subtitulo'=>"Nuevo Fundamento Juridico")); 

?>