<?php

class DefaultController extends Controller
{
    public $defaultAction = 'queryRapido';

    public function actionQueryRapido()
    {

        echo Nivel::testQuery();
    }

    public function actionQueryPesado()
    {
        $plantel_id = null;
        $periodo_actual_id = 15;
        while($plantel_id == null){
            $seccion_plantel_id = rand(1000,340000);
            $plantel_id = SeccionPlantel::testQuery($seccion_plantel_id);
        }
        var_dump($seccion_plantel_id,$plantel_id);
        //var_dump($seccion_plantel_id,$periodo_actual_id,$plantel_id);
        print_r(Estudiante::testQuery($seccion_plantel_id,$periodo_actual_id,$plantel_id));
    }

    public function actionEstatus(){

        echo 'Servicio Activo a la fecha '.date('d-m-Y H:i:s');
    }
}