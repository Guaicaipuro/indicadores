<?php

class DefaultController extends Controller {

    const MODULO = "estadistica.default";

    static $_permissionControl = array(
        'read' => 'Estadisticas',
        'write' => 'Estadisticas',
        'label' => 'Estadisticas'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
//'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array(
                    'index'
                ),
                'pbac' => array('read', 'write'),
            ),
//            array('allow', // allow authenticated user to perform 'create' and 'update' actions
//                'actions' => array(
//                    ''),
//                'pbac' => array('write'),
//            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

}
