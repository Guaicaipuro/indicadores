<div class = "widget-box">


    <div class="widget-header">
        <h5>Consulta de Notas </h5>
        <div class="widget-toolbar">
            <a  href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body" id="consultaTitulo">
        <div class="widget-body-inner" style="display: block;">
            <div class="widget-main form">

                <div class="row">
                            <div class="col-md-12" id ="solicitud-titulo">
                                <div id="scrolltable" style='border: 0px;background: #fff; overflow:auto;padding-right: 0px; padding-top: 0px; padding-left: 0px; padding-bottom: 0px;border-right: #DDDDDD 0px solid; border-top: #DDDDDD 0px solid;border-left: #DDDDDD 0px solid; border-bottom: #DDDDDD 0px solid;scrollbar-arrow-color : #999999; scrollbar-face-color : #666666;scrollbar-track-color :#3333333 ;height:400px; left: 28%; top: 300; width: 100%'>
                <?php
                if (!$dataProvider == array()) {
                    if (isset($dataConsultarNota[0])) {
                        ?>

                                            <?php
                                            //var_dump($dataProvider);
                                    //        die();


                                    $this->widget(
                                            'zii.widgets.grid.CGridView', array(
                                        'id' => 'ConsultaNotas1-grid',
                                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                        'dataProvider' => $dataConsultarNota[0],
                                        'summaryText' => false,
                                        'columns' => array(
                                            //                                        array(
//                                            "name" => "boton",
//                                            "type" => "raw",
//                                            'header' => '<div class="center">' . CHtml::checkBox('selec_todoEst', false, array('id' => 'selec_todoEst', 'title' => 'Seleccionar todos', 'class' => 'tooltipMatricula')) . "</div>"
//                                        ),
//                                        array(
//                                            'name' => 'mencion',
//                                            'type' => 'raw',
//                                            'header' => '<center><b>Mención</b></center>'
//            ),
                                            array(
                                                'name' => 'materia',
                                                'type' => 'raw',
                                                'header' => '<center><b>MATERIAS BÁSICAS 1ER AÑO</b></center>'
                                            ),
                                            array(
                                                'name' => 'nota',
                                                'type' => 'raw',
                                                'header' => '<center><b> Nota </b></center>'
                                            ),
                                        ),
                                        'pager' => array(
                                            'header' => '',
                                            'htmlOptions' => array('class' => 'pagination'),
                                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                        ),
                                            )
                                    );
                                }
                                echo '<hr>';

                                if (isset($dataConsultarNota[1])) {
                                    $this->widget(
                                            'zii.widgets.grid.CGridView', array(
                                        'id' => 'ConsultaNotas2-grid',
                                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                        'dataProvider' => $dataConsultarNota[1],
                                        'summaryText' => false,
                                        'columns' => array(
                                            //                                        array(
//                                            "name" => "boton",
//                                            "type" => "raw",
//                                            'header' => '<div class="center">' . CHtml::checkBox('selec_todoEst', false, array('id' => 'selec_todoEst', 'title' => 'Seleccionar todos', 'class' => 'tooltipMatricula')) . "</div>"
//                                        ),
//            array(
//                'name' => 'mencion',
//                'type' => 'raw',
//                'header' => '<center><b>Mención</b></center>'
//            ),
                                            array(
                                                'name' => 'materia',
                                                'type' => 'raw',
                                                'header' => '<center><b>MATERIAS BÁSICAS 2DO AÑO</b></center>'
                                            ),
                                            array(
                                                'name' => 'nota',
                                                'type' => 'raw',
                                                'header' => '<center><b> Nota </b></center>'
                                            ),
                                        ),
                                        'pager' => array(
                                            'header' => '',
                                            'htmlOptions' => array('class' => 'pagination'),
                                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                        ),
                                            )
                                    );
                                }
                                echo '<hr>';


                                if (isset($dataConsultarNota[2])) {

                                    $this->widget(
                                            'zii.widgets.grid.CGridView', array(
                                        'id' => 'ConsultaNotas3-grid',
                                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                        'dataProvider' => $dataConsultarNota[2],
                                        'summaryText' => false,
                                        'columns' => array(
                                            //                                        array(
//                                            "name" => "boton",
//                                            "type" => "raw",
//                                            'header' => '<div class="center">' . CHtml::checkBox('selec_todoEst', false, array('id' => 'selec_todoEst', 'title' => 'Seleccionar todos', 'class' => 'tooltipMatricula')) . "</div>"
//                                        ),
//            array(
//                'name' => 'mencion',
//                'type' => 'raw',
//                'header' => '<center><b>Mención</b></center>'
//            ),
                                            array(
                                                'name' => 'materia',
                                                'type' => 'raw',
                                                'header' => '<center><b>MATERIAS BÁSICAS 3ER AÑO</b></center>'
                                            ),
                                            array(
                                                'name' => 'nota',
                                                'type' => 'raw',
                                                'header' => '<center><b> Nota </b></center>'
                                            ),
                                        ),
                                        'pager' => array(
                                            'header' => '',
                                            'htmlOptions' => array('class' => 'pagination'),
                                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                        ),
                                            )
                                    );
                                    echo '<hr>';
                                }
                                if (isset($dataConsultarNota[3])) {
                                    $this->widget(
                                            'zii.widgets.grid.CGridView', array(
                                        'id' => 'ConsultaNotas4-grid',
                                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                        'dataProvider' => $dataConsultarNota[3],
                                        'summaryText' => false,
                                        'columns' => array(
                                            //                                        array(
//                                            "name" => "boton",
//                                            "type" => "raw",
//                                            'header' => '<div class="center">' . CHtml::checkBox('selec_todoEst', false, array('id' => 'selec_todoEst', 'title' => 'Seleccionar todos', 'class' => 'tooltipMatricula')) . "</div>"
//                                        ),
//            array(
//                'name' => 'mencion',
//                'type' => 'raw',
//                'header' => '<center><b>Mención</b></center>'
//            ),
                                            array(
                                                'name' => 'materia',
                                                'type' => 'raw',
                                                'header' => '<center><b>PROCESAMIENTOS DE DATOS</b></center>'
                                            ),
                                            array(
                                                'name' => 'nota',
                                                'type' => 'raw',
                                                'header' => '<center><b> Nota </b></center>'
                                            ),
                                        ),
                                        'pager' => array(
                                            'header' => '',
                                            'htmlOptions' => array('class' => 'pagination'),
                                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                        ),
                                            )
                                    );
                                }
                            } else {
                                ?>
                                    <div class="infoDialogBox">
                                            <p>
                                                <strong>Este estudiante no posee Notas registradas en el sistema.</strong>
                                            </p>
                                        </div>
                                        <!--                        <div class="panel-body">
                                                                    <p style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
        
                                            </p>
                                                            </div>-->
                                    <?php
                                }
                            ?>
                        </div>

                        <br>
                        <br>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>