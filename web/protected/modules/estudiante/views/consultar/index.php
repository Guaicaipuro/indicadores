<?php
/* @var $this ConsultarController */
/* @var $model Estudiante */

if($nombrePlantel != '') {
    $this->breadcrumbs = array(
        'Planteles' => array('/planteles/'),
        'Estudiantes',
    );
}
else {
    $this->breadcrumbs = array(
        'Estudiantes ',
    );
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#estudiante-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

$required = '<span class="required">*</span>'
?>

<?php
if (isset($plantelPK) && $plantelPK != '') {
    ?>
    <div class = "widget-box collapsed">

        <div class = "widget-header">
            <h5>Identificación del Plantel <?php echo '"' . $plantelPK['nombre'] . '"'; ?></h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: none;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row row-fluid center">
                        <div id="1eraFila" class="col-md-12">
                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Código del Plantel</b>', '', array("class" => "col-md-12")); ?>
                                <?php
                                if($plantelPK['cod_plantel'] != null)
                                {
                                    echo CHtml::textField('', $plantelPK['cod_plantel'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                else
                                {
                                    echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Código Estadistico</b>', '', array("class" => "col-md-12")); ?>
                                <?php
                                if($plantelPK['cod_estadistico'] != null)
                                {
                                    echo CHtml::textField('', $plantelPK['cod_estadistico'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                else
                                {
                                    echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Denominación</b>', '', array("class" => "col-md-12")); ?>
                                <?php
                                if($plantelPK['denominacion_id'] != null)
                                {
                                    echo CHtml::textField('', $plantelPK->denominacion->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                else
                                {
                                    echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                ?>
                            </div>

                        </div>

                        <div class = "col-md-12"><div class = "space-6"></div></div>

                        <div id="2daFila" class="col-md-12">
                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Nombre del Plantel</b>', '', array("class" => "col-md-12")); ?>
                                <?php
                                if($plantelPK['nombre'] != null)
                                {
                                    echo CHtml::textField('', $plantelPK['nombre'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                else
                                {
                                    echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Zona Educativa</b>', '', array("class" => "col-md-12")); ?>
                                <?php
                                if($plantelPK['zona_educativa_id'] != null)
                                {
                                    echo CHtml::textField('', $plantelPK->zonaEducativa->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                else
                                {
                                    echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Estado</b>', '', array("class" => "col-md-12")); ?>
                                <?php
                                if($plantelPK['estado_id'] != null)
                                {
                                    echo CHtml::textField('', $plantelPK->estado->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                else
                                {
                                    echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                }
                                ?>
                            </div>

                        </div>

                        <div class = "col-md-12"><div class = "space-6"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>

<div class="widget-box">

    <div class="widget-header">
        <h5>B&uacute;squeda Avanzada de Estudiantes</h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div class="widget-body-inner" style="display: block;">
            <div class="widget-main">
                <div class="row">
                    <div class="infoDialogBox" id="infoEstudiante">
                        <p> Estimado usuario, si desea realizar una busqueda por :
                            <br>&nbsp;&nbsp;&nbsp;- Cédula de Identidad Venezolana : V-999999
                            <br>&nbsp;&nbsp;&nbsp;- Cédula de Identidad Extranjera : E-999999
                            <br>&nbsp;&nbsp;&nbsp;- Pasaporte : P-999999
                            <br>Solo debe ingresar los digitos en el campo <b>"Documento de Identidad del Estudiante".</b>&nbsp;&nbsp; Ejemplo: <b>(999999).</b>
                        </p>
                    </div>
                </div>
                <a href="#" class="search-button"></a>
                <div style="display:block" class="search-form">
                    <div class="widget-main form">

                        <div class="row">
                            <div style="padding-left:10px;" class="col-md-12 text-right">
                                <?php
                                /**
                                 * SI SOLO EL DIRECTOR DEL PLANTEL PUEDE AGEGAR ESTUDIANTES A SU PLANTEL
                                 * Condición: Yii::app()->user->group == UserGroups::DIRECTOR
                                 */

                                if(Yii::app()->user->pbac('estudiante.escolaridad.admin')) {
                                    ?>
                                    <a class="btn btn-success btn-next btn-sm" data-last="Finish" href="/estudiante/escolaridad/">
                                        <i class="fa fa-plus icon-on-right"></i>
                                        Registrar Escolaridad
                                    </a>
                                <?php }
                                if(Yii::app()->user->pbac('estudiante.crear.write')){
                                    $url = '';
                                    if(isset($plantel_id) AND $plantel_id!=null){
                                        $url = 'index/id/' . base64_encode($plantel_id);
                                    }
                                    ?>
                                    <a id='' class="btn btn-success btn-next btn-sm" data-last="Finish" href="/estudiante/crear/<?php echo $url; ?>">
                                        <i class="fa fa-plus icon-on-right"></i>
                                        Registrar Nuevo Estudiante
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="space-6"></div>
                        <?php
                        $this->renderPartial('_search', array(
                            'model' => $model,
                            'plantel_id' => $plantel_id,
                        ));
                        ?>

                    </div><!-- search-form -->
                </div><!-- search-form -->
            </div>
        </div>
    </div>

</div>


<div class="widget-box">

    <div class="widget-header">
        <h5>Estudiantes</h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div class="widget-body-inner" style="display: block;">
            <div class="widget-main">

                <a href="#" class="search-button"></a>
                <div style="display:block" class="search-form">
                    <div class="widget-main form">
                        <div class="col-lg-12"><div class="space-6"></div></div>

                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                            'pager' => array('pageSize' => 1),
                            'summaryText' => false,
                            'id' => 'estudiante-grid',
                            'dataProvider' => $model->search(),
                            'filter' => $model,
                            'afterAjaxUpdate' => "function(){
                                
                                $('#Estudiante_documento_identidad').focus();
                                
                                $('.lnk-titulo-digital').on('click', function(evt){
                                    evt.preventDefault();
                                    dialogVerificacionDatos(this);
                                });

                                $('#Estudiante_nombres').bind('keyup blur', function() {
                                    makeUpper(this);
                                    keyAlphaNum(this, true, true);
                                });
                                $('#Estudiante_apellidos').bind('keyup blur', function() {
                                    makeUpper(this);
                                    keyAlphaNum(this, true, true);
                                });
                                $('#Estudiante_codigo_plantel').bind('keyup blur', function(evt) {
                                 evt.preventDefault();
                                    makeUpper(this);
                                });
                                $('#Estudiante_documento_identidad_representante').bind('keyup blur', function(evt) {
                                 evt.preventDefault();
                                    keyAlphaNum(this, false,false);
                                    makeUpper(this);
                                });
                                $('#Estudiante_documento_identidad').bind('keyup blur', function(evt) {
                                 evt.preventDefault();
                                    keyAlphaNum(this, false,false);
                                    makeUpper(this);
                                });
                                $('#Estudiante_cedula_escolar').bind('keyup blur', function(evt) {
                                 evt.preventDefault();
                                    keyNum(this, false);// true acepta la ñ y para que sea español
                                });
                            }",
                            'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination'),
                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                            ),
                            'columns' => array(
                                array(
                                    'header' => '<center>Código del Plantel</center>',
                                    'name' => 'plantel_actual_id',
                                    'value' => '(isset($data->plantelActual->cod_plantel))? $data->plantelActual->cod_plantel : ""',
                                    'filter' => CHtml::textField('Estudiante[codigo_plantel]', null),
                                ),
                                array(
                                    'header' => '<center>Documento de Identidad del Representante</center>',
                                    'name' => 'representante_id',
                                    'value' => '(isset($data->representante->documento_identidad))? $data->representante->documento_identidad : ""',
                                    'filter' => CHtml::textField('Estudiante[documento_identidad_representante]', null),
                                ),
                                array(
                                    'header' => '<center>Documento de Identidad del Estudiante</center>',
                                    'name' => 'documento_identidad',
                                    'filter' => CHtml::textField('Estudiante[documento_identidad]', null),
                                ),
                                array(
                                    'header' => '<center>Cédula Escolar</center>',
                                    'name' => 'cedula_escolar',
                                    'filter' => CHtml::textField('Estudiante[cedula_escolar]', null),
                                ),
                                array(
                                    'header' => '<center>Nombres del Estudiante</center>',
                                    'name' => 'nombres',
                                    'filter' => false,
                                    //'filter' => CHtml::textField('Estudiante[nombres]', null),
                                ),
                                array(
                                    'header' => '<center>Apellidos del Estudiante</center>',
                                    'name' => 'apellidos',
                                    'filter' => false,
                                    //'filter' => CHtml::textField('Estudiante[apellidos]', null),
                                ),
                                array(
                                    'header' => '<center>Estado</center>',
                                    'name' => 'estado_id',
                                    'value' => '(is_object($data->estado) && isset($data->estado->nombre))? $data->estado->nombre: ""',
                                    'filter' => CHtml::listData(
                                        CEstado::getData()
                                        , 'id', 'nombre'
                                    ),
                                    'filter' => false,
//                                     'filter' => CHtml::listData(
//                                            CEstado::getData()
//                                            , 'id', 'nombre'
//                                    ),
                                ),
                                array('type' => 'raw',
                                    'header' => '<center>Acciones</center>',
                                    'filter' => CHtml::hiddenField('Estudiante[plantel_id]', $plantel_id, array('id' => 'Estudiante_plantel_id')),
                                    'value' => array($this, 'columnaAcciones'),
                                ),
                            ),
                        ));
                        ?>
                    </div><!-- search-form -->
                </div><!-- search-form -->
            </div>
        </div>
    </div>
</div>
<div class="hide" id="divMsgRegNuevoEstu">
    <div class="alert alert-info bigger-110">
        <p class="bigger-110 center"> Estimado usuario, para <strong>registrar</strong> un nuevo <strong>estudiante</strong> debe hacerlo desde la funcionalidad de <strong>matrícula</strong>.</p>
    </div>
</div>

<div id="dialogSolicitudTitulo" class="hide">
    <div id="divFormVerificacionDatosEstudiante">
        <div class="widget-box">

            <div class="widget-header">
                <h5>Verificación de Datos del Estudiantes</h5>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse" id="aCollapseFormEstudianteTitulo">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>

            </div>

            <div class="widget-body">
                <div style="display: block;" class="widget-body-inner">
                    <div class="widget-main">

                        <a class="search-button" href="#"></a>
                        <div class="search-form" style="display:block">
                            <div class="widget-main form">

                                <div class="wide form">

                                    <form id="formVerificacionDatosEstudiante" name="formVerificacionDatosEstudiante" method="post" action="/estudiante/modificar/datosTitulo/id/" data-action="/estudiante/modificar/datosTitulo/id/">

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-4">
                                                    <label for="cedulaEstudianteVerif" class="col-md-12">Documento de Identidad <?php echo $required; ?></label>
                                                    <input type="text" class="span-12" required="required" readonly="readonly" name="cedulaEstudiante" id="cedulaEstudianteVerif" />
                                                    <input type="hidden" class="span-12" required="required" readonly="readonly" name="origenCedulaEstudiante" id="origenCedulaEstudianteVerif" />
                                                    <input type="hidden" class="span-12" required="required" readonly="readonly" name="nroCedulaEstudiante" id="nroCedulaEstudianteVerif" />
                                                </div>
                                                <div class="col-xs-4">
                                                    <label for="sexoEstudianteVerif" class="col-md-12">Género <?php echo $required; ?></label>
                                                    <?php echo CHtml::dropDownList('EstudianteVerif[sexo]', '', array('F'=>'Femenino', 'M'=>'Masculino'), array('required'=>true, 'class'=>'span-12', 'id'=>'sexoEstudianteVerif', 'prompt'=>'- - -')); ?>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label for="correoElectronicoEstudianteVerif" class="col-md-12">Correo Electrónico <?php echo $required; ?></label>
                                                    <input type="email" required="required" class="span-12" id="correoElectronicoEstudianteVerif" name="EstudianteVerif[correo]" />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 space-6"></div>
                                            <div class="col-xs-12">
                                                <div class="col-xs-4">
                                                    <label for="nombreEstudianteVerif" class="col-md-12">Nombre <?php echo $required; ?></label>
                                                    <input type="text" class="span-12" readonly="readonly" id="nombreEstudianteVerif" />
                                                </div>
                                                <div class="col-xs-4">
                                                    <label for="apellidoEstudianteVerif" class="col-md-12">Apellido <?php echo $required; ?></label>
                                                    <input type="text" class="span-12" readonly="readonly" id="apellidoEstudianteVerif" />
                                                </div>
                                                <div class="col-xs-4">
                                                    <label for="fechaNacimientoEstudianteVerif" class="col-md-12">Fecha de Nacimiento <?php echo $required; ?></label>
                                                    <input type="text" required="required" class="span-12" id="fechaNacimientoEstudianteVerif" name="EstudianteVerif[fecha_nacimiento]" readOnly="readOnly" />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 space-6"></div>
                                            <div class="col-xs-12">
                                                <div class="col-xs-4">
                                                    <label for="estadoIdEstudianteVerif" class="col-md-12">Estado de Nacimiento <?php echo $required; ?></label>
                                                    <?php echo CHtml::dropDownList('EstudianteVerif[estado_nac_id]', '', CHtml::listData(CEstado::getData(), 'id', 'nombre'), array('required'=>true, 'class'=>'span-12 estadoIdEstudianteVerif', 'id'=>'estadoIdEstudianteVerif', 'prompt'=>'- - -')); ?>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label for="municipioIdEstudianteVerif" class="col-md-12">Municipio de Nacimiento <?php echo $required; ?></label>
                                                    <select name="EstudianteVerif[municipio_nac_id]" id="municipioIdEstudianteVerif" class="span-12 municipioIdEstudianteVerif" required="required">
                                                        <option> - - - </option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label for="parroquiaIdEstudianteVerif" class="col-md-12">Parroquia de Nacimiento <?php echo $required; ?></label>
                                                    <select name="EstudianteVerif[parroquia_nac_id]" id="parroquiaIdEstudianteVerif" class="span-12 parroquiaIdEstudianteVerif" required="required">
                                                        <option> - - - </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 space-6"></div>
                                        <div class="text-right">
                                            <button class="btn btn-primary btn-sm hide" type="submit" id="btnSubmitVerifDatosEstudiante">
                                                Guardar <i class="fa fa-save icon-on-right"></i>
                                            </button>
                                        </div>

                                    </form>
                                </div><!-- search-form -->

                            </div><!-- search-form -->
                        </div><!-- search-form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="resultSolicitudTitulo">
    </div>
    <div id="divListaTitulos">
    </div>
</div>


<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/estudiante/estudiante.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catastro.js', CClientScript::POS_END);
?>
