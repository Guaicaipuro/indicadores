<?php
/* @var $this CrearController */
/* @var $model Estudiante */
/* @var $form CActiveForm */
?>

<div class="widget-box">

    <div class="widget-header">
        <h5>Historico del Estudiante</h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body" id="idenEstudiante">
        <div class="widget-body-inner" style="display: block;">
            <div class="widget-main form">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>
                                    <center>
                                        <b>C&oacute;digo del Plantel</b>
                                    </center>
                                </th>
                                <th>
                                    <center>
                                        <b>Nombre del Plantel</b>
                                    </center>
                                </th>
                                <th>
                                    <center>
                                        <b>Grado &oacute; Nivel</b>
                                    </center>
                                </th>
                                <th>
                                    <center>
                                        <b>Secci&oacute;n o Lapso</b>
                                    </center>
                                </th>
                                <th>
                                    <center>
                                        <b>Periodo escolar</b>
                                    </center>
                                </th>
                                <th>
                                    <center>
                                        <b>Modalidad</b>
                                    </center>
                                </th>
                                <th>
                                    <center>
                                        <b>
                                            Escolaridad
                                            <table class="table table-striped table-bordered table-hover" style="margin-bottom: -10px; width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th>RG</th>
                                                        <th>RP</th>
                                                        <th>MP</th>
                                                        <th>DI</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </b>
                                    </center>
                                </th>
                                <th>
                                    <center>
                                        <b>Estado</b>
                                    </center>
                                </th>
                                <th>
                                    <center>
                                        <b>Permanencia</b>
                                    </center>
                                </th>
                                <th>
                                    <center>
                                        <b>Observaci&oacute;n</b>
                                    </center>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($historicoEstudiante) {
                            foreach ($historicoEstudiante AS $historico) {
                                ?>
                                    <tr class="odd">
                                        <td>
                                            <div>
                                                <?php echo $historico['cod_plantel']; ?>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <?php echo $historico['nombre_plantel']; ?>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <?php echo $historico['nombre_grado']; ?>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <?php echo $historico['nombre_seccion']; ?>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <?php echo $historico['periodo']; ?>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <?php echo $historico['nombre_modalidad']; ?>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="row" align="center">
                                                <?php
                                                if(is_numeric($historico['escolaridad'])) {
                                                    $rg_check = (int)$historico['escolaridad'][0];
                                                    $rp_check = (int)$historico['escolaridad'][1];
                                                    $mp_check = (int)$historico['escolaridad'][3];
                                                    $di_check = (int)$historico['escolaridad'][5];
                                                
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', (bool) $rg_check, array('disabled' => 'disabled')); ?> </div> <?php
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', (bool) $rp_check, array('disabled' => 'disabled')); ?> </div> <?php
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', (bool) $mp_check, array('disabled' => 'disabled')); ?> </div> <?php
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', (bool) $di_check, array('disabled' => 'disabled')); ?> </div> <?php
                                                }
                                                else {
                                                    if($historico['escolaridad'] == 'REGULAR') {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', true, array('disabled' => 'disabled')); ?> </div> <?php
                                                    } else {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', false, array('disabled' => 'disabled')); ?> </div> <?php
                                                    }
                                                    if($historico['escolaridad'] == 'REPITIENTE') {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', true, array('disabled' => 'disabled')); ?> </div> <?php
                                                    } else {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', false, array('disabled' => 'disabled')); ?> </div> <?php
                                                    }
                                                    if($historico['escolaridad'] == 'MATERIA PENDIENTE') {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', true, array('disabled' => 'disabled')); ?> </div> <?php
                                                    } else {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', false, array('disabled' => 'disabled')); ?> </div> <?php
                                                    }
                                                    if($historico['escolaridad'] == 'DOBLE INSCRIPCIÓN') {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', true, array('disabled' => 'disabled')); ?> </div> <?php
                                                    } else {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', false, array('disabled' => 'disabled')); ?> </div> <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <?php echo $historico['nombre_estado']; ?>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <?php echo (isset($historico['permanencia']) AND $historico['permanencia'] != '')?$historico['permanencia']:'CURSO'; ?>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <?php echo $historico['observacion']; ?>
                                            </div>
                                        </td>
                                    </tr>
                            <?php
                            }
                        }
                        else {
                            ?>
                            <tr class="odd">
                                <td colspan="9">No mantiene historico.</td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>