<?php
/*
 * DATOS DEL PLANTEL
 */

$documento_identidad = isset($datos_header['documento_identidad']) ? $datos_header['documento_identidad'] : '';
$tdocumento_identidad = isset($datos_header['tdocumento_identidad']) ? $datos_header['tdocumento_identidad'] : '';
$nombres = isset($datos_header['nombres']) ? $datos_header['nombres'] : '';
$logo = isset($datos_header['logo']) ? Yii::app()->basePath.'/../public/uploads/LogoPlanteles/'.$datos_header['logo'] : realpath(Yii::app()->basePath.'/../public/images/indice.svg');
$fecha_nacimiento = isset($datos_header['fecha_nacimiento']) ?  date("d-m-Y",strtotime($datos_header['fecha_nacimiento'])) : '';
$capital_nacimiento = isset($datos_header['capital_nacimiento']) ?$datos_header['capital_nacimiento'] : '';
$estado_nacimiento = isset($datos_header['estado_nacimiento']) ? $datos_header['estado_nacimiento'] : '';
$nombre_plantel = isset($datos_header['nombre_plantel']) ? $datos_header['nombre_plantel'] : '';
$direccion_plantel = isset($datos_header['direccion_plantel']) ? $datos_header['direccion_plantel'] : '';
$representante = isset($datos_header['representante']) ? $datos_header['representante'] : '';
$direccion_representante = isset($datos_header['direccion_representante']) ? $datos_header['direccion_representante'] : '';
$anio_escolar= isset($datos_header['anio_escolar']) ? $datos_header['anio_escolar'] : '';
$seccion = isset($datos_header['seccion']) ? $datos_header['seccion'] : '';
$grado = isset($datos_header['grado']) ? $datos_header['grado'] : '';
$cod_plantel = isset($datos_header['cod_plantel']) ? $datos_header['cod_plantel'] : '';


$periodo_escolar = (isset($datos_header['periodo'])) ? $datos_header['periodo'] : '';
?>
<!--<img src="<?php //echo yii::app()->basePath . '/../public/images/barra_n.png';                                               ?>" />
<br /><br/>-->
<!--<img src='/var/www/escolar/web/public/images/barra_n.png' />-->
<!--<br /><br/>-->

<table width="100%" style="vert-align: middle; text-align: center; font-size: 12px; font-family: Arial;">
    <tr>

        <td align="center">
            <img align="center"
                 src="<?php echo Yii::app()->basePath . '/../public/images/escudo.png'; ?>" />
            <br/><br/><br/><br/>
        </td>
    </tr>

    <tr>
        <td align="center"  style="font-size: 19px;"><strong>REPÚBLICA BOLIVARIANA DE VENEZUELA</strong></td>
    </tr>

    <tr>
        <!--                    <td style=" text-align: center; "><strong>MINISTERIO DE EDUCACIÓN, CULTURA Y DEPORTES</strong></td>-->
        <td align="center" style="font-size: 16px;"><strong>MINISTERIO DEL PODER POPULAR PARA LA EDUCACIÓN</strong></td>
    </tr>

    <tr>
        <td align="center"  style="  font-size: 15px;"><strong>Dirección General de Registro y Control Académico</strong></td>
    </tr>

    <tr>
        <td>&nbsp;<br/><br/><br/></td>
    </tr>
</table>
<table style="font-size:8px; text-align:center; border: 1px solid grey;border-collapse: collapse; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-spacing: 0px;">

    <tr>
        <td align="center" rowspan="2" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>BOLETÍN DE EVALUACIÓN</b>
        </td>
        <td align="center" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>ESTUDIANTE</b>
        </td>
        <td align="center" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>FECHA EMISIÓN</b>
        </td>
        <td align="center" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>LISTA</b>
        </td>
        <td align="center" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>COD.PLANTEL</b>
        </td>
        <td align="center" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>NIVEL</b>
        </td>
        <td align="center" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>CURSO</b>
        </td>
        <td align="center" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>SEC.</b>
        </td>
    </tr>

    <tr>
        <td align="center" style="padding:3px;border: 1px solid grey">
            <b><?php echo $nombres;?></b>
        </td>
        <td align="center" style=" padding:3px;border: 1px solid grey">
            <b> <?php echo date("d/m/Y");?></b>
        </td>
        <td align="center" style=" padding:3px;border: 1px solid grey">

        </td>
        <td align="center" style=" padding:3px;border: 1px solid grey">
            <b><?php echo $cod_plantel; ?></b>
        </td>
        <td align="center" style=" padding:3px;border: 1px solid grey">

        </td>
        <td align="center" style=" padding:3px;border: 1px solid grey">
            <b><?php echo $grado; ?></b>
        </td>
        <td align="center" style=" padding:3px;border: 1px solid grey">
            <b><?php echo $seccion;?></b>
        </td>
    </tr>

    <tr>
        <td align="center" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>CODIGO QR</b>
        </td>
        <td align="center" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>DOCUMENTO DE IDENTIDAD</b>
        </td>
        <td align="center" colspan="2" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>FECHA NACIMIENTO</b>
        </td>
        <td align="center" colspan="2" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>CIUDAD NACIMIENTO</b>
        </td>
        <td align="center" colspan="2" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>ESTADO DE NACIMIENTO</b>
        </td>
    </tr>

    <tr>
        <td align="center" rowspan="5" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b><img width="100" height="100" src="<?php echo Yii::app()->basePath.'/../public/downloads/qr/boletines/'.$nombreQr;?>" /></b>
        </td>
        <td align="center" style=" padding:3px;border: 1px solid grey">
            <b><?php
                echo $tdocumento_identidad."-".$documento_identidad;

                ?></b>
        </td>
        <td align="center" colspan="2" style=" padding:3px;border: 1px solid grey">
            <b><?php echo $fecha_nacimiento;?></b>
        </td>
        <td align="center" colspan="2" style=" padding:3px;border: 1px solid grey">
            <b><?php echo $capital_nacimiento;?></b>
        </td>
        <td align="center" colspan="2" style=" padding:3px;border: 1px solid grey">
            <b><?php echo $estado_nacimiento;?></b>
        </td>
    </tr>

    <tr>
        <td align="center" colspan="3" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>NOMBRE Y DIRECCIÓN DEL PLANTEL</b>
        </td>
        <td align="center" colspan="3" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>NOMBRE Y DIRECCIÓN DEL PADRE O REPRESENTANTE</b>
        </td>
        <td align="center" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>AÑO ESCOLAR</b>
        </td>
    </tr>

    <tr>
        <td align="center" colspan="3" rowspan="3" style=" padding:3px;border: 1px solid grey">
            <b><?php echo $nombre_plantel . " ". $direccion_plantel;?></b>
        </td>
        <td align="center" colspan="3" rowspan="3" style=" padding:3px;border: 1px solid grey">
            <b><?php echo $representante." ".$direccion_representante;?></b>
        </td>
        <td align="center" style=" padding:3px;border: 1px solid grey">
            <b><?php echo $anio_escolar;?></b>
        </td>
    </tr>

    <tr>
        <td align="center" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            <b>INASISTENCIAS</b>
        </td>
    </tr>
    <tr>
        <td align="center" style=" padding:3px;border: 1px solid grey">
            <b>--------</b>
        </td>
    </tr>

</table>

<!--<table   style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;">
    <tr>
        <th colspan="14" align="center" style="background:#E5E5E5; padding:3px;border: 1px solid grey">
            OTORGAMIENTO DE TÍTULOS A LOS ESTUDIANTES
        </th>
    </tr>
</table>-->

