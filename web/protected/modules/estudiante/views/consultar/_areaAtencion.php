<?php if( $modelEstudianteAreaAtencion=='' || $modelEstudianteAreaAtencion==array() ) { ?>
<div class="tab-pane active" id="areaAtencion">
    <div class="widget-box">
        <div class="widget-header">
            <h5>Área de Atención</h5>
        </div>
        <div class="widget-body">
            <div style="display:block;" class="widget-body-inner">
                <div class="widget-main">
                    <div>
                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'estudiante-area-atencion-grid',
                        'dataProvider' => $modelEstudianteAreaAtencion->search(),
                        //'filter' => $modelEstudianteAreaAtencion,
                        //'ajaxUrl' => '/estudiante/modificar/actualizarGridAreaAtencion/id/'.$estudiante_id,
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        //'summaryText' => 'Mostrando {start}-{end} de {count}',
                                 'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination'),
                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                            ), 
                        'columns' => array(
                            array(
                                'header'=>'<center>Área de Atención</center>',
                                'name'=>'area_atencion_id',
                                'value'=>'$data->areaAtencion->nombre',
                            ),
                            array(
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            'value'=>'$data->estatus=="I"? "INACTIVO" : "ACTIVO"',
                        ),
                        ),
                    ));
                       ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }else{ ?>
<div class="alertDialogBox">
        <p>
            Este Estudiante no Posee Áreas de Atención todavía.
        </p>
</div>
<?php } ?>