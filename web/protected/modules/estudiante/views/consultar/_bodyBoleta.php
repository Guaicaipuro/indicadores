<!--<style type="text/css">-->
<!---->
<!--    .estudiantes table, .estudiantes th, .estudiantes td {-->
<!--        border: 1px solid grey;-->
<!--    }-->
<!--    .estudiantes table {-->
<!--        border-collapse: collapse;-->
<!--        border-spacing: 0px;-->
<!--        text-align:center;-->
<!--    }-->
<!--</style>-->

<?php
if(!in_array($nivel,array(1,2))){
?>

<br/><br/><br/><br/><br/><br/>
<table class="estudiantes" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-collapse: collapse;
       border-spacing: 0px;
       text-align:center;
       border-left: 1px solid grey;
       border-right: 1px solid grey;
       border-bottom: 1px solid grey;
       ">

    <tr style="border: 1px solid grey;">
        <th width="30%" style=" border: 1px solid grey;background:#E5E5E5;" class="center">
            RESULTADO DE LA EVALUACIÓN
        </th>
        <th width="70%" colspan="5" style=" border: 1px solid grey;background:#E5E5E5;" class="center">
            HISTORIA DEL ALUMNO DURANTE EL AÑO ESCOLAR
        </th>
    </tr>
    <tr style="border: 1px solid grey;">
        <th width="30%" style=" border: 1px solid grey;background:#E5E5E5;" class="center">
            AREAS O MATERIAS
        </th>
        <th width="20%" style=" border: 1px solid grey;background:#E5E5E5;" class="center">
            1er LAPSO
        </th>
        <th width="20%" style=" border: 1px solid grey;background:#E5E5E5;" class="center">
            2do LAPSO
        </th>
        <th width="20%" style=" border: 1px solid grey;background:#E5E5E5;" class="center">
            3er LAPSO
        </th>
        <th width="5%" style=" border: 1px solid grey;background:#E5E5E5;" class="center">
            FINAL
        </th>
        <th width="5%" style=" border: 1px solid grey;background:#E5E5E5;" class="center">
            REVISIÓN
        </th>

    </tr>


    <?php

    //  print_r($datosCalificaciones);
    foreach ($datosCalificaciones as $key) {
//
//            $tdocumento_identidad = isset($value['tdocumento_identidad']) ? $value['tdocumento_identidad'] : '';
//            $documento_identidad = isset($value['documento_identidad']) ? $value['documento_identidad'] : '';
//            $serial = isset($value['serial']) ? $value['serial'] : '';
//            $prefijo = isset($value['prefijo']) ? $value['prefijo'] : '';
//            $nombres = isset($value['nombres']) ? $value['nombres'] : '';
//            $apellidos = isset($value['apellidos']) ? $value['apellidos'] : '';
//            $anio_egreso = isset($value['anio_egreso']) ? $value['anio_egreso'] : '';
//            $tipo_evaluacion = isset($value['tipo_evaluacion']) ? $value['tipo_evaluacion'] : '';
//            $fecha_otorgamiento = isset($value['fecha_otorgamiento']) ? $value['fecha_otorgamiento'] : '';
//            $sexo = isset($value['sexo']) ? $value['sexo'] : '';
//            $fecha_nacimiento = (isset($value['fecha_nacimiento'])) ? $value['fecha_nacimiento'] : '';
//            $fecha_array = explode('-', $fecha_nacimiento);
//            $anio = isset($fecha_array[0]) ? $fecha_array[0] : '';
//            $mes = isset($fecha_array[1]) ? $fecha_array[1] : '';
//            $dia = isset($fecha_array[2]) ? $fecha_array[2] : '';
//            $fechaOtorgamiento = date("d-m-Y", strtotime($fecha_otorgamiento));
//
//            $seccion = (isset($value['seccion'])) ? $value['seccion'] : '';
//            $grado = (isset($value['grado'])) ? $value['grado'] : '';
//
//
//            if ($prefijo == '') {
//                $serial = $serial;
//            } else {
//                $serial = $prefijo . ' ' . $serial;
//            }
//
//            $sexoFinal = $sexo;
//
//            if ($sexoFinal == '1') {
//                $sexoFinal = 'M';
//            } else {
//                if ($sexoFinal == '0') {
//                    $sexoFinal = 'F';
//                }
//            }
//            if ($sexoFinal == '' || $sexoFinal == null) {
//                $sexoFinal = '';
//            }
        ?>


        <tr style="border: 1px solid grey;">
            <td width="30%" style=" border: 1px solid grey;" class="center">
                <?php echo $key['nombre']; ?>
            </td>

            <td width="20%" style=" border: 1px solid grey;" class="center">
                <?php echo $key['calif_cuantitativa1']; ?>
            </td>

            <td width="20%" style=" border: 1px solid grey;" class="center">
                <?php echo $key['calif_cuantitativa2']; ?>
            </td>

            <td width="20%" style=" border: 1px solid grey;" class="center">
                <?php echo $key['calif_cuantitativa3']; ?>
            </td>
            <td width="5%" style=" border: 1px solid grey;" class="center">
                <?php
                echo (isset($key['calif_cuantitativa1']) AND isset($key['calif_cuantitativa2']) AND isset($key['calif_cuantitativa3'])) ? round((($key['calif_cuantitativa1'] + $key['calif_cuantitativa2'] + $key['calif_cuantitativa3']) / 3), 0) : null;
                ?>
            </td>

            <td width="5%" style=" border: 1px solid grey;" class="center">
            </td>

        </tr>

    <?php
    }
    echo "</table>";

    ?>

    <br>

    <?php }else{?>

    <table class="estudiantes" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-collapse: collapse;
       border-spacing: 0px;
       text-align:center;
       border-left: 1px solid grey;
       border-right: 1px solid grey;
       border-bottom: 1px solid grey;
       ">

        <tr style="border: 1px solid grey;">
            <th colspan="3" style=" border: 1px solid grey;background:#E5E5E5;" class="center">
                HISTORIA DEL ALUMNO DURANTE EL AÑO ESCOLAR
            </th>
        </tr>
        <tr style="border: 1px solid grey;">

            <th width="20%" style=" border: 1px solid grey;background:#E5E5E5;" class="center">
                EVALUACION  1er LAPSO
            </th>
            <th width="20%" style=" border: 1px solid grey;background:#E5E5E5;" class="center">
                EVALUACION 2do LAPSO
            </th>
            <th width="20%" style=" border: 1px solid grey;background:#E5E5E5;" class="center">
                EVALUACION 3er LAPSO
            </th>
        </tr>


        <?php



        foreach ($datosCalificaciones as $key) {

            ?>


            <tr style="border: 1px solid grey;">


                <td width="33.33%" style=" border: 1px solid grey;" align="justify" valign="top">
                    <?php echo CHtml::decode($key['resumen_evaluativo1']); ?>
                </td>

                <td width="33.33%" style=" border: 1px solid grey;" align="justify" valign="top">
                    <?php  echo CHtml::decode($key['resumen_evaluativo2']); ?>
                </td>

                <td width="33.33%" style=" border: 1px solid grey;" align="justify" valign="top">
                    <?php  echo CHtml::decode($key['resumen_evaluativo3']); ?>
                </td>


            </tr>

        <?php
        } ?>

        <?php
        if(isset($key['calif_nominal'])){?>
     <tr>
         <td colspan="2" border="0" style="border-color: #fff;" align="right"><b>Calificación Final:</b> </td>

         <td  style=" border: 1px solid grey; font-size: 14px;" class="center">
         <?php echo $key['calif_nominal'];?>

        </td>
     </tr>
            <?php

        };
        ?>
            <?php
        echo "</table>";

        ?>

        <br>

<?php } ?>



