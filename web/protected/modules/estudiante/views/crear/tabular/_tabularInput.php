<?php

?>
<div class="col-md-12">
    <div class="col-md-2">
        <?php

        echo CHtml::activeLabelEx($modelo, "enfermedad_id", array('class' => "span-7"));
        ?>
        <?php echo CHtml::activeDropDownList($modelo, "[$index]enfermedad_id", CHtml::listData(CEnfermedad::getData(), 'id', 'nombre'), array('class' => "span-7  enfer_idx", 'empty' => array('' => '- SELECCIONE -'), 'empty' => '- SELECCIONE -', 'data-id' => $index, 'readOnly' => 'readOnly')) ?>
    </div>
    <div class="col-md-2">
        <?php echo CHtml::activeLabelEx($modelo, "fecha de la enfermedad", array('class' => "span-7")); ?>
        <?php echo CHtml::activeTextField($modelo, "[$index]fecha_enfermedad", array('class' => "span-7 data-doc-ident fec_enf", 'data-id' => $index, 'readOnly' => 'readOnly')); ?>
    </div>
    <!--    <div class="col-md-3">
    <?php //echo CHtml::activeLabelEx($modelo,"nombres",array('class'=>"span-7")); ?>
        <?php //echo CHtml::activeTextField($modelo,"[$index]nombres",array('class'=>"span-7",'readOnly'=>'readOnly',)); ?>
        </div>-->
<!--    <div class="col-md-3">
<?php //echo CHtml::activeLabelEx($modelo,"apellidos",array('class'=>"span-7")); ?>
        <?php //echo CHtml::activeTextField($modelo,"[$index]apellidos",array('class'=>"span-7",'readOnly'=>'readOnly')); ?>
    </div>-->