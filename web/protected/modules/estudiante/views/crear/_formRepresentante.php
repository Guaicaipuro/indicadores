<?php
/* @var $this RepresentanteController */
/* @var $model Representante */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'representante-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <div id="resultadoRepresentante"></div>
    <div id="resultado">
        <div class="infoDialogBox">
            <p>
                Debe Ingresar los Datos Generales del Representante, los campos marcados con <span class="required">*</span> son requeridos.<br>
                Es obligatorio que el estudiante mantenga un representante legal.
            </p>
        </div>
    </div>

    <?php echo $form->errorSummary($model); ?>

    <!--DIV DE REPRESENTANTE LEGAL-->
    <div class="widget-box">
        <div class="widget-header">
            <h5>Informaci&oacute;n del Representante Legal</h5>

            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body" id="idenRepresentante">
            <div class="widget-body-inner" style="display: block;">
                <div class="widget-main form">

                    <div class="widget-box">
                        <div class="widget-header">
                            <h5>Identificaci&oacute;n del Representante</h5>

                            <div class="widget-toolbar">
                                <a data-action="collapse" href="#">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body" id="idenRepresentante">
                            <div class="widget-body-inner" style="display: block;">
                                <div class="widget-main form">

                                    <div class="row">
                                        <?php
                                        if (isset($plantelPK) && $plantelPK != '') {
                                            $plantel_id = base64_encode($plantelPK['id']);
                                        } else {
                                            $plantel_id = NULL;
                                        }
                                        ?>
                                        <input type="hidden" value="<?php echo $plantel_id; ?>" name="plantel_id" />
                                        <div id="divCedulaIdentidad" class="col-md-4">
                                            <?php echo CHtml::label('Tipo de documento', '', array("class" => "col-md-12")); ?>

                                            <?php
                                            echo $form->dropDownList(
                                                    $model, 'tdocumento_identidad', array(
                                                'V' => 'Venezolana',
                                                'E' => 'Extranjero',
                                                'P' => 'Pasaporte',
                                                    ), array(
                                                'empty' => '-Seleccione-',
                                                'class' => 'span-7',
                                                    //'id' => 'tdocumento_identidad_crear',
                                                    )
                                            );
                                            ?>

                                        </div>
                                        <div id="" class="col-md-4">
                                            <?php echo CHtml::label('Documento de identidad <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            echo $form->textField($model, 'documento_identidad', array(
                                                'size' => 10,
                                                'maxlength' => 10,
                                                'class' => 'span-7',
                                                //'title' => 'Ej: V-99999999 ó E-99999999',
                                                // 'placeholder' => 'V-0000000',
//
                                                'data-toggle' => 'tooltip',
                                                'data-placement' => 'bottom',
                                                //'placeholder' => 'V-0000000',
                                                //'id' => 'cedulaRepresentante',
                                                //'onkeypress' => 'return CedulaFormat(this, event)',
                                                'style' => '-webkit-user-select:none;-moz-user-select:none;-o-user-select:none;',
                                            ));
                                            ?>
                                        </div>

                                        <div class="col-md-4" >
                                            <?php echo CHtml::label('Nombres <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php echo CHtml::textField('nombreRepresentante', '', array('class' => 'span-7', 'style' => 'text-transform:uppercase;', 'style' => '-webkit-user-select:none;-moz-user-select:none;')); ?>
                                        </div>



                                    </div>

                                    <div class="row">
                                        <div class="col-md-4" >
                                            <?php echo CHtml::label('Apellidos <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php echo CHtml::textField('apellidoRepresentante', '', array('class' => 'span-7', 'style' => 'text-transform:uppercase;')); ?>
                                        </div>
                                        <div id="divFechaNacimiento" class="col-md-4">
                                            <?php echo CHtml::label('Fecha de Nacimiento', '', array("class" => "col-md-12")); ?>
                                            <?php echo CHtml::textField('fecha_nacimiento_representante', '', array('class' => 'span-7', 'id' => 'fecha_nacimiento_representante')); ?>
                                        </div>

                                        <div id="divEstadoCivil" class="col-md-4">
                                            <?php echo CHtml::label('Estado civil <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            echo $form->dropDownList(
                                                    $model, 'estado_civil_id', CHtml::listData($estadoCivil, 'id', 'nombre'), array(
                                                'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                                    )
                                            );
                                            ?>
                                        </div>


                                    </div>

                                    <div class="row">
                                        <div id="divAfinidad" class="col-md-4">
                                            <?php echo CHtml::label('Afinidad <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            echo $form->dropDownList(
                                                    $model, 'afinidad_id', CHtml::listData($afinidad, 'id', 'nombre'), array(
                                                'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                                    )
                                            );
                                            ?>
                                        </div>
                                        <div id="divGenero" class="col-md-4">
                                            <?php echo CHtml::label('Genero <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            echo $form->dropDownList(
                                                    $model, 'sexo', array(
                                                'F' => 'FEMENINO',
                                                'M' => 'MASCULINO',
                                                    ), array(
                                                'empty' => '-Seleccione-',
                                                'class' => 'span-7'
                                                    )
                                            );
                                            ?>
                                        </div>
                                        <div id="divNacionalidad" class="col-md-4">
                                            <?php echo CHtml::label('Nacionalidad <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            echo $form->dropDownList(
                                                    $model, 'nacionalidad', array(
                                                'V' => 'VENEZOLANA',
                                                'E' => 'EXTRANJERA',
                                                    ), array('options' => array('2' => array('selected' => true)), 'class' => 'span-7')
                                            );
                                            ?>
                                        </div>


                                    </div>
                                    <div class="row">
                                        <div id="divPais" class="col-md-4">
                                            <?php echo CHtml::label('Pais de nacimiento <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            /* 248 = VENEZUELA */
                                            echo $form->dropDownList($model, 'pais_id', CHtml::listData($pais, 'id', 'nombre'), array('options' => array('248' => array('selected' => true)), 'class' => 'span-7')
                                            );
                                            ?>
                                        </div>
                                        <div id="divEstadoNacRep" class="col-md-4">
                                            <?php echo CHtml::label('Estado de nacimiento', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            echo $form->dropDownList(
                                                    $model, 'estado_nac_id', CHtml::listData($estado, 'id', 'nombre'), array(
                                                'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                                    )
                                            );
                                            ?>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="widget-box collapsed">

                        <div class="widget-header">
                            <h5>Datos de Ubicaci&oacute;n Domiciliaria del Representante</h5>

                            <div class="widget-toolbar">
                                <a data-action="collapse" href="#">
                                    <i class="icon-chevron-down"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body" id="idenRepresentante">
                            <div class="widget-body-inner" style="display: block;">
                                <div class="widget-main form">

                                    <div class="row">

                                        <div id="divEstado" class="col-md-4">
                                            <?php echo CHtml::label('Estado de domicilio <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            echo $form->dropDownList(
                                                    $model, 'estado_id', CHtml::listData($estado, 'id', 'nombre'), array(
                                                'ajax' => array(
                                                    'type' => 'GET',
                                                    'update' => '#Representante_municipio_id',
                                                    'url' => CController::createUrl('crear/seleccionarMunicipio?switch=1&type=representante'),
                                                ),
                                                'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                                    )
                                            );
                                            ?>
                                        </div>

                                        <div id="divMunicipio" class="col-md-4">
                                            <?php echo CHtml::label('Municipio de domicilio <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            echo $form->dropDownList($model, 'municipio_id', array(), array(
                                                'empty' => '- SELECCIONE -',
                                                'class' => 'span-7',
                                                'ajax' => array(
                                                    'type' => 'GET',
                                                    'update' => '#Representante_parroquia_id',
                                                    'url' => CController::createUrl('crear/seleccionarParroquia?switch=1&type=representante'),
                                                ),
                                                'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                            ));
                                            ?>
                                        </div>

                                        <div id="divParroquia" class="col-md-4">
                                            <?php echo CHtml::label('Parroquia de domicilio <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            echo $form->dropDownList($model, 'parroquia_id', array(), array(
                                                'empty' => '- SELECCIONE -',
                                                'id' => 'Representante_parroquia_id',
                                                'class' => 'span-7',
                                                'ajax' => array(
                                                    'type' => 'GET',
                                                    'update' => '#Representante_urbanizacion_id',
                                                    'url' => CController::createUrl('/estudiante/crear/seleccionarUrbanizacion?switch=1&type=representante'),
                                                    'success' => 'function(resutl) {
                                $("#Representante_urbanizacion_id").html(resutl);
                                var parroquia_id = $("#Representante_parroquia_id").val();
                                var data=
                                        {
                                            parroquia_id: parroquia_id,

                                        };
                                $.ajax({
                                    type:"GET",
                                    data:data,
                                    url:"/estudiante/crear/seleccionarPoblacion?switch=1&type=representante",
                                    update:"#Representante_poblacion_id",
                                    success:function(result){$("#Representante_poblacion_id").html(result);}
                                });
                            }',
                                                ),
                                                'empty' => array('' => '- SELECCIONE -'),
                                            ));
                                            ?>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div id="divPoblacion" class="col-md-4">
                                            <?php echo CHtml::label('Población <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            echo $form->dropDownList($model, 'poblacion_id', array(), array(
                                                'empty' => '- SELECCIONE -',
                                                'id' => 'Representante_poblacion_id',
                                                'class' => 'span-7',
                                                'empty' => array('' => '- SELECCIONE -'),
                                            ));
                                            ?>
                                        </div>

                                        <div id="divUrbanizacion" class="col-md-4">
                                            <?php echo CHtml::label('Urbanización <span class="required">*</span>', '', array("class" => "col-md-12")); ?>

                                            <?php
                                            echo $form->dropDownList($model, 'urbanizacion_id', array(), array(
                                                'empty' => '- SELECCIONE -',
                                                'id' => 'Representante_urbanizacion_id',
                                                'class' => 'span-7',
                                                'empty' => array('' => '- SELECCIONE -'),
                                            ));
                                            ?>
                                        </div>

                                        <div id="divTipoVia" class="col-md-4">
                                            <?php echo CHtml::label('Tipo de via', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            $lista = Plantel::model()->obtenerTipoVia();

                                            echo $form->dropDownList($model, 'tipo_via_id', CHtml::listData($lista, 'id', 'nombre'), array(
                                                'empty' => '- SELECCIONE -',
                                                'id' => 'Representante_tipo_via_id',
                                                'class' => 'span-7',
                                                'empty' => array('' => '- SELECCIONE -'),
                                            ));
                                            ?>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div id="divVia" class="col-md-4">
                                            <?php echo CHtml::label('Via', '', array("class" => "col-md-12")); ?>
                                            <div class="autocomplete-w1">
                                                <?php echo $form->textField($model, 'via', array('size' => 160, 'maxlength' => 160, 'placeholder' => 'Introduzca nombre de la via', 'class' => 'span-7', 'id' => 'queryVia', 'onkeyup' => 'makeUpper("#queryVia");')); ?>
                                                <div id="log" style="height: 200px; width: 300px; overflow: auto;" class="ui-widget-content" hidden="hidden"></div>
                                            </div>
                                        </div>

                                        <div id="divDireccion" class="col-md-4">
                                            <?php echo CHtml::label('Dirección de domicilio', '', array("class" => "col-md-12")); ?>
                                            <?php echo $form->textField($model, 'direccion_dom', array('size' => 6, 'maxlength' => 100, 'class' => 'span-7', 'onkeyup' => 'makeUpper("#Representante_direccion_dom");')); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="widget-box collapsed">

                        <div class="widget-header">
                            <h5>Otros datos del Representante</h5>

                            <div class="widget-toolbar">
                                <a data-action="collapse" href="#">
                                    <i class="icon-chevron-down"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body" id="idenRepresentante">
                            <div class="widget-body-inner" style="display: block;">
                                <div class="widget-main form">

                                    <div class="row">

                                        <div id="divTelefonoMovil" class="col-md-4">
                                            <?php echo CHtml::label('Teléfono movil', '', array("class" => "col-md-12")); ?>
                                            <?php echo $form->textField($model, 'telefono_movil', array('size' => 15, 'maxlength' => 15, 'class' => 'span-7')); ?>
                                        </div>

                                        <div id="divTelefonoHabitaion" class="col-md-4">
                                            <?php echo CHtml::label('Teléfono de habitación', '', array("class" => "col-md-12")); ?>
                                            <?php echo $form->textField($model, 'telefono_habitacion', array('size' => 15, 'maxlength' => 15, 'class' => 'span-7')); ?>
                                        </div>

                                        <div id="divCorreo" class="col-md-4">
                                            <?php echo CHtml::label('Correo', '', array("class" => "col-md-12")); ?>
                                            <?php echo $form->emailField($model, 'correo', array('size' => 60, 'maxlength' => 200, 'placeholder' => 'ejemplo@ejemplo.com', 'class' => 'span-7')); ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div id="divProfecion" class="col-md-4">
                                            <?php echo CHtml::label('Profesión', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            echo $form->dropDownList(
                                                    $model, 'profesion_id', CHtml::listData($profesion, 'id', 'nombre'), array(
                                                'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                                    )
                                            );
                                            ?>
                                        </div>

                                        <div id="divEmpresa" class="col-md-4">
                                            <?php echo CHtml::label('Empresa', '', array("class" => "col-md-12")); ?>
                                            <?php echo $form->textField($model, 'empresa', array('size' => 15, 'maxlength' => 200, 'class' => 'span-7')); ?>
                                        </div>

                                        <div id="divTelefonoEmpresa" class="col-md-4">
                                            <?php echo CHtml::label('Telefono de la Empresa', '', array("class" => "col-md-12")); ?>
                                            <?php echo $form->textField($model, 'telefono_empresa', array('size' => 15, 'maxlength' => 15, 'class' => 'span-7')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="dialog_error" class="hide"><p></p></div>

                    <br>

                    <div class="row">

                        <div class="col-xs-6">
                            <a class="btn btn-danger" href="/estudiante/consultar" id="btnRegresar">
                                <i class="icon-arrow-left"></i>
                                Volver
                            </a>
                        </div>

                        <div class="col-xs-6">
                            <button class="btn btn-primary btn-next pull-right" title="Pasar al siguiente paso del estudiante" data-last="Finish" type="submit">
                                Siguiente
                                <i class="fa fa-mail-forward"></i>
                            </button>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--FIN DEL DIV DEL REPRESENTANTE LEGAL-->
</div><!-- form -->

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/estudiante/estudiante.js', CClientScript::POS_END);
?>

<script>

    $(document).ready(function() {

        $('#Representante_telefono_habitacion').mask('(0299) 999-9999');
        $('#Representante_telefono_movil').mask('(0499) 999-9999');
        $('#Representante_telefono_empresa').mask('(0299) 999-9999');

        /*Enriquex*/
        $("#Representante_pais_id").on('change', function() {
            if ($("#Representante_pais_id").val() == 248)
            {
                $("#Representante_estado_nac_id").attr('disabled', false);
                $("#divEstadoNacRep").fadeIn(0);
            }
            else
            {
                $("#Representante_estado_nac_id").attr('disabled', true);
                $("#divEstadoNacRep").fadeOut(0);
//                $("#Representante_estado_nac_id").val(1);
            }
        });

        $("#nombreRepresentante").attr('readonly', true);
        $("#apellidoRepresentante").attr('readonly', true);
        $("#fecha_nacimiento_representante").attr('readonly', true);
        $("#telefonoMovil").attr('readonly', true);
        $("#telefonoLocal").attr('readonly', true);
        $("#correo").attr('readonly', true);
        $("#afinidad").attr('disabled', true);
        $("#estado_id").attr('disabled', true);

        $('#nombreRepresentante').bind('keyup blur', function() {
            keyText(this, false);

        });

        $('#email').bind('keyup blur', function() {
            keyEmail(this, false);
        });

        $("#Representante_documento_identidad").bind('change, blur', function() {
            var valorDocumentoIdentidad = $(this).val();
            var valorTDocumentoIdentidad = $("#Representante_tdocumento_identidad").val();
            if (valorDocumentoIdentidad.length > 0) {
                if ($.inArray(valorTDocumentoIdentidad, ["V", "E", "P"]) != -1) {
                    buscarCedulaRepresentante(valorDocumentoIdentidad, valorTDocumentoIdentidad);
                }
                else {
                    $("#resultadoRepresentante").html('');
                    displayDialogBox('resultadoRepresentante', 'error', 'Estimado usuario, debe seleccionar un Tipo de Documento de Identidad para proceder a buscar los datos de la persona.')
                }

            }

            else {
                $("#nombreRepresentante").val('');
                $("#apellidoRepresentante").val('');
                $("#fecha_nacimiento_representante").val('');
                $("#telefonoMovil").val('');
                $("#telefonoLocal").val('');
                $("#correo").val('');
                $("#afinidad").val('');
                $("#estado_id").val('');


                $("#nombreRepresentante").attr('readonly', true);
                $("#apellidoRepresentante").attr('readonly', true);
                $("#fecha_nacimiento_representante").attr('readonly', true);
                $("#telefonoMovil").attr('readonly', true);
                $("#telefonoLocal").attr('readonly', true);
                $("#correo").attr('readonly', true);
                $("#afinidad").attr('disabled', true);
                $("#estado_id").attr('disabled', true);
            }
        });

        $("#Representante_documento_identidad").on('blur', function() {
            generarCedulaEscolar();
        });

    });

    function buscarCedulaRepresentante(documentoIdentidad, tdocumentoIdentidad) {
        var plantel_id = $("#plantel_id").val();
        if (documentoIdentidad != '' || documentoIdentidad != null) {
            $.ajax({
                url: "/estudiante/crear/buscarCedulaRepresentante",
                data: {documentoIdentidad: documentoIdentidad,
                    tdocumentoIdentidad: tdocumentoIdentidad,
                    plantel_id: plantel_id},
                dataType: 'json',
                type: 'post',
                success: function(resp) {
                    if (resp.statusCode === "mensaje") {

                        $("#nombreRepresentante").val('');
                        $("#apellidoRepresentante").val('');
                        $("#fecha_nacimiento_representante").val('');
                        $("#Representante_telefono_movil").val('');
                        $("#Representante_telefono_habitacion").val('');
                        $("#Representante_correo").val('');
                        $("#Representante_afinidad_id").val('');
                        $("#Representante_estado_id").val('');


                        $("#nombreRepresentante").attr('readonly', true);
                        $("#apellidoRepresentante").attr('readonly', true);
                        $("#fecha_nacimiento_representante").attr('readonly', true);
                        $("#Representante_telefono_movil").attr('readonly', true);
                        $("#Representante_telefono_habitacion").attr('readonly', true);
                        $("#Representante_correo").attr('readonly', true);
                        $("#Representante_afinidad_id").attr('disabled', true);
                        $("#Representante_estado_id").attr('disabled', true);


                        $("#Representante_documento_identidad").val('');
                        dialogo_error(resp.mensaje);
                    }
                    if (resp.statusCode === "successU") {

                        if (resp.edad >= 12) {
                            $("#nombreRepresentante").val(resp.nombre);
                            $("#apellidoRepresentante").val(resp.apellido);
                            $("#fecha_nacimiento_representante").val(resp.fecha_nacimiento);
                            $("#Representante_telefono_movil").val('');
                            $("#Representante_telefono_habitacion").val('');
                            $("#Representante_correo").val('');
                            $("#Representante_afinidad_id").val('');
                            $("#Representante_estado_id").val('');

                            $("#nombreRepresentante").attr('readonly', true);
                            $("#apellidoRepresentante").attr('readonly', true);
                            $("#fecha_nacimiento_representante").attr('readonly', true);
                            $("#Representante_telefono_movil").attr('readonly', false);
                            $("#Representante_telefono_habitacion").attr('readonly', false);
                            $("#Representante_correo").attr('readonly', false);
                            $("#Representante_afinidad_id").attr('disabled', false);
                            $("#Representante_estado_id").attr('disabled', false);
                        } else {

                            $("#nombreRepresentante").val('');
                            $("#apellidoRepresentante").val('');
                            $("#fecha_nacimiento_representante").val('');
                            $("#Representante_telefono_movil").val('');
                            $("#Representante_telefono_habitacion").val('');
                            $("#Representante_correo").val('');
                            $("#Representante_afinidad_id").val('');
                            $("#Representante_estado_id").val('');

                            $("#Representante_documento_identidad").val('');
                            var mensaje = "Estimado usuario el representante debe ser mayor a 11 años.";
                            dialogo_error(mensaje);
                        }

                        if (resp.exist == true) {

                            if (resp.edad >= 12) {

                                $("#Representante_estado_civil_id").val(resp.estado_civil_id);
                                $("#Representante_sexo").val(resp.sexo);
                                $("#Representante_nacionalidad").val(resp.nacionalidad);
                                $("#Representante_pais_id").val(resp.pais_id);
                                $("#Representante_estado_nac_id").val(resp.estado_nac_id);

                                $("#Representante_empresa").val(resp.empresa);
                                $("#Representante_telefono_empresa").val(resp.telefono_empresa);
                                $("#Representante_telefono_empresa").blur();
                                $("#Representante_profesion_id").val(resp.profesion_id);

                                $("#Representante_telefono_movil").val(resp.telefonoMovil);
                                $("#Representante_telefono_habitacion").val(resp.telefonoHabitacion);
                                $("#Representante_telefono_movil").blur();
                                $("#Representante_telefono_habitacion").blur();
                                $("#Representante_correo").val(resp.correo);
                                $("#Representante_afinidad_id").val(resp.afinidad_id);
                                $("#Representante_estado_id").val(resp.estado_id);
                                $("#fecha_nacimiento_representante").val(resp.fecha_nacimiento);
                                $("#fecha_nacimiento_representante").attr('readonly', true);
                                $("#Representante_tipo_via_id").val(resp.tipo_via_id);
                                $("#Representante_direccion_dom").val(resp.direccion_dom);
                                $("#queryVia").val(resp.via);

                                if (resp.estado_id != null)
                                    $.ajax({
                                        type: "GET",
                                        url: "/estudiante/crear/seleccionarMunicipio?switch=3&type=representanteActualizado",
                                        data: {estado_id: resp.estado_id},
                                        success: function(data) {

                                            $("#Representante_municipio_id").html(data);
                                            $("#Representante_municipio_id").val(resp.municipio_id);
                                        }
                                    });

                                if (resp.municipio_id != null)
                                    $.ajax({
                                        type: "GET",
                                        url: "/estudiante/crear/seleccionarParroquia?switch=3&type=representanteActualizado",
                                        data: {municipio_id: resp.municipio_id},
                                        success: function(data) {
                                            $("#Representante_parroquia_id").html(data);
                                            $("#Representante_parroquia_id").val(resp.parroquia_id);

                                        }
                                    });

                                if (resp.poblacion_id != null || resp.urbanizacion_id != null) {
                                    var dato = {
                                        parroquia_id: resp.parroquia_id,
                                    };
                                    $.ajax({
                                        type: "GET",
                                        data: dato,
                                        url: "/estudiante/crear/seleccionarUrbanizacion?switch=3&type=representanteActualizado",
                                        update: "#Representante_urbanizacion_id",
                                        success: function(resutl) {
                                            $("#Representante_urbanizacion_id").html(resutl);
                                            $("#Representante_urbanizacion_id").val(resp.urbanizacion_id);

                                            $.ajax({
                                                type: "GET",
                                                data: dato,
                                                url: "/estudiante/crear/seleccionarPoblacion?switch=3&type=representanteActualizado",
                                                update: "#Representante_poblacion_id",
                                                success: function(result) {
                                                    $("#Representante_poblacion_id").html(result);
                                                    $("#Representante_poblacion_id").val(resp.poblacion_id);
                                                }
                                            });
                                        },
                                    });
                                }

                            } else {
                                $("#nombreRepresentante").val('');
                                $("#apellidoRepresentante").val('');
                                $("#fecha_nacimiento_representante").val('');
                                $("#Representante_telefono_movil").val('');
                                $("#Representante_telefono_habitacion").val('');
                                $("#Representante_correo").val('');
                                $("#Representante_afinidad_id").val('');
                                $("#Representante_estado_id").val('');
                                var mensaje = "Estimado usuario el representante debe ser mayor a 11 años.";
                                dialogo_error(mensaje);
                            }
                        }
                    }
                }
            });
        }
    }

   
</script>