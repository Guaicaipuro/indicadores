<?php
$this->breadcrumbs=array(
	'Constancias',
);
?>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'constancia-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <div class="widget-box">
        <div class="widget-header">
            <h5>B&uacute;squeda de Constancia</h5>
            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="widget-body">
            <div class="widget-body-inner" style="display: block;">
                <div class="widget-main">
                    <div id="validaciones">
              <div id="mensajesphp">
                  <?php
                    if (isset($error)):
                        if($error == 1):
                        $this->renderPartial('//msgBox', array('class' => 'alertDialogBox', 'message' => 'Estimado Usuario el Estudiante no existe o no se encuentra inscrito en el periodo escolar actual'));
                        endif;
                    endif;
                    ?>
             </div>
                        
                        <?php if (Yii::app()->user->hasFlash('error')) { ?>
                                    <div class="errorDialogBox"><p><?php echo Yii::app()->user->getFlash('error'); ?></p></div>
                                <?php } ?>
                        

                        <div class="infoDialogBox">
                            <p>
                                En esta Sección, podrá Solicitar la Constancia de Estudio o Promoción del Estudiante, Los Campos marcados con <span class="required">*</span> son requeridos.
                            </p>
                        </div>
                    </div>
                    <a href="#" class="search-button"></a>
                    <div style="display:block" class="search-form">
                        <div class="widget-main form">
                            <div class="row">
                                <div>
                                    <div class="col-md-4">
                                        <label class="col-md-12" for="grupo" style="font-weight: bold">Tipo de Documento <span class="required">*</span> </label>
                                        <?php
                                        echo CHtml::radioButtonList('comprobar_documento', '', array('cedula' => 'Cédula', 'pasaporte' => 'Pasaporte','cedulaEscolar'=>'Cédula Escolar'), array(
                                            'labelOptions' => array('style' => 'display:inline', 'id' => 'comprobar'), // add this code
                                            'separator' => '&nbsp &nbsp &nbsp',
                                        ));
                                        ?>
                                    </div>                                    
                                    
                                </div>
                                <div class="col-md-4 grupo">
                                    <label class="col-md-12" for="grupo" style="font-weight: bold">Nacionalidad <span class="required">*</span> </label>
                                    <?php echo Chtml::dropDownList('nacionalidad', '', array('V' => 'Venezolano', 'E' => 'Extranjero'), array('style' => 'width:52%;','empty'=>'--Seleccione--','required'=>'required','id'=>'nacionalidad')); ?>
                                </div>
                                <div class="col-md-4 cedulaEscolar hide">
                                    <?php echo CHtml::label('Cédula Escolar <span class="required">*</span>', '',array('style'=>'font-weight: bold;')); ?><br>
                                    <?php echo CHtml::textField('cedulaEscolar', '', array('id' => 'cedulaEscolar')); ?>
                                </div>
                                
                                

                            </div>
                            <div class="row grupo">
                                <div class="col-md-4 grupo">
                                    <div class="col-md-12"><label for="cedula" style="font-weight: bold">Cédula<span id="cedula_req" class="hide"> <span class="required">*</span></span> </label></div>
                                    <?php echo CHtml::textField('cedula', '', array('disabled' => 'disabled', 'id' => 'cedula')); ?>
                                </div>

                                <div class="col-md-4 grupo">
                                    <div class="col-md-12"><label for="pasaporte" style="font-weight: bold">Pasaporte<span id="pasaporte_req" class="hide"> <span class="required">*</span> </span></label></div>
                                    <?php echo CHtml::textField('pasaporte', '', array('disabled' => 'disabled', 'id' => 'pasaporte')); ?>
                                </div>
                            </div>
                            <?php //Select con el Cual se Indica al Usuario que Tipo de Constancia requiere. ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="col-md-12"><label for="tipoConstancia" style="font-weight: bold">Tipo de Constancia <span class="required">*</span></span></label></div>
                                    <?php echo CHtml::dropDownList('tipoConstancia', '', array('1'=>'Constancia de Estudio','2'=>'Constancia de Promoción de 3ER NIVEL','3'=>'Constancia de Promoción de 6TO GRADO'), array( 'id' => 'tipoConstancia')); ?>
                                </div>                                
                            </div>
                            <div class="col-md-5">
                                <?php echo CHtml::button('Buscar', array('class' => 'btn btn-info btn-xs', 'id' => 'buscar', 'style' => 'width:110px;heigth:30', 'submit' => array('constancia/buscar'))); ?>

                            </div>
                            <br>
                        </div>
                    </div>
                </div><!-- search-form -->
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#buscar").on("click", function() {
            var cedula = $.trim($('#cedula').val());
            var pasaporte = $.trim($('#pasaporte').val());
            var cedulaEscolar =$.trim($('#cedulaEscolar').val());
            if (cedula==="" && pasaporte==="" && cedulaEscolar==="") {
                $("#mensajesphp").addClass("class", "hide");
                displayDialogBox('validaciones', 'error', 'DATOS FALTANTES: Cédula, Pasaporte o Cédula Escolar no pueden ser vacios');
                return false;
            }else{
                    if($('#nacionalidad').val()==="" && cedulaEscolar==="")
                    {
                        displayDialogBox('validaciones', 'error', 'DATOS FALTANTES: Por Favor Seleccione la Nacionalidad');
                        return false;
                    }else{
                        return true;
                         }
                 }
        });      
        $("#comprobar_documento_0").click(function() {
            if ($("#comprobar_documento_0").val() === 'cedula') {
                $("#cedula").removeAttr("disabled");
                $("#pasaporte").attr("disabled", "disabled");
                $("#cedula_req").removeAttr("class", "hide");
                $("#pasaporte_req").attr("class", "hide");
                $('.grupo').show();
                $('.cedulaEscolar').addClass('hide');
                $("#pasaporte, #cedulaEscolar").val('');
            }
        });
        $("#comprobar_documento_1").click(function() {
            if ($("#comprobar_documento_1").val() === 'pasaporte') {
                $("#pasaporte").removeAttr("disabled");
                $("#cedula").attr("disabled", "disabled");
                $("#pasaporte_req").removeAttr("class", "hide");
                $("#cedula_req").attr("class", "hide");
                $('.grupo').show();
                $('.cedulaEscolar').addClass('hide');
                $("#cedulaEscolar, #cedula").val('');
            }
        });
        
        $("#comprobar_documento_2").click(function() {
            if ($("#comprobar_documento_2").val() === 'cedulaEscolar') {
                $('.grupo').hide();
                $('.cedulaEscolar').removeClass('hide');
                $('#nacionalidad').prop('selectedIndex',0);
                $("#pasaporte, #cedula").val('');
            }
        });

        $('#cedula,#cedulaEscolar').bind('keyup blur', function() {
            keyNum(this, true);
        });
        $('#cedula').bind('blur', function() {
            clearField(this);
        });
        $('#pasaporte').bind('blur', function() {
            clearField(this);
        });
        });
</script>
<?php //$this->endWidget(); ?>
</div><!-- form -->