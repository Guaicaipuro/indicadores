<?php
$documento_identidad = $resultado['documento_identidad'];
$tdocumento_identidad = $resultado['tdocumento_identidad'];
$cedula_escolar = $resultado['cedula_escolar'];

if ($documento_identidad != '' AND $documento_identidad != null) {
    if ($tdocumento_identidad != '' AND $tdocumento_identidad != null) {
        $identificacion = $tdocumento_identidad . '-' . $documento_identidad;
    } else {
        $identificacion = $documento_identidad;
    }
} else {
    if ($cedula_escolar != '' AND $cedula_escolar != null) {
        $identificacion = 'C.E: ' . $cedula_escolar;
    } else {
        $identificacion = 'NO POSEE';
    }
}
?>
<?php ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<div>
    <br>
    <br>
    <table width="100%" style="vert-align: middle; text-align: center; font-size: 16px; font-family: Arial;">

        <tr>
            <td style=" text-align: center; font-size: 19px;"><strong>REPÚBLICA BOLIVARIANA DE VENEZUELA</strong></td>
        </tr>

        <tr>
            <!--                    <td style=" text-align: center; "><strong>MINISTERIO DE EDUCACIÓN, CULTURA Y DEPORTES</strong></td>-->
            <td style=" text-align: center; font-size: 16px;"><strong>MINISTERIO DEL PODER POPULAR PARA LA EDUCACIÓN</strong></td>
        </tr>


        <tr>
            <td style=" text-align: center; font-size: 15px;"><strong>Dirección General de Registro y Control Académico</strong></td>
        </tr>

        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <br>
    <br>
    <div align="center">
        <p align="center" style="font-weight:bold;text-align: center;"> CONSTANCIA DE ESTUDIO </p>
    </div>
    <div>
        <p style="text-align: justify;text-indent: 2em;"><justify>Quien suscribe, Jefe de la Dirección de Control de Estudios del plantel educativo <strong><?php echo $resultado['nombre_plantel']; ?></strong>,
                por medio de la presente hace constar que el(la) Ciudadano(a) <strong><?php echo $resultado['apellido_estudiante'] . ' ' . $resultado['nombre_estudiante'] ?></strong>, titular del Documento de Identidad
                <strong><?php echo $identificacion; ?></strong>, esta cursando <strong><?php echo $resultado['nombre_grado']; ?></strong> en la sección <strong>'<?php echo $resultado['nombre_seccion']; ?>'</strong> de <strong><?php echo $resultado['nivel']; ?></strong>
                durante el periodo escolar <strong><?php echo $periodo_escolar; ?></strong>.
            </justify>
        </p>
    </div>
    <div>
        <p style="text-align: justify;text-indent: 2em;">
            <justify>Constancia que se expide, a petición de la parte interesada en la ciudad de <strong><?php echo  $resultado['capital']; ?></strong>,
                el día <?php echo $fecha_expedicion;?>.
            </justify>
        </p>
    </div>
    <br>
    <br>
    <div>
        <p style="text-align: center;"><?php $imagen = Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $cedula . '.png';?>
            <img style="width: 20%;" src="<?php echo $imagen ?>">
        </p>
    </div>
    <br>
    <p style="text-align: justify;text-indent: 2em;">
        En caso de requerir verificar dicho documento escanee la imagen o sino acceda a traves del siguiente enlace:
    </p>
