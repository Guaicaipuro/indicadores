
<?php if($tipoConstancia==3){ ?>
    <br><div align="center"><b>CONSTANCIA DE PROMOCIÓN<br>EN EL NIVEL DE EDUCACIÓN PRIMARIA</b></div>
<?php } else{ ?>
    <br><div align="center"><b>CONSTANCIA DE PROMOCIÓN<br>EN EL NIVEL DE EDUCACIÓN INICIAL</b></div>
<?php } ?>
<br>
<p align="justify" style="text-align: justify;text-indent: 2em;margin-right: 50px; margin-left: 50px;">
    Quien suscribe, <b>JAVIER ENRIQUE ROJAS MORENO</b>, en su condición de Director General de Registro y Control Académico del
    Ministerio del Poder Popular para la Educación, hace constar por medio de la presente que el (la) estudiante <b><?php echo $datosConstancia['nombres'].' '.$datosConstancia['apellidos']; ?></b>
    portador (a) <?php echo ($datosConstancia['cedula_escolar'])?'de la Cédula Escolar Nº':'del Documento de Identidad Nº'; ?> <b><?php echo ($datosConstancia['cedula_escolar'])?$datosConstancia['cedula_escolar']:$datosConstancia['documento_identidad']; ?></b>, nacido (a) en
    <?php echo 'el estado <b>'.$datosConstancia['estado_nac']; ?></b> en la fecha <?php echo str_replace('-','/',Utiles::transformDate($datosConstancia['fecha_nacimiento'],'-','ymd','dmy')); ?></b>, <b>cursó el <?php echo ($tipoConstancia == 2)?  'III Grupo de la Etapa Preescolar del Nivel de Educación
        Inicial': '6TO. GRADO del Nivel de Educación Primaria'; ?> </b> en el plantel <b><?php echo $datosConstancia['plantel']; ?></b>, ubicado (a) en la
    <?php echo '<b>'.$datosConstancia['direccion_plantel'].'</b>, parroquia <b>'.$datosConstancia['parroquia_plantel'].'</b>, municipio <b>'.$datosConstancia['municipio_plantel'].'</b>, estado <b>'.$datosConstancia['estado_plantel'].'</b>'; ?>,
    adscrito (a) a la Zona Educativa  <b><?php echo $datosConstancia['zona_educativa']; ?></b> y
    fue promovido (a) al <b><?php echo ($tipoConstancia == 2)?'1ER. GRADO': '1ER. AÑO';?></b> del Nivel de Educación <?php echo ($tipoConstancia == 2)?'Primaria': 'Media';?>, durante el Año Escolar <b><?php echo $datosConstancia['periodo_escolar']; ?></b> .
</p>
<p align="justify" style="text-align: justify;text-indent: 2em;margin-right: 50px; margin-left: 50px;">
    Constancia que se expide en <b><?php echo Yii::app()->user->estadoName; ?></b>, a los <b><?php echo $fecha[0]; ?></b> días del mes de <b><?php echo $meses[$fecha[1]]; ?></b>
    de <b><?php echo $fecha[2]; ?></b>.
</p>
<table  align="center" style=" border: 1px solid #000;" width="400px">
    <tr style=" border: 1px solid #000;">
        <th style="text-align: left;border: 1px solid #000;;">
            Dirección  General de Registro y Control Académico
        </th>
    </tr>
    <tr style=" border: 1px solid #000;">
        <td style="text-align: left;border: 1px solid #000;">
            Director (a): JAVIER ENRIQUE ROJAS MORENO
        </td>
    </tr>
    <tr style=" border: 1px solid #000;">
        <td style="text-align: left;border: 1px solid #000; ">
            Número de C.I.: 6842695
        </td>
    </tr>
    <tr style=" border: 1px solid #000;">
        <td  style="text-align: left;">
            Firma:
            <br>
            <table width="400px" >
                <tr>
                    <td width="40%" align="center">
                        <img
                            style="width: 150px;"
                            src="<?php echo Yii::app()->basePath . '/../' . '/public/images/firma_jefe_drce.png'; ?>"
                            />
                    </td>
                    <td width="10%" align="center">
                        &nbsp;
                    </td>
                    <td width="40%" align="center">
                        <img
                            style="width: 150px;"
                            src="<?php echo Yii::app()->basePath . '/../' . '/public/images/sello_jefe_drce.png'; ?>"
                            />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="vert-align: bottom;text-align:justify;font-size: 7px;">
            <br>
            Por delegación de firma, según Resolución Nº 025 de fecha 25 de marzo de 2014 del
            Ministerio del Poder Popular para la Educación.
            Publicada en la Gaceta Oficial de la República Bolivariana de Venezuela
            Nº 40.379, de fecha 25 de marzo de 2014
        </td>
    </tr>
</table>
<!--<table  align="center" style=" border: 1px solid #000;" width="40%">
    <tr>
        <th style="text-align: center;height: 130px;" valign="middle">
            <p >
                SELLO DE LA DIRECCIÓN
            </p>
            <p>
                GENERAL
            </p>
            <p>
                DE REGISTRO Y CONTROL
            </p>
            <p >
                ACADÉMICO
            </p>
        </th>
    </tr>
</table>-->
<br>
<table align="center" style=" border: 0px solid #FFF;" width="50%">
    <tr>
        <td align="center" style=" border: 0px solid #FFF;" >
            <img
                style="width: 130px;"
                src="<?php echo Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $datosConstancia['cedula_escolar'].'_'.$tipoConstancia. '.png'; ?>"
                />
        </td>
    </tr>
</table>

