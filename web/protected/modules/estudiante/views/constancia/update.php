<?php
/* @var $this ConstanciaController */
/* @var $model Constancia */

$this->breadcrumbs=array(
	'Constancias'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Constancia', 'url'=>array('index')),
	array('label'=>'Create Constancia', 'url'=>array('create')),
	array('label'=>'View Constancia', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Constancia', 'url'=>array('admin')),
);
?>

<h1>Update Constancia <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>