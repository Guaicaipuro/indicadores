<?php
/* @var $this EscolaridadController */
/* @var $model TalumnoAcad */

$this->breadcrumbs=array(
	'Talumno Acads'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TalumnoAcad', 'url'=>array('index')),
	array('label'=>'Create TalumnoAcad', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#talumno-acad-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Talumno Acads</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'talumno-acad-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'calumno',
		'cestadistico',
		'cgradoano',
		'cseccion',
		'cplan',
		'cescolaridad1',
		/*
		'cescolaridad2',
		'fperiodoescolar',
		'fmatricula',
		'dobservacion',
		'fegreso',
		'lapso',
		'cdea',
		'cestado',
		'cregimen',
		'cnivel',
		'cestatusegreso',
		'observacione',
		'carea_atencion',
		'tipo_matri',
		'calumno2',
		's_cedula_escolar',
		's_cedula_identidad',
		's_carnet_diplomatico',
		's_alumno_id',
		's_plantel_id',
		's_nivel_nombre',
		's_grado_nombre',
		's_periodo_nombre',
		's_modalidad_nombre',
		's_escolaridad_nombre',
		'historico',
		's_plan_id',
		'id',
		'gplantel_plantel_id',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
