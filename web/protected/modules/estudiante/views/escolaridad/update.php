<?php
/* @var $this EscolaridadController */
/* @var $model TalumnoAcad */

$this->breadcrumbs=array(
	'Talumno Acads'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TalumnoAcad', 'url'=>array('index')),
	array('label'=>'Create TalumnoAcad', 'url'=>array('create')),
	array('label'=>'View TalumnoAcad', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TalumnoAcad', 'url'=>array('admin')),
);
?>

<h1>Update TalumnoAcad <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>