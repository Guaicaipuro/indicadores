<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="tab-pane active" id="programaApoyo">


    <div class="widget-box">
        <div class="widget-header">
            <h5>Programas de Apoyo</h5>

            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div style="display:block;" class="widget-body-inner">
                <div class="widget-main">

                    <div class="row space-6"></div>
                    <div>
                        <div id="resultadoOperacion">
                            <div id="msj_exitosoDos" class="successDialogBox hide">
                            <p>

                            </p>
                            </div>
                            <div id="div-successDos" class="">
                                <p>
                                </p>
                            </div>
                            <div class="infoDialogBox">
                                <p>
                                    En esta área podrá registrar, consultar y/o actualizar sus programas de Apoyos.
                                </p>
                            </div>
                        </div>
                        <div class="pull-right" style="padding-left:10px;">
                            <a type="button" id='btnRegistrarNuevoProgramaApoyo' data-last="Finish"
                               class="btn btn-success btn-next btn-sm">
                                <i class="fa fa-plus icon-on-right"></i>
                                Registrar Nuevo Programa de Apoyo </a>
                        </div>
                        <div class="row space-20"></div>
                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'estudiante-programa-apoyo-grid',
                        'dataProvider' => $modelEstudianteProgramaApoyo->search(),
                        'filter' => $modelEstudianteProgramaApoyo,
                        'ajaxUrl' => '/estudiante/modificar/actualizarGridProgramaApoyo/id/'.$estudiante_id,
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'summaryText' => 'Mostrando {start}-{end} de {count}',
                                 'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination'),
                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                            ),
                        'afterAjaxUpdate' => "function(){
                            
                                            //FUNCION QUE PERMITE VISUALIZAR LOS DATOS DEL AREA DE ATENCION
                                            $('.verDatos').unbind('click');
                                            $('.verDatos').on('click', function(e) {
                                                e.preventDefault();
                                                var registro_id;
                                                registro_id = $(this).attr('data');
                                                Loading.show();
                                                consultarProgramaApoyo(registro_id);
                                            });
                                            
                                            //FUNCION QUE PERMITE EDITAR LOS DATOS DEL AREA DE ATENCION
                                            $('.EditarDatos').unbind('click');
                                            $('.EditarDatos').on('click', function(e) {
                                                    e.preventDefault();
                                                    var registro_id;
                                                    registro_id = $(this).attr('data');
                                                    modificarProgramaApoyo(registro_id);
                                                    });
                                            
                                            //FUNCION QUE PERMITE INACTIVAR EL REGISTRO DE AREA DE ATENCION DE RESPECTIVO ESTUDIANTE.
                                            $('.InactivarDatos').unbind('click');
                                            $('.InactivarDatos').on('click', function(e) {
                                                e.preventDefault();
                                                var registro_id;
                                                registro_id = $(this).attr('data');
                                                inactivarProgramaApoyo(registro_id);
                                            });
                                            //FUNCION QUE PERMITE ACTIVAR EL REGISTRO DE AREA DE ATENCION DE RESPECTIVO ESTUDIANTE.
                                            $('.activarDatos').unbind('click');
                                            $('.activarDatos').on('click', function(e) {
                                                e.preventDefault();
                                                var registro_id;
                                                registro_id = $(this).attr('data');
                                                activarProgramaApoyo(registro_id);
                                            });
                                                    
                                                }",
                        'columns' => array(
                            array(
                                'header'=>'<center>Programa de Apoyo</center>',
                                'name'=>'programa_apoyo_id',
                                'value'=>'$data->programaApoyo->nombre',
                                'filter'=>CHtml::listData($programaApoyo,'id','nombre'),
                            ),
                            array(
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            'filter' => array(
                                'A' => 'Activo',
                                'I' => 'Inactivo'
                            ),
                            'value' => array($this, 'estatus'),
                        ),
                        array('type' => 'raw',
                                    'header' => '<center>Acciones</center>',
                                    'value' => array($this, 'columnaAcciones'),
                             ),
                        ),
                    ));
                       ?> 
                        <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input id="estudiante_id" type="hidden" value="<?php echo $estudiante_id; ?>">
    <div id="formEstudianteProgramaApoyo" class="hide">
    </div>
    <div id="dialogPantallaDos" class="hide">
    </div> 
    <div id="dialog_inactivacionDos" class="hide">
    <div id="pregunta_inactivar" class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas Seguro(a) que Desea Inactivar este Registro de Programa de Apoyo?
        </p>
    </div>
    <div id="msj_error_inactivarDos" class="errorDialogBox hide">
        <p>
        </p>
    </div>
    </div>
    <div id="dialog_activacionDos" class="hide">
    <div id="pregunta_activarDos" class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas Seguro(a) que Desea Activar este Registro de Programa de Apoyo?
        </p>
    </div>
    <div id="msj_error_activarDos" class="errorDialogBox hide">
        <p>
        </p>
    </div>
    </div>
    <div id="dialog_error" class="hide"></div>
</div>

<script>
    $(document).on('ready',function(){
        
       //REGISTRO DEL PROGRAMA DE APOYO ASOCIADO AL ESTUDIANTE EN CUESTION. 
       $('#btnRegistrarNuevoProgramaApoyo').on('click',function(e){
           data= { estudiante_id: $('#estudiante_id').val() };
        $.ajax({
            url:'/estudiante/modificar/mostrarFormEstudianteProgramaApoyo',
            data: data,
            dataType: 'html',
            type: 'get',
            success: function(resp, resp2, resp3) { 
            try {
                                            var json = jQuery.parseJSON(resp3.responseText);
                                            if (json.status == "error") {
                                                var mensaje = '<b>'+json.mensaje+'</b>';
                                                var title = 'Programa de Apoyo';
                                                verDialogo(mensaje, title);
                                            }
                                        }
                                        catch (e) {
                                            
                                            
                                            //VENTANA MODAL QUE MUESTRA EL FORMULARIO PARA REGISTRAR LA RESPECTIVA AREA DE ATENCION    
                                            var dialogRegistrar = $("#formEstudianteProgramaApoyo").removeClass('hide').dialog({
                                                modal: true,
                                                width: '650px',
                                                draggable: false,
                                                resizable: false,
                                                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Agregar Nuevo Programa de Apoyo</h4></div>",
                                                title_html: true,
                                                buttons: [
                                                    {
                                                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                                                        "class": "btn btn-danger btn-xs",
                                                        click: function() {
                                                            $('#EstudianteProgramaApoyo_programa_apoyo_id').prop('selectedIndex',0);
                                                            $('#div-result').addClass('hide');
                                                            $('#div-successDos').addClass('hide');
                                                            $(this).dialog("close");
                                                        }
                                                    },
                                                    {
                                                        html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                                                        "class": "btn btn-primary btn-xs",
                                                        click: function() {
                                                                            //FUNCION QUE INVOCA EL REGISTRO DEL ESTUDIANTE CON SU PROGRAMA DE APOYO.
                                                                            registrarEstudianteProgramaApoyo();
                                                                          }
                                                    }
                                                ]
                                            });
                                            $("#msj_exitosoDos").addClass('hide');
                                            $("#msj_exitosoDos p").html('');
                                            $("#msj_error").addClass('hide');
                                            $("#msj_error p").html('');
                                            $("#formEstudianteProgramaApoyo").html(resp);
                                            $("html, body").animate({scrollTop: 0}, "fast");

                                                document.onkeypress=function(tecla)
                                                {
                                                        if(tecla.keyCode==13)
                                                          {
                                                            $("#estudiante-programa-apoyo-form").submit(); 
                                                            return false;
                                                          }
                                                }
                                                $("#estudiante-programa-apoyo-form").submit(function( e ) {
						e.preventDefault();
						//FUNCION QUE INVOCA EL REGISTRO DEL ESTUDIANTE CON SU AREA DE ATENCION.
                                                registrarEstudianteProgramaApoyo();
						});
                                        }
            
            }
            
            });
        });
        //Función que Permite Desplegar una Ventana para Ver el Registro del área de Atención.
        $(".verDatos").unbind('click');
        $(".verDatos").on('click', function(e) {
            e.preventDefault();
            var registro_id;
            registro_id = $(this).attr("data");
            Loading.show();
            consultarProgramaApoyo(registro_id);
        });
        //Funcion Que Permite Editar los Registros  del área de Atención.
        $(".EditarDatos").unbind('click');
        $(".EditarDatos").on('click', function(e) {
                                                    e.preventDefault();
                                                    var registro_id;
                                                    registro_id = $(this).attr("data");
                                                    modificarProgramaApoyo(registro_id);
                                                    });
        //FUNCION QUE PERMITE INACTIVAR EL REGISTRO DE AREA DE ATENCION DE RESPECTIVO ESTUDIANTE.
        $(".InactivarDatos").unbind('click');
        $(".InactivarDatos").on('click', function(e) {
            e.preventDefault();
            var registro_id;
            registro_id = $(this).attr("data");
            inactivarProgramaApoyo(registro_id);
        });
        //FUNCION QUE PERMITE ACTIVAR EL REGISTRO DE AREA DE ATENCION DE RESPECTIVO ESTUDIANTE.
        $(".activarDatos").unbind('click');
        $(".activarDatos").on('click', function(e) {
            e.preventDefault();
            var registro_id;
            registro_id = $(this).attr("data");
            activarProgramaApoyo(registro_id);
        });
    });
    
    function consultarProgramaApoyo(registro_id) {
           // alert(registro_id);
            var data = {
                id: registro_id
            };

            $.ajax({
                url: "/estudiante/modificar/VerDatosEstudianteProgramaApoyo",
                data: data,
                dataType: 'html',
                type: 'get',
                success: function(resp)
                {

                    var dialog_consultar = $("#dialogPantallaDos").removeClass('hide').dialog({
                        modal: true,
                        width: '800px',
                        draggable: false,
                        resizable: false,
                        //position: 'top',
                        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='icon-list bigger-110'></i>&nbsp; Detalles del Registro del Programa de Apoyo</h4></div>",
                        title_html: true,
                        buttons: [
                            {
                                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                                "class": "btn btn-danger btn-xs",
                                click: function() {
                                    dialog_consultar.dialog("close");
                                }
                            }
                        ]
                    });
                    try {
                        var json = jQuery.parseJSON(resp3.responseText);

                        if (json.statusCode == "error") {
                          //  alert('error');
                            $("#msj_error_consulta").removeClass('hide');
                            $("#msj_error_consulta p").html(json.mensaje);
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                    catch (e) {
                       // alert('catch');
                        $("#msj_error_consulta").addClass('hide');
                        $("#msj_error_consulta p").html('');
                        $("#dialogPantallaDos").html(resp);
                        //$("html, body").animate({scrollTop: 0}, "fast");
                    }
                }
            });
            Loading.hide();
        }
                function refrescarGrid() {
                                            $('#estudiante-programa-apoyo-grid').yiiGridView('update', {
                                                data: $(this).serialize()
                                            });
                                     }
                                     
                function activarProgramaApoyo(registro_id) {

            $("#pregunta_activarDos").removeClass('hide');
            $("#msj_error_activarDos").addClass('hide');
            $("#msj_error_activarDos p").html('');

            var dialogActivar = $("#dialog_activacionDos").removeClass('hide').dialog({
                modal: true,
                width: '600px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Registro del Programa de Apoyo</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                        "class": "btn btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "<i class='fa icon-ok bigger-110'></i>&nbsp; Activar",
                        "class": "btn btn-success btn-xs",
                        click: function() {
                            var data = {
                                id: registro_id
                            }

                            $.ajax({
                                url: "/estudiante/modificar/activarEstudianteProgramaApoyo",
                                data: data,
                                dataType: 'html',
                                type: 'get',
                                success: function(resp, resp2, resp3) {

                                    try {
                                        var json = jQuery.parseJSON(resp3.responseText);

                                        if (json.statusCode == "success") {

                                            refrescarGrid();
                                            dialogActivar.dialog('close');
                                            $("#msj_error_activarDos").addClass('hide');
                                            $("#div-successDos,#div-error").hide();
                                            $("#msj_error_activarDos p").html('');
                                            $("#msj_exitosoDos").removeClass('hide');
                                            $("#msj_exitosoDos p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }

                                        if (json.statusCode == "error") {

                                            $("#pregunta_activarDos").addClass('hide');
                                            $("#div-successDos,#div-error").hide();
                                            $("#msj_exitosoDos").addClass('hide');
                                            $("#msj_exitosoDos p").html('');
                                            $("#msj_error_activarDos").removeClass('hide');
                                            $("#msj_error_activarDos p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }
                                    }
                                    catch (e) {

                                    }
                                }
                            })
                        }
                    }
                ]
            });
        }
        
        function inactivarProgramaApoyo(registro_id) {
            $("#pregunta_inactivar").removeClass('hide');
            $("#msj_error_inactivarDos").addClass('hide');
            $("#msj_error_inactivarDos p").html('');

            var dialogInactivar = $("#dialog_inactivacionDos").removeClass('hide').dialog({
                modal: true,
                width: '600px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Inactivar Registro del Programa de Apoyo</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                        "class": "btn btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            var data = {
                                id: registro_id
                            };
                            $.ajax({
                                url: "/estudiante/modificar/inactivarEstudianteProgramaApoyo",
                                data: data,
                                dataType: 'html',
                                type: 'post',
                                success: function(resp, resp2, resp3) {
                                    try {
                                        var json = jQuery.parseJSON(resp3.responseText);
                                        if (json.statusCode == "success") {
                                            refrescarGrid();
                                            dialogInactivar.dialog('close');
                                            $("#msj_error_inactivarDos").addClass('hide');
                                            $("#div-successDos,#div-error").hide();
                                            $("#msj_error_inactivarDos p").html('');
                                            $("#msj_exitosoDos").removeClass('hide');
                                            $("#msj_exitosoDos p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }

                                        if (json.statusCode == "error") {

                                            $("#pregunta_inactivar").addClass('hide');
                                            $("#div-successDos,#div-error").hide();
                                            $("#msj_exitosoDos").addClass('hide');
                                            $("#msj_exitosoDos p").html('');
                                            $("#msj_error_inactivarDos").removeClass('hide');
                                            $("#msj_error_inactivarDos p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }
                                    }
                                    catch (e) {

                                    }
                                }
                            })
                        }
                    }
                ]
            });
        }
        
         function registrarEstudianteProgramaApoyo()
        {
            var divResult= '';
            var urlDir   = '/estudiante/modificar/guardarEstudianteProgramaApoyo';
            var datos    = $("#estudiante-programa-apoyo-form").serialize();
            var loadingEfect= true;
            var showResult= true;
            var method="POST";
            var responseFormat ="html";
            var beforeSendCallback= function(){};
            var  successCallback=function(datahtml, resp2, resp3) {
            $('*').scrollTop(100);
                            try {
                                var response = jQuery.parseJSON(resp3.responseText);
                                    if(response.status =='success'){
                                        $("#formEstudianteProgramaApoyo").addClass('hide').dialog('close');
                                        divResult='div-successDos';
                                        displayDialogBox(divResult, response.status, response.mensaje,true,true);
                                        $('#EstudianteProgramaApoyo_programa_apoyo_id').prop('selectedIndex',0);
                                        refrescarGrid();
                                                                   }
                                } catch (e) {
                                    divResult='div-error';
                                    displayHtmlInDivId(divResult, datahtml,true);
                                }
                };
            var errorCallback  = function(){};
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);                                 
            
            }
            
            function modificarEstudianteProgramaApoyo()
        {
            var divResult= 'msj_exitosoDos';
            var urlDir   = '/estudiante/modificar/modificarEstudianteProgramaApoyo';
            var datos    = $("#modificar-estudiante-programa-apoyo-form").serialize();
            var loadingEfect= true;
            var showResult= true;
            var method="POST";
            var responseFormat ="html";
            var beforeSendCallback= function(){};
            var  successCallback=function(datahtml, resp2, resp3) {
            $('*').scrollTop(100);
                            try {
                                var response = jQuery.parseJSON(resp3.responseText);
                                    if(response.status =='success'){
                                        $("#formEstudianteProgramaApoyo").addClass('hide').dialog('close');
                                        divResult='div-successDos';
                                        displayDialogBox(divResult, response.status, response.mensaje,true,true);
                                        $('#EstudianteProgramaApoyo_programa_apoyo_id').prop('selectedIndex',0);
                                        refrescarGrid();
                                                                   }
                                } catch (e) {
                                    
                                                                                divResult='div-error';
                                                                                displayHtmlInDivId(divResult, datahtml,true);
                                }
                };
            var errorCallback  = function(){};
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);                                 
            function refrescarGrid() {
                                            $('#estudiante-programa-apoyo-grid').yiiGridView('update', {
                                                data: $(this).serialize()
                                            });
                                     }
            }
            function modificarProgramaApoyo(registro_id) 
        {
                                var data = {
                                            id: registro_id,
                                            estudiante_id: $('#estudiante_id').val(),
                                            };
                                $.ajax({
                                    url: "/estudiante/modificar/mostrarDatosProgramaApoyo",
                                    data: data,
                                    dataType: 'html',
                                    type: 'get',
                                    success: function(resp, resp2, resp3) {

                                        


                                        try {
                                            var json = jQuery.parseJSON(resp3.responseText);

                                            if (json.status == "error") {
                                                var mensaje = '<b>'+json.mensaje+'</b>';
                                                var title = 'Programa de Apoyo';
                                                verDialogo(mensaje, title);
                                            }
                                        }
                                        catch (e) {
                                            
                                            
                                            var dialogRegistrar = $("#formEstudianteProgramaApoyo").removeClass('hide').dialog({
                                            modal: true,
                                            width: '650px',
                                            draggable: false,
                                            resizable: false,
                                            title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Modificar Programa de Apoyo</h4></div>",
                                            title_html: true,
                                            buttons: [
                                                {
                                                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                                                    "class": "btn btn-danger btn-xs",
                                                    click: function() {
                                                        $(this).dialog("close");
                                                    }
                                                },
                                                {
                                                    html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                                                    "class": "btn btn-primary btn-xs",
                                                    click: function() {
                                                                       modificarEstudianteProgramaApoyo(); 
                                                                      }
                                                }
                                            ],
                                        });

                                            $("#msj_exitosoDos").addClass('hide');
                                            $("#msj_exitosoDos p").html('');
                                            $("#msj_error").addClass('hide');
                                            $("#msj_error p").html('');
                                            $("#formEstudianteProgramaApoyo").html(resp);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                            
                                            document.onkeypress=function(tecla)
                                                {
                                                        if(tecla.keyCode==13)
                                                          {
                                                            $("#modificar-estudiante-programa-apoyo-form").submit(); 
                                                            return false;
                                                          }
                                                }
                                                $("#modificar-estudiante-programa-apoyo-form").submit(function( e ) {
						e.preventDefault();
						//FUNCION QUE INVOCA EL REGISTRO DEL ESTUDIANTE CON SU AREA DE ATENCION.
                                                modificarEstudianteProgramaApoyo(); 
						});
                                        }

                                    }
                                })
        }
        
        function verDialogo(message, title, style, clickCallback, reload, buttonsExtra) {
        if (!style)
            style = 'info';

        var buttonsDialog = [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if (clickCallback) {
                        clickCallback();
                    }
                    if (reload) {
                        window.location.reload();
                    }
                }
            }
        ];

        if (buttonsExtra)
            buttonsDialog.push(buttonsExtra);

        displayDialogBox('dialog_error', style, message);

        var dialog = $("#dialog_error").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: buttonsDialog
        });
    }
</script>
