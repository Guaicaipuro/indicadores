<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => $modelEstudianteAreaAtencion->id=='' ? 'estudiante-area-atencion-form' : 'modificar-estudiante-area-atencion-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <div class="widget-box">
        <div id="div-error" class="">
                <p>
                </p>
        </div>
        <div id="resultadoRegistrar" class="infoDialogBox">
            <p>
                Los campos marcados con <span class="required">*</span> son requeridos.
            </p>
        </div>
        <div class="widget-header">
            <h4>Área de Atención</h4>

            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>

        </div>

        <div class="widget-body">
            <div class="widget-body-inner" style="display: block;">
                <div class="widget-main">

                    <a href="#" class="search-button"></a>
                    <div style="display:block" class="search-form">
                        <div class="widget-main form">
                            <div class="row">     
                                    Area de Atención <span class="required">*</span>
                                    <br>
                                    <?php echo $form->dropDownList($modelEstudianteAreaAtencion, 'area_atencion_id',CHtml::listData($areaAtencion,'id','nombre'),array('required'=>'required', 'class' => 'span-5', 'empty' => '--SELECCIONE--')); ?>
                                    <?php echo $form->hiddenField($modelEstudianteAreaAtencion,'estudiante_id',array('value'=>$estudiante_id)); ?>
                                    <?php echo $form->hiddenField($modelEstudianteAreaAtencion,'id'); ?>
                                    <input type="submit" value="enviar" class="hide">
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- search-form -->
                </div><!-- search-form -->
            </div>
        </div>
    </div>
</div>