<?php if(count($titulos)>0): ?>
<table class="table table-striped table-bordered table-hover table-inscripcion">
    <thead>
        <tr>
            <th class="text-center">Serial</th>
            <th class="text-center">Credencial</th>
            <th class="text-center">Documento de Identidad</th>
            <th class="text-center">C&oacute;digo del Plantel</th>
            <th class="text-center">Fecha de Otorgamiento</th>
            <th class="text-center">Plan</th>
            <th class="text-center">Menci&oacute;n</th>
            <th class="text-center">Estatus</th>
            <th class="text-center">Descarga</th>
        </tr>
    </thead>
    <tbody>
<?php foreach ($titulos as $titulo): ?>
        <tr>
            <td class="text-center" nowrap><?php echo $titulo['serial']; ?></td>
            <td class="text-center"><?php echo $titulo['credencial']; ?></td>
            <td class="text-center"><?php echo $titulo['origen_estudiante'].'-'.$titulo['cedula_estudiante']; ?></td>
            <td class="text-center"><?php echo $titulo['cod_plantel']; ?></td>
            <td class="text-center"><?php echo $titulo['fecha_otorgamiento']; ?></td>
            <td class="text-center"><?php echo $titulo['cod_plan'].' <br> '.$titulo['nombre_plan']; ?></td>
            <td class="text-center"><?php echo $titulo['nombre_mencion']; ?></td>
            <td class="text-center"><?php echo ($titulo['en_revision']=='0')?'Aprobado':'En revisi&oacute;n'; ?></td>
            <td class="text-center">
                <a href="<?php echo Yii::app()->params['webUrl'];?>/estudiante/generarTituloDigital/visual/id/<?php echo base64_encode($titulo['titulo_id']); ?>/legacy/<?php echo base64_encode($titulo['legacy']); ?>/estudiante/<?php echo base64_encode($titulo['estudiante_id']); ?>">
<!--                <a href="estudiante/generarTituloDigital/visual/id/--><?php //echo base64_encode($titulo['titulo_id']); ?><!--/legacy/--><?php //echo base64_encode($titulo['legacy']); ?><!--/estudiante/--><?php //echo base64_encode($titulo['estudiante_id']); ?><!--">-->
                    <i class="fa fa-file-pdf-o red"></i>
                </a>
            </td>
        </tr>
<?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>
