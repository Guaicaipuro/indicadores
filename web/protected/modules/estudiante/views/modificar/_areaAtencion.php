<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="tab-pane active" id="areaAtencion">


    <div class="widget-box">
        <div class="widget-header">
            <h5>Área de Atención</h5>

            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div style="display:block;" class="widget-body-inner">
                <div class="widget-main">

                    <div class="row space-6"></div>
                    <div>
                        <div id="resultadoOperacion">
                            <div id="msj_exitoso" class="successDialogBox hide">
                            <p>

                            </p>
                            </div>
                            <div id="div-success" class="">
                                <p>
                                </p>
                            </div>
                            <div class="infoDialogBox">
                                <p>
                                    En esta área podrá registrar, consultar y/o actualizar sus áreas de Atención.
                                </p>
                            </div>
                        </div>
                        <div class="pull-right" style="padding-left:10px;">
                            <a type="button" id='btnRegistrarNuevaAreaAtencion' data-last="Finish"
                               class="btn btn-success btn-next btn-sm">
                                <i class="fa fa-plus icon-on-right"></i>
                                Registrar Nuevo Área de Atención </a>
                        </div>
                        <div class="row space-20"></div>
                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'estudiante-area-atencion-grid',
                        'dataProvider' => $modelEstudianteAreaAtencion->search(),
                        'filter' => $modelEstudianteAreaAtencion,
                        'ajaxUrl' => '/estudiante/modificar/actualizarGridAreaAtencion/id/'.$estudiante_id,
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'summaryText' => 'Mostrando {start}-{end} de {count}',
                                 'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination'),
                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                            ),
                        'afterAjaxUpdate' => "function(){
                            
                                            //FUNCION QUE PERMITE VISUALIZAR LOS DATOS DEL AREA DE ATENCION
                                            $('.verDatosAreaAtencion').unbind('click');
                                            $('.verDatosAreaAtencion').on('click', function(e) {
                                                e.preventDefault();
                                                var registro_id;
                                                registro_id = $(this).attr('data');
                                                Loading.show();
                                                consultar(registro_id);
                                            });
                                            
                                            //FUNCION QUE PERMITE EDITAR LOS DATOS DEL AREA DE ATENCION
                                            $('.EditarDatosAreaAtencion').unbind('click');
                                            $('.EditarDatosAreaAtencion').on('click', function(e) {
                                                    e.preventDefault();
                                                    var registro_id;
                                                    registro_id = $(this).attr('data');
                                                    modificar(registro_id);
                                                    });
                                            
                                            //FUNCION QUE PERMITE INACTIVAR EL REGISTRO DE AREA DE ATENCION DE RESPECTIVO ESTUDIANTE.
                                            $('.InactivarDatosAreaAtencion').unbind('click');
                                            $('.InactivarDatosAreaAtencion').on('click', function(e) {
                                                e.preventDefault();
                                                var registro_id;
                                                registro_id = $(this).attr('data');
                                                inactivar(registro_id);
                                            });
                                            //FUNCION QUE PERMITE ACTIVAR EL REGISTRO DE AREA DE ATENCION DE RESPECTIVO ESTUDIANTE.
                                            $('.activarDatosAreaAtencion').unbind('click');
                                            $('.activarDatosAreaAtencion').on('click', function(e) {
                                                e.preventDefault();
                                                var registro_id;
                                                registro_id = $(this).attr('data');
                                                activar(registro_id);
                                            });
                                                    
                                                }",
                        'columns' => array(
                            array(
                                'header'=>'<center>Área de Atención</center>',
                                'name'=>'area_atencion_id',
                                'value'=>'$data->areaAtencion->nombre',
                                'filter'=>CHtml::listData($areaAtencion,'id','nombre'),
                            ),
                            array(
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            'filter' => array(
                                'A' => 'Activo',
                                'I' => 'Inactivo'
                            ),
                            'value' => array($this, 'estatus'),
                        ),
                        array('type' => 'raw',
                                    'header' => '<center>Acciones</center>',
                                    'value' => array($this, 'columnaAccionesAreaAtencion'),
                             ),
                        ),
                    ));
                       ?> 
                        <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input id="estudiante_id" type="hidden" value="<?php echo $estudiante_id; ?>">
    <div id="formEstudianteAreaAtencion" class="hide">
    </div>
    <div id="dialogPantalla" class="hide">
    </div> 
    <div id="dialog_inactivacion" class="hide">
    <div id="pregunta_inactivar" class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas Seguro(a) que Desea Inactivar este Registro de Área de Atención?
        </p>
    </div>
    <div id="msj_error_inactivar" class="errorDialogBox hide">
        <p>
        </p>
    </div>
    </div>
    <div id="dialog_activacion" class="hide">
    <div id="pregunta_activar" class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas Seguro(a) que Desea Activar este Registro de Área de Atención?
        </p>
    </div>
    <div id="msj_error_activar" class="errorDialogBox hide">
        <p>
        </p>
    </div>
    </div>
    <div id="dialog_error" class="hide"></div>
</div>

<script>
    $(document).on('ready',function(){
        
       //REGISTRO DEL AREA DE ATENCION ASOCIADO AL ESTUDIANTE EN CUESTION. 
       $('#btnRegistrarNuevaAreaAtencion').on('click',function(e){
           data= { estudiante_id: $('#estudiante_id').val() };
        $.ajax({
            url:'/estudiante/modificar/mostrarFormEstudianteAreaAtencion',
            data: data,
            dataType: 'html',
            type: 'get',
            success: function(resp, resp2, resp3) { 
            try {
                                            var json = jQuery.parseJSON(resp3.responseText);
                                            if (json.status == "error") {
                                                var mensaje = '<b>'+json.mensaje+'</b>';
                                                var title = 'Áreas de Atención';
                                                verDialogo(mensaje, title);
                                            }
                                        }
                                        catch (e) {
                                            
                                            
                                            //VENTANA MODAL QUE MUESTRA EL FORMULARIO PARA REGISTRAR LA RESPECTIVA AREA DE ATENCION    
                                            var dialogRegistrar = $("#formEstudianteAreaAtencion").removeClass('hide').dialog({
                                                modal: true,
                                                width: '650px',
                                                draggable: false,
                                                resizable: false,
                                                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Agregar Nueva Área de Atención</h4></div>",
                                                title_html: true,
                                                buttons: [
                                                    {
                                                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                                                        "class": "btn btn-danger btn-xs",
                                                        click: function() {
                                                            $('#EstudianteAreaAtencion_area_atencion_id').prop('selectedIndex',0);
                                                            $('#div-result').addClass('hide');
                                                            $('#div-success').addClass('hide');
                                                            $(this).dialog("close");
                                                        }
                                                    },
                                                    {
                                                        html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                                                        "class": "btn btn-primary btn-xs",
                                                        click: function() {
                                                                            //FUNCION QUE INVOCA EL REGISTRO DEL ESTUDIANTE CON SU AREA DE ATENCION.
                                                                            registrarEstudianteAreaAtencion();
                                                                          }
                                                    }
                                                ]
                                            });
                                            $("#msj_exitoso").addClass('hide');
                                            $("#msj_exitoso p").html('');
                                            $("#msj_error").addClass('hide');
                                            $("#msj_error p").html('');
                                            $("#formEstudianteAreaAtencion").html(resp);
                                            $("html, body").animate({scrollTop: 0}, "fast");

                                                document.onkeypress=function(tecla)
                                                {
                                                        if(tecla.keyCode==13)
                                                          {
                                                            $("#estudiante-area-atencion-form").submit(); 
                                                            return false;
                                                          }
                                                }
                                                $("#estudiante-area-atencion-form").submit(function( e ) {
						e.preventDefault();
						//FUNCION QUE INVOCA EL REGISTRO DEL ESTUDIANTE CON SU AREA DE ATENCION.
                                                registrarEstudianteAreaAtencion();
						});
                                        }
            
            }
            
            });
        });
        //Función que Permite Desplegar una Ventana para Ver el Registro del área de Atención.
        $(".verDatosAreaAtencion").unbind('click');
        $(".verDatosAreaAtencion").on('click', function(e) {
            e.preventDefault();
            var registro_id;
            registro_id = $(this).attr("data");
            Loading.show();
            consultar(registro_id);
        });
        //Funcion Que Permite Editar los Registros  del área de Atención.
        $(".EditarDatosAreaAtencion").unbind('click');
        $(".EditarDatosAreaAtencion").on('click', function(e) {
                                                    e.preventDefault();
                                                    var registro_id;
                                                    registro_id = $(this).attr("data");
                                                    modificar(registro_id);
                                                    });
        //FUNCION QUE PERMITE INACTIVAR EL REGISTRO DE AREA DE ATENCION DE RESPECTIVO ESTUDIANTE.
        $(".InactivarDatosAreaAtencion").unbind('click');
        $(".InactivarDatosAreaAtencion").on('click', function(e) {
            e.preventDefault();
            var registro_id;
            registro_id = $(this).attr("data");
            inactivar(registro_id);
        });
        //FUNCION QUE PERMITE ACTIVAR EL REGISTRO DE AREA DE ATENCION DE RESPECTIVO ESTUDIANTE.
        $(".activarDatosAreaAtencion").unbind('click');
        $(".activarDatosAreaAtencion").on('click', function(e) {
            e.preventDefault();
            var registro_id;
            registro_id = $(this).attr("data");
            activar(registro_id);
        });
    });
    
    function consultar(registro_id) {
           // alert(registro_id);
            var data = {
                id: registro_id
            };

            $.ajax({
                url: "/estudiante/modificar/VerDatosEstudianteAreaAtencion",
                data: data,
                dataType: 'html',
                type: 'get',
                success: function(resp)
                {

                    var dialog_consultar = $("#dialogPantalla").removeClass('hide').dialog({
                        modal: true,
                        width: '800px',
                        draggable: false,
                        resizable: false,
                        //position: 'top',
                        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='icon-list bigger-110'></i>&nbsp; Detalles del Registro del Área de Atención</h4></div>",
                        title_html: true,
                        buttons: [
                            {
                                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                                "class": "btn btn-danger btn-xs",
                                click: function() {
                                    dialog_consultar.dialog("close");
                                }
                            }
                        ]
                    });
                    try {
                        var json = jQuery.parseJSON(resp3.responseText);

                        if (json.statusCode == "error") {
                          //  alert('error');
                            $("#msj_error_consulta").removeClass('hide');
                            $("#msj_error_consulta p").html(json.mensaje);
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                    catch (e) {
                       // alert('catch');
                        $("#msj_error_consulta").addClass('hide');
                        $("#msj_error_consulta p").html('');
                        $("#dialogPantalla").html(resp);
                        //$("html, body").animate({scrollTop: 0}, "fast");
                    }
                }
            });
            Loading.hide();
        }
                function refrescarGridAreaAtencion() {
                                            $('#estudiante-area-atencion-grid').yiiGridView('update', {
                                                data: $(this).serialize()
                                            });
                                     }
                                     
                function activar(registro_id) {

            $("#pregunta_activar").removeClass('hide');
            $("#msj_error_activar").addClass('hide');
            $("#msj_error_activar p").html('');

            var dialogActivar = $("#dialog_activacion").removeClass('hide').dialog({
                modal: true,
                width: '600px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Registro de Área de Atención</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                        "class": "btn btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "<i class='fa icon-ok bigger-110'></i>&nbsp; Activar",
                        "class": "btn btn-success btn-xs",
                        click: function() {
                            var data = {
                                id: registro_id
                            }

                            $.ajax({
                                url: "/estudiante/modificar/activarEstudianteAreaAtencion",
                                data: data,
                                dataType: 'html',
                                type: 'get',
                                success: function(resp, resp2, resp3) {

                                    try {
                                        var json = jQuery.parseJSON(resp3.responseText);

                                        if (json.statusCode == "success") {

                                            refrescarGridAreaAtencion();
                                            dialogActivar.dialog('close');
                                            $("#msj_error_activar").addClass('hide');
                                            $("#div-success,#div-error").hide();
                                            $("#msj_error_activar p").html('');
                                            $("#msj_exitoso").removeClass('hide');
                                            $("#msj_exitoso p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }

                                        if (json.statusCode == "error") {

                                            $("#pregunta_activar").addClass('hide');
                                            $("#div-success,#div-error").hide();
                                            $("#msj_exitoso").addClass('hide');
                                            $("#msj_exitoso p").html('');
                                            $("#msj_error_activar").removeClass('hide');
                                            $("#msj_error_activar p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }
                                    }
                                    catch (e) {

                                    }
                                }
                            })
                        }
                    }
                ]
            });
            //$("#dialog_activacion").show();
        }
        
        function inactivar(registro_id) {
            $("#pregunta_inactivar").removeClass('hide');
            $("#msj_error_inactivar").addClass('hide');
            $("#msj_error_inactivar p").html('');

            var dialogInactivar = $("#dialog_inactivacion").removeClass('hide').dialog({
                modal: true,
                width: '600px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Inactivar Registro de Área de Atención</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                        "class": "btn btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            var data = {
                                id: registro_id
                            };
                            $.ajax({
                                url: "/estudiante/modificar/inactivarEstudianteAreaAtencion",
                                data: data,
                                dataType: 'html',
                                type: 'post',
                                success: function(resp, resp2, resp3) {

                                    try {
                                        var json = jQuery.parseJSON(resp3.responseText);

                                        if (json.statusCode == "success") {

                                            refrescarGridAreaAtencion();
                                            dialogInactivar.dialog('close');
                                            $("#msj_error_inactivar").addClass('hide');
                                            $("#div-success,#div-error").hide();
                                            $("#msj_error_inactivar p").html('');
                                            $("#msj_exitoso").removeClass('hide');
                                            $("#msj_exitoso p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }

                                        if (json.statusCode == "error") {

                                            $("#pregunta_inactivar").addClass('hide');
                                            $("#div-success,#div-error").hide();
                                            $("#msj_exitoso").addClass('hide');
                                            $("#msj_exitoso p").html('');
                                            $("#msj_error_inactivar").removeClass('hide');
                                            $("#msj_error_inactivar p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }
                                    }
                                    catch (e) {

                                    }
                                }
                            })
                        }
                    }
                ]
            });
            //$("#dialog_inactivacion").show();
        }
        
         function registrarEstudianteAreaAtencion()
        {
            var divResult= '';
            var urlDir   = '/estudiante/modificar/guardarEstudianteAreaAtencion';
            var datos    = $("#estudiante-area-atencion-form").serialize();
            var loadingEfect= true;
            var showResult= true;
            var method="POST";
            var responseFormat ="html";
            var beforeSendCallback= function(){};
            var  successCallback=function(datahtml, resp2, resp3) {
            $('*').scrollTop(100);
                            try {
                                var response = jQuery.parseJSON(resp3.responseText);
                                    if(response.status =='success'){
                                        $("#formEstudianteAreaAtencion").addClass('hide').dialog('close');
                                        divResult='div-success';
                                        displayDialogBox(divResult, response.status, response.mensaje,true,true);
                                        $('#EstudianteAreaAtencion_area_atencion_id').prop('selectedIndex',0);
                                        refrescarGridAreaAtencion();
                                                                   }
                                } catch (e) {
                                    divResult='div-error';
                                    displayHtmlInDivId(divResult, datahtml,true);
                                }
                };
            var errorCallback  = function(){};
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);                                 
            
            }
            
            function modificarEstudianteAreaAtencion()
        {
            var divResult= 'msj_exitoso';
            var urlDir   = '/estudiante/modificar/modificarEstudianteAreaAtencion';
            var datos    = $("#modificar-estudiante-area-atencion-form").serialize();
            var loadingEfect= true;
            var showResult= true;
            var method="POST";
            var responseFormat ="html";
            var beforeSendCallback= function(){};
            var  successCallback=function(datahtml, resp2, resp3) {
            $('*').scrollTop(100);
                            try {
                                var response = jQuery.parseJSON(resp3.responseText);
                                    if(response.status =='success'){
                                        $("#formEstudianteAreaAtencion").addClass('hide').dialog('close');
                                        divResult='div-success';
                                        displayDialogBox(divResult, response.status, response.mensaje,true,true);
                                        $('#EstudianteAreaAtencion_area_atencion_id').prop('selectedIndex',0);
                                        refrescarGridAreaAtencion();
                                                                   }
                                } catch (e) {
                                    
                                                                                divResult='div-error';
                                                                                displayHtmlInDivId(divResult, datahtml,true);
                                }
                };
            var errorCallback  = function(){};
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback);                                 
            function refrescarGridAreaAtencion() {
                                            $('#estudiante-area-atencion-grid').yiiGridView('update', {
                                                data: $(this).serialize()
                                            });
                                     }
            }
            function modificar(registro_id) 
        {
                                var data = {
                                            id: registro_id,
                                            estudiante_id: $('#estudiante_id').val(),
                                            };
                                $.ajax({
                                    url: "/estudiante/modificar/mostrarDatosAreaAtencion",
                                    data: data,
                                    dataType: 'html',
                                    type: 'get',
                                    success: function(resp, resp2, resp3) {

                                        


                                        try {
                                            var json = jQuery.parseJSON(resp3.responseText);

                                            if (json.status == "error") {
                                                var mensaje = '<b>'+json.mensaje+'</b>';
                                                var title = 'Áreas de Atención';
                                                verDialogo(mensaje, title);
                                            }
                                        }
                                        catch (e) {
                                            
                                            
                                            var dialogRegistrar = $("#formEstudianteAreaAtencion").removeClass('hide').dialog({
                                            modal: true,
                                            width: '650px',
                                            draggable: false,
                                            resizable: false,
                                            title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Modificar Área de Atención</h4></div>",
                                            title_html: true,
                                            buttons: [
                                                {
                                                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                                                    "class": "btn btn-danger btn-xs",
                                                    click: function() {
                                                        $(this).dialog("close");
                                                    }
                                                },
                                                {
                                                    html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                                                    "class": "btn btn-primary btn-xs",
                                                    click: function() {
                                                                       modificarEstudianteAreaAtencion(); 
                                                                      }
                                                }
                                            ],
                                        });

                                            $("#msj_exitoso").addClass('hide');
                                            $("#msj_exitoso p").html('');
                                            $("#msj_error").addClass('hide');
                                            $("#msj_error p").html('');
                                            $("#formEstudianteAreaAtencion").html(resp);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                            
                                            document.onkeypress=function(tecla)
                                                {
                                                        if(tecla.keyCode==13)
                                                          {
                                                            $("#modificar-estudiante-area-atencion-form").submit(); 
                                                            return false;
                                                          }
                                                }
                                                $("#modificar-estudiante-area-atencion-form").submit(function( e ) {
						e.preventDefault();
						//FUNCION QUE INVOCA EL REGISTRO DEL ESTUDIANTE CON SU AREA DE ATENCION.
                                                modificarEstudianteAreaAtencion(); 
						});
                                        }

                                    }
                                })
        }
        
        function verDialogo(message, title, style, clickCallback, reload, buttonsExtra) {
        if (!style)
            style = 'info';

        var buttonsDialog = [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if (clickCallback) {
                        clickCallback();
                    }
                    if (reload) {
                        window.location.reload();
                    }
                }
            }
        ];

        if (buttonsExtra)
            buttonsDialog.push(buttonsExtra);

        displayDialogBox('dialog_error', style, message);

        var dialog = $("#dialog_error").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: buttonsDialog
        });
    }
</script>
