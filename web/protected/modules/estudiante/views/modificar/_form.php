<?php
/* @var $this CrearController */
/* @var $model Estudiante */
/* @var $form CActiveForm */
//
//var_dump($model);
//var_dump($estado);
?>

    <div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'modificar-estudiante-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
        ));
        ?>

        <div id="resultadoEstudiante"></div>

        <?php
        if (isset($plantelAnteriorPK) && $plantelAnteriorPK != '') {
            ?>

            <div class = "widget-box collapsed">

                <div class = "widget-header">
                    <h5>Identificación del Plantel Anterior <?php echo '"' . $plantelAnteriorPK['nombre'] . '"'; ?></h5>

                    <div class = "widget-toolbar">
                        <a href = "#" data-action = "collapse">
                            <i class = "icon-chevron-down"></i>
                        </a>
                    </div>

                </div>

                <div class = "widget-body">
                    <div style = "display: none;" class = "widget-body-inner">
                        <div class = "widget-main">

                            <div class="row row-fluid center">
                                <div id="1eraFila" class="col-md-12">
                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Código del Plantel</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($plantelPK['cod_plantel'] != null) {
                                            echo CHtml::textField('', $plantelPK['cod_plantel'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Código Estadistico</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($plantelPK['cod_estadistico'] != null) {
                                            echo CHtml::textField('', $plantelPK['cod_estadistico'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Denominación</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($plantelPK['denominacion_id'] != null) {
                                            echo CHtml::textField('', $plantelPK->denominacion->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>

                                </div>

                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                <div id="2daFila" class="col-md-12">
                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Nombre del Plantel</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($plantelPK['nombre'] != null) {
                                            echo CHtml::textField('', $plantelPK['nombre'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Zona Educativa</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($plantelPK['zona_educativa_id'] != null) {
                                            echo CHtml::textField('', $plantelPK->zonaEducativa->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Estado</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($plantelPK['estado_id'] != null) {
                                            echo CHtml::textField('', $plantelPK->estado->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>

                                </div>

                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                <div id="3raFila" class="col-md-12">
                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Grado</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($model->grado_anterior_id != null) {
                                            echo CHtml::textField('', $model->gradoAnterior->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>


        <?php
        if (isset($plantelPK) && $plantelPK != '') {
            ?>
            <div class = "widget-box collapsed">

                <div class = "widget-header">
                    <h5>Identificación del Plantel <?php echo '"' . $plantelPK['nombre'] . '"'; ?></h5>

                    <div class = "widget-toolbar">
                        <a href = "#" data-action = "collapse">
                            <i class = "icon-chevron-down"></i>
                        </a>
                    </div>

                </div>

                <div class = "widget-body">
                    <div style = "display: none;" class = "widget-body-inner">
                        <div class = "widget-main">

                            <div class="row row-fluid center">
                                <div id="1eraFila" class="col-md-12">
                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Código del Plantel</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($plantelPK['cod_plantel'] != null) {
                                            echo CHtml::textField('', $plantelPK['cod_plantel'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Código Estadistico</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($plantelPK['cod_estadistico'] != null) {
                                            echo CHtml::textField('', $plantelPK['cod_estadistico'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Denominación</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($plantelPK['denominacion_id'] != null) {
                                            echo CHtml::textField('', $plantelPK->denominacion->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>

                                </div>

                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                <div id="2daFila" class="col-md-12">
                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Nombre del Plantel</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($plantelPK['nombre'] != null) {
                                            echo CHtml::textField('', $plantelPK['nombre'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Zona Educativa</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($plantelPK['zona_educativa_id'] != null) {
                                            echo CHtml::textField('', $plantelPK->zonaEducativa->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Estado</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($plantelPK['estado_id'] != null) {
                                            echo CHtml::textField('', $plantelPK->estado->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>

                                </div>

                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                <div id="3raFila" class="col-md-12">
                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('<b>Grado</b>', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        if ($model->grado_actual_id != null) {
                                            echo CHtml::textField('', $model->gradoActual->nombre, array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        } else {
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        <?php
        }
        ?>

        <div id="resultado">
            <div class="infoDialogBox">
                <p>
                    Debe Ingresar los Datos Generales del Estudiante, los campos marcados con <span class="required">*</span> son requeridos.
                </p>
            </div>
        </div>

        <div class="widget-box">

            <div class="widget-header">
                <h5>Identificaci&oacute;n del Estudiante</h5>

                <div class="widget-toolbar">
                    <a data-action="collapse" href="#">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>
            <?php
            //    var_dump($model);
            //    die();
            //    var_dump($model->tdocumento_identidad);
            //    die();
            ?>
            <div class="widget-body" id="idenEstudiante">
                <div class="widget-body-inner" style="display: block;">
                    <div class="widget-main form">
                        <div class="row">
                            <div id="divCedulaIdentidad" class="col-md-4">
                                <input type="hidden" value="<?php echo $_REQUEST['id']; ?>" name="estudiante_id" />
                                <input type="hidden" value="<?php echo $model->documento_identidad; ?>" name="cedula_hidden" id="cedula_hidden" />
                                <input type="hidden" value="<?php echo $model->tdocumento_identidad; ?>" name="tdocumento_identidad" id="tdocumento_identidad_hidden" />
                                <input type="hidden" value="<?php echo $model->nombres; ?>" name="nombres_hidden" id="nombres_hidden" />
                                <input type="hidden" value="<?php echo $model->apellidos; ?>" name="apellidos_hidden" id="apellidos_hidden" />
                                <input type="hidden" value="<?php echo $model->cedula_escolar; ?>" name="cedula_escolar_hidden" id="cedula_escolar_hidden" />
                                <input type="hidden" value="<?php echo $model->fecha_nacimiento; ?>" name="fecha_hidden" id="fecha_hidden" />
                                <?php
                                //                        if($model->documento_identidad == null || Yii::app()->user->pbac('estudiante.modificar.admin')) {
                                ?>


                                <?php echo CHtml::label('Tipo de documento', '', array("class" => "col-md-12")); ?>

                                <?php
                                echo $form->dropDownList(
                                    $model, 'tdocumento_identidad', array(
                                    'V' => 'Venezolana',
                                    'E' => 'Extranjero',
                                    'P' => 'Pasaporte',
                                    'C' => 'Cédula Extranjera',
                                    'D' => 'Carnet Diplomático',
                                    'O' => 'Otra Identificación',
                                ), array(
                                        'empty' => '-Seleccione-',
                                        'class' => 'span-7'
                                    )
                                );
                                ?>

                            </div>
                            <div class="col-md-4">
                                <?php echo CHtml::label('Documento de Identidad', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->textField($model, 'documento_identidad', array(
                                    'size' => 10,
                                    'maxlength' => 10,
                                    'class' => 'span-7',
                                    'title' => 'Ej: V-99999999 ó E-99999999',
                                    //'placeholder' => 'V-0000000',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'bottom',
                                    //'placeholder' => 'V-0000000',
                                    // 'id' => 'cedulaEstudiante',
                                    //'onkeypress' => 'return CedulaFormat(this, event)',
                                    'style' => '-webkit-user-select:none;-moz-user-select:none;-o-user-select:none;',
                                ));
                                ?>
                                <?php
                                //                        }
                                //                        else
                                //                        {
                                //                            echo $form->textField($model,'documento_identidad', array('disabled' => 'disabled', 'class' => 'span-7'));
                                //                        }
                                ?>
                            </div>

                            <div id="divNombres" class="col-md-4">
                                <?php echo CHtml::label('Nombres <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'nombres', array('size' => 60, 'maxlength' => 120, 'class' => 'span-7')); ?>
                            </div>

                        </div>

                        <div class="row">
                            <div id="divApellidos" class="col-md-4">
                                <?php echo CHtml::label('Apellidos <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'apellidos', array('size' => 60, 'maxlength' => 120, 'class' => 'span-7')); ?>
                            </div>
                            <div id="divFechaNacimiento" class="col-md-4">
                                <?php echo CHtml::label('Fecha de nacimiento <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'fecha_nacimiento', array('class' => 'span-7', 'id' => 'fecha', 'required' => 'required', 'readOnly' => 'readOnly')); ?>
                            </div>

                            <div id="divLateralidadMano" class="col-md-4">
                                <?php echo CHtml::label('Lateralidad', '', array("class" => "span-7")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'lateralidad_mano', array(
                                    'DER' => 'DERECHO',
                                    'IZQ' => 'IZQUIERDO',
                                    'AMB' => 'AMBIDIESTRO',
                                ), array(
                                        'empty' => '-Seleccione-',
                                        'class' => 'span-7',
                                        'id' => 'Estudiante_lateralidad'
                                    )
                                );
                                ?>
                            </div>

                        </div>

                        <div class="row">
                            <div id="divGenero" class="col-md-4">
                                <?php echo CHtml::label('Genero <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'sexo', array(
                                    'F' => 'FEMENINO',
                                    'M' => 'MASCULINO',
                                ), array(
                                        'empty' => '-Seleccione-',
                                        'class' => 'span-7'
                                    )
                                );
                                ?>
                            </div>
                            <div id="divCedulaEscolar" class="col-md-4">
                                <?php echo CHtml::label('Cédula Escolar <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <!--<input type="text" value="<?php echo $model->cedula_escolar ?>" class="span-7" disabled="disabled" />-->
                                <?php echo $form->textField($model, 'cedula_escolar', array('size' => 11, 'maxlength' => 11, 'class' => 'span-7', 'disabled' => 'disabled')); ?>
                            </div>


                            <div id="divEstadoCivil" class="col-md-4">
                                <?php echo CHtml::label('Estado civil <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'estado_civil_id', CHtml::listData($estadoCivil, 'id', 'nombre'), array(
                                        'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                    )
                                );
                                ?>
                            </div>

                        </div>

                        <div class="row">
                            <div id="divNacionalidad" class="col-md-4">
                                <?php echo CHtml::label('Nacionalidad <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'nacionalidad', array(
                                    'V' => 'VENEZOLANA',
                                    'E' => 'EXTRANJERA',
                                ), array('options' => array('2' => array('selected' => true)), 'class' => 'span-7')
                                );
                                ?>
                            </div>
                            <div id="divPais" class="col-md-4">
                                <?php echo CHtml::label('Pais de nacimiento <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                /* 248 = VENEZUELA */
                                echo $form->dropDownList(
                                    $model, 'pais_id', CHtml::listData($pais, 'id', 'nombre'), array('class' => 'span-7')
                                );
                                ?>
                            </div>

                            <div id="divEstadoNac" class="col-md-4">
                                <?php echo CHtml::label('Estado de nacimiento', '', array("class" => "col-md-12", 'required')); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'estado_nac_id', CHtml::listData($estado, 'id', 'nombre'), array('empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7')
                                );
                                ?>
                            </div>


                            <div id="divEscolaridad" class="col-md-4">
                                <?php
                                //                            if($escolaridadEstudiante != '') {
                                //                                echo CHtml::label('Escolaridad', '', array("class" => "col-md-12"));
                                //                                foreach ($escolaridadEstudiante as $escolaridad) {
                                //                                    echo '<input type="checkbox" checked disabled> ' . $escolaridad . '<br>';
                                //                                }
                                //                            }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div id="divEtnia" class="col-md-4">
                                <?php echo CHtml::label('Pueblos y Comunidades Indigenas', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'etnia_id', CHtml::listData($etnia, 'id', 'nombre'), array(
                                        'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                    )
                                );
                                ?>
                            </div>
                            <div id="divHablaIdiomaEtnia" class="col-md-4 <?php if($model->etnia_id=='') echo "hide"; ?>">
                                <?php echo CHtml::label('¿Habla El Idioma?  <span class="required">*</span>', '', array("class" => "col-md-12",'id'=>'IdiomaEtnia')); ?>
                                <?php
                                echo $form->radioButtonList( $model, 'idioma_etnia', array(1=>'SI',0=>'NO'),array('class'=>'idioma_etnia', 'labelOptions'=>array('style'=>'display:inline'),'separator'=>' ') );
                                ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <br>

        <div class="widget-box collapsed">

            <div class="widget-header">
                <h5>Datos de Ubicaci&oacute;n Domiciliaria del Estudiante</h5>

                <div class="widget-toolbar">
                    <a data-action="collapse" href="#">
                        <i class="icon-chevron-down"></i>
                    </a>
                </div>
            </div>

            <div class="widget-body" id="idenEstudiante">
                <div class="widget-body-inner" style="display: block;">
                    <div class="widget-main form">
                        <div class="row">

                            <div id="divEstado" class="col-md-4">
                                <?php echo CHtml::label('Estado de domicilio <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'estado_id', CHtml::listData($estado, 'id', 'nombre'), array(
                                        'ajax' => array(
                                            'type' => 'GET',
                                            'update' => '#Estudiante_municipio_id',
                                            'url' => CController::createUrl('crear/seleccionarMunicipio?switch=1&type=estudiante'),
                                        ),
                                        'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                    )
                                );
                                ?>
                            </div>

                            <div id="divMunicipio" class="col-md-4">
                                <?php echo CHtml::label('Municipio de domicilio <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList($model, 'municipio_id', array(), array(
                                    'empty' => '- SELECCIONE -',
                                    'class' => 'span-7',
                                    'ajax' => array(
                                        'type' => 'GET',
                                        'update' => '#Estudiante_parroquia_id',
                                        'url' => CController::createUrl('crear/seleccionarParroquia?switch=1&type=estudiante'),
                                    ),
                                    'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                ));
                                ?>
                            </div>

                            <div id="divParroquia" class="col-md-4">
                                <?php echo CHtml::label('Parroquia de domicilio <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList($model, 'parroquia_id', array(), array(
                                    'empty' => '- SELECCIONE -',
                                    'id' => 'Estudiante_parroquia_id',
                                    'class' => 'span-7',
                                    'ajax' => array(
                                        'type' => 'GET',
                                        'update' => '#Estudiante_urbanizacion_id',
                                        'url' => CController::createUrl('crear/seleccionarUrbanizacion?switch=1&type=estudiante'),
                                        'success' => 'function(resutl) {
                                    $("#Estudiante_urbanizacion_id").html(resutl);
                                    var parroquia_id = $("#Estudiante_parroquia_id").val();
                                    var data=
                                            {
                                                parroquia_id: parroquia_id,

                                            };
                                    $.ajax({
                                        type:"GET",
                                        data:data,
                                        url:"/estudiante/crear/seleccionarPoblacion?switch=1&type=estudiante",
                                        update:"#Estudiante_poblacion_id",
                                        success:function(result){  $("#Estudiante_poblacion_id").html(result);}
                                    });
                                }',
                                    ),
                                    'empty' => array('' => '- SELECCIONE -'),
                                ));
                                ?>
                            </div>

                        </div>

                        <div class="row">
                            <div id="divPoblacion" class="col-md-4">
                                <?php echo CHtml::label('Población <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList($model, 'poblacion_id', array(), array(
                                    'empty' => '- SELECCIONE -',
                                    'id' => 'Estudiante_poblacion_id',
                                    'class' => 'span-7',
                                    'empty' => array('' => '- SELECCIONE -'),
                                ));
                                ?>
                            </div>

                            <div id="divUrbanizacion" class="col-md-4">
                                <?php echo CHtml::label('Urbanización <span class="required">*</span>', '', array("class" => "col-md-12")); ?>

                                <?php
                                echo $form->dropDownList($model, 'urbanizacion_id', array(), array(
                                    'empty' => '- SELECCIONE -',
                                    'id' => 'Estudiante_urbanizacion_id',
                                    'class' => 'span-7',
                                    'empty' => array('' => '- SELECCIONE -'),
                                ));
                                ?>
                            </div>

                            <div id="divTipoVia" class="col-md-4">
                                <?php echo CHtml::label('Tipo de via <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                $lista = Plantel::model()->obtenerTipoVia();

                                echo $form->dropDownList($model, 'tipo_via_id', CHtml::listData($lista, 'id', 'nombre'), array(
                                    'empty' => '- SELECCIONE -',
                                    'id' => 'Estudiante_tipo_via_id',
                                    'class' => 'span-7',
                                    'empty' => array('' => '- SELECCIONE -'),
                                ));
                                ?>
                            </div>
                        </div>

                        <div class="row">

                            <div id="divVia" class="col-md-4">
                                <?php echo CHtml::label('Via <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <div class="autocomplete-w1">
                                    <?php echo $form->textField($model, 'via', array('size' => 160, 'maxlength' => 160, 'placeholder' => 'Introduzca nombre de la via', 'class' => 'span-7', 'id' => 'query', 'onkeyup' => 'makeUpper("#query");')); ?>
                                    <div id="log" style="height: 200px; width: 300px; overflow: auto;" class="ui-widget-content" hidden="hidden"></div>
                                </div>
                            </div>

                            <div id="divDireccion" class="col-md-4">
                                <?php echo CHtml::label('Dirección de domicilio <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'direccion_dom', array('size' => 6, 'maxlength' => 100, 'class' => 'span-7', 'onkeyup' => 'makeUpper("#Estudiante_direccion_dom");')); ?>
                            </div>

                            <div id="divIdentificacionExtranjera" class="col-md-4">
                                <?php echo CHtml::label('Zona de ubicación <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'zona_ubicacion_id', CHtml::listData($zonaUbicacion, 'id', 'nombre'), array(
                                        'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                    )
                                );
                                ?>
                            </div>

                        </div>

                        <div class="row">

                            <div id="divTipoVivienda" class="col-md-4">
                                <?php echo CHtml::label('Tipo de vivienda <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'tipo_vivienda_id', CHtml::listData($tipoVivienda, 'id', 'nombre'), array(
                                        'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                    )
                                );
                                ?>
                            </div>

                            <div id="divUbicacionVivienda" class="col-md-4">
                                <?php echo CHtml::label('Ubicación de vivienda <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'ubicacion_vivienda_id', CHtml::listData($ubicacionVivienda, 'id', 'nombre'), array(
                                        'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                    )
                                );
                                ?>
                            </div>

                            <div id="divCondicionVivienda" class="col-md-4">
                                <?php echo CHtml::label('Condición de vivienda <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'condicion_vivienda_id', CHtml::listData($condicionVivienda, 'id', 'nombre'), array(
                                        'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                    )
                                );
                                ?>
                            </div>

                        </div>

                        <div class="row">

                            <div id="divCondicionInfraestructura" class="col-md-4">
                                <?php echo CHtml::label('Condición de infraestructura <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'condicion_infraestructura_id', CHtml::listData($condicionInfraestructura, 'id', 'nombre'), array(
                                        'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                    )
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>

        <div class="widget-box collapsed">

            <div class="widget-header">
                <h5>Otros datos del Estudiante</h5>

                <div class="widget-toolbar">
                    <a data-action="collapse" href="#">
                        <i class="icon-chevron-down"></i>
                    </a>
                </div>
            </div>

            <div class="widget-body" id="idenEstudiante">
                <div class="widget-body-inner" style="display: block;">
                    <div class="widget-main form">

                        <div class="row">

                            <div id="divBeca" class="col-md-4">
                                <?php echo CHtml::label('Beca <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'beca', array(
                                    '1' => 'SI',
                                    '0' => 'NO',
                                ), array(
                                        'empty' => '-Seleccione-',
                                        'class' => 'span-7',
                                        'id' => 'Beca'
                                    )
                                );
                                ?>
                            </div>

                            <div id="divIngresoFamiliar" class="col-md-4">
                                <?php echo CHtml::label('Ingreso familiar', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'ingreso_familiar', array('size' => 20, 'maxlength' => 10, 'class' => 'span-7')); ?>
                            </div>

                            <div id="divCanaima" class="col-md-4">
                                <?php echo CHtml::label('Canaima <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'canaima', array(
                                    '' => '-Seleccione-',
                                    '1' => 'SI',
                                    '0' => 'NO'
                                ), array(
                                        'class' => 'span-7',
                                        'id' => 'Canaima'
                                    )
                                );
                                ?>
                            </div>

                        </div>

                        <div class="row">
                            <div id="divSerialCanaima" class="col-md-4">
                                <?php echo CHtml::label('Serial de la canaima', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'serial_canaima', array('size' => 50, 'maxlength' => 50, 'class' => 'span-7')); ?>
                            </div>

                            <div id="divTelefonoMovil" class="col-md-4">
                                <?php echo CHtml::label('Teléfono movil', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'telefono_movil', array('size' => 15, 'maxlength' => 15, 'class' => 'span-7')); ?>
                            </div>

                            <div id="divTelefonoHabitaion" class="col-md-4">
                                <?php echo CHtml::label('Teléfono de habitación', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->textField($model, 'telefono_habitacion', array('size' => 15, 'maxlength' => 15, 'class' => 'span-7')); ?>
                            </div>
                        </div>

                        <div class="row">
                            <div id="divCorreo" class="col-md-4">
                                <?php echo CHtml::label('Correo', '', array("class" => "col-md-12")); ?>
                                <?php echo $form->emailField($model, 'correo', array('size' => 60, 'maxlength' => 200, 'placeholder' => 'ejemplo@ejemplo.com', 'class' => 'span-7')); ?>
                            </div>

                            <?php /* ?>
                            <div id="divDiversidadFuncional" class="col-md-4">
                                <?php echo CHtml::label('Áreas de Atención', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo $form->dropDownList(
                                    $model, 'diversidad_funcional_id', CHtml::listData($diversidadFuncional, 'id', 'nombre'), array(
                                        'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                                    )
                                );
                                ?>
                            </div>
                            <?php */ ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div id="dialog_error" class="hide"><p></p></div>

        <br>

        <div class="row">

            <div class="col-xs-6">
                <a class="btn btn-danger" href="<?php echo $urlVolver; ?>" id="btnRegresar">
                    <i class="icon-arrow-left"></i>
                    Volver
                </a>
            </div>

            <div class="col-xs-6">
                <button class="btn btn-primary btn-next pull-right" title="Guardar Datos generales del Estudiante" data-last="Finish" type="submit">
                    Guardar
                    <i class="icon-save icon-on-right"></i>
                </button>
            </div>

        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->


    <script>

        $(document).ready(function() {
            $('#Estudiante_telefono_habitacion').mask('(0299) 999-9999');
            $('#Estudiante_telefono_movil').mask('(0499) 999-9999');

            var estado_id, municipio_id, parroquia_id, isAdmin;
            estado_id = '<?php ($model->estado_id) ? print($model->estado_id) : print(null) ?>';
            isAdmin = '<?php (Yii::app()->user->pbac('admin')) ? print(TRUE) : print(null) ?>';
            municipio_id = '<?php ($model->municipio_id) ? print($model->municipio_id) : print(null) ?>';
            parroquia_id = '<?php ($model->parroquia_id) ? print($model->parroquia_id) : print(null) ?>';
            poblacion_id = '<?php ($model->poblacion_id) ? print($model->poblacion_id) : print(null) ?>';
            urbanizacion_id = '<?php ($model->urbanizacion_id) ? print($model->urbanizacion_id) : print(null) ?>';
            id_representa = '<?php ($model->id) ? print($model->id) : print(null) ?>';

            if (isAdmin != '') {
                $("#Estudiante_cedula_escolar").attr('disabled', false);
            }



            if (estado_id != '') {

                $.ajax({
                    type: "GET",
                    url: "/estudiante/crear/seleccionarMunicipio/?switch=3&type=representanteActualizado",
                    data: {estado_id: estado_id},
                    success: function(data) {

                        $("#Estudiante_municipio_id").html(data);
                        $("#Estudiante_municipio_id").val(municipio_id);
                    }
                });
            }
            if (municipio_id != '') {
                $.ajax({
                    type: "GET",
                    url: "/estudiante/crear/seleccionarParroquia/?switch=3&type=representanteActualizado",
                    data: {municipio_id: municipio_id},
                    success: function(data) {
                        $("#Estudiante_parroquia_id").html(data);
                        $("#Estudiante_parroquia_id").val(parroquia_id);

                    }
                });
            }
            if (poblacion_id != '' || urbanizacion_id != '') {
                var dato = {
                    parroquia_id: parroquia_id,
                };
                $.ajax({
                    type: "GET",
                    data: dato,
                    url: "/estudiante/crear/seleccionarUrbanizacion/?switch=3&type=representanteActualizado",
                    update: "#Plantel_urbanizacion_id",
                    success: function(resutl) {
                        $("#Estudiante_urbanizacion_id").html(resutl);
                        $("#Estudiante_urbanizacion_id").val(urbanizacion_id);


                        $.ajax({
                            type: "GET",
                            data: dato,
                            url: "/estudiante/crear/seleccionarPoblacion/?switch=3&type=representanteActualizado",
                            update: "#Plantel_poblacion_id",
                            success: function(result) {
                                $("#Estudiante_poblacion_id").html(result);
                                $("#Estudiante_poblacion_id").val(poblacion_id);
                            }
                        });
                    },
                });
            }

            $("#cedula").bind('blur', function() {

                var valorCedula = $(this).val();
                var cedula_hidden = $("#cedula_hidden").val();

//        alert(cedula_hidden);
//        alert(valorCedula);

                if (valorCedula != cedula_hidden) {
                    if (valorCedula.length > 0) {
                        var cedula = valorCedula;
                        buscarCedulaAutoridad(cedula);
                        generarCedulaEscolar();
                    }
                    else {

                        $("#cedula").val("");
                        $("#Estudiante_nombres").val('');
                        $("#Estudiante_apellidos").val('');
                        $("#cedula_escolar").val('');
                        $("#fecha").val('');
                        $("#Estudiante_nombres").attr('readonly', false);
                        $("#Estudiante_apellidos").attr('readonly', false);
                        generarCedulaEscolar();
                    }
                }
            });



            $("#Estudiante_estatura").bind('keyup blur', function() {
                keyNum(this, true);
            });

            $("#Estudiante_nombres").bind('keyup blur', function() {
                keyTextOnly(this, true);
                makeUpper(this);
            });

            $("#Estudiante_apellidos").bind('keyup blur', function() {
                keyTextOnly(this, true);
                makeUpper(this);
            });

            $("#Estudiante_estatura").bind('blur', function() {
                var mensaje1 = "El valor en el campo Estatura no es correcto, el Rango de estatura es de 0.40m a 2.50m ";
                var estatura = $("#Estudiante_estatura").val();
                //var num=estatura.substring(0,1);
                if (estatura.length > 0 || estatura !== 0 || estatura !== 0.00) {
                    if ((estatura >= 0.40) && (estatura <= 2.50)) {
                        // dialog_error(num);
                    } else {
                        dialogo_error(mensaje1);
                        $("#Estudiante_estatura").val('');
                    }
                }
            });

            $("#orden_nacimiento").on('change', function() {
                generarCedulaEscolar();
            });

            /*Enriquex*/
            $("#Estudiante_pais_id").on('change', function() {
                if ($("#Estudiante_pais_id").val() == 248)
                {
                    $("#Estudiante_estado_nac_id").attr('disabled', false);
                }
                else
                {
                    $("#Estudiante_estado_nac_id").attr('disabled', true);
                }
            });

            /*Enriquex*/
            $("#Estudiante_serial_canaima").attr('disabled', true);
            $("#Canaima").on('change', function() {
                canaima = $("#Canaima").val();
                if (canaima == 0) {
                    $("#Estudiante_serial_canaima").attr('disabled', true);
                    $("#Estudiante_serial_canaima").val('');
                }
                else if (canaima == 1) {
                    $("#Estudiante_serial_canaima").attr('disabled', false);
                    $("#Estudiante_serial_canaima").focus();
                }
                else {
                    $("#Estudiante_serial_canaima").attr('disabled', true);
                    $("#Estudiante_serial_canaima").val('');
                }
            });


            /*****************Meylin***************************/
            $('#fecha').unbind('click');
            $('#fecha').unbind('focus');

            //Array para dar formato en español
            $.datepicker.regional['es'] =
            {
                closeText: 'Cerrar',
                prevText: 'Previo',
                nextText: 'Próximo',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                    'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
                dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                dateFormat: 'dd-mm-yy',
                'showOn': 'focus',
                'showOtherMonths': false,
                'selectOtherMonths': true,
                'changeMonth': true,
                'changeYear': true,
                minDate: new Date(1800, 1, 1),
                maxDate: 'today',
                onSelect: function(selected, evnt) {
                    generarCedulaEscolar();
                },
                initStatus: 'Selecciona la fecha', isRTL: false};
            $.datepicker.setDefaults($.datepicker.regional['es']);
            $('#fecha').datepicker();
        });//------------FIN DOCUMENT

    </script>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/estudiante/estudiante.js', CClientScript::POS_END);
?>