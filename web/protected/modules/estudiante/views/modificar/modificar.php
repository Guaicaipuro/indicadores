<?php
$this->breadcrumbs = array(
    'Estudiantes' => array('/estudiante/'),
    'Modificar Estudiante',
);
$urlVolver = '/estudiante/';
?>

<div class="form">
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#representante" id="representanteTab">Representante</a></li>
            <li><a data-toggle="tab" href="#estudiante" id="estudianteTab">Estudiante</a></li>
            <li><a data-toggle="tab" href="#historico" id="historicoTab">Historico del estudiante</a></li>
            <!--            <li><a data-toggle="tab" href="#historialMedico" id="historialMedicoTab">Historial medico</a></li>-->
            <li><a data-toggle="tab" href="#datosAntropometricos" id="datosAntropometricosTab">Datos Antropometricos</a></li>
            <li><a data-toggle="tab" href="#areaAtencion" id="areaAtencionTab">Área de Atención</a></li>
            <li><a data-toggle="tab" href="#programaApoyo" id="programaApoyoTab">Programa de Apoyo</a></li>
            <li><a data-toggle="tab" href="#notasEstudiante" id="notasEstudianteTab">Notas</a></li>
            <li><a data-toggle="tab" href="#tituloEstudiante" id="tituloEstudianteTab">Título</a></li>
        </ul>

        <div class="tab-content">

            <div id="notasEstudiante" class="tab-pane">
                <?php
                $this->renderPartial('_formNotasEstudiante', array(

                    'dataProvider' => $dataProvider,
                    'resultadoBusqueda' => $resultadoBusqueda,
                    'historicoEstudiante' => $historicoEstudiante,
                    'dataConsultarNota' => $dataConsultarNota,

                    'urlVolver' => $urlVolver));
                ?>
            </div>

            <div id="tituloEstudiante" class="tab-pane">
                <?php
                $this->renderPartial('_formTituloEstudiante', array(

                    'dataProvider' => $dataProvider,
                    'resultadoBusqueda' => $resultadoBusqueda,
                    'historicoEstudiante' => $historicoEstudiante,
                    'dataConsultarNota' => $dataConsultarNota,
                    'urlVolver' => $urlVolver,
                    'firmantes'=>$firmantes,
                    'sinacoes'=>$sinacoes));
                ?>
            </div>

            <div id="historico" class="tab-pane">
                <?php
                $this->renderPartial('_formHistorico', array('model' => $model, 'estadoCivil' => $estadoCivil, 'genero' => $genero, 'estado' => $estado, 'pais' => $pais, 'zonaUbicacion' => $zonaUbicacion, 'condicionVivienda' => $condicionVivienda, 'tipoVivienda' => $tipoVivienda, 'ubicacionVivienda' => $ubicacionVivienda, 'condicionInfraestructura' => $condicionInfraestructura, 'etnia' => $etnia, 'diversidadFuncional' => $diversidadFuncional, 'plantelPK' => $plantelPK, '$plantelAnteriorPK' => $plantelAnteriorPK, 'urlVolver' => $urlVolver, 'historicoEstudiante' => $historicoEstudiante));
                ?>
            </div>
            <div id="estudiante" class="tab-pane">
                <?php
                $this->renderPartial('_form', array('model' => $model, 'estadoCivil' => $estadoCivil, 'genero' => $genero, 'estado' => $estado, 'pais' => $pais, 'zonaUbicacion' => $zonaUbicacion, 'condicionVivienda' => $condicionVivienda, 'tipoVivienda' => $tipoVivienda, 'ubicacionVivienda' => $ubicacionVivienda, 'condicionInfraestructura' => $condicionInfraestructura, 'etnia' => $etnia, 'diversidadFuncional' => $diversidadFuncional, 'plantel_id' => $plantel_id, 'plantelPK' => $plantelPK, 'plantelAnteriorPK' => $plantelAnteriorPK, 'urlVolver' => $urlVolver));
                ?>
            </div>
            <div id="historialMedico" class="tab-pane">
                <?php
                $this->renderPartial('_formHistorialMedico', array('model' => $modelHistorialMedico, 'estadoCivil' => $estadoCivil, 'genero' => $genero, 'estado' => $estado, 'pais' => $pais, 'zonaUbicacion' => $zonaUbicacion, 'condicionVivienda' => $condicionVivienda, 'tipoVivienda' => $tipoVivienda, 'ubicacionVivienda' => $ubicacionVivienda, 'condicionInfraestructura' => $condicionInfraestructura, 'etnia' => $etnia, 'diversidadFuncional' => $diversidadFuncional, 'tipoSangre' => $tipoSangre, 'plantel_id' => $plantel_id, 'plantelPK' => $plantelPK));
                ?>
            </div>
            <div id="datosAntropometricos" class="tab-pane">
                <?php
                $this->renderPartial('_formDatosAntropometricos', array('model' => $modelDatosAntropometricos,'estudiante_id'=>$estudiante_id, 'estadoCivil' => $estadoCivil, 'genero' => $genero, 'estado' => $estado, 'pais' => $pais, 'zonaUbicacion' => $zonaUbicacion, 'condicionVivienda' => $condicionVivienda, 'tipoVivienda' => $tipoVivienda, 'ubicacionVivienda' => $ubicacionVivienda, 'condicionInfraestructura' => $condicionInfraestructura, 'etnia' => $etnia, 'diversidadFuncional' => $diversidadFuncional, 'plantel_id' => $plantel_id, 'plantelPK' => $plantelPK));
                ?>
            </div>

            <?php /*@author Jonathan Huaman  Fecha 20/04/2015
                                    *  Integración de la Pestaña Área de Atención a la Estructura de Modificar del Estudiante.
                                    *  @params se Envían los Parámetros estudiante_id=>código del Estudiante, modelEstudianteAreaAtención=>Modelo EstudianteAreaAtencion, areaAtencion=>los Datos de las Areas de Atención.
                                    */
            ?>
            <div id="areaAtencion" class="tab-pane ">
                <?php
                $this->renderPartial('_areaAtencion', array(
                    'estudiante_id' => $estudiante_id,
                    'modelEstudianteAreaAtencion'=>$modelEstudianteAreaAtencion,
                    'areaAtencion'=>$areaAtencion,
                ));
                ?>
            </div>
            <div id="programaApoyo" class="tab-pane">
                <?php
                $this->renderPartial('_programaApoyo', array(
                    'estudiante_id' => $estudiante_id,
                    'modelEstudianteProgramaApoyo'=>$modelEstudianteProgramaApoyo,
                    'programaApoyo'=>$programaApoyo,
                ));
                ?>
            </div>

            <div id="representante" class="tab-pane active">
                <?php
                $this->renderPartial('_formRepresentante', array('model' => $modelRepresentante, 'estadoCivil' => $estadoCivil, 'genero' => $genero, 'estado' => $estado, 'pais' => $pais, 'zonaUbicacion' => $zonaUbicacion, 'condicionVivienda' => $condicionVivienda, 'tipoVivienda' => $tipoVivienda, 'ubicacionVivienda' => $ubicacionVivienda, 'condicionInfraestructura' => $condicionInfraestructura, 'etnia' => $etnia, 'afinidad' => $afinidad, 'diversidadFuncional' => $diversidadFuncional, 'profesion' => $profesion, 'plantel_id' => $plantel_id, 'plantelPK' => $plantelPK, 'urlVolver' => $urlVolver));
                ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $.mask.definitions['~']='[+-]';
    });
</script>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js', CClientScript::POS_END);?>
