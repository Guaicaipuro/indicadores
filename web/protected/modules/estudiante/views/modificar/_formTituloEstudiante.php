<div class = "widget-box">

    <div class="widget-header">
        <h5>Consulta de T&iacute;tulo </h5>
        <div class="widget-toolbar">
            <a  href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div id="consultaTitulo2" class="widget-body" >
        <div class="widget-body-inner" >
            <div class="widget-main form">

                <div class="row">
                    <div class="col-md-12" id ="solicitud-titulo2">
                        <?php
//                            var_dump($dataProvider);
//                                    die();
//var_dump($dataProvider);
//die();
                        if ($dataProvider) {

                            $this->widget(
                                    'zii.widgets.grid.CGridView', array(
                                'id' => 'solicitudTitulo-grid',
                                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                'dataProvider' => $dataProvider,
                                'summaryText' => false,
                                'columns' => array(
                                    //                                        array(
//                                            "name" => "boton",
//                                            "type" => "raw",
//                                            'header' => '<div class="center">' . CHtml::checkBox('selec_todoEst', false, array('id' => 'selec_todoEst', 'title' => 'Seleccionar todos', 'class' => 'tooltipMatricula')) . "</div>"
//                                        ),
                                    array(
                                        'name' => 'cedula_identidad',
                                        'type' => 'raw',
                                        'header' => '<center><b>Cédula Identidad</b></center>'
                                    ),
                                    array(
                                        'name' => 'nombreApellido',
                                        'type' => 'raw',
                                        'header' => '<center><b>Nombres y Apellidos</b></center>'
                                    ),
                                    array(
                                        'name' => 'serial',
                                        'type' => 'raw',
                                        'header' => '<center><b> Serial Asignado </b></center>'
                                    ),
                                    array(
                                        'name' => 'plantelCode_estadi',
                                        'type' => 'raw',
                                        'header' => '<center><b> Plantel y Código Estadístico </b></center>'
                                    ),
                                    array(
                                        'name' => 'plan_nombre',
                                        'type' => 'raw',
                                        'header' => '<center><b> Nombre del Plan</b></center>'
                                    ),
                                    array(
                                        'name' => 'nombre_mencion',
                                        'type' => 'raw',
                                        'header' => '<center><b>Mención</b></center>'
                                    ),
                                    array(
                                        'name' => 'anoEgreso',
                                        'type' => 'raw',
                                        'header' => '<center><b>Año de Egreso</b></center>'
                                    ),
                                    array(
                                        'name' => 'fechaOtorgacion',
                                        'type' => 'raw',
                                        'header' => '<center><b>Fecha de Expedición</b></center>'
                                    ),
                                ),
                                'pager' => array(
                                    'header' => '',
                                    'htmlOptions' => array('class' => 'pagination'),
                                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                    'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                    'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                    'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                ),
                                    )
                            );
                            if($dataProvider != array()):?>
                                <div class="space-6"><div class="row"></div></div>
                                <div id="" class="grid-view">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th colspan="4"><center><b>AUTORIDADES FIRMANTES DEL DOCUMENTO</b></center></th>
                                        </tr>
                                        <tr>
                                            <th ><center><b>Cargo</b></center></td>
                                            <th ><center><b>Cédula</b></center></td>
                                            <th ><center><b>Nombres</b></center></td>
                                            <th ><center><b>Apellidos</b></center></td>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        if($sinacoes){
                                            if(count($firmantes)>0){
                                                foreach($firmantes as $key => $item){
                                                    echo "<tr>";
                                                    echo "<td ><center>".$item['ccargo']."</center></td>";
                                                    echo "<td ><center>".$item['ccedula']."</center></td>";
                                                    echo "<td ><center>".$item['dnombres']."</center></td>";
                                                    echo "<td ><center>".$item['dapellidos']."</center></td>";
                                                    echo "</tr>";
                                                }
                                            }
                                            else {
                                                echo "<tr>";
                                                echo "<td colspan='4'>No se encontraron resultado</td>";
                                                echo "<tr>";
                                            }
                                        }
                                        else {
                                            if(array_key_exists('origen_dir_plantel',$resultadoBusqueda[0]) AND array_key_exists('cedula_dir_plantel',$resultadoBusqueda[0]) AND array_key_exists('nombre_dir_plantel',$resultadoBusqueda[0])){
                                                echo "<tr>";
                                                echo "<td ><center>DIRECTOR</center></td>";
                                                if(!is_null($resultadoBusqueda[0]['origen_dir_plantel']) OR !is_null($resultadoBusqueda[0]['cedula_dir_plantel']) OR !is_null($resultadoBusqueda[0]['nombre_dir_plantel'])){
                                                    echo "<td ><center>".((!is_null($resultadoBusqueda[0]['origen_dir_plantel']))?$resultadoBusqueda[0]['origen_dir_plantel']:'V').'-'.$resultadoBusqueda[0]['cedula_dir_plantel']."</center></td>";
                                                    echo "<td colspan='2'><center>".strtoupper($resultadoBusqueda[0]['nombre_dir_plantel'])."</center></td>";
                                                }
                                                else {
                                                    echo "<td colspan='3'><center>NO ASIGNADO</center></td>";
                                                }
                                                echo "</tr>";
                                            }
                                            if(array_key_exists('origen_drcee_zona',$resultadoBusqueda[0]) AND array_key_exists('cedula_drcee_zona',$resultadoBusqueda[0]) AND array_key_exists('nombre_apellido_drcee_zona',$resultadoBusqueda[0])){
                                                echo "<tr>";
                                                echo "<td ><center>JEFE DE REGISTRO Y CONTROL DE ESTUDIO Y EVALUACIÓN DE ZONA</center></td>";
                                                if(!is_null($resultadoBusqueda[0]['origen_drcee_zona']) OR !is_null($resultadoBusqueda[0]['cedula_drcee_zona']) OR !is_null($resultadoBusqueda[0]['nombre_apellido_drcee_zona'])) {
                                                    echo "<td ><center>" . ((!is_null($resultadoBusqueda[0]['origen_drcee_zona']))?$resultadoBusqueda[0]['origen_drcee_zona']:'V'). '-' . $resultadoBusqueda[0]['cedula_drcee_zona'] . "</center></td>";
                                                    echo "<td colspan='2'><center>" . strtoupper($resultadoBusqueda[0]['nombre_apellido_drcee_zona']) . "</center></td>";
                                                }
                                                else {
                                                    echo "<td colspan='3'><center>NO ASIGNADO</center></td>";
                                                }
                                                echo "</tr>";
                                            }
                                            if(array_key_exists('origen_funcio_desig',$resultadoBusqueda[0]) AND array_key_exists('cedula_funcio_desig',$resultadoBusqueda[0]) AND array_key_exists('nombre_funcio_desig',$resultadoBusqueda[0])){
                                                echo "<tr>";
                                                echo "<td ><center>FUNCIONARIO DESIGNADO MPPE</center></td>";
                                                if(!is_null($resultadoBusqueda[0]['origen_funcio_desig']) OR !is_null($resultadoBusqueda[0]['cedula_funcio_desig']) OR !is_null($resultadoBusqueda[0]['nombre_funcio_desig'])) {
                                                    echo "<td ><center>".((!is_null($resultadoBusqueda[0]['origen_funcio_desig']))?$resultadoBusqueda[0]['origen_funcio_desig']:'V').'-'.$resultadoBusqueda[0]['cedula_funcio_desig']."</center></td>";
                                                    echo "<td colspan='2'><center>".strtoupper($resultadoBusqueda[0]['nombre_funcio_desig'])."</center></td>";
                                                }
                                                else {
                                                    echo "<td colspan='3'><center>NO ASIGNADO</center></td>";
                                                }
                                                echo "</tr>";
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif;
                        } else {
                                ?>
                                    <div class="infoDialogBox">
                                            <p>
                                                <strong>Este estudiante no posee Título registrado en el sistema.</strong>
                                            </p>
                                        </div>
                                        <!--                        <div class="panel-body">
                                                                    <p style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
        
                                            </p>
                                                            </div>-->
                                    <?php
                                }
                        ?>

                    </div>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>

</div>