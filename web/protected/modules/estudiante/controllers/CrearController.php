<?php

/**
 * Modulo congelado para comenzar con el modulo de calificaciones.
 * culminar la interfaz de estudiante en todas las pestañas
 * realizar las validaciones
 */
class CrearController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    const MODULO = "Estudiante.Crear";

    static $_permissionControl = array(
        'read' => 'Creación de Estudiantes',
        'write' => 'Creación de Estudiantes',
        'label' => 'Creación de Estudiantes'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                /*                 * ********ALEXIS*********** */
                'actions' => array(
                    'index',
                    'GuardarHistorialMedico',
                    'enfermedadHistorial',
                    'vacunaHistorial',
                    'seleccionarMunicipio',
                    'seleccionarParroquia',
                    'addTabularInputs',
                    'addTabularInputsVacuna',
                    'seleccionarPoblacion',
                    'addTabularInputsAsTable',
                    'seleccionarUrbanizacion',
                    'seleccionarTipoVia',
                    'buscarEstudiante',
                    'viaAutoComplete',
                    'buscarCedulaRepresentante'),
                'pbac' => array('read', 'write'),
            ),
            /*   array('allow', // allow authenticated user to perform 'create' and 'update' actions
              'actions' => array('create', 'update'),
              'users' => array('@'),
              ),
              array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions' => array('admin', 'delete'),
              'users' => array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    public function actions() {
        return array(
            'addTabularInputs' => array(
                'class' => 'ext.yii-playground.actions.XTabularInputAction',
                'modelName' => 'EnfermedadEstudiante',
                'viewName' => 'tabular/_tabularInput',
            ),
            'addTabularInputsVacuna' => array(
                'class' => 'ext.yii-playground.actions.XTabularInputAction',
                'modelName' => 'VacunaEstudiante',
                'viewName' => 'tabular/_tabularInputVacuna',
            ),
//            'addTabularInputsAsTable' => array(
//                'class' => 'ext.yii-playground.actions.XTabularInputAction',
//                'modelName' => 'EnfermedadEstudiante',
//                'viewName' => 'tabular/_tabularInputAsTable',
//            ),
        );
    }

    public function actionSeleccionarMunicipio() {
        if ($_REQUEST['type'] == 'representanteActualizado') {
            $request = '';
        } else if ($_REQUEST['type'] == 'representante') {
            $request = $_REQUEST['Representante'];
        } else if ($_REQUEST['type'] == 'estudiante') {
            $request = $_REQUEST['Estudiante'];
        }

        if ($_REQUEST['switch'] == 1) {
            $item = $request['estado_id'];
        } else if ($_REQUEST['switch'] == 2) {
            $item = $request['estado_nac_id'];
        } else if ($_REQUEST['switch'] == 3) {
            $item = $_REQUEST['estado_id'];
        } else {
            $item = '';
        }

        if ($item == '' || $item == NULL) {
            $lista = array('empty' => '-SELECCIONE-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $lista = Municipio::model()->findAll(array('condition' => 'estado_id =' . $item, 'order' => 'nombre ASC'));
            $lista = CHtml::listData($lista, 'id', 'nombre');

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    public function actionSeleccionarParroquia() {
        if ($_REQUEST['type'] == 'representanteActualizado') {
            $request = '';
        } else if ($_REQUEST['type'] == 'representante') {
            $request = $_REQUEST['Representante'];
        } else {
            $request = $_REQUEST['Estudiante'];
        }
        if ($_REQUEST['switch'] == 1) {
            $item = $request['municipio_id'];
        } else if ($_REQUEST['switch'] == 2) {
            $item = $request['municipio_nac_id'];
        } else if ($_REQUEST['switch'] == 3) {
            $item = $_REQUEST['municipio_id'];
        } else {
            $item = '';
        }

        if ($item == '' || $item == NULL) {
            $lista = array('empty' => '-SELECCIONE-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $lista = Parroquia::model()->findAll(array('condition' => 'municipio_id =' . $item, 'order' => 'nombre ASC'));
            $lista = CHtml::listData($lista, 'id', 'nombre');

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    public function actionSeleccionarPoblacion() {
        if ($_REQUEST['type'] == 'representanteActualizado') {
            $request = '';
        }
        if ($_REQUEST['switch'] == 1) {
            $item = $_REQUEST['parroquia_id'];
        } else if ($_REQUEST['switch'] == 2) {
            $item = $_REQUEST['parroquia_nac_id'];
        } else if ($_REQUEST['switch'] == 3) {
            $item = $_REQUEST['parroquia_id'];
        } else {
            $item = '';
        }

        if ($item == '' || $item == NULL) {
            $lista = array('empty' => '-SELECCIONE-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $lista = Plantel::model()->obtenerPoblacion($item);
            $lista = CHtml::listData($lista, 'id', 'nombre');
            //$data = CJSON::encode(Plantel::model()->obtenerPoblacion($item)); echo "$data";

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    public function actionSeleccionarUrbanizacion() {
        if ($_REQUEST['type'] == 'representanteActualizado') {
            $request = '';
        } else if ($_REQUEST['type'] == 'representante') {
            $request = $_REQUEST['Representante'];
        } else {
            $request = $_REQUEST['Estudiante'];
        }
        if ($_REQUEST['switch'] == 1) {
            $item = $request['parroquia_id'];
        } else if ($_REQUEST['switch'] == 2) {
            $item = $request['parroquia_nac_id'];
        } else if ($_REQUEST['switch'] == 3) {
            $item = $_REQUEST['parroquia_id'];
        } else {
            $item = '';
        }
        //$item=$_REQUEST['parroquia_id'];

        if ($item == '' || $item == NULL) {
            $lista = array('empty' => '-SELECCIONE-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $lista = Plantel::model()->obtenerUrbanizacion($item);
            $lista = CHtml::listData($lista, 'id', 'nombre');
            //$data = CJSON::encode(Plantel::model()->obtenerPoblacion($item)); echo "$data";

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    public function actionSeleccionarTipoVia() {
        if ($_REQUEST['type'] == 'representanteActualizado') {
            $request = '';
        }
        if ($_REQUEST['switch'] == 1) {
            $item = $_REQUEST['Estudiante']['parroquia_id'];
        } else if ($_REQUEST['switch'] == 2) {
            $item = $_REQUEST['Estudiante']['parroquia_nac_id'];
        } else if ($_REQUEST['switch'] == 3) {
            $item = $_REQUEST['parroquia_id'];
        } else {
            $item = '';
        }


        if ($item == '' || $item == NULL) {
            $lista = array('empty' => '-SELECCIONE-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $lista = Plantel::model()->obtenerTipoVia($item);
            $lista = CHtml::listData($lista, 'id', 'nombre');
            //$data = CJSON::encode(Plantel::model()->obtenerPoblacion($item)); echo "$data";

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    public function actionViaAutoComplete() {
        $id = $_GET["id"];

        $_GET['term'] = strtoupper($_GET['term']);
        $res = array();

        if (isset($_GET['term']) && !empty($id)) {
            $res = Plantel::obtenerVia($id, $_GET['term']);
        }

        echo CJSON::encode($res);
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new Estudiante;
        $modelRepresentante = new Representante;
        $modelHistorialMedico = new HistorialMedico;
        $modelDatosAntropometricos = new DatosAntropometricos;
        $modelEnfermedadEstudiante = new EnfermedadEstudiante;
        $modelEnfermedad = new Enfermedad;
        $modelVacunaEstudiante = new VacunaEstudiante;

        if (Yii::app()->getSession()->get('datosRepresentante')) {
            $datosRepresentante = Yii::app()->getSession()->get('datosRepresentante');
            $modelRepresentante->attributes = $datosRepresentante[0];
        }

        /**
         * IDENTIFICO SI ES DIRECTOR Y DE SER ASI QUE ESTE CONSULTANDO UNO DE SUS PLANTELES.
         */
        /* CONSULTO SI EL PLANTEL ES DEL DIRECTOR */
        $plantelPK = '';
        $plantel_id = '';
        $nombrePlantel = '';
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '' || isset($_REQUEST['plantel_id'])) {
            if (isset($_REQUEST['id'])) {
                $plantel_id = $_REQUEST['id'];
            }
            if (isset($_REQUEST['plantel_id'])) {
                $plantel_id = $_REQUEST['plantel_id'];
            }

            $plantel_id = base64_decode($plantel_id);

            if ($plantel_id != '') {
                $plantelPK = Plantel::model()->findByPk($plantel_id);
            } else {
                $plantelPK = '';
            }
            $datosPlantel = Plantel::model()->DatosPlantel($plantel_id);
            if ($datosPlantel != NULL) {
                $nombrePlantel = $datosPlantel[0]['nombre'];
            }
//            else if($datosPlantel == NULL && $plantelPK == NULL){
//                $this->redirect('/estudiante/');
//            }
            else if ($datosPlantel == NULL && $plantelPK != NULL && Yii::app()->user->group != UserGroups::DIRECTOR) {
                $nombrePlantel = $plantelPK['nombre'];
            } else if ($datosPlantel == NULL && $plantelPK != NULL && Yii::app()->user->group == UserGroups::DIRECTOR) {
                $this->redirect('/estudiante/');
            }
        }




        /* DATOS REPRESENTANTE */
        if (isset($_POST['Representante'])) {
            $modelRepresentante->attributes = $_POST['Representante'];
            $datosRepresentante[] = $_POST['Representante'];
            $datosRepresentante[0]['plantel_id'] = base64_decode($_POST['plantel_id']);
            $datosRepresentante[0]['nombres'] = strtoupper($_POST['nombreRepresentante']);
            $datosRepresentante[0]['apellidos'] = strtoupper($_POST['apellidoRepresentante']);
            $datosRepresentante[0]['fecha_nacimiento'] = Utiles::transformDate($_POST['fecha_nacimiento_representante']);
            $datosRepresentante[0]['telefono_habitacion'] = Utiles::onlyNumericString($_POST['Representante']['telefono_habitacion']);
            $datosRepresentante[0]['telefono_movil'] = Utiles::onlyNumericString($_POST['Representante']['telefono_movil']);
            $datosRepresentante[0]['telefono_empresa'] = Utiles::onlyNumericString($_POST['Representante']['telefono_empresa']);

            $documento_identidad_repre = $datosRepresentante[0]['documento_identidad'];
            $tdocumento_identidad_repre = $datosRepresentante[0]['tdocumento_identidad'];
            $representante = Representante::model()->findByAttributes(array('documento_identidad' => $documento_identidad_repre, 'tdocumento_identidad' => $tdocumento_identidad_repre));
            if ($representante) {
                $datosRepresentante[0]['existe'] = true;
                $datosRepresentante[0]['representante_id'] = $representante['id'];
            } else {
                $datosRepresentante[0]['existe'] = false;
            }

            $pais = $model->obtenerDatosPais($datosRepresentante[0]['pais_id']);
            if ($pais[0]['nombre'] == 'Venezuela' || $pais[0]['nombre'] == 'VENEZUELA') {
                $datosRepresentante[0]['estado_nac_id'] = $_POST['Representante']['estado_nac_id'];
            }
            if ($_POST['Representante']['documento_identidad']) {
                $datosRepresentante[0]['documento_identidad'] = trim($_POST['Representante']['documento_identidad']);
            }
            $modeloFormularioRepresentante = $_POST['Representante'];
            $modelRepresentante->tdocumento_identidad = $modeloFormularioRepresentante['tdocumento_identidad'];
            $modelRepresentante->attributes = $datosRepresentante[0];
            $modelRepresentante->nombres = $datosRepresentante[0]['nombres'];
            $modelRepresentante->apellidos = $datosRepresentante[0]['apellidos'];
            $modelRepresentante->sexo = $datosRepresentante[0]['sexo'];
            $modelRepresentante->telefono_habitacion = $datosRepresentante[0]['telefono_habitacion'];
            $modelRepresentante->telefono_movil = $datosRepresentante[0]['telefono_movil'];
            $modelRepresentante->telefono_empresa = $datosRepresentante[0]['telefono_empresa'];
//            $modelRepresentante->setScenario('gestionRepresentante');
            $modelRepresentante->setScenario('gestionRepresentante');

            if (!$modelRepresentante->validate()) {
                $this->renderPartial('//errorSumMsg', array('model' => $modelRepresentante));
                Yii::app()->end();
            } else {
                Yii::app()->getSession()->add('datosRepresentante', $datosRepresentante);
                Yii::app()->end();
            }
        }
        /* DATOS ESTUDIANTE */

        if (isset($_POST['Estudiante'])) {

            /* VALIDO SI EXISTE REGISTRO DE REPRESENTANTE */
            if (!Yii::app()->getSession()->get('datosRepresentante')) {

                echo '<div class="errorDialogBox"><p>Debes actualizar los datos del representante legal para poder crear un estudiante.</p></div>';
                Yii::app()->end();
            }

            $model->attributes = $_POST['Estudiante'];
            $model->setScenario('gestionEstudianteCrear');
            $modeloFormulario = $_POST['Estudiante'];
            $model->idioma_etnia= $modeloFormulario['idioma_etnia'];
            $model->tdocumento_identidad = $modeloFormulario['tdocumento_identidad'];
            if ($_POST['Estudiante']['documento_identidad'] > 0) {
                $model->documento_identidad = $cedulaEstudiante = trim($_POST['Estudiante']['documento_identidad']);
            }

            $model->telefono_movil = Utiles::onlyNumericString($_POST['Estudiante']['telefono_movil']);
            $model->telefono_habitacion = Utiles::onlyNumericString($_POST['Estudiante']['telefono_habitacion']);

            $model->nombres = strtoupper($_POST['Estudiante']['nombres']);
            $model->apellidos = strtoupper($_POST['Estudiante']['apellidos']);

            $datosRepresentante = Yii::app()->getSession()->get('datosRepresentante');
            $model->plantel_actual_id = $datosRepresentante[0]['plantel_id'];

            $model->fecha_nacimiento = Utiles::transformDate($_POST['Estudiante']['fecha_nacimiento']);

            $model->documento_identidad = ($_POST['Estudiante']['documento_identidad']);
            $model->sexo = $_POST['Estudiante']['sexo'];
            $model->usuario_ini_id = Yii::app()->user->id;
            $model->fecha_ini = date("Y-m-d H:i:s");
            $model->estatus = "X";
            $numeroCedula = trim($_POST['Estudiante']['documento_identidad']);

            if($model->pais_id==248 AND ($model->estado_nac_id == null OR $model->estado_nac_id=='')){
                $model->addError('estado_nac_id','El campo Estado de Nacimiento es requerido.');
            }
            if ($model->hasErrors() OR !$model->validate()) {
                $this->renderPartial('//errorSumMsg', array('model' => $model));
                Yii::app()->end();
            } else {
                /* SE REALIZA EL GUARDAR */
                $datosRepresentante = Yii::app()->getSession()->get('datosRepresentante');
                $datosHistorialMedico = Yii::app()->getSession()->get('datosHistorialMedico');
                if ($datosRepresentante[0]['existe'] == true) {
                    /* MODIFICAR LOS DATOS DEL REPRESENTANTE LEGAL */
                    $modelRepresentante = $modelRepresentante->findByPk($datosRepresentante[0]['representante_id']);
                }
                $modelRepresentante->attributes = $datosRepresentante[0];
                $modelRepresentante->fecha_nacimiento = Utiles::transformDate($datosRepresentante[0]['fecha_nacimiento']);
                $modelRepresentante->nombres = $datosRepresentante[0]['nombres'];
                $modelRepresentante->apellidos = $datosRepresentante[0]['apellidos'];
                $modelRepresentante->sexo = $datosRepresentante[0]['sexo'];
                $modelRepresentante->documento_identidad = $datosRepresentante[0]['documento_identidad'];
                $modelRepresentante->tdocumento_identidad = $datosRepresentante[0]['tdocumento_identidad'];
                $modelRepresentante->usuario_ini_id = Yii::app()->user->id;
                $modelRepresentante->fecha_ini = date("Y-m-d H:i:s");
                $modelRepresentante->estatus = "A";

                /* SI EL ESTUDIANTE NO POSEE CEDULA */
                if ($model->documento_identidad == '') {
                    $model->documento_identidad = NULL;
                }

                /* SI DEJA EL INGRESO FAMILIAR VACIO Y PAIS */
                //$model->ingreso_familiar = 100;
//                var_dump($model->ingreso_familiar);die();
                if ($model->ingreso_familiar == '') {
                    $model->ingreso_familiar = 0;
                }

                /* CONSULTO LA CEDULA ESCOLAR EN CASO DE QUE EXISTA */
                $consulCedulaEscolar = (string) $model->cedula_escolar;
//                var_dump($model->cedula_escolar);
//                die();
                $cedula_escolar = $model->findAll(array('condition' => "cedula_escolar = " . " '" . $consulCedulaEscolar . "'"));

                if ($cedula_escolar) {
                    echo '<div class="errorDialogBox"><p>La cédula escolar ya se ecuentra registrada, verifique el orden de nacimiento del estudiante para continuar con el registro.</p></div>';
                    Yii::app()->end();
                }
                /* CONSULTO QUE LA CEDULA DE IDENTIDAD EXISTA */


//                var_dump($_POST['Estudiante']['documento_identidad']);
//                var_dump($numeroCedula);
                if ($numeroCedula) {
                    $busquedaCedula = Estudiante::model()->findByAttributes(array('documento_identidad' => $model->documento_identidad, 'tdocumento_identidad' => $model->tdocumento_identidad)); // valida si existe la cedula en la tabla saime
                    if ($busquedaCedula) {
                        echo '<div class="errorDialogBox"><p>La cédula de identidad de este estudiante ya se ecuentra registrada.</p></div>';
                        Yii::app()->end();
                    }
                }
//&& $model->save()
                if($modelRepresentante->id>0){
                    $model->representante_id=$modelRepresentante->id;
                }
                if ($modelRepresentante->save() && $model->save()) {
                    //$representante_id = $model->obtenerYAsignarRepresentanteAlEstudiante($model->documento_identidad, $model->tdocumento_identidad, $modelRepresentante->documento_identidad, $modelRepresentante->tdocumento_identidad);
                    $DatosAntropometricosSesion = 0;
                    $DatosAntropometricosSesion = Yii::app()->getSession()->get('DatosAntropometricos');
                    if ($DatosAntropometricosSesion != 0) {
                        $modelDatosAntropometricos->attributes = $DatosAntropometricosSesion;
                        $idEstudiante = (int) $model->id;
                        $modelDatosAntropometricos->estudiante_id = $idEstudiante;
                        $modelDatosAntropometricos->usuario_ini_id = Yii::app()->user->id;
                        $modelDatosAntropometricos->estatus = 'A';
                        if ($modelDatosAntropometricos->save()) {

                        } else {
                            Yii::app()->user->setFlash('mensajeError', "Ha ocurrido un error por favor intente nuevamente");
                            $this->renderPartial('//flashMsgv2');
                            Yii::app()->end();
                        }
                    }
                    $datosHistorialEnfermedad = 0;
                    $datosHistorialEnfermedad = Yii::app()->getSession()->get('enfermedadEstudiante');

                    if ($datosHistorialEnfermedad != 0) {
                        $modelDatosEnfermedad = new EnfermedadEstudiante;
                        $modelDatosEnfermedad->attributes = $datosHistorialEnfermedad;
                        $idEstudiante = (int) $model->id;
                        $modelDatosEnfermedad->estudiante_id = $idEstudiante;
                        $modelDatosEnfermedad->usuario_ini_id = Yii::app()->user->id;
                        $modelDatosEnfermedad->estatus = 'A';
                        if ($modelDatosEnfermedad->save()) {

                        } else {
                            Yii::app()->user->setFlash('mensajeError', "Ha ocurrido un error por favor intente nuevamente");
                            $this->renderPartial('//flashMsgv2');
                            Yii::app()->end();
                        }
                    }

                    $datosHistorialVacuna = 0;
                    $datosHistorialVacuna = Yii::app()->getSession()->get('enfermedadVacuna');

                    if ($datosHistorialVacuna != 0) {
                        $modelDatosVacuna = new VacunaEstudiante;
                        $modelDatosVacuna->attributes = $datosHistorialVacuna;
                        $idEstudiante = (int) $model->id;
                        $modelDatosVacuna->estudiante_id = $idEstudiante;
                        $modelDatosVacuna->usuario_ini_id = Yii::app()->user->id;
                        $modelDatosVacuna->estatus = 'A';
                        if ($modelDatosVacuna->save()) {

                        } else {
                            Yii::app()->user->setFlash('mensajeError', "Ha ocurrido un error por favor intente nuevamente");
                            $this->renderPartial('//flashMsgv2');
                            Yii::app()->end();
                        }
                    }

                    Yii::app()->getSession()->remove('enfermedadVacuna');
                    Yii::app()->getSession()->remove('enfermedadEstudiante');
                    Yii::app()->getSession()->remove('datosRepresentante');
                    Yii::app()->getSession()->remove('DatosAntropometricos');
                    $this->registerLog('Escritura', self::MODULO . 'Index', 'EXITOSO', 'Se registro un estudiante con éxito con su representante.');
                }
                Yii::app()->end();
            }
        }
//        /* HISTORIAL MEDICO */
//        if (isset($_POST['HistorialMedico'])) {
//            $datosHistorialMedico = $_POST['HistorialMedico'];
//            $modelHistorialMedico->attributes = $_POST['HistorialMedico'];
//            $modelHistorialMedico->setScenario('gestionHistorialMedico');
//            if (!$modelHistorialMedico->validate()) {
//                $this->renderPartial('//errorSumMsg', array('model' => $modelHistorialMedico));
//                Yii::app()->end();
//            } else {
//                Yii::app()->getSession()->add('datosHistorialMedico', $datosHistorialMedico);
//                Yii::app()->end();
//            }
//        }
        /* DATOS ANTROPOMETRICOS */
        if (isset($_POST['DatosAntropometricos'])) {
//            $estatura = $_POST['estatura'];
            $DatosAntropometricos = $_POST['DatosAntropometricos'];
            $peso = (int) $DatosAntropometricos['peso'];
            $zapato = (int) $DatosAntropometricos['talla_zapato'];
            $pantalon = (int) $DatosAntropometricos['talla_pantalon'];

            if (($peso > 9 and $peso < 150) and $pantalon > 7 and $zapato > 17) {
                if (is_numeric($DatosAntropometricos['talla_camisa']) and $DatosAntropometricos['talla_camisa'] > 1 and $DatosAntropometricos['talla_camisa'] < 17) {

//                    var_dump($DatosAntropometricos['talla_camisa']);
//                    echo "hola 2";
//                    die();
                    $modelDatosAntropometricos->attributes = $_POST['DatosAntropometricos'];

                    Yii::app()->getSession()->add('DatosAntropometricos', $DatosAntropometricos);
                    Yii::app()->end();
//                    $modelDatosAntropometricos->setScenario('gestionDatosAntropometricos');

                } else {
                    //var_dump($DatosAntropometricos);
                    if ($DatosAntropometricos['talla_camisa'] == 'SS' OR $DatosAntropometricos['talla_camisa'] == 'S' OR $DatosAntropometricos['talla_camisa'] == 'M' OR $DatosAntropometricos['talla_camisa'] == 'L' OR $DatosAntropometricos['talla_camisa'] == 'XXXL' OR $DatosAntropometricos['talla_camisa'] == 'XL' OR $DatosAntropometricos['talla_camisa'] == 'XXL') {
//                         
//                        die();
                        $modelDatosAntropometricos->attributes = $_POST['DatosAntropometricos'];

                        Yii::app()->getSession()->add('DatosAntropometricos', $DatosAntropometricos);
                        Yii::app()->end();
                        // $modelDatosAntropometricos->setScenario('gestionDatosAntropometricos');
//                        if (!$modelDatosAntropometricos->validate()) {
//                            $this->renderPartial('//errorSumMsg', array('model' => $modelDatosAntropometricos));
//                            Yii::app()->end();
//                        } else {
//                            Yii::app()->end();
//                        }
                    } else {

                        Yii::app()->user->setFlash('mensajeError', "Dato inválido talla camisa");
                        $this->renderPartial('//flashMsgv2');
                        Yii::app()->end();
                    }
                }
            } elseif ($peso < 9 and $peso > 150) {
                Yii::app()->user->setFlash('mensajeError', "Dato inválido en el campo: Peso. verifique he intente de nuevo.");
                $this->renderPartial('//flashMsgv2');
                Yii::app()->end();
            } elseif ($pantalon < 7) {
                Yii::app()->user->setFlash('mensajeError', "Dato inválido en el campo: Talla pantalón. verifique he intente de nuevo.");
                $this->renderPartial('//flashMsgv2');
                Yii::app()->end();
            } elseif ($zapato > 17) {
                Yii::app()->user->setFlash('mensajeError', "Dato inválido en el campo: Talla zapato. verifique he intente de nuevo.");
                $this->renderPartial('//flashMsgv2');
                Yii::app()->end();
            }
        }
        $estadoCivil = EstadoCivil::model()->findAll();
        $genero = Genero::model()->findAll();
        $estado = Estado::model()->findAll(array('order' => 'nombre ASC'));
        $pais = $model->obtenerPaises();
        $zonaUbicacion = ZonaUbicacion::model()->findAll();
        $condicionVivienda = $model->obtenerCondicionVivienda();
        $tipoVivienda = $model->obtenerTipoVivienda();
        $ubicacionVivienda = $model->obtenerUbicacionVivienda();
        $condicionInfraestructura = CondicionInfraestructura::model()->findAll();
        $etnia = $model->obtenerEtnia();
        $afinidad = Afinidad::model()->findAll(); /* AFINIDAD OTRO REPRESENTANTE LEGAL */
        $afinidadMadre = Afinidad::model()->findAll(array('condition' => "nombre ILIKE 'Madre'")); /* AFINIDAD OTRO REPRESENTANTE MADRE */
        $afinidadPadre = Afinidad::model()->findAll(array('condition' => "nombre ILIKE 'Padre'")); /* AFINIDAD OTRO REPRESENTANTE MADRE */
        $diversidadFuncional = $model->obtenerDiversidadFuncional();
        $tipoSangre = $model->obtenerTipoSangre();
        $profesion = $model->obtenerProfesion();

        $this->render('crear', array(
            'model' => $model,
            'modelRepresentante' => $modelRepresentante,
            'modelHistorialMedico' => $modelHistorialMedico,
            'modelDatosAntropometricos' => $modelDatosAntropometricos,
            'estadoCivil' => $estadoCivil,
            'genero' => $genero,
            'estado' => $estado,
            'pais' => $pais,
            'zonaUbicacion' => $zonaUbicacion,
            'condicionVivienda' => $condicionVivienda,
            'tipoVivienda' => $tipoVivienda,
            'ubicacionVivienda' => $ubicacionVivienda,
            'condicionInfraestructura' => $condicionInfraestructura,
            'etnia' => $etnia,
            'afinidad' => $afinidad,
            'afinidadMadre' => $afinidadMadre,
            'afinidadPadre' => $afinidadPadre,
            'diversidadFuncional' => $diversidadFuncional,
            'tipoSangre' => $tipoSangre,
            'profesion' => $profesion,
            'nombrePlantel' => $nombrePlantel,
            'plantel_id' => $plantel_id,
            'plantelPK' => $plantelPK,
            'modelEnfermedadEstudiante' => $modelEnfermedadEstudiante,
            'modelVacunaEstudiante' => $modelVacunaEstudiante
        ));
    }

    public function actionBuscarCedulaRepresentante() {
        $documento_identidad = $this->getRequest('documentoIdentidad');
        $tdocumento_identidad = $this->getRequest('tdocumentoIdentidad');
        $busquedaCedula = null;
        if ($documento_identidad AND $tdocumento_identidad) {
            if (in_array($tdocumento_identidad, array('V', 'E', 'P'))) {
                $representantes = new Representante;
                $busquedaCedula = AutoridadPlantel::model()->busquedaSaimeMixta($tdocumento_identidad, $documento_identidad); // valida si existe la cedula en la tabla saime
                $representanteTemp = Representante::model()->findByAttributes(array('documento_identidad' => $documento_identidad, 'tdocumento_identidad' => $tdocumento_identidad));
                /* SI EXISTE UN REPRESENTANTE REGISTRADO CON EL NUMERO DE DOCUMENTO DE IDENTIDAD QUE SE INGRESO EN EL FORMULARIO */
                if (isset($representanteTemp->id)) {
                    $exist = true;
                }
                /* SI NO EXISTE EL NUMERO DE DOCUMENTO DE IDENTIDAD REGISTRADO EN LA TABLA REPRESENTANTE */ else {
                    $exist = false;
                }
                if (!$busquedaCedula AND $representanteTemp == array()) {
                    $mensaje = "El Documento de Identidad no se encuentra registrada en nuestro sistema, si cree que esto puede ser un error "
                        . "por favor contacte al personal de soporte mediante "
                        . "<a href='mailto:soporte_gescolar@me.gob.ve'>soporte_gescolar@me.gob.ve</a>";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); // NO EXISTE EN SAIME
                    Yii::app()->end();
                } else {
                    if ($exist == false) {
                        if (!empty($busquedaCedula['fecha_nacimiento'])) {
                            $fecha_nacimiento = date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento']));
                            $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                        } else {
                            $fecha_nacimiento = "";
                            $edad = 0;
                        }
                        echo json_encode(array(
                            'statusCode' => 'successU',
                            'nombre' => strtoupper($busquedaCedula['nombre']),
                            'apellido' => strtoupper($busquedaCedula['apellido']),
                            'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento'])),
                            'edad' => $edad,
                            'exist' => $exist));
                    } else {
                        if (!empty($representanteTemp->fecha_nacimiento)) {
                            $fecha_nacimiento = date("d-m-Y", strtotime($representanteTemp->fecha_nacimiento));
                            $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($representanteTemp->fecha_nacimiento)));
                        } else {
                            $fecha_nacimiento = "";
                            $edad = 0;
                        }
                        echo json_encode(array(
                            'statusCode' => 'successU',
                            'exist' => $exist,
                            /* BLOQUE 1 DATOS DEL REPRESENTANTE */
                            'nombre' => $representanteTemp->nombres,
                            'apellido' => $representanteTemp->apellidos,
                            'fecha_nacimiento' => $fecha_nacimiento,
                            'estado_civil_id' => $representanteTemp->estado_civil_id,
                            'afinidad_id' => $representanteTemp->afinidad_id,
                            'sexo' => $representanteTemp->sexo,
                            'nacionalidad' => $representanteTemp->nacionalidad,
                            'pais_id' => $representanteTemp->pais_id,
                            'estado_nac_id' => $representanteTemp->estado_nac_id,
                            /* BLOQUE 2 DATOS DE UBICACION */
                            'estado_id' => $representanteTemp->estado_id,
                            'municipio_id' => $representanteTemp->municipio_id,
                            'parroquia_id' => $representanteTemp->parroquia_id,
                            'poblacion_id' => $representanteTemp->poblacion_id,
                            'urbanizacion_id' => $representanteTemp->urbanizacion_id,
                            'tipo_via_id' => $representanteTemp->tipo_via_id,
                            'via' => $representanteTemp->via,
                            'direccion_dom' => $representanteTemp->direccion_dom,
                            /* BLOQUE 3 OTROS DATOS */
                            'edad' => $edad,
                            'telefonoMovil' => $representanteTemp->telefono_movil,
                            'telefonoHabitacion' => $representanteTemp->telefono_habitacion,
                            'correo' => $representanteTemp->correo,
                            'empresa' => $representanteTemp->empresa,
                            'telefono_empresa' => $representanteTemp->telefono_empresa,
                            'profesion_id' => $representanteTemp->profesion_id));
                    }
                }
            } else {
                $mensaje = "ERROR!!!!! No ha ingresado los parámetros necesarios para cumplir con la respuesta a su petición. La Cédula debe contener caracteres numéricos.";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); // NO EXISTE EN SAIME
            }
        } else {
            $mensaje = "ERROR!!!!! No ha ingresado los parámetros necesarios para cumplir con la respuesta a su petición. La Cédula debe contener caracteres numéricos.";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); // NO EXISTE EN SAIME
        }
    }

    public function actionBuscarEstudiante() {
        $documento_identidad = $this->getRequest('documentoIdentidad');
        $tdocumento_identidad = $this->getRequest('tdocumentoIdentidad');
        $busquedaCedula = null;
        if ($documento_identidad AND $tdocumento_identidad) {
            if (in_array($tdocumento_identidad, array('V', 'E', 'P'))) {
                $estudiantes = new Estudiante;
                $busquedaCedula = AutoridadPlantel::model()->busquedaSaimeMixta($tdocumento_identidad, $documento_identidad); // valida si existe la cedula en la tabla saime
                if (!$busquedaCedula) {
                    $mensaje = "Esta Cedula de Identidad no se encuentra registrada en nuestro sistema, si cree que esto puede ser un error "
                        . "por favor contacte al personal de soporte mediante "
                        . "<a href='mailto:soporte_gescolar@me.gob.ve'>soporte_gescolar@me.gob.ve</a>";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); // NO EXISTE EN SAIME
                    Yii::app()->end();
                } else { /* SI LA CEDULA EXISTE EN EL SAIME */
                    // echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'fecha' =>  date("d/m/Y", strtotime($busquedaCedula['fecha'])), 'error' => true, 'mensaje'=>$mensaje  ));
                    /* SI EL ESTUDIANTE YA SE ENCUENTRA REGISTRADO EN LA TABLA DE ESTUDIANTE */
                    $validaCedula = Estudiante::model()->findByAttributes(array('documento_identidad' => $documento_identidad, 'tdocumento_identidad' => $tdocumento_identidad));
                    if (isset($validaCedula)) {
                        $mensaje = "<p>El estudiante asociado a este número de cédula ya está registrado.";
                        $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'mensaje', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento'])), 'edad' => $edad, 'mensaje' => $mensaje)); //'error' => true,
                        Yii::app()->end();
                    } else { /* SI EL ESTUDIANTE NO SE ENCUENTRA REGISTRADO EN LA TABLA DE ESTUDIANTE */
                        $nombreEstudianteSaime = $busquedaCedula['nombre'];
                        $apellidoEstudianteSaime = $busquedaCedula['apellido'];
                        $nombreFiltrado = Utiles::onlyAlphaNumericWithSpace($nombreEstudianteSaime);
                        $apellidoFiltrado = Utiles::onlyAlphaNumericWithSpace($apellidoEstudianteSaime);
                        if ($nombreEstudianteSaime != $nombreFiltrado || $apellidoEstudianteSaime != $apellidoFiltrado) {
                            $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                            echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'edad' => $edad, 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento']))));
                        } else {
                            $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                            echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'edad' => $edad, 'bloqueo' => true, 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento']))));
                        }
                    }
                }
            } else {
                $mensaje = "ERROR!!!!! No ha ingresado los parámetros necesarios para cumplir con la respuesta a su petición. La Cédula debe contener solo caracteres numéricos.";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); //
            }
        } else {
            $mensaje = "ERROR!!!!! No ha ingresado los parámetros necesarios para cumplir con la respuesta a su petición. La Cédula debe contener solo caracteres numéricos.";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); //
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Estudiante the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Estudiante::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadModelRepresentante($id) {
        $model = Representante::model()->findAll(array('condition' => 'documento_identidad = ' . $id));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionEnfermedadHistorial() {
        if (Yii::app()->request->isAjaxRequest) {

            $enfermedadEstudiante = $_POST['EnfermedadEstudiante'];
            $index = $this->getPost('index');
            $fechaEnfermedad = $this->getPost('fecha_enfermedad');
            $tenfermedad_id = $this->getPost('tenfermedad_id');
            unset($enfermedadEstudiante[$index]);
            $enfermedadEstudianteOrdenado = array_values($enfermedadEstudiante);

            if (count($enfermedadEstudianteOrdenado) > 1) {

                foreach ($enfermedadEstudianteOrdenado as $index => $valor) {
//                var_dump($valor['fecha_enfermedad']);
//                var_dump($fechaEnfermedad);
//                var_dump($index);
                    //    if ($index != 0) {
//                var_dump(isset($valor['enfermedad_id']));
//                var_dump($valor['fecha_enfermedad']);
//                var_dump($fechaEnfermedad);
//                var_dump($tenfermedad_id);
//                 die();
                    if (isset($valor['enfermedad_id']) AND $valor['enfermedad_id'] == $tenfermedad_id AND isset($valor['fecha_enfermedad']) AND $valor['fecha_enfermedad'] == $fechaEnfermedad ) {

                        Yii::app()->user->setFlash('mensajeError', "Enfermedad repetida");
                        $this->renderPartial('//flashMsgv2');
                        Yii::app()->end();
                        //   }
                    }

                }

            } else {
                Yii::app()->user->setFlash('mensajeError', "Se debe registrar al menos 1 enfermedad y su fecha de contagio.!!");
                $this->renderPartial('//flashMsgv2');
                Yii::app()->end();
            }

            Yii::app()->getSession()->add('enfermedadEstudiante', $enfermedadEstudianteOrdenado);
            Yii::app()->user->setFlash('mensajeExitoso', "Se ha registrado  el datos  del estudiante exitosamente , puede continuar con el registro del estudiante o seguir agregando datos de enfermedad");
            $this->renderPartial('//flashMsg');


//        echo is_array($enfermedadEstudianteOrdenado);
//        die();
//        $enfermedadEstudianteOrdenado = array_unique();

//     Yii::app()->end();
        } else {

            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionvacunaHistorial() {
        if (Yii::app()->request->isAjaxRequest) {

            $vacunaEstudiante = $_POST['VacunaEstudiante'];
            $index = $this->getPost('index');
            $fechavacuna = $this->getPost('fecha_vacuna');
            $tvacuna_id = $this->getPost('tvacuna_id');
            unset($enfermedadEstudiante[$index]);
            $$vacunaEstudianteOrdenado = array_values($vacunaEstudiante);

            if (count($$vacunaEstudianteOrdenado) > 1) {

                foreach ($$vacunaEstudianteOrdenado as $index => $valor) {
//                var_dump($valor['fecha_enfermedad']);
//                var_dump($fechaEnfermedad);
//                var_dump($index);
                    //    if ($index != 0) {
//                var_dump(isset($valor['enfermedad_id']));
//                var_dump($valor['fecha_enfermedad']);
//                var_dump($fechaEnfermedad);
//                var_dump($tenfermedad_id);
//                 die();
                    if (isset($valor['vacuna_id']) AND $valor['vacuna_id'] == $tvacuna_id AND isset($valor['vacuna_enfermedad']) AND $valor['fecha_vacuna'] == $fechavacuna) {

                        Yii::app()->user->setFlash('mensajeError', "vacuna repetida");
                        $this->renderPartial('//flashMsgv2');
                        Yii::app()->end();
                        //   }
                    }
                }
            } else {
                Yii::app()->user->setFlash('mensajeError', "Se debe registrar al menos 1 enfermedad y su fecha de contagio.!!");
                $this->renderPartial('//flashMsgv2');
                Yii::app()->end();
            }

            Yii::app()->getSession()->add('enfermedadVacuna', $$vacunaEstudianteOrdenado);
            Yii::app()->user->setFlash('mensajeExitoso', "Se ha registrado  el datos  del estudiante exitosamente , puede continuar con el registro del estudiante o seguir agregando datos de enfermedad");
            $this->renderPartial('//flashMsg');


//        echo is_array($enfermedadEstudianteOrdenado);
//        die();
//        $enfermedadEstudianteOrdenado = array_unique();
//     Yii::app()->end();
        } else {

            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

//    public function actionGuardarHistorialEnfermedades() {
//        if (Yii::app()->request->isAjaxRequest) {
//
//                $enfermedadEstudiante = $_POST['EnfermedadEstudiante'];
//            Yii::app()->getSession()->add('enfermedadEstudiante', $enfermedadEstudianteOrdenado);
//            Yii::app()->user->setFlash('mensajeExitoso', "Se ha actualizado los datos del estudiante exitosamente , puede continuar con el registro del estudiante");
//            $this->renderPartial('//flashMsg');
//        } else {
//
//            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
//        }
//    }

    /**
     * Performs the AJAX validation.
     * @param Estudiante $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'estudiante-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
