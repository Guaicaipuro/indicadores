<?php

class ModificarController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Modificación de Estudiantes',
        'write' => 'Modificación de Estudiantes',
        'admin' => 'Modificación la Cedula Escolar',
        'label' => 'Modificación de Estudiantes'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        //LECTURA O CONSULTA
        return array(
            array('allow',
                'actions' => array(
                    'seleccionarMunicipio',
                    'seleccionarParroquia',
                    'seleccionarLocalidad',
                    'seleccionarPoblacion',
                    'seleccionarUrbanizacion',
                    'buscarAutoridad',
                    'buscarEstudiante',
                    'index',),
                'pbac' => array('read'),
            ),
            //en esta seccion colocar todos los action del modulo
            array('allow',
                'actions' => array(
                    'datos',
                    'guardarDatosAntropometricos',
                    'datosTitulo',
                    /*Agregado por @author Jonathan Huaman, Fecha 20/04/2015
                                         * Acciones para Registrar,Ver,Editar, Activar e Inactivar Datos del Modelo EstudianteAreaAtencion 
                                         */
                    'guardarEstudianteAreaAtencion',
                    'VerDatosEstudianteAreaAtencion',
                    'MostrarDatosAreaAtencion',
                    'ModificarEstudianteAreaAtencion',
                    'mostrarFormEstudianteAreaAtencion',
                    'InactivarEstudianteAreaAtencion',
                    'ActivarEstudianteAreaAtencion',
                    'estatusAreaAtencion',
                    'actualizarGridAreaAtencion',
                    /*
                                        *FIN de Agregado de Acciones. 
                                        */

                    /*Agregado por @author Jonathan Huaman, Fecha 20/04/2015
                                         * Acciones para Registrar,Ver,Editar, Activar e Inactivar Datos del Modelo EstudianteProgramaApoyo
                                         */
                    'mostrarFormEstudianteProgramaApoyo',
                    'guardarEstudianteProgramaApoyo',
                    'actualizarGridProgramaApoyo',
                    'verDatosEstudianteProgramaApoyo',
                    'mostrarDatosProgramaApoyo',
                    'modificarEstudianteProgramaApoyo',
                    'inactivarEstudianteProgramaApoyo',
                    'activarEstudianteProgramaApoyo',
                    /*
                                        *FIN de Agregado de Acciones. 
                                        */
                ),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    const MODULO = "Estudiante.ModificarController.";



    public function actionBuscarEstudiante() {
        $documento_identidad = $this->getRequest('documentoIdentidad');
        $tdocumento_identidad = $this->getRequest('tdocumentoIdentidad');
        $estudiante_id = $this->getRequest('estudiante_id');
        $busquedaCedula = null;
        if ($documento_identidad AND $tdocumento_identidad) {
            if (in_array($tdocumento_identidad, array('V', 'E', 'P'))) {
                $estudiantes = new Estudiante;
                $busquedaCedula = AutoridadPlantel::model()->busquedaSaimeMixta($tdocumento_identidad, $documento_identidad); // valida si existe la cedula en la tabla saime
                if (!$busquedaCedula) {
                    $mensaje = "Esta Cedula de Identidad no se encuentra registrada en nuestro sistema, si cree que esto puede ser un error "
                        . "por favor contacte al personal de soporte mediante "
                        . "<a href='mailto:soporte_gescolar@me.gob.ve'>soporte_gescolar@me.gob.ve</a>";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); // NO EXISTE EN SAIME
                    Yii::app()->end();
                } else { /* SI LA CEDULA EXISTE EN EL SAIME */
                    // echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'fecha' =>  date("d/m/Y", strtotime($busquedaCedula['fecha'])), 'error' => true, 'mensaje'=>$mensaje  ));
                    /* SI EL ESTUDIANTE YA SE ENCUENTRA REGISTRADO EN LA TABLA DE ESTUDIANTE */
                    $validaCedula = Estudiante::model()->findByAttributes(array('documento_identidad' => $documento_identidad, 'tdocumento_identidad' => $tdocumento_identidad));
                    if (isset($validaCedula)) {
                        $estudiante_id_confirmacion = (is_object($validaCedula) AND isset($validaCedula->id)) ? $validaCedula->id : null;
                        if (base64_decode($estudiante_id) == $estudiante_id_confirmacion) {
                            $nombreEstudianteSaime = $busquedaCedula['nombre'];
                            $apellidoEstudianteSaime = $busquedaCedula['apellido'];
                            $nombreFiltrado = Utiles::onlyAlphaNumericWithSpace($nombreEstudianteSaime);
                            $apellidoFiltrado = Utiles::onlyAlphaNumericWithSpace($apellidoEstudianteSaime);
                            if ($nombreEstudianteSaime != $nombreFiltrado || $apellidoEstudianteSaime != $apellidoFiltrado) {
                                $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                                echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'edad' => $edad, 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento']))));
                            } else {
                                $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                                echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'edad' => $edad, 'bloqueo' => true, 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento']))));
                            }
                        } else {
                            $mensaje = "<p>El estudiante asociado a este número de cédula ya está registrado.";
                            $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                            echo json_encode(array('statusCode' => 'mensaje', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento'])), 'edad' => $edad, 'mensaje' => $mensaje)); //'error' => true,
                            Yii::app()->end();
                        }
                    } else { /* SI EL ESTUDIANTE NO SE ENCUENTRA REGISTRADO EN LA TABLA DE ESTUDIANTE */
                        $nombreEstudianteSaime = $busquedaCedula['nombre'];
                        $apellidoEstudianteSaime = $busquedaCedula['apellido'];
                        $nombreFiltrado = Utiles::onlyAlphaNumericWithSpace($nombreEstudianteSaime);
                        $apellidoFiltrado = Utiles::onlyAlphaNumericWithSpace($apellidoEstudianteSaime);
                        if ($nombreEstudianteSaime != $nombreFiltrado || $apellidoEstudianteSaime != $apellidoFiltrado) {
                            $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                            echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'edad' => $edad, 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento']))));
                        } else {
                            $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                            echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'edad' => $edad, 'bloqueo' => true, 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento']))));
                        }
                    }
                }
            } else {
                $mensaje = "ERROR!!!!! No ha ingresado los parámetros necesarios para cumplir con la respuesta a su petición. La Cédula debe contener solo caracteres numéricos.";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); //
            }
        } else {
            $mensaje = "ERROR!!!!! No ha ingresado los parámetros necesarios para cumplir con la respuesta a su petición. La Cédula debe contener solo caracteres numéricos.";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); //
        }
    }

    public function actionIndex($id, $bc=null) {
        $sinacoes = false;
        $firmantes = array();
        /**
         * IDENTIFICO SI ES DIRECTOR Y DE SER ASI QUE ESTE CONSULTANDO UNO DE SUS PLANTELES.
         */
        /* CONSULTO SI EL PLANTEL ES DEL DIRECTOR */
        $plantelPK = '';
        $plantel_id = '';
        $nombrePlantel = '';
        $estudiante_id = $id;

        $dataConsultaTitulo = array();
        $resultadoBusqueda = array();
        $historialEstudiante = array();
        $dataConsultarNota = array();

        if(strlen($estudiante_id)>0 && is_numeric($this->getIdDecoded($estudiante_id))) {
            $plantel_id = Estudiante::model()->findAll(array('condition' => 'id = :id', 'params'=>array(':id'=>$this->getIdDecoded($estudiante_id))));

            $plantel_anterior_id = $plantel_id[0]['plantel_anterior_id'];
            $plantel_id = $plantel_id[0]['plantel_actual_id'];

            $plantelPK = Plantel::model()->findByPk($plantel_id);
            $plantelAnteriorPK = Plantel::model()->findByPk($plantel_anterior_id);
        }
        /* EMPIEZA EL MODULO DE MODIFICACION ESTUDIANTE */
        if(strlen($estudiante_id)>0 && is_numeric($this->getIdDecoded($estudiante_id))) {
            $estudiante_id = $this->getIdDecoded($id);
            if (is_numeric($estudiante_id)) {
                $model = $this->loadModel($estudiante_id);

                /*CONSULTAR TITULO DEL ESTUDIANTE - MODULO DESARROLLADO POR ENRIQUEX00*/
                $nacionalidad = $model->tdocumento_identidad;
                $documentoIdentidadEStudiante = $model->documento_identidad;

                $resultadoBusqueda = Titulo::model()->buscarEstTitulo($nacionalidad, $documentoIdentidadEStudiante);

                if ($resultadoBusqueda) {
                    $estudianteID = $resultadoBusqueda[0]['estudiante_id'];
                    if(array_key_exists('mes_egreso',$resultadoBusqueda[0]) AND array_key_exists('anio_egreso',$resultadoBusqueda[0])){
                        $sinacoes = true;
                        $mes_egreso = $resultadoBusqueda[0]['mes_egreso'];
                        $anio_egreso = $resultadoBusqueda[0]['anio_egreso'];
                        $cod_plantel = $resultadoBusqueda[0]['cod_plantel'];
                        $periodo =  substr($anio_egreso,2,2);
                        $firmantes = Titulo::model()->buscarFirmantesTituloSinacoes($cod_plantel,$periodo,$anio_egreso,$mes_egreso);
                        if (!$firmantes) {
                            $firmantes = array();
                        }
                    }
                    if ($estudianteID) {
                        $documentoIdentidadEStudiante = (int) $documentoIdentidadEStudiante;
                        $compara = false;
                        for ($i = 1; $i < 5; $i++) {
                            $coincidir = $i;
                            if ($i == 4) {
                                $coincidir = 'P';
                            }

                            $buscarNotas = Titulo::model()->buscarNotaEstudiante($documentoIdentidadEStudiante, $coincidir);


                            if ($buscarNotas) {

                                $dataConsultarNota[] = $this->dataProviderConsultaNota($buscarNotas);
                                $compara = TRUE;
                            }



                        }
//                        var_dump($dataConsultarNota[3]);
//                        die();
                        if ($compara == false) {
                            $dataConsultarNota = array();
                        }
//                        var_dump($dataConsultarNota);
//                        die();

                        $dataConsultaTitulo = $this->dataProviderConsultaTitulo($resultadoBusqueda);


//
//                        var_dump($dataConsultarNota);
                        //  var_dump($dataConsultaTitulo);
                        //     die();
                        //$this->renderPartial('mostrarResultadoBusqueda', array(
                        //'plantelPK' => $plantelPK
                        //), FALSE, TRUE);
//                $respuesta['statusCode'] = 'alert';
//                echo json_encode($respuesta);
                    }
                }

                if (!$model->tdocumento_identidad) {
                    $model->tdocumento_identidad = '';

                    //$model->setScenario('gestionEstudiante');
//                    if (strlen($model->documento_identidad) > 2) {
//                        $model->documento_identidad = 'V-' . $model->documento_identidad;
//                    }
                    if ($model) {
                        /* BUSQUEDA POR INSCRIPCION_ESTUDIANTE Y POR TALUMNOS_ACAD */
//                    var_dump($model->id);die();
                        if(is_numeric($model->documento_identidad)){
                            $identificacion = $model->documento_identidad;

                        }
                        else {
                            if(is_numeric($model->cedula_escolar)){
                                $identificacion = $model->cedula_escolar;
                            }
                            else {
                                $identificacion=null;
                            }
                        }
                        $historicoEstudiante = Estudiante::model()->historicoEstudiante($model->documento_identidad,$model->cedula_escolar, $model->id);
                        if ($model->representante_id) {
                            $modelRepresentante = $this->loadModelRepresentante($model->representante_id);
//                            $modelRepresentante->documento_identidad = 'V-' . $modelRepresentante->documento_identidad;
                        } else {
                            $modelRepresentante = new Representante;
                        }
                    }
                } else {

                    //$model->setScenario('gestionEstudiante');
//                if (strlen($model->documento_identidad) > 2) {
//                    $model->documento_identidad = 'V-' . $model->documento_identidad;
//                }
                    if ($model) {
                        /* BUSQUEDA POR INSCRIPCION_ESTUDIANTE Y POR TALUMNOS_ACAD */

                        if(is_numeric($model->documento_identidad)){
                            $identificacion = $model->documento_identidad;

                        }
                        else {
                            if(is_numeric($model->cedula_escolar)){
                                $identificacion = $model->cedula_escolar;
                            }
                            else {
                                $identificacion=null;
                            }
                        }
                        $historicoEstudiante = Estudiante::model()->historicoEstudiante($model->documento_identidad,$model->cedula_escolar, $model->id);
                        if ($model->representante_id) {
                            $modelRepresentante = $this->loadModelRepresentante($model->representante_id);
//                        $modelRepresentante->documento_identidad = 'V-' . $modelRepresentante->documento_identidad;
                        } else {
                            $modelRepresentante = new Representante;
                        }
                    }
                }

                $modelHistorialMedico = new HistorialMedico;
                $modelDatosAntropometricos = new DatosAntropometricos;
//                var_dump($modelDatosAntropometricos);
//                die();
//                var_dump('hola');

                $modelDatosAntropometricos = $this->loadModelDatosAntropometrico($model->id);
                $estadoCivil = EstadoCivil::model()->findAll();
                $genero = Genero::model()->findAll();
                $estado = Estado::model()->findAll(array('order' => 'nombre ASC'));
                $pais = $model->obtenerPaises();
                $zonaUbicacion = ZonaUbicacion::model()->findAll();
                $condicionVivienda = $model->obtenerCondicionVivienda();
                $tipoVivienda = $model->obtenerTipoVivienda();
                $ubicacionVivienda = $model->obtenerUbicacionVivienda();
                $condicionInfraestructura = CondicionInfraestructura::model()->findAll();
                $etnia = $model->obtenerEtnia();
                $afinidad = Afinidad::model()->findAll(); /* AFINIDAD OTRO REPRESENTANTE LEGAL */
                $afinidadMadre = Afinidad::model()->findAll(array('condition' => "nombre ILIKE 'Madre'")); /* AFINIDAD OTRO REPRESENTANTE MADRE */
                $afinidadPadre = Afinidad::model()->findAll(array('condition' => "nombre ILIKE 'Padre'")); /* AFINIDAD OTRO REPRESENTANTE MADRE */
                $diversidadFuncional = $model->obtenerDiversidadFuncional();
                $tipoSangre = $model->obtenerTipoSangre();
                $profesion = $model->obtenerProfesion();

                /**
                 *   Agregado por Jonathan Huaman Fecha 20-04-2015.
                 */
                $modelEstudianteAreaAtencion= new EstudianteAreaAtencion('search');
                $modelEstudianteAreaAtencion->estudiante_id= $estudiante_id;
                $areaAtencion= EstudianteAreaAtencion::model()->ObtenerDatosEstudianteAreaAtencion($estudiante_id);
                $modelEstudianteProgramaApoyo= new EstudianteProgramaApoyo('search');
                $modelEstudianteProgramaApoyo->estudiante_id=$estudiante_id;
                $programaApoyo=  EstudianteProgramaApoyo::model()->ObtenerDatosEstudianteProgramaApoyo($estudiante_id);

                $this->render('modificar', array(
                    'model' => $model,
                    'modelRepresentante' => $modelRepresentante,
                    'modelHistorialMedico' => $modelHistorialMedico,
                    'modelDatosAntropometricos' => $modelDatosAntropometricos,
                    'estadoCivil' => $estadoCivil,
                    'genero' => $genero,
                    'estado' => $estado,
                    'pais' => $pais,
                    'zonaUbicacion' => $zonaUbicacion,
                    'condicionVivienda' => $condicionVivienda,
                    'tipoVivienda' => $tipoVivienda,
                    'ubicacionVivienda' => $ubicacionVivienda,
                    'condicionInfraestructura' => $condicionInfraestructura,
                    'etnia' => $etnia,
                    'afinidad' => $afinidad,
                    'afinidadMadre' => $afinidadMadre,
                    'afinidadPadre' => $afinidadPadre,
                    'diversidadFuncional' => $diversidadFuncional,
                    'tipoSangre' => $tipoSangre,
                    'profesion' => $profesion,
                    'plantel_id' => $plantel_id,
                    'plantelPK' => $plantelPK,
                    'plantelAnteriorPK' => $plantelAnteriorPK,
                    'historicoEstudiante' => $historicoEstudiante,
                    'estudiante_id'=>$id,
                    'modelEstudianteAreaAtencion'=> $modelEstudianteAreaAtencion,
                    'areaAtencion'=>$areaAtencion,
                    'modelEstudianteProgramaApoyo'=>$modelEstudianteProgramaApoyo,
                    'programaApoyo'=>$programaApoyo,
                    /*ESTO UN MODULO ACTUALIZADO POR ENRIQUE*/
                    'dataProvider' => $dataConsultaTitulo,
                    'resultadoBusqueda' => $resultadoBusqueda,
                    'dataConsultarNota' => $dataConsultarNota,
                    'firmantes'=>$firmantes,
                    'sinacoes'=>$sinacoes
                    /*FIN DEL MODULO ACTUALIZADO*/
                ));
            } else {
                throw new CHttpException(404, 'No se ha especificado el Estudiante que desea modificar. Vuelva a la página anterior e intentelo de nuevo.'); // esta vacio el request
            }
        }
    }

    function dataProviderConsultaTitulo($resultadoBusqueda) {
        foreach ($resultadoBusqueda as $key => $value) {
            $cedula_identidad = $value['documento_identidad'];
            $nombres = $value['nombres'];
            $apellidos = $value['apellidos'];
            $plantel = $value['nombreplantel'];
            $plan_nombre = $value['nombre_plan'];
            $nombre_mencion = $value['nombre_mencion'];
            $cod_estadistico = $value['cod_estadistico'];
            $serial = $value['serial'];
            // $anoEgreso = $value['ano_egreso'];
            (isset($value['anio_egreso']) && $value['anio_egreso'] != null && $value['anio_egreso'] != '')? $anoEgreso = $value['anio_egreso']:$anoEgreso = 'NO POSEE';
            // (array_key_exists('ano_egreso',$value) || isset($value['ano_egreso']))?$anoEgreso = $value['ano_egreso']:$anoEgreso = null; //Comente esto *Marisela*

            $fechaOtorgacion = $value['fecha_otorgamiento'];
            if($fechaOtorgacion!=null){
                $fechaOtorgacion = date("d-m-Y", strtotime($fechaOtorgacion));
            }else{
                $fechaOtorgacion='NO POSEE';
            }

            $plantelCode_estadi = $plantel . ' ' . $cod_estadistico;
            $nombreApellido = $nombres . ' ' . $apellidos;
//            $botones = "<div class='center'>" . CHtml::checkBox('EstSolicitud[]', "false", array('value' => base64_encode($value['id']),
////                   'onClick' => "Estudiante('')",
//                        "title" => "solicitud")
//                    ) .
//                    "</div>";
//            $columna .= CHtml::checkBox('repitiente[]', false, array('id' => 'repitiente[]', 'title' => 'Repitente')) . '&nbsp;&nbsp;&nbsp;';
            $rawData[] = array(
                'id' => $key,
                'cedula_identidad' => '<center>' . $cedula_identidad . '</center>',
                'nombreApellido' => '<center>' . $nombreApellido . '</center>',
                'plan_nombre' => '<center>' . $plan_nombre . '</center>',
                'nombre_mencion' => "<center>" . $nombre_mencion . '</center>',
                'serial' => "<center>" . $serial . "</center>",
                'plantelCode_estadi' => "<center>" . $plantelCode_estadi . '</center>',
                'anoEgreso' => "<center>" . $anoEgreso . '</center>',
                'fechaOtorgacion' => "<center>" . $fechaOtorgacion . '</center>'
                //  'botones' => '<center>' . $botones . '</center>'
            );
        }
        // var_dump($rawData);
        //  die();
        return new CArrayDataProvider($rawData, array(
                'pagination' => false,
                //    'pagination' => array(
                //      'pageSize' => 5,
                //),
            )
        );
    }

    function dataProviderConsultaNota($buscarNotas) {
//        var_dump($buscarNotas);
//        die();
        foreach ($buscarNotas as $key => $value) {
//            var_dump($buscarNotas['materia']);
//            die();
            $materia = $value['materia'];
            $mencion = $value['mencion'];
            $nota = $value['nota'];
//            $botones = "<div class='center'>" . CHtml::checkBox('EstSolicitud[]', "false", array('value' => base64_encode($value['id']),
////                   'onClick' => "Estudiante('')",
//                        "title" => "solicitud")
//                    ) .
//                    "</div>";
//            $columna .= CHtml::checkBox('repitiente[]', false, array('id' => 'repitiente[]', 'title' => 'Repitente')) . '&nbsp;&nbsp;&nbsp;';
            $rawData[] = array(
                'id' => $key,
                'materia' => '<center>' . $materia . '</center>',
                'mencion' => '<center>' . $mencion . '</center>',
                'nota' => '<center>' . $nota . '</center>',
                //  'botones' => '<center>' . $botones . '</center>'
            );
        }

        //  die();
//        var_dump($rawData);
//        die();
        return new CArrayDataProvider($rawData, array(
                'pagination' => false,
                //    'pagination' => array(
                //      'pageSize' => 5,
                //),
            )
        );
    }

    public function actionDatos() {
        /* DATOS REPRESENTANTE */
        if (isset($_POST['Representante'])) {
            $id = $_REQUEST['estudiante_id'];
            $estudiante_id = base64_decode($id);
            if (is_numeric($estudiante_id)) {
                $model = $this->loadModel($estudiante_id);
                if ($model) {
                    if ($model->representante_id) {
                        /* BUSCA EL REPRESENTANTE QUE ESTA ASOCIADO CON EL ESTUDIANTE ACTUAL */
                        $estudiante = Estudiante::model()->findAll(array('condition' => 'id = ' . $estudiante_id));
                        $representante_id_actual = $estudiante[0]['representante_id'];
                        $documento_identidad_representante = $_POST['Representante']['documento_identidad'];
                        $tdocumento_identidad_representante = $_POST['Representante']['tdocumento_identidad'];
                        $representante = Representante::model()->findByAttributes(array('documento_identidad' => $documento_identidad_representante, 'tdocumento_identidad' => $tdocumento_identidad_representante));
                        if ($representante) {
                            if(isset($representante[0])){
                                $representante_id_request = $representante[0]['id'];
                            }
                            if(isset($representante['id'])){
                                $representante_id_request = $representante['id'];
                            }
                        } else {
                            $representante_id_request = NULL;
                        }
                        if ($representante_id_actual == $representante_id_request) {
                            $modelRepresentante = $this->loadModelRepresentante($representante_id_actual);
                            //$modelRepresentante->documento_identidad = 'V-' . $modelRepresentante->documento_identidad;
                        } else {
                            if ($representante_id_request != NULL) {
                                $modelRepresentante = $this->loadModelRepresentante($representante_id_request);
                                //$modelRepresentante->documento_identidad = 'V-' . $modelRepresentante->documento_identidad;
                            } else {
                                $modelRepresentante = new Representante;
                            }
                        }
                    } else {
                        $modelRepresentante = new Representante;
                    }
//                    var_dump($modelRepresentante);die();
                }
            }
            $modelRepresentante->attributes = $_POST['Representante'];
            $datosRepresentante[] = $_POST['Representante'];
            $datosRepresentante[0]['nombres'] = strtoupper($_POST['nombreRepresentante']);
            $datosRepresentante[0]['apellidos'] = strtoupper($_POST['apellidoRepresentante']);
            $datosRepresentante[0]['fecha_nacimiento'] = Utiles::transformDate($_POST['fecha_nacimiento_representante']);
            $datosRepresentante[0]['telefono_habitacion'] = Utiles::onlyNumericString($_POST['Representante']['telefono_habitacion']);
            $datosRepresentante[0]['telefono_movil'] = Utiles::onlyNumericString($_POST['Representante']['telefono_movil']);
            $datosRepresentante[0]['telefono_empresa'] = Utiles::onlyNumericString($_POST['Representante']['telefono_empresa']);

            $modelRepresentante->documento_identidad = $documento_identidad_representante = $datosRepresentante[0]['documento_identidad'];
            $modelRepresentante->tdocumento_identidad = $tdocumento_identidad_representante = $datosRepresentante[0]['tdocumento_identidad'];
            //$numeroCedula = $datosRepresentante[0]['documento_identidad'];
            $representante = Representante::model()->findByAttributes(array('documento_identidad' => $documento_identidad_representante, 'tdocumento_identidad' => $tdocumento_identidad_representante));
            if ($representante) {
                $datosRepresentante[0]['existe'] = true;
                $datosRepresentante[0]['representante_id'] = $representante['id'];
            } else {
                $datosRepresentante[0]['existe'] = false;
            }
            $pais = $model->obtenerDatosPais($datosRepresentante[0]['pais_id']);
            if ($pais[0]['nombre'] == 'Venezuela' || $pais[0]['nombre'] == 'VENEZUELA') {
                $datosRepresentante[0]['estado_nac_id'] = $_POST['Representante']['estado_nac_id'];
            }
//            if ($_POST['Representante']['documento_identidad']) {
//                $datosRepresentante[0]['documento_identidad'] = substr($_POST['Representante']['documento_identidad'], 2);
//            }
            $modelRepresentante->attributes = $datosRepresentante[0];
            $modelRepresentante->nombres = $datosRepresentante[0]['nombres'];
            $modelRepresentante->apellidos = $datosRepresentante[0]['apellidos'];
            $modelRepresentante->sexo = $datosRepresentante[0]['sexo'];
            $modelRepresentante->telefono_habitacion = $datosRepresentante[0]['telefono_habitacion'];
            $modelRepresentante->telefono_movil = $datosRepresentante[0]['telefono_movil'];
            $modelRepresentante->telefono_empresa = $datosRepresentante[0]['telefono_empresa'];
            $modelRepresentante->setScenario('gestionRepresentante');

            if (!$modelRepresentante->validate()) {
                $this->renderPartial('//errorSumMsg', array('model' => $modelRepresentante));
                Yii::app()->end();
            } else {

                if ($modelRepresentante->save()) {
                    $estudiante_id = base64_decode($_REQUEST['estudiante_id']);
                    if (is_numeric($estudiante_id)) {
                        //$modelEstudiante = Estudiante::model()->findAll(array('condition' => 'id = ' . $estudiante_id));
                        //$representante_id = $modelRepresentante->findAll(array('condition' => "documento_identidad = '" . $documento_identidad_representante . "' AND tdocumento_identidad = '" . $tdocumento_identidad_representante . "'"));
                        $model->representante_id = $modelRepresentante->id;
                        if($model->save()){
                            $this->registerLog('LECTURA', self::MODULO . 'Index', 'EXITOSO', 'Se modifico un representante con éxito.');
                        }
                        else{
                            $this->registerLog('LECTURA', self::MODULO . 'Index', 'ERROR', 'No se modifico un representante con éxito.');
                        }
                    }
                    Yii::app()->end();
                }
            }
        }
        /* DATOS ESTUDIANTE */

        if (isset($_POST['Estudiante'])) {
            $id = $_REQUEST['estudiante_id'];
            $estudiante_id = base64_decode($id);
            if (is_numeric($estudiante_id)) {
                $model = $this->loadModel($estudiante_id);
                /*if (strlen($model->documento_identidad) > 2) {
                    $model->documento_identidad = 'V-' . $model->documento_identidad;
                 }*/
                if ($model) {
                    if ($model->representante_id) {
                        $modelRepresentante = $this->loadModelRepresentante($model->representante_id);
                        //$modelRepresentante->documento_identidad = 'V-' . $modelRepresentante->documento_identidad;
                    } else {
                        $modelRepresentante = new Representante;
                    }
                }
            }
            $modeloFormulario = $_POST['Estudiante'];
            $model->attributes = $_POST['Estudiante'];
            if(in_array(Yii::app()->user->group,array(UserGroups::JEFE_DRCEE, UserGroups::ADMIN_REG_CONTROL))){
                $model->setScenario('moddrcee');
            }else {
                $model->setScenario('gestionEstudiante');
            }
            $model->idioma_etnia= $modeloFormulario['idioma_etnia'];
            $model->tdocumento_identidad = $modeloFormulario['tdocumento_identidad'];
            $model->documento_identidad = $_POST['cedula_hidden'];
            $model->nombres = strtoupper($_POST['nombres_hidden']);
            $model->apellidos = strtoupper($_POST['apellidos_hidden']);
            if ($model->ingreso_familiar == null) {
                $model->ingreso_familiar = 0;
            }

            if (Yii::app()->user->pbac('estudiante.modificar.admin')) {
                if ($_POST['Estudiante']['documento_identidad'] > 0) {
                    $model->documento_identidad = $cedulaEstudiante = $_POST['Estudiante']['documento_identidad'];
                    $model->cedula_escolar = $cedulaEstudiante = trim($_POST['Estudiante']['cedula_escolar']);
                }

                $model->nombres = strtoupper($_POST['Estudiante']['nombres']);
                $model->apellidos = strtoupper($_POST['Estudiante']['apellidos']);
            }

            $model->telefono_movil = Utiles::onlyNumericString($_POST['Estudiante']['telefono_movil']);
            $model->telefono_habitacion = Utiles::onlyNumericString($_POST['Estudiante']['telefono_habitacion']);
            $model->fecha_nacimiento = Utiles::transformDate($_POST['Estudiante']['fecha_nacimiento']);

            $model->documento_identidad = $_POST['Estudiante']['documento_identidad'];
            $model->sexo = $_POST['Estudiante']['sexo'];
            $model->usuario_ini_id = Yii::app()->user->id;
            $model->fecha_ini = date("Y-m-d H:i:s");
            $model->estatus = "A";

            if($model->pais_id==248 AND ($model->estado_nac_id == null OR $model->estado_nac_id=='')){
                $model->addError('estado_nac_id','El campo Estado de Nacimiento es requerido.');
            }

            if ($model->hasErrors() OR !$model->validate() ) {
                $this->renderPartial('//errorSumMsg', array('model' => $model));
                Yii::app()->end();
            } else {
                /* SE REALIZA EL GUARDAR */
                $datosRepresentante = Yii::app()->getSession()->get('datosRepresentante');
                $datosHistorialMedico = Yii::app()->getSession()->get('datosHistorialMedico');
                if ($datosRepresentante[0]['existe'] == true) {
                    /* MODIFICAR LOS DATOS DEL REPRESENTANTE LEGAL */
                    $modelRepresentante = $modelRepresentante->findByPk($datosRepresentante[0]['representante_id']);
                    $modelRepresentante->attributes = $datosRepresentante[0];
                    $modelRepresentante->fecha_nacimiento = Utiles::transformDate($datosRepresentante[0]['fecha_nacimiento']);
                    $modelRepresentante->nombres = $datosRepresentante[0]['nombres'];
                    $modelRepresentante->apellidos = $datosRepresentante[0]['apellidos'];
                    $modelRepresentante->sexo = $datosRepresentante[0]['sexo'];
                    $modelRepresentante->usuario_ini_id = Yii::app()->user->id;
                    $modelRepresentante->fecha_ini = date("Y-m-d H:i:s");
                    $modelRepresentante->estatus = "A";
                }

                /* SI EL ESTUDIANTE NO POSEE CEDULA */
                if ($model->documento_identidad == '') {
                    $model->documento_identidad = NULL;
                }

                //var_dump($model->documento_identidad);
                //var_dump($model->cedula_escolar);
                /*Valido si el estudiante posee o no documento_identidad o cedula_escolar, de no poseer alguna de las dos guardar un null*/
                if($model->cedula_escolar == ''){
                    $model->cedula_escolar = NULL;
                }

                //var_dump($model->documento_identidad);
                //var_dump($model->cedula_escolar);

                if ($model->save()) {
                    if ($model->representante_id == '') {
                        $representante_id = 'NULL';
                    } else {
                        $representante_id = $model->representante_id;
                    }
                    $documento_identidad_representante = Representante::model()->findAll(array('condition' => 'id = ' . $representante_id));
                    $is_empty = empty($documento_identidad_representante);
                    if ($is_empty) {
                        $cedula_representante = 'NULL';
                        $tdocumento_identidad = 'NULL';
                    } else {
                        $cedula_representante = $documento_identidad_representante[0]['documento_identidad'];
                        $tdocumento_identidad = $documento_identidad_representante[0]['tdocumento_identidad'];
                    }
                    $representante_id = $model->obtenerYAsignarRepresentanteAlEstudiante($model->documento_identidad, $model->tdocumento_identidad, $cedula_representante, $tdocumento_identidad);



                    $this->registerLog('ESCRITURA', self::MODULO . 'DATOS', 'EXITOSO', 'Se MODIFICO un estudiante con éxito.');
                }
                Yii::app()->end();
            }
        }

    }

    public function actionGuardarDatosAntropometricos(){
        /* DATOS ANTROPOMETRICOS */
        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['DatosAntropometricos']) AND isset($_POST['estudiante_id_mod'])) {
                $estudiante_id = base64_decode($_POST['estudiante_id_mod']);

                $datosAntropometricos = $this->loadModelDatosAntropometrico($estudiante_id);

                if($datosAntropometricos==null){
                    $datosAntropometricos = new DatosAntropometricos();
                    $datosAntropometricos->attributes = $_POST['DatosAntropometricos'];
                }
                else {
                    $postDatosAntropometricos = $this->getPost('DatosAntropometricos');
                    (isset($postDatosAntropometricos['peso']))?$peso =$postDatosAntropometricos['peso'] :$peso = '';
                    (isset($postDatosAntropometricos['talla_camisa']))?$talla_camisa =$postDatosAntropometricos['talla_camisa'] :$talla_camisa = '';
                    (isset($postDatosAntropometricos['talla_zapato']))?$talla_zapato =$postDatosAntropometricos['talla_zapato'] :$talla_zapato = '';
                    (isset($postDatosAntropometricos['talla_pantalon']))?$talla_pantalon =$postDatosAntropometricos['talla_pantalon'] :$talla_pantalon = '';
                    (isset($postDatosAntropometricos['estatura']))?$estatura =$postDatosAntropometricos['estatura'] :$estatura= '';

                    $datosAntropometricos->peso=$peso;
                    $datosAntropometricos->talla_camisa=$talla_camisa;
                    $datosAntropometricos->talla_zapato=$talla_zapato;
                    $datosAntropometricos->talla_pantalon=$talla_pantalon;
                    $datosAntropometricos->estatura=$estatura;
                    $datosAntropometricos->estudiante_id = $estudiante_id;
                }
                $datosAntropometricos->usuario_ini_id = Yii::app()->user->id;
                $datosAntropometricos->estatus = 'A';


                if($datosAntropometricos->validate()){
                    if(!$datosAntropometricos->save()){
                        $mensaje = 'Estimado usuario, ha ocurrido un error durante el proceso de registro, intente nuevamente. Si el error persiste porfavor comuniquese mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>';
                        Yii::app()->user->setFlash('mensajeError', "$mensaje");
                        $this->renderPartial('//flashMsgv2');
                        Yii::app()->end();
                    }
                }
                else {
                    $this->renderPartial('//errorSumMsg', array('model' => $datosAntropometricos));
                    Yii::app()->end();
                }
            } else
                throw new CHttpException(404, 'No se ha encontrado el recurso que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        } else
            throw new CHttpException(404, 'Estimado Usuario, usted no esta autorizado para acceder mediante esta via.');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Estudiante the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Estudiante::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadModelRepresentante($id) {
        $model = Representante::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadModelDatosAntropometrico($idEstudiante) {
//        Representante::model()->findByAttributes(array('documento_identidad' => $documento_identidad_repre, 'tdocumento_identidad' => $tdocumento_identidad_repre));
        $model = DatosAntropometricos::model()->findByAttributes(array('estudiante_id' => $idEstudiante));
        if ($model === null)
            $model = new DatosAntropometricos ();
        return $model;
    }

    public function loadModelEstudianteAreaAtencion($id)
    {
        $model=  EstudianteAreaAtencion::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     *
     * @param base64 $id Id del Estudiante
     * @throws CHttpException
     */
    public function actionDatosTitulo($id){
        $idDecoded = $this->getIdDecoded($id);
        if($this->hasPost('EstudianteVerif') && Yii::app()->request->isAjaxRequest){
            $datosEstudiante = $this->getPost('EstudianteVerif');
            $origenCedula = $this->getPost('origenCedulaEstudiante');
            $nroCedula = $this->getPost('nroCedulaEstudiante');
            if(is_numeric($idDecoded)){
                $model = $this->loadModel($idDecoded);
                if($model){
                    $atributosIniciales = $model->attributes;
                    $model->attributes = $datosEstudiante;
                    $model->fecha_nacimiento = Utiles::transformDate($model->fecha_nacimiento);
                    if($atributosIniciales!=$model->attributes){
                        if($model->save()){
                            $titulos = EstudianteTitulo::model()->getTitulosEstudiante($idDecoded, $origenCedula, $nroCedula);
                            if(!is_null($titulos) && count($titulos)>0){
                                $this->renderPartial("_listaTitulos", array('titulos'=>$titulos));
                            }
                            else{
                                $this->renderPartial("//msgBox", array('class'=>'alertDialogBox', 'message'=>'Esta persona no posee credenciales o títulos registrados en el sistema.'));
                            }
                        }
                        else{
                            $errores = CHtml::errorSummary($model);
                            if(strlen($errores)>1){
                                $this->renderPartial("//errorSumMsg", array('model'=>$model));
                            }else{
                                $this->renderPartial("//msgBox", array('class'=>'errorDialogBox', 'message'=>'Ha ocurrido un error, comuniquese con el administrador del sistema y notifique lo sucedido.'));
                            }
                        }
                    }else{
                        $titulos = EstudianteTitulo::model()->getTitulosEstudiante($idDecoded, $origenCedula, $nroCedula);
                        if(!is_null($titulos) && count($titulos)>0){
                            $this->renderPartial("_listaTitulos", array('titulos'=>$titulos));
                        }
                        else{
                            $this->renderPartial("//msgBox", array('class'=>'alertDialogBox', 'message'=>'Esta persona no posee credenciales o títulos registrados en el sistema.'));
                        }
                    }
                }else{
                    throw new CHttpException(404, 'El estudiante indicado no se ha encontrado.');
                }
            }
            else{
                throw new CHttpException(401, 'No ha proporcionado los datos necesarios para efectuar esta acción.');
            }
        }
        else{
            throw new CHttpException(403, 'No está permitido efectuar esta acción mediante esta vía.');
        }

    }

    /**
     * Performs the AJAX validation.
     * @param Estudiante $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'estudiante-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * @author Jonathan Huaman Fecha 20/04/2015
     * @params Recibe el POST del Formulario vía Ajax.
     * Función que Permite Asociar el Estudiante con sus respectivas areas de Atención.
     */
    public function actionGuardarEstudianteAreaAtencion()
    {
        if($_POST)
        {
            $modelEstudianteAreaAtencion= new EstudianteAreaAtencion();
            $mensaje='';
            $_POST['EstudianteAreaAtencion']['estudiante_id']= base64_decode($_POST['EstudianteAreaAtencion']['estudiante_id']);
            if(is_numeric($_POST['EstudianteAreaAtencion']['estudiante_id']) == false)
            {
                $mensaje="Error al Traer Código del Estudiante, Comuniquese con el Administrador del Sistema.";
                echo json_encode(array('statusCode'=>'error','mensaje'=>$mensaje));
            }else
            {
                $modelEstudianteAreaAtencion->attributes= $_POST['EstudianteAreaAtencion'];
                $modelEstudianteAreaAtencion->estatus='A';
                $modelEstudianteAreaAtencion->usuario_ini_id = Yii::app()->user->id;
                $modelEstudianteAreaAtencion->fecha_ini = date("Y-m-d H:i:s");

                if( $modelEstudianteAreaAtencion->validate() && $modelEstudianteAreaAtencion->save() )
                {
                    $mensaje='Registro Satisfactorio del Área de Atención';
                    echo json_encode(array('status'=>'success','mensaje'=>$mensaje));
                }else
                {
                    $this->renderPartial('//errorSumMsg', array('model' => $modelEstudianteAreaAtencion));
                }
            }
        }
    }
    /**

     * @param   type $data,
     * @author Jonathan Huaman, Fecha 20/04/2015
     * @return $columna, el cual permite devolver los respectivos iconos para visualizar,Editar, activar e Inactivar los Datos de la Respectiva área de Atención
     */
    public function columnaAccionesAreaAtencion($data)
    {
        $id = $data["id"];
        if($data->estatus=='A'){
            $columna = CHtml::link("", "", array("class" => "fa fa-search verDatosAreaAtencion", "data" => base64_encode($id), "title" => "ver Datos")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green EditarDatosAreaAtencion", "data" => base64_encode($id), "title" => "Editar Datos")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("data" => base64_encode($id), "class" => "fa fa-trash-o red remove-data InactivarDatosAreaAtencion", "style" => "color:#555;", "title" => "Inactivar Datos"));
        }
        else{
            $columna = CHtml::link("", "", array("class" => "fa fa-search verDatosAreaAtencion","data"=>  base64_encode($id), "title" => "Ver Datos")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("data"=>  base64_encode($id), "class" => "fa icon-ok green remove-data activarDatosAreaAtencion", "style" => "color:#555;", "title" => "Activar Datos"));
        }
        return $columna;
    }



    public function columnaAcciones($data)
    {
        $id = $data["id"];
        if($data->estatus=='A'){
            $columna = CHtml::link("", "", array("class" => "fa fa-search verDatos", "data" => base64_encode($id), "title" => "ver Datos")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green EditarDatos", "data" => base64_encode($id), "title" => "Editar Datos")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("data" => base64_encode($id), "class" => "fa fa-trash-o red remove-data InactivarDatos", "style" => "color:#555;", "title" => "Inactivar Datos"));
        }
        else{
            $columna = CHtml::link("", "", array("class" => "fa fa-search verDatos","data"=>  base64_encode($id), "title" => "Ver Datos")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("data"=>  base64_encode($id), "class" => "fa icon-ok green remove-data activarDatos", "style" => "color:#555;", "title" => "Activar Datos"));
        }
        return $columna;
    }


    /**
     *     @author Jonathan Huaman, Fecha 20/04/2015
     *     @params $id, Recibe el parametro id para poder visualizar el Registro
     */
    public function actionVerDatosEstudianteAreaAtencion()
    {
        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        if (!is_numeric($id_decod))
        {
            $mensaje = 'No se ha encontrado el registro del Talento que ha solicitado para consultar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $model = EstudianteAreaAtencion::model()->findByPk($id_decod);
            if ($model != null)
            {
                $this->renderPartial('detallesEstudianteAreaAtencion', array('model' => $model), false, true);
            } else {
                $mensaje = 'No se ha encontrado el registro del Talento que ha solicitado para consultar. Recargue la página e intentelo de nuevo. </br>';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }
    /**
     *     @author Jonathan Huaman, Fecha 20/04/2015
     *     @params $estudiante_id
     */
    public function actionMostrarFormEstudianteAreaAtencion() {
        $estudiante_id= $_REQUEST['estudiante_id'];
        $modelEstudianteAreaAtencion=new EstudianteAreaAtencion();
        $areaAtencion= EstudianteAreaAtencion::model()->ObtenerAreaAtencionEstudiante( base64_decode($estudiante_id) );
        if($areaAtencion=='' || $areaAtencion==array())
        {
            $mensaje="Estimado Usuario, No Existen más Áreas de Atención Disponibles. Comuniquese con el Administrador del Sistema";
            echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
        }
        else
        {
            if(is_numeric(base64_decode($estudiante_id))==false)
            {
                $mensaje="Ocurrio Un error con el Código de Estudiante, recargue e intente Nuevamente.";
                echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
            }
            else{
                if($_GET)
                {
                    $this->renderPartial('_formEstudianteAreaAtencion',array('modelEstudianteAreaAtencion'=>$modelEstudianteAreaAtencion,'estudiante_id'=>$estudiante_id,'areaAtencion'=>$areaAtencion),false,true);
                }
            }
        }
    }


    public function actionMostrarDatosAreaAtencion() {

        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $estudiante_id= $_REQUEST['estudiante_id'];
        $mensaje = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro de Area de Atención que ha solicitado para modificar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $modelEstudianteAreaAtencion = EstudianteAreaAtencion::model()->findByPk($id_decod);
            $modelEstudianteAreaAtencion->id = base64_encode($modelEstudianteAreaAtencion->id);
            $areaAtencion= EstudianteAreaAtencion::model()->ObtenerAreaAtencionEstudiante( base64_decode($estudiante_id) );

            if($areaAtencion=='' || $areaAtencion==array())
            {
                $mensaje="Estimado Usuario, No Existen más Áreas de Atención para Editar. Comuniquese con el Administrador del Sistema";
                echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
            }
            else
            {
                array_push($areaAtencion,array( 'id'=>$modelEstudianteAreaAtencion->area_atencion_id,'nombre' => $modelEstudianteAreaAtencion->areaAtencion->nombre ) );

                if ($modelEstudianteAreaAtencion != null) {
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    //$this->renderPartial('_form', array('model' => $model), false, true);
                    $this->renderPartial('_formEstudianteAreaAtencion',array('modelEstudianteAreaAtencion'=>$modelEstudianteAreaAtencion,'estudiante_id'=>$estudiante_id,'areaAtencion'=>$areaAtencion),false,true);
                } else {
                    $mensaje = 'No se ha encontrado el registro de Area de Atención que ha solicitado para modificar. Recargue la página e intentelo de nuevo. </br>';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                }

            }
        }
    }
    /**
     *   @author Jonathan Huaman Fecha 20/04/2015
     *  Esta función permite Guardar los Datos Modificados asociados a un Registro en Particular del Area de Atención del Estudiante en Cuestión.
     */
    public function actionModificarEstudianteAreaAtencion()
    {
        if($_POST['EstudianteAreaAtencion'])
        {
            $mensaje='';
            $_POST['EstudianteAreaAtencion']['estudiante_id']= base64_decode($_POST['EstudianteAreaAtencion']['estudiante_id']);
            $_POST['EstudianteAreaAtencion']['id']= base64_decode($_POST['EstudianteAreaAtencion']['id'] );
            if( is_numeric($_POST['EstudianteAreaAtencion']['estudiante_id']) == false || is_numeric($_POST['EstudianteAreaAtencion']['id'])==false)
            {
                $mensaje="Error al Traer Código del Estudiante o Código del Area de Atención, Comuniquese con el Administrador del Sistema.";
                echo json_encode(array('statusCode'=>'error','mensaje'=>$mensaje));
            }else
            {
                $modelEstudianteAreaAtencion= EstudianteAreaAtencion::model()->findByPk($_POST['EstudianteAreaAtencion']['id']);
                $usuario_id = Yii::app()->user->id;
                $estatus = 'A';
                $fecha = date('Y-m-d H:i:s');
                $modelEstudianteAreaAtencion->area_atencion_id = $_POST['EstudianteAreaAtencion']['area_atencion_id'];
                $modelEstudianteAreaAtencion->estatus = $estatus;
                $modelEstudianteAreaAtencion->usuario_act_id = $usuario_id;
                $modelEstudianteAreaAtencion->fecha_act = $fecha;

                if( $modelEstudianteAreaAtencion->validate() && $modelEstudianteAreaAtencion->save() )
                {
                    $mensaje='Registro Satisfactorio del Área de Atención';
                    echo json_encode(array('status'=>'success','mensaje'=>$mensaje));
                }else
                {
                    $this->renderPartial('//errorSumMsg', array('model' => $modelEstudianteAreaAtencion));
                }
            }
        }
    }
    /**
     * @author Jonathan Huaman Fecha 20/04/2015
     * Esta función Permite la Inactivación lógica del Registro del estudiante con su respectiva area de atención.
     */
    public function actionInactivarEstudianteAreaAtencion() {

        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        $mensaje_exitoso = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro de Area de Atención que ha solicitado para inactivar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $modelEstudianteAreaAtencion = EstudianteAreaAtencion::model()->findByPk($id_decod);
            if ($modelEstudianteAreaAtencion != null) {
                $usuario_id = Yii::app()->user->id;
                $estatus = 'I'; //Estatus inactivado
                $fecha = date('Y-m-d H:i:s');
                $modelEstudianteAreaAtencion->estatus = $estatus;
                $modelEstudianteAreaAtencion->usuario_act_id = $usuario_id;
                $modelEstudianteAreaAtencion->fecha_eli = $fecha;
                //   var_dump($model);
                if ($modelEstudianteAreaAtencion->save()) {
                    $this->registerLog('INACTIVAR', 'estudiante.Modificar.InactivarEstudianteAreaAtencion', 'EXITOSO', 'Permite inactivar un registro de Area de Atencion del Estudiante');
                    $mensaje_exitoso = 'Inactivación exitosa del Area de Atención';
                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                } else { // error que no guardo
                    $mensaje.= 'No se pudo inactivar el registro de Área de Atencion';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                }
            } else {
                $mensaje = 'Por favor seleccione un registro del Talento para inactivar';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }

    /**
     * @author Jonathan Huaman Fecha 20/04/2015
     * Esta función Permite la activación lógica del Registro del estudiante con su respectiva area de atención.
     */
    public function actionActivarEstudianteAreaAtencion() {
        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        $mensaje_exitoso = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro del Área de Atencion que ha solicitado para activar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $modelEstudianteAreaAtencion = EstudianteAreaAtencion::model()->findByPk($id_decod);
            if ($modelEstudianteAreaAtencion != null) {
                $usuario_id = Yii::app()->user->id;
                $estatus = 'A'; //Estatus inactivado
                $fecha = date('Y-m-d H:i:s');
                $modelEstudianteAreaAtencion->estatus = $estatus;
                $modelEstudianteAreaAtencion->usuario_ini_id = $usuario_id;
                $modelEstudianteAreaAtencion->fecha_ini = $fecha;
                $modelEstudianteAreaAtencion->fecha_eli = null;
                $modelEstudianteAreaAtencion->usuario_act_id = $usuario_id;
                $modelEstudianteAreaAtencion->fecha_act = null;
                if ($modelEstudianteAreaAtencion->save()) {
                    $this->registerLog('ACTIVAR', 'estudiante.modificar.ActivarEstudianteAreaAtencion', 'EXITOSO', 'Permite activar un registro de un área de Atención asociado a un Estudiante.');
                    $mensaje_exitoso = 'Activación exitosa del Área de Atención';
                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                } else { // error que no guardo
                    $mensaje.= 'No se pudo activar el registro del área de atención asociado al Estudiante, Por favor intente nuevamente <br>';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                }
            } else {
                $mensaje = 'Por favor seleccione un registro del área de atención para activar';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }

    public function estatus($data) {
        $estatus = $data['estatus'];
        if (($estatus == 'A') || ($estatus == '')) {
            return 'Activo';
        } else if ($estatus == 'I') {
            return 'Inactivo';
        }
    }

    /**
     * @author Jonathan huaman fecha 20/04/2015
     * Esta funcion permite actualizar el gridView de los registros de Area de Atención asociados a su respectivo Estudiante.
     * @params recibe como parametros, estudiante_id, area_atencion_id , estatus.
     * @throws CHttpException
     */
    public function actionActualizarGridAreaAtencion()
    {
        if($this->hasRequest('ajax') AND $this->hasRequest('id') )
        {
            print_r($_REQUEST);

            $estudiante_id = $_REQUEST['id'];
            if(isset($_REQUEST['EstudianteAreaAtencion']['area_atencion_id']) || isset($_REQUEST['EstudianteAreaAtencion']['estatus']))
            {
                $area_atencion_id= $_REQUEST['EstudianteAreaAtencion']['area_atencion_id'];
                $estatus= $_REQUEST['EstudianteAreaAtencion']['estatus'];
            }

            $modelEstudianteAreaAtencion = new EstudianteAreaAtencion('search');
            $modelEstudianteAreaAtencion->estudiante_id = base64_decode($estudiante_id);

            if(isset($_REQUEST['EstudianteAreaAtencion']['area_atencion_id']) || isset($_REQUEST['EstudianteAreaAtencion']['estatus']))
            {
                $modelEstudianteAreaAtencion->area_atencion_id=$area_atencion_id;
                $modelEstudianteAreaAtencion->estatus= $estatus;
            }
            $areaAtencion= EstudianteAreaAtencion::model()->ObtenerDatosEstudianteAreaAtencion( base64_decode($estudiante_id) );

            $this->render(
                '_areaAtencion',
                array(
                    'modelEstudianteAreaAtencion'=>$modelEstudianteAreaAtencion,
                    'estudiante_id'=>$estudiante_id,
                    'areaAtencion'=>$areaAtencion,
                )
            );
        }
        else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }


    /**
     *     @author Jonathan Huaman, Fecha 20/04/2015
     *     @params $estudiante_id
     */
    public function actionMostrarFormEstudianteProgramaApoyo() {
        $estudiante_id= $_REQUEST['estudiante_id'];
        $modelEstudianteProgramaApoyo=new EstudianteProgramaApoyo();
        $programaApoyo= EstudianteProgramaApoyo::model()->ObtenerProgramaApoyoEstudiante( base64_decode($estudiante_id) );
        if($programaApoyo=='' || $programaApoyo==array())
        {
            $mensaje="Estimado Usuario, No Existen más Programas de Apoyos Disponibles. Comuniquese con el Administrador del Sistema";
            echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
        }
        else
        {
            if(is_numeric(base64_decode($estudiante_id))==false)
            {
                $mensaje="Ocurrio Un error con el Código de Estudiante, recargue e intente Nuevamente.";
                echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
            }
            else{
                if($_GET)
                {
                    $this->renderPartial('_formEstudianteProgramaApoyo',array('modelEstudianteProgramaApoyo'=>$modelEstudianteProgramaApoyo,'estudiante_id'=>$estudiante_id,'programaApoyo'=>$programaApoyo),false,true);
                }
            }
        }
    }
    /**
     * @author Jonathan Huaman Fecha 20/04/2015
     * @params Recibe el POST del Formulario vía Ajax.
     * Función que Permite Asociar el Estudiante con sus respectivas areas de Atención.
     */
    public function actionGuardarEstudianteProgramaApoyo()
    {
        if($_POST)
        {
            $modelEstudianteProgramaApoyo= new EstudianteProgramaApoyo();
            $mensaje='';
            $_POST['EstudianteProgramaApoyo']['estudiante_id']= base64_decode($_POST['EstudianteProgramaApoyo']['estudiante_id']);
            if(is_numeric($_POST['EstudianteProgramaApoyo']['estudiante_id']) == false)
            {
                $mensaje="Error al Traer Código del Estudiante, Comuniquese con el Administrador del Sistema.";
                echo json_encode(array('statusCode'=>'error','mensaje'=>$mensaje));
            }else
            {
                $modelEstudianteProgramaApoyo->attributes= $_POST['EstudianteProgramaApoyo'];
                $modelEstudianteProgramaApoyo->estatus='A';
                $modelEstudianteProgramaApoyo->usuario_ini_id = Yii::app()->user->id;
                $modelEstudianteProgramaApoyo->fecha_ini = date("Y-m-d H:i:s");

                if( $modelEstudianteProgramaApoyo->validate() && $modelEstudianteProgramaApoyo->save() )
                {
                    $mensaje='Registro Satisfactorio del Programa de Apoyo';
                    echo json_encode(array('status'=>'success','mensaje'=>$mensaje));
                }else
                {
                    $this->renderPartial('//errorSumMsg', array('model' => $modelEstudianteProgramaApoyo));
                }
            }
        }
    }

    /**
     * @author Jonathan huaman fecha 20/04/2015
     * Esta funcion permite actualizar el gridView de los registros de Area de Atención asociados a su respectivo Estudiante.
     * @params recibe como parametros, estudiante_id, area_atencion_id , estatus.
     * @throws CHttpException
     */
    public function actionActualizarGridProgramaApoyo()
    {
        if($this->hasRequest('ajax') AND $this->hasRequest('id') )
        {
            $estudiante_id = $_REQUEST['id'];
            if(isset($_REQUEST['EstudianteProgramaApoyo']['programa_apoyo_id']) || isset($_REQUEST['EstudianteProgramaApoyo']['estatus']))
            {
                $programa_apoyo_id= $_REQUEST['EstudianteProgramaApoyo']['programa_apoyo_id'];
                $estatus= $_REQUEST['EstudianteProgramaApoyo']['estatus'];
            }

            $modelEstudianteProgramaApoyo = new EstudianteProgramaApoyo('search');
            $modelEstudianteProgramaApoyo->estudiante_id = base64_decode($estudiante_id);

            if(isset($_REQUEST['EstudianteProgramaApoyo']['programa_apoyo_id']) || isset($_REQUEST['EstudianteProgramaApoyo']['estatus']))
            {
                $modelEstudianteProgramaApoyo->programa_apoyo_id=$programa_apoyo_id;
                $modelEstudianteProgramaApoyo->estatus= $estatus;
            }
            $programaApoyo= EstudianteProgramaApoyo::model()->ObtenerDatosEstudianteProgramaApoyo( base64_decode($estudiante_id) );

            $this->render(
                '_programaApoyo',
                array(
                    'modelEstudianteProgramaApoyo'=>$modelEstudianteProgramaApoyo,
                    'estudiante_id'=>$estudiante_id,
                    'programaApoyo'=>$programaApoyo,
                )
            );
        }
        else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    /**
     *     @author Jonathan Huaman, Fecha 20/04/2015
     *     @params $id, Recibe el parametro id para poder visualizar el Registro
     */
    public function actionVerDatosEstudianteProgramaApoyo()
    {
        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        if (!is_numeric($id_decod))
        {
            $mensaje = 'No se ha encontrado el registro del Programa de Apoyo que ha solicitado para consultar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $model = EstudianteProgramaApoyo::model()->findByPk($id_decod);
            if ($model != null)
            {
                $this->renderPartial('detallesEstudianteProgramaApoyo', array('model' => $model), false, true);
            } else {
                $mensaje = 'No se ha encontrado el registro del Programa de Apoyo que ha solicitado para consultar. Recargue la página e intentelo de nuevo. </br>';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }

    public function actionMostrarDatosProgramaApoyo() {

        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $estudiante_id= $_REQUEST['estudiante_id'];
        $mensaje = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro del Programa de Apoyo que ha solicitado para modificar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $modelEstudianteProgramaApoyo = EstudianteProgramaApoyo::model()->findByPk($id_decod);
            $modelEstudianteProgramaApoyo->id = base64_encode($modelEstudianteProgramaApoyo->id);
            $programaApoyo= EstudianteProgramaApoyo::model()->ObtenerProgramaApoyoEstudiante( base64_decode($estudiante_id) );

            if($programaApoyo=='' || $programaApoyo==array())
            {
                $mensaje="Estimado Usuario, No Existen más Programas de Apoyo para Editar. Comuniquese con el Administrador del Sistema";
                echo json_encode(array('status'=>'error','mensaje'=>$mensaje));
            }
            else
            {
                array_push($programaApoyo,array( 'id'=>$modelEstudianteProgramaApoyo->programa_apoyo_id,'nombre' => $modelEstudianteProgramaApoyo->programaApoyo->nombre ) );

                if ($modelEstudianteProgramaApoyo != null) {
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    //$this->renderPartial('_form', array('model' => $model), false, true);
                    $this->renderPartial('_formEstudianteProgramaApoyo',array('modelEstudianteProgramaApoyo'=>$modelEstudianteProgramaApoyo,'estudiante_id'=>$estudiante_id,'programaApoyo'=>$programaApoyo),false,true);
                } else {
                    $mensaje = 'No se ha encontrado el registro del Programa de Apoyo que ha solicitado para modificar. Recargue la página e intentelo de nuevo. </br>';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                }

            }
        }
    }

    /**
     *   @author Jonathan Huaman Fecha 20/04/2015
     *  Esta función permite Guardar los Datos Modificados asociados a un Registro en Particular del Area de Atención del Estudiante en Cuestión.
     */
    public function actionModificarEstudianteProgramaApoyo()
    {
        if($_POST['EstudianteProgramaApoyo'])
        {
            $mensaje='';
            $_POST['EstudianteProgramaApoyo']['estudiante_id']= base64_decode($_POST['EstudianteProgramaApoyo']['estudiante_id']);
            $_POST['EstudianteProgramaApoyo']['id']= base64_decode($_POST['EstudianteProgramaApoyo']['id'] );
            if( is_numeric($_POST['EstudianteProgramaApoyo']['estudiante_id']) == false || is_numeric($_POST['EstudianteProgramaApoyo']['id'])==false)
            {
                $mensaje="Error al Traer Código del Estudiante o Código del Programa de Apoyo, Comuniquese con el Administrador del Sistema.";
                echo json_encode(array('statusCode'=>'error','mensaje'=>$mensaje));
            }else
            {
                $modelEstudianteProgramaApoyo= EstudianteProgramaApoyo::model()->findByPk($_POST['EstudianteProgramaApoyo']['id']);
                $usuario_id = Yii::app()->user->id;
                $estatus = 'A';
                $fecha = date('Y-m-d H:i:s');
                $modelEstudianteProgramaApoyo->programa_apoyo_id = $_POST['EstudianteProgramaApoyo']['programa_apoyo_id'];
                $modelEstudianteProgramaApoyo->estatus = $estatus;
                $modelEstudianteProgramaApoyo->usuario_act_id = $usuario_id;
                $modelEstudianteProgramaApoyo->fecha_act = $fecha;

                if( $modelEstudianteProgramaApoyo->validate() && $modelEstudianteProgramaApoyo->save() )
                {
                    $mensaje='Actualización Satisfactoria del Programa de Apoyo';
                    echo json_encode(array('status'=>'success','mensaje'=>$mensaje));
                }else
                {
                    $this->renderPartial('//errorSumMsg', array('model' => $modelEstudianteProgramaApoyo));
                }
            }
        }
    }

    /**
     * @author Jonathan Huaman Fecha 20/04/2015
     * Esta función Permite la Inactivación lógica del Registro del estudiante con su respectiva area de atención.
     */
    public function actionInactivarEstudianteProgramaApoyo() {

        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        $mensaje_exitoso = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro del Programa de Apoyo que ha solicitado para inactivar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $modelEstudianteProgramaApoyo = EstudianteProgramaApoyo::model()->findByPk($id_decod);
            if ($modelEstudianteProgramaApoyo != null) {
                $usuario_id = Yii::app()->user->id;
                $estatus = 'I'; //Estatus inactivado
                $fecha = date('Y-m-d H:i:s');
                $modelEstudianteProgramaApoyo->estatus = $estatus;
                $modelEstudianteProgramaApoyo->usuario_act_id = $usuario_id;
                $modelEstudianteProgramaApoyo->fecha_eli = $fecha;
                //   var_dump($model);
                if ($modelEstudianteProgramaApoyo->save()) {
                    $this->registerLog('INACTIVAR', 'estudiante.Modificar.InactivarEstudianteProgramaApoyo', 'EXITOSO', 'Permite inactivar un registro del programa de apoyo del Estudiante');
                    $mensaje_exitoso = 'Inactivación exitosa del Programa de Apoyo';
                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                } else { // error que no guardo
                    $mensaje.= 'No se pudo inactivar el registro del Programa de Apoyo';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                }
            } else {
                $mensaje = 'Por favor seleccione un registro del programa de apoyo para inactivar';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }


    /**
     * @author Jonathan Huaman Fecha 20/04/2015
     * Esta función Permite la activación lógica del Registro del estudiante con su respectiva area de atención.
     */
    public function actionActivarEstudianteProgramaApoyo() {
        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        $mensaje_exitoso = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro del Programa de Apoyo que ha solicitado para activar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $modelEstudianteProgramaApoyo = EstudianteProgramaApoyo::model()->findByPk($id_decod);
            if ($modelEstudianteProgramaApoyo != null) {
                $usuario_id = Yii::app()->user->id;
                $estatus = 'A'; //Estatus inactivado
                $fecha = date('Y-m-d H:i:s');
                $modelEstudianteProgramaApoyo->estatus = $estatus;
                $modelEstudianteProgramaApoyo->usuario_ini_id = $usuario_id;
                $modelEstudianteProgramaApoyo->fecha_ini = $fecha;
                $modelEstudianteProgramaApoyo->fecha_eli = null;
                $modelEstudianteProgramaApoyo->usuario_act_id = $usuario_id;
                $modelEstudianteProgramaApoyo->fecha_act = null;
                if ($modelEstudianteProgramaApoyo->save()) {
                    $this->registerLog('ACTIVAR', 'estudiante.modificar.ActivarEstudianteProgramaApoyo', 'EXITOSO', 'Permite activar un registro de un Programa de Apoyo asociado a un Estudiante.');
                    $mensaje_exitoso = 'Activación exitosa del Programa de Apoyo';
                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                } else { // error que no guardo
                    $mensaje.= 'No se pudo activar el registro del Programa d Apoyo asociado al Estudiante, Por favor intente nuevamente <br>';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                }
            } else {
                $mensaje = 'Por favor seleccione un registro del Programa de Apoyo para activar';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }

}
