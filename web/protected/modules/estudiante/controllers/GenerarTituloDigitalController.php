<?php

/**
 * @author José Gabriel González y Nelson González
 * @editedAt 2014-01-07
 */
class GenerarTituloDigitalController extends Controller {

    public $defaultAction = 'imprimir';
    
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Digitalizar de Título',
        'write' => 'Digitalizar de Título',
        'label' => 'Digitalizar de Título'
    );

    const MODULO = "titulo.DigitalTitulo";

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'visual',),
                'pbac' => array('write', 'admin',),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index',),
                'pbac' => array('read',),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        $this->render('index', array(
        ));
    }
    
    /**
     * Esta acción emite el título o certificación digital dado un id de título o certificación digital, 
     * id del estudiante y si los datos son legados o no. A su vez guarda los datos del título o certificación digital
     * para su envío posterior.
     * 
     * @param integer(base64Enc) $id Id del Título
     * @param integer(base64Enc) $legacy 0/1 Si debe buscar en los legados o no.
     * @param integer $estudiante Id del Estudiante ya sea en los legados o registros nuevos dependiendo del parámetro $legacy
     */
    public function actionVisual($id, $legacy, $estudiante) {

        Yii::import('ext.qrcode.QRCode');
        $modelDigitalTitulo = new TituloDigital();

        $isLegacy = $this->getIdDecoded($legacy);
        $tituloId = $this->getIdDecoded($id);
        $estudianteId = $this->getIdDecoded($estudiante);
        //$array=array('isLegacy'=>$isLegacy,'tituloId'=>$tituloId,'estudianteId'=>$estudianteId);
        
        $meses = Utiles::$MESES_STR;
        //ld($array);
//        die();
        //$mpdf = Yii::app()->ePdf->mpdf('', 'A4-L',0, '', 13, 13, 13, 13, 9, 9, 'L');
        $mpdf = Yii::app()->ePdf->mpdf('', 'A4-L',0, '', 15, 15, 16, 9, 9, 9, 'L');
        //$mpdf=Yii::app()->ePdf->mpdf('','', 0, '', 15, 15, 16, 16, 9, 9, 'L');

        $dia = date("d-m-Y");
        $diaArchivo = $dia;

        
        try{

            $dataTitulo = TituloDigital::model()->getDataTituloDigital((int)$isLegacy, (int)$tituloId, (int)$estudianteId);
            $modelDigitalTitulo->attributes = $dataTitulo;
            $modelDigitalTitulo->codigo_verificacion = md5(json_encode($dataTitulo));
            $modelDigitalTitulo->is_legacy = $isLegacy;
            $modelDigitalTitulo->observacion = json_encode($dataTitulo)."(EncodeMethod: md5)";
            $modelDigitalTitulo->usuario_ini_id = Yii::app()->user->id;
            $modelDigitalTitulo->fecha_ini = date('Y-m-d H:i:s');
            $modelDigitalTitulo->estatus = 'A';

             /*ld($dataTitulo);
            die();*/
            // ld($modelDigitalTitulo);
            $codigo = $modelDigitalTitulo->codigo_verificacion;

            /**
             * qr
            */
            $url = Yii::app()->getBaseUrl(true)."/verificacion/tituloDigital/verificar/codigo/$codigo";
            $qrGenerator = new QRCode($url);
            $command = 'chmod 777 -R '.Utiles::getPathToSaveTituloQr();
            exec($command);
            $srcQr = Utiles::getPathToSaveTituloQr()."/$codigo.png";
            $qrGenerator->create($srcQr);
            $command = 'chmod 777 -R '.Utiles::getPathToSaveTituloQr();
            exec($command);

            //salva el titulo digital en la tabla (hay que condicionarlo para que lo haga si ya este no a sido salvado anteriormente)

            if(!$modelDigitalTitulo->exiteSolucitudTituloDigital($tituloId) ) {
                /*$exite=$modelDigitalTitulo->exiteSolucitudTituloDigital($tituloId);

            ld($exite);
            die();*/

                if ($modelDigitalTitulo->save()) {

                } else {
                    $this->render('//errorSumMsg', array('model' => $modelDigitalTitulo));
                    die();
                }
            }


            /*$exite=$modelDigitalTitulo->exiteSolucitudTituloDigital($tituloId);

            //ld($exite.$tituloId);
            ld($exite);
            die();*/

            $fechaNacimiento = explode('-', $dataTitulo['fecha_nacimiento']);
            $fechaOtorgamiento = explode('-', $dataTitulo['fecha_otorgamiento']);
            $monthName = strtoupper($meses[(int) $fechaNacimiento[1] - 1]);
            //no me trae la fecha de otorgamiento en la no legacy
            $mesNombreExpendicion = strtoupper($meses[(int) $fechaOtorgamiento[1] - 1]);
            //$mesNombreExpendicion='';

            $fechaOtor=$dataTitulo['fecha_otorgamiento'];
            //$EstuadianteEgresado=$dataTitulo['cedula_estudiante'];
            $EstuadianteEgresado=$dataTitulo['nombre_estudiante'];

            /**
            ld
             */
            /*$array=array('isLegacy'=>$isLegacy,'tituloId'=>$tituloId,'estudianteId'=>$estudianteId, 'fechaNacimiento'=>$fechaNacimiento, 'fechaOtorgamiento'=>$fechaOtorgamiento,'monthName'=>$monthName,'mesNombreExpendicion'=>$mesNombreExpendicion,'fechaOtor'=>$fechaOtor);
            $exite=$modelDigitalTitulo->exiteSolucitudTituloDigital($tituloId);
            ld($exite.$tituloId);
            ld($dataTitulo);
            ld($array);
            ld($srcQr);
            die();*/
            //BARINITAS, Estado BARINAS
            $dataTitulo['nacido_en']=ucwords(Utiles::strtolower_utf8($dataTitulo['nacido_en']));

            $header = $this->renderPartial('_headerReporteDigitalTitulo', array('resultadoBusqueda' => $dataTitulo, 'fechaNacimiento' => $fechaNacimiento, 'monthName' => $monthName, 'mesNombreExpendicion' => $mesNombreExpendicion, 'fechaOtorgamiento' => $fechaOtorgamiento, 'stringMd5' => $codigo), true);
            $body = $this->renderPartial('_bodyReporteDigitalTitulo', array('resultadoBusqueda' => $dataTitulo, 'fechaNacimiento' => $fechaNacimiento, 'monthName' => $monthName, 'mesNombreExpendicion' => $mesNombreExpendicion, 'fechaOtorgamiento' => $fechaOtorgamiento, 'stringMd5' => $codigo), true);
            $footer = $this->renderPartial('_footerReporteDigitalTitulo', array('stringMd5' => $codigo, 'resultadoBusqueda' => $dataTitulo,'srcQr'=>$srcQr), true);


            //$plantillaTituloDigital=$this->renderPartial('_plantillaTituloDigital', array('resultadoBusqueda' => $dataTitulo, 'fechaNacimiento' => $fechaNacimiento, 'monthName' => $monthName, 'mesNombreExpendicion' => $mesNombreExpendicion, 'fechaOtorgamiento' => $fechaOtorgamiento, 'stringMd5' => $codigo), true);
            //$plantillaTituloDigital = $this->renderPartial('_plantillaTituloDigital', array('resultadoBusqueda' => $dataTitulo, 'fechaNacimiento' => $fechaNacimiento, 'monthName' => $monthName, 'mesNombreExpendicion' => $mesNombreExpendicion, 'fechaOtorgamiento' => $fechaOtorgamiento, 'stringMd5' => $codigo), true);
            //$mpdf->WriteHTML($plantillaTituloDigital);


            //--$mpdf->SetFont('sans-serif');
            $mpdf->SetHTMLHeader($header);
            $mpdf->WriteHTML($body);
            //-- $mpdf->setHTMLFooter($footer . '<br>' . '<p style="text-align:center;"> {PAGENO} / {nb}</p>');

            $mpdf->setHTMLFooter($footer);
//            $mpdf->WriteHTML($footer);



            $mpdf->Output($EstuadianteEgresado . '/' . $diaArchivo . '.pdf', 'I');

        }
//        $mpdf->Output('1235','1235');

        catch(Exception $e){
            $this->render('//msgBox', array('class'=>'errorDialogBox', 'message'=>$e->getMessage()));
        }
        $this->render('//msgBox', array('class'=>'errorDialogBox', 'message'=>$e->getMessage()));
    }
    public function actionHola() {
        echo "\n --------d> que me cuentas\n";
    }
}
