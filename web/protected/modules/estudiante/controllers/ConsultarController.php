<?php

class ConsultarController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction = 'index';
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Consulta de Estudiante',
        //  'write' => 'Consulta de Planteles', // no lleva etiqueta write
        'label' => 'Consulta de Estudiante'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
//'accessControl', // perform access control for CRUD operations
//'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'reporte', 'informacion', 'verBoletin'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('write'),
            ),
            /* array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions'=>array('admin','delete'),
              'users'=>array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
//        echo "hola";
//        die();
        /**
         * IDENTIFICO SI ES DIRECTOR Y DE SER ASI QUE ESTE CONSULTANDO UNO DE SUS PLANTELES.
         */
        /* CONSULTO SI EL PLANTEL ES DEL DIRECTOR */
        $plantelPK = '';
        $plantel_id = '';
        $nombrePlantel = '';

        /*Yii::app()->cache->delete('METADATA:matricula.estudiante');
        $table = Yii::app()->cache->get('METADATA:matricula.estudiante');
        var_dump($table);*/
        if ($this->hasRequest('id') && $this->getRequest('id') != '') {
            $plantel_id = $this->getRequest('id');
            $plantel_id = base64_decode($plantel_id);
            $plantelPK = Plantel::model()->findByPk($plantel_id);
            $datosPlantel = Plantel::model()->DatosPlantel($plantel_id);
            if ($datosPlantel != NULL) {
                $nombrePlantel = $datosPlantel[0]['id'];
            } else if ($datosPlantel == NULL && $plantelPK == NULL) {
                $this->redirect('/estudiante/');
            } else if ($datosPlantel == NULL && $plantelPK != NULL && Yii::app()->user->group != UserGroups::DIRECTOR) {
                $nombrePlantel = $plantelPK['id'];
            } else if ($datosPlantel == NULL && $plantelPK != NULL && Yii::app()->user->group == UserGroups::DIRECTOR) {
                $this->redirect('/estudiante/');
            }
        }

        $model = new Estudiante('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('Estudiante')) {
            $model->attributes = $form=$this->getQuery('Estudiante');

            if(array_key_exists('codigo_plantel',$form)){
                $model->plantel_actual_id=trim($form['codigo_plantel']);
            }
            else {
                if(array_key_exists('plantel_id',$form) AND strlen($form['plantel_id'])>2){
                    $model->plantel_actual_id=trim($form['plantel_id']);
                }
                else {
                    if(!is_null($plantel_id) AND $plantel_id!= ''){
                        $model->plantel_actual_id=trim($form['plantel_id']);
                    }
                    else {
                        if(array_key_exists('plantel_actual_id',$form)){
                            $model->plantel_actual_id=trim($form['plantel_actual_id']);
                        }
                    }
                }
            }
            if(array_key_exists('documento_identidad_representante',$form) ){
                $model->representante_id=trim($form['documento_identidad_representante']);
            }
            else {
                if(array_key_exists('representante_id',$form) ){
                    $model->representante_id=trim($form['representante_id']);
                }
            }
        }
        $this->render('index', array(
                'model' => $model,
                'nombrePlantel' => $nombrePlantel,
                'plantel_id' => $plantel_id,
                'plantelPK' => $plantelPK,
            )
        );
    }

    /**
     *
     * @param type $id Id del Estudiante
     * @throws CHttpException
     */
    public function actionInformacion($id) {
        $sinacoes = false;
        $firmantes = array();
        /**
         * IDENTIFICO SI ES DIRECTOR Y DE SER ASI QUE ESTE CONSULTANDO UNO DE SUS PLANTELES.
         */
        /* CONSULTO SI EL PLANTEL ES DEL DIRECTOR */
        $plantelPK = '';
        $plantel_id = '';
        $nombrePlantel = '';

        $dataConsultaTitulo = array();
        $resultadoBusqueda = array();
        $historialEstudiante = array();
        $dataConsultarNota = array();

        $estudiante_id = $id;
        if(strlen($estudiante_id)>0 && is_numeric($this->getIdDecoded($estudiante_id))) {
            $plantel_id = Estudiante::model()->findAll(array('condition' => 'id = :id', 'params'=>array(':id'=>$this->getIdDecoded($estudiante_id))));
            $plantel_anterior_id = $plantel_id[0]['plantel_anterior_id'];
            $plantel_id = $plantel_id[0]['plantel_actual_id'];

            $plantelPK = Plantel::model()->findByPk($plantel_id);
            $plantelAnteriorPK = Plantel::model()->findByPk($plantel_anterior_id);
            $datosPlantel = Plantel::model()->DatosPlantel($plantel_id);
//            var_dump($datosPlantel);die();
            if ($datosPlantel != NULL) {
                $nombrePlantel = $datosPlantel[0]['id'];
            }
//            else if($datosPlantel == NULL && $plantelPK == NULL){
//                $this->redirect('/estudiante/');
//            }
            else if ($datosPlantel == NULL && $plantelPK != NULL && Yii::app()->user->group != UserGroups::DIRECTOR) {
                $nombrePlantel = $plantelPK['id'];
            }
//            else if($datosPlantel == NULL && $plantelPK != NULL && Yii::app()->user->group == UserGroups::DIRECTOR) {
//                $this->redirect('/estudiante/1');
//            }
        }
        if (array_key_exists('id', $_REQUEST) && $_REQUEST['id'] !== '') {
            $id = $_REQUEST['id'];
            $estudiante_id = base64_decode($id);
            if (is_numeric($estudiante_id)) {
                $model = $this->loadModel($estudiante_id);

                /*CONSULTAR TITULO DEL ESTUDIANTE - MODULO DESARROLLADO POR ENRIQUEX00*/
                $nacionalidad = $model->tdocumento_identidad;
                $documentoIdentidadEStudiante = $model->documento_identidad;

                $resultadoBusqueda = Titulo::model()->buscarEstTitulo($nacionalidad, $documentoIdentidadEStudiante);

                if ($resultadoBusqueda) {
                    $estudianteID = $resultadoBusqueda[0]['estudiante_id'];
                    if(array_key_exists('mes_egreso',$resultadoBusqueda[0]) AND array_key_exists('anio_egreso',$resultadoBusqueda[0])){
                        $sinacoes = true;
                        $mes_egreso = $resultadoBusqueda[0]['mes_egreso'];
                        $anio_egreso = $resultadoBusqueda[0]['anio_egreso'];
                        $cod_plantel = $resultadoBusqueda[0]['cod_plantel'];
                        $periodo =  substr($anio_egreso,2,2);
                        $firmantes = Titulo::model()->buscarFirmantesTituloSinacoes($cod_plantel,$periodo,$anio_egreso,$mes_egreso);
                        if (!$firmantes) {
                            $firmantes = array();
                        }
                    }
                    if ($estudianteID) {
                        $documentoIdentidadEStudiante = (int) $documentoIdentidadEStudiante;
                        $compara = false;
                        for ($i = 1; $i < 5; $i++) {
                            $coincidir = $i;
                            if ($i == 4) {
                                $coincidir = 'P';
                            }

                            $buscarNotas = Titulo::model()->buscarNotaEstudiante($documentoIdentidadEStudiante, $coincidir);


                            if ($buscarNotas) {

                                $dataConsultarNota[] = $this->dataProviderConsultaNota($buscarNotas);
                                $compara = TRUE;
                            }



                        }
//                        var_dump($dataConsultarNota[3]);
//                        die();
                        if ($compara == false) {
                            $dataConsultarNota = array();
                        }
//                        var_dump($dataConsultarNota);
//                        die();

                        $dataConsultaTitulo = $this->dataProviderConsultaTitulo($resultadoBusqueda);


//
//                        var_dump($dataConsultarNota);
                        //  var_dump($dataConsultaTitulo);
                        //     die();
                        //$this->renderPartial('mostrarResultadoBusqueda', array(
                        //'plantelPK' => $plantelPK
                        //), FALSE, TRUE);
//                $respuesta['statusCode'] = 'alert';
//                echo json_encode($respuesta);
                    }
                }

                /*if (strlen($model->documento_identidad) > 2) {
                    $model->documento_identidad = 'V-' . $model->documento_identidad;
                }*/
                if ($model) {
                    /* BUSQUEDA POR INSCRIPCION_ESTUDIANTE Y POR TALUMNOS_ACAD */
//                    var_dump($model->id);die();
                    if(is_numeric($model->documento_identidad)){
                        $identificacion = $model->documento_identidad;

                    }
                    else {
                        if(is_numeric($model->cedula_escolar)){
                            $identificacion = $model->cedula_escolar;
                        }
                        else {
                            $identificacion=null;
                        }
                    }
                    $historicoEstudiante = Estudiante::model()->historicoEstudiante($model->documento_identidad,$model->cedula_escolar, $model->id);
                    if ($model->representante_id) {
                        $modelRepresentante = $this->loadModelRepresentante($model->representante_id);
                        $modelRepresentante->documento_identidad = 'V-' . $modelRepresentante->documento_identidad;
                    } else {
                        $modelRepresentante = new Representante;
                    }
                }
            }

            $modelHistorialMedico = new HistorialMedico;
            $modelDatosAntropometricos = new DatosAntropometricos;
            $modelDatosAntropometricos = $this->loadModelDatosAntropometrico($model->id);
            $estadoCivil = EstadoCivil::model()->findAll();
            $genero = Genero::model()->findAll();
            $estado = Estado::model()->findAll(array('order' => 'nombre ASC'));
            $pais = $model->obtenerPaises();
            $zonaUbicacion = ZonaUbicacion::model()->findAll();
            $condicionVivienda = $model->obtenerCondicionVivienda();
            $tipoVivienda = $model->obtenerTipoVivienda();
            $ubicacionVivienda = $model->obtenerUbicacionVivienda();
            $condicionInfraestructura = CondicionInfraestructura::model()->findAll();
            $etnia = $model->obtenerEtnia();
            $afinidad = Afinidad::model()->findAll(); /* AFINIDAD OTRO REPRESENTANTE LEGAL */
            $afinidadMadre = Afinidad::model()->findAll(array('condition' => "nombre ILIKE 'Madre'")); /* AFINIDAD OTRO REPRESENTANTE MADRE */
            $afinidadPadre = Afinidad::model()->findAll(array('condition' => "nombre ILIKE 'Padre'")); /* AFINIDAD OTRO REPRESENTANTE MADRE */
            $diversidadFuncional = $model->obtenerDiversidadFuncional();
            $tipoSangre = $model->obtenerTipoSangre();
            $profesion = $model->obtenerProfesion();


            /**
             *          Agregado Por Jonathan Huaman Fecha 22-04-2015
             *
             */
            $modelEstudianteAreaAtencion= new EstudianteAreaAtencion('search');
            $modelEstudianteAreaAtencion->estudiante_id= $estudiante_id;
            //$areaAtencion= EstudianteAreaAtencion::model()->ObtenerDatosEstudianteAreaAtencion($estudiante_id);
            $modelEstudianteProgramaApoyo= new EstudianteProgramaApoyo('search');
            $modelEstudianteProgramaApoyo->estudiante_id=$estudiante_id;
            //$programaApoyo=  EstudianteProgramaApoyo::model()->ObtenerDatosEstudianteProgramaApoyo($estudiante_id);

            $this->render('informacion', array(
                'model' => $model,
                'modelRepresentante' => $modelRepresentante,
                'modelHistorialMedico' => $modelHistorialMedico,
                'modelDatosAntropometricos' => $modelDatosAntropometricos,
                'estadoCivil' => $estadoCivil,
                'genero' => $genero,
                'estado' => $estado,
                'pais' => $pais,
                'zonaUbicacion' => $zonaUbicacion,
                'condicionVivienda' => $condicionVivienda,
                'tipoVivienda' => $tipoVivienda,
                'ubicacionVivienda' => $ubicacionVivienda,
                'condicionInfraestructura' => $condicionInfraestructura,
                'etnia' => $etnia,
                'afinidad' => $afinidad,
                'afinidadMadre' => $afinidadMadre,
                'afinidadPadre' => $afinidadPadre,
                'diversidadFuncional' => $diversidadFuncional,
                'tipoSangre' => $tipoSangre,
                'profesion' => $profesion,
                'plantelPK' => $plantelPK,
                'plantelAnteriorPK' => $plantelAnteriorPK,
                'nombrePlantel' => $nombrePlantel,
                'historicoEstudiante' => $historicoEstudiante,
                'modelEstudianteAreaAtencion'=> $modelEstudianteAreaAtencion,
                'modelEstudianteProgramaApoyo'=>$modelEstudianteProgramaApoyo,
                /*ESTO UN MODULO ACTUALIZADO POR ENRIQUE*/
                'dataProvider' => $dataConsultaTitulo,
                'resultadoBusqueda' => $resultadoBusqueda,
                'historicoEstudiante' => $historicoEstudiante,
                'dataConsultarNota' => $dataConsultarNota,
                'firmantes'=>$firmantes,
                'sinacoes'=>$sinacoes
                /*FIN DEL MODULO ACTUALIZADO*/
            ));
        } else {
            throw new CHttpException(404, 'No se ha especificado el Estudiante que desea modificar. Vuelva a la página anterior e intentelo de nuevo.'); // esta vacio el request
        }
    }

    function dataProviderConsultaTitulo($resultadoBusqueda) {
        foreach ($resultadoBusqueda as $key => $value) {
            $cedula_identidad = $value['documento_identidad'];
            $nombres = $value['nombres'];
            $apellidos = $value['apellidos'];
            $plantel = $value['nombreplantel'];
            $plan_nombre = $value['nombre_plan'];
            $nombre_mencion = $value['nombre_mencion'];
            $cod_estadistico = $value['cod_estadistico'];
            $serial = $value['serial'];
            // $anoEgreso = $value['ano_egreso'];
            (isset($value['anio_egreso']) && $value['anio_egreso'] != null && $value['anio_egreso'] != '')? $anoEgreso = $value['anio_egreso']:$anoEgreso = 'NO POSEE';
            // (array_key_exists('ano_egreso',$value) || isset($value['ano_egreso']))?$anoEgreso = $value['ano_egreso']:$anoEgreso = null; //Comente esto *Marisela*

            $fechaOtorgacion = $value['fecha_otorgamiento'];
            if($fechaOtorgacion!=null){
                $fechaOtorgacion = date("d-m-Y", strtotime($fechaOtorgacion));
            }else{
                $fechaOtorgacion='NO POSEE';
            }

            $plantelCode_estadi = $plantel . ' ' . $cod_estadistico;
            $nombreApellido = $nombres . ' ' . $apellidos;
//            $botones = "<div class='center'>" . CHtml::checkBox('EstSolicitud[]', "false", array('value' => base64_encode($value['id']),
////                   'onClick' => "Estudiante('')",
//                        "title" => "solicitud")
//                    ) .
//                    "</div>";
//            $columna .= CHtml::checkBox('repitiente[]', false, array('id' => 'repitiente[]', 'title' => 'Repitente')) . '&nbsp;&nbsp;&nbsp;';
            $rawData[] = array(
                'id' => $key,
                'cedula_identidad' => '<center>' . $cedula_identidad . '</center>',
                'nombreApellido' => '<center>' . $nombreApellido . '</center>',
                'plan_nombre' => '<center>' . $plan_nombre . '</center>',
                'nombre_mencion' => "<center>" . $nombre_mencion . '</center>',
                'serial' => "<center>" . $serial . "</center>",
                'plantelCode_estadi' => "<center>" . $plantelCode_estadi . '</center>',
                'anoEgreso' => "<center>" . $anoEgreso . '</center>',
                'fechaOtorgacion' => "<center>" . $fechaOtorgacion . '</center>'
                //  'botones' => '<center>' . $botones . '</center>'
            );
        }
        // var_dump($rawData);
        //  die();
        return new CArrayDataProvider($rawData, array(
                'pagination' => false,
                //    'pagination' => array(
                //      'pageSize' => 5,
                //),
            )
        );
    }

    function dataProviderConsultaNota($buscarNotas) {
//        var_dump($buscarNotas);
//        die();
        foreach ($buscarNotas as $key => $value) {
//            var_dump($buscarNotas['materia']);
//            die();
            $materia = $value['materia'];
            $mencion = $value['mencion'];
            $nota = $value['nota'];
//            $botones = "<div class='center'>" . CHtml::checkBox('EstSolicitud[]', "false", array('value' => base64_encode($value['id']),
////                   'onClick' => "Estudiante('')",
//                        "title" => "solicitud")
//                    ) .
//                    "</div>";
//            $columna .= CHtml::checkBox('repitiente[]', false, array('id' => 'repitiente[]', 'title' => 'Repitente')) . '&nbsp;&nbsp;&nbsp;';
            $rawData[] = array(
                'id' => $key,
                'materia' => '<center>' . $materia . '</center>',
                'mencion' => '<center>' . $mencion . '</center>',
                'nota' => '<center>' . $nota . '</center>',
                //  'botones' => '<center>' . $botones . '</center>'
            );
        }

        //  die();
//        var_dump($rawData);
//        die();
        return new CArrayDataProvider($rawData, array(
                'pagination' => false,
                //    'pagination' => array(
                //      'pageSize' => 5,
                //),
            )
        );
    }


    /*
     * Botones del accion (crear, consultar)
     */
    public function columnaAcciones($data){

        $id = $data["id"]; /* ID DEL ESTUDIANTE */
        $estatus = $data["estatus"];
        $columna = '<div class="btn-group">
                        <button class="btn btn-xs dropdown-toggle" data-toggle="dropdown">
                            Seleccione
                            <span class="icon-caret-down icon-on-right"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-yellow pull-right">';
        $bc = '';

        if (isset($_REQUEST['bc']) && $_REQUEST['bc'] == '1') {
            $bc = '/bc/1/id/' . base64_encode($id);
        } else {
            $bc = '/id/' . base64_encode($id);
        }

        $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Consultar Datos</span>", Yii::app()->createUrl("/estudiante/consultar/informacion" . $bc), array("class" => "fa fa-search-plus", "title" => "Consultar Datos de ".ucwords(strtolower($data['nombres'].' '.$data['apellidos'])))) . '</li>';
        $plantel_actual_id = $data['plantel_actual_id'];
        $usuarioDirectorActivo = Estudiante::model()->optenerEstatusDirector(Yii::app()->user->id, $data['plantel_actual_id']);
        $plantel_id = '';
        if ($usuarioDirectorActivo[0]['plantel_id'] == $data['plantel_actual_id']) {
            $plantel_id = Plantel::model()->datosPlantel($plantel_actual_id);
        }
        if ((Yii::app()->user->pbac("estudiante.modificar.write")) || (Yii::app()->user->pbac("estudiante.modificar.admin"))) {
            $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Editar Datos</span>", Yii::app()->createUrl("/estudiante/modificar/index" . $bc), array("class" => "fa fa-pencil purple", "title" => "Editar Datos del Estudiante")) . '</li>';
        }
        //$columna .= CHtml::link("", "/estudiante/consultar/reporte?id=" . base64_encode($id), array("class" => "fa fa-print", "title" => "Imprimir estudiante")) . '&nbsp;&nbsp;';
        //$columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarNivel($id)", "title" => "Consultar este nivel")) . '&nbsp;&nbsp;';
        //$columna .= CHtml::link("", "", array('onClick' => "activarNivel($id)", "class" => "fa icon-ok green", "title" => "Activar este nivel")) . '&nbsp;&nbsp;';

        if (Yii::app()->user->group == UserGroups::ROOT || Yii::app()->user->group == UserGroups::ADMIN_0) {
            /**
             * @author Marisela
             * @editedBy Gabriel
             * Boton de ver boletin
             */
            $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Ver Boletin</span>",
                    Yii::app()->createUrl("/estudiante/consultar/verBoletin" . $bc),
                    array("class" => "fa fa-arrow-circle-down red",
                        "title" => "Ver Boletin de Calificaciones de ".ucwords(strtolower($data['nombres'].' '.$data['apellidos']))
                    )
                ) . '</li>';

            /**
             * @author Gabriel
             */
            $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Ver Título Digital</span>",
                    Yii::app()->createUrl("/estudiante/consultar/verTituloDigital" . $bc),
                    array(
                        "class" => "lnk-titulo-digital fa fa-graduation-cap blue",
                        "data-estudiante-id-enc"=>base64_encode($id),
                        "data-estudiante-origen"=>$data['tdocumento_identidad'],
                        "data-estudiante-cedula"=>$data['documento_identidad'],
                        "data-estudiante-nombres"=>$data['nombres'],
                        "data-estudiante-apellidos"=>$data['apellidos'],
                        "data-estudiante-correo"=>$data['correo'],
                        "data-estudiante-estado-id"=>$data['estado_nac_id'],
                        "data-estudiante-municipio-id"=>$data['municipio_nac_id'],
                        "data-estudiante-parroquia-id"=>$data['parroquia_nac_id'],
                        "data-estudiante-fecha-nacimiento"=>$data['fecha_nacimiento'],
                        "data-estudiante-sexo"=>$data['sexo'],
                        "title" => "Ver Título Digital de ".ucwords(strtolower($data['nombres'].' '.$data['apellidos']))
                    )
                ).'</li>';
            $columna .= '</ul></div>';
        }
        return $columna;
    }

    public function actionVerBoletin() {

        $id = (isset($_REQUEST['id'])) ? base64_decode($_REQUEST['id']) : '';
        // $lapso = (isset($_REQUEST['lapso'])) ? base64_decode($_REQUEST['lapso']) : "";

        if ($id != '') {

            $modulo = "Estudiante.Consultar.verBoletin";
            $ip = Yii::app()->request->userHostAddress;
            $ruta = yii::app()->basePath;
            $ruta = $ruta . '/yiic';
            $username = Yii::app()->user->name;
            $usuario_id = Yii::app()->user->id;
            $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_actual_id = $periodo_escolar_actual_id['id'];
            $idInscripcion = Estudiante::model()->obtenerIdInscripcion($id);

            if($idInscripcion){
                $datos_header = CalificacionAsignaturaEstudiante::cargarCorreosRepresentantes($idInscripcion);
                $nivel =$datos_header[0]['nivel_id'] ;
                $datosCalificaciones = AsignaturaEstudiante::model()->obtenerAsignaturasEstudianteBoletin($idInscripcion,base64_encode(1),$nivel);
                //$datos_footer = array();


                if ($datos_header != array()) {
                    /* encrypt md5 del ID de Inscripcion con el ID del estudiante*/
                    $codigo_verificacion = md5($idInscripcion.$id);

                    Yii::import('ext.qrcode.QRCode');

                    $nombre_pdf = 'Boletin_' . $datos_header[0]['id'] . '_' . $datos_header[0]['periodo_id'];
                    $mpdf = new mpdf('', 'A4', 0, '', 15, 15, 75, 100);

                    $url =Yii::app()->getBaseUrl(true).'/verificacion/boletinDigital/verificacion/id/'.$codigo_verificacion;
                    $urlVerificacion =Yii::app()->getBaseUrl(true).'/verificacion/boletinDigital/';
                    $code = new QRCode($url);
                    $direccion_qr = Yii::app()->basePath.'/../public/downloads/qr/boletines/'.'Boletin_' . $datos_header[0]['id'] . '_' . $datos_header[0]['periodo_id'].'.png';
                    $code->create($direccion_qr);
                    $this->registrarDescargaBoletin($idInscripcion,$codigo_verificacion);

                    $header = $this->renderPartial('_headerBoleta', array('datos_header' => $datos_header[0],'nombreQr'=>$nombre_pdf.'.png'), true);
                    $body = $this->renderPartial('_bodyBoleta', array('datosCalificaciones' => $datosCalificaciones,'nivel'=>$nivel,'codigo_verificacion'=>$codigo_verificacion,'url'=>$url), true);
                    $footer = $this->renderPartial('_footerBoleta', array('codigo_verificacion'=>$codigo_verificacion,'url'=>$urlVerificacion), true);
                    $mpdf->SetFont('sans-serif');
                    $mpdf->SetHTMLHeader($header);
                    $mpdf->WriteHTML($body);
                    $mpdf->SetHTMLFooter($footer);

                    $mpdf->Output($nombre_pdf . '.pdf', 'D');

                    $this->registerLog('REPORTES', $modulo . 'PDF', 'EXITOSO', 'Descarga el boletin del estudiante con el nombre: ' . $datos_header[0]['nombres'] . ' del plantel ' . $datos_header[0]['nombre_plantel'] . ' del periodo escolar ' . $periodo_actual_id, $ip, $usuario_id, $username);
                }
            }else {
                throw new CHttpException(404, 'No se ha encontrado la boleta del estudiante que ha solicitado. Recargue la página e intentelo de nuevo.');
            }

        } else {
            throw new CHttpException(404, 'No se ha encontrado el recurso solicitado. Recargue la página e intentelo de nuevo.');
        }
    }
    public function actionReporte() {
        if (isset($_GET['id'])) {
            $idEstudiante = base64_decode($_GET['id']);
            if (is_numeric($idEstudiante)) {
                //if (isset($idEstudiante)) {
                $modelRepresentante = '';
                $estudiante = Estudiante::model()->findByPk($idEstudiante);
                if ($estudiante->representante_id) {
                    if (is_numeric($estudiante->representante_id)) {
                        $modelRepresentante = $this->loadModelRepresentante($estudiante->representante_id);
                        if ($estudiante) {
                            $salida = $estudiante->nombres . '_' . $estudiante->apellidos . '_' . $estudiante->documento_identidad . '.pdf';
                            $salida = str_replace(' ', '_', $salida);
                            $mPDF = Yii::app()->ePdf->mpdf();
                            $mPDF->WriteHTML($this->renderPartial('_pdfHeader', array(), true));
                            $mPDF->WriteHTML($this->renderPartial('/consultar/reporte', array('model' => $estudiante, 'modelRepresentante' => $modelRepresentante), true));
                            $mPDF->Output($salida, EYiiPdf::OUTPUT_TO_DOWNLOAD);
                        }
                    }
                } else {
                    $this->redirect(array('estudiante/index'));
                }
            } else {
                throw new CHttpException(404, "Recurso no encontrado.");
            }
        } else {
            throw new CHttpException(404, "Recurso no encontrado.");
        }
    }
    public function registrarDescargaBoletin($inscripcion_id,$codigo_verificacion) {

        $respuesta = false;
        if(isset($inscripcion_id) and $inscripcion_id != '' and isset($codigo_verificacion)  and  $codigo_verificacion!= ''){


            $modelBoletin= BoletinDigital::model()->findByAttributes(array('codigo_verificacion'=>$codigo_verificacion));

            if($modelBoletin){
                $respuesta = true;
            }else{

                $model = new BoletinDigital;
                $model->inscripcion_id = $inscripcion_id;
                $model->codigo_verificacion = $codigo_verificacion;
                $model->usuario_ini_id = Yii::app()->user->id;
                $model->fecha_ini = date('Y-m-d H:i:s');
                $model->estatus = 'A';


                if($model->save()){
                    $respuesta = true;
                }
            }

        }

        return $respuesta;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Estudiante the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Estudiante::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadModelRepresentante($id) {
        $model = Representante::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Estudiante $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'estudiante-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function loadModelDatosAntropometrico($idEstudiante) {
//        Representante::model()->findByAttributes(array('documento_identidad' => $documento_identidad_repre, 'tdocumento_identidad' => $tdocumento_identidad_repre));
        $model = DatosAntropometricos::model()->findByAttributes(array('estudiante_id' => $idEstudiante));
        if ($model === null)
            $model = new DatosAntropometricos ();
        return $model;
    }

}
