<?php

class ConstanciaController extends Controller {
    public $defaultAction = 'buscar';
    CONST PERIODO_ESCOLAR_ID = 15;
    CONST PERIODO_ESCOLAR = '2014-2015';

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    static $_permissionControl = array(
        'read' => 'Consulta de Constancias',
        'write' => 'Gestion de Constancias',
        'label' => 'Módulo de Constancias'
    );

    /**
     * @Return array filtros de acción
     */

    /**
     * Especifica las reglas de control de acceso.
     * Este método es utilizado por el filtro 'AccessControl'.
     * Reglas de control de acceso a una matriz @ return
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'activar', 'delete', 'buscar', 'prueba'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    /**
     *
     * @author Richard Massri, acción Modificada por Jonathan Huaman.
     * Función que permite Realizar o Generar las Diferentes Constancias Correspondientes, sea el caso de Constancia de Estudio o Constancia de Promoción.
     */
    public function actionBuscar() {
        $error = 4;
        $tipoConstancia = '';
        $fecha_expedicion = date('d-m-Y');
        $cedula = '';
        $cedulaEscolar = '';
        $nacionalidad = '';
        $pasaporte = '';
        $identificacion = '';
        $model = new Constancia();
        Yii::import('ext.qrcode.QRCode');
        if ($this->hasPost('cedula') || $this->hasPost('pasaporte') || $this->hasPost('cedulaEscolar') || $this->hasPost('tipoConstancia') || $this->hasPost('nacionalidad')) {
            $tipoConstancia=$this->getPost('tipoConstancia');
            $cedula=$this->getPost('cedula');
            $pasaporte=$this->getPost('pasaporte');
            $cedulaEscolar=$this->getPost('cedulaEscolar');
            $nacionalidad=$this->getPost('nacionalidad');
            if( is_numeric($cedula) AND $cedula>0){
                $identificacion=$cedula;
            }
            else {
                if(is_numeric($cedulaEscolar) AND $cedulaEscolar>0){
                    $identificacion=$cedulaEscolar;
                }
                else {
                    if(isset($pasaporte) AND strlen($pasaporte)>0){
                        $identificacion=$pasaporte;
                    }
                }
            }
            //Se Genera Constancia de Estudio, Código de Richard.
            if($tipoConstancia==1)
            {

                $resultado = Constancia::model()->buscarEstudiante($identificacion, $nacionalidad, self::PERIODO_ESCOLAR_ID);
                $verificarConstancion=$model->verificarQrCedula($identificacion,$tipoConstancia);
                // var_dump($resultado); die();
                if ($resultado){

                    if(empty($verificarConstancion)){

                        //$id_estudiante = $resultado['estudiante_id'];
                        $fecha_nacimiento = $resultado['fecha_nacimiento'];
                        $implode=  implode("", $resultado);
                        $codigo_qr= md5($implode);
                        $url=Yii::app()->getBaseUrl(true)."/verificacion/constanciaVerificar/index/verificacion/".$codigo_qr.'/r/1';
                        $code = new QRCode($url);
                        $code->create(Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $cedula . '.png');
                        $command = 'chmod 777 -R . ' . Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $cedula . '.png';
                        exec($command);
                        $model->codigo_qr=$codigo_qr;
                        $model->usuario_act_id=Yii::app()->user->id;
                        $model->inscripcion_estudiante_id=$resultado['id_inscripcion'];
                        $model->nombre_estudiante=$resultado['nombre_estudiante'];
                        $model->apellido_estudiante=$resultado['apellido_estudiante'];
                        $model->nombre_plantel=$resultado['nombre_plantel'];
                        $model->codigo_estadistico=$resultado['codigo_estadistico'];
                        $model->nombre_seccion=$resultado['nombre_seccion'];
                        $model->nombre_grado=$resultado['nombre_grado'];
                        $model->documento_identidad=$resultado['documento_identidad'];
                        $model->nombre_estado=$resultado['estado'];
                        $model->nombre_capital=$resultado['capital'];
                        $model->nombre_nivel=$resultado['nivel'];
                        $model->fecha_nacimiento=$resultado['fecha_nacimiento'];
                        $model->url=$url;
                        $model->cedula_escolar=$cedula;
                        $model->tipo_constancia_id=1;
                        $model->periodo_escolar=self::PERIODO_ESCOLAR;
                        //var_dump($model); die();
                        $model->save();
                        $mPDF = Yii::app()->ePdf->mpdf('', 'A4', 0, '', 15, 15, 16, 16, 9, 9, 'M');
                        $mPDF->WriteHTML($this->renderPartial('_pdfHeader', array(), true));
                        $mPDF->setHTMLFooter($this->renderPartial('_pdfFooterConstancia', array('codigo'=>$codigo_qr,'enlace'=>Yii::app()->getBaseUrl(true)."/verificacion/constanciaVerificar/"), true));
                        $mPDF->WriteHTML($this->renderPartial('view_pdf_estudiante', array('resultado'=>$resultado,'periodo_escolar'=>self::PERIODO_ESCOLAR,'fecha_expedicion'=>$fecha_expedicion,'cedula' => $cedula, 'url' => $url, 'nacionalidad' => $nacionalidad, 'fecha_nacimiento' => $fecha_nacimiento,'periodo_escolar'=>self::PERIODO_ESCOLAR), true, false));
                        $mPDF->Output('Constancia_Estudio_' .$identificacion.'_'.self::PERIODO_ESCOLAR.'.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
                    }else{
                        $id= $verificarConstancion['id'];
                        $modelConstancia= Constancia::model()->findByPk($id);
                        $fecha_nacimiento = $resultado['fecha_nacimiento'];
                        $implode=  implode("", $resultado);
                        $codigo_qr= md5($implode);
                        $url=Yii::app()->getBaseUrl(true)."/verificacion/constanciaVerificar/index/verificacion/".$codigo_qr."/r/1";
                        $code = new QRCode($url);
                        $code->create(Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $cedula . '.png');
                        $command = 'chmod 777 -R . ' . Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $cedula . '.png';
                        exec($command);
                        $modelConstancia->codigo_qr=$codigo_qr;
                        $modelConstancia->usuario_act_id=Yii::app()->user->id;
                        $modelConstancia->url=$url;
                        $modelConstancia->tipo_constancia_id=1;
                        $modelConstancia->save();

                        //$resultado = Constancia::model()->buscarEstudiante($cedula, $nacionalidad);
                        $url=$verificarConstancion['url'];
                        $mPDF = Yii::app()->ePdf->mpdf('', 'A4', 0, '', 15, 15, 16, 16, 9, 9, 'M');
                        $mPDF->setHTMLFooter($this->renderPartial('_pdfFooterConstancia', array('codigo'=>$codigo_qr,'enlace'=>Yii::app()->getBaseUrl(true)."/verificacion/constanciaVerificar/"), true));
                        $mPDF->WriteHTML($this->renderPartial('_pdfHeader', array(), true));
                        $mPDF->WriteHTML($this->renderPartial('view_pdf_estudiante', array('tipoConstancia'=>$tipoConstancia,'resultado'=>$resultado,'periodo_escolar'=>self::PERIODO_ESCOLAR,'fecha_expedicion'=>$fecha_expedicion,'cedula' => $cedula, 'url' => $url, 'nacionalidad' => $nacionalidad, 'fecha_nacimiento' => $fecha_nacimiento,'periodo_escolar'=>self::PERIODO_ESCOLAR), true, false));
                        $mPDF->Output('Constancia_Estudio_' .$identificacion.'_'.self::PERIODO_ESCOLAR.'.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
                    }
                } else {
                    $error = 1;
                }

            }
            else{
                /**
                 *                  Código para Generar las Respectivas Constancias de Promoción de 3er Nivel o en su defecto, 6to Grado de Cualquier Estudiante Registrado en Sistema siempre y Cuando Existan sus Registros.
                 */
                if($tipoConstancia==2 || $tipoConstancia==3)
                {

                    // Se Obtiene el Tipo de Documento de Identidad Correspondiente, Sea cedula, pasaporte o cédula Escolar.
                    if(isset($_POST['cedula']) && $_POST['cedula']!="")
                    {
                        $documento_identidad=$_POST['cedula'];
                        $documento="CEDULA";
                    }
                    if(isset($_POST['pasaporte']) && $_POST['pasaporte']!="")
                    {
                        $documento_identidad=$_POST['pasaporte'];
                        $documento="PASAPORTE";
                    }
                    if(isset($_POST['cedulaEscolar']) && $_POST['cedulaEscolar']!="")
                    {
                        $documento_identidad=$_POST['cedulaEscolar'];
                        $documento="CEDULAESCOLAR";
                    }

                    //Si Existe Nacionalidad se Obtiene.
                    if(isset($_POST['nacionalidad']))
                    {
                        $nacionalidad= $_POST['nacionalidad'];
                    }
                    else
                    {
                        $nacionalidad= "";
                    }

                    //Se Define el Grado a Buscar la Constancia de Promocion. si es 2 se Indica que es 3ER NIVEL.
                    if($tipoConstancia==2)
                        $grado=11; //CONSTANTE, 3ER NIVEL ES EL CODIGO 11 EN CALIDAD.
                    //Si es 3 se Indica que es 6TO GRADO.
                    if($tipoConstancia==3)
                        $grado=18; //CONSTANTE 6TO GRADO ES EL CODIGO 18 EN CALIDAD.

                    //Se Genera un Array donde se pasan todos los parametros necesarios a la función que busca los datos del Estudiante para Generar la Constancia.
                    $datos= array($documento_identidad,$nacionalidad==''?'NULL':$nacionalidad,$documento,$grado,self::PERIODO_ESCOLAR_ID);

                    //Se Obtienen todos los Datos del Estudiante para generar la Constancia en Cuestión.
                    $datosConstancia = Constancia::model()->buscarEstudiantePromocion($datos);

                    if(!empty($datosConstancia))
                    {

                        if(isset($datosConstancia['estado_nac']) AND !is_null($datosConstancia['estado_nac']) AND isset($datosConstancia['sexo']) AND !is_null($datosConstancia['sexo']) AND isset($datosConstancia['fecha_nacimiento']) AND !is_null($datosConstancia['fecha_nacimiento']) )
                        {

                            if(isset($datosConstancia['estado_plantel']) AND !is_null($datosConstancia['estado_plantel']) AND isset($datosConstancia['municipio_plantel']) AND !is_null($datosConstancia['municipio_plantel']) AND isset($datosConstancia['parroquia_plantel']) AND !is_null($datosConstancia['parroquia_plantel']) AND isset($datosConstancia['direccion_plantel']) AND !is_null($datosConstancia['direccion_plantel']) )
                            {
                                //echo "PUDE ENTRAR AQUI version dos"; die();

                                $cedulaEscolar='';
                                $cedula='';
                                if(strlen($documento_identidad)>9)
                                {
                                    $cedulaEscolar=$documento_identidad;
                                    $ExisteConstancia= Constancia::model()->find("cedula_escolar= '$cedulaEscolar'");
                                }
                                else
                                {
                                    $cedula=$documento_identidad;
                                    $ExisteConstancia= Constancia::model()->find("documento_identidad='$cedula'");
                                }

                                if(empty($ExisteConstancia))
                                {

                                    $model->usuario_ini_id= Yii::app()->user->id;
                                    $model->estatus='A';
                                    $model->tipo_constancia_id=$tipoConstancia;
                                    $model->nombre_estudiante=$datosConstancia['nombres'];
                                    $model->apellido_estudiante=$datosConstancia['apellidos'];
                                    $model->nombre_plantel=$datosConstancia['plantel'];
                                    $model->estado_nac=$datosConstancia['estado_nac'];
                                    $model->estado_plantel=$datosConstancia['estado_plantel'];
                                    $model->zona_educativa=$datosConstancia['zona_educativa'];
                                    $model->municipio_plantel=$datosConstancia['municipio_plantel'];
                                    $model->parroquia_plantel=$datosConstancia['parroquia_plantel'];
                                    $model->direccion_plantel=$datosConstancia['direccion_plantel'];
                                    $model->fecha_nacimiento=$datosConstancia['fecha_nacimiento'];
                                    $model->cedula_escolar=$datosConstancia['cedula_escolar'];
                                    $model->documento_identidad=$datosConstancia['documento_identidad'];
                                    $model->periodo_escolar=$datosConstancia['periodo_escolar'];
                                    $model->inscripcion_estudiante_id=$datosConstancia['inscripcion_estudiante_id'];

                                    $datosQr[] = $model->cedula_escolar;
                                    $datosQr[] = $model->documento_identidad;
                                    $datosQr[] = $model->tipo_constancia_id;
                                    $datosQr[] = $model->nombre_estudiante;
                                    $datosQr[] = $model->apellido_estudiante;
                                    $datosQr[] = $model->fecha_nacimiento;
                                    $datosQr[] = $model->periodo_escolar;
                                    $implode=  implode("", $datosQr);
                                    $codigo_qr= md5($implode);
                                    $url=Yii::app()->getBaseUrl(true)."/verificacion/constanciaVerificar/index/verificacion/".$codigo_qr."/r/1";
                                    $code = new QRCode($url);
                                    $code->create(Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $datosConstancia['cedula_escolar'] .'_'.$tipoConstancia.'.png');
                                    $command = 'chmod 777 -R . ' . Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $datosConstancia['cedula_escolar'] .'_'.$tipoConstancia . '.png';
                                    exec($command);
                                    $model->codigo_qr=$codigo_qr;
                                    $model->url=$url;
                                    $model->save();
                                }
                                else {
                                    $id= $ExisteConstancia->id;
                                    $modelConstancia= Constancia::model()->findByPk($id);
                                    /*$modelConstancia->tipo_constancia_id=$tipoConstancia;*/
                                    $modelConstancia->fecha_act= date('Y-m-d H:i:s');
                                    $modelConstancia->usuario_act_id= Yii::app()->user->id;
                                    /*$modelConstancia->documento_identidad=$datosConstancia['documento_identidad'];
                                    $modelConstancia->cedula_escolar=$datosConstancia['cedula_escolar'];
                                    $modelConstancia->url=NULL;
                                    $modelConstancia->codigo_qr=NULL;*/


                                    $datosQr[] = $modelConstancia->cedula_escolar;
                                    $datosQr[] = $modelConstancia->documento_identidad;
                                    $datosQr[] = $modelConstancia->tipo_constancia_id;
                                    $datosQr[] = $modelConstancia->nombre_estudiante;
                                    $datosQr[] = $modelConstancia->apellido_estudiante;
                                    $datosQr[] = $modelConstancia->fecha_nacimiento;
                                    $datosQr[] = $modelConstancia->periodo_escolar;
                                    $implode=  implode("", $datosQr);
                                    $codigo_qr= md5($implode);
                                    $url=Yii::app()->getBaseUrl(true)."/verificacion/constanciaVerificar/index/verificacion/".$codigo_qr."/r/1";
                                    $code = new QRCode($url);
                                    $code->create(Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $datosConstancia['cedula_escolar'] .'_'.$tipoConstancia.'.png');
                                    $command = 'chmod 777 -R . ' . Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $datosConstancia['cedula_escolar'] .'_'.$tipoConstancia . '.png';
                                    exec($command);
                                    $modelConstancia->codigo_qr=$codigo_qr;
                                    $modelConstancia->url=$url;
                                    $modelConstancia->save();
                                }

                                //$resultado = Constancia::model()->buscarEstudiante($cedula, $nacionalidad);
                                $fecha=date('d-m-Y');
                                $fecha= explode('-', $fecha);
                                $meses= array("01"=>'ENERO',"02"=>'FEBRERO',"03"=>'MARZO',"04"=>'ABRIL',"05"=>"MAYO","06"=>"JUNIO","07"=>"JULIO","08"=>"AGOSTO","09"=>"SEPTIEMBRE","10"=>"OCTUBRE","11"=>"NOVIEMBRE","12"=>"DICIEMBRE");
                                $mPDF = Yii::app()->ePdf->mpdf('', 'A4', 0, '', 15, 15, 16, 16, 9, 9, 'M');
                                $mPDF->setHTMLFooter($this->renderPartial('_pdfFooterConstanciaPromocion', array('firma'=>'DGRCA/DGEI/HMVC/JCR/AMO/hmvc','codigo'=>$codigo_qr), true));
                                $mPDF->WriteHTML($this->renderPartial('_pdfHeaderConstanciaPromocion', array(), true));
                                $mPDF->WriteHTML($this->renderPartial('_pdfConstanciaPromocion', array('url'=>$url,'','datosConstancia'=>$datosConstancia,'fecha'=>$fecha,'meses'=>$meses,'tipoConstancia'=>$tipoConstancia), true, false));
                                $mPDF->Output($datosConstancia['nombres']. '_'.$datosConstancia['apellidos'] . '.pdf','D');
                            }
                            else
                            {
                                Yii::app()->user->setFlash('error', 'Estimado usuario, para generar la Constancia de Promoción requerimos que actualice los datos de ubicación del <strong>Plantel</strong>.');
                            }




                        }
                        else
                        {
                                Yii::app()->user->setFlash('error', 'Estimado usuario, para generar la Constancia de Promoción requerimos que actualice los datos de ubicación del <strong>Estudiante</strong>.');
                        }

                    }
                    else
                    {
                        Yii::app()->user->setFlash('error', 'Estimado usuario, el estudiante no cumple los requisitos para generar la Constancia de Promoción en este año escolar. ');
                    }
                }
            }
        }
        $this->render('_form', array('model' => $model, 'error' => $error));
    }

    public function actionVerificar() {
        if (isset($_REQUEST['verificacion'])) {
            $codigo_verificacion = $_REQUEST['verificacion'];
            $verificarQr = TituloDigital::model()->verificarQr($codigo_verificacion);
            if ($verificarQr) {
                $this->render('_verificacion', array('verificarQr' => $verificarQr));
            } else {
                throw new CHttpException(404, 'El Requerimiento no Existe');
            }
        } else {
            throw new CHttpException(404, 'El Requerimiento no Existe');
        }
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Constancia;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Constancia'])) {
            $model->attributes = $_POST['Constancia'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Constancia'])) {
            $model->attributes = $_POST['Constancia'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $groupId = Yii::app()->user->group;
        $model = new Constancia('search');
        if (isset($_GET['Constancia']))
            $model->attributes = $_GET['Constancia'];
        $usuarioId = Yii::app()->user->id;
        $dataProvider = new CActiveDataProvider('Constancia');
        $this->render('admin', array(
            'model' => $model,
            'groupId' => $groupId,
            'usuarioId' => $usuarioId,
            'dataProvider' => $dataProvider,
        ));
    }

    public function loadModel($id) {
        $model = Constancia::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    /**
     * Performs the AJAX validation.
     * @param Constancia $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'constancia-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionPrueba() {
        Yii::import('ext.qrcode.QRCode');
        $code = new QRCode("data to encode");
        $code->create(Yii::app()->basePath . '/qr/file.png');
    }

    public function columnaAcciones($datas) {
        $id = $datas["id"];
        $id2 = $datas["estatus"];
        $id = base64_encode($id);
        if ($id2 == "A") {
            $columna = CHtml::link("", "#", array("class" => "fa fa-search", "title" => "Buscar Esta Constancia", "onClick" => "VentanaDialog('$id','/catalogo/mencion/view','Mención','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-pencil green", "title" => "Modificar Constancia", "onClick" => "VentanaDialog('$id','/catalogo/mencion/update','Modificar Mención','update')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa icon-trash red", "title" => "Eliminar Constancia", "onClick" => "VentanaDialog('$id','/catalogo/mencion/borrar','Eliminar Mención','borrar')")) . '&nbsp;&nbsp;';
        } else {
            $columna = CHtml::link("", "#", array("class" => "fa fa-search", "title" => "Buscar Mencion", "onClick" => "VentanaDialog('$id','/catalogo/mencion/view','Mención','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-check", "title" => "Activar Mencion", "onClick" => "VentanaDialog('$id','/catalogo/mencion/activar','activar Mención','activar')")) . '&nbsp;&nbsp;';
        }
        return $columna;
    }

}
