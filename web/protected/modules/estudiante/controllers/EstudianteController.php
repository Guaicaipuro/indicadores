<?php

class EstudianteController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction = 'index';
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Consulta de Estudiante',
        //  'write' => 'Consulta de Planteles', // no lleva etiqueta write
        'label' => 'Consulta de Estudiante'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('write'),
            ),
            /* array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions'=>array('admin','delete'),
              'users'=>array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Estudiante;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Estudiante'])) {
            $model->attributes = $_POST['Estudiante'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Estudiante'])) {
            $model->attributes = $_POST['Estudiante'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new Estudiante('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Estudiante']))
            $model->attributes = $_GET['Estudiante'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Estudiante('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Estudiante']))
            $model->attributes = $_GET['Estudiante'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }
    
    
    public function columnaAcciones($data)
    /*
     * Botones del accion (crear, consultar)
     */ {
        $id = $data["id"];
        $estatus = $data["estatus"];
        if (($estatus == 'A') || ($estatus == '')) {
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarNivel($id)", "title" => "Consultar este nivel")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green", "onClick" => "modificarNivel($id)", "title" => "Modificar nivel")) . '&nbsp;&nbsp;';
            //$columna .= CHtml::link("", "", array("class" => "icon-trash red remove-data", "onClick" => "eliminarNivel($id)", "title" => "Inactivar nivel")) . '&nbsp;&nbsp;';
        } else if ($estatus == 'E') {
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarNivel($id)", "title" => "Consultar este nivel")) . '&nbsp;&nbsp;';
            //$columna .= CHtml::link("", "", array('onClick' => "activarNivel($id)", "class" => "fa icon-ok green", "title" => "Activar este nivel")) . '&nbsp;&nbsp;';
        }

        return $columna;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Estudiante the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Estudiante::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Estudiante $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'estudiante-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
