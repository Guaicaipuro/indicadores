<?php
/* @var $this ZonaEducativaController */
/* @var $model ZonaEducativa */

$this->breadcrumbs=array(
	'Zona Educativas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ZonaEducativa', 'url'=>array('index')),
	array('label'=>'Create ZonaEducativa', 'url'=>array('create')),
	array('label'=>'View ZonaEducativa', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ZonaEducativa', 'url'=>array('admin')),
);
?>

<h1>Update ZonaEducativa <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>