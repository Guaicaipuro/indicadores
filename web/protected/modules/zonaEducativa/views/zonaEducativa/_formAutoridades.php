<?php
/*
 * Modificación de las Autoridades por cada Zona Educativa
 *
 */
?>

<div class="form" id="_formAutoridades">


    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'plantelAutoridad-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>
    <?php //$form->hiddenField($usuario, '');  ?>
    <div class="tab-pane active" id="autoridades">

        <div id="autor" class="widget-box">


            <div id="resultadoZonaAutoridades">
            </div>
            <div id="validaciones"> </div>

            <div class="widget-header">
                <h5>Autoridad de la Zona Educativa</h5>
                <div class="widget-toolbar">
                    <a  href="#" data-action="collapse">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div id="autoridadesZona" class="widget-body" >
                <div class="widget-body-inner" >
                    <div class="widget-main form">


                        <div class="row">
                            <?php echo '<input type="hidden" id="zona_id" value=' . $zona_id . ' name="zona_id"/>'; ?>
                            <div class="col-md-12" id ="listaAutoridad">
                                <div class="col-md-5">
                                    <div class="col-md-12"><label for="Plantel_cedula">Cedula<span class="required">*</span></label></div>
                                    <?php echo '<input type="text" data-toggle="tooltip" data-placement="bottom" placeholder="V-0000000" title="Ej: V-99999999 ó E-99999999" id="cedula"  style="padding:3px 4px" maxlength="10" size="10" class="span-6" name="cedula" onkeypress = "return CedulaFormat(this, event)" />'; ?>
                                    <button  id = "btnBuscarCedula"  class="btn btn-info btn-xs" type="button" style="padding-top: 2px; padding-bottom: 2px;" >
                                        <i class="icon-search"></i>
                                        Buscar
                                    </button>

                                </div>

                                <?php
//if (isset($dataProviderAutoridades) && $dataProviderAutoridades !== array()) {
                                $this->widget(
                                        'zii.widgets.grid.CGridView', array(
                                    'id' => 'autoridades-grid',
                                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                    // 40px is the height of the main navigation at bootstrap
                                    'dataProvider' => $dataProviderAutoridades,
                                    'summaryText' => false,
                                     'afterAjaxUpdate' => " function(){
                                        $('#ZonaEducativa_nombre').bind('keyup blur', function () {
                                             keyText(this, true, true);
                                             makeUpper(this);
                                        });
                                        
                                    }

                                ",
                                    'columns' => array(
                                        array(
                                            'name' => 'cargo',
                                            'type' => 'raw',
                                            'header' => '<center><b>Cargo</b></center>'
                                        ),
                                        array(
                                            'name' => 'nombre',
                                            'type' => 'raw',
                                            'header' => '<center><b>Nombre y Apellido</b></center>',
                                            'filter' => CHtml::textField('ZonaEducativa[nombre]'),
                                        ),
                                        array(
                                            'name' => 'cedula',
                                            'type' => 'raw',
                                            'header' => '<center><b>Cédula</b></center>'
                                        ),
                                        array(
                                            'name' => 'correo',
                                            'type' => 'raw',
                                            'header' => '<center><b>Correo Electrónico</b></center>'
                                        ),
                                        array(
                                            'name' => 'telefono_celular',
                                            'type' => 'raw',
                                            'header' => '<center><b>Teléfono</b></center>'
                                        ),
                                        array(
                                            'name' => 'telefono_fijo',
                                            'type' => 'raw',
                                            'header' => '<center><b>Teléfono Fijo</b></center>'
                                        ),
                                        array(
                                            'header' => 'Acciones',
                                            'name' => 'boton',
                                            'type' => 'raw'
                                        ),
                                    ),
                                    'pager' => array(
                                        'header' => '',
                                        'htmlOptions' => array('class' => 'pagination'),
                                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                    ),
                                        )
                                );
//}
                                ?>
                            </div>
                        </div>
                        <br>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div id="botones" class="row">
        <div class="col-md-6">
            <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("../zonaEducativa"); ?>" class="btn btn-danger">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
        </div>
    </div>

</div>
<?php
$this->endWidget();
?>
<?php Yii::app()->getSession()->add('usuario', $usuario); ?>
<div id = "agregarAutoridad" class="hide">
    <?php $this->renderPartial('_formAgregarAutoridad', array('usuario' => $usuario, 'zona_id' => $zona_id)); ?>

</div>



<div id = "dialog_cargo" class="hide">
    <?php $this->renderPartial('_formCargo', array('usuario' => $usuario, 'zona_id' => $zona_id)); ?>

</div>

<div class="hide" id="datosAutoridad"> </div>
<div id = "dialog_success" class="hide">
    <div class="successDialogBox bigger-110">
        <p class="bigger-110">

        </p>
    </div>

    <div id = "dialog_error" class="hide">
        <div class="alertDialogBox bigger-110">
            <p class="bigger-110">

            </p>
        </div>
    </div>

    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
    <div id="dialogPantalla" class="hide"></div>
    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/zonaEducativa/zonaEducativa.js', CClientScript::POS_END);
    ?>


    <script >
        $('#cedula').tooltip({
            show: {
                effect: "slideDown",
                delay: 250,
            }
        });

        $("#btnBuscarCedula").click(function() {
            var cedula = $("#cedula").val();

            var tam = cedula.length;
            var mensaje = "Estimado usuario, el formato de la Cedula de Identidad no es el correcto";
            if (tam > 2 && tam <= 10) {
                buscarCedulaAutoridad(cedula);
            }
            else {
                dialogo_error(mensaje);
            }
        });
        $(".change-data").unbind('click');
        $(".change-data").click(function() {

            usuario_id = $(this).attr('data-id');
            zona_id = $("#zona_id").val();
            data = {
                usuario_id: usuario_id,
                zona_id: zona_id
            };
            executeAjax('datosAutoridad', 'buscarAutoridad', data, true, true, 'get', '');

            $("#datosAutoridad").removeClass('hide').dialog({
                modal: true,
                width: '800px',
                draggable: false,
                resizable: false,
                position: ['center', 50],
                title: "<div class='widget-header'><h4 class='smaller blue'><i class='icon-user'></i> Datos de la Autoridad de la Zona Educativa</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                            $("#datosAutoridad").html('').addClass('hide');
                        }
                    },
                    {
                        html: "Actualizar Datos <i class='icon-save bigger-110'></i>",
                        "class": "btn btn-primary btn-xs",
                        id: "btnActualizarDatosAutoridadZona",
                        click: function() {
                            var divResult = "#resultado-cambio-datos";
                            var divResultAjaxCallback = "_formAutoridades";
                            var mensaje = "";

                            var email = $("#email").val();
                            var emailBackup = $("#emailBackup").val();

                            var telf_fijo = $("#telf_fijo").val();
                            var telf_fijoBackup = $("#telf_fijoBackup").val();

                            var telf_cel = $("#telf_cel").val();
                            var telf_celBackup = $("#telf_celBackup").val();

                            //                        var cargo_id = $("#cargo_id_autoridad option:selected").val();
                            //                        var cargo_idBackup = $("#cargo_idBackup").val();

                            var usuario_id = $("#usuario_id").val();
                            var zona_id = $("#zona_id").val();
                            if (zona_id.length > 0 && usuario_id.length > 0) {
                                $("#resultado-cambio-datos").html('');
                                //                            if (emailBackup != email || telf_fijo != telf_fijoBackup || telf_cel != telf_celBackup || cargo_id != cargo_idBackup) {
                                if (emailBackup != email || telf_fijo != telf_fijoBackup || telf_cel != telf_celBackup) {
                                    if (telf_cel != telf_celBackup && (!isValidPhone(telf_cel, 'movil') || telf_cel.length != 11)) {
                                        mensaje = "El teléfono celular no posee el formato correcto <br>";
                                        $("#telf_cel").val(telf_celBackup);

                                    }
                                    if (telf_fijo != telf_fijoBackup && (!isValidPhone(telf_fijo, 'fijo') || telf_fijo.length != 11)) {
                                        mensaje = mensaje + "El teléfono fijo no posee el formato correcto <br>";
                                        $("#telf_fijo").val(telf_fijoBackup);
                                    }
                                    if (emailBackup != email && (!isValidEmail(email) || email.length < 3)) {
                                        mensaje = mensaje + "El correo electrónico no posee el formato correcto <br>";
                                        $("#email").val(emailBackup);
                                    }
                                    //                                if (cargo_id != cargo_idBackup && (!$.isNumeric(cargo_id) || cargo_id == "")) {
                                    //                                    mensaje = mensaje + "El cargo seleccionado es invalido <br>";
                                    //                                    $("#cargo_id").val(cargo_idBackup);
                                    //                                }

                                    if (mensaje == "") {
                                        var datos = $("#zonaAutoridades-form").serialize();
                                        var datosAjaxCallback = {zona_id: zona_id, usuario_id: usuario_id};
                                        var urlDir = "actualizarDatosAutoridad";
                                        var urlDirAjaxCallback = "getAutoridadZona";
                                        var conEfecto = true;
                                        var showHTML = true;
                                        var method = "POST";
                                        var callback = function() {
                                            $("#emailBackup").val($("#email").val());
                                            executeAjax(divResultAjaxCallback, urlDirAjaxCallback, datosAjaxCallback, conEfecto, showHTML, method);
                                        };

                                        executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                                    }
                                    else {
                                        displayDialogBox(divResult, 'error', mensaje);
                                    }
                                }

                            }
                            else {

                                displayDialogBox(divResult, 'error', 'No se ha podido identificar al usuario al que desea actualizar los datos. Recargue la página e intenetelo de nuevo.');

                            }

                        }
                    },
                    {
                        html: "Resetear Clave <i class='icon-key bigger-110'></i>",
                        "class": "btn btn-success btn-xs",
                        id: "btnResetClaveAutoridadZona",
                        click: function() {

                            //Cambiar el Correo
                            var divResult = "resultado-cambio-datos";

                            var email = $("#email").val();
                            var emailBackup = $("#emailBackup").val();
                            var usuario_id = $("#usuario_id").val();

                            if (usuario_id.length > 0) {

                                var datos = {id: usuario_id, zona_id: zona_id, email: email};
                                var urlDir = "/control/autoridadesZona/resetearClave";
                                var conEfecto = true;
                                var showHTML = true;
                                var method = "POST";
                                var callback = null;

                                executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            }
                            else {

                                displayDialogBox(divResult, 'error', 'No se ha podido identificar al usuario al que desea modificar el correo. Recargue la página e intenetelo de nuevo.');

                            }

                        }
                    }
                ]
            });

        });
    </script>