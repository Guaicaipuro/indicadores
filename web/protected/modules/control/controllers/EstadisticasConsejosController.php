<?php

class EstadisticasConsejosController extends Controller
{

    public $defaultAction = 'estadisticasEstados';

    //despues de la declaración de la clase va el siguiente codigo
    public $layout = '//layouts/main';
    static $_permissionControl = array(
        'read' => 'Consulta de Estadisticas de los Consejos Educativos',
        'write' => 'Consulta de Estadisticas de los Consejos Educativos',
        'admin' => 'Consulta de Estadisticas de los Consejos Educativos',
        'label' => 'Consulta de Estadisticas de los Consejos Educativos'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array(
                    'estadisticasProyectos',
                    'estadisticasEstados',
                    'pdfEstadisticasEstados',
                    'pdfEstadisticasProyectos'
                    ),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('estadisticasProyectos','estadisticasEstados'),
                'pbac' => array('write',),
            ),            
            array('allow',
                'actions' => array(
                    'estadisticasProyectos',
                    'estadisticasEstados',
                    'pdfEstadisticasEstados',
                    'pdfEstadisticasProyectos'
                    ),
                'pbac' => array('read',),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionEstadisticasProyectos()
    {
        
        $model = new ConsejoEducativo();
        $estado_id = null;
        $estados = CHtml::listData(CEstado::getData(),'id','nombre');
        
        if(!Yii::app()->user->pbac('control.estadisticasConsejos.admin'))
        {
            $estado_id = Yii::app()->user->estado;
            $estado_nombre = $estados[$estado_id];
        }elseif(isset($_GET['estado']) && !empty($_GET['estado'])){
            $estado_id = $_GET['estado'];
            $estado_nombre = $estados[$estado_id];
        }

        if( !isset($estado_nombre) || is_null($estado_nombre) || empty($estado_nombre))
        {
            $header_nombre = "Entidad Federal";
        }else{
            $header_nombre = "Municipios de {$estado_nombre}";
        }
        $dataProvider = $model ->estadisticasProyectosPorEstados($estado_id);

        $proyectos = CHtml::listData(ProyectoConsejo::model()->findAll(array('order' => 'nombre ASC','condition' => "estatus='A'")), 'id', 'nombre');

        $this->render('estadisticasProyectos',array(
            'model'=>$model,
            'dataProvider' => $dataProvider,
            'estados'=>$estados,
            'proyectos'=>$proyectos,
            'header_nombre'=> $header_nombre
        ) );    
    }

    public function actionPdfEstadisticasProyectos($estado)
    {
        /* SI $estado TIENE ALGUN VALOR, QUIERE DECIR QUE SE HIZO LA BUSQUEDA
        POR UN ESTADO EN ESPECIFICO Y SE DEBEN MOSTRAR SON LOS MUNICIPIOS
        DE ESE ESTADO. DE LO CONTRARIO SE REALIZA LA BUSQUEDA DE TODOS LOS
        ESTADOS DE VENEZUELA */

        if(!empty($estado) && !is_numeric($estado))
        {
            throw new CHttpException(404,'The requested page does not exist.');
        }
        $estados = CHtml::listData(CEstado::getData(),'id','nombre');

        if(!Yii::app()->user->pbac('control.estadisticasConsejos.admin'))
        {
            $estado_id = Yii::app()->user->estado;
            $estado_nombre = $estados[$estado_id];
        }elseif(!empty($estado) && !is_null($estado))
        {
            $estado_id = $estado;
            $estado_nombre = $estados[$estado_id];
        }

        if( !isset($estado_nombre) || is_null($estado_nombre) || empty($estado_nombre))
        {
            $headerCol1 = "Entidad Federal";
        }else{
            $headerCol1 = "Municipios de {$estado_nombre}";
        }

        if(isset(Yii::app()->session['dataProviderProyectosPorEstados']) 
            AND !empty(Yii::app()->session['dataProviderProyectosPorEstados']))
        {
            $model = new ConsejoEducativo;
            $dataProviderEstados = Yii::app()->session['dataProviderProyectosPorEstados'];
            $proyectos = CHtml::listData(ProyectoConsejo::model()->findAll(array('order' => 'nombre ASC','condition' => "estatus='A'")), 'id', 'nombre');

            $total_consejos = $this->totalPorColumna($dataProviderEstados,'total_consejos');
            $con_proyectos = $this->totalPorColumna($dataProviderEstados,'con_proyectos');

            $footer_table = array(
                'total_consejos'=>$total_consejos,
                'con_proyectos'=>$con_proyectos,
            );

            foreach ($proyectos as $id => $nombre) {
                
                $footer_table['c'.$id] =  $this->totalPorColumna($dataProviderEstados,'c'.$id);
            }

            $header = $this->renderPartial('_headerReporteEstadisticasProyectos',
                array(),true);
            
            $body =$this->renderPartial('_bodyReporteEstadisticasProyectos',
                array(
                    'dataProviderEstados'=> $dataProviderEstados,
                    'headerCol1'=> $headerCol1,
                    'proyectos'=>$proyectos,
                    'footer_table'=> $footer_table,
                ),true
            );


            
            $modulo = 'planteles.ConsejoEducativo.PdfEstadisticasProyectos';
            $ip = Yii::app()->request->userHostAddress;
            $username = Yii::app()->user->name;
            $usuario_id = Yii::app()->user->id;

            $fecha_actual = date('d-m-y');

            $name = 'Estadísticas de Consejos Educativos';
            $mpdf = new mpdf('', 'LEGAL-L', 0, '', 15, 15, 55, 30); // Creo un Nuevo Archivo PDF
            $mpdf->setHTMLFooter('<br>' . '<p style="text-align:center;"> {PAGENO} / {nb}</p><p style="text-align:right;">'.$fecha_actual.'</p>'); //Footer del PDF
            $mpdf->SetFont('sans-serif');
            //$header = '<div align="center" style="font-weight: bold; font-size:25en">Estadísticas de Consejos Educativos</div>';
            $mpdf->SetHTMLHeader($header);
            $mpdf->WriteHTML($body); //Escritura del Codigo HTML y PHP
            $mpdf->Output($name . '.pdf', 'D'); //SALIDA DEL PDF, I->Mostrar en Página Web, D-> Descarga Directa
            $this->registerLog('REPORTES', $modulo, 'EXITOSO', 'Generación de un Reporte de las Estadísticas de Consejos Educativos', $ip, $usuario_id, $username);               


        }else{
            throw new CHttpException(404,'The requested page does not exist.');
        }
    }

    public function actionEstadisticasEstados()
    {
        $model = new ConsejoEducativo();
        $estado_id = null;
        $estados = CHtml::listData(CEstado::getData(),'id','nombre');
        
        if(!Yii::app()->user->pbac('control.estadisticasConsejos.admin'))
        {
            $estado_id = Yii::app()->user->estado;
            $estado_nombre = $estados[$estado_id];
        }elseif(isset($_GET['estado']) && !empty($_GET['estado'])){
            $estado_id = $_GET['estado'];
            $estado_nombre = $estados[$estado_id];
        }

        if( !isset($estado_nombre) || is_null($estado_nombre) || empty($estado_nombre))
        {
            $header_nombre = "Entidad Federal";
        }else{
            $header_nombre = "Municipios de {$estado_nombre}";
        }

        $dataProvider = $model->estadisticasPorEstados($estado_id);

        $this->render('estadisticasEstados',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
            'estados'=>$estados,
            'header_nombre' => $header_nombre
        ) );    
    }

    /*FUNCION PARA GENERAR EL PDF DE LAS ESTADISTICAS DE LOS CONSEJOS EDUCATIVOS*/
    public function actionPdfEstadisticasEstados($estado)
    {
         /* SI $estado TIENE ALGUN VALOR, QUIERE DECIR QUE SE HIZO LA BUSQUEDA
        POR UN ESTADO EN ESPECIFICO Y SE DEBEN MOSTRAR SON LOS MUNICIPIOS
        DE ESE ESTADO. DE LO CONTRARIO SE REALIZA LA BUSQUEDA DE TODOS LOS
        ESTADOS DE VENEZUELA */

        if(!empty($estado) && !is_numeric($estado))
        {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        $estados = CHtml::listData(CEstado::getData(),'id','nombre');

        if(!Yii::app()->user->pbac('control.estadisticasConsejos.admin'))
        {
            $estado_id = Yii::app()->user->estado;
            $estado_nombre = $estados[$estado_id];
        }elseif(!empty($estado) && !is_null($estado))
        {
            $estado_id = $estado;
            $estado_nombre = $estados[$estado_id];
        }

        if( !isset($estado_nombre) || is_null($estado_nombre) || empty($estado_nombre))
        {
            $headerCol1 = "Entidad Federal";
        }else{
            $headerCol1 = "Municipios de {$estado_nombre}";
        }

        if(isset(Yii::app()->session['dataProviderEstados']) 
            AND !empty(Yii::app()->session['dataProviderEstados']))
        {
            $model = new ConsejoEducativo;
            $dataProviderEstados = Yii::app()->session['dataProviderEstados'];

            $total_planteles =$this->totalPorColumna($dataProviderEstados,'total_planteles');
            $total_consejos = $this->totalPorColumna($dataProviderEstados,'total_consejos');
            $cant_actualizados = $this->totalPorColumna($dataProviderEstados,'cant_actualizados');
            $cant_desactualizados = $this->totalPorColumna($dataProviderEstados,'cant_desactualizados');
            $con_proyectos = $this->totalPorColumna($dataProviderEstados,'con_proyectos');
            $circuito_educativo = $this->totalPorColumna($dataProviderEstados,'circuito_educativo');
            $comite_economia = $this->totalPorColumna($dataProviderEstados,'comite_economia');

            $footer_table = array(
                'total_planteles'=>$total_planteles,
                'total_consejos'=>$total_consejos,
                'cant_actualizados'=>$cant_actualizados,
                'cant_desactualizados'=>$cant_desactualizados,
                'con_proyectos'=>$con_proyectos,
                'circuito_educativo'=>$circuito_educativo,
                'comite_economia'=>$comite_economia
            );
            
            $header = $this->renderPartial('_headerReporteEstadisticasEstados',
                array(),true
            );

            $body =$this->renderPartial('_bodyReporteEstadisticasEstados',
                array(
                    'dataProviderEstados'=> $dataProviderEstados,
                    'headerCol1'=> $headerCol1,
                    'footer_table'=> $footer_table,
                ),true
            );

            $modulo = 'planteles.ConsejoEducativo.PdfEstadisticasEstados';
            $ip = Yii::app()->request->userHostAddress;
            $username = Yii::app()->user->name;
            $usuario_id = Yii::app()->user->id;

            $fecha_actual = date('d-m-y');

            $name = 'Estadísticas de Consejos Educativos';
            $mpdf = new mpdf('', 'LEGAL-L', 0, '', 15, 15, 55, 30); // Creo un Nuevo Archivo PDF
            $mpdf->setHTMLFooter('<br>' . '<p style="text-align:center;"> {PAGENO} / {nb}</p><p style="text-align:right;">'.$fecha_actual.'</p>'); //Footer del PDF
            $mpdf->SetFont('sans-serif');
            //$header = '<div align="center" style="font-weight: bold; font-size:25en">Estadísticas de Consejos Educativos</div>';
            $mpdf->SetHTMLHeader($header);
            $mpdf->WriteHTML($body); //Escritura del Codigo HTML y PHP
            $mpdf->Output($name . '.pdf', 'D'); //SALIDA DEL PDF, I->Mostrar en Página Web, D-> Descarga Directa
            $this->registerLog('REPORTES', $modulo, 'EXITOSO', 'Generación de un Reporte de las Estadísticas de Consejos Educativos', $ip, $usuario_id, $username);               
        }else{
            throw new CHttpException(404,'The requested page does not exist.');
        }
    }

    /* FUNCION PARA CALCULAR LOS TOTALES( EL FOOTER DEL CGRIDVIEW) DE LAS ESTADISTICAS POR ESTADO*/
    public function totalPorColumna($data,$columna){
        /* $data es lo que se obtiene de $model->estadisticasPorEstado()*/
        /* $columna CONTIENE EL NOMBRE DE LA COLUMNA A LA QUE SE QUIERE OBTENER EL TOTAL*/
        $total_columna = 0;

        foreach ($data->rawData as $key => $value) {
            /* rawData ES EL ARRAY QUE CONTIENE LOS VALORES DE LA CONSULTA*/
        $total_columna += $value[$columna];
        } 
        return $total_columna;
    } 
}