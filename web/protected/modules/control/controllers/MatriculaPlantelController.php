<?php

class MatriculaPlantelController extends Controller {

    public $defaultAction = 'index';
    //despues de la declaración de la clase va el siguiente codigo
    public $layout = '//layouts/main';
    static $_permissionControl = array(
        'read' => 'Consulta de Matriculado Por Plantel',

        'read' => 'Consulta de Matriculado Por Plantel',
        'write' => 'Creación y Modificación de Matriculado Por Plantel',
        'admin' => 'Administración Completa  de Matriculado Por Plantel',
        'label' => 'Módulo de de Matriculado Por Plantel'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('index', 'loadGradoSeccion', 'ValidarCodPlantel', 'detallesMatricula', 'detalle' ),
                'pbac' => array('read', 'write', 'admin'),
            ),
            array('allow',
                'actions' => array('index', 'estadisticoMatricula'),
                'pbac' => array('read',),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
//        $this->render('index');
        $this->render('consulta');
    }

    /***
     * @throws CHttpException
     * Carga de Grados con Seccion en el select: seccionPlantelId
     * @param $periodoId  Id del Periodo
     * @param $codPlantel  Codigo del Plantel
     */
    public function actionLoadGradoSeccion(){
        $periodoId = $this->getRequest('periodoId');
        $codPlantel = $this->getRequest('codPlantel');
        $plantel=Plantel::model()->validarCodPlantelRegistrar($codPlantel);

        if(!empty($plantel) AND !empty($periodoId)) {
            $plantel = Plantel::model()->validarCodPlantel(null, $codPlantel, '1');
            $plantelId = $plantel['plantel_id'];
            ld($plantelId);

            $seccionPlantel = SeccionPlantel::model()->obtenerSeccionPlantel($periodoId, $plantelId);
            $seccionPlantel = CHtml::listData($seccionPlantel, 'seccionplantelid', 'seccionplantel');
            if ( !empty ($seccionPlantel)){
                foreach ($seccionPlantel AS $value=>$name){
                    echo CHtml::tag(
                        'option',array('value'=>$value),
                        CHtml::encode($name),true
                    );
                }
            }

        }

    }

    /***
     * @throws CHttpException
     * Validadcion para: Redireccionar a la view detallada de la matricula de la seccionPlantel
     * @param $periodoId  Id del Periodo
     * @param $codPlantel  Codigo del Plantel
     * @param $seccionPlantelId  Id de seccionPlantel
     */
    public function actionDetallesMatricula() {
        $respuesta= array();

        $periodoId = $this->getRequest('periodoId');
        $codPlantel = $this->getRequest('codPlantel');
        $seccionPlantelId = $this->getRequest('seccionPlantelId');

        $plantel=Plantel::model()->validarCodPlantelRegistrar($codPlantel);

        if(!empty($plantel) AND !empty($periodoId) AND !empty($seccionPlantelId)) {
            $plantel = Plantel::model()->validarCodPlantel(null, $codPlantel, '1');
            $plantelId = $plantel['plantel_id'];

            //$columna .= CHtml::link("", "/planteles/matricula/inscripcionIndividual/id/" . base64_encode($data->id) . "/plantel/" . base64_encode($data->plantel_id) . "/key/" . base64_encode($data->plantel_id * 9), array("class" => "fa fa-user blue inscribir", 'data-id' => base64_encode($data->id), 'data-description' => $data_description, "title" => "Inscribir Estudiante")) . '&nbsp;&nbsp;';
            //CController::forward('/planteles/seccionPlantel/detallesMatriculaSeccion/seccionPlantelId/'.$seccionPlantelId.'/periodoId/'.$periodoId);


            ////////////////////////////
            //$respuesta = array('status'=>'exito','mensaje'=>'Se mostrara la Matricula de la Seccion', 'url'=>'/planteles/matricula15/matriculaInscripcionIndividual/seccionPlantelId/'.base64_encode($seccionPlantelId). "/plantelId/" . base64_encode($plantelId). "/key/" . base64_encode($plantelId * 9) . '/periodoId/'.base64_encode($periodoId));
            /*$respuesta = array('status'=>'exito','mensaje'=>'Se mostrara la Matricula de la Seccion', 'url'=>'/planteles/matricula/matriculaInscripcionIndividual/seccionPlantelId/'.base64_encode($seccionPlantelId). "/plantelId/" . base64_encode($plantelId). "/key/" . base64_encode($plantelId * 9) . '/periodoId/'.base64_encode($periodoId));*/
            $respuesta = array('status'=>'exito','mensaje'=>'Se mostrara la Matricula de la Seccion', 'url'=>'/control/matriculaPlantel/detalle/seccionPlantelId/'.base64_encode($seccionPlantelId). "/plantelId/" . base64_encode($plantelId). "/key/" . base64_encode($plantelId * 9) . '/periodoId/'.base64_encode($periodoId));


            //SeccionPlantelController::actionDetallesMatriculaSeccion($seccionPlantelId, $periodoId);

        }else{
            $respuesta = array('status'=>'error','mensaje'=>'xxxxx');

        }
        echo json_encode($respuesta);

    }

    /***
     * @throws CHttpException
     * Redirecciona a la view detallada de la matricula de la seccionPlantel
     * @param $seccionPlantelId  Id de seccionPlantel
     * @param $plantelId  Id del Plantel
     * @param $periodoId  Id del Periodo
     */
    public function actionDetalle(){
        $seccionPlantelId = $this->getRequest('seccionPlantelId');
        $plantelId = $this->getRequest('plantelId');
        $periodoId = $this->getRequest('periodoId');
        $key = $this->getRequest('key');
        if( !empty($seccionPlantelId) AND !empty($plantelId) AND !empty($periodoId) ) {
            //CController::forward('/planteles/seccionPlantel/detallesMatriculaSeccion/seccionPlantelId/'.base64_decode($seccionPlantelId).'/periodoId/'.base64_decode($periodoId));
            CController::forward('/planteles/matricula15/matriculaInscripcionIndividual/seccionPlantelId/'.$seccionPlantelId . "/plantelId/" . $plantelId . "/key/" . $key . '/periodoId/' . $periodoId, true);
            //$respuesta = array('status' => 'exito', 'mensaje' => 'Se mostrara la Matricula de la Seccion', 'url' => '/planteles/matricula/matriculaInscripcionIndividual/seccionPlantelId/' . base64_encode($seccionPlantelId) . "/plantelId/" . base64_encode($plantelId) . "/key/" . base64_encode($plantelId * 9) . '/periodoId/' . base64_encode($periodoId));
        }else{
            throw new CHttpException(404, 'No se ha encontrado el recurso que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.'); // no recibe todos los parametros
        }
    }
    /***
     * @throws CHttpException
     * Valida que exista el Codigo del Plantel asignado a un Plantel
     * @param $codPlantel  Codigo del Plantel
     */
    public function actionValidarCodPlantel(){
        $codPlantel=$this->getPost('codPlantel');
        if(isset($codPlantel)){
            $plantel=Plantel::model()->validarCodPlantelRegistrar($codPlantel);
            if($plantel!='1'){
                $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'El Código de Plantel  ingresado no se encuentra asignado a un Plantel.'));
            }
        }
    }

}
