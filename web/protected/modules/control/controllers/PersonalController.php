<?php

class PersonalController extends Controller
{
    public $layout = '//layouts/main';
    static $_permissionControl = array(
        'read' => 'Consulta de Estadisticas de Personal',
        'admin' => 'Permite Descargar CSV',
        'label' => 'Estadisticas de Personal'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('index','reporteGeneral'),
                'pbac' => array('read'),
            ),
            array('allow',
                'actions' => array('reportePersonalEstado'),
                'pbac' => array('admin',),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->render('index');
    }
    // REPORTE GENERAL TOTALIZADO DE Estadisticas de Personal POR ESTADO EN LA PÁGINA

    public function actionReporteGeneral()
    {

        if (Yii::app()->request->isAjaxRequest) {
            $model = new Personal();
            $dataReport = $model->reportePersonalGeneral();
            $fechaHora = $model->getFechaReporteGeneral();

            $this->renderPartial('reporteGeneral', array('dataReport' => $dataReport,'fechaHora'=>$fechaHora), false, true);

        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    // REPORTE DE CONGRESOS PEDAGÓGICOS POR ESTADO EN CSV

    public function actionReportePersonalEstado()
    {
        $headers = array(
            'Estado',
            'Personal',
            'Administrativo',
            'Docente',
            'Obrero'
        );

        $colDef = array(
            'estado' => array(),
            'personal' => array(),
            'administrativo' => array(),
            'docente' => array(),
            'obrero' => array(),
        );

        $model = new Personal();
        $dataReport = $model->reportePersonalGeneral();
        $fechaHora = $model->getFechaReporteGeneral();

        $fileName = 'Personal_Sum_' . date('Y-m-d') . '.csv';

        CsvExport::export(
            $dataReport, // a CActiveRecord array OR any CModel array
            $colDef, $headers, true, $fileName,';',$fechaHora
        );
    }
}