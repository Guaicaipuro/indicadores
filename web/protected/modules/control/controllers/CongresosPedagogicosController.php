<?php

class CongresosPedagogicosController extends Controller
{

    public $layout = '//layouts/main';
    static $_permissionControl = array(
        'read' => 'Consulta de Congresos Pedagogicos',
        'admin' => 'Permite Descargar CSV',
        'label' => 'Congresos Pedagogicos'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
      return array(
            array('allow',
                'actions' => array('index', 'reporteGeneral'),
                'pbac' => array('read'),
            ),
          array('allow',
                'actions' => array('ReporteCongresosEstado'),
                'pbac' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
	{
		$this->render('index');
	}

    // REPORTE GENERAL TOTALIZADO DE CONGRESOS PEDAGÓGICOS POR ESTADO EN LA PÁGINA

    public function actionReporteGeneral()
    {

        if (Yii::app()->request->isAjaxRequest) {

            $model = new CongresoPedagogico();
            $dataReport = $model->reporteCongresosGeneral();

            $this->renderPartial('reporteGeneral', array('dataReport' => $dataReport), false, true);

        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    // REPORTE DE CONGRESOS PEDAGÓGICOS POR ESTADO EN CSV

    public function actionReporteCongresosEstado()
    {
        $headers = array(
            'Estado',
            'Cantidad'
        );

        $colDef = array(
            'estado' => array(),
            'congresos' => array(),
        );

        $model = new CongresoPedagogico();

        $dataReport = $model->reporteCongresosGeneral();

        $fileName = 'CongresosPedagogicos_' . date('Y-m-d') . '.csv';

        CsvExport::export(
            $dataReport, // a CActiveRecord array OR any CModel array
            $colDef, $headers, true, $fileName
        );
    }

}