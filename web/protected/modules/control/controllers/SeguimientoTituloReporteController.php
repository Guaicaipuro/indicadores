<?php
/**
 * Created by PhpStorm.
 * User: mari
 * Date: 19/06/15
 * Time: 11:49 AM
 */

class SeguimientoTituloReporteController extends Controller {


    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Consulta de Seguimiento de Título',
        'admin' => 'Permite Descargar Seguimiento de Título',
        'label' => 'Consulta de Seguimiento de Título'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            //'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('estadisticoSegTitulo','estadisticoSegTituloMunicipio','reporteCsvEstado'),
                'users' => array('admin'),
            ),
            /* array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions'=>array('admin','delete'),
              'users'=>array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }



    public function actionIndex(){
        $this->render('index');
    }


    public function actionEstadisticoSegTitulo() {
       // var_dump($_REQUEST);die();
        if (Yii::app()->request->isAjaxRequest) {

                $titulo = 'Estado';
                $estado = new Estado('search');
                $result = $estado->estadosList();
//var_dump($result);die();
            $dataReport = Titulo::model()->reporteEstadisticoSeguimientoTitulo();
           //  var_dump($dataReport);die();

            $this->renderPartial('estadisticaSegTitulo', array('titulo' => $titulo, 'dataReport' => $dataReport, 'estado' => $result), false, true);
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }


    public function actionEstadisticoSegTituloMunicipio() {

        $id = (isset($_REQUEST['id']) && !is_numeric($_REQUEST['id']))? base64_decode($_REQUEST['id']):null;
        if ($id) {
            $titulo = 'Municipio';
            $municipio = new Municipio('search');
            $result = $municipio->municipioList();
            $dataReport = Titulo::model()->reporteEstadisticoSeguimientoTituloMunicipio($id);
            $this->renderPartial('estadisticaSegTituloMunicipio', array('titulo' => $titulo, 'dataReport' => $dataReport, 'estado' => $result,'estado_id'=>$id), false, true);
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionReporteCsvEstado()
    {
        if(isset($_REQUEST['estado'])){

            $estado_id = $this->getRequest('estado');
            $id_decoded = base64_decode($estado_id);
            $control = $this->getRequest('control');
            $control_decoded = base64_decode($control);
            $entro = 1;

        }elseif(isset($_REQUEST['municipio'])){

            $municipio_id = $this->getRequest('municipio');
            $id_decoded = base64_decode($municipio_id);
            $control = $this->getRequest('control');
            $control_decoded = base64_decode($control);
            $entro = 0;
        }

        $nombreControl = '';
        if ($id_decoded != '' && $control_decoded != ''){

            $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_actual_id = $periodo_escolar_actual_id['id'];

            if($entro == 1) {
                $datosPK = Estado::model()->findByPk($id_decoded);
                $nombre = $datosPK['nombre'];
            }
            if($entro == 0) {
                $datosPK = Municipio::model()->findByPk($id_decoded);
                $nombre = $datosPK['nombre'];
            }

            switch ($control_decoded) {
                case 'T':
                    $nombreControl = 'Seriales_Asignados_Estudiantes_del_Estado_';
                    break;
                case 'Z':
                    $nombreControl = 'Seriales_Entregados_Zona_Educativa_del_Estado_';
                    break;
                case 'P':
                    $nombreControl = 'Seriales_Entregados_a_Planteles_del_Estado_';
                    break;
                case 'E':
                    $nombreControl = 'Seriales_Entregados_a_Estudiantes_del_Estado_';
                    break;
                case 'MP':
                    $nombreControl = 'Seriales_Entregados_a_Planteles_del_Municipio_';
                    break;
                case 'ME':
                    $nombreControl = 'Seriales_Entregados_a_Estudiantes_del_Municipio_';
                    break;
            }

            $datosReporte = Titulo::model()->reporteEstadisticoSeguimientoTituloEstadoCsv($id_decoded, $periodo_actual_id, $control_decoded);
          // var_dump($datosReporte);die();
            if ($datosReporte != false) {
                $headers = array('Código Plantel', 'Nombre del Plantel', 'Código del Plan', 'Nacionalidad',
                    'Documento identidad', 'Tipo documento de identidad', 'Cédula Escolar', 'Serial', 'Nombres', 'Apellidos', 'Grado', 'Sección', 'Período','Estatus del Título Anterior');

                $colDef = array('cod_plantel' => array(), 'nombre_plantel' => array(), 'codigo_plan' => array(),
                    'nacionalidad' => array(), 'documento_identidad' => array(), 'tdocumento_identidad' => array(), 'cedula_escolar' => array(), 'serial' => array(), 'nombres' => array(), 'apellidos' => array(),
                    'grado' => array(), 'seccion' => array(), 'periodo' => array(), 'estatus_titulo' => array());

                $fileName = 'seguimiento_titulo_' . $nombreControl.$nombre .'_'. date('YmdHis') . '.csv';

                CsvExport::export(
                    $datosReporte, // a CActiveRecord array OR any CModel array
                    $colDef, $headers, true, $fileName);
            }else{
                $this->redirect(array('index'));
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionEstadisticoSegTituloParroquia() {

        $id = (isset($_REQUEST['id']) && !is_numeric($_REQUEST['id']))? base64_decode($_REQUEST['id']):null;//$id es el municipio_id
        $estado_id = (isset($_REQUEST['estado_id']) && !is_numeric($_REQUEST['estado_id']))? base64_decode($_REQUEST['estado_id']):null;
        if ($id != null && $estado_id != null) {
            $datosMunicipio = Municipio::model()->findByPk($id);
            $nombreMunicipio = (isset($datosMunicipio) && $datosMunicipio != null)?$datosMunicipio['nombre']:'';
            $titulo = 'Parroquias del Municipio: ' .$nombreMunicipio;
            $parroquia = new Parroquia('search');
            $result = $parroquia->parroquiaList($id);
            $dataReport = Titulo::model()->reporteEstadisticoSeguimientoTituloParroquia($id);
            $this->renderPartial('estadisticaSegTituloParroquia', array('titulo' => $titulo, 'dataReport' => $dataReport, 'estado' => $result,'estado_id'=>$estado_id,'municipio_id'=>$id), false, true);
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }


    public function actionEstadisticoSegTituloPlanteles() {
        $estado_id = (isset($_REQUEST['estado_id']) && !is_numeric($_REQUEST['estado_id']))? base64_decode($_REQUEST['estado_id']):null;
        $municipio_id = (isset($_REQUEST['municipio_id']) && !is_numeric($_REQUEST['municipio_id']))? base64_decode($_REQUEST['municipio_id']):null;
        $parroquia_id = (isset($_REQUEST['parroquia_id']) && !is_numeric($_REQUEST['parroquia_id']))? base64_decode($_REQUEST['parroquia_id']):null;
        //var_dump($estado_id .' - '. $municipio_id .' - '. $parroquia_id);die();
        if ($estado_id != null && $municipio_id != null && $parroquia_id != null) {
            $titulo = 'Planteles';
            $plantel = new Plantel('search');
            $result = $plantel->plantelesList($parroquia_id);
            $dataReport = Titulo::model()->reporteEstadisticoSeguimientoTituloPlanteles($parroquia_id);
            $this->renderPartial('estadisticaSegTituloPlanteles', array('titulo' => $titulo, 'dataReport' => $dataReport, 'estado' => $result,'estado_id'=>$estado_id,'municipio_id'=>$municipio_id, 'parroquia_id'=>$parroquia_id), false, true);
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

}