<?php
/* @var $this EstadisticasConsejosController */

$this->breadcrumbs=array(
	'Estadisticas Consejos'=>array('/control/estadisticasConsejos'),
	'Estadisticas de los Consejos Educativos por Proyectos',
);
$this->pageTitle = 'Estadisticas de los Consejos Educativos por Proyectos';
?>

<?php 
    $columnas = array(

        array(
            'header' => "<center>{$header_nombre}</center>",
            'name' => 'nombre',
            'footer'=>'Totales',
            'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
            //'htmlOptions' => array(),
        ),

        array(
            'header' => '<center>Consejos Registrados</center>',
            'name' => 'total_consejos',
            'htmlOptions' => array('style'=>'text-align: center;'),
            'footer'=>$this->totalPorColumna($dataProvider,'total_consejos'),
            'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
        ),
        array(
            'header' => '<center>Consejos con Proyectos</center>',
            'name' => 'con_proyectos',
            'htmlOptions' => array('style'=>'text-align: center;'),
            'footer'=>$this->totalPorColumna($dataProvider,'con_proyectos'),
            'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
        ),        
    );
foreach ($proyectos as $id => $nombre) {
    

    $columnas[] =  array(
            'header' => $nombre,
            'headerHtmlOptions' =>array('style'=>'text-align:center;'),
            'name' => 'c'.$id,
            'htmlOptions' => array('style'=>'text-align: center;'),
            'footer'=>$this->totalPorColumna($dataProvider,'c'.$id),
            'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
        );
}
?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Estadisticas de los Proyectos de los Consejos Educativos</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
                <div id="result"></div>
                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá visualizar las Estadísticas de los Proyectos de los Consejos Educativos .
                            </p>
                        </div>
                    </div> 

                    <div class="row space-20"></div>

                    <div class="col-md-12">
                        <div class="col-md-4">
                            <?php if(Yii::app()->user->pbac('control.estadisticasConsejos.admin')): ?>
                                Estado
                                <?php echo CHtml::dropDownList('estado','',$estados, array('prompt'=>'** TODOS LOS ESTADOS **', 'class' => 'span-10',)); ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">

                            <?php 
                                echo CHtml::link(" Exportar Resultados", '/control/estadisticasConsejos/PdfEstadisticasProyectos/estado/', array("id"=>"pdfEstados","class" => "btn btn-primary fa fa-file-pdf-o  blue", "title" => "Exportar Resultados"));
                            ?>  
                        </div>                                              
                    </div>

                </div>
                    <?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'estadisticas-estados-grid',
                        'dataProvider'=>$dataProvider,
                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                            'summaryText' => 'Mostrando {count} resultados',
                        'columns'=>$columnas,/*array(
                           /* array(
                                'header' => '<center>Id</center>',
                                'name' => 'id',
                                'htmlOptions' => array(),
                                'filter' => CHtml::textField('CategoriaConsejo[id]', $model->id, array('title' => '',)),
                            ),
                        
                            array(
                                'header' => '<center>Entidad Federal</center>',
                                'name' => 'nombre',
                                'footer'=>'Totales',
                                'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
                                //'htmlOptions' => array(),
                            ),
                            array(
                                'header' => '<center>Consejos Registrados</center>',
                                'name' => 'total_consejos',
                                'htmlOptions' => array('style'=>'text-align: center;'),
                                'footer'=>$model->totalPorColumna($model->estadisticasPorEstados(),'total_consejos'),
                                'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
                            ),
                            array(
                                'header' => '<center>Consejos con Proyectos</center>',
                                'name' => 'con_proyectos',
                                'htmlOptions' => array('style'=>'text-align: center;'),
                                'footer'=>$model->totalPorColumna($model->estadisticasPorEstados(),'con_proyectos'),
                                'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
                            ),
                            
                            array(
                                'header' => '<center>Socio-Productivos</center>',
                                'name' => 'c1',
                                'htmlOptions' => array('style'=>'text-align: center;'),
                                //'footer'=>$model->totalPorColumna($model->estadisticasPorEstados(),'socio_productivo'),
                                //'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
                            ),
                            array(
                                'header' => '<center>Socio-Comunitarios</center>',
                                'name' => 'c2',
                                'htmlOptions' => array('style'=>'text-align: center;'),
                                //'footer'=>$model->totalPorColumna($model->estadisticasPorEstados(),'socio_comunitario'),
                                //'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold;'),
                            ),

                            array(
                                'header' => '<center>Formación de Familia</center>',
                                'name' => 'c3',
                                'htmlOptions' => array('style'=>'text-align: center;'),
                                //'footer'=>$model->totalPorColumna($model->estadisticasPorEstados(),'socio_comunitario'),
                                //'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold;'),
                            ),

                            array(
                                'header' => '<center>Seguridad Integral Escolar</center>',
                                'name' => 'c4',
                                'htmlOptions' => array('style'=>'text-align: center;'),
                                //'footer'=>$model->totalPorColumna($model->estadisticasPorEstados(),'socio_comunitario'),
                                //'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold;'),
                            ),

                            array(
                                'header' => '<center>Formación en Valores Ciudadanos Y Derechos Humanos</center>',
                                'name' => 'c5',
                                'htmlOptions' => array('style'=>'text-align: center;'),
                                //'footer'=>$model->totalPorColumna($model->estadisticasPorEstados(),'socio_comunitario'),
                                //'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold;'),
                            ),
                                            /*
                            array(
                                        'type' => 'raw',
                                        'header(string)der' => '<center>Acción</center>',
                                        'value' => array($this, 'getActionButtons'),
                                        'htmlOptions' => array('nowrap'=>'nowrap'),
                                    ),
                                    
                        ),*/
                    )); ?>
            </div>
        </div>
    </div>
</div>

<?php
    /**
     * Yii::app()->clientScript->registerScriptFile(
     *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/CategoriaConsejoController/categoria-consejo/admin.js', CClientScript::POS_END
     *);
     */
?>
<div id="dialogPantalla" class="hide" ></div>

<script type="text/javascript">

    $(document).ready(function(){

        var rutaPdf = $('#pdfEstados').prop('href');
        
        $('#estado').on('change',function(){
            var estado_id = $(this).val();
            $('#pdfEstados').prop('href',rutaPdf+estado_id);
            refrescarGrid();
        });
    });



function refrescarGrid(){
    $('#estadisticas-estados-grid').yiiGridView('update', {
        data: $('*').serialize()
    });
}
</script>



