<style>
table#principal {
	width: 100%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th{
	background-color: #8BBBCC;
}
td {
    padding: 10px;
}

</style>

<table id="principal">
	<tr>
		<th width="20%">
			<?php echo $headerCol1; ?>
		</th>
		<th width="10%">
			Planteles
		</th>
		<th width="10%">
			Consejos Registrados
		</th>
		<th width="10%">
			Consejos Actualizados
		</th>
		<th width="10%">
			Consejos No Actualizados
		</th>
		<th width="10%">
			Consejos con Proyectos
		</th>
		<th width="15%">
			Forman Parte de un Circuito Educativo Comunitario
		</th>
		<th width="15%">
			Poseen Comite de Económia Escolar
		</th>				
	</tr>

	<?php 
		foreach ($dataProviderEstados->rawData as $key => $value) {?>
		
			<tr>
				<td>
					<?php echo $value['nombre'];  ?>
				</td>
				<td  style="text-align:center;">
					<?php echo $value['total_planteles']; ?>
				</td>
				<td  style="text-align:center;">
					<?php echo $value['total_consejos']; ?>
				</td>
				<td  style="text-align:center;">
					<?php echo $value['cant_actualizados']; ?>
				</td>
				<td  style="text-align:center;">
					<?php echo $value['cant_desactualizados']; ?>
				</td>
				<td  style="text-align:center;">
					<?php echo $value['con_proyectos']; ?>
				</td>
				<td  style="bold; text-align:center;">
					<?php echo $value['circuito_educativo']; ?>
				</td>
				<td  style="bold; text-align:center;">
					<?php echo $value['comite_economia']; ?>
				</td>								
			</tr>
		
	<?php } /* FIN DEL FOREACH */?>

	<tr>
		<td width="30%" style="font-weight: bold; text-align:center;">
			Totales
		</td>
		<td width="10%" style="font-weight: bold; text-align:center;">
			<?php echo $footer_table['total_planteles'] ?>
		</td>
		<td width="10%" style="font-weight: bold; text-align:center;">
			<?php echo $footer_table['total_consejos'] ?>
		</td>
		<td width="10%" style="font-weight: bold; text-align:center;">
			<?php echo $footer_table['cant_actualizados'] ?>
		</td>
		<td width="10%" style="font-weight: bold; text-align:center;">
			<?php echo $footer_table['cant_desactualizados'] ?>
		</td>
		<td width="10%" style="font-weight: bold; text-align:center;">
			<?php echo $footer_table['con_proyectos'] ?>
		</td>
		<td width="10%" style="font-weight: bold; text-align:center;">
			<?php echo $footer_table['circuito_educativo'] ?>
		</td>
		<td width="10%" style="font-weight: bold; text-align:center;">
			<?php echo $footer_table['comite_economia'] ?>
		</td>				
	</tr>

</table>