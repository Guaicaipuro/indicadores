<?php

/* @var $this EstadisticasConsejosController */

$this->breadcrumbs=array(
    'Estadisticas Consejos'=>array('/control/estadisticasConsejos'),
    'Estadisticas Consejos Actualizados',
);

$this->pageTitle = 'Estadisticas de los Consejos Educativos por Estados';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Estadisticas de los Consejos Educativos por Estados a nivel Nacional</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
            <div id="result">
                
            </div>
                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá visualizar las Estadísticas de los Consejos Educativos por Estados.
                            </p>
                        </div>
                    </div> 

                    <div class="row space-20"></div>

                    <div class="col-md-12">
                        <div class="col-md-4">
                            <?php if(Yii::app()->user->pbac('control.estadisticasConsejos.admin')): ?>
                                Estado
                                <?php echo CHtml::dropDownList('estado','',$estados, array('prompt'=>'** TODOS LOS ESTADOS **', 'class' => 'span-12',)); ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">

                            <?php 
                                echo CHtml::link(" Exportar Resultados", '/control/estadisticasConsejos/PdfEstadisticasEstados/estado/', array("id"=>"pdfEstados","class" => "btn btn-primary fa fa-file-pdf-o  blue", "title" => "Exportar Resultados"));
                            ?>  
                        </div>                                              
                    </div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'estadisticas-estados-grid',
    'dataProvider'=>$dataProvider,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {count} resultados',
    'columns'=>array(
       /* array(
            'header' => '<center>Id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('CategoriaConsejo[id]', $model->id, array('title' => '',)),
        ),*/
    
        array(
            'header' => "<center>$header_nombre</center>",
            'name' => 'nombre',
            'footer'=>'Totales',
            'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
            //'htmlOptions' => array(),
        ),
        array(
            'header' => '<center>Planteles</center>',
            'name' => 'total_planteles',
            'htmlOptions' => array('style'=>'text-align: center;'),
            'footer'=>$this->totalPorColumna($dataProvider,'total_planteles'),
            'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
        ),
        array(
            'header' => '<center>Consejos Registrados</center>',
            'name' => 'total_consejos',
            'htmlOptions' => array('style'=>'text-align: center;'),
            'footer'=>$this->totalPorColumna($dataProvider,'total_consejos'),
            'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
        ),
        array(
            'header' => '<center>Consejos Actualizados</center>',
            'name' => 'cant_actualizados',
            'htmlOptions' => array('style'=>'text-align: center;'),
            'footer'=>$this->totalPorColumna($dataProvider,'cant_actualizados'),
            'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
        ),
        array(
            'header' => '<center>Consejos NO Actualizados</center>',
            'name' => 'cant_desactualizados',
            'htmlOptions' => array('style'=>'text-align: center;'),
            'footer'=>$this->totalPorColumna($dataProvider,'cant_desactualizados'),
            'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
        ),
        array(
            'header' => '<center>Consejos con Proyectos</center>',
            'name' => 'con_proyectos',
            'htmlOptions' => array('style'=>'text-align: center;'),
            'footer'=>$this->totalPorColumna($dataProvider,'con_proyectos'),
            'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold'),
        ),
        array(
            'header' => 'Forman Parte de un Circuito Educativo Comunitario',
            'name' => 'circuito_educativo',
            'headerHtmlOptions' => array('style'=>'text-align: center;'),
            'htmlOptions' => array('style'=>'text-align: center;'),
            'footer'=>$this->totalPorColumna($dataProvider,'circuito_educativo'),
            'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold;'),
        ),
        array(
            'header' => 'Poseen Comite de Económia Escolar',
            'name' => 'comite_economia',
            'headerHtmlOptions' => array('style'=>'text-align: center;'),
            'htmlOptions' => array('style'=>'text-align: center;'),
            'footer'=>$this->totalPorColumna($dataProvider,'comite_economia'),
            'footerHtmlOptions' => array('style'=>'text-align: center; font-weight: bold;'),
        ),           


        /*
        array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
                */
    ),
)); ?>
            </div>
        </div>
    </div>
</div>

<?php
    /**
     * Yii::app()->clientScript->registerScriptFile(
     *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/CategoriaConsejoController/categoria-consejo/admin.js', CClientScript::POS_END
     *);
     */
?>
<div id="dialogPantalla" class="hide" ></div>

<script type="text/javascript">

    $(document).ready(function(){

        var rutaPdf = $('#pdfEstados').prop('href');
        
        $('#estado').on('change',function(){
            var estado_id = $(this).val();
            $('#pdfEstados').prop('href',rutaPdf+estado_id);
            refrescarGrid();
        });
    });

function refrescarGrid(nombre_estado){
    $('#estadisticas-estados-grid').yiiGridView('update', {
        data: $('*').serialize()
    });
}

</script>

