<style>
table#principal {
	width: 100%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th{
	background-color: #8BBBCC;
	text-align: center;
}
td {
    padding: 10px;
    text-align: center;
}

</style>

<table id="principal">
	<tr>
		<th>
			<?php echo $headerCol1; ?>
		</th>
		<th>
			Consejos Registrados
		</th>
		<th>
			Consejos con Proyectos
		</th>

		<?php foreach ($proyectos as $id => $nombre) { ?>	
			<th>
				<?php echo $nombre; ?>
			</th>			
		<?php } ?>	
	</tr>

	<?php 
		foreach ($dataProviderEstados->rawData as $key => $value) {?>
		
			<tr>
				<td style="text-align: left;">
					<?php echo $value['nombre'];  ?>
				</td>
				<td>
					<?php echo $value['total_consejos']; ?>
				</td>
				<td>
					<?php echo $value['con_proyectos']; ?>
				</td>
				<?php foreach ($proyectos as $id => $nombre) { ?>
					<td>
						<?php echo $value['c'.$id]; ?>
					</td>
				<?php } ?>			
			</tr>
		
	<?php } /* FIN DEL FOREACH */?>

	<tr>
		<td style="font-weight: bold;">
			Totales
		</td>
		<td style="font-weight: bold;">
			<?php echo $footer_table['total_consejos'] ?>
		</td>
		</td>
		<td style="font-weight: bold;">
			<?php echo $footer_table['con_proyectos'] ?>
		</td>
		<?php foreach ($proyectos as $id => $nombre) { ?>
			<td style="font-weight: bold;">
				<?php echo $footer_table['c'.$id] ?>
			</td>
		<?php } ?>		
	</tr>

</table>