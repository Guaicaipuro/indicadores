<div class="breadcrumb row row-fluid" style="margin-left: 2px;">

    <div class = "pull-left" style ="padding-left: 20px; padding: 20px">

        <?php if (Yii::app()->user->pbac('admin')): ?>
            <a  href="<?php echo Yii::app()->createUrl("/control/matriculacionReporte/reporteCSV/"); ?>"  id="reporteMatriculaTotal"   class = "btn btn-primary btn-next btn-sm ">
                Exportar Totales
                <i class="fa fa-file-text-o icon-on-right"></i>
            </a>
        <?php endif; ?>


    </div>

</div>

<div class="space-6"></div>

<table class="report table table-striped table-bordered table-hover">

    <thead>
    <tr><th colspan="6"><center><b>Matricula Periodo 2013-2014</b></center></th></tr>
    <tr>
        <th nowrap rowspan="2" class="center">
            <?php echo ucfirst($nivel);?>
        </th>
        <th title="Total de Planteles" rowspan="2" class="center">
            Total de Planteles con Estudiantes Matriculados
        </th>
    </tr>
    <tr>
        <th class="center" title="Cantidad de estudiante matriculado periodo inicial">
            Estudiantes Matriculados Inicial
        </th>
        <th class="center" title="Cantidad de estudiante matriculado en primaria">
            Estudiantes Matriculados Primaria
        </th>
        <th class="center" title="Cantidad de estudiante matriculados en otros niveles">
            Estudiantes Matriculados Otros
        </th>
        <th class="center" title="Cantidad de estudiante matriculado">
            Cantidad de Estudiantes Matriculados
        </th>
    </tr>
    </thead>

    <tbody>
    <?php if (empty($dataReport)): ?>
        <tr>
            <td colspan="11">
                <div class="alertDialogBox" style="margin-top: 10px;">
                    <p>
                        No se han encontrado Registros.
                    </p>
                </div>
            </td>
        </tr>
    <?php else: ?>

        <?php
        $sumPlanteles = 0;
        $sum_est_inicial = 0;
        $sum_est_primaria = 0;
        $sum_est_otros = 0;

        foreach ($dataReport as $data):
            $sumPlanteles = $sumPlanteles + $data['planteles'];
            $sum_est_inicial = $sum_est_inicial + $data['cant_estudiante_inicial'];
            $sum_est_primaria = $sum_est_primaria + $data['cant_estudiante_primaria'];
            $sum_est_otros = $sum_est_otros + $data['cant_estudiante_otros'];
            ?>

            <?php
            $dependencyId = 0;


            if (strtolower($data['nombre']) == 'total') {

                if($nivel=='estado'){
                    $dependencyId = $data[$nivel];
                    $dep='estado_id';
                }
                elseif($nivel=='municipio'){
                    $dependencyId = $data[$nivel];
                    $dep='municipio_id';
                }

            } else {
                $dependencyId = $data['id'];
            }
            ?>

            <tr>
                <td class="center">
                    <?php if ($nivel == 'municipio' && strtolower($data['nombre']) != 'total'): ?>
                        <?php echo ucwords(strtolower($data['nombre'])); ?>
                    <?php elseif ($nivel == 'estado' && strtolower($data['nombre']) != 'total'): ?>

                    <a href="/control/MatriculacionReporte/ReporteDetalladoMatricula/col/matricula/lev/<?php echo (strtolower($data['nombre']) != 'total') ? $nivel : $anteriorNivel; ?>/dep/<?php echo $dependencyId; ?>">     <?php echo ucwords(strtolower($data['nombre'])); ?>


                        <?php elseif (strtolower($data['nombre']) == 'total'): ?>

                                <b><i>    <?php echo ucwords(Utiles::strtolower_utf8($data['nombre'])); ?>       </b></i>                <?php endif; ?>
                    </a>
                </td>
                <td class="center">
                    <?php if (strtolower($data['nombre']) != 'total'): ?>
                    <a href="/control/MatriculacionReporte/ReporteDetalladoMatricula/col/planteles/lev/<?php echo (strtolower($data['nombre']) != 'total') ? $nivel : $anteriorNivel; ?>/dep/<?php echo $dependencyId; ?>">
                        <?php echo $data['planteles']; ?>
                        <?php endif; ?>
                        <?php if (strtolower($data['nombre']) == 'total'): ?>
                            <?php echo ucwords(Utiles::strtolower_utf8($data['planteles'])); ?> <?php endif; ?>
                    </a>
                </td>

                <td class="center">
                    <?php if (strtolower($data['nombre']) != 'total'): ?>
                    <a href="/control/MatriculacionReporte/ReporteDetalladoMatricula/col/matricula_inicial/lev/<?php echo (strtolower($data['nombre']) != 'total') ? $nivel : $anteriorNivel; ?>/dep/<?php echo $dependencyId; ?>">
                        <?php echo $data['cant_estudiante_inicial']; ?>
                        <?php endif; ?>
                        <?php if (strtolower($data['nombre']) == 'total'): ?>

                            <?php echo ucwords(Utiles::strtolower_utf8($data['cant_estudiante_inicial'])); ?> <?php endif; ?>
                    </a>
                </td>

                <td class="center">
                    <?php if (strtolower($data['nombre']) != 'total'): ?>
                    <a href="/control/MatriculacionReporte/ReporteDetalladoMatricula/col/matricula_primaria/lev/<?php echo (strtolower($data['nombre']) != 'total') ? $nivel : $anteriorNivel; ?>/dep/<?php echo $dependencyId; ?>">
                        <?php echo $data['cant_estudiante_primaria']; ?>
                        <?php endif; ?>
                        <?php if (strtolower($data['nombre']) == 'total'): ?>
                            <?php echo ucwords(Utiles::strtolower_utf8($data['cant_estudiante_primaria'])); ?>
                        <?php endif; ?>
                    </a>
                </td>

                <td class="center">
                    <?php if (strtolower($data['nombre']) != 'total'): ?>
                    <a href="/control/MatriculacionReporte/ReporteDetalladoMatricula/col/matricula_otros/lev/<?php echo (strtolower($data['nombre']) != 'total') ? $nivel : $anteriorNivel; ?>/dep/<?php echo $dependencyId; ?>">
                        <?php echo $data['cant_estudiante_otros']; ?>
                        <?php endif; ?>
                        <?php if (strtolower($data['nombre']) == 'total'): ?>
                            <?php echo ucwords(Utiles::strtolower_utf8($data['cant_estudiante_otros'])); ?>
                        <?php endif; ?>
                    </a>
                </td>


                <td class="center">
                    <?php if (strtolower($data['nombre']) != 'total'): ?>
                    <a href="/control/MatriculacionReporte/ReporteDetalladoMatricula/col/matricula/lev/<?php echo (strtolower($data['nombre']) != 'total') ? $nivel : $anteriorNivel; ?>/dep/<?php echo $dependencyId; ?>">
                        <?php echo $data['cantidad_estudiante']; ?>
                        <?php endif; ?>
                        <?php if (strtolower($data['nombre']) == 'total'): ?>
                            <?php echo ucwords(Utiles::strtolower_utf8($data['cantidad_estudiante'])); ?> <?php endif; ?>
                    </a>
                </td>
            </tr>

        <?php endforeach; ?>
        <tr>
            <th class="center"><i>Total</i></th>
            <th class="center"><i><?php echo $sumPlanteles; ?></i></th>
            <th class="center"><i><?php echo $sum_est_inicial; ?></i></th>
            <th class="center"><i><?php echo $sum_est_primaria; ?></i></th>
            <th class="center"><i><?php echo $sum_est_otros; ?></i></th>
            <th class="center"><i><?php echo $sum_est_inicial + $sum_est_primaria + $sum_est_otros; ?></i></th>
        </tr>

    <?php endif; ?>

    </tbody>

</table>
<span class="small">Reporte Generado el día: <?php echo $date; ?></span>
<script type="text/javascript">
    $(document).ready(function () {

        $('.contact-data').unbind('click');
        $('.contact-data').on('click',
            function (e) {
                e.preventDefault();
                var estado_id = $(this).attr('data-id');
                verDatosContacto('zona_educativa', estado_id);
            }
        );

        $('.observ-data').unbind('click');
        $('.observ-data').on('click',
            function (e) {
                e.preventDefault();
                var estado_id = $(this).attr('data-id');
                var estado = $(this).attr('data-estado');
                dialogObservacion(estado_id, estado);
            }
        );

        $('.rep-control').unbind('click');
        $('.rep-control').on('click',
            function (e) {
                e.preventDefault();
                var estado_id = $(this).attr('data-id');
                var estado = $(this).attr('data-estado');
                dialogRegistroControl('zona_educativa', estado_id, estado);
            }
        );

    });
</script>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/control/matriculaReporte.js', CClientScript::POS_END); ?>
