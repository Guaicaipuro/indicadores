<div class="pull-left" style="padding: 0px 0px 20px 20px">

    <?php if (Yii::app()->user->pbac('admin')): ?>
        <a href="<?php echo Yii::app()->createUrl("/control/congresosPedagogicos/reporteCongresosEstado/"); ?>" class="btn btn-primary btn-next btn-sm">
            Exportar Totales
            <i class="fa fa-file-text-o icon-on-right"></i>
        </a>
    <?php endif; ?>

</div><br>

<table class="report table table-striped table-bordered table-hover">
    <thead>
      <tr><th colspan="8" class="center"><b>Cantidad de Congresos Pedag&oacute;gicos Registrados por Estado</b></th></tr>
      <tr>
        <th class="center">Estado</th>
        <th class="center">Cantidad</th>
      </tr>
    </thead>
    <tbody>
      <?php
        $sum = 0;
        foreach ($dataReport as $data):
          $sum += $data['congresos'];
      ?>
      <tr title="<?php echo "Estado: ".$data['estado']." | Cantidad: ".$data['congresos']; ?>" class="text-info">
        <td class="center">
          <?php echo $data['estado']; ?>
        </td>
        <td class="center">
          <?php echo $data['congresos']; ?>
        </td>
      </tr>
      <?php endforeach; ?>
      <tr>
        <th class="center">
           <i>Total de Congresos Pedagógicos Registrados</i>
        </th>
        <th class="center">
          <i><?php echo $sum; ?></i>
        </th>
      </tr>
    </tbody>
</table>