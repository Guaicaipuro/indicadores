<?php
?>
<div class="breadcrumb row row-fluid" style="margin-left: 2px;">


</div>
<div class = "pull-left" style ="padding-left: 20px; padding: 20px">

    <?php if (Yii::app()->user->pbac('admin')): ?>
        <a  href="<?php echo Yii::app()->createUrl("/control/autoridadesPlantel/reporteTotalDirectores/"); ?>"  id="reporteAutoridadesTotal"   class = "btn btn-primary btn-next btn-sm ">
            Exportar Totales
            <i class="fa fa-file-text-o icon-on-right"></i>
        </a>
    <?php endif; ?>
</div>
<?php
$sumDirectores = 0;
$sum_sin_director_oficial= 0;
$sum_con_director_oficial= 0;
$sum_sin_director_privado = 0;
$sum_con_director_privado = 0;
$sum_con_director_otros = 0;
$sum_sin_director_otros = 0;




foreach ($dataReport as $data):
    $sumDirectores = $sumDirectores + $data['total'];
    $sum_sin_director_oficial = $sum_sin_director_oficial + $data['sin_director_oficial'];
    $sum_con_director_oficial = $sum_con_director_oficial + $data['con_director_oficial'];
    $sum_sin_director_privado = $sum_sin_director_privado + $data['sin_director_privado'];
    $sum_con_director_privado = $sum_con_director_privado + $data['con_director_privado'];
    $sum_con_director_otros = $sum_con_director_otros + $data['con_director_otros'];
    $sum_sin_director_otros = $sum_sin_director_otros + $data['sin_director_otros'];

    ?>        <?php endforeach; ?>

<?php foreach ($dataReport as $data):

    //var_dump($dataReport);
    //echo $data[$nivel];
    ?>

    <?php

    $avance = 0;
    if((int)$data['total']>0):
        $avance = ($data['con_director_total']*100)/$data['total'];
        //echo $avance;
    endif;

    $dependencyId = 0;
    ?>
<?php endforeach; ?>

<table class="report table table-striped table-bordered table-hover">

    <thead>
    <tr><th colspan="8"><center><b>Registro Detallado de Planteles</b></center></th></tr>
    <tr>
    <tr>
        <th nowrap rowspan="2" class="center">
            <?php echo $titulo; ?>
        </th>
        <!--<th title="Total de Planteles" rowspan="2" class="center">
            Total
        </th>-->
        <th title="Planteles con Director Registrado" rowspan="2" class="center">
            Total de Planteles con Director
        </th>
        <!-- <th title="Porcentaje de Avance" rowspan="2" class="center">
             %
         </th>-->
        <th colspan="2" class="center" title="Directores de Planteles de Dependencia Nacional, Estadal o Municipal">
            Planteles Oficiales
        </th>
        <th colspan="2" class="center" title="Directores de Planteles Privados">
            Planteles Privados
        </th>
        <th colspan="2" class="center" title="Directores de Planteles Subvencionados">
            Otros Planteles
        </th>
        <?php if($nivel=='estado'): ?>

        <?php endif; ?>
    </tr>
    <tr>
        <th class="center" title="Planteles de Dependencia Nacional, Estadal o Municipal Sin Director Registrado">
            Sin Director
        </th>
        <th class="center" title="Planteles de Dependencia Nacional, Estadal o Municipal Con Director Registrado">
            Con Director
        </th>
        <th class="center" title="Planteles de Dependencia Nacional, Estadal o Municipal Sin Director Registrado">
            Sin Director
        </th>
        <th class="center" title="Planteles de Dependencia Nacional, Estadal o Municipal Con Director Registrado">
            Con Director
        </th>
        <th class="center" title="Planteles de Dependencia Nacional, Estadal o Municipal Sin Director Registrado">
            Sin Director
        </th>
        <th class="center" title="Planteles de Dependencia Nacional, Estadal o Municipal Con Director Registrado">
            Con Director
        </th>

    </tr>

    </thead>

    <tbody>
    <?php if(empty($dataReport)): ?>
        <tr>
            <td colspan="11">
                <div class="alertDialogBox" style="margin-top: 10px;">
                    <p>
                        No se han encontrado Registros.
                    </p>
                </div>
            </td>
        </tr>
    <?php else: ?>

        <?php foreach ($dataReport as $data): ?>

            <?php

            $avance = 0;
            if((int)$data['total']>0):
                $avance = ($data['con_director_total']*100)/$data['total'];
            endif;

            $dependencyId = 0;

            //   if(strtolower($data['nombre'])=='total'){

            if($nivel=='estado'){
                $dependencyId = $data[$nivel];
                $dep='estado_id';
            }
            elseif($nivel=='municipio'){
                $dependencyId = $data[$nivel];
                $dep='municipio_id';
            }

//            }else{
//                $dependencyId = $data['id'];
//            }
            ?>

            <tr>

                <!--            <td class="center">-->
                <!--                <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/planteles/lev/--><?php //echo (strtolower($data['nombre'])!='total')?$nivel:$anteriorNivel; ?><!--/dep/--><?php //echo $dependencyId; ?><!--">-->
                <!--                    --><?php //echo $data['planteles']; ?>
                <!--                </a>-->
                <!--            </td>-->
                <td class="center">
                    <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/planteles/lev/<?php echo (strtolower($data[$nivel]))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data[$dep]; ?>">
                        <?php echo $data[$nivel]; ?>
                    </a>
                </td>
                <td class="center">
                    <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/con_director/lev/<?php echo (strtolower($data[$nivel]))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data[$dep]; ?>">
                        <?php echo $data['total']; ?>
                    </a>
                </td>
                <td class="center">
                    <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/publ_sin_director/lev/<?php echo (strtolower($data[$nivel]))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data[$dep]; ?>">
                        <?php echo $data['sin_director_oficial']; ?>
                    </a>
                </td>
                <td class="center">
                    <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/publ_con_director/lev/<?php echo (strtolower($data[$nivel]))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data[$dep]; ?>">
                        <?php echo $data['con_director_oficial']; ?>
                    </a>
                </td>
                <td class="center">
                    <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/priv_sin_director/lev/<?php echo (strtolower($data[$nivel]))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data[$dep]; ?>">
                        <?php echo $data['sin_director_privado']; ?>
                    </a>
                </td>
                <td class="center">
                    <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/priv_con_director/lev/<?php echo (strtolower($data[$nivel]))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data[$dep]; ?>">
                        <?php echo $data['con_director_privado']; ?>
                    </a>
                </td>
                <td class="center">
                    <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/otros_sin_director/lev/<?php echo (strtolower($data[$nivel]))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data[$dep]; ?>">
                        <?php echo $data['sin_director_otros']; ?>
                    </a>
                </td>
                <td class="center">
                    <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/otros_con_director/lev/<?php echo (strtolower($data[$nivel]))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data[$dep]; ?>">
                        <?php echo $data['con_director_otros']; ?>
                    </a>
                </td>
            </tr>


        <?php endforeach; ?>
        <tr>
            <th class="center"><i>Total</i></th>
            <th class="center"><i><?php echo $sumDirectores; ?></i></th>
            <th class="center"><i><?php echo $sum_sin_director_oficial; ?></i></th>
            <th class="center"><i><?php echo $sum_con_director_oficial; ?></i></th>
            <th class="center"><i><?php echo $sum_sin_director_privado; ?></i></th>
            <th class="center"><i><?php echo $sum_con_director_privado; ?></i></th>
            <th class="center"><i><?php echo $sum_sin_director_otros; ?></i></th>
            <th class="center"><i><?php echo $sum_con_director_otros; ?></i></th>
        </tr>


    <?php endif; ?>
    </tbody>

</table>
<span class="small">Reporte: <?php echo date("d-m-Y"); ?></span> 
        
       