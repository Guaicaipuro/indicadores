<?php
/* @var $this AutoridadesplantelController */

$this->breadcrumbs = array(
    'Control' => '/control',
    'Autoridades de Plantel',
);

if(Yii::app()->user->pbac('control.matriculacionReporte.read') AND Yii::app()->user->pbac('control.matriculacionReporte.write') AND Yii::app()->user->pbac('control.matriculacionReporte.admin')){
    $levelMatricula = 'estado';
    $dependencyMatricula = Yii::app()->user->estado;
}
else {
    $levelMatricula='municipio';
    $dependencyMatricula = Yii::app()->user->estado;
}
if(Yii::app()->user->pbac('control.matriculacionReporte15.read') AND Yii::app()->user->pbac('control.matriculacionReporte15.write') AND Yii::app()->user->pbac('control.matriculacionReporte15.admin')){
    $levelMatricula15 = 'estado';
    $dependencyMatricula15 = Yii::app()->user->estado;
}
else {
    $levelMatricula15='municipio';
    $dependencyMatricula15 = Yii::app()->user->estado;
}
if(Yii::app()->user->pbac('control.autoridadPlantel.read') AND Yii::app()->user->pbac('control.autoridadPlantel.write') AND Yii::app()->user->pbac('control.autoridadPlantel.admin')){
    $level = 'estado';
    $dependency = Yii::app()->user->estado;
}
else {
    $level='municipio';
    $dependency = Yii::app()->user->estado;
}
?>

<input type="hidden" name="level" id="level" value="<?php echo $level;?>">
<input type="hidden" name="dependency" id="dependency" value="<?php echo $dependency;?>">
<input type="hidden" name="levelMatricula" id="levelMatricula" value="<?php echo $levelMatricula;?>">
<input type="hidden" name="dependencyMatricula" id="dependencyMatricula" value="<?php echo $dependencyMatricula;?>">
<input type="hidden" name="levelMatricula15" id="levelMatricula15" value="<?php echo $levelMatricula15;?>">
<input type="hidden" name="dependencyMatricula15" id="dependencyMatricula15" value="<?php echo $dependencyMatricula15;?>">
<div class="col-xs-12">
    <div class="row row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#directores">Reportes</a>
                </li>
            </ul>

            <div class="tab-content">

                <div id="directores"  class="tab-pane active">

                    <div class="widget-box">

                        <div class="widget-header">
                            <h5>Reportes Estadísticos por planteles </h5>

                            <div class="widget-toolbar">
                                <a href="#" data-action="collapse">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">

                            <div class="widget-body-inner">

                                <div class="widget-main form">
                                    
                                    

                                    <div class="row-fluid" style="text-align: right;">
                           

                                        <div class="btn-group" >
                                            
                                            
                               
                                            <button class="btn btn-sm dropdown-toggle" data-toggle="dropdown">
                                                Tipo de Reporte
                                                <span id="selectTipoReporteText"></span>
                                                <span class="icon-caret-down icon-on-right"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-info pull-right">
                                                <li>
                                                    <a id="repEstadistico">Reporte General de Planteles &nbsp;<i class="fa fa-file-text-o"></i></a>
                                                </li>
                                       
                                        
                                                <?php if ((Yii::app()->user->pbac('write') || Yii::app()->user->pbac('read') ) || Yii::app()->user->pbac('admin')): ?>
                                                    <li>

                                                        <a id="repEstadisticoMatricula">Reporte Estadístico Matriculación 2013-1014&nbsp;<i class="fa fa-file-text-o"></i></a>

                                                    </li>
                                                    <li>
                                                        <a id="repEstadisticoMatricula15">Reporte Estadístico Matriculación 2014-2015 &nbsp;<i class="fa fa-file-text-o"></i></a>
                                                    </li>
                                           
                                                <?php endif; ?>
                                                    
                                            </ul>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div id="resultado-script" class="row-fluid hide"></div>


                                    <div class="space-6"></div>

                                    <div class="row row-fluid hide" id="fechaCondicion">
                                        <div class="row-fluid col-md-12">

                                            <?php
                                            $fechaActual = date('d-m-Y');

                                            echo CHtml::textField('fecha_desde', "$fechaActual", array('size' => 10, 'maxlength' => 10, 'id' => 'fecha_desde', 'style' => 'width:200px;padding-top:2px;', 'readOnly' => 'readOnly'));
                                            ?>

                                            <button style="padding-top: 3px; padding-bottom: 2px;" type="button" class="btn btn-info btn-xs" id="busqueda_fecha">
                                                <i class="icon-search"></i>
                                                Consultar
                                            </button>

                                        </div>
                                    </div>

                                    <div class="space-6"></div>

                                    <div id="resultado" class="row-fluid" style="min-height: 300px;">

                                        <div class="infoDialogBox">
                                            <p>
                                                Seleccione el Tipo de Reporte que desea consultar.
                                            </p>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<div id="dialog-contacto" class="hide">
    <div class="center">
        <img src="<?php echo Yii::app()->baseUrl; ?>/public/images/ajax-loader-red.gif">
    </div>
</div>

<div id="dialog-observacion" class="hide">
    <form id="form-control-zona-directores">
        <div id="autoridadZonaContacto" class="tab-pane active">

            <div id="resultadoControlZonaDirectores">
                <div class="infoDialogBox">
                    <p>
                        Todos los campos con <span class="required">*</span> son requeridos. 
                    </p>
                </div>
            </div>

            <div class="widget-box">

                <div class="widget-header" style="border-width: thin">
                    <h5>Observación de Control</h5>

                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="icon-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div class="widget-body">

                    <div class="widget-body-inner">

                        <div class="widget-main form">

                            <div class="row">

                                <div class="row-fluid">

                                    <div class="col-md-12">
                                        <label for="observacion" class="col-md-12">Observación <span class="required">*</span></label>
                                        <textarea id="control_zona_observacion" name="control_zona_observacion" maxlength="400" style="width: 99%;"></textarea>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </form>
</div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/control/autoridadesPlantel.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jchart/jchart.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jchart/modules/exporting.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jchart/themes/grid.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/control/directoresDiario.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/control/matriculaReporte.js', CClientScript::POS_END); ?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/control/matriculaReporte15.js', CClientScript::POS_END); ?>

