
<div id="indexConsultarMatricula">
    <?php
    /* @var $this AtencionSolicitudController */

    $this->breadcrumbs = array(
        // 'Asignación de Seriales' => array("../../titulo/atencionSolicitud"),
        'Consultar Matricula'
    );
    ?>

    <div class = "widget-box">

        <div class = "widget-header">
            <h5>Consultar Matricula</h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">
                    <div id="errorConsultaMatricula" class="hide errorDialogBox" ><p></p> </div>

                    <div id="informacion">
<!--                        <div id="resultado" class="row-fluid" style="min-height: 300px;"></div>-->
                        <div id="resultadoOperacion"></div>
                        <div class="infoDialogBox">
                            <p>
                                Por favor ingrese todos los datos para su busqueda.
                            </p>
                        </div>
                        <div id="alertaConsultarMatricula"></div>
                    </div>

                    <div class="row row-fluid" style="padding-bottom: 20px; padding-top: 20px">

                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'busqueda_form',
                            // 'action' => 'atencionSolicitud/mostrarConsultaPlantel'
                        ));
                        ?>
<!--                        <div id="1eraFila" class="col-md-12">-->
                        <div class="row">

                            <div class="col-md-12">
                                <div class="col-md-5">
                                    <?php echo CHtml::label('Período <span class="required">*</span>','periodo_id'); ?>
                                    <!--                                PeriodoEscolar::model()->findAll(array('limit'=>50))-->
<!--                                    --><?php //echo CHtml::dropDownList('periodoId', 'id', CHtml::listData(PeriodoEscolar::model()->findAll(), 'id', 'periodo'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",
//                                        'ajax'=>array(
//                                        'type'=>'POST',
//                                        'url'=>CController::createUrl('matriculaPlantel/loadGrado'),
//                                        'update'=>'#gradoId',
//                                        )
//                                    )); ?>
                                    <?php echo CHtml::dropDownList('periodoId', 'id', CHtml::listData(PeriodoEscolar::model()->findAll(
                                        array("condition"=>"id >= :periodoId",
                                            'order' => 'id DESC',
                                            'params'=>array(':periodoId'=>14)
                                        )),
                                        'id', 'periodo'),
                                        array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required"
                                        , 'ajax' => array(
                                                'type' => 'GET',
                                                'url' => CController::createUrl('matriculaPlantel/loadGradoSeccion'),
                                                'update' => '#seccionPlantelId',
                                        )
                                    )); ?>
                                    <!--                                --><?php //echo $form->dropDownList($model, 'tipo_personal_id', CHtml::listData(TipoPersonal::model()->findAll(array('limit'=>50)), 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                </div>

                                <div class="col-md-5" >
                                    <?php echo CHtml::label('Codigo del Plantel <span class="required">*</span>', '', array("class" => "col-md-12", 'id' => 'codigo_Plantel_id')); ?><span ></span>
                                    <div id="campoBusqueda" >
                                        <!--                                    --><?php //echo CHtml::textField('codigoPlantel', '', array('class' => 'span-7', 'maxlength' => 10, 'style' => 'width: 90%', 'id' => 'codigoPlantel_id')); ?>
                                        <?php echo CHtml::textField('codPlantel', '', array('class' => 'span-12', 'maxlength' => 10, 'id' => 'codPlantel', 'prompt'=>'- - -'
                                        ,  "required"=>"required"
                                            , 'ajax' => array(
                                                'type' => 'GET',
                                                'url' => CController::createUrl('matriculaPlantel/loadGradoSeccion'),
                                                'update' => '#seccionPlantelId',
                                            )
                                        )); ?>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-10">
                                    <?php echo CHtml::label('Seccion <span class="required">*</span>','grado_id'); ?>
                                    <!--                                --><?php //echo CHtml::dropDownList('gradoID', 'id', CHtml::listData(Grado::model()->findAll(), 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                    <?php echo CHtml::dropDownList('seccionPlantelId', 'id', array(), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required"
//                                        'ajax'=>array(
//                                        'type'=>'POST',
//                                        'url'=>CController::createUrl('matriculaPlantel/loadSeccion'),
//                                        'update'=>'#seccionId',
//                                    )
                                    )); ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-10">
                                    <?php echo CHtml::label('&nbsp;&nbsp;', '', array("class" => "col-md-12")); ?>

                                    <button  id = "btnConsultar"  class="btn btn-primary btn-sm" type="submit" data-last="Finish">
                                        <i class="fa fa-search icon-on-right"></i>
                                        Consultar
                                    </button>
                                </div>
                            </div>

                        </div>
                        <div class = "col-md-12"><div class = "space-6"></div></div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
</div>

<div id="ResultMatricula"  class="hide">

</div>
<hr>
<div class="col-md-6" style="padding-top: 1%">
    <?php
    $getUrl= Yii::app()->request->urlReferrer;
    if (empty($getUrl)){
        $getUrl= Yii::app()->request->getBaseUrl(true);
    }
    ?>
    <a id="btnRegresar" href="<?php echo $getUrl; ?>"class="btn btn-danger">
        <i class="icon-arrow-left"></i>
        Volver
    </a>
</div>
<?php
//Yii::app()->clientScript->registerScriptFile(
//    //Yii::app()->request->getBaseUrl(true). '/public/js/modules/titulo/consulta/serial.js',CClientScript::POS_END
//      Yii::app()->request->getBaseUrl(true). '/public/js/modules/control/matriculaPlantel/consulta.js',CClientScript::POS_END
//);
//?>

<?php
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->getBaseUrl(true).'/public/js/modules/control/matriculaPlantel/consulta.js',CClientScript::POS_END
);

?>
<?php
//Yii::app()->clientScript->registerScriptFile(
//    Yii::app()->request->getBaseUrl(true). '/public/js/modules/titulo/consulta/serial.js',CClientScript::POS_END
//);
//?>
