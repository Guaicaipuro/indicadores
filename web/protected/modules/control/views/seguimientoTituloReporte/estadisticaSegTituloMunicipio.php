<?php
/**
 * Created by PhpStorm.
 * User: mari
 * Date: 21/07/15
 * Time: 09:18 AM
 */
?>
<div class="breadcrumb row row-fluid" style="margin-left: 2px;">
<li>
<a onclick="repEstadisticoTitulo();">Estado</a>
</li>
</div>
<div class = "pull-left" style ="padding-left: 20px; padding: 20px">

    <?php //if (Yii::app()->user->pbac('admin')): ?>
    <!--        <a  href="--><?php ////echo Yii::app()->createUrl("/control/autoridadesPlantel/reporteTotalDirectores/"); ?><!--"  id="reporteAutoridadesTotal"   class = "btn btn-primary btn-next btn-sm ">-->
    <!--            Exportar Totales-->
    <!--            <i class="fa fa-file-text-o icon-on-right"></i>-->
    <!--        </a>-->
    <?php //endif; ?>
</div>
<?php
$sum_seriales_plantel = 0;
$sum_seriales_estudiante = 0;




foreach ($dataReport as $data):
    $sum_seriales_plantel = $sum_seriales_plantel + $data['cant_seriales_plantel'];
    $sum_seriales_estudiante = $sum_seriales_estudiante + $data['cant_seriales_estudiante'];

    ?>        <?php endforeach; ?>



<table class="report table table-striped table-bordered table-hover">

    <thead>
    <tr><th colspan="8"><center><b>Seguimiento de título</b></center></th></tr>
    <tr>
    <tr>
        <th nowrap rowspan="2" class="center">
            <?php echo $titulo; ?>
        </th>

        <th title="Total de Seriales Entregados a Planteles" rowspan="2" class="center">
            Total de Seriales Entregados a Planteles
        </th>

        <th title="Total de Seriales Entregados a Estudiantes" rowspan="2" class="center">
            Total de Seriales Entregados a Estudiantes
        </th>


    </tr>

    </thead>

    <tbody>
    <?php if(empty($dataReport)): ?>
        <tr>
            <td colspan="11">
                <div class="alertDialogBox" style="margin-top: 10px;">
                    <p>
                        No se han encontrado Registros.
                    </p>
                </div>
            </td>
        </tr>
    <?php else: ?>

        <?php foreach ($dataReport as $data): ?>


            <tr>

                <td class="center">
<!--                    <a onclick="mostrarTitulosParroquia('--><?php //echo base64_encode($data['municipio_id']); ?><!--//','<?php //echo base64_encode($estado_id); ?>//');" id="<?php //echo base64_encode($data['municipio_id']); ?><!--">-->
                        <?php echo $data['municipio']; ?>
<!--                    </a>-->
                </td>
                <td class="center">
                    <a href="/control/seguimientoTituloReporte/reporteCsvEstado/municipio/<?php echo base64_encode($data['municipio_id']); ?>/control/<?php echo base64_encode('MP'); ?>">
                        <?php echo $data['cant_seriales_plantel']; ?>
                    </a>
                </td>
                <td class="center">
                    <a href="/control/seguimientoTituloReporte/reporteCsvEstado/municipio/<?php echo base64_encode($data['municipio_id']); ?>/control/<?php echo base64_encode('ME'); ?>">
                        <?php echo $data['cant_seriales_estudiante']; ?>
                    </a>
                </td>

            </tr>


        <?php endforeach; ?>
        <tr>
            <th class="center"><i>Total</i></th>
            <th class="center"><i><?php echo $sum_seriales_plantel; ?></i></th>
            <th class="center"><i><?php echo $sum_seriales_estudiante; ?></i></th>

        </tr>


    <?php endif; ?>
    </tbody>

</table>
<span class="small">Reporte: <?php echo date("d-m-Y"); ?></span>

