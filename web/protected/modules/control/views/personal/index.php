<?php
/* @var $this PersonalController */

$this->breadcrumbs=array(
    'Control' => '/control',
	'Personal de Planteles',
);
?>
<div class="col-xs-12">
    <div class="row row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#congresos">Reportes</a>
                </li>
            </ul>

            <div class="tab-content">

                <div id="congresos"  class="tab-pane active">

                    <div class="widget-box">

                        <div class="widget-header">
                            <h5>Personal Registrado por Estado</h5>

                            <div class="widget-toolbar">
                                <a href="#" data-action="collapse">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">

                            <div class="widget-body-inner">

                                <div class="widget-main form">

                                    <div class="row-fluid" style="text-align: right;">

                                        <div class="btn-group" >

                                            <button class="btn btn-sm dropdown-toggle" data-toggle="dropdown">
                                                Tipo de Reporte
                                                <span id="selectTipoReporteText"></span>
                                                <span class="icon-caret-down icon-on-right"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-info pull-right">
                                                <li>
                                                    <a id="repEstadistico">Reporte General &nbsp;<i class="fa fa-file-text-o"></i></a>
                                                </li>

                                            </ul>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div id="resultado-script" class="row-fluid hide"></div>


                                    <div class="space-6"></div>

                                    <div class="row row-fluid hide" id="fechaCondicion">
                                        <div class="row-fluid col-md-12">

                                            <button style="padding-top: 3px; padding-bottom: 2px;" type="button" class="btn btn-info btn-xs" id="busqueda_fecha">
                                                <i class="icon-search"></i>
                                                Consultar
                                            </button>

                                        </div>
                                    </div>

                                    <div class="space-6"></div>

                                    <div id="resultado" class="row-fluid" style="min-height: 300px;">

                                        <div class="infoDialogBox">
                                            <p>
                                                Seleccione el Tipo de Reporte que desea consultar.
                                            </p>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/control/personal.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jchart/jchart.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jchart/modules/exporting.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jchart/themes/grid.js', CClientScript::POS_END); ?>

