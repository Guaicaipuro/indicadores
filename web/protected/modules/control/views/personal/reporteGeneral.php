<div class="pull-left" style="padding: 0px 0px 20px 20px">

    <?php if (Yii::app()->user->pbac('control.personal.admin')): ?>
        <a href="<?php echo Yii::app()->createUrl("/control/personal/reportePersonalEstado/"); ?>" class="btn btn-primary btn-next btn-sm">
            Exportar Totales
            <i class="fa fa-file-text-o icon-on-right"></i>
        </a>
    <?php endif; ?>

</div><br>

<table class="report table table-striped table-bordered table-hover">
    <thead>
    <tr><th colspan="8" class="center"><b>Personal Registrado por Estado</b></th></tr>
    <tr>
        <th class="center">Estado</th>
        <th class="center">Personal</th>
        <th class="center">Administrativo</th>
        <th class="center">Docente</th>
        <th class="center">Obrero</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $sumPers= 0;
    $sumAdmi = 0;
    $sumDoce = 0;
    $sumObre = 0;
    foreach ($dataReport as $data):
        $sumPers += $data['personal'];
        $sumAdmi += $data['administrativo'];
        $sumDoce += $data['docente'];
        $sumObre += $data['obrero'];
        ?>
        <tr title="<?php echo "Estado: ".$data['estado']." | Cantidad: ".$data['personal']; ?>" class="text-info">
            <td class="center">
                <?php echo $data['estado']; ?>
            </td>
            <td class="center">
                <?php echo $data['personal']; ?>
            </td>
            <td class="center">
                <?php echo $data['administrativo']; ?>
            </td>
            <td class="center">
                <?php echo $data['docente']; ?>
            </td>
            <td class="center">
                <?php echo $data['obrero']; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <th class="center">
            <i>Total de Personal Registrado</i>
        </th>
        <th class="center">
            <i><?php echo $sumPers; ?></i>
        </th>
        <th class="center">
            <i><?php echo $sumAdmi; ?></i>
        </th>
        <th class="center">
            <i><?php echo $sumDoce; ?></i>
        </th>
        <th class="center">
            <i><?php echo $sumObre; ?></i>
        </th>
    </tr>
    </tbody>
</table>
<span class="small">Reporte: <?php echo $fechaHora; ?></span>