<?php

/**
 * Description of DirectoresPlantel
 *
 * @author Jean Carlos Barboza
 */
class Matriculacion15 extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gplantel.autoridad_plantel';
    }

    public function reporteEstadisticoMatriculado($level, $dependency_id) {
        if ($level == 'estado') {
            $where = 'e.id != 45'; // Excluye Dependencias Federales (Id=45)
            $sql = "SELECT e.id as id, ie.estado as nombre, COUNT(ie.cod_plantel) as planteles
                    ,
                    SUM(cantidad_estudiante_total) AS cantidad_estudiante,
                    SUM(cantidad_estudiante_inicial) AS cant_estudiante_inicial,
                    SUM(cantidad_estudiante_primaria) AS cant_estudiante_primaria,
                    SUM(cantidad_estudiante_otros) AS cant_estudiante_otros,
                    SUM(cantidad_estudiante_adulto) AS cant_estudiante_adulto,
                    SUM(cantidad_estudiante_especial) AS cant_estudiante_especial
                FROM control.matricula_plantel_periodo_15 ie
                  INNER JOIN public.estado e
                    ON e.nombre = ie.estado
                    LEFT JOIN public.municipio m ON (m.estado_id=e.id AND m.nombre=ie.municipio)

                WHERE $where
                GROUP BY ie.estado, e.id
                ORDER BY ie.estado ASC

";
        } else {

            $where = 'e.id = ' . $dependency_id;
            $sql = "SELECT m.id as id, m.nombre as nombre, COUNT(ie.cod_plantel) as planteles
                    ,
                    SUM(cantidad_estudiante_total) AS cantidad_estudiante,
                    SUM(cantidad_estudiante_inicial) AS cant_estudiante_inicial,
                    SUM(cantidad_estudiante_primaria) AS cant_estudiante_primaria,
                    SUM(cantidad_estudiante_otros) AS cant_estudiante_otros,
                     SUM(cantidad_estudiante_adulto) AS cant_estudiante_adulto,
                    SUM(cantidad_estudiante_especial) AS cant_estudiante_especial
                FROM control.matricula_plantel_periodo_15 ie
                  INNER JOIN public.estado e
                    ON e.nombre = ie.estado
                    LEFT JOIN public.municipio m ON (m.estado_id=e.id AND m.nombre=ie.municipio)

                WHERE $where
                GROUP BY m.id,m.nombre
                ORDER BY m.nombre ASC

";
        }
        $connection = Yii::app()->dbEstadistica;
        $command = $connection->createCommand($sql);
        $resultado = $command->queryAll();

        return $resultado;
    }


    public function reporteEstadisticoMatriculadoGral($level, $dependency_id = null) {

        $resultado = array();
        $periodo_Escolar_id = 15;

        if (in_array($level, array( 'estado', 'municipio'))) {

            if ($level == 'estado') {
                $camposSeleccionados = "e.id AS id, e.nombre, 'AAA'||e.nombre AS titulo , count(DISTINCT ie.plantel) as planteles ";
                $camposSeleccionadosTotales = "0 AS id, 'TOTAL' AS nombre , 'ZZZTOTAL' AS titulo, count(DISTINCT ie.plantel) as planteles";

                $camposAgrupados = "ie.estado, e.id, e.nombre, titulo";
                $where = 'ie.estado != 45'; // Excluye Dependencias Federales (Id=45)
                $orderBy = 'titulo ASC, nombre ASC';
            } elseif ($level == 'municipio') {

                $camposSeleccionados = "e.id AS estado_id, m.id, m.nombre, 'AAA'||m.nombre AS titulo ,count(DISTINCT ie.plantel) as planteles ";
                $camposSeleccionadosTotales = "0 AS estado_id, 0 AS id, 'TOTAL' AS nombre , 'ZZZTOTAL' AS titulo, count(DISTINCT ie.plantel) as planteles";
                $camposAgrupados = "ie.estado, e.id, e.nombre, m.id, m.nombre, titulo  ";
                $where = 'ie.estado = ' . $dependency_id;
                $orderBy = 'titulo ASC';
            }

            $sql = "SELECT  $camposSeleccionados
                    ,
                    SUM(CASE WHEN (ie.inscripcion_id IS NOT NULL) THEN 1 ELSE 0 END) AS cantidad_estudiante,
                    SUM(CASE WHEN (ie.inscripcion_id IS NOT NULL AND ie.nivel_id=1) THEN 1 ELSE 0 END) AS cant_estudiante_inicial,
                    SUM(CASE WHEN (ie.inscripcion_id IS NOT NULL AND ie.nivel_id=2) THEN 1 ELSE 0 END) AS cant_estudiante_primaria,
                    SUM(CASE WHEN (ie.inscripcion_id IS NOT NULL AND ie.nivel_id not in (1,2)) THEN 1 ELSE 0 END) AS cant_estudiante_otros
                FROM control.matricula_15 ie     
                LEFT JOIN public.estado e 
                    ON e.id = ie.estado

                LEFT JOIN public.municipio m
                    ON ie.municipio = m.id
                WHERE
                    $where AND ie.periodo_id=$periodo_Escolar_id
                GROUP BY
                    $camposAgrupados
                UNION

                SELECT  $camposSeleccionadosTotales
                    ,
                    SUM(CASE WHEN (ie.inscripcion_id IS NOT NULL) THEN 1 ELSE 0 END) AS cantidad_estudiante,
                    SUM(CASE WHEN (ie.inscripcion_id IS NOT NULL AND ie.nivel_id=1) THEN 1 ELSE 0 END) AS cant_estudiante_inicial,
                    SUM(CASE WHEN (ie.inscripcion_id IS NOT NULL AND ie.nivel_id=2) THEN 1 ELSE 0 END) AS cant_estudiante_primaria,
                    SUM(CASE WHEN (ie.inscripcion_id IS NOT NULL AND ie.nivel_id not in (1,2)) THEN 1 ELSE 0 END) AS cant_estudiante_otros

                FROM control.matricula_15 ie
                WHERE
                    $where AND ie.periodo_id=$periodo_Escolar_id
                ORDER BY
                    $orderBy";

            //echo "<pre><code>$sql</code></pre>";
            //die();
            $connection = Yii::app()->dbEstadistica;
            //$command = $connection->createCommand($sql);
            if ($level == 'municipio') {
                $dependency = new CDbCacheDependency('SELECT MAX(ie.estudiante_id) FROM control.matricula_15 ie WHERE ie.estado = ' . $dependency_id);
            } else {
                $dependency = new CDbCacheDependency('SELECT MAX(ie.estudiante_id) FROM control.matricula_15 ie');
            }
            $command   = Yii::app()->dbEstadistica->cache(28800, $dependency)->createCommand($sql);
            //secho $sql;
            //echo "<br>";
            //echo "<br>";
            //echo "estoy en reporteEstadistico";
            //die();
            $resultado = $command->queryAll();
        }

        return $resultado;
    }


    public function reporteDetalladoPlantelMatriculaConteo($column, $level, $dependency, $fecha = null, $orderBy = null) {
        //    var_dump($level);die();
        $periodo_Escolar_id = 15;
        $columnFilter = $this->getColumnFilter($column, $fecha, $dependency, $periodo_Escolar_id);
        $drillDownFilter = $this->getGeoDrillDownFilter($level, $dependency);
        //   var_dump($drillDownFilter);die();
        //$where = $drillDownFilter . ' AND ' . $columnFilter;
        $where = $drillDownFilter;

        $group_by = "p.cod_plantel, p.cod_estadistico,denominacion, p.correo, p.condicion_estudio, p.nombre, zona_educativa,tipo_dependencia,p.estatus,fundacion,
    p.estado,p.municipio,p.parroquia, p.direccion, p.dir_email, p.telefono_fijo, p.telefono_otro, zona_ubicacion,clase_plantel,
    categoria, tipo_matricula,turno, modalidad,dir_cedula,dir_nombre,dir_apellido, dir_usuario,

    dir_telefono,dir_celular, dir_email,dir_twitter";

        if (is_null($orderBy)) {
            $orderBy = 'p.cod_plantel,'
                . 'p.nombre, '
                . 'p.estado, '
                . 'p.municipio, '
                . 'p.parroquia, '
                . 'p.fundacion';

        }

        $sql = "SELECT DISTINCT
                    p.cod_plantel,
                    p.cod_estadistico,
                    p.denominacion AS denominacion,
                    p.nombre,
                    p.zona_educativa AS zona_educativa,
                    p.tipo_dependencia AS tipo_dependencia,
                    p.estatus AS estatus,
                    p.fundacion AS fundacion,
                    p.estado AS estado,
                    p.municipio AS municipio,
                    p.parroquia AS parroquia,

                    p.direccion,
                    p.correo,
                    p.telefono_fijo,
                    p.telefono_otro,
                    p.zona_ubicacion AS zona_ubicacion,
                    p.clase_plantel AS clase_plantel,
                    p.categoria AS categoria,
                    p.condicion_estudio AS condicion_estudio,
                    p.tipo_matricula AS tipo_matricula,
                    p.turno AS turno,
                    p.modalidad AS modalidad,
                    p.dir_cedula AS dir_cedula,
                    p.dir_nombre AS dir_nombre,
                    p.dir_apellido AS dir_apellido,
                    p.dir_usuario AS dir_usuario,
                    p.dir_telefono AS dir_telefono,
                    p.dir_celular AS dir_celular,
                    p.dir_email AS dir_email,
                    p.dir_twitter AS dir_twitter,
                    
                    SUM(CASE WHEN (ie.inscripcion_id IS NOT NULL) THEN 1 ELSE 0 END) AS cantidad_estudiante,
                    SUM(CASE WHEN (ie.inscripcion_id IS NOT NULL AND ie.nivel_id=1) THEN 1 ELSE 0 END) AS cant_estudiante_inicial,
                    SUM(CASE WHEN (ie.inscripcion_id IS NOT NULL AND ie.nivel_id=2) THEN 1 ELSE 0 END) AS cant_estudiante_primaria,
                    SUM(CASE WHEN (ie.inscripcion_id IS NOT NULL AND ie.nivel_id not in (1,2)) THEN 1 ELSE 0 END) AS cant_estudiante_otros
                FROM
                    control.autoridad_plantel p
                LEFT JOIN public.estado es ON p.estado = es.nombre
                LEFT JOIN public.municipio mc ON p.municipio = mc.nombre
                LEFT JOIN public.parroquia pq ON p.parroquia = pq.nombre
                LEFT JOIN control.matricula_15 ie ON ie.plantel=p.cod_plantel

                WHERE
                    $where
                GROUP BY $group_by
                ORDER BY
                    $orderBy";

        $connection = Yii::app()->dbEstadistica;
//        $dependency = new CDbCacheDependency('SELECT MAX(p.cod_plantel) FROM control.autoridad_plantel p
//                LEFT JOIN public.estado es ON p.estado = es.nombre
//                LEFT JOIN public.municipio mc ON p.municipio = mc.nombre
//                LEFT JOIN control.matricula_15 ie ON ie.plantel=p.cod_plantel
//                LEFT JOIN public.parroquia pq ON p.parroquia = pq.nombre WHERE ' . $where);
        //$command = $connection->createCommand($sql);
        //  $command   = Yii::app()->dbEstadistica->cache(28800, $dependency)->createCommand($sql);

        $command = $connection->createCommand($sql);

        //     echo "$sql";
        //    die();
        $resultado = $command->queryAll();
//         var_dump($resultado);
        //        die();

        return $resultado;
    }


    private function getColumnFilter($column, $fecha = null, $dependency, $periodo_Escolar_id) {

        switch ($column) {
            case 'planteles':
                $where = "p.cod_plantel IS NOT NULL";

                break;
            case'matricula':
                $where = "p.estatus= 'ACTIVO' ";
                break;
            case'matricula_inicial':
                $where = "p.estatus= 'ACTIVO' AND ie.nivel_id=1 AND ";
                break;
            case'matricula_primaria':
                $where = "p.estatus= 'ACTIVO' AND ie.nivel_id=2";
                break;
            case'matricula_otros':
                $where = "p.estatus= 'ACTIVO' AND ie.nivel_id not in (1,2)";
                break;
            case 'con_director':
                $where = "p.cod_plantel IS NOT NULL AND p.dir_nombre IS NOT NULL";
                break;
            case 'publ_sin_director':
                $where = "p.cod_plantel IS NOT NULL AND p.dir_nombre IS NULL AND p.tipo_dependencia_id IN ('Nacional', 'Estadal', 'Municipal', 'Autonoma')";
                break;
            case 'publ_con_director':
                $where = "p.cod_plantel IS NOT NULL AND p.dir_nombre IS NOT NULL AND p.tipo_dependencia IN ('Nacional', 'Estadal', 'Municipal', 'Autonoma')";
                break;
            case 'priv_sin_director':
                $where = "p.cod_plantel IS NOT NULL AND p.dir_nombre IS NULL AND p.tipo_dependencia = 'Privada'";
                break;
            case 'priv_con_director':
                $where = "p.cod_plantel IS NOT NULL AND p.dir_nombre IS NOT NULL AND p.tipo_dependencia = 'Privada'";
                break;
            case 'otros_sin_director':
                $where = "p.cod_plantel IS NOT NULL AND p.dir_nombre IS NULL AND p.tipo_dependencia NOT IN ('Nacional', 'Estadal', 'Municipal', 'Autonoma', 'Privada')";
                break;
            case 'otros_con_director':
                $where = "p.cod_plantel IS NOT NULL AND p.dir_nombre IS NOT NULL AND p.tipo_dependencia NOT IN ('Nacional', 'Estadal', 'Municipal', 'Autonoma', 'Privada')";
                break;
            case 'con_director_fecha':
                //$where = "p.cod_plantel IS NOT NULL AND p.dir_nombre IS NOT NULL AND to_char(ap.fecha_ini,'YYYY-MM-DD')='$fecha'";//maikel
                break;
            default:
                $where = "p.cod_plantel IS NULL";

                break;
        }

        return $where;
    }

    //*******FUNCION OPTIMIZADA PARA REPORTE DETALLADO


    public function reporteDetalladoPlantelMatricula($column, $level, $dependency, $fecha = null, $orderBy = null) {
        $periodo_Escolar = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_Escolar_id = $periodo_Escolar['id'];
        //$columnFilter = $this->getColumnFilter($column, $fecha, $dependency, $periodo_Escolar_id);
        $drillDownFilter = $this->getGeoDrillDownFilter($level, $dependency);
        $mat_inicial = ' ,(SELECT COUNT(DISTINCT ie.estudiante_id) FROM control.matricula ie WHERE ie.plantel=p.cod_plantel AND ie.periodo_id=$periodo_Escolar_id AND ie.nivel_id=$level) AS cantidad_estudiante_inicial ';
        //$where = $drillDownFilter . ' AND ' . $columnFilter;
        $where = $drillDownFilter;

        $group_by = "p.cod_plantel, p.cod_estadistico,denominacion, p.correo, p.condicion_estudio, p.nombre, zona_educativa,tipo_dependencia,p.estatus,fundacion,
    p.estado,p.municipio,p.parroquia, p.direccion, p.dir_email, p.telefono_fijo, p.telefono_otro, zona_ubicacion,clase_plantel,
    categoria, tipo_matricula,turno, modalidad,dir_cedula,dir_nombre,dir_apellido, dir_usuario,
    dir_telefono,dir_celular, dir_email,dir_twitter, p.cantidad_estudiante_inicial, p.cantidad_estudiante_primaria,p.cantidad_estudiante_otros, p.cantidad_estudiante_total,p.cantidad_estudiante_adulto,p.cantidad_estudiante_especial";

        if (is_null($orderBy)) {
            $orderBy = 'p.cod_plantel,'
                . 'p.nombre, '
                . 'p.estado, '
                . 'p.municipio, '
                . 'p.parroquia, '
                . 'p.parroquia, '
                . 'p.fundacion';
        }

        $sql = "SELECT DISTINCT
                    p.cod_plantel,
                    p.cod_estadistico,
                    p.denominacion AS denominacion,
                    p.nombre,
                    p.zona_educativa AS zona_educativa,
                    p.tipo_dependencia AS tipo_dependencia,
                    p.estatus AS estatus,
                    p.fundacion AS fundacion,
                    p.estado AS estado,
                    p.municipio AS municipio,
                    p.parroquia AS parroquia,
                    p.direccion,
                    p.correo,
                    p.telefono_fijo,
                    p.telefono_otro,

                    p.zona_ubicacion AS zona_ubicacion,
                    p.clase_plantel AS clase_plantel,
                    p.categoria AS categoria,
                    p.condicion_estudio AS condicion_estudio,
                    p.tipo_matricula AS tipo_matricula,
                    p.turno AS turno,
                    p.modalidad AS modalidad,
                    p.dir_cedula AS dir_cedula,
                    p.dir_nombre AS dir_nombre,
                    p.dir_apellido AS dir_apellido,
                    p.dir_usuario AS dir_usuario,
                    p.dir_telefono AS dir_telefono,
                    p.dir_celular AS dir_celular,
                    p.dir_email AS dir_email,
                    p.dir_twitter AS dir_twitter,
                    p.cantidad_estudiante_inicial,
                    p.cantidad_estudiante_primaria,
                    p.cantidad_estudiante_otros,
                    p.cantidad_estudiante_adulto,
                    p.cantidad_estudiante_especial,
                    p.cantidad_estudiante_total
                FROM
                    control.matricula_plantel_periodo_15 p
                LEFT JOIN public.estado es ON p.estado = es.nombre
                LEFT JOIN public.municipio mc ON p.municipio = mc.nombre AND es.id=mc.estado_id
             
                WHERE 
                $where "
            . " order by
                p.cantidad_estudiante_total DESC";
        $connection = Yii::app()->dbEstadistica;
//        $dependency = new CDbCacheDependency('SELECT MAX(p.cod_plantel) FROM control.autoridad_plantel p
//                LEFT JOIN public.estado es ON p.estado = es.nombre
//                LEFT JOIN public.municipio mc ON p.municipio = mc.nombre
//                LEFT JOIN control.matricula ie ON ie.plantel=p.cod_plantel
//                LEFT JOIN public.parroquia pq ON p.parroquia = pq.nombre WHERE ' . $where);
        $command = $connection->createCommand($sql);
        //  $command = Yii::app()->dbEstadistica->cache(28800, $dependency)->createCommand($sql);

        $resultado = $command->queryAll();
        // var_dump($resultado);
        // die();

        return $resultado;
    }




    private function getGeoDrillDownFilter($level, $dependency) {

        switch ($level) {

            /*
                        case 'region':
                            if ((int) $dependency !== 0)
                                $where = 'es.region_id = ' . $dependency . ' AND es.id != 45'; //No incluye Dependencias Federales (45)
                            else
                                $where = 'es.id != 45'; //No incluye Dependencias Federales (45)
                            break;

            */
            case 'estado':
                $where = 'es.id = ' . $dependency . ' AND es.id != 45'; //No incluye Dependencias Federales (45)
                break;

            case 'municipio':


                $where = 'mc.id = ' . $dependency . ' AND es.id != 45'; //No incluye Dependencias Federales (45)
                break;
            default:
                $where = '';

                break;
        }

        return $where;
    }

}

//        SELECT r.id, r.nombre, 'AAA'||r.nombre AS titulo,
//        SUM(CASE WHEN (p.id IS NOT NULL) THEN 1 ELSE 0 END) as plantel,

//        from region r
//        inner JOIN public.estado e ON r.id = e.region_id
//        LEFT JOIN gplantel.plantel p ON e.id = p.estado_id
//        LEFT JOIN public.municipio m ON p.municipio_id = m.id
//        WHERE 1 = 1
//        GROUP BY r.id, r.nombre
//        ORDER BY titulo ASC, nombre ASC
//        return $resultado;

