<?php
/**
 *
 * Author: Eliecer Chicott
 * Email: eachicot@gmail.com
 * Date: 23/04/2015
 *
 */

class CongresoPedagogico extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'gplantel.plantel';
    }

    public function relations()
    {
        return array(
            'gplantel.congreso_pedagogico'=>array(self::HAS_MANY, 'gplantel.congreso_pedagogico', 'plantel_id'),
            'estado_id'=>array(self::BELONGS_TO, 'estado', 'estado_id')
        );
    }

    public function reporteCongresosGeneral()
    {

        $sql = "SELECT e.nombre as Estado, COUNT(c.id) as Congresos FROM gplantel.plantel p
                INNER JOIN gplantel.congreso_pedagogico c ON p.id = c.plantel_id
                INNER JOIN estado e ON p.estado_id = e.id
                GROUP BY e.nombre
                ORDER BY e.nombre ASC";

        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $resultado = $command->queryAll();

        return $resultado;
    }
}