<?php

/**
 * Description of DirectoresPlantel
 *
 * @author Jose Gabriel Gonzalez
 */
class DirectoresPlantel extends CActiveRecord {


    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gplantel.autoridad_plantel';
    }
    
    public function reporteEstadisticoGeneral($nivel, $dependency_id){
        
       if ($nivel=='municipio'){ 
         $where = 'es.id= '.$dependency_id;
         $sql = "SELECT DISTINCT
  a.municipio,
  es.id as estado_id,
  mc.id as municipio_id,
 SUM(CASE WHEN  (a.dir_nombre IS NOT NULL) THEN 1 ELSE 0 END) AS total,
  SUM(CASE WHEN (a.cod_plantel IS NULL AND a.tipo_dependencia='Estadal' OR a.tipo_dependencia='Nacional' OR a.tipo_dependencia='Municipal' OR a.tipo_dependencia='Autonoma' ) THEN 1 ELSE 0 END) AS oficial,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.tipo_dependencia='Privada') THEN 1 ELSE 0 END) AS privado,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.tipo_dependencia <>'Privada' AND a.tipo_dependencia<>'Nacional' AND a.tipo_dependencia<>'Municipal' AND a.tipo_dependencia<>'Estadal' AND a.tipo_dependencia<>'Autonoma') THEN 1 ELSE 0 END) AS otros,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NOT NULL) THEN 1 ELSE 0 END) AS con_director_total,

  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NULL) THEN 1 ELSE 0 END) AS sin_director_total,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NULL AND (a.tipo_dependencia='Estadal' OR a.tipo_dependencia='Nacional' OR a.tipo_dependencia='Municipal' OR a.tipo_dependencia='Autonoma') ) THEN 1 ELSE 0 END) AS sin_director_oficial,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NULL AND a.tipo_dependencia='Privada') THEN 1 ELSE 0 END) AS sin_director_privado,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NULL AND a.tipo_dependencia <>'Privada' AND a.tipo_dependencia<>'Nacional' AND a.tipo_dependencia<>'Municipal' AND a.tipo_dependencia<>'Estadal' AND a.tipo_dependencia<>'Autonoma') THEN 1 ELSE 0 END) AS sin_director_otros,


  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_cedula::int IS NOT NULL AND (a.tipo_dependencia='Estadal' OR a.tipo_dependencia='Nacional' OR a.tipo_dependencia='Municipal' OR a.tipo_dependencia='Autonoma') ) THEN 1 ELSE 0 END) AS con_director_oficial,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NOT NULL AND a.tipo_dependencia='Privada') THEN 1 ELSE 0 END) AS con_director_privado,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NOT NULL AND (a.tipo_dependencia <>'Privada' AND  a.tipo_dependencia<>'Nacional' AND a.tipo_dependencia<>'Municipal' AND a.tipo_dependencia<>'Estadal' AND a.tipo_dependencia<>'Autonoma')) THEN 1 ELSE 0 END) AS con_director_otros
  

FROM 
control.autoridad_plantel a
INNER JOIN public.estado es ON a.estado = es.nombre
LEFT JOIN public.municipio mc ON a.municipio = mc.nombre AND es.id=mc.estado_id

where $where
GROUP BY a.municipio, es.id,mc.id
ORDER BY a.municipio;
";

       }else{
                    $sql = "SELECT DISTINCT
  a.estado, 
  es.id as estado_id,
 SUM(CASE WHEN  (a.dir_nombre IS NOT NULL) THEN 1 ELSE 0 END) AS total,
  SUM(CASE WHEN (a.cod_plantel IS NULL AND a.tipo_dependencia='Estadal' OR a.tipo_dependencia='Nacional' OR a.tipo_dependencia='Municipal' OR a.tipo_dependencia='Autonoma' ) THEN 1 ELSE 0 END) AS oficial,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.tipo_dependencia='Privada') THEN 1 ELSE 0 END) AS privado,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.tipo_dependencia <>'Privada' AND a.tipo_dependencia<>'Nacional' AND a.tipo_dependencia<>'Municipal' AND a.tipo_dependencia<>'Estadal' AND a.tipo_dependencia<>'Autonoma') THEN 1 ELSE 0 END) AS otros,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NOT NULL) THEN 1 ELSE 0 END) AS con_director_total,

  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NULL) THEN 1 ELSE 0 END) AS sin_director_total,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NULL AND (a.tipo_dependencia='Estadal' OR a.tipo_dependencia='Nacional' OR a.tipo_dependencia='Municipal' OR a.tipo_dependencia='Autonoma') ) THEN 1 ELSE 0 END) AS sin_director_oficial,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NULL AND a.tipo_dependencia='Privada') THEN 1 ELSE 0 END) AS sin_director_privado,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NULL AND a.tipo_dependencia <>'Privada' AND a.tipo_dependencia<>'Nacional' AND a.tipo_dependencia<>'Municipal' AND a.tipo_dependencia<>'Estadal' AND a.tipo_dependencia<>'Autonoma') THEN 1 ELSE 0 END) AS sin_director_otros,


  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_cedula::int IS NOT NULL AND (a.tipo_dependencia='Estadal' OR a.tipo_dependencia='Nacional' OR a.tipo_dependencia='Municipal' OR a.tipo_dependencia='Autonoma') ) THEN 1 ELSE 0 END) AS con_director_oficial,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NOT NULL AND a.tipo_dependencia='Privada') THEN 1 ELSE 0 END) AS con_director_privado,
  SUM(CASE WHEN (a.cod_plantel IS NOT NULL AND a.dir_nombre IS NOT NULL AND (a.tipo_dependencia <>'Privada' AND  a.tipo_dependencia<>'Nacional' AND a.tipo_dependencia<>'Municipal' AND a.tipo_dependencia<>'Estadal' AND a.tipo_dependencia<>'Autonoma')) THEN 1 ELSE 0 END) AS con_director_otros
  

FROM 
control.autoridad_plantel a 
LEFT JOIN public.estado es ON a.estado = es.nombre
             
GROUP BY a.estado, es.id


ORDER BY

a.estado ASC;

";
          
       }
       
        $connection = Yii::app()->dbEstadistica;
        $command = $connection->createCommand($sql);
        $resultado = $command->queryAll();

        return $resultado;

        
    }

    public function reporteEstadistico($level, $dependency_id=null){

        $resultado = array();

        if(in_array($level,array('region', 'estado', 'municipio')) && (is_null($dependency_id) || is_numeric($dependency_id))){

            if($level=='region'){
                $camposSeleccionados = "r.id, r.nombre, 'AAA'||r.nombre AS titulo ";
                $camposSeleccionadosTotales = "0 AS id, 'TOTAL' AS nombre , 'ZZZTOTAL' AS titulo";
                $camposAgrupados = "r.id, r.nombre, titulo ";
                $where = '1 = 1';
                $orderBy = 'titulo ASC, nombre ASC';
            }
            elseif($level=='estado'){
                $camposSeleccionados = "e.region_id AS region_id, r.nombre AS region, e.id AS id, e.nombre, 'AAA'||e.nombre AS titulo  ";
                $camposSeleccionadosTotales = "$dependency_id AS region_id, 'X' AS region, 0 AS id, 'TOTAL' AS nombre , 'ZZZTOTAL' AS titulo";
                $camposAgrupados = "e.region_id, r.nombre, e.id, e.nombre, titulo  ";
                $where = 'e.id != 45 AND e.region_id = '.$dependency_id; // Excluye Dependencias Federales (Id=45)
                $orderBy = 'titulo ASC, nombre ASC';
            }elseif($level=='municipio'){
                $camposSeleccionados = "e.region_id AS region_id, r.nombre AS region, e.id AS estado_id, e.nombre AS estado, m.id, m.nombre, 'AAA'||m.nombre AS titulo  ";
                $camposSeleccionadosTotales = "0 AS region_id, 'X' AS region, $dependency_id AS estado_id, 'X' AS estado, 0 AS id, 'TOTAL' AS nombre , 'ZZZTOTAL' AS titulo";
                $camposAgrupados = "e.region_id, r.nombre, e.id, e.nombre, m.id, m.nombre, titulo  ";
                $where = 'm.estado_id = '.$dependency_id;
                $orderBy = 'titulo ASC, nombre ASC';
            }

            $sql = "SELECT  DISTINCT $camposSeleccionados
                            , SUM(CASE WHEN (p.id IS NOT NULL) THEN 1 ELSE 1 END) AS planteles
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NOT NULL) THEN 1 ELSE 0 END) AS con_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NULL) THEN 1 ELSE 0 END) AS sin_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NOT NULL AND p.tipo_dependencia_id IN (1, 2, 3)) THEN 1 ELSE 0 END) AS publ_con_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NULL AND p.tipo_dependencia_id IN (1, 2, 3)) THEN 1 ELSE 0 END) AS publ_sin_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NOT NULL AND p.tipo_dependencia_id = 6) THEN 1 ELSE 0 END) AS priv_con_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NULL AND p.tipo_dependencia_id = 6) THEN 1 ELSE 0 END) AS priv_sin_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NOT NULL AND p.tipo_dependencia_id NOT IN (1,2,3,6)) THEN 1 ELSE 0 END) AS otros_con_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NULL AND p.tipo_dependencia_id NOT IN (1,2,3,6)) THEN 1 ELSE 0 END) AS otros_sin_director
                    FROM 
                    
                        public.region r
                        INNER JOIN public.estado e
                            ON r.id = e.region_id
                        LEFT JOIN gplantel.plantel p
                            ON e.id = p.estado_id
                        LEFT JOIN public.municipio m
                            ON p.municipio_id = m.id
                    WHERE
                        $where
                    GROUP BY
                        $camposAgrupados
                    UNION

                    SELECT  $camposSeleccionadosTotales
                            , SUM(CASE WHEN (p.id IS NOT NULL) THEN 1 ELSE 0 END) AS planteles
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NOT NULL) THEN 1 ELSE 0 END) AS con_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NULL) THEN 1 ELSE 0 END) AS sin_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NOT NULL AND p.tipo_dependencia_id IN (1, 2, 3)) THEN 1 ELSE 0 END) AS publ_con_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NULL AND p.tipo_dependencia_id IN (1, 2, 3)) THEN 1 ELSE 0 END) AS publ_sin_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NOT NULL AND p.tipo_dependencia_id = 6) THEN 1 ELSE 0 END) AS priv_con_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NULL AND p.tipo_dependencia_id = 6) THEN 1 ELSE 0 END) AS priv_sin_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NOT NULL AND p.tipo_dependencia_id NOT IN (1,2,3,6)) THEN 1 ELSE 0 END) AS otros_con_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NULL AND p.tipo_dependencia_id NOT IN (1,2,3,6)) THEN 1 ELSE 0 END) AS otros_sin_director
                    FROM public.region r
                        INNER JOIN public.estado e
                            ON r.id = e.region_id
                        LEFT JOIN gplantel.plantel p
                            ON e.id = p.estado_id
                        LEFT JOIN public.municipio m
                            ON p.municipio_id = m.id
                    WHERE
                        $where
                    ORDER BY
                        $orderBy";

            //echo "<pre><code>$sql</code></pre>";

            $connection = Yii::app()->dbEstadistica;
            $command = $connection->createCommand($sql);
            $resultado = $command->queryAll();

        }

        return $resultado;

    }


    public function reporteGrafico($estadoId=null){

        $resultado = array();
        $camposSeleccionados = "e.region_id AS id, r.nombre AS region, e.id AS estado_id, e.nombre, 'AAA'||e.nombre AS titulo  ";
        $camposAgrupados = "e.region_id, r.nombre, e.id, e.nombre, titulo  ";
        $where = 'e.id != 45';
        $orderBy = 'titulo ASC, e.nombre ASC';

        if(!is_null($estadoId) && is_numeric($estadoId)){
            $where .= " AND e.id = $estadoId ";
        }

        $sql = "SELECT  $camposSeleccionados
                        , SUM(CASE WHEN (p.id IS NOT NULL) THEN 1 ELSE 0 END) AS planteles
                        , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NOT NULL) THEN 1 ELSE 0 END) AS con_director
                        , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NULL) THEN 1 ELSE 0 END) AS sin_director
                FROM public.region r
                    INNER JOIN public.estado e
                        ON r.id = e.region_id
                    LEFT JOIN gplantel.plantel p
                        ON e.id = p.estado_id
                WHERE
                    $where
                GROUP BY
                    $camposAgrupados
                ORDER BY
                    $orderBy";

        //ld($sql);

        $connection = Yii::app()->dbEstadistica;
        $command = $connection->createCommand($sql);
        $resultado = $command->queryAll();

        return $resultado;

    }


}
