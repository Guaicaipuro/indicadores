    <?php

class TituloDigitalController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'verificar'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'verificar'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionIndex() {
        $model = new TituloDigital();
        if (Yii::app()->user->id) {
            if (isset($_REQUEST['verificacion'])) {
                $codigo_verificacion = $_REQUEST['verificacion'];
                if ($codigo_verificacion == "" || $codigo_verificacion == NULL) {
                    $respuesta['statusCode'] = 'Error-codigo-vacio';
                    $respuesta['mensaje'] = $this->renderPartial('//msgBox', array('class' => 'alertDialogBox', 'message' => 'Código no pude estar vacio'), TRUE);
                    echo json_encode($respuesta);
                    Yii::app()->end();
                }
                $verificarQr = TituloDigital::model()->verificarQr($codigo_verificacion);
                //var_dump($verificarQr); die();
                if ($verificarQr) {
                    $this->renderPartial('_verificacion', array('verificarQr' => $verificarQr));
                    Yii::app()->end();
                } else {
                    $respuesta['statusCode'] = 'Error';
                    $respuesta['mensaje'] = $this->renderPartial('//msgBox', array('class' => 'alertDialogBox', 'message' => 'Código Inexistente'), TRUE);
                    echo json_encode($respuesta);
                    Yii::app()->end();
                }
            }
            $this->render('_form', array('model' => $model));
        } else {
            throw new CHttpException(404, 'El Requerimiento no Existe');
        }
    }
    public function actionVerificar() {
        if (isset($_REQUEST['codigo'])) {
            $codigo_verificacion = $_REQUEST['codigo'];
            $verificarQr = TituloDigital::model()->verificarQr($codigo_verificacion);
            if ($verificarQr) {
                $this->render('_verificacion', array('verificarQr' => $verificarQr));
            } else {
                throw new CHttpException(404, 'El Requerimiento no Existe');
            }
        } else {
            throw new CHttpException(404, 'El Requerimiento no Existe');
        }
    }

    public function loadModel($id) {
        $model = TituloDigital::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    /**
     * Performs the AJAX validation.
     * @param TituloDigital $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'titulo-digital-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
