<?php

class BoletinDigitalController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='index';
    public  $layout = '//layouts/column2';
    CONST PERIODO_ESCOLAR = '2014-2015';
    CONST PERIODO_ESCOLAR_ID = 15;

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de BoletinDigitalController',
        'write' => 'Creación y Modificación de BoletinDigitalController',
        'admin' => 'Administración Completa  de BoletinDigitalController',
        'label' => 'Módulo de BoletinDigitalController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    /*public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('index', 'consulta', 'registro', 'edicion', 'eliminacion', 'activacion'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion',),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }*/


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model=new BoletinDigital('search');
        $model->unsetAttributes();  // clear any default values
        $this->render('admin',array(
                'model'=>$model,
            )
        );
    }

    public function actionVerificar()
    {
        if($this->hasPost('BoletinDigital')){
            $model = new BoletinDigital();
            $model->attributes=$form = $this->getPost('BoletinDigital');
            $boletinDigital = $model->findByAttributes(array('codigo_verificacion'=>$model->codigo_verificacion));
            $modulo = "Estudiante.Consultar.verBoletin";
            $ip = Yii::app()->request->userHostAddress;
            $ruta = yii::app()->basePath;
            $ruta = $ruta . '/yiic';
            $username = Yii::app()->user->name;
            $usuario_id = Yii::app()->user->id;
            if(isset($boletinDigital->inscripcion_id) AND !is_null($boletinDigital->inscripcion_id)){
                if($boletinDigital->inscripcion_id){
                    $datos_header = CalificacionAsignaturaEstudiante::cargarCorreosRepresentantes($boletinDigital->inscripcion_id);
                    $nivel =$datos_header[0]['nivel_id'] ;
                    $datosCalificaciones = AsignaturaEstudiante::model()->obtenerAsignaturasEstudianteBoletin($boletinDigital->inscripcion_id,base64_encode(1),$nivel);
                    //$datos_footer = array();
                    if ($datos_header != array()) {
                        Yii::import('ext.qrcode.QRCode');
                        $nombre_pdf = 'Boletin_' . $datos_header[0]['id'] . '_' . $datos_header[0]['periodo_id'];
                        $mpdf = new mpdf('', 'A4', 0, '', 15, 15, 75, 100);
                        /* encrypt md5 del ID de Inscripcion con el ID del estudiante*/
                        $url =Yii::app()->getBaseUrl(true).'/verificacion/boletinDigital/verificacion/id/'.$model->codigo_verificacion;
                        $urlVerificacion =Yii::app()->getBaseUrl(true).'/verificacion/boletinDigital/';

                        $code = new QRCode($url);
                        $direccion_qr = Yii::app()->basePath.'/../public/downloads/qr/boletines/'.'Boletin_' . $datos_header[0]['id'] . '_' . $datos_header[0]['periodo_id'].'.png';
                        $direccion_pdf = Yii::app()->basePath.'/../public/downloads/pdf/boletines/'.'Boletin_' . $datos_header[0]['id'] . '_' . $datos_header[0]['periodo_id'].'.pdf';
                        $code->create($direccion_qr);
                        $header = $this->renderPartial('_headerBoleta', array('datos_header' => $datos_header[0],'nombreQr'=>$nombre_pdf.'.png'), true);
                        $body = $this->renderPartial('_bodyBoleta', array('datosCalificaciones' => $datosCalificaciones,'nivel'=>$nivel), true);
                        $footer = $this->renderPartial('_footerBoleta', array('codigo_verificacion'=>$model->codigo_verificacion,'url'=>$urlVerificacion), true);

                        $mpdf->SetFont('sans-serif');
                        $mpdf->SetHTMLHeader($header);
                        $mpdf->WriteHTML($body);
                        $mpdf->SetHTMLFooter($footer);

                        $mpdf->Output($direccion_pdf, 'F');
                        $this->registerLog('REPORTES', $modulo . 'PDF', 'EXITOSO', 'Descarga el boletin del estudiante con el nombre: ' . $datos_header[0]['nombres'] . ' del plantel ' . $datos_header[0]['nombre_plantel'] . ' del periodo escolar ' . self::PERIODO_ESCOLAR_ID, $ip, $usuario_id, $username);

                        if(file_exists($direccion_pdf)){
                            $this->layout='//layouts/browser';
                            $this->setPageTitle('Verificación de Boletin Digital');
                            $this->render('view',array(
                                    'ruta_pdf'=>$nombre_pdf.'.pdf',
                                )
                            );
                            Yii::app()->end();
                        }
                        else {
                            throw new CHttpException(500, 'Estimado usuario, ocurrio un error durante el proceso. Recargue la página e intentelo de nuevo.');
                        }
                    }
                }else {
                    throw new CHttpException(404, 'No se ha encontrado la boleta del estudiante que ha solicitado. Recargue la página e intentelo de nuevo.');
                }


            }
            else {
                Yii::app()->user->setFlash('error', "Estimado usuario, el código de verificación suministrado es invalido.");
                $this->redirect('index');
                Yii::app()->end();
            }
        }
        else {
            throw new CHttpException(403,'The requested page does not exist.');
        }

    }
    public function actionVerificacion()
    {
        if($this->hasQuery('id')){
            $model = new BoletinDigital();
            $model->codigo_verificacion=$form = $this->getQuery('id');
            $boletinDigital = $model->findByAttributes(array('codigo_verificacion'=>$model->codigo_verificacion));
            $modulo = "Estudiante.Consultar.verBoletin";
            $ip = Yii::app()->request->userHostAddress;
            $ruta = yii::app()->basePath;
            $ruta = $ruta . '/yiic';
            $username = Yii::app()->user->name;
            $usuario_id = Yii::app()->user->id;
            if(isset($boletinDigital->inscripcion_id) AND !is_null($boletinDigital->inscripcion_id)){
                if($boletinDigital->inscripcion_id){
                    $datos_header = CalificacionAsignaturaEstudiante::cargarCorreosRepresentantes($boletinDigital->inscripcion_id);
                    $nivel =$datos_header[0]['nivel_id'] ;
                    $datosCalificaciones = AsignaturaEstudiante::model()->obtenerAsignaturasEstudianteBoletin($boletinDigital->inscripcion_id,base64_encode(1),$nivel);
                    //$datos_footer = array();
                    if ($datos_header != array()) {
                        Yii::import('ext.qrcode.QRCode');
                        $nombre_pdf = 'Boletin_' . $datos_header[0]['id'] . '_' . $datos_header[0]['periodo_id'];
                        $mpdf = new mpdf('', 'A4', 0, '', 15, 15, 75, 100);
                        /* encrypt md5 del ID de Inscripcion con el ID del estudiante*/
                        $url =Yii::app()->getBaseUrl(true).'/verificacion/boletinDigital/verificacion/id/'.$model->codigo_verificacion;
                        $urlVerificacion =Yii::app()->getBaseUrl(true).'/verificacion/boletinDigital/';

                        $code = new QRCode($url);
                        $direccion_qr = Yii::app()->basePath.'/../public/downloads/qr/boletines/'.'Boletin_' . $datos_header[0]['id'] . '_' . $datos_header[0]['periodo_id'].'.png';
                        $direccion_pdf = Yii::app()->basePath.'/../public/downloads/pdf/boletines/'.'Boletin_' . $datos_header[0]['id'] . '_' . $datos_header[0]['periodo_id'].'.pdf';
                        $code->create($direccion_qr);
                        $header = $this->renderPartial('_headerBoleta', array('datos_header' => $datos_header[0],'nombreQr'=>$nombre_pdf.'.png'), true);
                        $body = $this->renderPartial('_bodyBoleta', array('datosCalificaciones' => $datosCalificaciones,'nivel'=>$nivel), true);
                        $footer = $this->renderPartial('_footerBoleta', array('codigo_verificacion'=>$model->codigo_verificacion,'url'=>$urlVerificacion), true);

                        $mpdf->SetFont('sans-serif');
                        $mpdf->SetHTMLHeader($header);
                        $mpdf->WriteHTML($body);
                        $mpdf->SetHTMLFooter($footer);

                        $mpdf->Output($direccion_pdf, 'F');
                        $this->registerLog('REPORTES', $modulo . 'PDF', 'EXITOSO', 'Descarga el boletin del estudiante con el nombre: ' . $datos_header[0]['nombres'] . ' del plantel ' . $datos_header[0]['nombre_plantel'] . ' del periodo escolar ' . self::PERIODO_ESCOLAR_ID, $ip, $usuario_id, $username);

                        if(file_exists($direccion_pdf)){
                            $this->layout='//layouts/browser';
                            $this->setPageTitle('Verificación de Boletin Digital');
                            $this->render('view',array(
                                    'ruta_pdf'=>$nombre_pdf.'.pdf',
                                )
                            );
                            Yii::app()->end();
                        }
                        else {
                            throw new CHttpException(500, 'Estimado usuario, ocurrio un error durante el proceso. Recargue la página e intentelo de nuevo.');
                        }
                    }
                }else {
                    throw new CHttpException(404, 'No se ha encontrado la boleta del estudiante que ha solicitado. Recargue la página e intentelo de nuevo.');
                }


            }
            else {
                Yii::app()->user->setFlash('error', "Estimado usuario, el código de verificación suministrado es invalido.");
                $this->redirect('index');
                Yii::app()->end();
            }
        }
        else {
            throw new CHttpException(403,'The requested page does not exist.');
        }

    }



    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return BoletinDigital the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if(is_numeric($id)){
            $model=BoletinDigital::model()->findByPk($id);
            if($model===null){
                throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
        }
        else{
            return null;
        }
    }

    /**
     * Performs the AJAX validation.
     * @param BoletinDigital $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='boletin-digital-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $id = base64_encode($id_encoded);
        $columna = '<div class="action-buttons">';
        $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/verificacion/boletinDigital/consulta/id/'.$id)) . '&nbsp;&nbsp;';
        $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '/verificacion/boletinDigital/edicion/id/'.$id)) . '&nbsp;&nbsp;';
        if($data->estatus=='A'){
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Eliminar", 'href' => '/verificacion/boletinDigital/eliminacion/id/'.$id)) . '&nbsp;&nbsp;';
        }else{
            $columna .= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar", 'href' => '/verificacion/boletinDigital/activacion/id/'.$id)) . '&nbsp;&nbsp;';
        }
        $columna .= '</div>';
        return $columna;
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }
}
