<?php
$fecha = date('d-m-Y');
?>
<div class="infoDialogBox">
    <p>

        <b>Documento: </b><?php echo $verificarQr['documento']; ?><br>
        <b>Código de Verificación: </b><?php echo $verificarQr['codigo_qr']; ?><br>
        <b>Emitido a la fecha: </b> <?php echo $fecha;?> <br>
        <b>Nombre del Estudiante: </b> <?php echo $verificarQr['nombre_estudiante'];?> <br>
        <b>Apellido del Estudiante: </b> <?php echo $verificarQr['apellido_estudiante'];?> <br>
        <b>Fecha de Nacimiento: </b> <?php echo $verificarQr['fecha_nacimiento'];?> <br>
        <?php if(!is_null($verificarQr['documento_identidad'])):?>
            <b>Documento de Identidad:</b> <?php echo$verificarQr['documento_identidad']; ?> <br>
        <?php endif; ?>
        <?php if(!is_null($verificarQr['cedula_escolar'])):?>
            <b>Cédula Escolar: </b> <?php echo $verificarQr['cedula_escolar'];?> <br>
        <?php endif; ?>
        <?php if($verificarQr['tipo_constancia_id']==1):?>
            <b>Nombre del Plantel: </b> <?php echo $verificarQr['nombre_plantel'];?> <br>
            <b>Codigo Estadistico:</b> <?php echo $verificarQr['codigo_estadistico']; ?> <br>
            <b>Nombre de Sección:</b> <?php echo $verificarQr['nombre_seccion']; ?> <br>
            <b>Nombre de Grado:</b> <?php echo $verificarQr['nombre_grado']; ?> <br>
            <b>Nombre del Estado:</b> <?php echo $verificarQr['nombre_estado']; ?> <br>
            <b>Nombre de la Capital:</b> <?php echo $verificarQr['nombre_capital']; ?> <br>
            <b>Nombre del Nivel:</b> <?php echo $verificarQr['nombre_nivel']; ?> <br>
        <?php endif; ?>
        <?php if($verificarQr['tipo_constancia_id']==2 OR $verificarQr['tipo_constancia_id']==3):?>
            <b>Estado de Nacimiento: </b> <?php echo $verificarQr['estado_nac'];?> <br>
            <b>Nombre del Plantel: </b> <?php echo $verificarQr['nombre_plantel'];?> <br>
            <b>Dirección del Plantel: </b> <?php echo $verificarQr['direccion_plantel'];?> <br>
            <b>Estado del Plantel: </b> <?php echo $verificarQr['estado_plantel'];?> <br>
            <b>Municipio del Plantel: </b> <?php echo $verificarQr['municipio_plantel'];?> <br>
            <b>Parroquia del Plantel: </b> <?php echo $verificarQr['parroquia_plantel'];?> <br>
        <?php endif; ?>
        <b>Periodo Escolar: </b> <?php echo $verificarQr['periodo_escolar'];?> <br>
    </p>
</div>