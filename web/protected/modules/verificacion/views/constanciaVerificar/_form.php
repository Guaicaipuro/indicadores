<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'titulo-digital-form',
    'enableAjaxValidation' => false,
        ));
?>
<div id="error">
</div>
<div id="codigo-vacio">
</div>
<div class="widget-box">

    <div class="widget-header">
        <h5>B&uacute;squeda de Verificación</h5>
        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>


    <div class="widget-body">
        <div class="widget-body-inner" style="display: block;">
            <div class="widget-main">
                <a href="#" class="search-button"></a>
                <div style="display:block" class="search-form">
                    <div class="widget-main form">
                        <div class="col-md-5">
                            <div class="col-md-12"><label for="Plantel_cedula">Código QR<span class="required">*</span></label></div>
                            <?php echo '<input type="text" data-toggle="tooltip" data-placement="bottom" placeholder="Codigo Qr" title="Ej:99999999" id="verificar"  style="padding:3px 4px" maxlength="50" size="10" class="span-10" name="nombre_qr">'; ?>
                            <?php echo CHtml::button('Buscar', array('class' => 'btn btn-info btn-xs', 'id' => 'yt0')); ?>
                        </div>
                        <br>
                        <br>
                    </div>
                </div>
            </div><!-- search-form -->
        </div>
    </div>
</div>
<div id="exito">
</div>
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>





<?php $this->endWidget(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#verificar').bind('keyup blur', function() {
            keyAlphaNum(this, true);
            clearField(this);
        });
        $("#yt0").on('click', function() {
            var verificacion = $('#verificar').val();
            Loading.show();
            $.ajax({
                url: '/verificacion/constanciaVerificar',
                data: {verificacion: verificacion},
                dataType: 'html',
                type: 'GET',
                success: function(resp, resp1, resp3)
                {
                    try {
                        var json = jQuery.parseJSON(resp3.responseText);
                        if (json.statusCode === 'Error') {
                            $("#error").removeClass('hide');
                            $("#codigo-vacio").addClass('hide');
                            $("#exito").addClass('hide');
                            $("#error").html(json.mensaje);
                            Loading.hide();
                        }
                        if (json.statusCode === 'Error-codigo-vacio') {
                            $("#codigo-vacio").removeClass('hide');
                            $("#error").addClass('hide');
                            $("#exito").addClass('hide');
                            $("#codigo-vacio").html(json.mensaje);
                            Loading.hide();
                        }
                    } catch (e) {
                        $("#exito").removeClass('hide');
                        $("#codigo-vacio").addClass('hide');
                        $("#error").addClass('hide');
                        $("#exito").html(resp);
                        Loading.hide();
                    }
                }

            });
        });
    });


    function keyAlphaNum(element, with_space, with_spanhol) {
    if (with_space && with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\- ]/g)) {
            element.value = element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\- ]/g, '');
        }
//alert('1.- '+with_space+with_spanhol);
    } else if (with_spanhol) {
        if (element.value.match(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-]/g)) {
            element.value = element.value.replace(/[^0-9a-zA-ZáÁéÉíÍóÓúÚñÑäÄëËïÏöÖüÜ\-]/g, '');
        }
//alert('2.- '+with_space+with_spanhol);
    } else if (with_space) {
        if (element.value.match(/[^0-9a-zA-Z\- ]/g)) {
            element.value = element.value.replace(/[^0-9a-zA-Z\- ]/g, '');
        }
//alert('3.- '+with_space+with_spanhol);
    } else {
        if (element.value.match(/[^0-9a-zA-Z\-]/g)) {
            element.value = element.value.replace(/[^0-9a-zA-Z\-]/g, '');
        }
//alert('4.- '+with_space+with_spanhol);
    }

}

</script>

