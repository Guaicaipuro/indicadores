<?php
$fecha = date('d-m-Y');
?>
<div class="infoDialogBox">
    <p>
        <b> Verificación de documento: </b><?php echo $verificarQr['codigo_verificacion'] . '<br>'; ?>
        <b> Emitido a la fecha: </b> <?php echo $fecha;?>
        <?php
        echo '<b>'. '<br>'."Nombres y Apellidos:" .'</b>' . $verificarQr['nombre_estudiante'] . ' ' .'<br>'.
        '<b>'. 'Cedula de identidad: '.'</b>' . $verificarQr['cedula_estudiante'] . ' ' .'<br>'.
        '<b>'.'Zona Educativa:' .'</b>'. $verificarQr['zona_educativa'] . ' ' .'<br>'.
        '<b>'.'Nombre Plantel:'. '</b>' . $verificarQr['plantel'] . ' ' . '<br>'.
        '<b>'.'Codigo del Plantel:'.'</b>' . $verificarQr['codigo_plantel'] . ' ' .'<br>'.
       '<b>'. 'Mencion:' .'</b>'.$verificarQr['mencion']. ' '.'<br>'.
        '<b>'.'Nacido En:'.'</b>'.$verificarQr['nacido_en'] . ' ' .'<br>'.
        '<b>'.'Lugar de Expedicion:' .'</b>' . $verificarQr['lugar_expedicion'] . ' ' .'<br>'.
        '<b>'.'Fecha de Expedición:' .'</b>' . $verificarQr['fecha_expedicion'] . ' ' .'<br>'.
        '<b>'.'Firma Digital de Zona Educativa:' .'</b>' . $verificarQr['firma_digital_director_zona_educativa'] . ' ' .'<br>'.
        '<b>'.'Origen del Director del Plantel:'.'</b>' . $verificarQr['origen_director_plantel'] . ' ' .'<br>'.
        '<b>'.'Cédula del Director del Plantel:'.'</b>' . $verificarQr['cedula_director_plantel'] . ' ' .'<br>'.
        '<b>'.'Nombre del Direcor del Plantel:' .'</b>'. $verificarQr['nombre_director_plantel'] . ' ' .'<br>'.
        '<b>'.'Origen del Funcionario Designado:'.'</b>' . $verificarQr['origen_funcionario_designado'] . ' ' .'<br>'.
        '<b>'.'Cédula del funcionario designado:'.'</b>' . $verificarQr['cedula_funcionario_designado'] . ' ' .'<br>'.
        '<b>'.'Nombre del Funcionario Designado:'.'</b>' . $verificarQr['nombre_funcionario_designado'] . ' ' .'<br>'.
        '<b>'.'Firma Digital del Funcionario Designado:'.'</b>' . $verificarQr['firma_digital_funcionario_designado'] . ' ' .'<br>';
        ?>
    </p>
    <p><a href="/titulo/consultarTitulo/busqueda/id/<?php echo base64_encode($verificarQr['cedula_estudiante']).'/cod/'.base64_encode($verificarQr['origen_estudiante']); ?>">Ver más</a></p>
</div>