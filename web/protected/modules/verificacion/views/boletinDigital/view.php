<?php
/* @var $this BoletinDigitalController */
?>
<div class="col-xs-12">
    <div class="row-fluid">
        <div class="form">
            <?php
            if(isset($ruta_pdf) AND file_exists(realpath(Yii::app()->basePath.'/../public/downloads/pdf/boletines/'.$ruta_pdf))){ ?>
                <div class="successDialogBox"><p class="note">Hemos verificado exitosamente el boletin</p></div>
                <object class="col-md-12" style="height:600px;">
                    <embed class="col-md-12"  style="height:600px;" src="<?php echo Yii::app()->getBaseUrl(true).'/public/downloads/pdf/boletines/'.$ruta_pdf;?>" type="application/pdf"></embed>
                </object>
            <?php } else { ?>
                <div class="errorDialogBox"><p class="note">Estimado usuario, ha ocurrido un error durante el proceso.</p></div>
            <?php } ?>
        </div>
    </div>
</div>