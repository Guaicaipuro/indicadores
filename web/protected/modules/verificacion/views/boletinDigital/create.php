<?php
/* @var $this BoletinDigitalController */
/* @var $model BoletinDigital */

$this->pageTitle = 'Registro de Boletin Digitals';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Boletin Digitals'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>