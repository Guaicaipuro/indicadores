<?php

/* @var $this BoletinDigitalController */
/* @var $model BoletinDigital */

$this->breadcrumbs=array(
    'Verificación',
    'Boletin Digital'=>array('lista'),
    'Administración',
);
$this->pageTitle = 'Verificación de Boletin Digital';

?>
    <div class="widget-box">
        <div class="widget-header">
            <h5>Verificar de Boletin Digital</h5>

            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div style="display:block;" class="widget-body-inner">
                <div class="widget-main">

                    <div class="row space-6"></div>
                    <div>
                        <div id="resultadoOperacion">
                            <?php if(Yii::app()->user->hasFlash('error')){ ?>
                                <div class="errorDialogBox">
                                    <p>
                                        <?php echo Yii::app()->user->getFlash('error'); ?>
                                    </p>
                                </div>
                            <?php }
                            else  { ?>
                                <div class="infoDialogBox">
                                    <p>
                                        En este módulo podrá verificar los datos de los Boletin Digitales.
                                    </p>
                                </div>
                            <?php } ?>


                        </div>
                        <div class="row">
                            <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'boletin-digital-form',
                                'action'=>$this->createUrl("/verificacion/boletinDigital/verificar/"),
                                'htmlOptions' => array('data-form-type'=>'verificacion',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                            )); ?>
                            <div class="col-md-4">
                                <?php echo $form->labelEx($model,'codigo_verificacion'); ?>
                                <?php echo $form->textField($model,'codigo_verificacion',array('size'=>50, 'maxlength'=>50, 'class' => 'span-12', "required"=>"required",)); ?>
                            </div>
                            <div class="col-md-6 pull-left ">
                                <button class="btn btn-primary btn-next" style="margin-top: 15px;" data-last="Finish" type="submit">
                                    Verificar
                                    <i class="icon-save icon-on-right"></i>
                                </button>
                            </div>
                            <?php $this->endWidget(); ?>
                        </div>
                        <div class="row space-20"></div>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php
/**
 * Yii::app()->clientScript->registerScriptFile(
 *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/BoletinDigitalController/boletin-digital/admin.js', CClientScript::POS_END
 *);
 */
?>