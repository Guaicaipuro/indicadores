<?php
/* @var $this BoletinDigitalController */
/* @var $model BoletinDigital */

$this->pageTitle = 'Actualización de Datos de Boletin Digitals';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Boletin Digitals'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>