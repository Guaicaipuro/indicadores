<?php
/**
 * This is the model class for table "servicio.departamento".
 *
 * The followings are the available columns in table 'servicio.departamento':
 * @property integer $id
 * @property string $nombre
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $usuario_ini
 * @property string $usuario_act
 *
 * The followings are the available model relations:
 * @property Categoria[] $categorias
 * @property JefeDepartamento[] $jefeDepartamentos
 */
class DepartamentoIndic extends Departamento{
    
    
    
    
    
    
    
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Departamento the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
   
}