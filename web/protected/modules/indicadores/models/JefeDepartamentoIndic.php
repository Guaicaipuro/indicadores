<?php
/**
 * This is the model class for table "servicio.jefe_departamento".
 *
 * The followings are the available columns in table 'servicio.jefe_departamento':
 * @property integer $id
 * @property integer $departamento_id
 * @property integer $usuario_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $usuario_ini
 * @property string $usuario_act
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuario
 * @property Departamento $departamento
 */
class JefeDepartamentoIndic extends JefeDepartamento {

    public function getListaDepartamento($userId, $groupId) {
        $resultadoDepart = null;
        if (is_numeric($userId) and is_numeric($groupId)) {
            if (in_array($groupId, array(UserGroups::ADMIN_0, UserGroups::DESARROLLADOR))) {
                $resultadoDepart = Departamento::model()->findAll("estatus = 'A'");
            }
            else {
                $departamentos = array();
                $resultadoDepart = $this->with('departamento')->findAll("t.estatus = 'A' AND t.usuario_id = :jefe" , array(':jefe' => $userId));
                foreach ($resultadoDepart as $jefeDepartamento){
                    $departamentos[]= $jefeDepartamento->departamento;
                    
                }
                $resultadoDepart = $departamentos;  
            }
        }
        return $resultadoDepart;
    }
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Departamento the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
