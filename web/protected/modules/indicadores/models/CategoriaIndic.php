<?php
/**
 * This is the model class for table "servicio.categoria".
 *
 * The followings are the available columns in table 'servicio.categoria':
 * @property integer $id
 * @property string $nombre
 * @property integer $departamento_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $usuario_ini
 * @property string $usuario_act
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property Indicadores[] $indicadores
 * @property Departamento $departamento
 */
class CategoriaIndic extends CategoriaDepartamento {
 
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Departamento the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
}
