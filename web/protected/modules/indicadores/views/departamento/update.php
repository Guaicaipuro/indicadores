<?php
/* @var $this DepartamentoController */
/* @var $model Departamento */

$this->pageTitle = 'Actualización de Datos de Departamentos';
      $this->breadcrumbs=array(
	'Departamentos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>