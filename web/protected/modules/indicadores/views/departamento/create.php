<?php
/* @var $this DepartamentoController */
/* @var $model Departamento */

$this->pageTitle = 'Registro de Departamentos';
      $this->breadcrumbs=array(
	'Departamentos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>