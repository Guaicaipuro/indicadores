<?php
/* @var $this CategoriaDepartamentoController */
/* @var $model CategoriaDepartamento */

$this->pageTitle = 'Actualización de Datos de Categoria Departamentos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Categoria Departamentos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>