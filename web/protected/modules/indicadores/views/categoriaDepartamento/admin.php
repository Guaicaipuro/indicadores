<?php

/* @var $this CategoriaDepartamentoController */
/* @var $model CategoriaDepartamento */

?>
<div>

    <div class="pull-right" style="padding-left:10px;">
        <a href="<?php echo $this->createUrl("/indicadores/categoriaDepartamento/registro/dep/".$dep); ?>" type="submit" id='newRegisterCategoria' data-last="Finish" class="btn btn-success btn-next btn-sm">
            <i class="fa fa-plus icon-on-right"></i>
            Registrar Nueva Categoría de Departamento
        </a>
    </div>

    <div class="row space-20"></div>

</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'categoria-departamento-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
                    categoriaDepartamento.manejarEventosLista();
                }",
	'columns'=>array(
        array(
            'header' => '<center>Nombre de Categoría</center>',
            'name' => 'nombre',
            'htmlOptions' => array('class'=>'span-12'),
            'filter' => CHtml::textField('CategoriaDepartamento[nombre]', $model->nombre, array('title' => '',)),
        ),
        array(
            'header' => '<center>Departamento</center>',
            'value' => '(is_object($data->departamento))?$data->departamento->nombre:""'
        ),
        
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            'filter' => array('A'=>'Activo', 'I'=>'Inactivo'),
        ),
	
        array(
            'type' => 'raw',
            'header' => '<center>Acción</center>',
            'value' => array($this, 'getActionButtons'),
            'htmlOptions' => array('nowrap'=>'nowrap'),
        ),
    ),
));
?>