<?php
/* @var $this CategoriaDepartamentoController */
/* @var $model CategoriaDepartamento */

$this->pageTitle = 'Registro de Categoria Departamentos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Categoria Departamentos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>