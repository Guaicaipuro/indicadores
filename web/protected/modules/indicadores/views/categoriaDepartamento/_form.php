<?php
/* @var $this CategoriaDepartamentoController */
/* @var $model CategoriaDepartamento */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">
        <div class="form">
            <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'categoria-departamento-form',
                    'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
            )); ?>

            <div id="div-result">
            <?php
            if($model->hasErrors()):
                $this->renderPartial('//errorSumMsg', array('model' => $model));
            elseif(Yii::app()->user->hasFlash('success')): $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => Yii::app()->user->getFlash('success')));
            else:
            ?>
                        <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
            <?php
                   endif;
            ?>
            </div>

            <div id="div-datos-generales">

                <div class="widget-box">

                    <div class="widget-header">
                        <h5>Datos Generales</h5>

                        <div class="widget-toolbar">
                            <a data-action="collapse" href="#">
                                <i class="icon-chevron-up"></i>
                            </a>
                        </div>
                    </div>

                    <div class="widget-body">
                        <div class="widget-body-inner">
                            <div class="widget-main">
                                <div class="widget-main form">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="col-md-6">
                                                <?php echo $form->labelEx($model,'nombre'); ?>
                                                <?php echo $form->textField($model,'nombre',array('id'=>'CategoriaDepartamento_nombre_registro', 'size'=>60, 'maxlength'=>180, 'class' => 'span-12', "required"=>"required",)); ?>
                                            </div>


                                            <div class="col-md-6">
                                                <?php echo $form->labelEx($model,'departamento_id'); ?>
                                                <?php echo $form->dropDownList($model, 'departamento_id', CHtml::listData(Departamento::model()->findAll(array('limit'=>50)), 'id', 'nombre'), array('id'=>'CategoriaDepartamento_departamento_id_registro', 'prompt'=>'- - -', 'class' => 'span-12', "required"=>"required", "disabled"=>"disabled" )); ?>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row hide">

                        <div class="col-md-6 wizard-actions">
                            <button id="submitGuardarCategoriaDepartamento" class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                Guardar
                                <i class="icon-save icon-on-right"></i>
                            </button>
                        </div>

                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div><!-- form -->
    </div>
</div>
