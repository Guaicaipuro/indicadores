<?php

/* @var $this AdministracionController */
/* @var $model jefeDepartamentoIndic */
/* @var $form CActiveForm */

$this->breadcrumbs=array(
	'Indicadores'=>"#",
	'Administración',
);
$this->pageTitle = 'Administración de Indicadores';

?>
<div class="widget-box">
    <div class="widget-header">
        <h4>Categorías</h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-body-inner" style="display:block;">
            <div class="widget-main">
                <div id="div-departamentos">
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los Datos de Categorías.
                            </p>
                        </div>
                        <select name="departamento_id" id='departamento_id'>
                            <?php foreach ($departamentos as $departamento): ?>
                                <option value="<?php echo $departamento->id; ?>"><?php echo strtoupper($departamento->nombre); ?></option>
                            <?php endforeach; ?>
                        </select>                        
                    </div> 
                </div>
                
            </div>
        </div>
    </div>
</div>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/indicadores/dapartamentos.js', CClientScript::POS_END);?>