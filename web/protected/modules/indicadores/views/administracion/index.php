<?php

/* @var $this AdministracionController */
/* @var $model jefeDepartamentoIndic */
/* @var $form CActiveForm */

$this->breadcrumbs=array(
	'Indicadores'=>"#",
	'Administración',
);
$this->pageTitle = 'Administración de Indicadores';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Categorías</h5>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-body-inner" style="display:block;">
            <div class="widget-main">
                <div id="div-departamentos">
                    <label class="col-md-12" for="departamento_id">Seleccione el Departamento <span class="required">*</span></label>
                    <select name="departamento_id" id='departamento_id' title="Seleccione un departamento para listar sus categorías">
                        <option value=""> - - - </option>
                        <?php foreach ($departamentos as $departamento): ?>
                        <option value="<?php echo $departamento->id; ?>"><?php echo strtoupper($departamento->nombre); ?></option>
                        <?php endforeach; ?>
                    </select>  
                </div>
                <div class="space-6"></div>
                <div id="div-resultado-categoria-departamento"></div>
                <div class="space-6"></div>
                <div id="div-categorias">
                    <div class="infoDialogBox">
                        <p>
                            En este módulo podrá registrar y/o actualizar los Datos de Categorías. Para esto debe primero seleccionar un departamento.
                        </p>
                    </div>
                </div>
                <div id="div-indicadores"></div>
                
            </div>
        </div>
    </div>
</div>
<div id="div-modal-categoria-departamento" class="hide"></div>
<div id="div-modal-indicadores" class="hide"></div>
<div id="div-modal-valor-indicadores" class="hide"></div>
<?php $this->widget('ext.loading.LoadingWidget');?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.scroll-to.js', CClientScript::POS_END);?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/indicadores/departamento.js', CClientScript::POS_END);?>