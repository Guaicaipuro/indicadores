<?php
/* @var $this AdministrcionController */

$this->breadcrumbs=array(
    'administracion'=>array('/consulta'),
    'Administración'
);
$this->pageTitle = 'Administración de Indicadores';
?>
<!--<h1><?php echo $this->uniqueId . '/' . $this->action->id; ?></h1>

<p>
This is the view content for action "<?php echo $this->action->id; ?>".
The action belongs to the controller "<?php echo get_class($this); ?>"
in the "<?php echo $this->module->id; ?>" module.
</p>
<p>
You may customize this page by editing <tt><?php echo __FILE__; ?></tt>
</p>-->
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Indicadores</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="widget-body">
        <div style="display:block" class="widget-body-inner">
            <div class="widget-main">
                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Indicadores.
                            </p>
                        </div>
                    </div>
                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/indicadores/Administracion/consulta"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Indicadores
                        </a>
                    </div>
                    <div class="row space-20">
                        <?php echo $departamento;?>
                    </div>
                    <script type="text/javascript">
                        alert("Hola Mundo!");
                    </script> 
                </div>
               
            </div>
        </div>
    </div>
</div>

    <div class="indicadores" onclick="">
       <a id="btnRegresarInicio" class="btn btn-primary" href="/">Regresar a la página Inicio</a>
       
    </div>
     
</div>