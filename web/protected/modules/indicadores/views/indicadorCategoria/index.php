<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Indicadores de la Categoría <b><?php echo $categoria; ?></b></h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Indicadores de la Categoría <b><?php echo $categoria; ?></b>
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/indicadores/indicadorCategoria/registro/cat/".  base64_encode($model->categoria_id)); ?>" type="submit" id='newRegisterIndicador' data-last="Finish" class="btn btn-success btn-next btn-sm" data-categoria-id="<?php echo $model->categoria_id; ?>" data-categoria="<?php echo $categoria; ?>">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Indicador de la Categoría <?php echo $categoria; ?>
                        </a>
                    </div>

                    <div class="row space-20"></div>

                </div>

                <div class="grid-view" id="categoria-departamento-grid">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>
                                    <center>Nombre de la Variable</center>
                                </th>
                                <th>
                                    <center>Categoría</center>
                                </th>
                                <th>
                                    <center>Estatus</center>
                                </th>
                                <th>
                                    <center>Acciones</center>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($listaIndicadores)>0): ?>
                            <?php   foreach ($listaIndicadores as $indicador): ?>
                            <tr>
                                <td><?php echo Utiles::onlyAlphaNumericWithSpace($indicador->variable); ?></td>
                                <td><?php echo Utiles::onlyAlphaNumericWithSpace((is_object($indicador->categoria))?$indicador->categoria->nombre:""); ?></td>
                                <td><?php echo strtr($indicador->estatus, array('A'=>'Activo', 'I'=>'Inactivo')); ?></td>
                                <td><?php echo $this->getActionButtons($indicador); ?></td>
                            </tr>
                            <?php   endforeach; ?>
                            <?php else: ?>
                            <tr>
                                <td colspan="4">
                                    <div class="alertDialogBox">
                                        <p>
                                            No se han registrado indicadores para la categoría <?php echo $categoria; ?>
                                        </p>
                                    </div>
                                </td>
                            </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                
                </div>
                
            </div>
        </div>
    </div>
</div>
