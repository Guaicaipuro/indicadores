<?php
/* @var $this IndicadorCategoriaController */
/* @var $model Indicador */

$this->pageTitle = 'Actualización de Datos de Indicadors';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Indicadors'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>