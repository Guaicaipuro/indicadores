<?php
/* @var $this IndicadorCategoriaController */
/* @var $modelValorIndicador ValorIndicador */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">
         <div id="datosGenerales Indicadores">
            <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'indicador-form',
                        'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                )); ?>

                <div id="div-result">
        <?php
           if($modelValorIndicador->hasErrors()):
               $this->renderPartial('//errorSumMsg', array('model' => $modelValorIndicador));
           elseif(Yii::app()->user->hasFlash('success')):               $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => Yii::app()->user->getFlash('success')));
           else:
        ?>
                        <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                <?php
                   endif;
               ?>
                        </div>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model,'indicador_id'); ?>
                                                            <input class="span-12 form-control" value="<?php echo $model->variable; ?>" disabled="disabled"/>
                                                            <?php echo $form->hiddenField($modelValorIndicador, 'indicador_id', array('class' => 'hide')); ?>
                                                        </div>
                                                        
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($modelValorIndicador,'valor'); ?>
                                                            <?php echo $form->textField($modelValorIndicador,'valor',array('size'=>11, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row hide">

                                    <div class="col-md-6 wizard-actions">
                                        <button id="submitGuardarindicadores" class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                            Guardar
                                            <i class="icon-save icon-on-right"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

            </div>
        </div>

        <?php
            /**
             * Yii::app()->clientScript->registerScriptFile(
             *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/IndicadorCategoriaController/indicador/form.js',CClientScript::POS_END
             *);
             */
        ?>
    </div>
</div>
