<?php
/* @var $this IndicadorCategoriaController */
/* @var $model Indicador */

$this->pageTitle = 'Registro de Indicadors';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Indicadors'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>