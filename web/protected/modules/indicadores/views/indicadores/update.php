<?php
/* @var $this IndicadoresController */
/* @var $model Indicadores */

$this->pageTitle = 'Actualización de Datos de Indicadores';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Indicadores'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>