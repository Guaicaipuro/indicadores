<?php

/* @var $this IndicadoresController */
/* @var $model Indicadores */

$this->breadcrumbs=array(
        'Catálogo' => array('/catalogo'),
	'Indicadores'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Indicadores';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Indicadores</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Indicadores.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/indicadores/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Indicadores                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'indicadores-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
                    
                }",
	'columns'=>array(
        array(
            'header' => '<center>Id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Indicadores[id]', $model->id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Variable</center>',
            'name' => 'variable',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Indicadores[variable]', $model->variable, array('title' => '',)),
        ),
        array(
            'header' => '<center>Valor</center>',
            'name' => 'valor',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Indicadores[valor]', $model->valor, array('title' => '',)),
        ),
        array(
            'header' => '<center>Categoria_id</center>',
            'name' => 'categoria_id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Indicadores[categoria_id]', $model->categoria_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Fecha_ini</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Indicadores[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),
        array(
            'header' => '<center>Fecha_act</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Indicadores[fecha_act]', $model->fecha_act, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>usuario_ini</center>',
            'name' => 'usuario_ini',
            'htmlOptions' => array(),
                        'filter' => CHtml::textField('Indicadores[usuario_ini]', $model->usuario_ini, array('title' => '',)),
                    ),
        array(
            'header' => '<center>Usuario_act</center>',
            'name' => 'usuario_act',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Indicadores[usuario_act]', $model->usuario_act, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>

<?php
    /**
     * Yii::app()->clientScript->registerScriptFile(
     *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/IndicadoresController/indicadores/admin.js', CClientScript::POS_END
     *);
     */
?>