<?php
/* @var $this IndicadoresController */
/* @var $model Indicadores */

$this->pageTitle = 'Registro de Indicadores';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Indicadores'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>