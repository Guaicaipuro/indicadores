<?php

class CategoriaDepartamentoController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de CategoriaDepartamentoController',
        'write' => 'Creación y Modificación de CategoriaDepartamentoController',
        'admin' => 'Administración Completa  de CategoriaDepartamentoController',
        'label' => 'Módulo de CategoriaDepartamentoController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'activacion'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion',),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Lists all models.
     */
    public function actionLista($dep)
    {
        
        $departamentoId = $this->getIdDecoded($dep);
        if(is_numeric($departamentoId)){
            $model=new CategoriaDepartamento('search');
            $model->unsetAttributes();  // clear any default values
            $model->estatus = 'A';
            if($this->hasQuery('CategoriaDepartamento')){
                $model->attributes=$this->getQuery('CategoriaDepartamento');
            }
            $model->departamento_id = $departamentoId;
            $dataProvider = $model->search();
            if(Yii::app()->request->isAjaxRequest){
                Yii::app()->clientScript->scriptMap = array('jquery.min.js'=>false,);
                $this->renderPartial('admin',array(
                    'model'=>$model,
                    'dep' => $dep,
                    'dataProvider'=>$dataProvider,
                ), false, true);
            }
            else{
                $this->render('admin',array(
                    'model'=>$model,
                    'dep' => $dep,
                    'dataProvider'=>$dataProvider,
                ));
            }
        }
        else{
            throw new CHttpException(400, "No se han proporcionado los datos necesarios para efectuar esta acción.");
        }
        
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro($dep)
    {
        if(Yii::app()->request->isAjaxRequest){
            $departamentoId = $this->getIdDecoded($dep);
            if(is_numeric($departamentoId)){
                $model=new CategoriaDepartamento;
                $model->departamento_id = $departamentoId;
                if($this->hasPost('CategoriaDepartamento')){
                    $model->attributes= $this->getPost('CategoriaDepartamento');
                    $model->beforeInsert();
                    if($model->save()){
                        $this->registerLog('ESCRITURA', 'modulo.CategoriaDepartamento.registro', 'EXITOSO', 'El Registro de los datos de CategoriaDepartamento se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                        $this->renderPartial('//msgBox',array('classname' => 'successDialogBox', 'message' => 'El proceso de registro de los datos se ha efectuado exitosamente'));
                        Yii::app()->end();
                    }
                    else{
                        $this->renderPartial('//errorSumMsg',array($model));
                        Yii::app()->end();
                    }
                }
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('create', array(
                        'model'=>$model,
                        'dep'=>$dep,
                    ), false, true);
                }
                else{
                    $this->render('create', array(
                        'model'=>$model,
                        'dep'=>$dep,
                    ));
                }

            }
            else {
                throw new CHttpException(400, "E0011C: No se han proporcionado los datos necesarios para efectuar esta acción.");
            }
        }
        else{
            throw new CHttpException(403, "E0012C: No está permitido efectuar esta acción mediante esta vía.");
        }

    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id, $dep) {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);

        if (Yii::app()->request->isAjaxRequest) {
            $departamentoId = $this->getIdDecoded($dep);
            if (is_numeric($departamentoId)) {
                
                $model->departamento_id = $departamentoId;

                if ($this->hasPost('CategoriaDepartamento')) {
                    $model->attributes = $this->getPost('CategoriaDepartamento');
                    $model->beforeUpdate();
                    if ($model->save()) {
                        $this->registerLog('ACTUALIZACION', 'modulo.CategoriaDepartamento.edicion', 'EXITOSO', 'La Actualización de los datos de CategoriaDepartamento se ha efectuado exitósamente. Data-> ' . json_encode($model->attributes));
                        if (Yii::app()->request->isAjaxRequest) {
                            $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => 'La actualización de los Datos se ha efectuado de forma exitosa.'));
                            Yii::app()->end();
                        }
                    }
                }

                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('create', array(
                        'model'=>$model,
                        'dep'=>$dep,
                    ), false, true);
                }
                else{
                    $this->render('create', array(
                        'model'=>$model,
                        'dep'=>$dep,
                    ));
                }

            } else {
                throw new CHttpException(400, "E0011C: No se han proporcionado los datos necesarios para efectuar esta acción.");
            }
        } else {
            throw new CHttpException(403, "E0012C: No está permitido efectuar esta acción mediante esta vía.");
        }
    }

    /**
     * Logical Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        
        if($model){
            $model->beforeDelete();
            if($model->save()){
                $this->registerLog('ELIMINACION', 'modulo.CategoriaDepartamento.eliminacion', 'EXITOSO', 'La Eliminación de los datos de CategoriaDepartamento se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La eliminación del registro se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }

        $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
    }
    
    /**
     * Activation of a particular model Logicaly Deleted.
     * If activation is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be activated
     */
    public function actionActivacion($id){
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        if($model){
            $model->beforeActivate();
            if($model->save()){
                $this->registerLog('ACTIVACION', 'modulo.CategoriaDepartamento.activacion', 'EXITOSO', 'La Activación de los datos de CategoriaDepartamento se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La activación de este registro se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }
        $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return CategoriaDepartamento the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if(is_numeric($id)){
            $model=CategoriaDepartamento::model()->findByPk($id);
            if($model===null){
                throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
        }
        else{
            return null;
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CategoriaDepartamento $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='categoria-departamento-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $departamentoId = $data["departamento_id"];
        $estatus = $data["estatus"];
        $id = base64_encode($id_encoded);
        $columna = '<div class="action-buttons">';
        // $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in ver-categoria", "title" => "Ver datos", 'href' => '/indicadores/categoriaDepartamento/consulta/id/'.$id, 'data-id'=>$id, 'data-departamento'=>$departamentoId,)) . '&nbsp;&nbsp;';
        $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green  editar-categoria", "title" => "Editar datos", 'href' => '/indicadores/categoriaDepartamento/edicion/id/'.$id.'/dep/'.$departamentoId, 'data-id'=>$id, 'data-departamento'=>$departamentoId,)) . '&nbsp;&nbsp;';
        $columna .= CHtml::link("", "", array("class" => "fa fa-bar-chart-o orange ver-indicadores-categoria", "title" => "Ver Indicadores", 'href' => '/indicadores/indicadorCategoria/lista/cat/'.$id.'/dep/'.$departamentoId, 'data-id'=>$id, 'data-departamento'=>$departamentoId, 'data-categoria'=>$data->nombre,)) . '&nbsp;&nbsp;';
        if($data->estatus=='A'){
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red estatus-categoria", "title" => "Eliminar", 'href' => '/indicadores/categoriaDepartamento/eliminacion/id/'.$id, 'data-id'=>$id, 'data-departamento'=>$departamentoId, 'data-estatus'=>$estatus)) . '&nbsp;&nbsp;';
        }else{
            $columna .= CHtml::link("", "", array("class" => "fa fa-check green estatus-categoria", "title" => "Activar", 'href' => '/indicadores/categoriaDepartamento/activacion/id/'.$id, 'data-id'=>$id, 'data-departamento'=>$departamentoId, 'data-estatus'=>$estatus,)) . '&nbsp;&nbsp;';
        }
        
        $columna .= '</div>';
        return $columna;
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }
}
