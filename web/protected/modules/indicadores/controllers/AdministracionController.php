<?php

class AdministracionController extends Controller
{   
    public $layout='//layouts/column1';
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='consulta';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de AdministracionController',
        'write' => 'Creación y Modificación de AdministracionController',
        'admin' => 'Administración Completa  de AdministracionController',
        'label' => 'Módulo de AdministracionController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'activacion'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion',),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    public function actionAdmin(){
        $model = new $departamentos;
        $departamentos = JefeDepartamentoIndic::model()->getListaDepartamento($userId, $groupId);
        $this->render('admin', array(
            'model' => $model,
            'departamentos' => $departamentos,
        ));
    }

    public function actionIndex(){
            $model= CActiveRecord::model('JefeDepartamentoIndic')->findAll();
            $this->render('index', array(
                'model' => $model,
            ));
	}
        
        public function actionView() {
            $this->render('view');
               
        }
        public function actionConsulta(){
            $model= CActiveRecord::model('JefeDepartamentoIndic')->findAll();
            $userId = yii::app()->user->id;
            $groupId = yii::app()->user->group;
            $departamentos = JefeDepartamentoIndic::model()->getListaDepartamento($userId, $groupId);
            //var_dump($departamentos);
            $departamentosDataProvider= new CArrayDataProvider($departamentos, array(
                'pagination' => false,
                )
            );
            $this->render('index', array(
                'model' => $model,
                'departamentos' => $departamentos,
                'departamentosDataProvider' => $departamentosDataProvider,
            ));
           
        }
        public function actionConsultaCategoria(){
            $model = CActiveRecord::model('CategoriaIndic')->findAll();
            $userId = yii::app()->user->id;
            $groupId = yii::app()->user->group;
            $categorias = CategoriaIndic::model()->getListaCategoriaDepartamento($userId, $groupId, $indicadores_id);
            $categoriasDataProvider = new CActiveDataProvider($categorias, array(
                'pagination' => false,
                )
            );
            $this->render('index', array(
                'model' => $model,
                'categorias'=> $categorias,
                'categoriasDataProvider'=> $categoriasDataProvider,
            ));
            
        }

}
