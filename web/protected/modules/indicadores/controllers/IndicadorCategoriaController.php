<?php

class IndicadorCategoriaController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de IndicadorCategoriaController',
        'write' => 'Creación y Modificación de IndicadorCategoriaController',
        'admin' => 'Administración Completa  de IndicadorCategoriaController',
        'label' => 'Módulo de IndicadorCategoriaController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'activacion', 'registrarValor'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'registrarValor'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Lists all models.
     * @var int $cat Id de la Categoría de Departamento
     */
    public function actionLista($cat, $dep)
    {
        $departamentoId = $this->getIdDecoded($dep);
        $categoriaId = $this->getIdDecoded($cat);
        $categoria = $this->getQuery('categoria');
        if(is_numeric($categoriaId)){
            $model=new Indicador('search');
            $model->unsetAttributes();  // clear any default values
            $model->estatus = 'A';
            if($this->hasQuery('Indicador')){
                $model->attributes=$this->getQuery('Indicador');
            }
            $model->categoria_id = $categoriaId;
            $listaIndicadores = $model->findIndicadoresByCategoria();
            if(Yii::app()->request->isAjaxRequest){
                Yii::app()->clientScript->scriptMap = array('jquery.min.js'=>false,);
                $this->renderPartial('index',array(
                    'model'=>$model,
                    'categoria'=>$categoria,
                    'dep' => $dep,
                    'listaIndicadores'=>$listaIndicadores,
                ), false, true);
            }
            else{
                $this->render('admin',array(
                    'model'=>$model,
                    'categoria'=>$categoria,
                    'dep' => $dep,
                    'dataProvider'=>$dataProvider,
                ));
            }
        }
        else{
            throw new CHttpException(400, "No se han proporcionado los datos necesarios para efectuar esta acción.");
        }

    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @var int $cat Id de Categoría
     */
    public function actionRegistro($cat)
    {
        if(Yii::app()->request->isAjaxRequest){
            $categoriaId = $this->getIdDecoded($cat);
            if(is_numeric($categoriaId)){
                $model=new Indicador;
                $model->categoria_id = $categoriaId;
                if($this->hasPost('Indicador')){
                    $model->attributes= $this->getPost('Indicador');
                    $model->beforeInsert();
                    if($model->save()){
                        $this->registerLog('ESCRITURA', 'modulo.Indicador.registro', 'EXITOSO', 'El Registro de los datos de Indicador se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                        
                        if(!Yii::app()->request->isAjaxRequest){
                            Yii::app()->user->setFlash('success', 'El proceso de registro de los datos se ha efectuado exitosamente');
                            $this->redirect(array('edicion','id'=>base64_encode($model->id),));
                        }
                        else{
                            $this->renderPartial('//msgBox',array('message' => 'El proceso de registro de los datos se ha efectuado exitosamente', 'classname' => 'successDialogBox'));
                            Yii::app()->end();
                        }
                        
                    }
                    else{
                        $this->renderPartial('//errorSumMsg',array($model));
                        Yii::app()->end();
                    }
                }
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('create', array(
                        'model'=>$model,
                        'cat'=>$cat,
                    ), false, true);
                }
                else{
                    $this->render('create', array(
                        'model'=>$model,
                        'cat'=>$cat,
                    ));
                }

            }
            else {
                throw new CHttpException(400, "E0011C: No se han proporcionado los datos necesarios para efectuar esta acción.");
            }
        }
        else{
            throw new CHttpException(403, "E0012C: No está permitido efectuar esta acción mediante esta vía.");
        }

    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     * @param integer $cat the ID of the categoria del departamento
     */
    public function actionEdicion($id, $cat)
    {
            $idDecoded = $this->getIdDecoded($id);
            $catDecoded = $this->getIdDecoded($cat);
            $model = $this->loadModel($idDecoded);

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if($this->hasPost('Indicador'))
            {
                $model->attributes=$this->getPost('Indicador');
                $model->beforeUpdate();
                if($model->save()){
                    $this->registerLog('ACTUALIZACION', 'modulo.Indicador.edicion', 'EXITOSO', 'La Actualización de los datos de Indicador se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                    if(Yii::app()->request->isAjaxRequest){
                        $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La actualización de los Datos se ha efectuado de forma exitosa.'));
                        Yii::app()->end();
                    }
                }
            }

            $this->renderPartial('update',array(
                    'model'=>$model,
            ));
    }
    
    
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     * @param integer $cat the ID of the categoria del departamento
     */
    public function actionRegistrarValor($id, $cat)
    {
            $idDecoded = $this->getIdDecoded($id);
            $catDecoded = $this->getIdDecoded($cat);
            $model = $this->loadModel($idDecoded);

            $modelValorIndicador = new ValorIndicador();
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if($this->hasPost('ValorIndicador'))
            {
                $modelValorIndicador->attributes=$this->getPost('ValorIndicador');
                $modelValorIndicador->indicador_id = $model->id;
                $modelValorIndicador->beforeInsert();
                if($modelValorIndicador->save()){
                    $this->registerLog('REGISTRO', 'modulo.Indicador.registroValor', 'EXITOSO', 'El Registro de los datos del Valor del Indicador se ha efectuado exitósamente. Data-> '.json_encode($modelValorIndicador->attributes));
                    if(Yii::app()->request->isAjaxRequest){
                        $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'El registro de los datos se ha efectuado de forma exitosa.'));
                        Yii::app()->end();
                    }
                }
            }

            $this->renderPartial('_formValorIndicador',array(
                'model'=>$model,
                'modelValorIndicador'=>$modelValorIndicador,
                'formType'=>'registro',
            ));
    }
    
    

    /**
     * Logical Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        
        if($model){
            $model->beforeDelete();
            if($model->save()){
                $this->registerLog('ELIMINACION', 'modulo.Indicador.eliminacion', 'EXITOSO', 'La Eliminación de los datos de Indicador se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La eliminación del registro se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }

        $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
    }
    
    /**
     * Activation of a particular model Logicaly Deleted.
     * If activation is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be activated
     */
    public function actionActivacion($id){
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        if($model){
            $model->beforeActivate();
            if($model->save()){
                $this->registerLog('ACTIVACION', 'modulo.Indicador.activacion', 'EXITOSO', 'La Activación de los datos de Indicador se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                if(Yii::app()->request->isAjaxRequest){
                    $this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La activación de este registro se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }
        $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Indicador the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if(is_numeric($id)){
            $model=Indicador::model()->findByPk($id);
            if($model===null){
                throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
        }
        else{
            return null;
        }
    }

    /**
     * Performs the AJAX validation.
     * @param Indicador $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='indicador-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $id = base64_encode($id_encoded);
        $categoeriaId = $data["categoria_id"];
        $categoeria = (is_object($data->categoria))?$data->categoria->nombre:"";
        $columna = '<div class="action-buttons">';
        $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green  editar-indicador", "title" => "Editar datos", 'href' => '/indicadores/indicadorCategoria/edicion/cat/'.base64_encode($categoeriaId).'/id/'.$id, 'data-categoria-id'=>$categoeriaId, 'data-categoria'=>$categoeria)) . '&nbsp;&nbsp;';
        $columna .= CHtml::link("", "", array("class" => "fa fa-bar-chart-o orange registrar-valor", "title" => "Cargar Valor de Indicador", 'href' => '/indicadores/indicadorCategoria/registrarValor/id/'.$id.'/cat/'.  base64_encode($categoeriaId), 'data-categoria-id'=>$categoeriaId, 'data-categoria'=>$categoeria)) . '&nbsp;&nbsp;';
        if($data->estatus=='A'){
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red  estatus-indicador", "title" => "Eliminar", 'href' => '/indicadores/indicadorCategoria/eliminacion/id/'.$id, 'data-categoria-id'=>$categoeriaId, 'data-categoria'=>$categoeria)) . '&nbsp;&nbsp;';
        }else{
            $columna .= CHtml::link("", "", array("class" => "fa fa-check green estatus-indicador", "title" => "Activar", 'href' => '/indicadores/indicadorCategoria/activacion/id/'.$id, 'data-categoria-id'=>$categoeriaId, 'data-categoria'=>$categoeria)) . '&nbsp;&nbsp;';
        }
        $columna .= '</div>';
        return $columna;
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }
}
