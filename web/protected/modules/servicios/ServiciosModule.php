<?php

class ServiciosModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'servicios.models.*',
			'control.models.*',
			'servicios.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			if(!Yii::app()->params['testing']){
				$auth = new Autenticar();
				$auth->checkAuth();
				if(!$auth->autorizado AND !$auth->parametros){
					throw new CHttpException(403, 'Estimado usuario, no esta permitido realizar esta acción mediante esta via.');
				}
				return $auth->autorizado;
			}
			else {
				return true;
			}
		}
		else
			return false;
	}
}
