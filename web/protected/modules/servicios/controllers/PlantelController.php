<?php
/**
 * Created by PhpStorm.
 * User: isalaz01
 * Date: 21/07/15
 * Time: 10:07 AM
 */

class PlantelController extends Controller
{
    public function actionReporteGeneral()
    {
        $response = new stdClass();
        $response->error=true;
        if(Yii::app()->request->requestType=='GET'){
            $matricula_periodo_anterior = Matriculacion::reporteEstadisticoMatriculado('estado',null);
            $matricula_periodo_actual = Matriculacion15::reporteEstadisticoMatriculado('estado',null);
            $total_inicial_2014=0;
            $total_primaria_2014=0;
            $total_otros_2014=0;
            $total_inicial_2015=0;
            $total_primaria_2015=0;
            $total_otros_2015=0;
            $total_adulto_2015=0;
            $total_especial_2015=0;

            foreach($matricula_periodo_anterior as $data => $item){
                $total_inicial_2014+=$item['cant_estudiante_inicial'];
                $total_primaria_2014+=$item['cant_estudiante_primaria'];
                $total_otros_2014+=$item['cant_estudiante_otros'];
            }
            foreach($matricula_periodo_actual as $data => $item){
                $total_inicial_2015+=$item['cant_estudiante_inicial'];
                $total_primaria_2015+=$item['cant_estudiante_primaria'];
                $total_otros_2015+=$item['cant_estudiante_otros'];
                $total_adulto_2015+=$item['cant_estudiante_adulto'];
                $total_especial_2015+=$item['cant_estudiante_especial'];
            }
            $response->periodo['2015']['estados']=$matricula_periodo_actual;
            $response->periodo['2015']['totales']['inicial']=$total_inicial_2015;
            $response->periodo['2015']['totales']['primaria']=$total_primaria_2015;
            $response->periodo['2015']['totales']['media']=$total_otros_2015;
            $response->periodo['2015']['totales']['adulto']=$total_adulto_2015;
            $response->periodo['2015']['totales']['especial']=$total_especial_2015;

            $response->periodo['2014']['estados']=$matricula_periodo_anterior;
            $response->periodo['2014']['totales']['inicial']=$total_inicial_2014;
            $response->periodo['2014']['totales']['primaria']=$total_primaria_2014;
            $response->periodo['2014']['totales']['media']=$total_otros_2014;
            $response->error=false;
        }
        else {
            throw new CHttpException(403, 'Estimado usuario, no esta permitido realizar esta acción mediante esta via.');
            //$response->mensaje='Estimado usuario, no esta permitido realizar esta acción mediante esta via';
        }
        echo json_encode($response);
        Yii::app()->end();
    }

    public function actionDepartamentosDisponibles()
    {
        $response = new stdClass();
        $response->error=true;
        if(Yii::app()->request->requestType=='GET'){
            $data = Departamento::getDepartamentosDisponibles();

            $response->error=false;
            $response->data=$data;
        }
        else {
            throw new CHttpException(403, 'Estimado usuario, no esta permitido realizar esta acción mediante esta via.');
            //$response->mensaje='Estimado usuario, no esta permitido realizar esta acción mediante esta via';
        }
        echo json_encode($response);
        Yii::app()->end();
    }

    public function actionReportesDisponibles()
    {
        $response = new stdClass();
        $response->error=true;
        if(Yii::app()->request->requestType=='GET'){
            $data = CategoriaApp::getCategoriasDisponibles();
            $response->error=false;
            $response->data=$data;
        }
        else {
            throw new CHttpException(403, 'Estimado usuario, no esta permitido realizar esta acción mediante esta via.');
            //$response->mensaje='Estimado usuario, no esta permitido realizar esta acción mediante esta via';
        }
        echo json_encode($response);
        Yii::app()->end();
    }

    public function actionPlantelesCercanos($long,$lat,$rto)
    {
        $response = new stdClass();
        $response->error=true;
        if(Yii::app()->request->requestType=='GET'){
            if($this->hasQuery('long') and $this->hasQuery('lat') and $this->hasQuery('rto')){
                $long=$this->getIdDecoded($this->getQuery('long'));
                $lat=$this->getIdDecoded($this->getQuery('lat'));
                $rto=$this->getIdDecoded($this->getQuery('rto'));

                if($long and $lat and $rto){
                    $response->error=false;
                    $data = Plantel::getPlantelesCercanos($long,$lat,$rto);
                    //$data = array($long, $lat, $rto);
                    $response->data=$data;
                }
                else {
                    $response->mensaje='Estimado usuario, no hemos encontrado la información solicitada.';
                }

            }
            else {
                throw new CHttpException(403, 'Estimado usuario, no esta permitido realizar esta acción mediante esta via.');
            }

        }
        else {
            throw new CHttpException(403, 'Estimado usuario, no esta permitido realizar esta acción mediante esta via.');
            //$response->mensaje='Estimado usuario, no esta permitido realizar esta acción mediante esta via';
        }
        echo json_encode($response);
        Yii::app()->end();
    }

    public function actionReporteEspecifico()
    {
        $response = new stdClass();
        $response->error=true;
        if(Yii::app()->request->requestType=='GET'){
            if($this->hasQuery('id')){
                $id=$this->getIdDecoded($this->getQuery('id'));
                if($id){
                    $response->error=false;
                    $data = ReporteTotalApp::getDataCategoria($id);
                    $response->data=$data;
                }
                else {
                    $response->mensaje='Estimado usuario, no hemos encontrado la información solicitada.';
                }

            }
            else {
                throw new CHttpException(403, 'Estimado usuario, no esta permitido realizar esta acción mediante esta via.');
            }

        }
        else {
            throw new CHttpException(403, 'Estimado usuario, no esta permitido realizar esta acción mediante esta via.');
            //$response->mensaje='Estimado usuario, no esta permitido realizar esta acción mediante esta via';
        }
        echo json_encode($response);
        Yii::app()->end();
    }

    public function actionNoticias()
    {
        $response = new stdClass();
        $response->error=true;
        if(Yii::app()->request->requestType=='GET'){
            $data = Noticia::getNoticias();
            $response->data=$data;

            if($data){
                $response->error=false;
            }

        }
        else {
            throw new CHttpException(403, 'Estimado usuario, no esta permitido realizar esta acción mediante esta via.');
            //$response->mensaje='Estimado usuario, no esta permitido realizar esta acción mediante esta via';
        }

        echo json_encode($response);
        Yii::app()->end();
    }
    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }
}