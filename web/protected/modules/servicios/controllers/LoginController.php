<?php

class LoginController extends Controller
{
    public function actionVerificar()
    {
        $response = new stdClass();
        $response->error=true;
        if(Yii::app()->request->requestType=='GET'){
            if($this->hasQuery('username') AND $this->hasQuery('password')){
                $username = $this->getQuery('username');
                $password = $this->getQuery('password');
                $model = UserGroupsUser::model()->findByAttributes(array('username' => $username));
                if($model AND $model->password==md5($password . $model->getSalt())){
                    $response->error=false;
                    $response->nombre=$model->nombre;
                    $response->apellido=$model->apellido;
                    $response->username=$model->username;
                    $response->rol=null;
                    if(is_object($model->relUserGroupsGroup) AND isset($model->relUserGroupsGroup->groupname)){
                        $response->rol=$model->relUserGroupsGroup->groupname;
                    }
                }
                else {
                    $response->mensaje='Error en Usuario/Contraseña';
                }
            }
            else {
                $response->mensaje='Estimado usuario, no se recibieron los datos necesarios.';
            }
        }
        else {
            throw new CHttpException(403, 'Estimado usuario, no esta permitido realizar esta acción mediante esta via.');
            //$response->mensaje='Estimado usuario, no esta permitido realizar esta acción mediante esta via';
        }

        echo json_encode($response);
        Yii::app()->end();
    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}