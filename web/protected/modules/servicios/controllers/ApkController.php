<?php

/**
 * Created by PhpStorm.
 * User: aljomori
 * Date: 05/08/15
 * Time: 1:27 PM
 */
class ApkController extends Controller {

    const APK_VERSION = 0.2;


    public function actionUpdate()
    {
        $response = new stdClass();
        $response->error=true;
        if(Yii::app()->request->requestType=='GET'){
            if($this->hasQuery('version')){
                $version=$this->getIdDecoded($this->getQuery('version'));
                $svn_apk_version = SvnApk::getUltimaVersion();
//var_dump($svn_apk_version);
                if($version AND $version < (int)$svn_apk_version){
                    $response->error=false;
                    $response->need_update=true;
                }
                else {
                    $response->need_update=false;
                    $response->mensaje='Aplicaci&oacute;n actualizada.';
                }

            }
            else {

                    $response->mensaje = 'Estimado usuario, no hemos encontrado la información solicitada.';
                }
            } else {

                throw new CHttpException(403, 'Estimado usuario, no esta permitido realizar esta acción mediante esta via.');
            }

        echo json_encode($response);
        Yii::app()->end();
    }

}
