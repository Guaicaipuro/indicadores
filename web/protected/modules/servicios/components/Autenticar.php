<?php
/**
 * Created by PhpStorm.
 * User: isalaz01
 * Date: 11/06/15
 * Time: 03:11 PM
 */

class Autenticar {

    public $username;
    public $password;
    public $token;
    public $ip;
    public $app_id;
    public $autorizado = false;
    public $parametros = false;

    public function __construct(){
        $headers = getallheaders();
        if((isset($headers['gmt_ve']) AND isset($headers['site_sig']) AND isset($headers['exo_pi']))) {

            $this->parametros = true;
            $this->username = base64_decode($headers['gmt_ve']);
            $this->password = base64_decode($headers['site_sig']);
            $this->token = base64_decode($headers['exo_pi']);
            if((isset($headers['alt_gr_ap']))){
                $this->app_id = base64_decode($headers['alt_gr_ap']);
            }
            /*$this->username = ($headers['HTTP_X_USERNAME']);
            $this->password = ($headers['HTTP_X_PASSWORD']);
            $this->token = ($headers['HTTP_X_TOKEN']);*/
        }
    }
    public function checkAuth()
    {
        if($this->parametros){
            $ipsValidas = array();
            $this->password = $this->_crypData($this->token,$this->password);
            $cliente = Cliente::model()->findByAttributes(array('usuario' => $this->username,'clave'=>md5($this->password),'token'=>$this->token));

            if(is_object($cliente) AND (is_object($cliente->clienteConfianzas) OR isset($cliente->app_id))){
                if(is_object($cliente->clienteConfianzas)){
                    foreach($cliente->clienteConfianzas as $clientes){
                        $ipsValidas[]=$clientes->ip;
                    }
                    if(in_array($this->ip,$ipsValidas)){
                        $this->autorizado = true;
                    }
                }
                else {
                    if($cliente->app_id==$this->app_id){
                        $this->autorizado = true;
                    }
                }
            }
        }
    }

    private function _crypData($key,$encoded_data){
        $key_length = strlen($key);
        $length = strlen($encoded_data);
        $result = '';
        $tmp = '';

        for ($i = 0; $i < $length; $i++) {
            $tmp = $encoded_data[$i];

            for ($j = 0; $j < $key_length; $j++) {
                $tmp = chr(ord($tmp) ^ ord($key[$j]));
            }

            $result .= $tmp;
        }
        return $result;
    }
}