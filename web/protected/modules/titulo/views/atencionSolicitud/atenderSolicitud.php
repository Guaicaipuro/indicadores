<input type="hidden" id="codigo_estadistico" value="<?php echo $codigoe ?>" name="codigo_estadistico">
<input type="hidden" id="codigo_plantel" value="<?php echo $codigop ?>" name="codigo_plantel">
<input type="hidden" id="tipoBusqueda" value="<?php echo $tipoBusqueda ?>" name="tipoBusqueda">
<input type="hidden" id="primerSerial" value="<?php echo $primer_serial ?>" name="primerSerial">
<input type="hidden" id="ultimoSerial" value="<?php echo $ultimo_serial ?>" name="ultimoSerial">
<input type="hidden" id="cantidadSerial" value="<?php echo $sumaTotal ?>" name="cantidadSerial">
<input type="hidden" id="plantel_id" value="<?php echo $plantel_id ?>" name="plantel_id">

<div id="respuestBuscar" class="hide errorDialogBox" ><p></p> </div>
<div id="busqued" class="hide alertDialogBox" ><p></p> </div>
<div id="campo_vacio" class="hide alertDialogBox" ><p></p> </div>

<div id="exitoPorPlantel" class="hide successDialogBox" ><p></p> </div>
<div id="errorPorPlantel" class="hide errorDialogBox" ><p></p> </div>

<?php if ($mensaje != '') { ?>
    <div id="datosAlterados" class="errorDialogBox" >
        <p>
            <?php echo $mensaje ?>
        </p>
    </div>
<?php } ?>
<?php if ($mensaje_cantidadSeriales != '') { ?>
    <div id="mensaje_cantidadSeriales" class="errorDialogBox" >
        <p>
            <?php echo $mensaje_cantidadSeriales ?>
        </p>
    </div>
<?php } ?>
<div id="informacion">
    <div class="infoDialogBox">
        <p>
            Por favor debe Ingresar los campos marcados con <span class="required">*</span> porque son requeridos,
            recuerde que para guardar el lote de los seriales de titulos debe agregar el lote primero.
        </p>
    </div>
</div>





<div class="tabbable">
    <ul class="nav nav-tabs">
        <?php if ($tienenEstudiantes == true) { ?>
            <li><a href="#atenderSolicitud" data-toggle="tab"><i class="fa fa-plus"></i> Asignar Seriales Disponibles</a></li>
        <?php } ?>
        <li class="active" ><a href="#atenderSolicitudPorLotes" data-toggle="tab"><i class="fa fa-bars"></i> Lista de Solicitudes</a></li>
    </ul>

    <div class="tab-content">
        <?php
        if ($tienenEstudiantes == true) {
            ?>
            <div class="tab-pane" id="atenderSolicitud">


                <div class = "widget-box collapsed">

                    <div class = "widget-header">
                        <h5>Identificación del Plantel <?php echo '"' . $resultadoDatosPlantel[0]['nombre'] . '"'; ?></h5>

                        <div class = "widget-toolbar">
                            <a href = "#" data-action = "collapse">
                                <i class = "icon-chevron-down"></i>
                            </a>
                        </div>

                    </div>

                    <div class = "widget-body">
                        <div style = "display: none;" class = "widget-body-inner">
                            <div class = "widget-main">

                                <div class="row row-fluid center">
                                    <div id="1eraFila" class="col-md-12">
                                        <div class="col-md-4" >

                                            <?php echo CHtml::label('<b>Código del Plantel</b>', '', array("class" => "col-md-12")); ?>
                                            <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['cod_plantel'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                                        </div>

                                        <div class="col-md-4" >
                                            <?php echo CHtml::label('<b>Código Estadistico</b>', '', array("class" => "col-md-12")); ?>
                                            <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['cod_estadistico'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                                        </div>

                                        <div class="col-md-4" >
                                            <?php echo CHtml::label('<b>Denominación</b>', '', array("class" => "col-md-12")); ?>
                                            <?php
                                            echo CHtml::textField('', $resultadoDatosPlantel[0]['denominacion'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                            ?>
                                        </div>

                                    </div>

                                    <div class = "col-md-12"><div class = "space-6"></div></div>

                                    <div id="2daFila" class="col-md-12">
                                        <div class="col-md-4" >
                                            <?php echo CHtml::label('<b>Nombre del Plantel</b>', '', array("class" => "col-md-12")); ?>
                                            <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['nombre'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                                        </div>

                                        <div class="col-md-4" >
                                            <?php echo CHtml::label('<b>Zona Educativa</b>', '', array("class" => "col-md-12")); ?>
                                            <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['zona_educativa'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                                        </div>

                                        <div class="col-md-4" >
                                            <?php echo CHtml::label('<b>Estado</b>', '', array("class" => "col-md-12")); ?>
                                            <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['estado'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                                        </div>

                                    </div>

                                    <div class = "col-md-12"><div class = "space-6"></div></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>




                <div class = "widget-box">

                    <div class = "widget-header">
                        <h5>Planes</h5>

                        <div class = "widget-toolbar">
                            <a href = "#" data-action = "collapse">
                                <i class = "icon-chevron-down"></i>
                            </a>
                        </div>

                    </div>

                    <div class = "widget-body">
                        <div style = "display: block;" class = "widget-body-inner">
                            <div class = "widget-main">

                                <div class="row align-center">
                                    <div class="col-md-12" id ="plan">
                                        <?php if ($tienePlanes == true) { ?>
                                            <?php
                                            if (isset($planesObtenidos) && $planesObtenidos !== array()) {
                                                $this->widget(
                                                        'zii.widgets.grid.CGridView', array(
                                                    'id' => 'planes',
                                                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                                    'dataProvider' => $planesObtenidos,
                                                    'summaryText' => false,
                                                    'columns' => array(
                                                        array(
                                                            'name' => 'plan',
                                                            'type' => 'raw',
                                                            'header' => '<center><b>Plan</b></center>'
                                                        ),
                                                        array(
                                                            'name' => 'mencion',
                                                            'type' => 'raw',
                                                            'header' => '<center><b>Mención</b></center>'
                                                        ),
                                                    ),
                                                    'pager' => array(
                                                        'header' => '',
                                                        'htmlOptions' => array('class' => 'pagination'),
                                                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                                    ),
                                                        )
                                                );
                                            }
                                            ?>
                                        <?php } elseif ($tienePlanes == false) { ?>

                                            <table style="width:650%;" class="table table-striped table-bordered table-hover" class="align-center">
                                                <thead>

                                                    <tr>
                                                        <th>
                                                            <b>Plan</b>
                                                        </th>
                                                        <th>
                                                            <b>Mención</b>
                                                        </th>
                                                        <th>
                                                            <b>Cantidad de Estudiantes</b>
                                                        </th>
                                                    </tr>

                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td colspan="3">
                                                            Este plantel no tiene planes asignados.
                                                        </td>
                                                    </tr>
                                                </tbody>

                                            </table>

                                        <?php } ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>



                <!-- Muestro la parte de agregar seriales por lotes -->

                <div class = "widget-box">

                    <div class = "widget-header">
                        <h5>Seriales de Facsímiles</h5>

                        <div class = "widget-toolbar">
                            <a href = "#" data-action = "collapse">
                                <i class = "icon-chevron-down"></i>
                            </a>
                        </div>

                    </div>

                    <div class = "widget-body">
                        <div style = "display: block;" class = "widget-body-inner">
                            <div class = "widget-main">

                                <div class="row align-center">
                                    <div class="col-md-12" id ="serial">
                                        <div class="col-md-12">

                                            <div class = "col-md-12"><div class = "space-6"></div></div>


                                            <div class="col-md-12">

                                                <?php if ($dataProviderSerialesAsignad != array()) { ?>
                                                    <div class = "widget-box">

                                                        <div class = "widget-header">
                                                            <h5>Seriales Asignados</h5>

                                                            <div class = "widget-toolbar">
                                                                <a href = "#" data-action = "collapse">
                                                                    <i class = "icon-chevron-down"></i>
                                                                </a>
                                                            </div>

                                                        </div>

                                                        <div class = "widget-body">
                                                            <div style = "display: block;" class = "widget-body-inner">
                                                                <div class = "widget-main">

                                                                    <div class="row align-center">
                                                                        <div class="col-md-12">

                                                                            <div class="col-md-12" id="1eraFila">

                                                                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                                                                <div id="1eraFila" class="col-md-12">

                                                                                    <?php
                                                                                    if (isset($dataProviderSerialesAsignad) && $dataProviderSerialesAsignad !== array()) {
                                                                                        $this->widget(
                                                                                                'zii.widgets.grid.CGridView', array(
                                                                                            'id' => 'planes',
                                                                                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                                                                            'dataProvider' => $dataProviderSerialesAsignad,
                                                                                            'summaryText' => false,
                                                                                            'columns' => array(
                                                                                                array(
                                                                                                    'name' => 'primer_serial',
                                                                                                    'type' => 'raw',
                                                                                                    'header' => '<center><b>Primer Serial</b></center>'
                                                                                                ),
                                                                                                array(
                                                                                                    'name' => 'ultimo_serial',
                                                                                                    'type' => 'raw',
                                                                                                    'header' => '<center><b>Ultimo Serial</b></center>'
                                                                                                ),
                                                                                                array(
                                                                                                    'name' => 'cantidad_serial',
                                                                                                    'type' => 'raw',
                                                                                                    'header' => '<center><b>Cantidad de Estudiantes</b></center>'
                                                                                                ),
                                                                                            ),
                                                                                            'pager' => array(
                                                                                                'header' => '',
                                                                                                'htmlOptions' => array('class' => 'pagination'),
                                                                                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                                                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                                                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                                                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                                                                            ),
                                                                                                )
                                                                                        );
                                                                                    }
                                                                                    ?>

                                                                                </div>

                                                                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                <?php } ?>

                                            </div>
                                            <!--  <div class="col-md-6">
                                            <?php // echo CHtml::label('Cantidad Títulos Solicitados:', '', array("class" => "col-md-6")); ?>
                                            <?php // echo CHtml::textField('suma', "$sumaTotal", array('class' => 'span-6'));    ?>


                                              </div>

                                              <div class="col-md-6">
                                            <?php // echo CHtml::label('¿Registró las Calificaciones? <span class="required">*</span>', '', array("class" => "col-md-4"));    ?>

                                            <?php //echo CHTML::dropDownList('cargo_calificacion', 'cargo_calificacion', array('1' => 'Si', '0' => 'No'), array('empty' => '-SELECCIONE-', 'class' => 'span-7'));    ?>
                                              </div>
                                          </div>

                                          <div class = "col-md-12"><div class = "space-6"></div></div>
                                            -->
                                            <!-- Boton de agregar lote -->
                                            <div  class="pull-right" style="padding-left: 20px; padding: 20px" >
                                                <?php
                                                // if ($sumaPermitida < $sumaTotal) {
                                                ?>
                                                <button  id = "btnAgregarLote"  class="btn btn-primary btn-sm" type="submit" data-last="Finish">
                                                    <i class="fa fa-plus icon-on-right"></i>
                                                    Agregar Lote
                                                </button>
                                                <?php //}    ?>
                                            </div>

                                            <div id='mostarSeriales' class="col-md-12">

                                                <div class = "col-md-12"><div class = "space-6"></div></div>
                                                <div class="col-md-12">
                                                    <?php
                                                    if (isset($dataProviderMostrarSeriales) && $dataProviderMostrarSeriales !== array()) {
                                                        $this->widget(
                                                                'zii.widgets.grid.CGridView', array(
                                                            'id' => 'planes',
                                                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                                            'dataProvider' => $dataProviderMostrarSeriales,
                                                            'summaryText' => false,
                                                            'columns' => array(
                                                                array(
                                                                    'name' => 'primer_serial',
                                                                    'type' => 'raw',
                                                                    'header' => '<center><b>Primer Serial</b></center>'
                                                                ),
                                                                array(
                                                                    'name' => 'ultimo_serial',
                                                                    'type' => 'raw',
                                                                    'header' => '<center><b>Ultimo Serial</b></center>'
                                                                ),
                                                                array(
                                                                    'name' => 'cantidad_serial',
                                                                    'type' => 'raw',
                                                                    'header' => '<center><b>Cantidad de Estudiantes</b></center>'
                                                                ),
                                                                array(
                                                                    'name' => 'fecha_entrega',
                                                                    'type' => 'raw',
                                                                    'header' => '<center><b>Fecha de Entrega</b></center>'
                                                                ),
                                                                array(
                                                                    'name' => 'asignado',
                                                                    'type' => 'raw',
                                                                    'header' => '<center><b>Asignado Por</b></center>'
                                                                ),
                                                            ),
                                                            'pager' => array(
                                                                'header' => '',
                                                                'htmlOptions' => array('class' => 'pagination'),
                                                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                                            ),
                                                                )
                                                        );
                                                    }
                                                    ?>




                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'datosFuncionario_form'
                ));
                ?>
                <div class = "widget-box">

                    <div class = "widget-header">
                        <h5>Datos del Funcionario Responsable de la Recepción de Títulos</h5>

                        <div class = "widget-toolbar">
                            <a href = "#" data-action = "collapse">
                                <i class = "icon-chevron-down"></i>
                            </a>
                        </div>

                    </div>

                    <div class = "widget-body">
                        <div style = "display: block;" class = "widget-body-inner">
                            <div class = "widget-main">

                                <div class="row align-center">
                                    <div class="col-md-12">

                                        <div class="col-md-12" id="1eraFila">
                                            <div id="1eraFila" class="col-md-12">
                                                <div class="col-md-4" >
                                                    <?php echo $form->labelEx($model, 'cedula_responsable', array("class" => "col-md-12")); ?>
                                                    <?php echo $form->textField($model, 'cedula_responsable', array('class' => 'span-7', 'style' => 'width: 90%', 'ata-toggle' => 'tooltip', 'data-placement' => 'bottom', 'placeholder' => 'V-0000000', 'title' => 'Ej: V-99999999 ó E-99999999', 'maxlength' => '10', 'size' => '10', 'onkeypress' => 'return CedulaFormat(this, event)')); ?>
                                                </div>

                                                <div class="col-md-4" >
                                                    <?php echo $form->labelEx($model, 'nombre_responsable', array("class" => "col-md-12")); ?>
                                                    <?php echo $form->textField($model, 'nombre_responsable', array('class' => 'span-7', 'style' => 'width: 90%')); ?>
                                                </div>

                                                <div class="col-md-4" >
                                                    <?php echo $form->labelEx($model, 'apellido_responsable', array("class" => "col-md-12")); ?>
                                                    <?php echo $form->textField($model, 'apellido_responsable', array('class' => 'span-7', 'style' => 'width: 90%')); ?>
                                                </div>

                                            </div>

                                            <div class = "col-md-12"><div class = "space-6"></div></div>

                                            <div id="2eraFila" class="col-md-12">
                                                <div class="col-md-4" >
                                                    <?php echo $form->labelEx($model, 'origen_responsable', array("class" => "col-md-12")); ?>
                                                    <?php echo $form->dropDownList($model, 'origen_responsable', array('V' => 'Venezolano(a)', 'E' => 'Extranjero(a)'), array('empty' => '-SELECCIONE-', 'class' => 'span-7', 'style' => 'width: 90%')); ?>
                                                </div>

                                                <div class="col-md-4" >
                                                    <?php echo $form->labelEx($model, 'cargo_responsable_id', array("class" => "col-md-12")); ?>
                                                    <?php echo $form->dropDownList($model, 'cargo_responsable_id', CHtml::listData($cargo, 'id', 'nombre'), array('empty' => '- SELECCIONE -', 'class' => 'span-7', 'style' => 'width: 90%'));
                                                    ?>
                                                </div>

                                                <div class="col-md-4" >
                                                    <?php echo $form->labelEx($model, 'observacion', array("class" => "col-md-12")); ?>
                                                    <?php echo $form->textArea($model, 'observacion', array('class' => 'span-7', 'style' => 'width: 90%')); ?>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="padding-top: 1%">
                        <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("titulo/atencionSolicitud"); ?>"class="btn btn-danger">
                            <i class="icon-arrow-left"></i>
                            Volver
                        </a>
                    </div>
                    <?php if ($dataProviderMostrarSeriales != array()) { ?>
                        <?php
                        //  if ($sumaPermitida == $sumaTotal) {
                        ?>
                        <div class="col-md-6 wizard-actions" style="padding-top: 1%">
                            <button id="btnGuardarDistribucion" type="submit" data-last="Finish" class="btn btn-primary btn-next">
                                Guardar
                                <i class="icon-save icon-on-right"></i>
                            </button>
                        </div>
                        <?php
                        //    }
                    }
                    ?>
                    <?php $this->endWidget(); ?>
                </div>
            </div>

        <?php } ?>












        <div class="tab-pane active" id="atenderSolicitudPorLotes">

            <?php
            $this->renderPartial('listaSolicitudPorPlantel', array(
                'resultadoDatosPlantel' => $resultadoDatosPlantel,
                'planesObtenidos' => $planesObtenidos,
                'dataProviderAsignacionSerial' => $dataProviderAsignacionSerial,
                'serialAsignados' => $serialAsignados,
                'dataProviderAsignacionSerialListo' => $dataProviderAsignacionSerialListo
            ));
            ?>

        </div>


    </div>
</div>



<div id = "mensaje_agregar_lote" class="hide">
    <div class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">

        </p>
    </div>
</div>

<div id = "dialog_error" class="hide">
    <div class="errorDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">

        </p>
    </div>
</div>


<script>
    $(document).ready(function() {
        $("#suma").attr('disabled', true);

        //        $('#SeguimientoPapelMoneda_cedula_responsable').bind('keyup blur', function() {
        //            keyNum(this, false);
        //        });

        $('#SeguimientoPapelMoneda_nombre_responsable').bind('keyup blur', function() {
            keyLettersAndSpaces(this, false);
            makeUpper(this);
        });

        $('#SeguimientoPapelMoneda_apellido_responsable').bind('keyup blur', function() {
            keyLettersAndSpaces(this, false);
            makeUpper(this);
        });

        $('#SeguimientoPapelMoneda_observacion').bind('keyup blur', function() {
            keyText(this, false);
            makeUpper(this);
        });

        $('#SeguimientoPapelMoneda_nombre_responsable').bind('blur', function() {
            clearField(this);
        });

        $('#SeguimientoPapelMoneda_apellido_responsable').bind('blur', function() {
            clearField(this);
        });

        $('#SeguimientoPapelMoneda_observacion').bind('blur', function() {
            clearField(this);
        });

        $("#SeguimientoPapelMoneda_cedula_responsable").bind('blur', function() {

            var valorCedula = $(this).val();

            if (valorCedula.length > 0) {
                var cedula = valorCedula;
                buscarCedulaAutoridad(cedula);
            }
        });

        $("html, body").animate({scrollTop: 0}, "fast");
        var mensaje = 'Usted ya asigno el lote correspondiente a los estudiantes aspirantes a graduación en el plantel <?php echo '"' . $resultadoDatosPlantel[0]['nombre'] . '"'; ?>';

        $("#btnAgregarLote").unbind('click');
        $("#btnAgregarLote").click(function() {
            $("html, body").animate({scrollTop: 0}, "fast");
            agregar_lote(<?php echo $plantel_id ?>, <?php echo $periodo_actual_id ?>);


        });

        $("#datosFuncionario_form").submit(function(evt) {

            evt.preventDefault();
            var cedulaCompleta = $("#SeguimientoPapelMoneda_cedula_responsable").val();
            var cedula = new Array();
            cedula = cedulaCompleta.split("V-");
            var cedulaNueva = cedula[1];


            var data = {
                plantel_id: $("#plantel_id").val(),
                cedula_responsable: cedulaNueva,
                nombre_responsable: $("#SeguimientoPapelMoneda_nombre_responsable").val(),
                apellido_responsable: $("#SeguimientoPapelMoneda_apellido_responsable").val(),
                origen_responsable: $("#SeguimientoPapelMoneda_origen_responsable").val(),
                cargo_responsable_id: $("#SeguimientoPapelMoneda_cargo_responsable_id").val(),
                observacion: $("#SeguimientoPapelMoneda_observacion").val(),
                //     cargo_calificacion: $("#cargo_calificacion").val(),
                primer_serial: $("#primer_serial").val(),
                ultimo_serial: $("#ultimo_serial").val(),
                suma_total: $("#cantidadSerial").val()
            }

            $.ajax({
                url: "atencionSolicitud/guardarLoteSeriales",
                data: data,
                dataType: 'html',
                type: 'post',
                success: function(resp, resp2, resp3) {

                    try {

                        var json = jQuery.parseJSON(resp3.responseText);
                        if (json.statusCode === "error") {

                            $("#exitoAtenderSolicitud p").html('');
                            $("#exitoAtenderSolicitud").addClass('hide');
                            $("#respuestBuscar").removeClass('hide');
                            $("#respuestBuscar p").html(json.mensaje);//Pinto el ultimo serial segun la cantidada de estudiantes.
                            $("html, body").animate({scrollTop: 0}, "fast");
                        } else if (json.statusCode === "success") {

                            $("#respuestBuscar p").html('');
                            $("#respuestBuscar").addClass('hide');
                            $("#atenderSolicitud p").html('');
                            $("#atenderSolicitud").addClass('hide');
                            $("#exitoAtenderSolicitud").removeClass('hide');
                            $("#exitoAtenderSolicitud p").html(json.mensaje);
                            $("#informacion").addClass('hide');
                            $("html, body").animate({scrollTop: 0}, "fast");

                        }
                        Loading.hide();
                    } catch (e) {


                        //                        $("#error_agregarLote p").html('');
                        //                        $("#error_agregarLote").addClass('hide');
                        //                        $("#alertas_agregarLote p").html('');
                        //                        $("#alertas_agregarLote").addClass('hide');
                        //                        $("#ultimo_serial").val(resp);
                        //                        $("#cantidad_serial").val(cantidad_graduando);
                        Loading.hide();
                    }
                    Loading.hide();
                }
            });
        });

    });





    //* DATOS A PARTIR DE LA CEDULA DE IDENTIDAD  *

    function CedulaFormat(vCedulaName, evento) {
        tecla = getkey(evento);
        vCedulaName.value = vCedulaName.value.toUpperCase();
        vCedulaValue = vCedulaName.value;
        valor = vCedulaValue.substring(2, 12);
        tam = vCedulaValue.length;
        var numeros = '0123456789/';
        var digit;
        var shift;
        var ctrl;
        var alt;
        var escribo = true;
        tam = vCedulaValue.length;

        if (shift && tam > 1) {
            return false;
        }
        for (var s = 0; s < valor.length; s++) {
            digit = valor.substr(s, 1);
            if (numeros.indexOf(digit) < 0) {
                noerror = false;
                break;
            }
        }
        if (escribo) {
            if (tecla == 8 || tecla == 37) {
                if (tam > 2)
                    vCedulaName.value = vCedulaValue.substr(0, tam - 1);
                else
                    vCedulaName.value = '';
                return false;
            }
            if (tam == 0 && tecla == 69) {
                vCedulaName.value = 'E-';
                return false;
            }
            if (tam == 0 && tecla == 86) {
                vCedulaName.value = 'V-';
                return false;
            }
            else if ((tam == 0 && !(tecla < 14 || tecla == 69 || tecla == 86 || tecla == 46)))
                return false;
            else if ((tam > 1) && !(tecla < 14 || tecla == 16 || tecla == 46 || tecla == 8 || (tecla >= 48 && tecla <= 57) || (tecla >= 96 && tecla <= 105)))
                return false;
        }
    }

    function getkey(e) {
        if (window.event) {

            shift = event.shiftKey;
            ctrl = event.ctrlKey;
            alt = event.altKey;
            return window.event.keyCode;
        }
        else if (e) {
            var valor = e.which;
            if (valor > 96 && valor < 123) {
                valor = valor - 32;
            }
            return valor;
        }
        else
            return null;
    }

    function buscarCedulaAutoridad(cedula) {

        if (cedula != '' || cedula != null) {
            $.ajax({
                url: "atencionSolicitud/buscarCedula",
                data: {cedula: cedula},
                dataType: 'json',
                type: 'get',
                success: function(resp) {

                    if (resp.statusCode === "mensaje") {
                        dialogo_error(resp.mensaje);
                        $("#SeguimientoPapelMoneda_cedula_responsable").val('');
                        $("#SeguimientoPapelMoneda_nombre_responsable").val('');
                        $("#SeguimientoPapelMoneda_apellido_responsable").val('');

                    }
                    if (resp.statusCode === "successU") {
                        var nombres = $.trim(resp.nombre);
                        var apellidos = $.trim(resp.apellido);

                        $("#SeguimientoPapelMoneda_nombre_responsable").val(nombres);
                        $("#SeguimientoPapelMoneda_apellido_responsable").val(apellidos);

                        if (resp.error === true) {
                            dialogo_error(resp.mensaje);
                            $("#SeguimientoPapelMoneda_cedula_responsable").val('');
                            $("#SeguimientoPapelMoneda_nombre_responsable").val('');
                            $("#SeguimientoPapelMoneda_apellido_responsable").val('');
                            $("#SeguimientoPapelMoneda_cedula_responsable").reset();
                        }

                    }
                    if (resp.statusCode === "error") {
                        dialogo_error(resp.mensaje);

                        $("#SeguimientoPapelMoneda_cedula_responsable").val('');
                        $("#SeguimientoPapelMoneda_nombre_responsable").val('');
                        $("#SeguimientoPapelMoneda_apellido_responsable").val('');

                    }
                    if (resp.bloqueo === true) {


                        $("#SeguimientoPapelMoneda_nombre_responsable").attr('readonly', true);
                        $("#SeguimientoPapelMoneda_apellido_responsable").attr('readonly', true);

                    }
                    if (resp.bloqueo === false) {

                        $("#SeguimientoPapelMoneda_nombre_responsable").attr('readonly', false);
                        $("#SeguimientoPapelMoneda_apellido_responsable").attr('readonly', false);

                    }
                }


            });
        }
    }



    $("#btnAsignacionSerialPorPlantel").unbind('click');
    $("#btnAsignacionSerialPorPlantel").click(function() {

        var plantel_id = <?php echo $plantel_id; ?>;
        Loading.show();
        $.ajax({
            url: "/titulo/atencionSolicitud/guardarAsignacionSerialesPorPlantel",
            data: {plantel_id: plantel_id},
            dataType: 'html',
            type: 'post',
            success: function(resp, resp2, resp3) {

                try {

                    var json = jQuery.parseJSON(resp3.responseText);
                    if (json.statusCode === "success") {

                        $("#errorPorPlantel p").html('');
                        $("#errorPorPlantel").addClass('hide');
                        $("#listaSolicitudPorPlantel").addClass('hide');
                        $("#exitoPorPlantel").removeClass('hide');
                        $("#exitoPorPlantel p").html(json.mensaje);
                        $("html, body").animate({scrollTop: 0}, "fast");

                    } else if (json.statusCode === "error") {

                        $("#exitoPorPlantel p").html('');
                        $("#exitoPorPlantel").addClass('hide');
                        $("#errorPorPlantel").removeClass('hide');
                        $("#errorPorPlantel p").html(json.mensaje);
                        $("html, body").animate({scrollTop: 0}, "fast");

                    }
                    Loading.hide();
                } catch (e) {

                    //     window.location.href = 'http://escolar.dev/titulo/atencionSolicitud/mostrarConsultaPlantel/tipoBusqueda/' + tipo_busqueda + '/' + 'codigoe' + '/' + codigo_e + '/' + 'codigop' + '/' + codigo_p + '/';
                    //$("#index").html(resp);
                    $("html, body").animate({scrollTop: 0}, "fast");
                    Loading.hide();
                }
                Loading.hide();
            }
        });

    });

</script>
