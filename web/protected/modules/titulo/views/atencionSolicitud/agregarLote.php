<?php
$fecha_entrega = date("d-m-Y");
$usuario = Yii::app()->user->name;
?>
<input type="hidden" id="periodo_actual_id" value="<?php echo $periodo_actual_id ?>" name="periodo_actual_id">
<input type="hidden" id="plantel_id" value="<?php echo $plantel_id ?>" name="plantel_id">
<div id="error_agregar" class="hide errorDialogBox" ><p></p> </div>

<div class="infoDialogBox">
    <p>
        Por favor debe ingresar el primer y ultimo serial para agregar un lote de seriales.
    </p>
</div>


<div class = "widget-box">

    <div class = "widget-header">
        <h5>Agregar Lote</h5>

        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-down"></i>
            </a>
        </div>

    </div>

    <div class = "widget-body">
        <div style = "display: block;" class = "widget-body-inner">
            <div class = "widget-main">

                <div id="error_agregarLote" class="hide alertDialogBox" ><p></p> </div>
                <div id="alertas_agregarLote" class="hide alertDialogBox" ><p></p> </div>

                <div class="row align-center">
                    <div class="col-md-12">

                        <div class="col-md-4">
                            <?php echo CHtml::label('Primer Serial <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('primer_serial', '', array('class' => 'span-7', 'maxlength' => 10)); ?>
                        </div>

                        <div class="col-md-4">
                            <?php echo CHtml::label('Ultimo Serial <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('ultimo_serial', '', array('class' => 'span-7', 'maxlength' => 10)); ?>
                        </div>

                        <div class="col-md-4">
                            <?php echo CHtml::label('Cantidad Serial', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('cantidad_serial', '', array('class' => 'span-7', 'maxlength' => 10)); ?>
                        </div>

                    </div>

                    <div class = "col-md-12"><div class = "space-6"></div></div>

                    <div class="col-md-12">

                        <div class="col-md-4">
                            <?php echo CHtml::label('Fecha de Entrega', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('fecha_entrega', "$fecha_entrega", array('class' => 'span-7', 'maxlength' => 10)); ?>
                        </div>

                        <div class="col-md-4">
                            <?php echo CHtml::label('Asignado Por', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('asignado', "$usuario", array('class' => 'span-7', 'maxlength' => 10)); ?>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function() {
        $('#primer_serial').bind('keyup blur', function() {
            keyNum(this, false);
        });
        //    $("#ultimo_serial").attr('readonly', true);
        $("#cantidad_serial").attr('readonly', true);
        $("#asignado").attr('readonly', true);
        $("#fecha_entrega").attr('readonly', true);
        //    $("#primer_serial span").addClass('required').html('*');


//        $("#primer_serial").blur(function() {
//            Loading.show();
//            var mensaje = '';
//            var error = false;
//            //    var cantidad_graduando =<?php // echo $cantidadGraduando;                           ?>;
//            var data = {
//                primer_serial: $("#primer_serial").val(),
//                ultimo_serial: $("#ultimo_serial").val(),
//                cantidad_serial: $("#cantidad_serial").val()
//            }
//            var primer_serial = $("#primer_serial").val();
//
//            //   alert($("#cantidad_serial").val());
//            if (primer_serial == '') {
//                mensaje = mensaje + "Por favor el primer serial no puede estar vacío <br>";
//                error = true;
//            }
//            if (!/^([0-9])*$/.test(primer_serial)) {
//                mensaje = mensaje + "Por favor el primer serial solo puede contener números, por favor verifique el dato que introdujo <br>";
//                error = true;
//            }
//
////            if ($("#ultimo_serial").val() != '') {
////                mensaje = mensaje + "El campo ultimo serial no puede contener nada por que el código es generado por el sistema <br>";
////                error = true;
////            }
////            if ($("#cantidad_serial").val() != '') {
////                mensaje = mensaje + "El campo cantidad de serial no puede contener nada por el código es generado por el sistema <br>";
////                error = true;
////            }
//
//            if (error == false) {
//                Loading.show();
//                $.ajax({
//                    url: "/titulo/atencionSolicitud/verificarSerial",
//                    data: data,
//                    dataType: 'html',
//                    type: 'post',
//                    success: function(resp, resp2, resp3) {
//                        try {
//
//                            var json = jQuery.parseJSON(resp3.responseText);
//                            if (json.statusCode === "ultimoSerial") {
//
//                                $("#error_agregarLote p").html('');
//                                $("#error_agregarLote").addClass('hide');
//                                $("#alertas_agregarLote p").html('');
//                                $("#alertas_agregarLote").addClass('hide');
//
////                                var cantidad_estudiantes = json.mensaje - primer_serial + 1;
////
////                                if (cantidad_estudiantes > 0) {
////
////                                    $("#cantidad_serial").val(cantidad_estudiantes);//Pinto la cantidad de estudiantes graduando.
////                                }
//                                $("#ultimo_serial").val(json.mensaje);//Pinto el ultimo serial segun la cantidada de estudiantes.
//
//                            } else if (json.statusCode === "alert") {
//
//                                $("#error_agregarLote p").html('');
//                                $("#error_agregarLote").addClass('hide');
//                                $("#alertas_agregarLote").removeClass('hide');
//                                $("#alertas_agregarLote p").html(json.mensaje);
//                                $("#ultimo_serial").val('');
//                                $("#cantidad_serial").val('');
//                            }
//
//                            Loading.hide();
//                        } catch (e) {
//
//                            $("#error_agregarLote p").html('');
//                            $("#error_agregarLote").addClass('hide');
//                            $("#alertas_agregarLote p").html('');
//                            $("#alertas_agregarLote").addClass('hide');
//                            $("#ultimo_serial").val(resp);
//                            //     $("#cantidad_serial").val(cantidad_graduando);
//                            Loading.hide();
//                        }
//                        Loading.hide();
//                    }
//                });
//            } else {
//                $("#ultimo_serial").val('');
//                $("#cantidad_serial").val('');
//                $("#error_agregarLote").removeClass('hide');
//                $("#error_agregarLote p").html(mensaje);
//            }
//            Loading.hide();
//        });









        $("#ultimo_serial").blur(function() {
            Loading.show();
            var mensaje = '';
            var error = false;
            var data = {
                primer_serial: $("#primer_serial").val(),
                ultimo_serial: $("#ultimo_serial").val(),
                periodo_actual_id: $("#periodo_actual_id").val(),
                plantel_id: $("#plantel_id").val()
            }
            var ultimo_serial = $("#ultimo_serial").val();
            var primer_serial = $("#primer_serial").val();
            if (primer_serial == '') {
                mensaje = mensaje + "Por favor el primer serial no puede estar vacío <br>";
                error = true;
            }
            if (ultimo_serial == '') {
                mensaje = mensaje + "Por favor el ultimo serial no puede estar vacío <br>";
                error = true;
            }
            if (!/^([0-9])*$/.test(ultimo_serial)) {
                mensaje = mensaje + "Por favor el ultimo serial solo puede contener números, por favor verifique el dato que introdujo <br>";
                error = true;
            }

            if (!/^([0-9])*$/.test(primer_serial)) {
                mensaje = mensaje + "Por favor el primer serial solo puede contener números, por favor verifique el dato que introdujo <br>";
                error = true;
            }

            if (ultimo_serial < primer_serial) {
                mensaje = mensaje + "Por favor el dato que introdujo en el campo ultimo serial no puede ser menor al primer serial, intente nuevamente <br>";
                error = true;
            }

            if (error == false) {
                Loading.show();
                //     var error_ultimoSerial = 'Por favor verifique el ultimo serial que ingreso ya que ese limite de seriales no estan disponibles, intente nuevamente <br>';
                $.ajax({
                    url: "atencionSolicitud/mostrarCantidadEstudiante",
                    data: data,
                    dataType: 'json',
                    type: 'post',
                    success: function(resp) {
                        //       try {

                        //      var json = jQuery.parseJSON(resp3.responseText);
                        if (resp.statusCode === "cantidadTitulo") {


                            $("#error_agregarLote p").html('');
                            $("#error_agregarLote").addClass('hide');
                            $("#alertas_agregarLote p").html('');
                            $("#alertas_agregarLote").addClass('hide');
                            //   $("#cantidad_serial").val(cantidad_graduando);//Pinto la cantidad de estudiantes graduando.
                            $("#cantidad_serial").val(resp.cantidadTitulo);//Pinto el ultimo serial segun la cantidada de estudiantes.
                        }
                        if (resp.statusCode === "alert") {
                            $("#error_agregarLote p").html('');
                            $("#error_agregarLote").addClass('hide');
                            $("#alertas_agregarLote").removeClass('hide');
                            $("#alertas_agregarLote p").html(resp.mensaje);
                            //   $("#ultimo_serial").val('');
                            $("#cantidad_serial").val('');
                        }
                        Loading.hide();
                    }
                });
            } else {
                //     $("#ultimo_serial").val('');
                $("#cantidad_serial").val('');
                $("#error_agregarLote").removeClass('hide');
                $("#error_agregarLote p").html(mensaje);
            }
            Loading.hide();
        });





    });

</script>