<div class="row-fluid">
    <div class="widget-box">

        <div class="widget-header">
            <h5>Carga Masiva de Seriales de Papel Moneda</h5>

            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>

        </div>

        <div class="widget-body">
            <div class="widget-body-inner" style="display:block;">
                <div class="widget-main form">
                    <div class="row row-fluid">

                        <div class="infoDialogBox">
                            <p>
                                Descargue el archivo que contiene el formato de carga de Seriales para Titulos. El archivo debe estar en formato xls o odt calc.
                            </p>
                        </div>

                        <div class="space-6"></div>

                        <div class="text-center">
                            <a id="link_lightbox_descarga_formato" href="/public/images/formatos/formato_seriales.png">
                                <img src="/public/images/file_xls.png" />
                            </a>
                            <div class="space-6"></div>
                            <span class="btn btn-success smaller-90 fileinput-button">
                                <i class="fa fa-upload"></i>
                                <span> Seleccione un archivo...</span>
                                <!-- The file input field used as target for the file upload widget -->
                                <input type="file" name="files[]" id="fileupload" />
                            </span>
                            <input type="hidden" name="csrfToken" id="csrfToken" value="<?php echo $token; ?>" />
                        </div>
                    </div>
                    <div class="grid-view" id="papel-moneda-grid">
                    </div>
                    <div class="pager">
                        <ul class="paginator" id="pagination-papel-moneda-grid">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>