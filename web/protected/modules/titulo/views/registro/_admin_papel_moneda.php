<?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'id' => 'clase-plantel-grid',
        'filter' => $model,
        'dataProvider' => $model->search(),
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
            function(){

                $('#PapelMoneda_fecha_ini').datepicker();
                $.datepicker.setDefaults($.datepicker.regional = {
                        dateFormat: 'dd-mm-yy',
                        showOn:'focus',
                        showOtherMonths: false,
                        selectOtherMonths: true,
                        changeMonth: true,
                        changeYear: true,
                        minDate: new Date(1800, 1, 1),
                        maxDate: 'today'
                    });

                $('#PapelMoneda_serial').bind('keyup blur', function () {
                    keyNum(this, false);
                });

                $('#PapelMoneda_serial').bind('blur', function () {
                    clearField(this);
                });

                $('#PapelMoneda_codigo_plantel').bind('keyup blur', function () {
                    keyText(this, true);
                });

                $('#PapelMoneda_codigo_plantel').bind('blur', function () {
                    clearField(this, true);
                });

                $('#PapelMoneda_fecha_ini').bind('blur', function () {
                    clearField(this);
                });

                $('#PapelMoneda_fecha_ini').bind('blur', function () {
                    keyNum(this, false, true);
                });

            }
        ",
        'columns' => array(
            array(
                'header' => '<center>Serial</center>',
                'name' => 'serial',
                'filter' => CHtml::textField('PapelMoneda[serial]', $model->serial, array('title' => 'Serial del Facsímil', 'maxlength' => '20')),
            ),
            array(
                'header' => '<center>Estatus Actual</center>',
                'name' => 'estatus_actual_id',
                'value' => '(is_object($data->estatusActual))?$data->estatusActual->nombre:""',
                'filter' => CHtml::listData(
                    EstatusTitulo::model()->findAll(
                        array(
                            'condition' => "estatus = 'A'",
                            'order' => 'id ASC'
                        )
                    ), 'id', 'nombre'
                ),
            ),
            array(
                'header' => '<center>Zona Educativa</center>',
                'name' => 'zona_educativa_id',
                'value' => '(is_object($data->zonaEducativa))?$data->zonaEducativa->nombre:""',
                'filter' => CHtml::listData(
                    ZonaEducativa::model()->findAll(
                        array(
                            'condition' => "estatus = 'A'",
                            'order' => 'id ASC'
                        )
                    ), 'id', 'nombre'
                ),
            ),
            array(
                'header' => '<center> Plantel Asignado </center>',
                'name' => 'cod_plantel',
                'value' => '(is_object($data->plantelAsignado))? $data->plantelAsignado->cod_plantel.": ".$data->plantelAsignado->nombre : ""',
                'filter' => CHtml::textField('PapelMoneda[cod_plantel]', (is_object($model->plantelAsignado))? $model->plantelAsignado->cod_plantel:"", array('title' => 'Código DEA del Plantel Asignado', 'maxlength' => '15')),
            ),
            array(
                'header' => '<center>Fecha de Registro</center>',
                'name' => 'fecha_ini',
                'value' => array($this, 'fechaIni'),
                'filter' => CHtml::textField('PapelMoneda[fecha_ini]', Utiles::transformDate($model->fecha_ini, '-', 'ymd', 'dmy'), array('placeHolder' => 'DD-MM-AAAA',)),
            ),
            array(
                'header' => '<center>Registrado por</center>',
                'name' => 'username_ini',
                'value' => '(is_object($data->usuarioIni))?$data->usuarioIni->username.": ".$data->usuarioIni->nombre." ".$data->usuarioIni->apellido:""',
                'filter' => CHtml::textField('PapelMoneda[username_ini]', (is_object($model->usuarioIni))?$model->usuarioIni->username:"", array('title' => 'Usuario que ha efectuado el Registro', 'maxlength' => '15')),
            ),
        ),
        'emptyText' => 'No se han encontrado Solicitudes o Notificaciones activas, si desea puede filtrar las solicitudes por otros estatus!',
    ));
?>