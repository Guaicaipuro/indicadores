<?php
/* @var $this RegistroController */

$this->breadcrumbs = array(
    'Título' => '/titulo',
    'Registro de Seriales',
);
?>
<div class="col-xs-12">
    <div class="row-fluid">
        <div class="form">
            <div class="tabbable">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#listaPapelMoneda">
                            <i class="fa fa-bars"></i> Lista de Papel Moneda
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#cargaMasiva">
                            <i class="fa fa-file-excel-o"></i> Carga Masiva por Archivo
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#cargaProgramada">
                            <i class="fa fa-tasks"></i> Carga Programada por Rangos de Seriales
                        </a>
                    </li>
                </ul>
                <div class="tab-content">

                    <div id="listaPapelMoneda" class="tab-pane active">
                        <?php $this->renderPartial('_admin_papel_moneda', array('model'=>$model)); ?>
                    </div>

                    <div id="cargaMasiva" class="tab-pane">
                        <?php $this->renderPartial('_form_carga_masiva', array('model'=>$model, 'token'=>$token)); ?>
                    </div>

                    <div id="cargaProgramada" class="tab-pane">
                        <div class="row">
                            <div id="infoCargaProgramada">
                                <div class="infoDialogBox">
                                    <p>
                                        La Solicitud de Registro Programado por rango de seriales permite efectuar el Registro de Seriales de forma asíncrona. Este registro se efectuará mediante un proceso automático que se ejecuta con una periodicidad de cada 4 Horas.
                                    </p>
                                </div>
                            </div>
                            <div class="row-fluid col-md-12">
                                <div class="text-right">
                                    <button id="btn_registro_programado" class="btn btn-success btn-next btn-sm">
                                        <i class="fa fa-plus icon-on-right"></i>
                                        Nueva Petición de Registro Programado
                                    </button>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row-fluid col-md-12">
                                <?php $this->renderPartial('_admin_carga_programada', array('registroProgramado'=>$registroProgramado)); ?>
                            </div>
                        </div>
                        <div id="dialogFormCargaProgramada" class="hide"></div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="/public/js/jquery.upload/css/jquery.fileupload.css">
<link rel="stylesheet" href="/public/js/jquery.upload/css/jquery.fileupload-ui.css">
<link rel="stylesheet" href="/public/js/fancybox-1.3.4/jquery.fancybox-1.3.4.css">

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="/public/js/jquery.upload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="/public/js/jquery.upload/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="/public/js/jquery.upload/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<!-- blueimp Gallery script -->
<script src="/public/js/jquery.upload/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/public/js/jquery.upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload validation plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload-ui.js"></script>

<script src="/public/js/bootstrap-paginator.js"></script>

<script src="/public/js/fancybox-1.3.4/jquery.fancybox-1.3.4.js"></script>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/titulo/registro/masivo.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/titulo/registro/programada.js',CClientScript::POS_END); ?>