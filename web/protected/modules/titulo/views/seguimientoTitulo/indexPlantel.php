<div id="index">
    <?php
    $this->breadcrumbs = array(
        'Consulta de Planteles' => array('/planteles/consultar/'),
        'Validación de Entrega de Seriales',
    );
    ?>

    <div id="error" class="hide errorDialogBox" >
        <p></p>
    </div>

    <div id="alerta" class="hide alertDialogBox" >
        <p></p>
    </div>
    <div id="seleccionEstudiante">
    </div>

    <div id="exitoso" class="hide successDialogBox" >
        <p></p>

        <div class="col-md-6" style="padding-top: 4%">
            <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("titulo/seguimientoTitulo/indexPlantel/id/" . base64_encode($plantel_id)); ?>"class="btn btn-danger">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
        </div>
    </div>

    <div id="zonaEdu">
        <form id="form_indexPlantel" method="post" enctype="multipart/form-data">
            <!--collapsed-->
            <div class = "widget-box ">

                <div class = "widget-header">
                    <h5>Identificación de las Autoridades que Firman el Título</h5>

                    <div class = "widget-toolbar">
                        <a href = "#" data-action = "collapse">
                            <i class = "icon-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class = "widget-body">
                    <div style = "display: block;" class = "widget-body-inner">
                        <div class = "widget-main">
                            <div class="row row-fluid center">

                                <div id="1eraFila" class="col-md-12">
                                    <table style="width:650%;" class="table table-bordered" class="align-center">
                                        <thead>
                                            <tr>
                                                <th>
                                        <p style="color: #c43030; height:5px;" class="pull-left">
                                            <b>Director</b>
                                        </p>
                                        </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="col-md-4" >
                                                        <?php echo CHtml::label('Documento de Identidad <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                                        <?php
                                                        echo CHtml::textField('documento_identidad_director', (isset($datosFirmante[0]['documento_identidad_director'])) ? $datosFirmante[0]['documento_identidad_director'] : "No Posee", array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                                        ?>
                                                    </div>

                                                    <div class="col-md-4" > <!--$plantelPK['cod_plantel']-->
                                                        <?php echo CHtml::label('Nombres <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                                        <?php echo CHtml::textField('nombre_director', (isset($datosFirmante[0]['nombre_director'])) ? $datosFirmante[0]['nombre_director'] : "No Posee", array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                                                    </div>
                                                    <div class="col-md-4" >
                                                        <?php echo CHtml::label('Apellidos <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                                        <?php echo CHtml::textField('apellido_director', (isset($datosFirmante[0]['apellido_director'])) ? $datosFirmante[0]['apellido_director'] : "No Posee", array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                                                    </div>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                <div id="2daFila" class="col-md-12">
                                    <table style="width:650%;" class="table table-bordered" class="align-center">
                                        <thead>
                                            <tr>
                                                <th>
                                        <p style="color: #c43030; height:5px;" class="pull-left">
                                            <b>Coordinador de Registro y Control de Estudio del Plantel</b>
                                        </p>
                                        </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="col-md-4" >
                                                        <?php echo CHtml::label('Documento de Identidad <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                                        <?php
                                                        echo CHtml::textField('documento_identidad_jefe', (isset($datosFirmante[1]['documento_identidad_jefe'])) ? $datosFirmante[1]['documento_identidad_jefe'] : "No Posee", array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                                        ?>
                                                    </div>
                                                    <div class="col-md-4" > <!--$plantelPK['cod_plantel']-->
                                                        <?php echo CHtml::label('Nombres <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                                        <?php echo CHtml::textField('nombre_jefe', (isset($datosFirmante[1]['nombre_jefe'])) ? $datosFirmante[1]['nombre_jefe'] : "No Posee", array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                                                    </div>
                                                    <div class="col-md-4" >
                                                        <?php echo CHtml::label('Apellidos <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                                        <?php echo CHtml::textField('apellido_jefe', (isset($datosFirmante[1]['apellido_jefe'])) ? $datosFirmante[1]['apellido_jefe'] : "No Posee", array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                                                    </div>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                <div id="3eraFila" class="col-md-12">
                                    <table style="width:650%;" class="table table-bordered" class="align-center">
                                        <thead>
                                            <tr>
                                                <th>
                                        <p style="color: #c43030; height:5px;" class="pull-left">
                                            <b>Funcionario Designado en Gaceta Oficial</b>
                                        </p>
                                        </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="col-md-4" >
                                                        <?php echo CHtml::label('Documento de Identidad <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                                        <?php
                                                        echo CHtml::textField('documento_identidad_funcionario', (isset($datosFirmante[2]['documento_identidad_funcionario'])) ? $datosFirmante[2]['documento_identidad_funcionario'] : "No Posee", array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                                        ?>
                                                    </div>
                                                    <div class="col-md-4" > <!--$plantelPK['cod_plantel']-->
                                                        <?php echo CHtml::label('Nombres <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                                        <?php echo CHtml::textField('nombre_funcionario', (isset($datosFirmante[2]['nombre_funcionario'])) ? $datosFirmante[2]['nombre_funcionario'] : "No Posee", array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                                                    </div>
                                                    <div class="col-md-4" >
                                                        <?php echo CHtml::label('Apellidos <span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                                        <?php echo CHtml::textField('apellido_funcionario', (isset($datosFirmante[2]['apellido_funcionario'])) ? $datosFirmante[2]['apellido_funcionario'] : "No Posee", array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                                                    </div>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class = "col-md-12"><div class = "space-6"></div></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class = "widget-box">

                <div class = "widget-header">
                    <h5>Validación de Entrega de Seriales a los Estudiantes del Plantel: <?php echo $nombrePlantel; ?> </h5>

                    <div class = "widget-toolbar">
                        <a href = "#" data-action = "collapse">
                            <i class = "icon-chevron-down"></i>
                        </a>
                    </div>

                </div>

                <div class = "widget-body">
                    <div style = "display: block;" class = "widget-body-inner">
                        <div class = "widget-main">
                            <div id="informacion" style="padding-top: 4px">
                                <?php if (isset($resultadoEstudiante) && $resultadoEstudiante !== array()) { ?>
                                    <div class="infoDialogBox">
                                        <p>
                                            <b> Por favor modifique los datos de los estudiantes que se requiera y seleccione los que ya se le entregó el título.</b>
                                        </p>
                                    </div>
                                <?php } ?>
                                <?php if (isset($mostrarEntregadoEstudiante) && $mostrarEntregadoEstudiante !== array()) { ?>
                                    <?php if ($verificarControlEstudiante == 4) { ?>
                                        <div class="successDialogBox">
                                            <p>
                                                <b>El proceso de validación de título a los estudiantes ha culminado con exito. <br>
                                                    A continuación se le muestra los estudiantes del plantel <?php echo $nombrePlantel; ?> que ya les fue entregado el título.</b>
                                            </p>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>

                            <div class="row row-fluid" style="padding-bottom: 4px; padding-top: 4px">
                                <?php if (isset($resultadoEstudiante) && $resultadoEstudiante !== array()) { ?>
                                    <div id="1eraFilaView" class="col-md-12">
                                        <div class="col-md-4" > <!--$plantelPK['cod_plantel']-->
                                            <?php echo CHtml::label('Fecha de Expedición de Título<span class="required">*</span>', '', array("class" => "col-md-12")); ?>
                                            <?php echo CHtml::textField('fecha_otorgamiento', '', array('class' => 'span-7')); ?>
                                        </div>
                                    </div>

                                    <div id="2daFilaView" class="col-md-12">

                                        <div class = "col-md-12"><div class = "space-6"></div></div>
                                        <input type="hidden" id="plantel_id"
                                               value="<?php echo base64_encode($plantel_id); ?>">
                                        <div class="col-md-12">

                                            <?php
                                            $this->widget(
                                                    'zii.widgets.grid.CGridView', array(
                                                'id' => 'seriales_plantel',
                                                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                                'dataProvider' => $resultadoEstudiante,
                                                'summaryText' => false,
                                                'columns' => array(
                                                    array(
                                                        'name' => 'key',
                                                        'type' => 'raw',
                                                        'header' => '<center></center>'
                                                    ),
                                                    array(
                                                        'name' => 'documento_identidad',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Documento Identidad</b></center>'
                                                    ),
                                                    array(
                                                        'name' => 'nombresFinal',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Nombres</b></center>'
                                                    ),
                                                    array(
                                                        'name' => 'apellidosFinal',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Apellidos</b></center>'
                                                    ),
                                                    array(
                                                        'name' => 'serial',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Serial</b></center>'
                                                    ),
                                                    array(
                                                        'header' => '<center><b>Estatus del Título Anterior</b></center>',
                                                        'name' => 'estatusTitulo_asignacion',
                                                        //  'value' => array($this, 'columnaEstatusTitulo'),
                                                        'type' => 'raw'
                                                    ),
//                                                    array(
//                                                        'header' => '<center><b>Cambiar Documento Identidad</b></center>',
//                                                        'value' => array($this, 'columnaDocumentoIdent'),
//                                                        'type' => 'raw'
//                                                    ),
                                                    array(
                                                        'header' => '<center><b>Cambiar Serial</b></center>',
                                                        'name' => 'serialNuevo_asignacion',
                                                        //'value' => array($this, 'columnaSerial'),
                                                        'type' => 'raw'
                                                    ),
                                                    array(
                                                        'header' => '<center><b>Año Egreso</b></center>',
                                                        'name' => 'anio_egreso',
                                                       // 'value' => array($this, 'columnaAnioEgreso'),
                                                        'type' => 'raw'
                                                    ),
                                                    array(
                                                        'header' => '<center><b>Tipo de Evaluación</b></center>',
                                                        'name' => 'tipo_evaluacion',
                                                        //'value' => array($this, 'columnaTipoEvaluacion'),
                                                        'type' => 'raw',
                                                    ),
                                                    array(
                                                        "name" => "botones",
                                                        "type" => "raw",
                                                        'header' => '<div class="center">' . '<b>Entregado Estudiante</b>' . '<br> ' . CHtml::checkBox('selec_todoEstudiante', false, array('id' => 'selec_todoEstudiante', 'title' => 'Título Entregado a los Estudiantes', 'class' => 'tooltipMatricula')) . "</div>"
                                                    ),
                                                ),
                                                'pager' => array(
                                                    'header' => '',
                                                    'htmlOptions' => array('class' => 'pagination'),
                                                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                    'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                    'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                    'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                                ),
                                                    )
                                            );
                                        }
                                        ?>
                                        <?php
                                        if (isset($resultadoEstudiante) && $resultadoEstudiante !== array() && isset($mostrarEntregadoEstudiante) && $mostrarEntregadoEstudiante !== array() && ($verificarControlEstudiante == 0)) {
                                            echo '<hr>';
                                            echo '<div class="successDialogBox">
                                        <p>
                                            <b>
                                                Estudiantes que ya verificó la entrega de Títulos.
                                            </b>
                                        </p>
                                    </div>';
                                        }
                                        ?>
                                        <?php
                                        if (isset($mostrarEntregadoEstudiante) && $mostrarEntregadoEstudiante !== array() && ($verificarControlEstudiante == 4 || $verificarControlEstudiante == 0 )) {
                                            $this->widget(
                                                    'zii.widgets.grid.CGridView', array(
                                                'id' => 'mostrar_seriales_Estudiante',
                                                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                                'dataProvider' => $mostrarEntregadoEstudiante,
                                                'summaryText' => false,
                                                'columns' => array(
                                                    array(
                                                        'name' => 'key',
                                                        'type' => 'raw',
                                                        'header' => '<center></center>'
                                                    ),
                                                    array(
                                                        'name' => 'documento_identidad',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Documento Identidad</b></center>'
                                                    ),
                                                    array(
                                                        'name' => 'nombresFinal',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Nombres</b></center>'
                                                    ),
                                                    array(
                                                        'name' => 'apellidosFinal',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Apellidos</b></center>'
                                                    ),
                                                    array(
                                                        'header' => '<center><b>Estatus del Título Anterior</b></center>',
                                                        'name' => 'estatusTitulo',
                                                        'type' => 'raw'
                                                    ),
                                                    array(
                                                        'name' => 'serial',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Serial</b></center>'
                                                    ),
                                                    array(
                                                        'name' => 'anio_egreso',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Año Egreso</b></center>'
                                                    ),
                                                    array(
                                                        'name' => 'tipo_evaluacion',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Tipo de Evaluación</b></center>'
                                                    ),
                                                    array(
                                                        'name' => 'fecha_otorgamiento',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Fecha de Expedición</b></center>'
                                                    ),
                                                    array(
                                                        'name' => 'asignadoEstudiante',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Verificación de Entrega</b></center>'
                                                    ),
                                                ),
                                                'pager' => array(
                                                    'header' => '',
                                                    'htmlOptions' => array('class' => 'pagination'),
                                                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                    'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                    'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                    'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                                ),
                                                    )
                                            );
                                            ?>
                                            <div class = "pull-left" style ="padding-left: 20px; padding: 20px">
                                                <a  href="<?php echo Yii::app()->createUrl("/titulo/seguimientoTitulo/reporteSerialesEntregados/plantel/" . base64_encode($plantel_id)); ?>"  id="reporteSerialesEntregados"   class = "btn btn-primary btn-next btn-sm ">
                                                    Imprimir Reporte
                                                    <i class=" fa-file-text-o icon-on-right"></i>
                                                </a>
                                            </div>
                                        <?php } ?>
                                        <?php if ($mostrarEstudiante == false) { ?>
                                            <table style="width:650%;" class="table table-striped table-bordered table-hover" class="align-center">
                                                <thead>

                                                    <tr>
                                                        <th>
                                                            <b></b>
                                                        </th>
                                                    </tr>

                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="alertDialogBox">
                                                                <p>
                                                                    Aun no se ha asignado ningún título a los estudiantes en específico del plantel <?php echo $nombrePlantel; ?>.
                                                                </p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>

                                            </table>
                                        <?php } ?>
                                        <?php if ($verificarControlEstudiante == 2) { ?>
                                            <table style="width:650%;" class="table table-striped table-bordered table-hover" class="align-center">
                                                <thead>

                                                    <tr>
                                                        <th>
                                                            <b></b>
                                                        </th>
                                                    </tr>

                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="alertDialogBox">
                                                                <p>
                                                                    El proceso de validación de la entrega del título a los estudiantes del plantel <?php echo $nombrePlantel; ?> aun no ha culminado, cuando finalice se le notificara mediante un correo.
                                                                </p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>

                                            </table>
                                        <?php } ?>

                                    </div>

                                </div>

                                <div class = "col-md-12"><div class = "space-6"></div></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="padding-top: 1%">
                    <a id="btnRegresar" href="<?php echo Yii::app()->createUrl('/planteles/consultar/'); ?>"class="btn btn-danger">
                        <i class="icon-arrow-left"></i>
                        Volver
                    </a>
                    <?php
                    if ($verificarControlEstudiante == 0) {
                        if (isset($resultadoEstudiante) && $resultadoEstudiante !== array()) {
                            ?>

                            <button id="btnAsignarEstudiante" type="button" class="btn btn-primary btn-next pull-right" role="button" aria-disabled="false"><span class="ui-button-text"><i class="icon-save info bigger-110"></i>&nbsp; Entregado al Estudiante</span>
                            </button>
                            <?php
                        }
                    }
                    ?>
                </div>



            </div>
        </form>
    </div>
    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
</div>
<div id="dialog_errorSaime" class="hide">
</div>
<div id="dialog_exitoSaime" class="hide">
</div>
<?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/select2.min.js', CClientScript::POS_END);
?>
<!--<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/select2.css" rel="stylesheet" />-->
<?php $estudiante_select = array(); ?>
<script>

//CAMBIO POR NUEVOS REQUERIMIENTOS. 03/12/14 MARISELA

//    function CedulaFormat(vCedulaName, evento) {
//        tecla = getkey(evento);
//        vCedulaName.value = vCedulaName.value.toUpperCase();
//        vCedulaValue = vCedulaName.value;
//        valor = vCedulaValue.substring(2, 10);
//        tam = vCedulaValue.length;
//        var numeros = '0123456789/';
//        var digit;
//        var shift;
//        var ctrl;
//        var alt;
//        var escribo = true;
//        tam = vCedulaValue.length;
//        primeraPosicion = vCedulaValue.substring(0, 1);
//        if (shift && tam > 1) {
//            return false;
//        }
//
//        for (var s = 0; s < valor.length; s++) {
//            digit = valor.substr(s, 1);
//            if (numeros.indexOf(digit) < 0) {
//                noerror = false;
//                break;
//            }
//        }
//
//        if (escribo) {
//            if (tecla == 8 || tecla == 37) {
//                if (tam > 2) {
//                    vCedulaName.value = vCedulaValue.substr(0, tam - 1);
//                } else
//                    vCedulaName.value = '';
//                return false;
//            }


//            if (tam == 0 && tecla == 69) {
//                vCedulaName.value = 'E-';
//                return false;
//            } else {
//                if ((tam == 0 && !(tecla < 14 || tecla == 69 || tecla == 86 || tecla == 80))) {
//                    return false;
//                } else {
//                    if (primeraPosicion == 'E') {
//                        if ((tam > 1) && !(tecla < 14 || tecla == 16 || tecla == 8 || (tecla >= 48 && tecla <= 57))) {
//                            return false;
//                        }
//                    }
//                }
//            }


//            if (tam == 0 && tecla == 86) {
//                vCedulaName.value = 'V-';
//                return false;
//            } else {
//                if ((tam == 0 && !(tecla < 14 || tecla == 69 || tecla == 86 || tecla == 80))) {
//                    return false;
//                }
//                else {
//                    if (primeraPosicion == 'V') {
//                        if ((tam > 1) && !(tecla < 14 || tecla == 16 || tecla == 8 || (tecla >= 48 && tecla <= 57))) {
//                            return false;
//                        }
//                    }
//                }
//            }

//            if (tam == 0 && tecla == 80) {
//                vCedulaName.value = 'P-';
//                return false;
//            }
//            else {
//                if ((tam == 0 && !(tecla < 14 || tecla == 69 || tecla == 86 || tecla == 80))) {
//                    return false;
//                } else {
//                    if (primeraPosicion == 'P') {
//                        if ((tam > 1) && !(tecla < 14 || tecla == 16 || tecla == 8 || (tecla >= 48 && tecla <= 57) || (tecla >= 96 && tecla <= 105) || (tecla >= 65 && tecla <= 90))) {
//                            return false;
//                        }
//                    }
//                }
//            }

//    }
//    }
//
//
//
//
//    function getkey(e) {
//    if (window.event) {
//
//    shift = event.shiftKey;
//            ctrl = event.ctrlKey;
//            alt = event.altKey;
//            return window.event.keyCode;
//    }
//    else if (e) {
//    var valor = e.which;
//            if (valor > 96 && valor < 123) {
//    valor = valor - 32;
//    }
//    return valor;
//    }
//    else
//            return null;
//    }


    $(document).ready(function() {

        $("html, body").animate({scrollTop: 0}, "fast");
//Validaciones
        $('.data-documentoNuevo').bind('keyup blur', function() {
            keyTextDash(this, false, false);
        });
        $('.data-cambiarNombres').bind('keyup blur', function() {
            keyAlpha(this, true);
            makeUpper(this);
        });
        $('.data-cambiarNombres').bind('blur', function() {
            clearField(this);
        });
        $('.data-cambiarApellidos').bind('keyup blur', function() {
            keyAlpha(this, true);
            makeUpper(this);
        });
        $('.data-cambiarApellidos').bind('blur', function() {
            clearField(this);
        });
//Fin

//Obteniendo variables de los firmantes.
        documento_director = $('#documento_identidad_director').val();
        nombre_director = $('#nombre_director').val();
        apellido_director = $('#apellido_director').val();
        documento_jefe = $('#documento_identidad_jefe').val();
        nombre_jefe = $('#nombre_jefe').val();
        apellido_jefe = $('#apellido_jefe').val();
        documento_funcionario = $('#documento_identidad_funcionario').val();
        nombre_funcionario = $('#nombre_funcionario').val();
        apellido_funcionario = $('#apellido_funcionario').val();
//Fin

        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: 'Previo',
            nextText: 'Próximo',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            dateFormat: 'dd-mm-yy',
            'showOn': 'focus',
            'showOtherMonths': false,
            'selectOtherMonths': true,
            'changeMonth': true,
            'changeYear': true,
            yearRange: "1900:today",
            maxDate: 'today',
            initStatus: 'Selecciona la fecha', isRTL: false
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
        $('#fecha_otorgamiento').datepicker();
        $('#fecha_otorgamiento').attr('readOnly', true);

        //
//CAMBIO POR NUEVOS REQUERIMIENTOS. 03/12/14 MARISELA
//
//        $('.data-documentoNuevo').unbind('blur');
//        $('.data-documentoNuevo').bind(' blur', function() {
//
//            var documento = $(this).val();
//            var cedula = new Array();
//            cedula = documento.split("-");
//            var tdocumento_identidad = cedula[0];
//            var documento_identidad = cedula[1];
//            var title;
//            var mensaje;
//            var documento_identidad_anterior;
//            var control = false;
//            var numero;
//            var documento = $(this).val();
//            var cedula = new Array();
//            cedula = documento.split("-");
//            var tdocumento_identidad = cedula[0];
//            var documento_identidad = cedula[1];
//            if (tdocumento_identidad != null && tdocumento_identidad != '') {
//                if (documento_identidad != null && documento_identidad != '') {
//
//                    var array = new Array();
//                    $('.data-documentoAnterior').each(function() {
//                        documento_identidad_anterior = base64_decode($(this).val());
//                        var primera = documento_identidad_anterior.charAt(0);
//                        var segunda = documento_identidad_anterior.charAt(1);
//                        if (primera == 'V' && segunda == '-') {
//                            numero = documento_identidad_anterior.substring(2, 15);
//                        } else {
//                            numero = documento_identidad_anterior;
//                        }
//                        if (numero != '') {
//                            array.push(numero);
//                        } else {
//                            array.push(0);
//                        }
//                    });
////Verifico que la cedula introducida sea igual a la cedula anterior.
//                    var y = 0;
//                    for (y = 0; y < array.length; y++) {
//                        if (array[y] == documento_identidad) {
//                            control = true;
//                            return false;
//                        }
//                    }
////Fin
//                    if (control == false) {
//                        validarCedula(tdocumento_identidad, documento_identidad, $(this));
//                    }
//                } else {
//                    title = 'Notificación de Error';
//                    mensaje = 'Estimado usuario por favor verifique los datos que esta introduciendo el documento de identidad no puede estar vacia.';
//                    $(this).val('');
//                    dialogo_errorSaime(mensaje, title);
//                }
//            }
//        });


        $('select[name="cambiarSerial[]"]').change(function(e) {

            var serial = $(this).val();
            //  var nserial = '';

            var resultado;
            resultado = validarSerial(serial);
            //   console.log(resultado);
            if (resultado == true) {
                var mensaje = 'Por favor seleccione otro serial ya que este serial se encuentra asignado a otro estudiante';
                //     var mensaje = 'Por favor seleccione otro serial ya que el serial ' + nserial + ' ya se encuentra asignado a otro estudiante';
                var title = 'Notificación de error';
                $(this).val('');
                dialogo_errorSaime(mensaje, title);
            }
        });

//Valido cuando quieran checked todo.
        $("#selec_todoEstudiante").unbind('click');
        $("#selec_todoEstudiante").click(function() {
            var select = $("#selec_todoEstudiante").is(':checked') ? true : false;
            if (select == true) {
                $('input[name="OtorgarEstudiante[]"]').each(function() {
                    $(this).attr('checked', 'checked');
                });
            }
            else {
                $('input[name="OtorgarEstudiante[]"]').each(function() {
                    $(this).attr('checked', false);
                });
            }
        });
//Fin


//DESBLOQUEAR CAMPO CAMBIAR SERIAL.
        $('.data-estatusTitulo').change(function() {

            var data_estatus_titulo = $(this).attr("data");
            var estatusTitulo = $('#estatusTitulo_'+ data_estatus_titulo).val();
                //console.log(estatusTitulo);
            if(estatusTitulo == 6){
                $('#cambiarSerial_' + data_estatus_titulo).attr('disabled','disabled');
                $('#anioEgreso_' + data_estatus_titulo).attr('disabled','disabled');
                $('#tipoEvaluacion_' + data_estatus_titulo).attr('disabled','disabled');
                $('#cambiarSerial_'+ data_estatus_titulo).val('');
                $('#anioEgreso_'+ data_estatus_titulo).val('');
                $('#tipoEvaluacion_'+ data_estatus_titulo).val('');
            }else{
                $('#cambiarSerial_' + data_estatus_titulo).removeAttr('disabled');
                $('#anioEgreso_' + data_estatus_titulo).removeAttr('disabled');
                $('#tipoEvaluacion_' + data_estatus_titulo).removeAttr('disabled');
            }

        });
//FIN



        var msgasignado = "<b>Estimado usuario, debe seleccionar por lo menos un Estudiante para realizar esta acción.</b>";
        $("#btnAsignarEstudiante").unbind('click');
        $("#btnAsignarEstudiante").click(function(e) {

            var mensaje = '';
            var error = false;
            var plantel_id = $('#plantel_id').val();
            var fecha_otorgamiento = $('#fecha_otorgamiento').val();
            var documento_identidad_director = $('#documento_identidad_director').val();
            var documento_identidad_jefe = $('#documento_identidad_jefe').val();
            var documento_identidad_funcionario = $('#documento_identidad_funcionario').val();
            if (fecha_otorgamiento == '') {
                mensaje = 'Por favor ingresar la fecha de expedición porque es obligatoria. <br>';
                displayDialogBox('seleccionEstudiante', 'error', mensaje);
                $("html, body").animate({scrollTop: 0}, "fast");
                $('#fecha_otorgamiento').addClass('error');
                Loading.hide();
                return false;
            }

            $('#fecha_otorgamiento').removeClass('error');
            $("#seleccionEstudiante").addClass('hide');
            $("#seleccionEstudiante p").html('');
            var estudiante_select = new Array();
            var estudiante_checked_msj = new Array();
            $('input[name="OtorgarEstudiante[]"]').each(function() {
                if ($(this).val() != '') {
                    var select = ($(this).is(':checked')) ? true : false;
                    if (select) {
                        estudiante_checked_msj.push(base64_decode($(this).val()));
                        estudiante_select.push($(this).val());
                    } else {
                        var valor_decod = 'MA=='; // valor 0 en base64_encode.
                        estudiante_checked_msj.push('0');
                        estudiante_select.push(valor_decod);
                    }
                }
            });

            var estudiantes_completos = new Array();
            var estudiantes_checked = new Array();
            $('input[name="OtorgarEstudiante[]"]').each(function() {
                //$('input[name="OtorgarEstudiante[]"]:checked')
                if ($(this).val() != '') {
                    estudiantes_completos.push($(this).val());
                    var select = ($(this).is(':checked')) ? true : false;
                    if (select) {
                        estudiantes_checked.push($(this).val());
                    }
                }
            });

//CAMBIO POR NUEVOS REQUERIMIENTOS. 03/12/14 MARISELA
//            var cambiarNombres_array = new Array();
//            $('input[name="cambiarNombres[]"]').each(function() {
//                if ($(this).val() != '') {
//                    cambiarNombres_array.push($(this).val());
//                }
//            });
//            var cambiarApellidos_array = new Array();
//            $('input[name="cambiarApellidos[]"]').each(function() {
//                if ($(this).val() != '') {
//                    cambiarApellidos_array.push($(this).val());
//                }
//            });
//            var cambiarDocumento_array = new Array();
//            $('input[name="cambiarDocumento[]"]').each(function() {
//                if ($(this).val() != '') {
//                    cambiarDocumento_array.push(base64_encode($(this).val()));
//                } else {
//                    var valor_decod = 'MA=='; // valor 0 en base64_encode.
//                    cambiarDocumento_array.push(valor_decod);
//                }
//            });

            var estatusTitulo_array = new Array();
            $('select[name="estatusTitulo[]"]').each(function() {

                if ($(this).val() != '') {
                    estatusTitulo_array.push(base64_encode($(this).val()));
                } else {
                    var valor_decod = 'MA=='; // valor 0 en base64_encode.
                    estatusTitulo_array.push(valor_decod);
                }
            });

            var cambiarSerial_array = new Array();
            $('select[name="cambiarSerial[]"]').each(function() {
                if ($(this).val() != '') {
                    cambiarSerial_array.push(base64_encode($(this).val()));
                } else {
                    var valor_decod = 'MA=='; // valor 0 en base64_encode.
                    cambiarSerial_array.push(valor_decod);
                }
            });
            var tamano = estudiante_checked_msj.length;

            var estatusTitulo_array = new Array();
            $('select[name="estatusTitulo[]"]').each(function() {
                if ($(this).val() != '') {
                    estatusTitulo_array.push($(this).val());
                } else {
                    estatusTitulo_array.push(0);
                }
            });

            var anioEgreso_array = new Array();
            $('select[name="anioEgreso[]"]').each(function() {
                if ($(this).val() != '') {
                    anioEgreso_array.push($(this).val());
                } else {
                    anioEgreso_array.push(0);
                }
            });
            var tipoEvaluacion_array = new Array();
            $('select[name="tipoEvaluacion[]"]').each(function() {
                if ($(this).val() != '') {
                    tipoEvaluacion_array.push($(this).val());
                } else {
                    tipoEvaluacion_array.push(0);
                }
            });

            //CAMBIO POR NUEVOS REQUERIMIENTOS. 03/12/14 MARISELA
//            var documentoAnterior_array = new Array();
//            $('input[name="documentoAnterior[]"]').each(function() {
//                var documento = base64_decode($(this).val());
//                var primeraPosicion = documento.substring(0, 1);
//                if (primeraPosicion != 'V' && primeraPosicion != 'E' && primeraPosicion != 'P') {
//                    documentoAnterior_array.push('0');
//                } else {
//                    documentoAnterior_array.push(base64_decode($(this).val()));
//                }
//            });
            if (tamano > 0) {
                var e = 0;
                for (e = 0; e <= estudiante_checked_msj.length; e++) {
                    if (estudiante_checked_msj[e] != '0' && (estatusTitulo_array[e] != 0 && estatusTitulo_array[e] != 6 && cambiarSerial_array[e] == 'MA==')) {
                        mensaje = mensaje + '<b>Por favor es obligatorio seleccionar el nuevo serial porque anteriormente seleccionó el estatus del titulo anterior. </b><br>';
                        error = true;
                        break;
                    }
                }
                var x = 0;
                for (x = 0; x <= estudiante_checked_msj.length; x++) {
                    if (estudiante_checked_msj[x] != '0' && anioEgreso_array[x] == 0 && estatusTitulo_array[x] != 6) {
                        mensaje = mensaje + '<b>Por favor es obligatorio seleccionar el año de egreso en todos los campos que chequeo. </b><br>';
                        error = true;
                        break;
                    }
                }

                var y = 0;
                for (y = 0; y <= estudiante_checked_msj.length; y++) {
                    if (estudiante_checked_msj[y] != '0' && tipoEvaluacion_array[y] == 0 && estatusTitulo_array[y] != 6) {
                        mensaje = mensaje + '<b>Por favor es obligatorio seleccionar el tipo de evalución en todos los campos que chequeo. </b><br>';
                        error = true;
                        break;
                    }
                }
////CAMBIO POR NUEVOS REQUERIMIENTOS. 03/12/14 MARISELA
//                var z = 0;
//                for (z = 0; z <= estudiante_checked_msj.length; z++) {
//                    if (estudiante_checked_msj[z] != '0' && documentoAnterior_array[z] == '0' && cambiarDocumento_array[z] == 'MA==') {
//                        mensaje = mensaje + '<b>Por favor modifique el tipo de documento de identidad en el campo "Cambiar Documento Identidad" ya que en el sistema no se encuentra registrado. </b><br>';
//                        error = true;
//                        break;
//                    }
//                }

            }

//CAMBIO POR NUEVOS REQUERIMIENTOS. 03/12/14 MARISELA
//            var documentoAnterior_array = new Array();
//            $('input[name="documentoAnterior[]"]').each(function() {
//                var documento = base64_decode($(this).val());
//                var primeraPosicion = documento.substring(0, 1);
//                if (primeraPosicion != 'V' && primeraPosicion != 'E' && primeraPosicion != 'P') {
//                    documentoAnterior_array.push('0');
//                } else {
//                    documentoAnterior_array.push(base64_decode($(this).val()));
//                }
//                var z = 0;
//                for (z = 0; z <= estudiante_checked_msj.length; z++) {
//                    if (estudiante_checked_msj[z] != '0' && documentoAnterior_array[z] == '0' && cambiarDocumento_array[z] == 'MA==') {
//                        mensaje = mensaje + '<b>Por favor modifique el tipo de documento de identidad en el campo "Cambiar Documento Identidad" ya que en el sistema no se encuentra registrado. </b><br>';
//                        error = true;
//                        return false;
//                    }
//                }
//            });
//Fin

//Validación del los firmantes del titulo
            if (documento_director == 'No Posee') {
                mensaje = mensaje + '<b>Por favor es obligatorio que ingrese el documento de identidad del director ya que no se encuentra en nuestro sistema. </b><br>';
                error = true;
            }
            if (nombre_director == 'No Posee') {
                mensaje = mensaje + '<b>Por favor es obligatorio que ingrese el nombre del director ya que no se encuentra en nuestro sistema. </b><br>';
                error = true;
            }
            if (apellido_director == 'No Posee') {
                mensaje = mensaje + '<b>Por favor es obligatorio que ingrese el apellido del director ya que no se encuentra en nuestro sistema. </b><br>';
                error = true;
            }
            if (documento_jefe == 'No Posee') {
                mensaje = mensaje + '<b>Por favor es obligatorio que ingrese el documento de identidad del jefe de control de estudio ya que no se encuentra en nuestro sistema. </b><br>';
                error = true;
            }
            if (nombre_jefe == 'No Posee') {
                mensaje = mensaje + '<b>Por favor es obligatorio que ingrese el nombre del jefe de control de estudio ya que no se encuentra en nuestro sistema. </b><br>';
                error = true;
            }
            if (apellido_jefe == 'No Posee') {
                mensaje = mensaje + '<b>Por favor es obligatorio que ingrese el apellido del jefe de control de estudio ya que no se encuentra en nuestro sistema. </b><br>';
                error = true;
            }
            if (documento_funcionario == 'No Posee') {
                mensaje = mensaje + '<b>Por favor es obligatorio que ingrese el documento de identidad del funcionario designado en gaceta oficial ya que no se encuentra en nuestro sistema, contacte al jefe de control de estudio correspondiente para que realice la actualización de los datos del funcionario. </b><br>';
                error = true;
            }
            if (nombre_funcionario == 'No Posee') {
                mensaje = mensaje + '<b>Por favor es obligatorio que ingrese el nombre del funcionario designado en gaceta oficial ya que no se encuentra en nuestro sistema, contacte al jefe de control de estudio correspondiente para que realice la actualización de los datos del funcionario. </b><br>';
                error = true;
            }
            if (apellido_funcionario == 'No Posee') {
                mensaje = mensaje + '<b>Por favor es obligatorio que ingrese el apellido del funcionario designado en gaceta oficial  ya que no se encuentra en nuestro sistema, contacte al jefe de control de estudio correspondiente para que realice la actualización de los datos del funcionario. </b><br>';
                error = true;
            }
//Fin

            if (estudiantes_checked.length > 0 && !jQuery.isEmptyObject(estudiantes_checked)) {
                if (error != true) {
                    var datos = {
                        fecha_otorgamiento_: fecha_otorgamiento,
                        documento_identidad_director_: documento_identidad_director,
                        documento_identidad_jefe_: documento_identidad_jefe,
                        documento_identidad_funcionario_: documento_identidad_funcionario,
                        id: plantel_id,
                        estudiante_select_: JSON.stringify(estudiante_select),
                        cambiar_serial_array_: JSON.stringify(cambiarSerial_array),
                        estatusTitulo_array_: JSON.stringify(estatusTitulo_array),
//                        cambiar_nombres_array_: JSON.stringify(cambiarNombres_array),
//                        cambiar_apellidos_array_: JSON.stringify(cambiarApellidos_array),
//                        cambiar_documento_array_: JSON.stringify(cambiarDocumento_array),
                        anio_egreso_array_: JSON.stringify(anioEgreso_array),
                        tipo_evaluacion_array_: JSON.stringify(tipoEvaluacion_array),
                    };
                    var method = 'POST';
                    var urlDir = '/titulo/seguimientoTitulo/asignarEstudiante/';
                    Loading.show();
                    $.ajax({
                        url: urlDir,
                        data: datos,
                        dataType: 'html',
                        type: method,
                        success: function(resp, resp2, resp3) {

                            try {

                                var json = jQuery.parseJSON(resp3.responseText);
                                if (json.statusCode === "success") {

                                    $("#seleccionEstudiante").addClass('hide');
                                    $("#seleccionEstudiante p").html('');
                                    $("#error").addClass('hide');
                                    $("#error p").html('');
                                    $("#zonaEdu").addClass('hide');
                                    $("#exitoso").removeClass('hide');
                                    $("#exitoso p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                    Loading.hide();
                                }
                                else if (json.statusCode === "error") {

                                    $("#exitoso").addClass('hide');
                                    $("#exitoso p").html('');
                                    $("#seleccionEstudiante").addClass('hide');
                                    $("#seleccionEstudiante p").html('');
                                    $("#error").removeClass('hide');
                                    $("#error p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                    Loading.hide();
                                }

                            } catch (e) {
                                $("#seleccionEstudiante").addClass('hide');
                                $("#seleccionEstudiante p").html('');
                                $("html, body").animate({scrollTop: 0}, "fast");
                                Loading.hide();
                            }
                            Loading.hide();
                        }
                    });
                }
                else {
                    $("#seleccionEstudiante").removeClass('hide');
                    displayDialogBox('seleccionEstudiante', 'error', mensaje);
                    $("html, body").animate({scrollTop: 0}, "fast");
                    Loading.hide();
                }
            }
            else {
                $("#seleccionEstudiante").removeClass('hide');
                displayDialogBox('seleccionEstudiante', 'error', msgasignado);
                $("html, body").animate({scrollTop: 0}, "fast");
                Loading.hide();
            }

        });
    });
    function validarSerial(serial) {
        var x = 0;
        var entro;
//        var mensaje = 'Por favor seleccione otro serial ya que el serial que seleccionó ya se encuentra asignado para otro estudiante';
//        var title = 'Notificación de error';
        var serial_array = new Array();
        $('select[name="cambiarSerial[]"]').each(function() {
            if ($(this).val() != '') {
                serial_array.push($(this).val());
            }
        });
        if (serial_array != '') {
            var tam = serial_array.length;
            if (tam > 1) {
                for (x = 0; x < serial_array.length - 1; x++) {
                    if (serial_array[x] == serial) {
                        entro = true;
                        // $('#cambiarSerial').val('');
                        return entro;
                        //dialogo_errorSaime(mensaje, title);
                    }
                }
            } else {
                return false;
            }
        }
    }


    function validarCedula(tdocumento_identidad, documento_identidad, objeto) {
        var title;
        var mensaje;
        var divResult = '';
        var urlDir = '/titulo/seguimientoTitulo/obtenerDatosPersona';
        var datos;
        var loadingEfect = false;
        var showResult = false;
        var method = 'GET';
        var responseFormat = 'json';
        var tam = 0;
        if (documento_identidad != null && documento_identidad != '')
            tam = documento_identidad.length;
        var beforeSendCallback = function() {
        };
        var successCallback = function() {

        };
        ;
        var errorCallback = function() {
        };
        if (documento_identidad != null && documento_identidad != '') {
            if (tdocumento_identidad != null && tdocumento_identidad != '') {
                if (tdocumento_identidad == 'V' && tam <= '8' && !isNaN(documento_identidad) || tdocumento_identidad == 'E' && tam <= '8' && !isNaN(documento_identidad) || tdocumento_identidad == 'P' && tam <= '15' && (isAlphaNumeric(documento_identidad) == true)) {
                    datos = {
                        documento_identidad: documento_identidad,
                        tdocumento_identidad: tdocumento_identidad
                    }
                    successCallback = function(response) {
                        if (response.statusCode == 'SUCCESS') {
                            var nombreSaime = response.nombres;
                            var apellidoSaime = response.apellidos;
                            title = 'Notificación';
                            mensaje = 'Estimado usuario el documento de identidad pertenecen a ' + apellidoSaime + ' ' + nombreSaime;
                            objeto.val('');
                            dialogo_exitoSaime(mensaje, title);
                            Loading.hide();
                        }
                        if (response.statusCode == 'ERROR') {
                            //   objeto.val('');
                            dialogo_errorSaime(response.mensaje, response.title);
                            Loading.hide();
                        }

                    };
                    Loading.show();
                    executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
                }
                else {
                    if (tdocumento_identidad == 'V' && (!isNaN(documento_identidad) == false) || tdocumento_identidad == 'E' && (!isNaN(documento_identidad) == false)) {
                        title = 'Notificación de Error';
                        mensaje = 'Estimado usuario el documento de identidad debe contener solo números, por favor verifique los datos introducidos.';
                        objeto.val('');
                        dialogo_errorSaime(mensaje, title);
                    }
                    if (tdocumento_identidad == 'V' && tam > '8') {
                        title = 'Notificación de Error';
                        mensaje = 'Estimado usuario el documento de identidad debe contener maximo 8 digitos, ejemplo V-00000000.';
                        objeto.val('');
                        dialogo_errorSaime(mensaje, title);
                    }
                    if (tdocumento_identidad == 'E' && tam > '8') {
                        title = 'Notificación de Error';
                        mensaje = 'Estimado usuario el documento de identidad debe contener maximo 8 digitos, ejemplo E-00000000.';
                        objeto.val('');
                        dialogo_errorSaime(mensaje, title);
                    }
                    if (tdocumento_identidad == 'P' && tam > '15') {
                        title = 'Notificación de Error';
                        mensaje = 'Estimado usuario el documento de identidad debe contener maximo 15 digitos, ejemplo P-000000000000000.';
                        objeto.val('');
                        dialogo_errorSaime(mensaje, title);
                    }
                    if (tdocumento_identidad == 'P' && (isAlphaNumeric(documento_identidad) == false)) {
                        title = 'Notificación de Error';
                        mensaje = 'Estimado usuario por favor verifique los datos que esta introduciendo el pasaporte solo puede contener números y letras.';
                        objeto.val('');
                        dialogo_errorSaime(mensaje, title);
                    }

                }

            }
            else {
                objeto.val('');
                title = 'Notificación de Error';
                mensaje = 'Estimado usuario debe ingresar un Tipo de Documento de Identidad Valido.';
                dialogo_errorSaime(mensaje, title);
            }
        } else {
            objeto.val('');
            title = 'Notificación de Error';
            mensaje = 'Estimado usuario debe ingresar un Documento de Identidad Valido.';
            dialogo_errorSaime(mensaje, title);
        }

    }

    function dialogo_errorSaime(mensaje, title, redireccionar) {
        displayDialogBox('dialog_errorSaime', 'info', mensaje);
        var dialogError = $("#dialog_errorSaime").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                    "class": "btn btn-xs",
                    click: function() {
                        dialogError.dialog("close");
                        if (redireccionar) {
                            window.location.reload();
                        }
                    }
                }
            ]
        });
    }

    function isAlphaNumeric(val)
    {
        if (val.match(/^[a-zA-Z0-9]+$/))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function dialogo_exitoSaime(mensaje, title, redireccionar) {
        displayDialogBox('dialog_exitoSaime', 'info', mensaje);
        var dialogExito = $("#dialog_exitoSaime").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-check-square-o'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                    "class": "btn btn-xs",
                    click: function() {
                        dialogExito.dialog("close");
                        if (redireccionar) {
                            window.location.reload();
                        }
                    }
                }
            ]
        });
    }

</script>


