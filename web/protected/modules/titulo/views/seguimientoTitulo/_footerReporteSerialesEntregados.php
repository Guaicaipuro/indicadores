<?php
$nombres_director = (isset($datosAutoridades[0]['nombre_director']) AND $datosAutoridades[0]['nombre_director'] != '') ? $datosAutoridades[0]['nombre_director'] : '';
$apellidos_director = (isset($datosAutoridades[0]['apellido_director']) AND $datosAutoridades[0]['apellido_director'] != '') ? $datosAutoridades[0]['apellido_director'] : '';
$cedula_director = (isset($datosAutoridades[0]['documento_identidad_director']) AND $datosAutoridades[0]['documento_identidad_director'] != '') ? $datosAutoridades[0]['documento_identidad_director'] : '';
$nombres_zona = (isset($datosAutoridades[1]['nombre_jefe']) AND $datosAutoridades[1]['nombre_jefe'] != '') ? $datosAutoridades[1]['nombre_jefe'] : '';
$apellidos_zona = (isset($datosAutoridades[1]['apellido_jefe']) AND $datosAutoridades[1]['apellido_jefe'] != '') ? $datosAutoridades[1]['apellido_jefe'] : '';
$cedula_zona = (isset($datosAutoridades[1]['documento_identidad_jefe']) AND $datosAutoridades[1]['documento_identidad_jefe'] != '') ? $datosAutoridades[1]['documento_identidad_jefe'] : '';
$nombres_funcionario = (isset($datosAutoridades[2]['nombre_funcionario']) AND $datosAutoridades[2]['nombre_funcionario'] != '') ? $datosAutoridades[2]['nombre_funcionario'] : '';
$apellidos_funcionario = (isset($datosAutoridades[2]['apellido_funcionario']) AND $datosAutoridades[2]['apellido_funcionario'] != '') ? $datosAutoridades[2]['apellido_funcionario'] : '';
$cedula_funcionario = (isset($datosAutoridades[2]['documento_identidad_funcionario']) AND $datosAutoridades[2]['documento_identidad_funcionario'] != '') ? $datosAutoridades[2]['documento_identidad_funcionario'] : '';
?>
<table >
    <tr>
        <td >
            <table width="250px" class="estudiantes_footer" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;border-collapse: collapse;
                   border-spacing: 0px;
                   text-align:left; border: 1px solid grey; ">
                <tr style="border: 1px solid grey;">
                    <th  style="text-align: left;border: 1px solid grey;"  colspan="2" class="center">
                        Fecha de Remisión:
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th width="170px" style=" text-align: left;border: 1px solid grey;"   class="center">
                        Director(a)
                    </th>
                    <th  style=" text-align: center;border: 1px solid grey;"  rowspan="7" class="center">
                        SELLO DEL PLANTEL
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th  style=" text-align: left;border: 1px solid grey;"  class="center">
                        Apellidos y Nombres:
                    </th>
                </tr>
                <tr style="text-align: left;border: 1px solid grey;">
                    <td  style=" border: 1px solid grey;"  class="center">
                        <?php echo $nombres_director . ' ' . $apellidos_director; ?>
                    </td>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th  style="text-align: left; border: 1px solid grey;"  class="center">
                        Cédula de Identidad:
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <td  style="text-align: left; border: 1px solid grey;"  class="center">
                        <?php echo $cedula_director; //str_replace(',', '.', number_format($cedula_director)); ?>
                    </td>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th  style="text-align: left; border: 1px solid grey;"  class="center">
                        FIRMA
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th  style=" border: 1px solid grey;"  class="center">
                        ____________________________
                    </th>
                </tr>
            </table>
        </td>
<!--        <td >-->
<!--            <table width="250px" class="estudiantes_footer" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;border-collapse: collapse;-->
<!--                   border-spacing: 0px;-->
<!--                   text-align:center; border: 1px solid grey; ">-->
<!---->
<!--                <tr>-->
<!---->
<!--                </tr></table>-->
<!--        </td>-->
        <td >
            <table width="250px" class="estudiantes_footer" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;border-collapse: collapse;
                   border-spacing: 0px;
                   text-align:center; border: 1px solid grey; ">
                <tr style="border: 1px solid grey;">
                    <th  style="text-align: left; border: 1px solid grey;"  colspan="2" class="center">
                        Fecha de Recepción:
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th  width="170px" style=" text-align: left;border: 1px solid grey;"   class="center">
                        Control de Estudio del Plantel
                    </th>
                    <th  style=" border: 1px solid grey;"  rowspan="7" class="center">
                        SELLO DE CONTROL DE ESTUDIOS
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th  style=" text-align: left;border: 1px solid grey;"  class="center">
                        Apellidos y Nombres:
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <td style="text-align: left; border: 1px solid grey;"  class="center">
                        <?php echo $nombres_zona . ' ' . $apellidos_zona; ?>
                    </td>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th  style="text-align: left; border: 1px solid grey;"  class="center">
                        Cédula de Identidad:
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <td  style="text-align: left; border: 1px solid grey;"  class="center">
                        <?php echo $cedula_zona; //str_replace(',', '.', number_format($cedula_zona)); ?>
                    </td>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th style=" text-align: left;border: 1px solid grey;"  class="center">
                        FIRMA
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th  style=" border: 1px solid grey;"  class="center">
                        ____________________________
                    </th>
                </tr>
            </table>
        </td>
        <td>
            <table width="250px" class="estudiantes_footer" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;border-collapse: collapse;
                   border-spacing: 0px;
                   text-align:left; border: 1px solid grey; ">
                <tr style="border: 1px solid grey;">
                    <th  style="text-align: left;border: 1px solid grey;"  colspan="2" class="center">
                        Fecha de Remisión:
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th width="170px" style=" text-align: left;border: 1px solid grey;"   class="center">
                        Funcionario Receptor
                    </th>
                    <th  style=" text-align: center;border: 1px solid grey;"  rowspan="7" class="center">
                        SELLO DEL FUNCIONARIO
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th  style=" text-align: left;border: 1px solid grey;"  class="center">
                        Apellidos y Nombres:
                    </th>
                </tr>
                <tr style="text-align: left;border: 1px solid grey;">
                    <td  style=" border: 1px solid grey;"  class="center">
                        <?php echo $nombres_funcionario . ' ' . $apellidos_funcionario; ?>
                    </td>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th  style="text-align: left; border: 1px solid grey;"  class="center">
                        Cédula de Identidad:
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <td  style="text-align: left; border: 1px solid grey;"  class="center">
                        <?php echo $cedula_funcionario; //str_replace(',', '.', number_format($cedula_director)); ?>
                    </td>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th  style="text-align: left; border: 1px solid grey;"  class="center">
                        FIRMA
                    </th>
                </tr>
                <tr style="border: 1px solid grey;">
                    <th  style=" border: 1px solid grey;"  class="center">
                        ____________________________
                    </th>
                </tr>
            </table>
        </td>
    </tr>

</table>