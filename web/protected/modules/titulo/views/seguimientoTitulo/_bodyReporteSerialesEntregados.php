<style type="text/css">

    .estudiantes table, .estudiantes th, .estudiantes td {
        border: 1px solid grey;
    }
    .estudiantes table {
        border-collapse: collapse;
        border-spacing: 0px;
        text-align:center;
    }
</style>

<?php
if (isset($datosEstudiante) AND $datosEstudiante != array()) {
    ?>
    <table class="estudiantes" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-collapse: collapse;
           border-spacing: 0px;
           text-align:center; border: 1px solid grey; ">

        <?php
        foreach ($datosEstudiante as $key => $value) {

            $tdocumento_identidad = isset($value['tdocumento_identidad']) ? $value['tdocumento_identidad'] : '';
            $documento_identidad = isset($value['documento_identidad']) ? $value['documento_identidad'] : '';
            $serial = isset($value['serial']) ? $value['serial'] : '';
            $prefijo = isset($value['prefijo']) ? $value['prefijo'] : '';
            $nombres = isset($value['nombres']) ? $value['nombres'] : '';
            $apellidos = isset($value['apellidos']) ? $value['apellidos'] : '';
            $anio_egreso = isset($value['anio_egreso']) ? $value['anio_egreso'] : '';
            $tipo_evaluacion = isset($value['tipo_evaluacion']) ? $value['tipo_evaluacion'] : '';
            $fecha_otorgamiento = isset($value['fecha_otorgamiento']) ? $value['fecha_otorgamiento'] : '';
            $sexo = isset($value['sexo']) ? $value['sexo'] : '';
            $fecha_nacimiento = (isset($value['fecha_nacimiento'])) ? $value['fecha_nacimiento'] : '';
            $fecha_array = explode('-', $fecha_nacimiento);
            $anio = isset($fecha_array[0]) ? $fecha_array[0] : '';
            $mes = isset($fecha_array[1]) ? $fecha_array[1] : '';
            $dia = isset($fecha_array[2]) ? $fecha_array[2] : '';
            $fechaOtorgamiento = ($value['fecha_otorgamiento'] != '' && $value['fecha_otorgamiento'] != '- - -')? date("d-m-Y", strtotime($fecha_otorgamiento)):'- - -';

            $seccion = (isset($value['seccion'])) ? $value['seccion'] : '';
            $grado = (isset($value['grado'])) ? $value['grado'] : '';
            $estatus_titulo = (isset($value['estatus_titulo'])) ? $value['estatus_titulo'] : "";

//            if ($documento_identidad != '') {
//                if ($tdocumento_identidad == "") {
//                    $documento_identidad = $documento_identidad;
//                } else {
//                    $documento_identidad = $tdocumento_identidad . '-' . $documento_identidad;
//                }
//            } else {
//                $documento_identidad = '';
//            }

            if ($prefijo == '') {
                $serial = $serial;
            } else {
                $serial = $prefijo . ' ' . $serial;
            }

            $sexoFinal = $sexo;

            if ($sexoFinal == '1') {
                $sexoFinal = 'M';
            } else {
                if ($sexoFinal == '0') {
                    $sexoFinal = 'F';
                }
            }
            if ($sexoFinal == '' || $sexoFinal == null) {
                $sexoFinal = '';
            }

//            $plan_estudio = isset($value['nombre_plan']) ? $value['nombre_plan'] : '';
//            $mencion = isset($value['mencion']) ? $value['mencion'] : '';
//
//            $cod_plan = isset($value['codigo_plan']) ? $value['codigo_plan'] : '';
//            $seccion = isset($value['seccion']) ? $value['seccion'] : '';
//            $grado = isset($value['grado']) ? $value['grado'] : '';
//            $plan = $plan_estudio . '[' . $mencion . ']';
//            $grado_seccion = $grado . ' " ' . $seccion . ' " ';
            ?>

            <tr>

                <td width="25px" align="center"  >
                    <?php echo $key + 1; ?>
                </td>
                <td  width="80px"  align="center">
                    <?php echo $serial; ?>
                </td>
                <td  width="40px" align="center">
                    <?php echo $tdocumento_identidad; ?>
                </td>
                <td  width="80px"  align="center">
                    <?php echo $documento_identidad; ?>
                </td>
                <td  width="150px"  align="center">
                    <?php echo $apellidos; ?>
                </td>
                <td  width="150px"  align="center">
                    <?php echo $nombres; ?>
                </td>
                <td  width="30px"  align="center">
                    <?php echo $sexoFinal; ?>
                </td>
                <td width="30px"  align="center">
                    <?php echo $dia; ?>
                </td>
                <td width="30px"  align="center">
                    <?php echo $mes; ?>
                </td>
                <td  width="30px" align="center">
                    <?php echo $anio; ?>
                </td>
                <td width="80px" align="center">
                    <?php echo $tipo_evaluacion; ?>
                </td>
                <td width="50px" align="center">
                    <?php echo $anio_egreso; ?>
                </td>
                <td width="80px" align="center">
                    <?php echo $fechaOtorgamiento; ?>
                </td>
                <td width="50px" align="center">
                    <?php echo $grado; ?>
                </td>
                <td width="50px" align="center">
                    <?php echo $seccion; ?>
                </td>
                <td width="150px" align="center">
                    <?php echo $estatus_titulo; ?>
                </td>

            </tr>

            <?php
        }
        echo "</table>";
    }
    ?>
<!--    <tr>
    <th colspan="6" class="center">
    </th>


    <th  class="center">
    </th>

</tr>-->






    <br>









