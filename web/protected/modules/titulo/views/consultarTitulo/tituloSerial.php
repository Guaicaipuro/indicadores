
<div id="indexConsultarTitulo">
    <?php
    /* @var $this AtencionSolicitudController */

    $this->breadcrumbs = array(
        // 'Asignación de Seriales' => array("../../titulo/atencionSolicitud"),
        'Consultar Titulo'
    );
    ?>

    <div class = "widget-box">

        <div class = "widget-header">
            <h5>Consultar Titulo</h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">
                    <div id="errorConsultaTitulo" class="hide errorDialogBox" ><p></p> </div>
                    <!--                    <div id="busqueda" class="hide alertDialogBox" ><p></p> </div>
                                        <div id="campos_vacios" class="hide alertDialogBox" ><p></p> </div>-->
                    <div id="informacion">
                        <div class="infoDialogBox">
                            <p>
                                Por favor ingrese la cédula de identidad y el tipo de documento de identidad del estudiante para su busqueda.
                            </p>
                        </div>
                        <div id="alertaConsultarTitulo"></div>
                    </div>

                    <div class="row row-fluid" style="padding-bottom: 20px; padding-top: 20px">

                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'busqueda_form',
                            // 'action' => 'atencionSolicitud/mostrarConsultaPlantel'
                        ));
                        ?>
                        <div id="1eraFila" class="col-md-12">

                            <div class="col-md-3" >
                                <?php echo CHtml::label('Tipo de Documento de Identidad <span class="required">*</span>', '', array("class" => "col-md-12", 'id' => 'tipo_nacionalidad')); ?><span ></span>
                                <?php echo CHTML::dropDownList('tipoNacionalidad', 'tipoBusqueda', array('V' => 'Venezolana', 'E' => 'Extrajera', 'P' => 'Pasaporte'), array('empty' => '-SELECCIONE-', 'class' => 'span-7', 'id' => 'tipoNacionalidad', 'text-align'=>'center')); ?>
                            </div>

                            <div class="col-md-2" >
                                <?php echo CHtml::label('Prefijo <span class="required">*</span>', '', array("class" => "col-md-12", 'id' => 'Estuadiante_Egresado')); ?><span ></span>
                                <div id="campoBusqueda" >
                                    <?php echo CHtml::textField('tituloPrefijo', '', array('class' => 'span-7', 'maxlength' => 10, 'style' => 'width: 90%', 'id' => 'tituloPrefijo')); ?>
                                </div>
                            </div>

                            <div class="col-md-3" >
                                <?php echo CHtml::label('Serial <span class="required">*</span>', '', array("class" => "col-md-12", 'id' => 'Estuadiante_Egresado')); ?><span ></span>
                                <div id="campoBusqueda" >
                                    <?php echo CHtml::textField('tituloSerial', '', array('class' => 'span-7', 'maxlength' => 10, 'style' => 'width: 90%', 'id' => 'tituloSerial')); ?>
                                </div>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('&nbsp;&nbsp;', '', array("class" => "col-md-12")); ?>

                                <button  id = "btnConsultar"  class="btn btn-primary btn-sm" type="submit" data-last="Finish">
                                    <i class="fa fa-search icon-on-right"></i>
                                    Consultar
                                </button>
                            </div>

                        </div>

                        <div class = "col-md-12"><div class = "space-6"></div></div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
</div>

<div id="ResultEgresados"  class="hide">

</div>
<hr>
<div class="col-md-6" style="padding-top: 1%">
    <?php
    $getUrl= Yii::app()->request->urlReferrer;
    if (empty($getUrl)){
        $getUrl= Yii::app()->request->getBaseUrl(true);
    }
    ?>
    <a id="btnRegresar" href="<?php echo $getUrl; ?>"class="btn btn-danger">
        <i class="icon-arrow-left"></i>
        Volver
    </a>
</div>

<?php
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->getBaseUrl(true). '/public/js/modules/titulo/consulta/serial.js',CClientScript::POS_END
);
?>
