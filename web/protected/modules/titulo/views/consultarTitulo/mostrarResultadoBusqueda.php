<div id="ResultEgresado"  class="widget-box">
    <!-- AQUI EMPIEZA DATOS GENERAL DEL ESTUDIANTE -->
    <div class="widget-box">

        <div class="widget-header">
            <h5>Datos Generales del Estudiante</h5>

            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-down"></i>
                </a>
            </div>
        </div>

        <div class="widget-body" id="idenEstudiante">
            <div class="widget-body-inner" style="display: block;">
                <div class="widget-main form">

                    <div class="row">


                        <div id="divtdocumento_identidad" class="col-md-4">
                            <?php echo CHtml::label('Tipo de documente de identidad', '', array("class" => "col-md-12")); ?>
                            <?php
                            //                            var_dump($resultadoBusqueda[0]['tdocumento_identidad']);
                            //
                            //
                            //                            die();
                            echo Chtml::textField('', $resultadoBusqueda[0]['tdocumento_identidad'], array('size' => 15, 'maxlength' => 15, 'class' => 'span-7', 'readonly' => 'readonly', 'disabled' => 'disabled'));
                            ?>
                        </div>

                        <div id="divdocumento_identidad" class="col-md-4">
                            <?php echo CHtml::label('Documento Identidad', '', array("class" => "col-md-12")); ?>
                            <?php echo Chtml::textField('', $resultadoBusqueda[0]['documento_identidad'], array('size' => 20, 'maxlength' => 10, 'class' => 'span-7', 'readonly' => 'readonly', 'disabled' => 'disabled')); ?>
                        </div>
                        <div id="divsexo" class="col-md-4">
                            <?php echo CHtml::label('Genero', '', array("class" => "col-md-12")); ?>
                            <?php echo Chtml::textField('', $resultadoBusqueda[0]['sexo'], array('size' => 15, 'maxlength' => 15, 'class' => 'span-7', 'readonly' => 'readonly', 'disabled' => 'disabled')); ?>
                        </div>

                    </div>

                    <div class="row">
                        <div id="divnombres" class="col-md-4">
                            <?php echo CHtml::label('Nombres', '', array("class" => "col-md-12")); ?>
                            <?php echo Chtml::textField('', $resultadoBusqueda[0]['nombres'], array('size' => 50, 'maxlength' => 50, 'class' => 'span-7', 'readonly' => 'readonly', 'disabled' => 'disabled')); ?>
                        </div>

                        <div id="divapellidos" class="col-md-4">
                            <?php echo CHtml::label('Apellidos', '', array("class" => "col-md-12")); ?>
                            <?php echo Chtml::textField('', $resultadoBusqueda[0]['apellidos'], array('size' => 15, 'maxlength' => 15, 'class' => 'span-7', 'readonly' => 'readonly', 'disabled' => 'disabled')); ?>
                        </div>

                        <div id="divcorreo" class="col-md-4">
                            <?php echo CHtml::label('Correo', '', array("class" => "col-md-12")); ?>
                            <?php echo Chtml::textField('', $resultadoBusqueda[0]['correo'], array('size' => 15, 'maxlength' => 15, 'class' => 'span-7', 'readonly' => 'readonly', 'disabled' => 'disabled')); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



</div>
<!-- AQUI TERMINA DATOS GENERAL DEL ESTUDIANTE -->



<!-- AQUI EMPIEZA HISTORIAL ESTUDIANTE -->
<div class="widget-box collapsed">

    <div class="widget-header">
        <h5>Historico del Estudiante</h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-down"></i>
            </a>
        </div>
    </div>

    <div class="widget-body" id="idenEstudiante">
        <div class="widget-body-inner" style="display: block;">
            <div class="widget-main form">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                <center>
                                    <b>C&oacute;digo del Plantel</b>
                                </center>
                            </th>
                            <th>
                                <center>
                                    <b>Nombre del Plantel</b>
                                </center>
                            </th>
                            <th>
                                <center>
                                    <b>Grado &oacute; Nivel</b>
                                </center>
                            </th>
                            <th>
                                <center>
                                    <b>Secci&oacute;n o Lapso</b>
                                </center>
                            </th>
                            <th>
                                <center>
                                    <b>Periodo escolar</b>
                                </center>
                            </th>
                            <th>
                                <center>
                                    <b>Modalidad</b>
                                </center>
                            </th>
                            <th>
                                <center>
                                    <b>
                                        Escolaridad
                                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: -10px; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>RG</th>
                                                <th>RP</th>
                                                <th>MP</th>
                                                <th>DI</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </b>
                                </center>
                            </th>
                            <th>
                                <center>
                                    <b>Estado</b>
                                </center>
                            </th>
                            <th>
                                <center>
                                    <b>Observaci&oacute;n</b>
                                </center>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($historicoEstudiante) {
                            foreach ($historicoEstudiante AS $historico) {
                                ?>
                                <tr class="odd">
                                    <td>
                                        <div>
                                            <?php echo $historico['cod_plantel']; ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <?php echo $historico['nombre_plantel']; ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <?php echo $historico['nombre_grado']; ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <?php echo $historico['nombre_seccion']; ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <?php echo $historico['periodo']; ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <?php echo $historico['nombre_modalidad']; ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row" align="center">
                                            <?php
                                            if (is_numeric($historico['escolaridad'])) {
                                                $rg_check = (int) $historico['escolaridad'][0];
                                                $rp_check = (int) $historico['escolaridad'][1];
                                                $mp_check = (int) $historico['escolaridad'][3];
                                                $di_check = (int) $historico['escolaridad'][5];
                                                ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', (bool) $rg_check, array('disabled' => 'disabled')); ?> </div> <?php
                                                ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', (bool) $rp_check, array('disabled' => 'disabled')); ?> </div> <?php
                                                ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', (bool) $mp_check, array('disabled' => 'disabled')); ?> </div> <?php
                                                ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', (bool) $di_check, array('disabled' => 'disabled')); ?> </div> <?php
                                            } else {
                                                if ($historico['escolaridad'] == 'REGULAR') {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', true, array('disabled' => 'disabled')); ?> </div> <?php
                                                } else {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', false, array('disabled' => 'disabled')); ?> </div> <?php
                                                }
                                                if ($historico['escolaridad'] == 'REPITIENTE') {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', true, array('disabled' => 'disabled')); ?> </div> <?php
                                                } else {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', false, array('disabled' => 'disabled')); ?> </div> <?php
                                                }
                                                if ($historico['escolaridad'] == 'MATERIA PENDIENTE') {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', true, array('disabled' => 'disabled')); ?> </div> <?php
                                                } else {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', false, array('disabled' => 'disabled')); ?> </div> <?php
                                                }
                                                if ($historico['escolaridad'] == 'DOBLE INSCRIPCIÓN') {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', true, array('disabled' => 'disabled')); ?> </div> <?php
                                                } else {
                                                    ?> <div class="col-md-3"> <?php echo CHtml::checkBox('RG', false, array('disabled' => 'disabled')); ?> </div> <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <?php echo $historico['nombre_estado']; ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <?php echo $historico['observacion']; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                            }
                        } else {
                            ?>
                            <div class="infoDialogBox">
                                <p>
                                    <strong>Este estudiante no posee Historial registrado en el sistema.</strong>
                                </p>
                            </div>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- AQUI TERMINA HISTORIAL ESTUDIANTE -->


<!-- AQUI NOTAS ESTUDIANTE -->
<div class = "widget-box collapsed">


    <div class="widget-header">
        <h5>Consulta de notas del estudiante </h5>
        <div class="widget-toolbar">
            <a  href="#" data-action="collapse">
                <i class="icon-chevron-down"></i>
            </a>
        </div>
    </div>

    <div class="widget-body" id="consultaTitulo">
        <div class="widget-body-inner" style="display: none;">
            <div class="widget-main form">

                <?php
                if (!$dataConsultarNota == array()) {
                if (isset($dataConsultarNota[0])) {
                ?>
                <div class="row">
                    <div class="col-md-12" id ="solicitud-titulo">
                        <div id="scrolltable" style='border: 0px;background: #fff; overflow:auto;padding-right: 0px; padding-top: 0px; padding-left: 0px; padding-bottom: 0px;border-right: #DDDDDD 0px solid; border-top: #DDDDDD 0px solid;border-left: #DDDDDD 0px solid; border-bottom: #DDDDDD 0px solid;scrollbar-arrow-color : #999999; scrollbar-face-color : #666666;scrollbar-track-color :#3333333 ;height:400px; left: 28%; top: 300; width: 100%'>

                            <?php
                            //var_dump($dataProvider);
                            //        die();


                            $this->widget(
                                'zii.widgets.grid.CGridView', array(
                                    'id' => 'ConsultaNotas1-grid',
                                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                    'dataProvider' => $dataConsultarNota[0],
                                    'summaryText' => false,
                                    'columns' => array(
                                        //                                        array(
//                                            "name" => "boton",
//                                            "type" => "raw",
//                                            'header' => '<div class="center">' . CHtml::checkBox('selec_todoEst', false, array('id' => 'selec_todoEst', 'title' => 'Seleccionar todos', 'class' => 'tooltipMatricula')) . "</div>"
//                                        ),
//                                        array(
//                                            'name' => 'mencion',
//                                            'type' => 'raw',
//                                            'header' => '<center><b>Mención</b></center>'
//            ),
                                        array(
                                            'name' => 'materia',
                                            'type' => 'raw',
                                            'header' => '<center><b>MATERIAS BÁSICAS 1ER AÑO</b></center>'
                                        ),
                                        array(
                                            'name' => 'nota',
                                            'type' => 'raw',
                                            'header' => '<center><b> Nota </b></center>'
                                        ),
                                    ),
                                    'pager' => array(
                                        'header' => '',
                                        'htmlOptions' => array('class' => 'pagination'),
                                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                    ),
                                )
                            );
                            }
                            echo '<hr>';

                            if (isset($dataConsultarNota[1])) {
                                $this->widget(
                                    'zii.widgets.grid.CGridView', array(
                                        'id' => 'ConsultaNotas2-grid',
                                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                        'dataProvider' => $dataConsultarNota[1],
                                        'summaryText' => false,
                                        'columns' => array(
                                            //                                        array(
//                                            "name" => "boton",
//                                            "type" => "raw",
//                                            'header' => '<div class="center">' . CHtml::checkBox('selec_todoEst', false, array('id' => 'selec_todoEst', 'title' => 'Seleccionar todos', 'class' => 'tooltipMatricula')) . "</div>"
//                                        ),
//            array(
//                'name' => 'mencion',
//                'type' => 'raw',
//                'header' => '<center><b>Mención</b></center>'
//            ),
                                            array(
                                                'name' => 'materia',
                                                'type' => 'raw',
                                                'header' => '<center><b>MATERIAS BÁSICAS 2DO AÑO</b></center>'
                                            ),
                                            array(
                                                'name' => 'nota',
                                                'type' => 'raw',
                                                'header' => '<center><b> Nota </b></center>'
                                            ),
                                        ),
                                        'pager' => array(
                                            'header' => '',
                                            'htmlOptions' => array('class' => 'pagination'),
                                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                        ),
                                    )
                                );
                            }
                            echo '<hr>';


                            if (isset($dataConsultarNota[2])) {

                                $this->widget(
                                    'zii.widgets.grid.CGridView', array(
                                        'id' => 'ConsultaNotas3-grid',
                                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                        'dataProvider' => $dataConsultarNota[2],
                                        'summaryText' => false,
                                        'columns' => array(
                                            //                                        array(
//                                            "name" => "boton",
//                                            "type" => "raw",
//                                            'header' => '<div class="center">' . CHtml::checkBox('selec_todoEst', false, array('id' => 'selec_todoEst', 'title' => 'Seleccionar todos', 'class' => 'tooltipMatricula')) . "</div>"
//                                        ),
//            array(
//                'name' => 'mencion',
//                'type' => 'raw',
//                'header' => '<center><b>Mención</b></center>'
//            ),
                                            array(
                                                'name' => 'materia',
                                                'type' => 'raw',
                                                'header' => '<center><b>MATERIAS BÁSICAS 3ER AÑO</b></center>'
                                            ),
                                            array(
                                                'name' => 'nota',
                                                'type' => 'raw',
                                                'header' => '<center><b> Nota </b></center>'
                                            ),
                                        ),
                                        'pager' => array(
                                            'header' => '',
                                            'htmlOptions' => array('class' => 'pagination'),
                                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                        ),
                                    )
                                );
                                echo '<hr>';
                            }
                            if (isset($dataConsultarNota[3])) {
                                $this->widget(
                                    'zii.widgets.grid.CGridView', array(
                                        'id' => 'ConsultaNotas4-grid',
                                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                        'dataProvider' => $dataConsultarNota[3],
                                        'summaryText' => false,
                                        'columns' => array(
                                            //                                        array(
//                                            "name" => "boton",
//                                            "type" => "raw",
//                                            'header' => '<div class="center">' . CHtml::checkBox('selec_todoEst', false, array('id' => 'selec_todoEst', 'title' => 'Seleccionar todos', 'class' => 'tooltipMatricula')) . "</div>"
//                                        ),
//            array(
//                'name' => 'mencion',
//                'type' => 'raw',
//                'header' => '<center><b>Mención</b></center>'
//            ),
                                            array(
                                                'name' => 'materia',
                                                'type' => 'raw',
                                                'header' => '<center><b>PROCESAMIENTOS DE DATOS</b></center>'
                                            ),
                                            array(
                                                'name' => 'nota',
                                                'type' => 'raw',
                                                'header' => '<center><b> Nota </b></center>'
                                            ),
                                        ),
                                        'pager' => array(
                                            'header' => '',
                                            'htmlOptions' => array('class' => 'pagination'),
                                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                        ),
                                    )
                                );
                            }
                            } else {
                                ?>
                                <div class="infoDialogBox">
                                    <p>
                                        <strong>Este estudiante no posee nota registrada en el sistema.</strong>
                                    </p>
                                </div>
                                <!--                        <div class="panel-body">
                                                            <p style="text-align: justify;font-size:15px;font-family: 'Helvetica';">

                                    </p>
                                                    </div>-->
                            <?php
                            }
                            ?>
                        </div>

                        <br>
                        <br>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<!-- AQUI NOTAS ESTUDIANTE -->


<!-- AQUI EMPIEZA CONSULTA TITULO -->
<div class = "widget-box collapsed">


    <div class="widget-header">
        <h5>Consulta de t&iacute;tulo </h5>
        <div class="widget-toolbar">
            <a  href="#" data-action="collapse">
                <i class="icon-chevron-down"></i>
            </a>
        </div>
    </div>

    <div id="consultaTitulo2" class="widget-body" >
        <div class="widget-body-inner" >
            <div class="widget-main form">

                <div class="row">
                    <div class="col-md-12" id ="solicitud-titulo2">
                        <?php
                        //                            var_dump($dataProvider);
                        //                                    die();
                        //var_dump($dataProvider);
                        //die();
                        if ($dataProvider) {

                            $this->widget(
                                'zii.widgets.grid.CGridView', array(
                                    'id' => 'solicitudTitulo-grid',
                                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                    'dataProvider' => $dataProvider,
                                    'summaryText' => false,
                                    'columns' => array(
                                        //                                        array(
//                                            "name" => "boton",
//                                            "type" => "raw",
//                                            'header' => '<div class="center">' . CHtml::checkBox('selec_todoEst', false, array('id' => 'selec_todoEst', 'title' => 'Seleccionar todos', 'class' => 'tooltipMatricula')) . "</div>"
//                                        ),
                                        array(
                                            'name' => 'cedula_identidad',
                                            'type' => 'raw',
                                            'header' => '<center><b>Cédula Identidad</b></center>'
                                        ),
                                        array(
                                            'name' => 'nombreApellido',
                                            'type' => 'raw',
                                            'header' => '<center><b>Nombres y Apellidos</b></center>'
                                        ),
                                        array(
                                            'name' => 'serial',
                                            'type' => 'raw',
                                            'header' => '<center><b> Serial Asignado </b></center>'
                                        ),
                                        array(
                                            'name' => 'plantelCode_estadi',
                                            'type' => 'raw',
                                            'header' => '<center><b> Plantel y Código Estadístico </b></center>'
                                        ),
                                        array(
                                            'name' => 'plan_nombre',
                                            'type' => 'raw',
                                            'header' => '<center><b> Nombre del Plan</b></center>'
                                        ),
                                        array(
                                            'name' => 'nombre_mencion',
                                            'type' => 'raw',
                                            'header' => '<center><b>Mención</b></center>'
                                        ),
                                        array(
                                            'name' => 'anoEgreso',
                                            'type' => 'raw',
                                            'header' => '<center><b>Año de Egreso</b></center>'
                                        ),
                                        array(
                                            'name' => 'fechaOtorgacion',
                                            'type' => 'raw',
                                            'header' => '<center><b>Fecha de Expedición</b></center>'
                                        ),
                                    ),
                                    'pager' => array(
                                        'header' => '',
                                        'htmlOptions' => array('class' => 'pagination'),
                                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                    ),
                                )
                            );
                        }

                        ?>

                    </div>
                    <?php
                    if($dataProvider != array()):?>
                        <div class="space-6"><div class="row"></div></div>
                        <div id="" class="grid-view">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th colspan="4"><center><b>AUTORIDADES FIRMANTES DEL DOCUMENTO</b></center></th>
                                </tr>
                                <tr>
                                    <th ><center><b>Cargo</b></center></td>
                                    <th ><center><b>Cédula</b></center></td>
                                    <th ><center><b>Nombres</b></center></td>
                                    <th ><center><b>Apellidos</b></center></td>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                if($sinacoes){
                                    if(count($firmantes)>0){
                                        foreach($firmantes as $key => $item){
                                            echo "<tr>";
                                            echo "<td ><center>".$item['ccargo']."</center></td>";
                                            echo "<td ><center>".$item['ccedula']."</center></td>";
                                            echo "<td ><center>".$item['dnombres']."</center></td>";
                                            echo "<td ><center>".$item['dapellidos']."</center></td>";
                                            echo "</tr>";
                                        }
                                    }
                                    else {
                                        echo "<tr>";
                                        echo "<td colspan='4'>No se encontraron resultado</td>";
                                        echo "<tr>";
                                    }
                                }
                                else {
                                    if(array_key_exists('origen_dir_plantel',$resultadoBusqueda[0]) AND array_key_exists('cedula_dir_plantel',$resultadoBusqueda[0]) AND array_key_exists('nombre_dir_plantel',$resultadoBusqueda[0])){
                                        echo "<tr>";
                                        echo "<td ><center>DIRECTOR</center></td>";
                                        if(!is_null($resultadoBusqueda[0]['origen_dir_plantel']) OR !is_null($resultadoBusqueda[0]['cedula_dir_plantel']) OR !is_null($resultadoBusqueda[0]['nombre_dir_plantel'])){
                                            echo "<td ><center>".((!is_null($resultadoBusqueda[0]['origen_dir_plantel']))?$resultadoBusqueda[0]['origen_dir_plantel']:'V').'-'.$resultadoBusqueda[0]['cedula_dir_plantel']."</center></td>";
                                            echo "<td colspan='2'><center>".strtoupper($resultadoBusqueda[0]['nombre_dir_plantel'])."</center></td>";
                                        }
                                        else {
                                            echo "<td colspan='3'><center>NO ASIGNADO</center></td>";
                                        }
                                        echo "</tr>";
                                    }
                                    if(array_key_exists('origen_drcee_zona',$resultadoBusqueda[0]) AND array_key_exists('cedula_drcee_zona',$resultadoBusqueda[0]) AND array_key_exists('nombre_apellido_drcee_zona',$resultadoBusqueda[0])){
                                        echo "<tr>";
                                        echo "<td ><center>JEFE DE REGISTRO Y CONTROL DE ESTUDIO Y EVALUACIÓN DE ZONA</center></td>";
                                        if(!is_null($resultadoBusqueda[0]['origen_drcee_zona']) OR !is_null($resultadoBusqueda[0]['cedula_drcee_zona']) OR !is_null($resultadoBusqueda[0]['nombre_apellido_drcee_zona'])) {
                                            echo "<td ><center>" . ((!is_null($resultadoBusqueda[0]['origen_drcee_zona']))?$resultadoBusqueda[0]['origen_drcee_zona']:'V'). '-' . $resultadoBusqueda[0]['cedula_drcee_zona'] . "</center></td>";
                                            echo "<td colspan='2'><center>" . strtoupper($resultadoBusqueda[0]['nombre_apellido_drcee_zona']) . "</center></td>";
                                        }
                                        else {
                                            echo "<td colspan='3'><center>NO ASIGNADO</center></td>";
                                        }
                                        echo "</tr>";
                                    }
                                    if(array_key_exists('origen_funcio_desig',$resultadoBusqueda[0]) AND array_key_exists('cedula_funcio_desig',$resultadoBusqueda[0]) AND array_key_exists('nombre_funcio_desig',$resultadoBusqueda[0])){
                                        echo "<tr>";
                                        echo "<td ><center>FUNCIONARIO DESIGNADO MPPE</center></td>";
                                        if(!is_null($resultadoBusqueda[0]['origen_funcio_desig']) OR !is_null($resultadoBusqueda[0]['cedula_funcio_desig']) OR !is_null($resultadoBusqueda[0]['nombre_funcio_desig'])) {
                                            echo "<td ><center>".((!is_null($resultadoBusqueda[0]['origen_funcio_desig']))?$resultadoBusqueda[0]['origen_funcio_desig']:'V').'-'.$resultadoBusqueda[0]['cedula_funcio_desig']."</center></td>";
                                            echo "<td colspan='2'><center>".strtoupper($resultadoBusqueda[0]['nombre_funcio_desig'])."</center></td>";
                                        }
                                        else {
                                            echo "<td colspan='3'><center>NO ASIGNADO</center></td>";
                                        }
                                        echo "</tr>";
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    <?php endif;
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- AQUI TERMINA CONSULTA TITULO -->