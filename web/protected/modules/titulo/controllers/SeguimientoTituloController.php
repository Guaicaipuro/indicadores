<?php

class SeguimientoTituloController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Seguimiento titulo',
        'write' => 'Seguimiento titulo',
        'admin' => 'Seguimiento titulo',
        'label' => 'Seguimiento titulo'
    );
    public $contar = 0;

    const MODULO = "Titulo.SeguimientoTitulo";

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
//'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('indexDrcee', 'indexZonaEdu', 'indexPlantel', 'ObtenerDatosPersona', 'reporteSerialesEntregados'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('asignarZonaEducativa', 'asignarPlantel', 'asignarEstudiante'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /*      FUNCIONALIDAD DE CUANDO LOS JEFE_DRCEE VERIFICAN QUE ENTREGARON A LAS ZONAS EDUCATIVAS LOS SERIALES CORRESPONDIENTES */

    public function actionIndexDrcee()
    {

        $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_actual_id = $periodo_escolar_actual_id['id'];
        $datosZonaEdu = Titulo::model()->datosPorZonaEdu($periodo_actual_id, $mostrar = 0);
        $estatus_ubicacion_titulo = 1;
        $cant_antes = 0;
        $cant_despues = 0;
        //       $verificarZonaEdu = Titulo::model()->verificarExistenciaZonaEdu($periodo_actual_id);
        $verificarControlZonaEdu = Titulo::model()->verificarControlSeguimiento($estatus_ubicacion_titulo, $periodo_actual_id);
//
        //  var_dump($datosZonaEdu);
        //  die();
        $mostrarZonas = true;
//        var_dump($verificarControlZonaEdu);
//        die();
        if ($verificarControlZonaEdu == false || $datosZonaEdu != false) {//No se ha realizado ningun proceso de validacion de lotes de seriales a las zonas educativas
            if ($datosZonaEdu != false) {

//Esto lo agregue nuevo.
                $datosZonaEduc = Titulo::model()->datosPorZonaEdu($periodo_actual_id, $mostrar = 1);
                if ($datosZonaEduc != false) {
                    $mostrarEntregadoZona = $this->dataProviderZonaEdu($datosZonaEduc);
                    foreach ($datosZonaEduc as $key => $datos) {
                        $cant_despues = $cant_despues + $datos['cant_seriales_asignado'];
                    }
                } else {
                    $mostrarEntregadoZona = array();
                }
//Fin

                foreach ($datosZonaEdu as $key => $datos) {
                    $cant_antes = $cant_antes + $datos['cant_seriales_asignado'];
                }
                $resultadoDrcee = $this->dataProviderZonaEdu($datosZonaEdu);
                $verificarControlZonaEdu = 0;

                $this->render('indexDrcee', array(
                    'resultadoDrcee' => $resultadoDrcee,
                    'mostrarZonas' => $mostrarZonas,
                    'verificarControlZonaEdu' => $verificarControlZonaEdu,
                    'mostrarEntregadoZona' => $mostrarEntregadoZona,
                    'cant_antes' => $cant_antes,
                    'cant_despues' => $cant_despues
                ));
            } else {
                $resultadoDrcee = array();
                $mostrarZonas = false;
                $verificarControlZonaEdu = 0;
                $mostrarEntregadoZona = array();
                $this->render('indexDrcee', array(
                    'resultadoDrcee' => $resultadoDrcee,
                    'mostrarZonas' => $mostrarZonas,
                    'verificarControlZonaEdu' => $verificarControlZonaEdu,
                    'mostrarEntregadoZona' => $mostrarEntregadoZona,
                    'cant_antes' => $cant_antes,
                    'cant_despues' => $cant_despues
                ));
            }
        } else {
            $resultadoDrcee = array();
            $datosZonaEdu = Titulo::model()->datosPorZonaEdu($periodo_actual_id, $mostrar = 1);
           // var_dump($datosZonaEdu);die();
            if ($datosZonaEdu != false){
                $mostrarEntregadoZona = $this->dataProviderZonaEdu($datosZonaEdu);
                foreach ($datosZonaEdu as $key => $datos){
                    $cant_despues = $cant_despues + $datos['cant_seriales_asignado'];
                }
            }
            else{
                $mostrarEntregadoZona = array();
            }
            $this->render('indexDrcee', array(
                'resultadoDrcee' => $resultadoDrcee,
                'mostrarZonas' => $mostrarZonas,
                'verificarControlZonaEdu' => $verificarControlZonaEdu,
                'mostrarEntregadoZona' => $mostrarEntregadoZona,
                'cant_antes'=>$cant_antes,
                'cant_despues'=>$cant_despues
            ));
        }
    }

    public function actionAsignarZonaEducativa() {

        if (Yii::app()->request->isAjaxRequest) {

            $zona_educativa_select = $_POST['zona_educativa_select'];

            foreach ($zona_educativa_select as $key => $value) {

                $zona_educativa_select[$key] = base64_decode($value);
            }
            $zona_educativa_pg_array = Utiles::toPgArray($zona_educativa_select);

            $ruta = yii::app()->basePath;
            $ruta = $ruta . '/yiic';
            $username = Yii::app()->user->name;
            $nombre = Yii::app()->user->nombre;
            $apellido = Yii::app()->user->apellido;
            $cedula = Yii::app()->user->cedula;
            $usuario_id = Yii::app()->user->id;
            $grupo_id = Yii::app()->user->group;
            $ip = Yii::app()->request->userHostAddress;

            $estatus_ubicacion_titulo = 1;
            $verificarProcesosCorriendo = Titulo::model()->verificarProcesosCorriendoSeguimiento($estatus_ubicacion_titulo);

            if ($verificarProcesosCorriendo == 0) {

//                echo "nohup /usr/bin/php $ruta SeguimientoTitulo AsignarZonaEducativa --zona_educativa_id=$zona_educativa_pg_array --username=$username --nombre=$nombre --apellido=$apellido --cedula=$cedula --usuario_id=$usuario_id --grupo_id=$grupo_id --ip=$ip 1>/dev/null & echo $!";
//                die(); //soporte_gescolar@me.gob.ve
                $resultado_comando = shell_exec("nohup /usr/bin/php $ruta SeguimientoTitulo AsignarZonaEducativa --zona_educativa_id=$zona_educativa_pg_array --username=$username --nombre=$nombre --apellido=$apellido --cedula=$cedula --usuario_id=$usuario_id --grupo_id=$grupo_id --ip=$ip 1>/dev/null & echo $!");
                $mensajeExitoso = "Estimado Usuario, el proceso de entrega de los lotes de papel moneda a las zonas educativas tardara un tiempo para realizarse. Cuando culmine el proceso se le notificara mediante un correo.";
                $respuesta['statusCode'] = "success";
                $respuesta['mensaje'] = $mensajeExitoso;
                echo json_encode($respuesta);
                //  Utiles::enviarCorreo('mari.lac.mor@gmail.com', 'Notificación de la validación de entrega de seriales a las zonas educativas', $resultado_comando);
                $respuesta['salida'] = $resultado_comando;
                //   header('Cache-Control: no-cache, must-revalidate');
                // header('Content-type: application/json');
                yii::app()->end();
            } else {
                $mensajeError = "Estimado Usuario, ya se esta ejecutando un proceso de entrega de los lotes de papel moneda a las zonas educativas, por favor espere hasta que reciba un correo con la notificación de la culminación del proceso.";
                $respuesta['statusCode'] = 'error';
                $respuesta['mensaje'] = $mensajeError;
                echo json_encode($respuesta);
            }
        } else {

            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function dataProviderZonaEdu($datosZonaEdu) {


        foreach ($datosZonaEdu as $key => $value) {
            $zona_educativa = $value['zona_educativa'];
            $cant_seriales_asignado = $value['cant_seriales_asignado'];
            $keys = $key + 1;

            $botones = "<div class='center'>" . CHtml::checkBox('OtorgarZonaEdu[]', "false", array('value' => base64_encode($value['id_zona_educativa']),
                        "title" => "Entregado")
                    ) .
                    "</div>";

            $rawData[] = array(
                'id' => $key,
                'zona_educativa' => '<center>' . $zona_educativa . '</center>',
                'cant_seriales_asignado' => '<center>' . $cant_seriales_asignado . '</center>',
                'key' => '<center>' . $keys . '</center>',
                'botones' => '<center>' . $botones . '</center>'
            );
        }

        return new CArrayDataProvider($rawData, array(
            'pagination' => false
                )
        );
    }

    /*      FIN      */


    /*      FUNCIONALIDAD DE CUANDO LOS JEFE_ZONA VERIFICAN QUE ENTREGARON A LOS PLANTELES LOS SERIALES CORRESPONDIENTES */

    public function actionIndexZonaEdu() {

        $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_actual_id = $periodo_escolar_actual_id['id'];
        $usuario_id = Yii::app()->user->id;
        $mostrar = null;
        $cant_antes = 0;
        $cant_despues = 0;
        $zona_educativa_id = Titulo::model()->obtenerIdZonaEdu($usuario_id);

        if ($zona_educativa_id != false)
            $datosPlantel = Titulo::model()->datosPorPlantel($periodo_actual_id, $zona_educativa_id, $mostrar = 0);
        else
            $datosPlantel = array();
        $estatus_ubicacion_titulo = 2; // El estatus 2 es Asignado a Plantel.
        $verificarControlPlantel = Titulo::model()->verificarControlSeguimiento($estatus_ubicacion_titulo, $periodo_actual_id);
//        var_dump($zona_educativa_id);
//        var_dump($datosPlantel);
//        die();
        $mostrarPlantel = true;
//        var_dump($verificarZonaEdu);
//        die();
        if ($verificarControlPlantel == false || $datosPlantel != false) {//No se ha realizado ningun proceso de validacion de lotes de seriales a las zonas educativas

            if ($datosPlantel != false) {

//Agregue esto.
                $datosPlantelVerifico = Titulo::model()->datosPorPlantel($periodo_actual_id, $zona_educativa_id, $mostrar = 1);
                if ($datosPlantelVerifico != false) {
                    $mostrarEntregadoPlantel = $this->dataProviderPlantel($datosPlantelVerifico);
                    foreach ($datosPlantelVerifico as $key => $datos){
                        $cant_despues = $cant_despues + $datos['cant_seriales_asignado_por_plantel'];
                    }
                } else{
                    $mostrarEntregadoPlantel = array();
                }
//Fin
                foreach ($datosPlantel as $datos){
                    $cant_antes = $cant_antes + $datos['cant_seriales_asignado_por_plantel'];
                }
                $resultadoPlantel = $this->dataProviderPlantel($datosPlantel);
                $verificarControlPlantel = 0;
                $this->render('indexZonaEdu', array(
                    'resultadoPlantel' => $resultadoPlantel,
                    'mostrarPlantel' => $mostrarPlantel,
                    'verificarControlPlantel' => $verificarControlPlantel,
                    'mostrarEntregadoPlantel' => $mostrarEntregadoPlantel,
                    'cant_antes'=>$cant_antes,
                    'cant_despues'=>$cant_despues
                ));
            } else {
                $resultadoPlantel = array();
                $mostrarPlantel = false;
                $verificarControlPlantel = 0;
                $mostrarEntregadoPlantel = array();
                $this->render('indexZonaEdu', array(
                    'resultadoPlantel' => $resultadoPlantel,
                    'mostrarPlantel' => $mostrarPlantel,
                    'verificarControlPlantel' => $verificarControlPlantel,
                    'mostrarEntregadoPlantel' => $mostrarEntregadoPlantel,
                    'cant_antes'=>$cant_antes,
                    'cant_despues'=>$cant_despues
                ));
            }
        } else {
            $resultadoPlantel = array();
            $datosPlantel = Titulo::model()->datosPorPlantel($periodo_actual_id, $zona_educativa_id, $mostrar = 1);
//            var_dump($datosPlantel);
//            die();
            if ($datosPlantel != false) {
                $mostrarEntregadoPlantel = $this->dataProviderPlantel($datosPlantel);
                foreach ($datosPlantel as $key => $datos){
                    $cant_despues = $cant_despues + $datos['cant_seriales_asignado_por_plantel'];
                }

            }else {
                $mostrarEntregadoPlantel = array();
            }

            $this->render('indexZonaEdu', array(
                'resultadoPlantel' => $resultadoPlantel,
                'mostrarPlantel' => $mostrarPlantel,
                'verificarControlPlantel' => $verificarControlPlantel,
                'mostrarEntregadoPlantel' => $mostrarEntregadoPlantel,
                'cant_antes'=>$cant_antes,
                'cant_despues'=>$cant_despues
            ));
        }
    }

    public function dataProviderPlantel($datosPlantel) {


        foreach ($datosPlantel as $key => $value) {
            $plantel = $value['plantel'];
            $cant_seriales_asignado_por_plantel = $value['cant_seriales_asignado_por_plantel'];
            $keys = $key + 1;

            $botones = "<div class='center'>" . CHtml::checkBox('OtorgarPlantel[]', "false", array('value' => base64_encode($value['id_plantel']),
                        "title" => "Entregado")
                    ) .
                    "</div>";

            $rawData[] = array(
                'id' => $key,
                'plantel' => '<center>' . $plantel . '</center>',
                'cant_seriales_asignado_por_plantel' => '<center>' . $cant_seriales_asignado_por_plantel . '</center>',
                'key' => '<center>' . $keys . '</center>',
                'botones' => '<center>' . $botones . '</center>'
            );
        }


        return new CArrayDataProvider($rawData, array(
            'pagination' => false
                )
        );
    }

    public function actionAsignarPlantel() {

        if (Yii::app()->request->isAjaxRequest) {

            $plantel_select = $_POST['plantel_select'];

            foreach ($plantel_select as $key => $value) {

                $plantel_select[$key] = base64_decode($value);
            }
            $plantel_pg_array = Utiles::toPgArray($plantel_select);

            $ruta = yii::app()->basePath;
            $ruta = $ruta . '/yiic';
            $username = Yii::app()->user->name;
            $nombre = Yii::app()->user->nombre;
            $apellido = Yii::app()->user->apellido;
            $cedula = Yii::app()->user->cedula;
            $usuario_id = Yii::app()->user->id;
            $grupo_id = Yii::app()->user->group;
            $ip = Yii::app()->request->userHostAddress;

            $estatus_ubicacion_titulo = 2;
            $verificarProcesosCorriendo = Titulo::model()->verificarProcesosCorriendoSeguimiento($estatus_ubicacion_titulo);

            if ($verificarProcesosCorriendo == 0) {
//                echo "nohup /usr/bin/php $ruta SeguimientoTitulo AsignarPlantel --plantel_id=$plantel_pg_array --username=$username --nombre=$nombre --apellido=$apellido --cedula=$cedula --usuario_id=$usuario_id --grupo_id=$grupo_id --ip=$ip 1>/dev/null & echo $!";
//                die();
                $resultado_comando = shell_exec("nohup /usr/bin/php $ruta SeguimientoTitulo AsignarPlantel --plantel_id=$plantel_pg_array --username=$username --nombre=$nombre --apellido=$apellido --cedula=$cedula --usuario_id=$usuario_id --grupo_id=$grupo_id --ip=$ip 1>/dev/null & echo $!");
                $mensajeExitoso = "Estimado Usuario, el proceso de entrega de los lotes de papel moneda a los planteles tardara un tiempo para realizarse. Cuando culmine el proceso se le notificara mediante un correo.";
                $respuesta['statusCode'] = "success";
                $respuesta['mensaje'] = $mensajeExitoso;
                echo json_encode($respuesta);
                // Utiles::enviarCorreo('mari.lac.mor@gmail.com', 'Notificación de la validación de entrega de seriales a los planteles', $resultado_comando);
                $respuesta['salida'] = $resultado_comando;
                //   header('Cache-Control: no-cache, must-revalidate');
                // header('Content-type: application/json');

                yii::app()->end();
            } else {
                $mensajeError = "Estimado Usuario, ya se esta ejecutando un proceso de entrega de los lotes de papel moneda a los planteles, por favor espere hasta que reciba un correo con la notificación de la culminación del proceso.";
                $respuesta['statusCode'] = 'error';
                $respuesta['mensaje'] = $mensajeError;
                echo json_encode($respuesta);
            }
        } else {

            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    /*      FIN      */


    /*      FUNCIONALIDAD DE CUANDO LOS DIRECTORES VERIFICAN QUE ENTREGARON A LOS ESTUDIANTES LOS SERIALES CORRESPONDIENTES */

    public function actionIndexPlantel($id) {

        $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_actual_id = $periodo_escolar_actual_id['id'];
        //   $usuario_id = Yii::app()->user->id;
        $mostrar = null;

        $plantel_id = base64_decode($id);

        if (!is_numeric($plantel_id)) {
            //  echo "es letra";
            throw new CHttpException(404, 'No se ha encontrado el plantel que ha solicitado. Recargue la página e intentelo de nuevo.');
        } else {
            $datosDirector = Titulo::model()->ObtenerDatosDirector($plantel_id, $periodo_actual_id);
            $datosJefeControlEst = Titulo::model()->ObtenerDatosJefeControlEst($plantel_id, $periodo_actual_id);
            $datosFuncionario = Titulo::model()->ObtenerDatosFuncionario($plantel_id, $periodo_actual_id);
            $datosFirmante = array_merge($datosDirector, $datosJefeControlEst, $datosFuncionario);
            $nombre_plantel = Plantel::model()->findByPk($plantel_id);
            $nombrePlantel = ($nombre_plantel != null) ? '"' . $nombre_plantel->nombre . '"' : "";
            if ($plantel_id != '' || $plantel_id != null) {
                $datosEstudiante = Titulo::model()->datosPorEstudiante($periodo_actual_id, $plantel_id, $mostrar = 0);
                //  var_dump($datosEstudiante);
                // die();
            } else
                $datosEstudiante = array();
//            var_dump($datosEstudiante);
//            die();
            $estatus_ubicacion_titulo = 3; // El estatus 2 es Asignado a Estudiante.
            $verificarControlEstudiante = Titulo::model()->verificarControlSeguimiento($estatus_ubicacion_titulo, $periodo_actual_id);
//        var_dump($zona_educativa_id);
//            var_dump($datosEstudiante);
//            die();
            $mostrarEstudiante = true;
            //  var_dump($datosEstudiante);
            //     die();
            if ($verificarControlEstudiante == false || $datosEstudiante != false) {//No se ha realizado ningun proceso de validacion de lotes de seriales a las zonas educativas
                if ($datosEstudiante != false) {
                    $resultadoEstudiante = $this->dataProviderEstudiante($datosEstudiante, $periodo_actual_id);
                    $verificarControlEstudiante = 0;

                    $datosEstudianteVerifico = Titulo::model()->datosPorEstudiante($periodo_actual_id, $plantel_id, $mostrar = 1);

                    if ($datosEstudianteVerifico != false)
                        $mostrarEntregadoEstudiante = $this->dataProviderEstudiante($datosEstudianteVerifico, $periodo_actual_id);
                    else
                        $mostrarEntregadoEstudiante = array();

                    $this->render('indexPlantel', array(
                        'resultadoEstudiante' => $resultadoEstudiante,
                        'mostrarEstudiante' => $mostrarEstudiante,
                        'verificarControlEstudiante' => $verificarControlEstudiante,
                        'mostrarEntregadoEstudiante' => $mostrarEntregadoEstudiante,
                        'plantel_id' => $plantel_id,
                        'nombrePlantel' => $nombrePlantel,
                        'datosFirmante' => $datosFirmante
                    ));
                } else {
                    $resultadoEstudiante = array();
                    $mostrarEstudiante = false;
                    $verificarControlEstudiante = 0;
                    $mostrarEntregadoEstudiante = array();
                    $this->render('indexPlantel', array(
                        'resultadoEstudiante' => $resultadoEstudiante,
                        'mostrarEstudiante' => $mostrarEstudiante,
                        'verificarControlEstudiante' => $verificarControlEstudiante,
                        'mostrarEntregadoEstudiante' => $mostrarEntregadoEstudiante,
                        'plantel_id' => $plantel_id,
                        'nombrePlantel' => $nombrePlantel,
                        'datosFirmante' => $datosFirmante
                    ));
                }
            } else {
                $resultadoEstudiante = array();
                $datosEstudiante = Titulo::model()->datosPorEstudiante($periodo_actual_id, $plantel_id, $mostrar = 1);

                if ($datosEstudiante != false) {
                    $mostrarEntregadoEstudiante = $this->dataProviderEstudiante($datosEstudiante, $periodo_actual_id);
                    $this->render('indexPlantel', array(
                        'resultadoEstudiante' => $resultadoEstudiante,
                        'mostrarEstudiante' => $mostrarEstudiante,
                        'verificarControlEstudiante' => $verificarControlEstudiante,
                        'mostrarEntregadoEstudiante' => $mostrarEntregadoEstudiante,
                        'plantel_id' => $plantel_id,
                        'nombrePlantel' => $nombrePlantel,
                        'datosFirmante' => $datosFirmante
                    ));
                } else {
                    $resultadoEstudiante = array();
                    $mostrarEstudiante = false;
                    $verificarControlEstudiante = 0;
                    $mostrarEntregadoEstudiante = array();
                    $this->render('indexPlantel', array(
                        'resultadoEstudiante' => $resultadoEstudiante,
                        'mostrarEstudiante' => $mostrarEstudiante,
                        'verificarControlEstudiante' => $verificarControlEstudiante,
                        'mostrarEntregadoEstudiante' => $mostrarEntregadoEstudiante,
                        'plantel_id' => $plantel_id,
                        'nombrePlantel' => $nombrePlantel,
                        'datosFirmante' => $datosFirmante
                    ));
                }
            }
        }
    }

    public function dataProviderEstudiante($datosEstudiante, $periodo_actual_id) {

        foreach ($datosEstudiante as $key => $value) {

            $tdocumento_identidad = $value['tdocumento_identidad'];
            $id_estudiante = $value['id_estudiante'];
            $documento_identidad = $value['documento_identidad'];
            $nombres = $value['nombres'];
            $apellidos = $value['apellidos'];
            $serial = $value['serial'];
            $serial_id = $value['serial_id'];
            $keys = $key + 1;
            $i = $key;
            $anio_egreso = (isset($value['anio_egreso']) && $value['anio_egreso'] != NULL)? $value['anio_egreso'] : $this->columnaAnioEgreso($i);
            $tipo_evaluacion = (isset($value['tipo_evaluacion']) && $value['tipo_evaluacion'] != NULL)? $value['tipo_evaluacion'] : $this->columnaTipoEvaluacion($i);
            $fecha_otorgamiento = isset($value['fecha_otorgamiento']) ? ($value['fecha_otorgamiento'] != '' && $value['fecha_otorgamiento'] != '- - -')? date("d-m-Y", strtotime($value['fecha_otorgamiento'])):'- - -' : "";
            $estatusTitulo = (isset($value['estatus_titulo']) && $value['estatus_titulo'] != '') ? $value['estatus_titulo'] : "";
            $asignadoEstudiante = isset($value['asignado_estudiante']) ? $value['asignado_estudiante'] : "";
            $estatusTitulo_asignacion = $this->columnaEstatusTitulo($i);
            $serialNuevo_asignacion = $this->columnaSerial($i); //   $count[] = $keys;
//            $anio_egreso = $this->columnaAnioEgreso($i);
//            $tipo_evaluacion = $this->columnaTipoEvaluacion($i);

//            $botones = "<div class='center'>" . CHtml::checkBox('OtorgarEstudiante[]', "false" ,array('value' => base64_encode($id_estudiante),
//                        "title" => "Entregado", 'id' => 'otorgarEstudiante_' . $i)
//                    ) .
//                    "</div>";
            $botones = "<div class='center'>
                            <input type='checkbox' name='OtorgarEstudiante[]' value='".base64_encode($id_estudiante)."' title = 'Entregado' id = 'otorgarEstudiante_".$i."' />
                        </div>";
            if ($tdocumento_identidad != '' && $documento_identidad != '' && $documento_identidad != null)
                $identidad = $tdocumento_identidad . '-' . $documento_identidad;
            else
                $identidad = $documento_identidad;
            $rawData[] = array(
                'id' => $key,
                'documento_identidad' => '<center>' . $identidad . '<input type="hidden" id="documentoAnterior" style="width: 120px" class="data span-7 data-documentoAnterior" name="documentoAnterior[]" value="' . base64_encode($identidad) . '" readonly="readonly" ></center>',
                //   'nombres' => '<center>' . "<input type='text' id='cambiarNombres' class='data span-7 data-cambiarNombres'  name='cambiarNombres[]' title='Introduzca los nombres correctos del estudiante' value='$nombres' >" . '<input type="hidden" id="nombresAnterior" class="data span-7" name="nombresAnterior[]" value="' . base64_encode($nombres) . '" readonly="readonly" >' . '</center>',
                //   'apellidos' => '<center>' . "<input type='text' id='cambiarApellidos' class='data span-7 data-cambiarApellidos' name='cambiarApellidos[]' title='Introduzca los apellidos correctos del estudiante' value='$apellidos'>" . '<input type="hidden" id="apellidosAnterior" class="data span-7" name="apellidosAnterior[]" value="' . base64_encode($apellidos) . '" readonly="readonly" ></center>',
                'nombresFinal' => '<center>' . $nombres . '</center>',
                'apellidosFinal' => '<center>' . $apellidos . '</center>',
                'serial' => '<center>' . $serial . '<input type="hidden" id="serialAnterior" class="data span-7 data-serialAnterior" name="serialAnterior[]" value="' . base64_encode($serial_id) . '" readonly="readonly" ></center>',
                'anio_egreso' => '<center>' . $anio_egreso . '</center>',
                'tipo_evaluacion' => '<center>' . $tipo_evaluacion . '</center>',
                'fecha_otorgamiento' => '<center>' . $fecha_otorgamiento . '</center>',
                'estatusTitulo' => '<center>' . $estatusTitulo . '</center>',
                'asignadoEstudiante' => '<center>' . $asignadoEstudiante . '</center>',
                'serialNuevo_asignacion' => '<center>' . $serialNuevo_asignacion . '</center>',
                'estatusTitulo_asignacion' => '<center>' . $estatusTitulo_asignacion . '</center>',
                'key' => '<center>' . $keys . '</center>',
                'botones' => '<center>' . $botones . '</center>'
            );
        }

//        $this->contar = count($count);
//        var_dump($this->contar);

        return new CArrayDataProvider($rawData, array(
            'pagination' => false
                )
        );
    }

//CAMBIE ESTO NUEVOS REQUERIMIENTOS.
//
//    public function columnaDocumentoIdent() {
//        $columnaAccion = '<input type="text" data-toggle="tooltip" data-placement="bottom" title="Introduzca el documento de identidad correcto" placeholder="V-0000000" title="Ej: V-99999999 ó E-99999999" id="cambiarDocumento" maxlength="15" size="15"  class=" data-documentoNuevo span-7" name="cambiarDocumento[]" onkeypress = "return CedulaFormat(this, event)" />';
//        //$columnaAccion = "<input type='text' id='cambiarDocumento' class='data span-7' style='width: 110px' name='cambiarDocumento[]' title='Introduzca el documento de identidad correcto'>";
//        $columna = '<center>' . $columnaAccion . '</center>';
//
//        return $columna;
//    }
//
//    public function columnaNombres() {
//
//        $columnaAccion = "<input type='text' id='cambiarNombres' class='data span-7 data-cambiarNombres' name='cambiarNombres[]' title='Introduzca los nombres correctos del estudiante'>";
//        $columna = '<center>' . $columnaAccion . '</center>';
//
//        return $columna;
//    }
//
//    public function columnaApellidos() {
//
//        $columnaAccion = "<input type='text' id='cambiarApellidos' class='data span-7 data-cambiarApellidos'  name='cambiarApellidos[]' title='Introduzca los apellidos correctos del estudiante'>";
//        $columna = '<center>' . $columnaAccion . '</center>';
//
//        return $columna;
//    }

    public function columnaSerial($i) {
        $usuario_id = Yii::app()->user->id;
        $id = (isset($_GET['id'])) ? $_GET['id'] : "";
        $plantel_id = base64_decode($id);
        //$plantel_id = Titulo::model()->obtenerIdPlantel($usuario_id);
        if ($plantel_id != '' && $plantel_id != null && is_numeric($plantel_id))
            $zona_educativa = Titulo::model()->obtenerZonaEduId($plantel_id);
        if ($zona_educativa != false)
            $selectSerial = Titulo::model()->SerialesDisponibles($zona_educativa);
//        var_dump($selectSerial);
//        die();
        if ($selectSerial != false)
            $columnaAccion = CHTML::dropDownList('cambiarSerial[]', 'cambiarSerial', CHtml::listData($selectSerial, 'id', 'nombre'), array('id' => 'cambiarSerial_' . $i, 'empty' => '- SELECCIONE -', 'class' => 'span-7 data-cambiarSerial', 'style' => 'width: 120px', 'disabled' => 'disabled', "data" => $i));
        else {
            $lista = array('id' => '', 'nombre' => '-SELECCIONE-');
            $columnaAccion = CHTML::dropDownList('cambiarSerial[]', 'cambiarSerial', CHtml::listData($lista, 'id', 'nombre'), array('id' => 'cambiarSerial_' . $i, 'class' => 'span-7 data-cambiarSerial', 'style' => 'width: 120px', 'disabled' => 'disabled', "data" => $i));
        }
        //"<input type='text' id='cambiarSerial' class='data span-7'  name='cambiarSerial[]' title='Elija un serial disponible en su zona educativa para el estudiante'>";
        $columna = '<center>' . $columnaAccion . '</center>';
        return $columna;
    }

    public function columnaAnioEgreso($i) {

        $yearNow = date("Y");
        $yearFrom = $yearNow - 20;
        $yearTo = $yearNow - 0;
        $arrYears = array();

        foreach (range($yearFrom, $yearTo) as $number) {
            $arrYears[$number] = $number;
        }
        $columnaAccion = CHTML::dropDownList('anioEgreso[]', 'anioEgreso', $arrYears, array('prompt' => '-SELECCIONE-', 'class' => 'span-7 data-anioEgreso', 'style' => 'width: 120px', 'title' => "Año de Egreso", "data" => $i,'id' => 'anioEgreso_' . $i));
        $columna = '<center>' . $columnaAccion . '</center>';

        return $columna;
    }

    public function columnaTipoEvaluacion($i) {

        $tipoEvaluacion = TipoEvaluacion::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A'"));
        $columnaAccion = CHTML::dropDownList('tipoEvaluacion[]', 'tipoEvaluacion', CHtml::listData($tipoEvaluacion, 'id', 'nombre'), array('empty' => '- SELECCIONE -', 'class' => 'span-7 data-tipoEvaluacion', 'style' => 'width: 120px', 'title' => "Tipo de Evaluación", "data" => $i,'id' => 'tipoEvaluacion_' . $i));
        $columna = '<center>' . $columnaAccion . '</center>';

        return $columna;
    }

    public function columnaEstatusTitulo($i) {

        $tipoEvaluacion = EstatusTitulo::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A' AND id NOT IN (1,2,3,4,5,11)"));
        $columnaAccion = CHTML::dropDownList('estatusTitulo[]', 'estatusTitulo', CHtml::listData($tipoEvaluacion, 'id', 'nombre'), array('empty' => '- SELECCIONE -', 'class' => 'span-7 data-estatusTitulo', 'style' => 'width: 120px', 'title' => "Estatus del Serial Anterior", "data" => $i,'id' => 'estatusTitulo_' . $i));
        $columna = '<center>' . $columnaAccion . '</center>';

        return $columna;
    }

    public function actionObtenerDatosPersona() {
        if (Yii::app()->request->isAjaxRequest) {
            $tDocumentoIdentidad = $this->getQuery('tdocumento_identidad');
            $documentoIdentidad = $this->getQuery('documento_identidad');
            $datosPersona = '';
            $nombresPersona = '';
            $apellidosPersona = '';
            $fecha_nacimientoFormat = '';
            if (in_array($tDocumentoIdentidad, array('V', 'E', 'P'))) {
                if (in_array($tDocumentoIdentidad, array('V', 'E'))) {
                    if (strlen((string) $documentoIdentidad) > 10) {
                        $mensaje = 'Estimado usuario el campo Documento de Identidad no puede superar los diez (10) caracteres.';
                        $title = 'Notificación de Error';
                        echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                        Yii::app()->end();
                    } else {
                        if (!is_numeric($documentoIdentidad)) {
                            $mensaje = 'Estimado usuario el campo Documento de Identidad debe poseer solo caracteres numericos.';
                            $title = 'Notificación de Error';
                            echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                            Yii::app()->end();
                        }
                    }
                }

                $datosPersona = AutoridadPlantel::model()->busquedaSaimeMixta($tDocumentoIdentidad, $documentoIdentidad);
                if ($datosPersona) {
                    (isset($datosPersona['nombre'])) ? $nombresPersona = $datosPersona['nombre'] : $nombresPersona = '';
                    (isset($datosPersona['apellido'])) ? $apellidosPersona = $datosPersona['apellido'] : $apellidosPersona = '';
                    (isset($datosPersona['sexo'])) ? $sexoPersona = $datosPersona['sexo'] : $sexoPersona = '';
                    (isset($datosPersona['fecha_nacimiento'])) ? $fecha_nacimiento = $datosPersona['fecha_nacimiento'] : $fecha_nacimiento = '';
                    $fecha_nacimientoFormat = date("d-m-Y", strtotime($fecha_nacimiento));
                    echo json_encode(array('statusCode' => 'SUCCESS', 'nombres' => $nombresPersona, 'apellidos' => $apellidosPersona, 'fecha_nacimiento' => $fecha_nacimientoFormat, 'sexo' => $sexoPersona));
                    Yii::app()->end();
                } else {
                    $mensaje = 'El Documento de Identidad ' . $tDocumentoIdentidad . '-' . $documentoIdentidad . ' no se encuentra registrado en nuestro sistema, por favor contacte al personal de soporte.';
                    $title = 'Notificación de Error';
                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                    Yii::app()->end();
                }
            } else {
                $mensaje = 'Estimado usuario, se han enviado valores no permitidos en el campo Tipo de Documento de Identidad.';
                $title = 'Notificación de Error';
                echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionAsignarEstudiante() {

        if (Yii::app()->request->isAjaxRequest) {
            $count = 0;
            $datosEstudiante = false;
            $id = (isset($_POST['id'])) ? $_POST['id'] : "";
            $plantel_id = base64_decode($id);
            $estudiante_select = (isset($_POST['estudiante_select_'])) ? $_POST['estudiante_select_'] : array();
            $estudiante_select = json_decode($estudiante_select);

            $fecha_otorgamiento = (isset($_POST['fecha_otorgamiento_'])) ? $_POST['fecha_otorgamiento_'] : "";
            $documento_identidad_director = (isset($_POST['documento_identidad_director_'])) ? $_POST['documento_identidad_director_'] : "";
            $documento_identidad_jefe = (isset($_POST['documento_identidad_jefe_'])) ? $_POST['documento_identidad_jefe_'] : "";
            $documento_identidad_funcionario = (isset($_POST['documento_identidad_funcionario_'])) ? $_POST['documento_identidad_funcionario_'] : "";

            $cambiarSerial_array = (isset($_POST['cambiar_serial_array_'])) ? $_POST['cambiar_serial_array_'] : array();
            $cambiarSerial_array = json_decode($cambiarSerial_array);
            $estatusTitulo_array = (isset($_POST['estatusTitulo_array_'])) ? $_POST['estatusTitulo_array_'] : array();
            $estatusTitulo_array = json_decode($estatusTitulo_array);
            $anioEgreso_array = (isset($_POST['anio_egreso_array_'])) ? $_POST['anio_egreso_array_'] : array();
            $anioEgreso_array = json_decode($anioEgreso_array);
            $tipoEvaluacion_array = (isset($_POST['tipo_evaluacion_array_'])) ? $_POST['tipo_evaluacion_array_'] : array();
            $tipoEvaluacion_array = json_decode($tipoEvaluacion_array);


//OBTENER DATOS ANTERIORES DE LOS ESTUDIANTES.
            $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_actual_id = $periodo_escolar_actual_id['id'];
            if ($plantel_id != '')
                $datosEstudiante = Titulo::model()->datosPorEstudiante($periodo_actual_id, $plantel_id, $mostrar = 0);
            if ($datosEstudiante != false) {
                $count = count($datosEstudiante);
                foreach ($datosEstudiante as $key => $value) {
                    $serialAnterior[$key] = base64_encode($value['serial_id']);
                    $nombresAnterior[$key] = base64_encode($value['nombres']);
                    $apellidosAnterior[$key] = base64_encode($value['apellidos']);
                    $documentoAnterior[$key] = base64_encode($value['tdocumento_identidad'] . '-' . $value['documento_identidad']);
                    $estudiantes[$key] = base64_encode($value['id_estudiante']);
                }
            }

//FIN

            if ($count == count($cambiarSerial_array)) {

                if ($estudiante_select != array()) {
                    foreach ($estudiante_select as $key => $value) {

                        $estudiante_select_[$key] = base64_decode($value);
                    }
                    $estudiante_pg_array = Utiles::toPgArray($estudiante_select_);
                } else {
                    $estudiante_select_ = array();
                    $estudiante_pg_array = Utiles::toPgArray($estudiante_select_);
                }

                if ($estudiantes != array()) {
                    foreach ($estudiantes as $key => $value) {

                        $estudiantes_[$key] = base64_decode($value);
                    }
                    $estudiantes_array = Utiles::toPgArray($estudiantes_);
                } else {
                    $estudiantes_ = array();
                    $estudiantes_array = Utiles::toPgArray($estudiantes_);
                }

    //Datos del CGridView
                if ($serialAnterior != array()) {
                    foreach ($serialAnterior as $key => $value) {

                        $serialAnterior_[$key] = base64_decode($value);
                    }
                    $serialAnterior_pg_array = Utiles::toPgArray($serialAnterior_, TRUE);
                } else {
                    $serialAnterior_ = array('');
                    $serialAnterior_pg_array = Utiles::toPgArray($serialAnterior_);
                }
    //Fin

                if ($cambiarSerial_array != array()) {
                    foreach ($cambiarSerial_array as $key => $value) {

                        $cambiarSerial_array_[$key] = base64_decode($value);
                    }
                    $cambiarSerial_array_pg_array = Utiles::toPgArray($cambiarSerial_array_, TRUE);
                } else {
                    $cambiarSerial_array_ = array('');
                    $cambiarSerial_array_pg_array = Utiles::toPgArray($cambiarSerial_array_);
                }

                $fechaOtorgamiento = date("Y-m-d", strtotime($fecha_otorgamiento));
                if ($documento_identidad_director != 'No Posee' && $documento_identidad_director != '') {

                    $documento = $documento_identidad_director;

                    $capturarCadena = explode("-", $documento);
                    $tdocumento_director_array_ = $capturarCadena[0];
                    $documento_director_array_ = $capturarCadena[1];
                } else {
                    $tdocumento_director_array_ = '0';
                    $documento_director_array_ = 0;
                }

                if ($documento_identidad_jefe != 'No Posee' && $documento_identidad_jefe != '') {

                    $documento = $documento_identidad_jefe;
                    $capturarCadena = explode("-", $documento);
                    $tdocumento_jefe_array_ = $capturarCadena[0];
                    $documento_jefe_array_ = $capturarCadena[1];
                } else {
                    $tdocumento_jefe_array_ = '0';
                    $documento_jefe_array_ = 0;
                }

                if ($documento_identidad_funcionario != 'No Posee' && $documento_identidad_funcionario != '') {

                    $documento = $documento_identidad_funcionario;
                    $capturarCadena = explode("-", $documento);
                    $tdocumento_funcionario_array_ = $capturarCadena[0];
                    $documento_funcionario_array_ = $capturarCadena[1];
                } else {
                    $tdocumento_funcionario_array_ = '0';
                    $documento_funcionario_array_ = 0;
                }

                if ($estatusTitulo_array != array()) {
                    foreach ($estatusTitulo_array as $key => $value) {

                        $estatusTitulo_array_[$key] = $value;
                    }
                    $estatusTitulo_array_pg_array = Utiles::toPgArray($estatusTitulo_array_);
                } else {
                    $estatusTitulo_array_ = array('');
                    $estatusTitulo_array_pg_array = Utiles::toPgArray($estatusTitulo_array_);
                }

                if ($anioEgreso_array != array()) {
                    foreach ($anioEgreso_array as $key => $value) {

                        $anioEgreso_array_[$key] = $value;
                    }
                    $anioEgreso_array_pg_array = Utiles::toPgArray($anioEgreso_array_);
                } else {
                    $anioEgreso_array_ = array('');
                    $anioEgreso_array_pg_array = Utiles::toPgArray($anioEgreso_array_);
                }

                if ($tipoEvaluacion_array != array()) {
                    foreach ($tipoEvaluacion_array as $key => $value) {

                        $tipoEvaluacion_array_[$key] = $value;
                    }
                    $tipoEvaluacion_array_pg_array = Utiles::toPgArray($tipoEvaluacion_array_);
                } else {
                    $tipoEvaluacion_array_ = array('');
                    $tipoEvaluacion_array_pg_array = Utiles::toPgArray($tipoEvaluacion_array_);
                }
    //Fin datos CGridView.

                $ruta = yii::app()->basePath;
                $ruta = $ruta . '/yiic';
                $username = Yii::app()->user->name;
                $nombre = Yii::app()->user->nombre;
                $apellido = Yii::app()->user->apellido;
                $cedula = Yii::app()->user->cedula;
                $usuario_id = Yii::app()->user->id;
                $grupo_id = Yii::app()->user->group;
                $ip = Yii::app()->request->userHostAddress;

//                echo "nohup /usr/bin/php $ruta SeguimientoTitulo AsignarEstudiante --estatusTitulo_array_pg_array=$estatusTitulo_array_pg_array --fechaOtorgamiento='$fechaOtorgamiento' --anioEgreso_array_pg_array=$anioEgreso_array_pg_array --tipoEvaluacion_array_pg_array=$tipoEvaluacion_array_pg_array --tdocumento_director_array_=$tdocumento_director_array_ --documento_director_array_=$documento_director_array_ --tdocumento_jefe_array_=$tdocumento_jefe_array_ --documento_jefe_array_=$documento_jefe_array_ --tdocumento_funcionario_array_=$tdocumento_funcionario_array_ --documento_funcionario_array_=$documento_funcionario_array_ --plantel_id=$plantel_id --estudiantes_array=$estudiantes_array --estudiante_pg_array=$estudiante_pg_array --serialAnterior=$serialAnterior_pg_array --serialNuevo=$cambiarSerial_array_pg_array --username=$username --nombre=$nombre --apellido=$apellido --cedula=$cedula --usuario_id=$usuario_id --grupo_id=$grupo_id --ip=$ip --count=$count 1>/dev/null & echo $!";
//                die();
                $resultado_comando = shell_exec("nohup /usr/bin/php $ruta SeguimientoTitulo AsignarEstudiante --estatusTitulo_array_pg_array=$estatusTitulo_array_pg_array --fechaOtorgamiento='$fechaOtorgamiento' --anioEgreso_array_pg_array=$anioEgreso_array_pg_array --tipoEvaluacion_array_pg_array=$tipoEvaluacion_array_pg_array --tdocumento_director_array_=$tdocumento_director_array_ --documento_director_array_=$documento_director_array_ --tdocumento_jefe_array_=$tdocumento_jefe_array_ --documento_jefe_array_=$documento_jefe_array_ --tdocumento_funcionario_array_=$tdocumento_funcionario_array_ --documento_funcionario_array_=$documento_funcionario_array_ --plantel_id=$plantel_id --estudiantes_array=$estudiantes_array --estudiante_pg_array=$estudiante_pg_array --serialAnterior=$serialAnterior_pg_array --serialNuevo=$cambiarSerial_array_pg_array --username=$username --nombre=$nombre --apellido=$apellido --cedula=$cedula --usuario_id=$usuario_id --grupo_id=$grupo_id --ip=$ip --count=$count 1>/dev/null & echo $!");

                $mensajeExitoso = "Estimado Usuario, el proceso de entrega de titulo a los estudiantes tardara un tiempo para realizarse. Cuando culmine el proceso se le notificara mediante un correo.";
                $respuesta['statusCode'] = "success";
                $respuesta['mensaje'] = $mensajeExitoso;
                echo json_encode($respuesta);
                //Utiles::enviarCorreo('mari.lac.mor@gmail.com', 'Notificación de la validación de entrega de seriales a los estudiantes', $resultado_comando);
                $respuesta['salida'] = $resultado_comando;
                //   header('Cache-Control: no-cache, must-revalidate');
                // header('Content-type: application/json');
                yii::app()->end();
            }else{
                throw new CHttpException(404, 'Ocurrio un error no se encontraron los datos que desea procesar. Recargue la página e intentelo de nuevo.');
            }
        } else {

            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionReporteSerialesEntregados() {

        $id = $this->getRequest('plantel');
        $plantel_id = base64_decode($id);
        if (!is_numeric($plantel_id)) {
            throw new CHttpException(404, 'No se ha encontrado el plantel que ha solicitado. Recargue la página e intentelo de nuevo.');
        } else {

            $modulo = "Titulo.SeguimientoTitulo.ReporteSerialesEntregados";
            $ip = Yii::app()->request->userHostAddress;
            $ruta = yii::app()->basePath;
            $ruta = $ruta . '/yiic';
            $username = Yii::app()->user->name;
            $usuario_id = Yii::app()->user->id;
            $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_actual_id = $periodo_escolar_actual_id['id'];

            $datosPlantel = Plantel::model()->obtenerDatosPlantel($plantel_id, $periodo_actual_id);
            $datosEstudiante = Titulo::model()->datosPorEstudiante($periodo_actual_id, $plantel_id, $mostrar = 1);
            $datosDirector = Titulo::model()->ObtenerDatosDirector($plantel_id, $periodo_actual_id);
            $datosJefeControlEst = Titulo::model()->ObtenerDatosJefeControlEst($plantel_id, $periodo_actual_id);
            $datosFuncionario = Titulo::model()->ObtenerDatosFuncionario($plantel_id, $periodo_actual_id);
            $datos_autoridades = array_merge($datosDirector, $datosJefeControlEst, $datosFuncionario);
           // $datos_autoridades = Plantel::model()->getDatosAutoridadReporte($plantel_id, $variable = 'DZ');
//            var_dump($datosPlantel);
//            var_dump($datos_autoridades);
//            die();
            if ($datosPlantel != false && $datosEstudiante != false) {
                $nombre_pdf = $datosPlantel['nombre_plantel'] . ' - ' . $datosPlantel['cod_plantel'];
                $mpdf = new mpdf('', 'LEGAL', 0, '', 15, 15, 56.2, 50);            //$mpdf->SetMargins(3,69.1,70);
                $header = $this->renderPartial('_headerReporteSerialesEntregados', array('datosPlantel' => $datosPlantel, 'datosEstudiante' => $datosEstudiante), true);
                $footer = $this->renderPartial('_footerReporteSerialesEntregados', array('datosAutoridades' => $datos_autoridades), true);
                $body = $this->renderPartial('_bodyReporteSerialesEntregados', array('datosPlantel' => $datosPlantel, 'datosEstudiante' => $datosEstudiante), true);
                $mpdf->SetFont('sans-serif');
                $mpdf->SetHTMLHeader($header);
                $mpdf->setHTMLFooter($footer . '<br>' . '<p style="text-align:center;"> {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($body);
                $mpdf->Output($nombre_pdf . '.pdf', 'D');
                $this->registerLog('REPORTES', $modulo . 'PDF', 'EXITOSO', 'Descarga del pdf de seguimiento de los seriales entregados a los estudiantes con el nombre: ' . $nombre_pdf . ' del plantel ' . $datosPlantel['nombre_plantel'], $ip, $usuario_id, $username);
            }
        }
    }

    /*      FIN      */
}
