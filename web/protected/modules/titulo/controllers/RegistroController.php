<?php

class RegistroController extends Controller {

    const A_ACTIVO = 'A';
    const A_EJECUTADO = 'X';
    const A_ELIMINADO = 'E';
    const A_ANULADO = 'D';

    static $_permissionControl = array(
        'read' => 'Consulta Lista de Papel Moneda',
        'write' => 'Registrar Papel Moneda',
        'admin' => 'Anulación de Papel Moneda',
        'label' => 'Módulo de Gestión de Papel Moneda'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index',),
                'pbac' => array('read', 'write',),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index','uploadFile', 'registroSeriales'),
                'pbac' => array('write',),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('anulacion'),
                'pbac' => array('admin',),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        $this->csrfTokenName = 'csrfToken';
        $token = $this->getCsrfToken('Carga masiva de Papel Moneda MPPE - Chavez Vive!');

        $papelMoneda = new PapelMoneda();
        $registroProgramado = new RegistroProgramadoSerial();

        if(Yii::app()->request->isAjaxRequest){
            if($this->hasQuery('RegistroProgramadoSerial')){
                $dataRegistroProgramado = $this->getQuery('RegistroProgramadoSerial');
                $registroProgramado->attributes = $dataRegistroProgramado;
                if(!is_null($dataRegistroProgramado['username_ini'])){
                    $registroProgramado->username_ini = $dataRegistroProgramado['username_ini'];
                }
                $this->renderPartial('_admin_carga_programada', array('registroProgramado'=>$registroProgramado,));
            }
            else{
                $dataPapelMoneda = $this->getQuery('PapelMoneda');
                $papelMoneda->attributes = $dataPapelMoneda;
                if(!is_null($dataPapelMoneda['cod_plantel'])){
                    $papelMoneda->cod_plantel = $dataPapelMoneda['cod_plantel'];
                }
                if(!is_null($dataPapelMoneda['username_ini'])){
                    $papelMoneda->username_ini = $dataPapelMoneda['username_ini'];
                }
                if(isset($dataPapelMoneda['username_act'])){
                    $papelMoneda->username_act = $dataPapelMoneda['username_act'];
                }
                $this->renderPartial('_admin_papel_moneda', array('model'=>$papelMoneda,));
            }
        }else{
            $this->render('index', array('model'=>$papelMoneda, 'token'=>$token,'registroProgramado'=>$registroProgramado));
        }
    }

    public function actionUploadFile(){

        if(Yii::app()->request->isAjaxRequest){
            $userId = Yii::app()->user->id;
            $upload_handler = new UploadHandler(null, true, null, date('YmdHis') . '-REGPM'.$userId, "/public/uploads/titulos/");
        }
        else{
            throw new CHttpException(403, 'No esta permitido efectuar esta operación mediante esta vía');
        }

    }

    public function actionRegistroSeriales(){

        if(Yii::app()->request->isAjaxRequest){

            $token = $this->getCsrfToken('Carga masiva de Papel Moneda MPPE - Chavez Vive!');

            $fileName = $this->getPost('archivo');
            $dirName = realpath(Yii::app()->basePath.'/..');
            $filePath = $dirName.'/public/uploads/titulos/'.$fileName;

            if($fileName && is_file($filePath) && Utiles::isValidExtension($fileName, array('xlsx','xls', 'ods'))){

                $this->csrfTokenName = 'csrfToken';

                if($this->validateCsrfToken()){

                    $archivoObj = new ArchivoPapelMoneda();
                    $archivoObj->nombre = $fileName;
                    $archivoObj->checksum = sha1_file($filePath);
                    $archivoObj->usuario_ini_id = Yii::app()->user->id;
                    $archivoObj->fecha_ini = date('Y-m-d H:i:s');
                    $archivoObj->estatus = 'A';

                    if($archivoObj->save()){

                        $result = new stdClass();

                        $readDataOnly = true;
                        $excelReader = new ExcelReader($filePath);

                        $result->archivo = $fileName;
                        $operacion = false;

                        list($operacion,
                             $result->class_style,
                             $result->message,
                             $objReaderExcel,
                             $objReader) = $excelReader->getReader($readDataOnly);

                        //Verifico que las extensiones del archivo sean las requeridas
                        if($operacion){

                            $sheetData = $objReader->getActiveSheet()->toArray(null,true,true,true);

                            $archivoObj = new PapelMoneda();

                            try{

                                list($operacion, $result->response) = $archivoObj->loadDataSeriales($sheetData, $fileName);

                                if($operacion){
                                    $result->class_style = "success";
                                    $result->message = "El proceso se ha completado. Puede ver el resultado detallado del mismo en la tabla siguiente.";
                                }else{
                                    $result->class_style = "alert";
                                    $result->message = "El proceso ha culminado con algunas advertencias. Puede ver el resultado detallado del mismo en la tabla siguiente.";
                                }

                            }
                            catch(Exception $e){

                                $result->class_style = "error";
                                $result->message = "Ha ocurrido un error en el proceso de carga. Recargue la página e intentelo de nuevo. Si el error se repite notifiquelo a los administradores del sistema. ({$e->getMessage()})";

                            }

                        }else{
                            $result->class_style = "error";
                            $result->message = "Ha ocurrido un error en el proceso de lectura del archivo. Recargue la página e intentelo de nuevo. Si el error se repite notifiquelo a los administradores del sistema.";
                        }

                        $result->token = $token;

                        $this->jsonResponse($result);
                    }

                }

            }
            else{
                throw new CHttpException(401, 'Datos insuficientes para efectuar la operación. Puede que el archivo cargado no cumpla con los requerimientos mínimos para ser procesado');
            }
        }
        else{
            throw new CHttpException(403, 'No esta permitido efectuar esta operación mediante esta vía');
        }
    }

    public function fechaIni($data) {
        $fechaIni = $data["fecha_ini"];
        if (empty($fechaIni)) {
            $fechaIni = "";
        } else {
            $fechaIni = date("d-m-Y", strtotime($fechaIni));
        }
        return $fechaIni;
    }

    public function fechaAct($data) {
        $fechaAct = $data["fecha_act"];
        if (empty($fechaAct)) {
            $fechaAct = "";
        } else {
            $fechaAct = date("d-m-Y", strtotime($fechaAct));
        }
        return $fechaAct;
    }

    public function fechaCarga($data) {
        $fechaCarga = $data["fecha_carga"];
        if (empty($fechaCarga)) {
            $fechaCarga = "";
        } else {
            $fechaCarga = date("d-m-Y", strtotime($fechaCarga));
        }
        return $fechaCarga;
    }

    public function columnaAcciones(){
      return "";
    }

    public function columnaAccionesRegistroProgramado($data){
        $id = $data["id"];
        $id_encoded = base64_encode($id);
        $columna = '';
        $estatus = $data["estatus"];
        if($estatus=='A'){
            $columna = '<div class="action-buttons">';
            $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar Solicitud", "onClick" => "registroProgramadoDialog('$id_encoded','/titulo/registroProgramadoSerial/editar','Atender Solicitud de Registro','update')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Anular Solicitud", "onClick" => "registroProgramadoDialog('$id_encoded','/titulo/registroProgramadoSerial/anular','Anular Solicitud de Registro','delete')")) . '&nbsp;&nbsp;';
            $columna .= '</div>';
        }
        elseif($estatus=='E'){
            $columna = '<div class="action-buttons">';
            $columna .= CHtml::link("", "", array("class" => "fa fa-check blue", "title" => "Reactivar Solicitud", "onClick" => "registroProgramadoDialog('$id_encoded','/titulo/registroProgramadoSerial/anular','Activar Solicitud de Registro','activate')")) . '&nbsp;&nbsp;';
            $columna .= '</div>';
        }

        return $columna;
    }

    public function estatus_asig() {
        $estatus = array();
        $estatus[] = array('id' => self::A_ACTIVO, 'nombre' => 'Activo');
        $estatus[] = array('id' => self::A_EJECUTADO, 'nombre' => 'Ejecutado');
        $estatus[] = array('id' => self::A_ELIMINADO, 'nombre' => 'Eliminado');
        return $estatus;
    }

    public function estatus($data) {
        $estatus = '';

        $groupId = Yii::app()->user->group;
        $id = $data["estatus"];
        if ($id == self::A_ACTIVO) {
            $estatus = 'Activo';
        }
        elseif ($id == self::A_ELIMINADO) {
            $estatus = 'Eliminado';
        }
        elseif ($id == self::A_EJECUTADO) {
            $estatus = 'Ejecutado';
        }

        return $estatus;
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
