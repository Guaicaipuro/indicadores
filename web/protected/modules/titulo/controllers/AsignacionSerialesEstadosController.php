<?php

class AsignacionSerialesEstadosController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Muestra solicitud por estados',
        'write' => 'Asignacion de Seriales por Estados',
        'admin' => 'Asignacion de Seriales por Estados',
        'label' => 'Asignacion de Seriales por Estados'
    );

    const MODULO = "Titulo.AsignacionPorEstado";

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
//'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'mostrarPlanteles', 'reportePorEstado', 'reporteCSV', 'obtenerPdf', 'generarReportePorEstado'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('guardarAsignacionSerialesPorEstados'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        $estado = new Estado;
        $listaEstados = Estado::model()->estadosList();
        $estado_id = null;
        $sumaEstudiantes = null;
        $suma_restantes = null;
        $sumaTotal = null;
        $resultadoPlanteles = array();
        $poseeEstudiantes = false;
        $nombreEstado = false;
        $verificarImpresionReporte = false;
        $mostrar_boton = false;
        $existePdf = false;
        $verificarImpresionReportePDF = 0;

        $this->render('index', array('estado' => $estado,
            'listaEstados' => $listaEstados,
            'resultadoPlanteles' => $resultadoPlanteles,
            'poseeEstudiantes' => $poseeEstudiantes,
            'estado_id' => $estado_id,
            'nombreEstado' => $nombreEstado,
            'sumaEstudiantes' => $sumaEstudiantes,
            'suma_restantes' => $suma_restantes,
            'sumaTotal' => $sumaTotal,
            'verificarImpresionReporte' => $verificarImpresionReporte,
            'mostrar_boton' => $mostrar_boton,
            'existePdf' => $existePdf,
            'verificarImpresionReportePDF' => $verificarImpresionReportePDF
        ));
    }

    public function actionMostrarPlanteles() {


        $estado = new Estado;
        $listaEstados = Estado::model()->estadosList();
        $estado_id = $_REQUEST['estado'];
        $periodo_actual_id = $_REQUEST['periodo'];
        $poseeEstudiantes = false;
//        $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
//        $periodo_actual_id = $periodo_escolar_actual_id['id'];
        $planteles = Titulo::model()->planteles($estado_id, $periodo_actual_id, $a = 'N');
        //   var_dump($planteles);
        $sumaEstudiantes = null;
        $suma_restantes = null;
        $sumaTotal = null;
        $existePdf = false;
        $estadoPK = Estado::model()->findByPk($estado_id);
        $nombreEstado = $estadoPK['nombre'];
        $fecha = date('Y');
        $ruta = yii::app()->basePath . '/../public/serial_titulo/';
        $nombrePdf = 'Distribucion_Seriales_' . $nombreEstado . '_' . $fecha . '.pdf';
        $verificarImpresionReportePDF = 0;

        $verificarImpresionReporte = Titulo::model()->verificarImpresionReporte($estado_id, $periodo_actual_id);

//        var_dump($planteles);
        //    die();
        if ($planteles != false || $verificarImpresionReporte != false) {

            if ($planteles != false && $verificarImpresionReporte == false || $planteles != false && $verificarImpresionReporte == 4) {
                //       echo "entre plantel";
                $mostrar_boton = true;
                $resultadoPlanteles = $this->dataProviderPlanteles($planteles);
                $suma_restante = Titulo::model()->mostrarInformacionGeneral($estado_id, $periodo_actual_id, $a = 'N');

                //   var_dump($suma_restante . ' restant');
                //     $suma_restantes = 0;

                foreach ($planteles as $key => $value) {
                    $cant_estudiantes[] = $value['cant_estudiantes'];
                    //   $suma_restantes = 2 + $suma_restantes;
                }
                $sumaEstudiantes = array_sum($cant_estudiantes);

                $suma_restantes = $suma_restante - $sumaEstudiantes;
//                var_dump($suma_restantes . ' restant');
//                var_dump($sumaEstudiantes . ' est');
                foreach ($cant_estudiantes as $value) {
                    //$restantes = 2;
                    $total[] = $value;
                }

                $sumaTotal = $suma_restante;

                //     var_dump($sumaTotal . ' total');
//                $informacionGeneral = Titulo::model()->mostrarInformacionGeneral($estado_id, $periodo_actual_id);
//                var_dump($informacionGeneral[0]['cant_estudiantes']);
//
//                if ($informacionGeneral[0]['cant_estudiantes'] != 0) {
//                    foreach ($informacionGeneral as $key => $value) {
//                        $cant_estudiantes = $value['cant_estudiantes'];
//                        $suma_restantes = $value['seriales_asignados_plantel'];
//                    }
//                    $sumaEstudiantes = $cant_estudiantes + $sumaEstudiantes;
//                    var_dump($sumaEstudiantes);
//                    foreach ($cant_estudiantes as $value) {
//                        $restantes = 2;
//                        $total[] = $value + $restantes;
//                    }
//                    $sumaTotal = array_sum($total) + $sumaTotal;
//                }
            } else {

                //       echo "entro else";
                $informacionGeneral = Titulo::model()->mostrarInformacionGeneral($estado_id, $periodo_actual_id, $a = 'S');

                $mostrar_boton = false;
                //   if ($planteles != false) {
                //       $resultadoPlanteles = $this->dataProviderPlanteles($planteles);
                //    } else {
                $resultadoPlanteles = array();
                // }
                if ($informacionGeneral != false) {
                    //  echo "entre";
                    $suma_restantes = 0;
                    foreach ($informacionGeneral as $key => $value) {
                        $cant_estudiantes[] = $value['cant_estudiantes'];
                        $suma_restantes = $value['seriales_asignados_plantel'];
                    }

                    $sumaEstudiantes = array_sum($cant_estudiantes);

                    foreach ($cant_estudiantes as $value) {
                        //  $restantes = 2;
                        $total[] = $value + $suma_restantes;
                    }
                    $sumaTotal = array_sum($total);

                    $existe_nombre_pdf = $ruta . 'Distribucion_Seriales_' . $nombreEstado . '_' . $fecha . '.pdf';

                    if (file_exists($existe_nombre_pdf)) {
                        //    echo "entro";
                        $existePdf = true;
                        $verificarImpresionReportePDF = Titulo::model()->verificarImpresionReportePDF($estado_id, $periodo_actual_id, $nombrePdf);
                    } else {
                        //  echo "entre abajo";
                        $verificarImpresionReportePDF = Titulo::model()->verificarImpresionReportePDF($estado_id, $periodo_actual_id, $nombrePdf);
                    }
                } else {

                    $mostrar_boton = true;
                    $sumaEstudiantes = 0;
                    $sumaTotal = 0;
                    $suma_restantes = 0;
                }
            }

            //      echo "entre abajo";
//            var_dump($sumaEstudiantes . ' estud');
//            var_dump($sumaTotal . ' total');
//            var_dump($suma_restantes . ' restantes');
            //     var_dump($verificarImpresionReportePDF);
            $estadoPK = Estado::model()->findByPk($estado_id);
            $nombreEstado = $estadoPK['nombre'];
            $this->renderPartial('index', array('estado' => $estado,
                'listaEstados' => $listaEstados,
                'resultadoPlanteles' => $resultadoPlanteles,
                'poseeEstudiantes' => $poseeEstudiantes,
                'estado_id' => $estado_id,
                'nombreEstado' => $nombreEstado,
                'sumaEstudiantes' => $sumaEstudiantes,
                'suma_restantes' => $suma_restantes,
                'sumaTotal' => $sumaTotal,
                'verificarImpresionReporte' => $verificarImpresionReporte,
                'mostrar_boton' => $mostrar_boton,
                'existePdf' => $existePdf,
                'verificarImpresionReportePDF' => $verificarImpresionReportePDF
            ));
        } else {
            //        echo "entre mas abajo";
            $poseeEstudiantes = true;
            $resultadoPlanteles = array();
            $mostrar_boton = false;
            $verificarImpresionReporte = false;
            $estadoPK = Estado::model()->findByPk($estado_id);
            $nombreEstado = $estadoPK['nombre'];
            $this->renderPartial('index', array('estado' => $estado,
                'listaEstados' => $listaEstados,
                'resultadoPlanteles' => $resultadoPlanteles,
                'poseeEstudiantes' => $poseeEstudiantes,
                'estado_id' => $estado_id,
                'nombreEstado' => $nombreEstado,
                'sumaEstudiantes' => $sumaEstudiantes,
                'suma_restantes' => $suma_restantes,
                'sumaTotal' => $sumaTotal,
                'verificarImpresionReporte' => $verificarImpresionReporte,
                'mostrar_boton' => $mostrar_boton,
                'existePdf' => $existePdf,
                'verificarImpresionReportePDF' => $verificarImpresionReportePDF
            ));
        }
    }

    public function dataProviderPlanteles($planteles) {


        foreach ($planteles as $key => $value) {
            $nombre = $value['nombre'];
            $cod_estadistico = $value['cod_estadistico'];
            $cod_plantel = $value['cod_plantel'];
            $cant_estudiantes = $value['cant_estudiantes'];

            // $restantes = 2;
            //  $total = $cant_estudiantes + $restantes;

            $rawData[] = array(
                'id' => $key,
                'nombre' => '<center>' . $nombre . '</center>',
                'cod_estadistico' => '<center>' . $cod_estadistico . '</center>',
                'cod_plantel' => '<center>' . $cod_plantel . '</center>',
                'cant_estudiantes' => '<center>' . $cant_estudiantes . '</center>'
                    //     'cant_estudiantes' => '<center>' . $cant_estudiantes . ' + ' . $restantes . ' = ' . $total . '</center>'
            );
        }

        return new CArrayDataProvider($rawData, array(
            'pagination' => false
                )
        );
    }

    public function actionGuardarAsignacionSerialesPorEstados() {

        $estado_id = $_REQUEST['estado_id'];
        $cantidad_estudiantes = $_REQUEST['cantidad_estudiantes'];
        $estadoPK = Estado::model()->findByPk($estado_id);
        $nombreEstado = $estadoPK['nombre'];
        $ruta = yii::app()->basePath;
        $ruta = $ruta . '/yiic';
        $username = Yii::app()->user->name;
        $nombre = Yii::app()->user->nombre;
        $apellido = Yii::app()->user->apellido;
        $cedula = Yii::app()->user->cedula;
        $usuario_id = Yii::app()->user->id;
        $grupo_id = Yii::app()->user->group;

// $obtenerCodigoVerificacion = Titulo::model()->obtenerCodigoVerificacion();
// echo "php $ruta AsignacionSerialesPorEstado AsignacionPorEstado --estado_id=$estado_id --username=$username --nombre=$nombre --apellido=$apellido --cedula=$cedula --usuario_id=$usuario_id --grupo_id=$grupo_id";
//soporte_gescolar@me.gob.ve

        $verificarProcesosCorriendo = Titulo::model()->verificarProcesosCorriendo();

        if ($verificarProcesosCorriendo == 0) {
            $zona_educativa = Titulo::model()->obtenerZonaEducativa($estado_id);
            $existenSerialesZonaEdu = Titulo::model()->existenSerialesZonaEducativa($zona_educativa);
            //     var_dump($existenSerialesZonaEdu);
//        die();
            if ($existenSerialesZonaEdu != 0) {
//                var_dump($existenSerialesZonaEdu . ' total');
//                var_dump($cantidad_estudiantes . ' est');
                if ($cantidad_estudiantes <= $existenSerialesZonaEdu) {
//                    echo "nohup /usr/bin/php $ruta AsignacionSerialesPorEstado AsignacionPorEstado --estado_id=$estado_id --username=$username --nombre=$nombre --apellido=$apellido --cedula=$cedula --usuario_id=$usuario_id --grupo_id=$grupo_id 1>/dev/null & echo $!";
//                    die();
                    $resultado_comando = shell_exec("nohup /usr/bin/php $ruta AsignacionSerialesPorEstado AsignacionPorEstado --estado_id=$estado_id --username=$username --nombre=$nombre --apellido=$apellido --cedula=$cedula --usuario_id=$usuario_id --grupo_id=$grupo_id 1>/dev/null & echo $!");
                    Utiles::enviarCorreo('soporte_gescolar@me.gob.ve', 'Notificación de asignación de seriales en el estado' . $nombreEstado, $resultado_comando);
                    $mensajeExitoso = "Estimado Usuario, el proceso de asignación de seriales de título en el  estado " . $nombreEstado . " tardara un tiempo para realizarse. Cuando culmine el proceso se le notificara mediante un correo.";
                    $respuesta['statusCode'] = 'success';
                    $respuesta['mensaje'] = $mensajeExitoso;
                    $respuesta['salida'] = $resultado_comando;
                    echo json_encode($respuesta);
                } else {
                    $mensajeError = "Estimado Usuario, el proceso de asignación de seriales de título en el  estado " . $nombreEstado . ", no se puede realizar porque la cantidad de seriales registrados es menor a los estudiantes a los que se le tiene que asignar seriales.";
                    $respuesta['statusCode'] = 'error';
                    $respuesta['mensaje'] = $mensajeError;
                    echo json_encode($respuesta);
                }
            } else {
                $mensajeError = "Estimado Usuario, el proceso de asignación de seriales de título en el  estado " . $nombreEstado . ", no se puede realizar porque no tiene seriales disponibles, registre los seriales correspondientes para este estado.";
                $respuesta['statusCode'] = 'error';
                $respuesta['mensaje'] = $mensajeError;
                echo json_encode($respuesta);
            }
        } else {
            $mensajeError = "Estimado Usuario, ya se esta ejecutando un proceso de asignación de seriales, por favor espere hasta que reciba un correo con la notificación de la culminación del proceso.";
            $respuesta['statusCode'] = 'error';
            $respuesta['mensaje'] = $mensajeError;
            echo json_encode($respuesta);
        }
//        if (is_null($resultado_comando)) {
//            $mensajeExitoso = "Estimado Usuario, el proceso de asignación de seriales de título tardara un tiempo para realizarse. Cuando culmine el proceso se le notificara mediante un correo";
//            $respuesta['statusCode'] = 'success';
//            $respuesta['mensaje'] = $mensajeExitoso;
//            echo json_encode($respuesta);
//        } else {
//            $msj = $resultado_comando;
//            Utiles::enviarCorreo('soporte_gescolar@me.gob.ve', 'Error en asignación de seriales por estado', $msj);
//            $mensajeExitoso = "Estimado Usuario, ha ocurrido un error durante el proceso. Intente nuevamente, de repetirse el error reporte lo ocurrido al administrador del sistema";
//            $respuesta['statusCode'] = 'error';
//            $respuesta['mensaje'] = $mensajeExitoso;
//            echo json_encode($respuesta);
//        }
    }

    public function actionGenerarReportePorEstado() {

        $estado_id = $this->getRequest('estado_id');

        if (is_numeric($estado_id) && $estado_id != '') {
            $ruta = yii::app()->basePath;
            $ruta = $ruta . '/yiic';
            $username = Yii::app()->user->name;
            $usuario_id = Yii::app()->user->id;
            $grupo_id = Yii::app()->user->group;
            $estadoPK = Estado::model()->findByPk($estado_id);
            $nombreEstado = $estadoPK['nombre'];

            $verificarProcesosCorriendoPDF = Titulo::model()->verificarProcesosCorriendoPDF();

            if ($verificarProcesosCorriendoPDF == 0) {
//                echo "nohup /usr/bin/php $ruta AsignacionSerialesPorEstado ReportePorEstado --estado_id=$estado_id --username=$username --usuario_id=$usuario_id --grupo_id=$grupo_id 1>/dev/null & echo $!";
//                die();
                $resultado_comando = shell_exec("nohup /usr/bin/php $ruta AsignacionSerialesPorEstado ReportePorEstado --estado_id=$estado_id --username=$username --usuario_id=$usuario_id --grupo_id=$grupo_id 1>/dev/null & echo $!");
                Utiles::enviarCorreo('soporte_gescolar@me.gob.ve', 'Notificación de generación del pdf del estado ' . $nombreEstado, $resultado_comando);
                $mensajeExitoso = "Estimado Usuario, el proceso de generación del pdf del  estado " . $nombreEstado . " tardara un tiempo para realizarse. Cuando culmine el proceso se le notificara mediante un correo.";
                $respuesta['statusCode'] = 'success';
                $respuesta['mensaje'] = $mensajeExitoso;
                $respuesta['salida'] = $resultado_comando;
                echo json_encode($respuesta);
            } else {
                $mensajeError = "Estimado Usuario, ya se esta ejecutando un proceso de generación de pdf, por favor espere hasta que reciba un correo con la notificación de la culminación del proceso.";
                $respuesta['statusCode'] = 'error';
                $respuesta['mensaje'] = $mensajeError;
                echo json_encode($respuesta);
            }
        }
    }

//    public function actionReporte() {
//
//        $estado_id = $this->getRequest('estado');
//        $estado_id_decoded = base64_decode($estado_id);
//
//        if (is_numeric($estado_id_decoded)) {
//            $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
//            $periodo_actual_id = $periodo_escolar_actual_id['id'];
//            $planteles = Titulo::model()->planteles($estado_id_decoded, $periodo_actual_id, $a = 'S');
//            $datosPlantel = Plantel::model()->obtenerDatosPlantel($planteles, $estado_id_decoded, $periodo_actual_id);
//            $datosEstudiantes = Estudiante::model()->obtenerDatosEstudiantesReporte($datosPlantel, $estado_id_decoded, $periodo_actual_id);
//            $datos_autoridades = Plantel::model()->obtenerDatosAutoridadReporte($planteles);
//
//            $nombre_pdf = 'hola'; //(isset($datosPlantel['cod_plantel']) AND $datosPlantel['cod_plantel'] != '') ? $datosPlantel['cod_plantel'] . '-Matricula' : '-Matricula';
//            $mpdf = new mpdf('', 'LEGAL', 0, '', 15, 15, 69.1, 50);            //$mpdf->SetMargins(3,69.1,70);
//            $header = $this->renderPartial('_headerReporteEstudiantesConSeriales', array('datosPlantel' => $datosPlantel), true);
//            $footer = $this->renderPartial('_footerReporteEstudiantesConSeriales', array('datosAutoridades' => $datos_autoridades), true);
//            $body = $this->renderPartial('_bodyReporteEstudiantesConSeriales', array('datosPlantel' => $datosPlantel, 'estudiantes' => $datosEstudiantes), true);
//            $mpdf->SetFont('sans-serif');
//            $mpdf->SetHTMLHeader($header);
//            $mpdf->setHTMLFooter($footer . '<br>' . '<p style="text-align:center;"> {PAGENO} / {nb}</p>');
//            $mpdf->WriteHTML($body);
//
//            //  $this->registerLog('LECTURA', self::MODULO . 'Reporte', 'EXITOSO', 'Entró matricular la Seccion Plantel' . $seccion_plantel_id);
//
//            $mpdf->Output($nombre_pdf . '.pdf', 'D');
//        } else
//            throw new CHttpException(404, 'Estimado Usuario, no se ha encontrado el recurso solicitado. Vuelva a la página anterior e intente de nuevo');
//    }
//    public function actionReportePorEstado() {
//
//        $estado_id = $this->getRequest('estado');
//        $estado_id_decoded = base64_decode($estado_id);
//
//        if (is_numeric($estado_id_decoded)) {
//            $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
//            $periodo_actual_id = $periodo_escolar_actual_id['id'];
//            $zona_educativa = Titulo::model()->obtenerZonaEducativa($estado_id);
////            var_dump($zona_educativa);
////            die();
//            $datosReporte = Titulo::model()->obtenerDatosReporteSerialesPorEstado($estado_id_decoded, $periodo_actual_id, $zona_educativa);
//            $estadoPK = Estado::model()->findByPk($estado_id_decoded);
//            $nombreEstado = $estadoPK['nombre'];
//
//            /* ------ Lotes de seriales por plantel ------ */
////            $plantel_id = Titulo::model()->planteles($estado_id_decoded, $periodo_actual_id, $a = 'S');
////            $serialesAsignados = SeguimientoPapelMoneda::model()->serialesAsignados($plantel_id);
////
////                $arraySeriales = array();
////                $lote = 0;
////                $cantidad = 0;
////                for ($x = 0; $x < count($serialesAsignados); $x++) {
////                    $comparar = $serialesAsignados[$x]['serial'];
////                    if ($x != count($serialesAsignados) - 1)
////                        $primer = $serialesAsignados[$x + 1]['serial'];
////
////                    if (isset($serialesAsignados[$x] ['serial'])) {
////
////                        if ((int) ($comparar + 1) == (int) $primer) {
////
////                            if ($x == 0) {
////                                $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x]['serial'];
////                                $cantidad = $cantidad + 1;
////                            }
////                            if ($x != 0) {
////                                $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x]['serial'];
////                                $cantidad = $cantidad + 1;
////                            }
////                        } else {
////                            $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x]['serial'];
////                            $lote = $lote + 1;
////                            $cantidad = 0;
////                            if ($x != count($serialesAsignados) - 1) {
////                                $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x + 1]['serial'];
////                                $cantidad = $cantidad + 1;
////                                $x = $x + 1;
////                            }
////                        }
////                    } else {
////
////                    }
////                }
////
////                if ($serialesAsignados != false) {
////                    $seriales = array();
////                    for ($y = 0; $y < count($arraySeriales); $y++) {
////                        for ($a = 0; $a < count($arraySeriales[$y]); $a++) {
////                            if ($a == 0) {
////                                $seriales[$y]['primer_serial'] = $arraySeriales[$y][$a];
////                            }
////                            if ($a == (count($arraySeriales[$y]) - 1)) {
////                                $seriales[$y]['ultimo_serial'] = $arraySeriales[$y][$a];
////                            }
////                            $seriales[$y]['cantidad_serial'] = count($arraySeriales[$y]);
////                        }
////                    }
////                    $dataProviderSerialesAsignad = $this->dataProviderMostrarSeriales($seriales, $mostrarSerialesAsignados = true);
////                }
//            /* ------ Fin ------ */
//            $fecha = date('d-m-Y H:i:s');
//            $ruta = '/var/www/escolar/web/public/serial_titulo';
//            $nombre_pdf = $ruta . '/Distribución_Seriales_' . $nombreEstado . '-' . $fecha; //(isset($datosPlantel['cod_plantel']) AND $datosPlantel['cod_plantel'] != '') ? $datosPlantel['cod_plantel'] . '-Matricula' : '-Matricula';
//            $mpdf = new mpdf('', 'LEGAL', 0, '', 15, 15, 38, 50);            //$mpdf->SetMargins(3,69.1,70);
//            $header = $this->renderPartial('_headerReporteEstudiantesConSerialesPorEstado', array('nombreEstado' => $nombreEstado), true);
////  $footer = $this->renderPartial('_footerReporteEstudiantesConSeriales', array('datosAutoridades' => $datos_autoridades), true);
//            $body = $this->renderPartial('_bodyReporteEstudiantesConSerialesPorEstado', array('datosReporte' => $datosReporte), true);
//            $mpdf->SetFont('sans-serif');
//            $mpdf->SetHTMLHeader($header);
////     $mpdf->setHTMLFooter($footer . '<br>' . '<p style="text-align:center;"> {PAGENO} / {nb}</p>');
//            $mpdf->WriteHTML($body);
//
////  $this->registerLog('LECTURA', self::MODULO . 'Reporte', 'EXITOSO', 'Entró matricular la Seccion Plantel' . $seccion_plantel_id);
//
//            $mpdf->Output($nombre_pdf . '.pdf', 'F');
//        } else
//            throw new CHttpException(404, 'Estimado Usuario, no se ha encontrado el recurso solicitado. Vuelva a la página anterior e intente de nuevo');
//    }

    public function actionObtenerPdf() {



        $estado_id = $this->getRequest('estado');
        $estado_id_decoded = base64_decode($estado_id);

        $buscarReporte = Titulo::model()->buscarReporte($estado_id_decoded);
        $ruta = yii::app()->basePath . '/../public/serial_titulo/';
        $mostrarReporte = $ruta . $buscarReporte;
        //    header("Content-Transfer-Encoding: binary");
        //  header("Content-type: application/octet-stream");
        header("Content-Type: application/pdf");
        header("Content-Disposition: attachment; filename=$buscarReporte");
        // header("Content-Length:");
        readfile($mostrarReporte);
    }

    public function actionReporteCSV() {

        $estado_id = $this->getRequest('estado');
        $estado_id_decoded = base64_decode($estado_id);
        $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_actual_id = $periodo_escolar_actual_id['id'];
        $estadoPK = Estado::model()->findByPk($estado_id_decoded);
        $nombreEstado = $estadoPK['nombre'];
        $zona_educativa = Titulo::model()->obtenerZonaEducativa($estado_id_decoded);
//        var_dump($zona_educativa);
//        die();
        $datosReporte = Titulo::model()->obtenerDatosReporteSerialesPorEstado($estado_id_decoded, $periodo_actual_id, $zona_educativa);
        if ($datosReporte != false) {
            $headers = array('Código Plantel', 'Nombre del Plantel', 'Código del Plan', 'Nacionalidad',
                'Documento identidad', 'Tipo documento de identidad', 'Cédula Escolar', 'Serial', 'Nombres', 'Apellidos', 'Grado', 'Sección', 'Período');

            $colDef = array('cod_plantel' => array(), 'nombre_plantel' => array(), 'codigo_plan' => array(),
                'nacionalidad' => array(), 'documento_identidad' => array(), 'tdocumento_identidad' => array(), 'cedula_escolar' => array(), 'serial' => array(), 'nombres' => array(), 'apellidos' => array(),
                'grado' => array(), 'seccion' => array(), 'periodo' => array());

            $fileName = 'asignación_seriales_en_el_estado-' . $nombreEstado . date('YmdHis') . '.csv';

            CsvExport::export(
                    $datosReporte, // a CActiveRecord array OR any CModel array
                    $colDef, $headers, true, $fileName);
        }
    }

}
