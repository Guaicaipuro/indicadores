<?php

class ConsultarTituloController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Consulta de Título',
        'write' => 'Consulta de Título',
        'label' => 'Consulta de Título'
    );

    const MODULO = "titulo.ConsultarTitulo";

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
//'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'BusquedaEstudianteEgresado', 'dataProviderConsultaTitulo','busqueda', 'busquedaEstudianteEgresadoPorSerial', 'serial'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('guardarLiquidacionSeriales'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        $this->render('index', array(
        ));
    }
    public function actionSerial(){
        $this->render('tituloSerial', array(
        ));
    }

    public function actionBusquedaEstudianteEgresado() {
        if (Yii::app()->request->isAjaxRequest) {
            $sinacoes = false;
            $firmantes = array();
            $tipoBusqueda = $this->getPost('tipoNacionalidad');
            $EstuadianteEgresado = $this->getPost('EstuadianteEgresado');

            if (($tipoBusqueda == 'E' || $tipoBusqueda == 'P' || $tipoBusqueda == 'V') AND (is_numeric($EstuadianteEgresado)) ) {
                if((integer)$EstuadianteEgresado <= 2147483647) {
                    $permitidos = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ";
                    for ($i = 0; $i < strlen($EstuadianteEgresado); $i++) {
                        if (strpos($permitidos, substr($EstuadianteEgresado, $i, 1)) === false) {
                            //  echo $nombres . " no es válido<br>";
                            $resultado = false;
                            break;
                        } else {
                            //echo $nombres . " es válido<br>";
                            //$nombres = $nombres;
                            $resultado = true;
                        }
                    }
                    if ($resultado == false) {
                        $respuesta['statusCode'] = 'alert';
                        $respuesta['mensaje'] = 'Estimado Usuario, solo se permite ingresar letras y números.';
                        echo json_encode($respuesta);
                        Yii::app()->end();
                    }

                    $resultadoBusqueda = Titulo::model()->buscarEstTitulo($tipoBusqueda, $EstuadianteEgresado);
                    if ($resultadoBusqueda) {
                        $estudianteID = $resultadoBusqueda[0]['estudiante_id'];
                        if(array_key_exists('mes_egreso',$resultadoBusqueda[0]) AND array_key_exists('anio_egreso',$resultadoBusqueda[0])){
                            $sinacoes = true;
                            $mes_egreso = $resultadoBusqueda[0]['mes_egreso'];
                            $anio_egreso = $resultadoBusqueda[0]['anio_egreso'];
                            $cod_plantel = $resultadoBusqueda[0]['cod_plantel'];
                            $periodo =  substr($anio_egreso,2,2);
                            $firmantes = Titulo::model()->buscarFirmantesTituloSinacoes($cod_plantel,$periodo,$anio_egreso,$mes_egreso);
                            if (!$firmantes) {
                                $firmantes = array();
                            }
                        }
                        if ($estudianteID) {
                            $EstuadianteEgresado = (int)$EstuadianteEgresado;
                            $compara = false;
                            for ($i = 1; $i < 5; $i++) {
                                $coincidir = $i;
                                if ($i == 4) {
                                    $coincidir = 'P';
                                }

                                $buscarNotas = Titulo::model()->buscarNotaEstudiante($EstuadianteEgresado, $coincidir);


                                if ($buscarNotas) {

                                    $dataConsultarNota[] = $this->dataProviderConsultaNota($buscarNotas);
                                    $compara = TRUE;
                                }


                            }
                            //                        var_dump($dataConsultarNota[3]);
                            //                        die();
                            if ($compara == false) {
                                $dataConsultarNota = array();
                            }
                            //                        var_dump($dataConsultarNota);
                            //                        die();
                            $historialEstudiante = Estudiante::model()->historicoEstudiante($EstuadianteEgresado, '', $estudianteID);
                            if (!$historialEstudiante) {
                                $historialEstudiante = array();
                            }


                            $dataConsultaTitulo = $this->dataProviderConsultaTitulo($resultadoBusqueda);


                            //
                            //                        var_dump($dataConsultarNota);
                            //  var_dump($dataConsultaTitulo);
                            //     die();
                            $this->renderPartial('mostrarResultadoBusqueda', array('dataProvider' => $dataConsultaTitulo,
                                'resultadoBusqueda' => $resultadoBusqueda,
                                'historicoEstudiante' => $historialEstudiante,
                                'dataConsultarNota' => $dataConsultarNota,
                                'firmantes'=>$firmantes,
                                'sinacoes'=>$sinacoes
                                //'plantelPK' => $plantelPK
                            ), FALSE, TRUE);
                            //                $respuesta['statusCode'] = 'alert';
                            //                echo json_encode($respuesta);
                        } else {
                            $respuesta['statusCode'] = 'alert';
                            $respuesta['mensaje'] = 'Ha ocurrido un error por favor reportarlo al administrador del sistema.';
                            echo json_encode($respuesta);
                            Yii::app()->end();
                        }
                    } else {

                        $respuesta['statusCode'] = 'alert';
                        $respuesta['mensaje'] = 'Estimado Usuario, no se encontro al estudiante egresado que esta buscando.';
                        echo json_encode($respuesta);
                        Yii::app()->end();
                    }

                    //            var_dump($resultadoBusqueda);
                    //            die();
                }else {
                    $respuesta['statusCode'] = 'alert';
                    $respuesta['mensaje'] = 'Estimado Usuario, no se encontro al estudiante egresado que esta buscando.';
                    echo json_encode($respuesta);
                    Yii::app()->end();
                }
            } else {
                $respuesta['statusCode'] = 'alert';
                $respuesta['mensaje'] = 'Estimado Usuario,se ha alterado los datos por favor intente de nuevo.';
                echo json_encode($respuesta);
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionBusquedaEstudianteEgresadoPorSerial() {
        if (Yii::app()->request->isAjaxRequest) {
            $sinacoes = false;
            $firmantes = array();
//            $tipoBusqueda = $_POST['tipoNacionalidad'];
//            $tituloPrefijo= $_POST['tituloPrefijo'];
//            $tituloSerial= $_POST['tituloSerial'];
            //$EstuadianteEgresado = $_POST['EstuadianteEgresado'];

            $tipoBusqueda = $this->getPost('tipoNacionalidad');
            $tituloPrefijo= $this->getPost('tituloPrefijo');
            $tituloSerial= $this->getPost('tituloSerial');


            if (($tipoBusqueda == 'E' || $tipoBusqueda == 'P' || $tipoBusqueda == 'V')) {
                $permitidos = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                for ($i = 0; $i < strlen($tituloPrefijo); $i++) {
                    if (strpos($permitidos, substr($tituloPrefijo, $i, 1)) === false) {
                        //  echo $nombres . " no es válido<br>";
                        $resultado = false;
                        break;
                    } else {
                        //echo $nombres . " es válido<br>";
                        //$nombres = $nombres;
                        $resultado = true;
                    }
                }
                if ($resultado == false) {
                    $respuesta['statusCode'] = 'alert';
                    $respuesta['mensaje'] = 'Estimado Usuario, solo se permite ingresar letras y números.';
                    echo json_encode($respuesta);
                    Yii::app()->end();
                }



                $resultadoBusqueda = Titulo::model()->buscarEstTitulo($tipoBusqueda, null, 'serial', $tituloPrefijo, $tituloSerial);


                if ($resultadoBusqueda) {
                    if(array_key_exists('mes_egreso',$resultadoBusqueda[0]) AND array_key_exists('anio_egreso',$resultadoBusqueda[0])){
                        $sinacoes = true;
                        $mes_egreso = $resultadoBusqueda[0]['mes_egreso'];
                        $anio_egreso = $resultadoBusqueda[0]['anio_egreso'];
                        $cod_plantel = $resultadoBusqueda[0]['cod_plantel'];
                        $periodo =  substr($anio_egreso,2,2);
                        $firmantes = Titulo::model()->buscarFirmantesTituloSinacoes($cod_plantel,$periodo,$anio_egreso,$mes_egreso);
                        if (!$firmantes) {
                            $firmantes = array();
                        }
                    }
                    $estudianteID = $resultadoBusqueda[0]['estudiante_id'];
                    $EstuadianteEgresado = $resultadoBusqueda[0]['documento_identidad'];
//                    var_dump($resultadoBusqueda);
//                    die();
                    if ($estudianteID) {
                        $EstuadianteEgresado = (int) $EstuadianteEgresado;
                        $compara = false;
                        for ($i = 1; $i < 5; $i++) {
                            $coincidir = $i;
                            if ($i == 4) {
                                $coincidir = 'P';
                            }

                            $buscarNotas = Titulo::model()->buscarNotaEstudiante($EstuadianteEgresado, $coincidir);


                            if ($buscarNotas) {

                                $dataConsultarNota[] = $this->dataProviderConsultaNota($buscarNotas);
                                $compara = TRUE;
                            }



                        }
//                        var_dump($dataConsultarNota[3]);
//                        die();
                        if ($compara == false) {
                            $dataConsultarNota = array();
                        }
//                        var_dump($dataConsultarNota);
//                        die();
                        $historialEstudiante = Estudiante::model()->historicoEstudiante($EstuadianteEgresado,'', $estudianteID);
                        if (!$historialEstudiante) {
                            $historialEstudiante = array();
                        }

                        $dataConsultaTitulo = $this->dataProviderConsultaTitulo($resultadoBusqueda);


//
//                        var_dump($dataConsultarNota);
                        //  var_dump($dataConsultaTitulo);
                        //     die();
                        $this->renderPartial('mostrarResultadoBusqueda', array('dataProvider' => $dataConsultaTitulo,
                            'resultadoBusqueda' => $resultadoBusqueda,
                            'historicoEstudiante' => $historialEstudiante,
                            'dataConsultarNota' => $dataConsultarNota,
                            'firmantes'=>$firmantes,
                            'sinacoes'=>$sinacoes
                            //'plantelPK' => $plantelPK
                        ), FALSE, TRUE);
//                $respuesta['statusCode'] = 'alert';
//                echo json_encode($respuesta);
                    } else {
                        $respuesta['statusCode'] = 'alert';
                        $respuesta['mensaje'] = 'Ha ocurrido un error por favor reportarlo al administrador del sistema.';
                        echo json_encode($respuesta);
                        Yii::app()->end();
                    }
                } else {

                    $respuesta['statusCode'] = 'alert';
                    $respuesta['mensaje'] = 'Estimado Usuario, no se encontro al estudiante egresado que esta buscando.';
                    echo json_encode($respuesta);
                    Yii::app()->end();
                }

//            var_dump($resultadoBusqueda);
//            die();
            } else {
                $respuesta['statusCode'] = 'alert';
                $respuesta['mensaje'] = 'Estimado Usuario,se ha alterado los datos por favor intente de nuevo.';
                echo json_encode($respuesta);
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionBusqueda() {
        if ($this->hasQuery('cod') AND $this->hasQuery('id')) {
            $tipoBusqueda = base64_decode($this->getQuery('cod'));
            $EstuadianteEgresado = base64_decode($this->getQuery('id'));
            $this->breadcrumbs=array(
                'Consultar Titulo' => array('/titulo/consultarTitulo/'),
                'Busqueda',
            );
            if (($tipoBusqueda == 'E' || $tipoBusqueda == 'P' || $tipoBusqueda == 'V')) {
                $permitidos = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ";
                for ($i = 0; $i < strlen($EstuadianteEgresado); $i++) {
                    if (strpos($permitidos, substr($EstuadianteEgresado, $i, 1)) === false) {
                        //  echo $nombres . " no es válido<br>";
                        $resultado = false;
                        break;
                    } else {
                        //echo $nombres . " es válido<br>";
                        //$nombres = $nombres;
                        $resultado = true;
                    }
                }
                if ($resultado == false) {
                    $respuesta['statusCode'] = 'alert';
                    $respuesta['mensaje'] = 'Estimado Usuario, solo se permite ingresar letras y números.';
                    echo json_encode($respuesta);
                    Yii::app()->end();
                }

                $resultadoBusqueda = Titulo::model()->buscarEstTitulo($tipoBusqueda, $EstuadianteEgresado);

                if ($resultadoBusqueda) {
                    $estudianteID = $resultadoBusqueda[0]['estudiante_id'];
                    if ($estudianteID) {
                        $EstuadianteEgresado = (int) $EstuadianteEgresado;
                        $compara = false;
                        for ($i = 1; $i < 5; $i++) {
                            $coincidir = $i;
                            if ($i == 4) {
                                $coincidir = 'P';
                            }

                            $buscarNotas = Titulo::model()->buscarNotaEstudiante($EstuadianteEgresado, $coincidir);


                            if ($buscarNotas) {

                                $dataConsultarNota[] = $this->dataProviderConsultaNota($buscarNotas);
                                $compara = TRUE;
                            }



                        }
//                        var_dump($dataConsultarNota[3]);
//                        die();
                        if ($compara == false) {
                            $dataConsultarNota = array();
                        }
//                        var_dump($dataConsultarNota);
//                        die();
                        $historialEstudiante = Estudiante::model()->historicoEstudiante($EstuadianteEgresado,'', $estudianteID);
                        if (!$historialEstudiante) {
                            $historialEstudiante = array();
                        }

                        $dataConsultaTitulo = $this->dataProviderConsultaTitulo($resultadoBusqueda);


//
//                        var_dump($dataConsultarNota);
//                        var_dump($dataConsultaTitulo);
                        // die();
                        $this->render('mostrarResultadoBusqueda', array(
                            'dataProvider' => $dataConsultaTitulo,
                            'resultadoBusqueda' => $resultadoBusqueda,
                            'historicoEstudiante' => $historialEstudiante,
                            'dataConsultarNota' => $dataConsultarNota,
                            //'plantelPK' => $plantelPK
                        ), FALSE, TRUE);
//                $respuesta['statusCode'] = 'alert';
//                echo json_encode($respuesta);
                    } else {
                        $class = 'errorDialogBox';
                        $message = 'Ha ocurrido un error por favor reportarlo al administrador del sistema.';
                        $this->render('//msgBox', array(
                            'class' => $class,
                            'message' => $message,
                        ));
                        Yii::app()->end();
                    }
                } else {
                    $class = 'errorDialogBox';
                    $message = 'Estimado Usuario, no se encontro al estudiante egresado que esta buscando.';
                    $this->render('//msgBox', array(
                        'class' => $class,
                        'message' => $message,
                    ));
                    Yii::app()->end();
                }

//            var_dump($resultadoBusqueda);
//            die();
            } else {
                $class = 'alertDialogBox';
                $message = 'Estimado Usuario,se ha alterado los datos por favor intente de nuevo.';
                $this->render('//msgBox', array(
                    'class' => $class,
                    'message' => $message,
                ));
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    function dataProviderConsultaTitulo($resultadoBusqueda) {
        foreach ($resultadoBusqueda as $key => $value) {
            $cedula_identidad = $value['documento_identidad'];
            $nombres = $value['nombres'];
            $apellidos = $value['apellidos'];
            $plantel = $value['nombreplantel'];
            $plan_nombre = $value['nombre_plan'];
            $nombre_mencion = $value['nombre_mencion'];
            $cod_estadistico = $value['cod_estadistico'];
            $serial = $value['serial'];
            // $anoEgreso = $value['ano_egreso'];
            (isset($value['anio_egreso']) && $value['anio_egreso'] != null && $value['anio_egreso'] != '')? $anoEgreso = $value['anio_egreso']:$anoEgreso = 'NO POSEE';
            // (array_key_exists('ano_egreso',$value) || isset($value['ano_egreso']))?$anoEgreso = $value['ano_egreso']:$anoEgreso = null; //Comente esto *Marisela*

            $fechaOtorgacion = $value['fecha_otorgamiento'];
            if($fechaOtorgacion!=null){
                $fechaOtorgacion = date("d-m-Y", strtotime($fechaOtorgacion));
            }else{
                $fechaOtorgacion='NO POSEE';
            }

            $plantelCode_estadi = $plantel . ' ' . $cod_estadistico;
            $nombreApellido = $nombres . ' ' . $apellidos;
//            $botones = "<div class='center'>" . CHtml::checkBox('EstSolicitud[]', "false", array('value' => base64_encode($value['id']),
////                   'onClick' => "Estudiante('')",
//                        "title" => "solicitud")
//                    ) .
//                    "</div>";
//            $columna .= CHtml::checkBox('repitiente[]', false, array('id' => 'repitiente[]', 'title' => 'Repitente')) . '&nbsp;&nbsp;&nbsp;';
            $rawData[] = array(
                'id' => $key,
                'cedula_identidad' => '<center>' . $cedula_identidad . '</center>',
                'nombreApellido' => '<center>' . $nombreApellido . '</center>',
                'plan_nombre' => '<center>' . $plan_nombre . '</center>',
                'nombre_mencion' => "<center>" . $nombre_mencion . '</center>',
                'serial' => "<center>" . $serial . "</center>",
                'plantelCode_estadi' => "<center>" . $plantelCode_estadi . '</center>',
                'anoEgreso' => "<center>" . $anoEgreso . '</center>',
                'fechaOtorgacion' => "<center>" . $fechaOtorgacion . '</center>'
                //  'botones' => '<center>' . $botones . '</center>'
            );
        }
        // var_dump($rawData);
        //  die();
        return new CArrayDataProvider($rawData, array(
                'pagination' => false,
                //    'pagination' => array(
                //      'pageSize' => 5,
                //),
            )
        );
    }
    function dataProviderConsultaNota($buscarNotas) {
//        var_dump($buscarNotas);
//        die();
        foreach ($buscarNotas as $key => $value) {
//            var_dump($buscarNotas['materia']);
//            die();
            $materia = $value['materia'];
            $mencion = $value['mencion'];
            $nota = $value['nota'];
//            $botones = "<div class='center'>" . CHtml::checkBox('EstSolicitud[]', "false", array('value' => base64_encode($value['id']),
////                   'onClick' => "Estudiante('')",
//                        "title" => "solicitud")
//                    ) .
//                    "</div>";
//            $columna .= CHtml::checkBox('repitiente[]', false, array('id' => 'repitiente[]', 'title' => 'Repitente')) . '&nbsp;&nbsp;&nbsp;';
            $rawData[] = array(
                'id' => $key,
                'materia' => '<center>' . $materia . '</center>',
                'mencion' => '<center>' . $mencion . '</center>',
                'nota' => '<center>' . $nota . '</center>',
                //  'botones' => '<center>' . $botones . '</center>'
            );
        }

        //  die();
//        var_dump($rawData);
//        die();
        return new CArrayDataProvider($rawData, array(
                'pagination' => false,
                //    'pagination' => array(
                //      'pageSize' => 5,
                //),
            )
        );
    }

}
