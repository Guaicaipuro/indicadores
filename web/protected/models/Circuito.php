<?php

/**
 * This is the model class for table "gplantel.circuito".
 *
 * The followings are the available columns in table 'gplantel.circuito':
 * @property integer $id
 * @property string $nombre_circuito
 * @property integer $estado_id
 * @property integer $municipio_id
 * @property integer $parroquia_id
 * @property string $tipo_circuito
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property Parroquia $parroquia
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 * @property Municipio $municipio
 * @property Estado $estado
 * @property CircuitoPlantel[] $circuitoPlantels
 */
class Circuito extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gplantel.circuito';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('nombre_circuito, estado_id, municipio_id, tipo_circuito', 'required'),
			array('estado_id, municipio_id, parroquia_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			
			array('nombre_circuito', 'length', 'max'=>30),
			array('tipo_circuito, estatus', 'length', 'max'=>1),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre_circuito, estado_id, municipio_id, parroquia_id, tipo_circuito, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
			
			array('nombre_circuito','unique','message'=>'Nombre de Circuito ya Existe, Escriba Otro'),

			//AGREGADO POR JONATHAN HUAMAN FECHA 01-06-2015//
			array('nombre_circuito,estado_id,tipo_circuito,municipio_id','required','on'=>'registroEdicionCircuito'),
			array('tipo_circuito','verificarParroquia', 'on' => 'registroEdicionCircuito'),
			array('nombre_circuito','unique','on'=>'registroEdicionCircuito'),
			//FIN DE AGREGACION//
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parroquia' => array(self::BELONGS_TO, 'Parroquia', 'parroquia_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'municipio' => array(self::BELONGS_TO, 'Municipio', 'municipio_id'),
			'estado' => array(self::BELONGS_TO, 'Estado', 'estado_id'),
			'circuitoPlantels' => array(self::HAS_MANY, 'CircuitoPlantel', 'circuito_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre_circuito' => 'Nombre Circuito',
			'estado_id' => 'Estado',
			'municipio_id' => 'Municipio',
			'parroquia_id' => 'Parroquia',
			'tipo_circuito' => 'Tipo Circuito',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		//if(strlen($this->nombre_circuito)>0) $criteria->compare('nombre_circuito',$this->nombre_circuito,true);
		if(strlen($this->nombre_circuito)>0) $criteria->addSearchCondition('t.nombre_circuito', '%' . $this->nombre_circuito . '%', false, 'AND', 'ILIKE');
		if(is_numeric($this->estado_id)) $criteria->compare('estado_id',$this->estado_id);
		if(is_numeric($this->municipio_id)) $criteria->compare('municipio_id',$this->municipio_id);
		if(is_numeric($this->parroquia_id)) $criteria->compare('parroquia_id',$this->parroquia_id);
		//if(strlen($this->tipo_circuito)>0) $criteria->compare('tipo_circuito',$this->tipo_circuito,true);
		if(in_array($this->tipo_circuito, array('M', 'P'))) $criteria->compare('tipo_circuito',$this->tipo_circuito,true);

		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(Utiles::isValidDate($this->fecha_elim, 'y-m-d')) $criteria->compare('fecha_elim',$this->fecha_elim);
		// if(strlen($this->fecha_elim)>0) $criteria->compare('fecha_elim',$this->fecha_elim,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);

		$criteria->join  = " LEFT JOIN public.estado e ON e.id = t.estado_id";
		$criteria->order = ' e.nombre';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Circuito the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function verificarParroquia($attributo,$params = null) {
        if ( ($this->$attributo != null OR $this->$attributo != '') )
        {
            if( ($this->parroquia_id==null OR $this->parroquia_id=='') AND $this->tipo_circuito=='P' )
                $this->addError('parroquia_id', 'Parroquia no puede ser nulo');
        }
    }

    public function reporteCircuitoGeneral()
    {

    	$sql = "SELECT 
					c.id, 
					c.nombre_circuito, 
					e.nombre AS estado, 
					m.nombre AS municipio,
					p.nombre AS parroquia,
                                        pl.cod_plantel as cod_plantel,
					pl.nombre AS plantel,
					(CASE WHEN cp.plantel_integral='S' THEN 'Integral' WHEN cp.plantel_integral='N' THEN 'Regular' END) AS tipo_plantel,
					(CASE WHEN c.tipo_circuito='M' THEN 'Municipal' WHEN c.tipo_circuito='P' THEN 'Parroquial' END) AS tipo_circuito,
                                        u.cedula as cedula_director,
					(CASE WHEN pl.director_actual_id is not null THEN  u.nombre ||' ' || u.apellido ELSE 'NO POSEE' END) AS nombre_director ,
					(CASE 
						WHEN u.telefono IS NULL OR u.telefono ='0' OR u.telefono ='' 
							THEN (CASE WHEN u.telefono_celular IS NULL OR u.telefono_celular ='0' OR u.telefono_celular =''
								THEN  'NO POSEE' ELSE u.telefono_celular END)
							ELSE
								(CASE WHEN u.telefono_celular IS NULL OR u.telefono_celular ='0' OR u.telefono_celular =''
									THEN u.telefono ELSE u.telefono || '/' || u.telefono_celular END) 
					END) AS telefono_director,
					
					(CASE WHEN pl.director_actual_id is not null THEN lower(u.email)  ELSE 'NO POSEE' END) AS correo_director
				FROM
					gplantel.circuito AS c 
					INNER JOIN public.estado AS e ON c.estado_id = e.id
					INNER JOIN public.municipio AS m ON c.municipio_id = m.id
					INNER JOIN gplantel.circuito_plantel AS cp ON cp.circuito_id = c.id
					INNER JOIN gplantel.plantel AS pl ON cp.plantel_id = pl.id
					LEFT JOIN seguridad.usergroups_user AS u ON pl.director_actual_id = u.id 
					LEFT JOIN public.parroquia AS p ON c.parroquia_id = p.id
				WHERE
					c.estatus = 'A'";
        
        if (in_array(Yii::app()->user->group, array(UserGroups::JEFE_ZONA)))
		{
			$estado_id = Yii::app()->user->estado;
			$sql .= " AND e.id = :estado_id ";
		}
		$sql.="ORDER BY e.nombre, c.nombre_circuito, m.nombre;";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        
        if (in_array(Yii::app()->user->group, array(UserGroups::JEFE_ZONA)))
		{
			$command->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
		}        
        $resultado = $command->queryAll();

        return $resultado;
    }


    public function reporteCircuitoIndividual()
    {
    	$id = $this->id;
    	$sql = "SELECT 
					c.id, 
					c.nombre_circuito, 
					e.nombre AS estado, 
					m.nombre AS municipio,
					p.nombre AS parroquia,
                                        pl.cod_plantel as cod_plantel,
					pl.nombre AS plantel,
					(CASE WHEN cp.plantel_integral='S' THEN 'Integral' WHEN cp.plantel_integral='N' THEN 'Regular' END) AS tipo_plantel,
					(CASE WHEN c.tipo_circuito='M' THEN 'Municipal' WHEN c.tipo_circuito='P' THEN 'Parroquial' END) AS tipo_circuito,
                                        u.cedula as cedula_director,
					(CASE WHEN pl.director_actual_id is not null THEN  u.nombre ||' ' || u.apellido ELSE 'NO POSEE' END) AS nombre_director ,
					(CASE 
						WHEN u.telefono IS NULL OR u.telefono ='0' OR u.telefono ='' 
							THEN (CASE WHEN u.telefono_celular IS NULL OR u.telefono_celular ='0' OR u.telefono_celular =''
								THEN  'NO POSEE' ELSE u.telefono_celular END)
							ELSE
								(CASE WHEN u.telefono_celular IS NULL OR u.telefono_celular ='0' OR u.telefono_celular =''
									THEN u.telefono ELSE u.telefono || '/' || u.telefono_celular END) 
					END) AS telefono_director,
					(CASE WHEN pl.director_actual_id is not null THEN lower(u.email)  ELSE 'NO POSEE' END) AS correo_director
				FROM
					gplantel.circuito AS c 
					INNER JOIN public.estado AS e ON c.estado_id = e.id
					INNER JOIN public.municipio AS m ON c.municipio_id = m.id
					INNER JOIN gplantel.circuito_plantel AS cp ON cp.circuito_id = c.id
					INNER JOIN gplantel.plantel AS pl ON cp.plantel_id = pl.id
					LEFT JOIN seguridad.usergroups_user AS u ON pl.director_actual_id = u.id 
					LEFT JOIN public.parroquia AS p ON c.parroquia_id = p.id
				WHERE
					c.estatus = 'A'
					AND c.id = :id
				ORDER BY cp.plantel_integral DESC;";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $command->bindParam(":id", $id, PDO::PARAM_INT);
        $resultado = $command->queryAll();

        return $resultado;
    }




    public function nuevoRegistroMapa($nuevo_id,$nombre_estado,$nombre_municipio,$capital_estado,$enlace,$id_app,$tipo_circuito)
    {
        $sql = "INSERT INTO circuitos_escolares_municipios(
            id_gid, id_mun, cod_muni, estado, municipio, capital, tabla_html,
            the_geom)
    VALUES (:nuevo_id,:nombre_estado,:nombre_municipio,:capital_estado,:enlace);";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $command->bindParam(":nuevo_id", $nuevo_id, PDO::PARAM_INT);
        $command->bindParam(":nombre_estado", $nombre_estado, PDO::PARAM_STR);
        $command->bindParam(":nombre_municipio", $nombre_municipio, PDO::PARAM_STR);
        $command->bindParam(":capital_estado", $capital_estado, PDO::PARAM_STR);
        $command->bindParam(":enlace", $enlace, PDO::PARAM_STR);
        $command->bindParam(":id_app", $id_app, PDO::PARAM_INT);

        $resultado = $command->queryAll();

        return $resultado;
    }

    public function verUltimoId($tipo_circuito)

    {
        if($tipo_circuito==1){
            $sql = " SELECT max(id_gid) FROM legacy.circuitos_escolares_municipios";

        }else{
            $sql = " SELECT max(id_gid) FROM legacy.circuitos_escolares_parroquias";
        }
        //$sql = " SELECT max(id_gid) FROM legacy.circuitos_escolares_municipios";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $command->bindParam(":id", $id, PDO::PARAM_INT);
        $resultado = $command->queryAll();

        return $resultado;


}
}
