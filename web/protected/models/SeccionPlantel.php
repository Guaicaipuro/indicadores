<?php

/**
 * This is the model class for table "gplantel.seccion_plantel".
 *
 * The followings are the available columns in table 'gplantel.seccion_plantel':
 * @property integer $id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $plantel_id
 * @property integer $grado_id
 * @property integer $plan_id
 * @property integer $capacidad
 * @property integer $turno_id
 * @property integer $seccion_id
 * @property integer $nivel_id
 *  @property integer $modalidad_id
 *
 * The followings are the available model relations:
 * @property SeccionPlantelPeriodo[] $seccionPlantelPeriodos
 * @property Nivel $nivel
 * @property Plan $plan
 * @property Grado $grado
 * @property Plantel $plantel
 * @property Turno $turno
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property SeccionPlantel $seccion
 * @property SeccionPlantel[] $seccionPlantels
 */
class SeccionPlantel extends CActiveRecord {

    public $estatus_asig_doc;
    public $estatus_seccion_p;

    CONST INDICE_SECCION_PLANTEL = 'INDICE_SECCION_PLANTEL:';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gplantel.seccion_plantel';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('seccion_id, grado_id, plan_id, nivel_id, capacidad, turno_id, modalidad_id', 'required', 'message' => 'El campo {attribute} no debe estar vacio'),
            array('plantel_id, grado_id, plan_id, capacidad, turno_id, seccion_id, nivel_id, modalidad_id', 'numerical', 'integerOnly' => true),
            //  array('fecha_ini, fecha_act, fecha_elim', 'length', 'max' => 6),
            array('estatus', 'length', 'max' => 1),
            array('capacidad', 'length', 'max' => 3),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus, plantel_id, grado_id, plan_id, capacidad, turno_id, seccion_id, nivel_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'seccionPlantelPeriodos' => array(self::HAS_MANY, 'SeccionPlantelPeriodo', 'seccion_plantel_id'),
            'nivel' => array(self::BELONGS_TO, 'Nivel', 'nivel_id'),
            'plan' => array(self::BELONGS_TO, 'Plan', 'plan_id'),
            'grado' => array(self::BELONGS_TO, 'Grado', 'grado_id'),
            'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
            'turno' => array(self::BELONGS_TO, 'Turno', 'turno_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'seccion' => array(self::BELONGS_TO, 'Seccion', 'seccion_id'),
            'cierreAsignacionDocentes' => array(self::HAS_MANY, 'CierreAsignacionDocente', 'seccion_plantel_id'),
            'modalidad' => array(self::BELONGS_TO, 'Modalidad', 'modalidad_id'),
                //'seccionPlantels' => array(self::HAS_MANY, 'SeccionPlantel', 'seccion_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'plantel_id' => 'Plantel',
            'grado_id' => 'Grado',
            'plan_id' => 'Plan',
            'modalidad_id' => 'Modalidad',
            'capacidad' => 'Capacidad',
            'turno_id' => 'Turno',
            'seccion_id' => 'Sección',
            'nivel_id' => 'Nivel',
        );
    }

    public function validarSeccion($seccion_id) {
        $estatus = 'A';
        $sql = "SELECT id
                    FROM gplantel.seccion
                    WHERE id=:seccion_id AND estatus=:estatus";
        $commandBusqueda = $this->getDbConnection()->createCommand($sql);
        $commandBusqueda->bindParam(':seccion_id', $seccion_id, PDO::PARAM_INT);
        $commandBusqueda->bindParam(':estatus', $estatus, PDO::PARAM_STR);
        $result = $commandBusqueda->execute(); //queryScalar devuelve el nombre si lo consigue si no lo encuentra devuelve false
        //  var_dump($result); die();
        return $result;
    }

    public function validarNivelPlanGrado($plantel_id, $nivel_id, $plan_id, $grado_id) {

        $estatus = 'A';
        $sql = "SELECT distinct nplantel.nivel_id, npl.plan_id, pag.grado_id"
                . " FROM "
                . " gplantel.nivel_plantel nplantel"
                . " INNER JOIN gplantel.nivel_plan npl on (nplantel.nivel_id = npl.nivel_id)"
                . " INNER JOIN gplantel.plantel p on (p.id = nplantel.plantel_id)"
                . " INNER JOIN gplantel.nivel n on (nplantel.nivel_id = n.id AND npl.nivel_id = n.id)"
                . " INNER JOIN gplantel.plan plan on (npl.plan_id = plan.id)"
                . " INNER JOIN gplantel.planes_grados_asignaturas pag on (pag.plan_id = plan.id)"
                . " INNER JOIN gplantel.grado g on (pag.grado_id = g.id)"
                . " WHERE nplantel.plantel_id = :plantel_id"
                // . " AND nplantel.nivel_id= '3'"
                . " AND n.id=:nivel_id"
                . " AND n.estatus=:estatus"
                // . " AND npl.nivel_id='3'"
                . " AND plan.id=:plan_id"
                . " AND plan.estatus=:estatus"
                // . " AND npl.plan_id= '176'"
                // . " AND pag.plan_id = '176'"
                . " AND g.estatus=:estatus"
                . " AND g.id=:grado_id";
        $commandBusqueda = $this->getDbConnection()->createCommand($sql);
        $commandBusqueda->bindParam(':plantel_id', $plantel_id, PDO::PARAM_INT);
        $commandBusqueda->bindParam(':nivel_id', $nivel_id, PDO::PARAM_INT);
        $commandBusqueda->bindParam(':plan_id', $plan_id, PDO::PARAM_INT);
        $commandBusqueda->bindParam(':grado_id', $grado_id, PDO::PARAM_STR);
        $commandBusqueda->bindParam(':estatus', $estatus, PDO::PARAM_INT);
        $result = $commandBusqueda->execute(); //execute devuelve el 1 si lo consigue si no lo encuentra devuelve 0
        // var_dump($result);
        //die();
        return $result;
    }

    public function validarTurno($turno_id) {
        $estatus = 'A';
        $sql = "SELECT id
                    FROM gplantel.turno
                    WHERE id=:turno_id AND estatus=:estatus";
        $commandBusqueda = $this->getDbConnection()->createCommand($sql);
        $commandBusqueda->bindParam(':turno_id', $turno_id, PDO::PARAM_INT);
        $commandBusqueda->bindParam(':estatus', $estatus, PDO::PARAM_STR);
        $result = $commandBusqueda->execute(); //queryScalar devuelve el nombre si lo consigue si no lo encuentra devuelve false
        //   var_dump($result); die();
        return $result;
    }

    public function guardarSeccion($secciones) {

        $usuario_id = Yii::app()->user->id;
        $estatus = 'A';
        foreach ($secciones as $key => $value) {
            $sql = "INSERT INTO gplantel.seccion_plantel
                (plantel_id, usuario_ini_id,grado_id,turno_id, estatus, seccion_id, capacidad, nivel_id, plan_id)
                VALUES (:plantel_id, :usuario_ini_id, :grado_id, :turno_id, :estatus, :seccion_id, :capacidad, :nivel_id, :plan_id) returning id";
            $guard = Yii::app()->db->createCommand($sql);

            $guard->bindParam(":plantel_id", $value['plantel_id'], PDO::PARAM_INT);
            $guard->bindParam(":usuario_ini_id", $usuario_id, PDO::PARAM_INT);
            $guard->bindParam(":grado_id", $value['grado_id'], PDO::PARAM_INT);
            $guard->bindParam(":turno_id", $value['turno_id'], PDO::PARAM_INT);
            $guard->bindParam(":estatus", $estatus, PDO::PARAM_STR);
            $guard->bindParam(":seccion_id", $value['seccion_id'], PDO::PARAM_INT);
            $guard->bindParam(":capacidad", $value['capacidad'], PDO::PARAM_INT);
            $guard->bindParam(":nivel_id", $value['nivel_id'], PDO::PARAM_INT);
            $guard->bindParam(":plan_id", $value['plan_id'], PDO::PARAM_INT);
            $resulatadoGuardo = $guard->execute(); //devuelve 1 cuando guarda
        }
        return $resulatadoGuardo;
    }

    public function actualizoSeccion($secciones) {
        $usuario_id = Yii::app()->user->id;
        $estatus = 'A';
        $fecha = date('Y-m-d H:i:s');

        foreach ($secciones as $key => $value) {
            $sql = "UPDATE gplantel.seccion_plantel
                    SET plantel_id=:plantel_id, nivel_id=:nivel_id, plan_id=:plan_id, usuario_act_id=:usuario_act_id, grado_id=:grado_id, turno_id=:turno_id, estatus=:estatus, seccion_id=:seccion_id, capacidad=:capacidad, fecha_act=:fecha_act
                    WHERE id=:id ";
            $guard = Yii::app()->db->createCommand($sql);

            $guard->bindParam(":plantel_id", $value['plantel_id'], PDO::PARAM_INT);
            $guard->bindParam(":nivel_id", $value['nivel_id'], PDO::PARAM_INT);
            $guard->bindParam(":plan_id", $value['plan_id'], PDO::PARAM_INT);
            $guard->bindParam(":usuario_act_id", $usuario_id, PDO::PARAM_INT);
            $guard->bindParam(":grado_id", $value['grado_id'], PDO::PARAM_INT);
            $guard->bindParam(":turno_id", $value['turno_id'], PDO::PARAM_INT);
            $guard->bindParam(":estatus", $estatus, PDO::PARAM_STR);
            $guard->bindParam(":seccion_id", $value['seccion_id'], PDO::PARAM_INT);
            $guard->bindParam(":capacidad", $value['capacidad'], PDO::PARAM_INT);
            $guard->bindParam(":fecha_act", $fecha, PDO::PARAM_INT);
            $guard->bindParam(":id", $value['id'], PDO::PARAM_INT);
            $resulatadoGuardo = $guard->execute(); //devuelve 1 cuando guarda
        }
        return $resulatadoGuardo;
    }

    public function eliminarSeccion($seccionId, $periodoId) {
//        var_dump($seccionId);
//        var_dump($periodoId);
        $usuario_id = Yii::app()->user->id;
        $estatus = 'E';
        $fecha = date('Y-m-d H:i:s');

        $sql = "UPDATE gplantel.seccion_plantel_periodo
                    SET estatus=:estatus, fecha_elim=:fecha_elim, usuario_act_id=:usuario_act_id
                    WHERE periodo_id=:periodoid AND seccion_plantel_id=:id";
        $guard = Yii::app()->db->createCommand($sql);

        $guard->bindParam(":usuario_act_id", $usuario_id, PDO::PARAM_INT);
        $guard->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $guard->bindParam(":fecha_elim", $fecha, PDO::PARAM_INT);
        $guard->bindParam(":id", $seccionId, PDO::PARAM_INT);
        $guard->bindParam(":periodoid", $periodoId, PDO::PARAM_INT);
        $resultadoGuardo = $guard->execute(); //devuelve 1 cuando guarda

        return $resultadoGuardo;
    }

    public function validacionSeccion($secciones) {

        $estatus = 'A';

        if ($secciones != array()) {
            // var_dump($secciones);
            foreach ($secciones as $key => $value) {
                $sql = "SELECT *
                    FROM gplantel.seccion_plantel
                    WHERE plantel_id=:plantel_id AND grado_id=:grado_id AND turno_id=:turno_id AND estatus=:estatus AND seccion_id=:seccion_id AND nivel_id=:nivel_id AND plan_id=:plan_id";
                $guard = Yii::app()->db->createCommand($sql);

                $guard->bindParam(":plantel_id", $value['plantel_id'], PDO::PARAM_INT);
                $guard->bindParam(":grado_id", $value['grado_id'], PDO::PARAM_INT);
                $guard->bindParam(":turno_id", $value['turno_id'], PDO::PARAM_INT);
                $guard->bindParam(":nivel_id", $value['nivel_id'], PDO::PARAM_INT);
                $guard->bindParam(":plan_id", $value['plan_id'], PDO::PARAM_INT);
                $guard->bindParam(":estatus", $estatus, PDO::PARAM_STR);
                $guard->bindParam(":seccion_id", $value['seccion_id'], PDO::PARAM_INT);
                $resulatadoGuardo = $guard->queryAll(); //devuelve un array con los id de los registros que existen, sino devuelve un array vacio.
                //  var_dump($resulatadoGuardo);
            }
            //  echo $sql;

            return $resulatadoGuardo;
        }
    }

    public function validacionUnicoGradoSeccion($secciones) {
        // var_dump($secciones);
        $estatus = 'A';
        foreach ($secciones as $key => $value) {
            $sql = "SELECT *
                    FROM gplantel.seccion_plantel
                    WHERE plantel_id=:plantel_id AND nivel_id=:nivel_id AND plan_id=:plan_id AND grado_id=:grado_id AND estatus=:estatus AND seccion_id=:seccion_id";
            $guard = Yii::app()->db->createCommand($sql);

            $guard->bindParam(":plantel_id", $value['plantel_id'], PDO::PARAM_INT);
            $guard->bindParam(":nivel_id", $value['nivel_id'], PDO::PARAM_INT);
            $guard->bindParam(":plan_id", $value['plan_id'], PDO::PARAM_INT);
            $guard->bindParam(":grado_id", $value['grado_id'], PDO::PARAM_INT);
            $guard->bindParam(":estatus", $estatus, PDO::PARAM_STR);
            $guard->bindParam(":seccion_id", $value['seccion_id'], PDO::PARAM_INT);
            $resulatadoGuardo = $guard->queryAll(); //devuelve un array con los id de los registros que existen, sino devuelve un array vacio.
            //  var_dump($resulatadoGuardo);
        }
        //var_dump($resulatadoGuardo);
        return $resulatadoGuardo;
    }

    public function validacionUniqueGradoSeccionTurno($secciones) {

        $estatus = 'A';
        foreach ($secciones as $key => $value) {
            $sql = "SELECT id
                    FROM gplantel.seccion_plantel
                    WHERE plantel_id=:plantel_id  AND grado_id=:grado_id AND turno_id=:turno_id AND estatus=:estatus AND seccion_id=:seccion_id ANd plan_id= :plan_id";
            $guard = Yii::app()->db->createCommand($sql);

            $guard->bindParam(":plantel_id", $value['plantel_id'], PDO::PARAM_INT);
            $guard->bindParam(":grado_id", $value['grado_id'], PDO::PARAM_INT);
            $guard->bindParam(":turno_id", $value['turno_id'], PDO::PARAM_INT);
            $guard->bindParam(":estatus", $estatus, PDO::PARAM_STR);
            $guard->bindParam(":seccion_id", $value['seccion_id'], PDO::PARAM_INT);
            $guard->bindParam(":plan_id", $value['plan_id'], PDO::PARAM_INT);
            $resulatadoGuardo = $guard->queryAll(); //devuelve un array con los id de los registros que existen, sino devuelve un array vacio.
            //  var_dump($resulatadoGuardo);
        }
        //var_dump($resulatadoGuardo);
        return $resulatadoGuardo;
    }

    /* Ignacio */

    public function obtenerDatosSeccion($seccion, $plantel_id, $periodo_id = 14) {
        $sql = "SELECT s.nombre seccion, g.nombre grado, g.id, p.nombre plan_estudio, p.cod_plan,sp.nivel_id,pl.cod_plantel,pl.modalidad_id,sp.grado_id,"
                . "(select count(ie.id) FROM matricula.inscripcion_estudiante ie "
                . " INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.id = ie.seccion_plantel_periodo_id)"
                . " INNER JOIN gplantel.seccion_plantel spi on (spi.id = spp.seccion_plantel_id)"
                . " WHERE spi.id =:seccion_plantel_id AND spi.plantel_id =:plantel_id  AND ie.periodo_id = :periodo_id AND ie.estatus = 'A')AS cant_estudiantes"
                . " FROM gplantel.seccion_plantel sp"
                . " INNER JOIN gplantel.seccion s ON (s.id = sp.seccion_id)"
                . " INNER JOIN gplantel.grado g ON (g.id = sp.grado_id)"
                . " INNER JOIN gplantel.plan p ON (p.id = sp.plan_id)"
                . " INNER JOIN gplantel.plantel pl ON (pl.id = sp.plantel_id)"
                . " WHERE sp.plantel_id = :plantel_id AND sp.id = :seccion_plantel_id";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":seccion_plantel_id", $seccion, PDO::PARAM_INT);
        $busqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
        $busqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_STR);
        $resulatadoBusqueda = $busqueda->queryRow();
        if ($resulatadoBusqueda !== array())
            return $resulatadoBusqueda;
        else
            return false;
    }

    public function obtenerSeccion($plantel_id, $periodo_id) {
        $resultado = '';
        $sql = "SELECT COUNT(DISTINCT ie.estudiante_id) as estudiantes,
gr.nombre as grado,p.nombre as plan,c.nombre as credencial,m.nombre as mencion
FROM gplantel.seccion_plantel sp
INNER JOIN gplantel.seccion_plantel_periodo spp
ON sp.id=spp.seccion_plantel_id
INNER JOIN gplantel.nivel n ON n.id=sp.nivel_id
INNER JOIN gplantel.grado gr on gr.id=sp.grado_id
INNER JOIN gplantel.plan p ON (p.id=sp.plan_id)
LEFT JOIN matricula.inscripcion_estudiante ie on (ie.seccion_plantel_periodo_id=spp.id)
LEFT JOIN gplantel.mencion m ON (m.id=p.mencion_id)
LEFT JOIN gplantel.credencial c ON (c.id=p.credencial_id)
WHERE sp.plantel_id=7627 AND spp.periodo_id=14
GROUP BY gr.nombre,n.nombre,p.nombre,c.nombre,m.nombre
ORDER BY gr.nombre,n.nombre,p.nombre,c.nombre,m.nombre";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam('plantel_id', $plantel_id, PDO::PARAM_INT);
        $consulta->bindParam('periodo_id', $periodo_id, PDO::PARAM_INT);
        $resultado = $consulta->queryAll();
        return $resultado;
    }

    /**
     * Obtener datos de Secion Plantel
     * @param $periodoId
     * @param $plantelId
     * @return mixed
     */
    public function obtenerSeccionPlantel($periodoId,$plantelId){
        $sql=" SELECT sp.id AS seccionPlantelId , g.nombre||' | Seccion: '||s.nombre || ' | Plan: ' || pln.nombre || ' | Mencion: ' || m.nombre AS seccionPlantel
                FROM gplantel.seccion_plantel_periodo AS spp
                LEFT JOIN gplantel.seccion_plantel AS sp ON sp.id = spp.seccion_plantel_id
                LEFT JOIN gplantel.grado AS g ON g.id = sp.grado_id
                LEFT JOIN gplantel.seccion AS s ON s.id = sp.seccion_id
                LEFT JOIN gplantel.plan AS pln ON pln.id = sp.plan_id
                LEFT JOIN gplantel.mencion AS m ON m.id = pln.mencion_id
                WHERE
                spp.periodo_id = :periodoId
                AND sp.plantel_id = :plantelId
                ORDER BY g.nombre
        ";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindValue(":periodoId", $periodoId, PDO::PARAM_STR);
        $consulta->bindValue(":plantelId", $plantelId, PDO::PARAM_STR);
        $resultado = $consulta->queryAll();
        return $resultado;
    }

    public function obtenerGrado($seccion) {
        $sql = " SELECT grado_id FROM gplantel.seccion_plantel  WHERE id = :seccion_plantel_id";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":seccion_plantel_id", $seccion, PDO::PARAM_INT);
        $resulatadoBusqueda = $busqueda->queryScalar();
        if (is_numeric($resulatadoBusqueda))
            return $resulatadoBusqueda;
        else
            return false;
    }

    public function cargarDetallesSeccion($seccion, $plantel_id) {
        $sql = "SELECT s.nombre as seccion, n.nombre as nivel, p.nombre as plan, g.nombre as grado, t.nombre as turno, sp.capacidad as capacidad, p.id as plan_id, sp.id as seccion_id, g.id as grado_id, p.cod_plan, m.nombre as modalidad"
                . " FROM gplantel.seccion_plantel sp"
                . " INNER JOIN gplantel.seccion s ON (s.id = sp.seccion_id)"
                . " INNER JOIN gplantel.grado g ON (g.id = sp.grado_id)"
                . " INNER JOIN gplantel.nivel n ON (n.id = sp.nivel_id)"
                . " INNER JOIN gplantel.plan p ON (p.id = sp.plan_id)"
                . " INNER JOIN gplantel.turno t ON (t.id = sp.turno_id)"
                . " LEFT JOIN gplantel.modalidad m ON (m.id = sp.modalidad_id)"
                . " WHERE sp.plantel_id = :plantel_id AND sp.id =:seccion_plantel_id";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":seccion_plantel_id", $seccion, PDO::PARAM_INT);
        $busqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_STR);
        $resulatadoBusqueda = $busqueda->queryAll();
        if ($resulatadoBusqueda !== array())
            return $resulatadoBusqueda;
        else
            return false;
    }

    /*  fin */

    /* LLENO LOS DROPDOWNLIST (PLAN Y NIVEL) DEL INDEX DE SECCION PLANTEL */

    public function llenarDropDown_plan_id($plantel_id) {

        $estatus = 'A';
        $sql = "SELECT DISTINCT npl.plan_id, p.nombre || ' [' || m.nombre ||'][' || p.dopcion || ']' as nombre, p.nombre as nombrePlan"
                . " FROM "
                . " gplantel.nivel_plan npl"
                . " INNER JOIN gplantel.nivel n on (npl.nivel_id = n.id)"
                . " INNER JOIN gplantel.nivel_plantel np on (np.nivel_id = n.id)"
                . " INNER JOIN gplantel.plan p on (npl.plan_id = p.id)"
                . " LEFT JOIN gplantel.mencion m on (p.mencion_id = m.id)"
                . " WHERE np.plantel_id IN ($plantel_id)"
                . " AND npl.estatus= :estatus"
                . " AND n.estatus= :estatus"
                . " AND np.estatus= :estatus"
                . " AND p.estatus= :estatus"
                . " ORDER BY npl.plan_id ASC";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":estatus", $estatus, PDO::PARAM_INT);
        $resultado = $busqueda->queryAll();
        $result = array();
        $resulta = array();
        foreach ($resultado as $r) {
            $nom = $r['nombre'];
            $nomPlan = $r['nombreplan'];
            $plan_id = $r['plan_id'];

            if ($nom == null) {
                $result = $nomPlan;
            } else {
                $result = $nom;
            }
            $resulta [] = array(
                'nombre' => $result,
                'plan_id' => $plan_id
            );
        }
        //  var_dump($resulta);
        //die()

        return $resulta;
    }

    public function llenarDropDown_modalidad_id($plantel_id) {
        
        $estatus='A';
         $sql = "SELECT m.id AS modalidad_id, m.nombre 
                        FROM gplantel.modalidad m
                        INNER JOIN gplantel.plantel_modalidad pm ON (pm.modalidad_id = m.id)
                        INNER JOIN gplantel.plantel p ON (p.id = pm.plantel_id)
                       WHERE p.id=:plantel_id
                       AND pm.estatus=:estatus
                       AND p.estatus=:estatus
                       AND m.estatus=:estatus";
         $resultado = Yii::app()->db->createCommand($sql);
         $resultado->bindParam(":estatus", $estatus, PDO::PARAM_STR);
         $resultado->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
         $result = $resultado->queryAll();
                return $result;
       
    }
    
    public function llenarDropDown_nivel_id($plantel_id) {

        $estatus = 'A';
        $sql = "SELECT n.id AS nivel_id, n.nombre AS nombre
                            FROM gplantel.modalidad_nivel mn
                            INNER JOIN gplantel.modalidad m ON (m.id = mn.modalidad_id)
                            INNER JOIN gplantel.nivel n ON (n.id = mn.nivel_id)
                            INNER JOIN gplantel.plantel_modalidad pm ON (pm.modalidad_id = m.id)
                            INNER JOIN gplantel.plantel p ON (p.id = pm.plantel_id)
                            INNER JOIN gplantel.nivel_plantel np ON (np.nivel_id = n.id AND np.plantel_id = p.id AND np.nivel_id = mn.nivel_id)
                           WHERE p.id=:plantel_id
                           AND pm.estatus=:estatus
                           AND p.estatus=:estatus
                           AND m.estatus=:estatus
                           AND mn.estatus=:estatus
                           ORDER BY m.nombre ASC";
//        $sql = "SELECT DISTINCT nplantel.nivel_id, n.nombre"
//                . " FROM "
//                . " gplantel.nivel_plantel nplantel"
//                . " INNER JOIN gplantel.plantel p on (p.id = nplantel.plantel_id)"
//                . " INNER JOIN gplantel.modalidad_nivel mn on (p.modalidad_id = mn.modalidad_id AND nplantel.nivel_id = mn.nivel_id)"
//                . " INNER JOIN gplantel.nivel n on (nplantel.nivel_id = n.id)"
//                . " WHERE nplantel.plantel_id = :plantel_id"
//                . " AND nplantel.estatus= :estatus"
//                . " AND p.estatus= :estatus"
//                . " AND mn.estatus= :estatus"
//                . " AND n.estatus= :estatus"
//                . " ORDER BY nplantel.nivel_id ASC";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_INT);
        $resultadoNiveles = $buqueda->queryAll();
        return $resultadoNiveles;
    }

    /* FIN */

    public function listaEstudiantesInscriptosEnSeccion($seccionId) {

        $estatus = 'A';
        $sql = "SELECT  e.cedula_escolar, e.fecha_nacimiento, e.nombres || ' ' || e.apellidos as nomApe, r.documento_identidad"
                . " FROM"
                . " gplantel.seccion_plantel sp"
                . " INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.seccion_plantel_id = sp.id)"
                . " INNER JOIN matricula.inscripcion_estudiante ie on (spp.id = ie.seccion_plantel_periodo_id)"
                . " INNER JOIN matricula.estudiante e on (e.id = ie.estudiante_id)"
                . " LEFT JOIN matricula.representante r on (r.id = e.representante_id)"
                . " WHERE spp.seccion_plantel_id = :seccion_plantel_id"
                . " AND ie.estatus= :estatus"
                . " ORDER BY ie.id DESC";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":seccion_plantel_id", $seccionId, PDO::PARAM_INT);
        $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $resultado = $buqueda->queryAll();
        //   var_dump($resultado); die();
        return $resultado;
    }

    public function existeEstudiantesInscriptosEnSeccion($seccionId, $periodo_id) {
        /* Modificar para que traiga la cedula escolar o pasaporte o cedula */
        $estatus = 'A';
        $sql = "SELECT  e.id,e.documento_identidad,e.tdocumento_identidad, e.cedula_escolar, e.fecha_nacimiento, e.nombres || ' ' || e.apellidos as nomApe, r.documento_identidad as cedula_representante"
                . " FROM"
                . " gplantel.seccion_plantel sp"
                . " INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.seccion_plantel_id = sp.id)"
                . " INNER JOIN matricula.inscripcion_estudiante ie on (spp.id = ie.seccion_plantel_periodo_id)"
                . " INNER JOIN matricula.estudiante e on (e.id = ie.estudiante_id)"
                . " LEFT JOIN matricula.representante r on (r.id = e.representante_id)"
                . " WHERE spp.seccion_plantel_id = :seccion_plantel_id AND spp.periodo_id = :periodo_id"
                . " AND ie.estatus= :estatus"
                . " ORDER BY ie.id DESC";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":seccion_plantel_id", $seccionId, PDO::PARAM_INT);
        $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
        $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $resultado = $buqueda->queryAll();
        //   var_dump($resultado); die();
        return new CArrayDataProvider($resultado, array(
            'pagination' => array(
                'pageSize' => 15,
            )
        ));
    }

    public function existeEstudiantesInscriptosEnSeccionDos($seccionId, $periodo_id) {
        /* Modificar para que traiga la cedula escolar o pasaporte o cedula */
        $estatus = 'A';
        $sql = "SELECT  e.id,e.documento_identidad,e.tdocumento_identidad, e.cedula_escolar, e.fecha_nacimiento, e.nombres || ' ' || e.apellidos as nomApe, r.documento_identidad as cedula_representante"
                . " FROM"
                . " gplantel.seccion_plantel sp"
                . " INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.seccion_plantel_id = sp.id)"
                . " INNER JOIN matricula.inscripcion_estudiante ie on (spp.id = ie.seccion_plantel_periodo_id)"
                . " INNER JOIN matricula.estudiante e on (e.id = ie.estudiante_id)"
                . " LEFT JOIN matricula.representante r on (r.id = e.representante_id)"
                . " WHERE spp.seccion_plantel_id = :seccion_plantel_id AND spp.periodo_id = :periodo_id"
                . " AND ie.estatus= :estatus"
                . " ORDER BY ie.id DESC";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":seccion_plantel_id", $seccionId, PDO::PARAM_INT);
        $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
        $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $resultado = $buqueda->queryAll();
        //   var_dump($resultado); die();
        return $resultado;
    }

    public function existeEstudiantesInscritosIndividualEnSeccion($seccionId, $periodo_id) {
//        var_dump($seccionId);
//        die();
        /* Modificar para que traiga la cedula escolar o pasaporte o cedula */
        $estatus = 'A';
        $sql = "SELECT  e.id,e.documento_identidad,e.tdocumento_identidad, e.cedula_escolar, e.fecha_nacimiento, e.nombres || ' ' || e.apellidos as nomApe, ie.estatus, ie.id as inscripcion_id,spp.seccion_plantel_id"
                . " FROM"
                . " gplantel.seccion_plantel sp"
                . " INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.seccion_plantel_id = sp.id)"
                . " INNER JOIN matricula.inscripcion_estudiante ie on (spp.id = ie.seccion_plantel_periodo_id and ie.plantel_id = sp.plantel_id)"
                . " INNER JOIN matricula.estudiante e on (e.id = ie.estudiante_id)"
                . " LEFT JOIN matricula.representante r on (r.id = e.representante_id)"
                . " WHERE spp.seccion_plantel_id = :seccion_plantel_id AND spp.periodo_id = :periodo_id"
                . " AND ie.estatus= :estatus"
                . " ORDER BY ie.id DESC";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":seccion_plantel_id", $seccionId, PDO::PARAM_INT);
        $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_STR);
        $resultado = $buqueda->queryAll();
        //   var_dump($resultado); die();
        return new CArrayDataProvider($resultado, array(
            'sort' => array(
                'defaultOrder' => 'documento_identidad ASC',
            ),
            'pagination' => array(
                'pageSize' => 15,
            )
        ));
    }

    public function esInicialPrimaria($seccion_plantel_id) {
        $sql = "SELECT sp.nivel_id
               FROM gplantel.seccion_plantel sp
               WHERE sp.id=:seccion_plantel_id AND ( sp.nivel_id=1 OR  sp.nivel_id=2) ";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        return $busqueda->queryScalar();
    }

    public function existeEstudiantesInscritosEnSeccion($seccionId, $periodo_id = 14) {

        $estatus = 'A';
        $sql = "SELECT  count(e.id)"
                . " FROM"
                . " gplantel.seccion_plantel sp"
                . " INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.seccion_plantel_id = sp.id)"
                . " INNER JOIN matricula.inscripcion_estudiante ie on (spp.id = ie.seccion_plantel_periodo_id)"
                . " INNER JOIN matricula.estudiante e on (e.id = ie.estudiante_id)"
                . " LEFT JOIN matricula.representante r on (r.id = e.representante_id)"
                . " WHERE spp.seccion_plantel_id = :seccion_plantel_id AND spp.periodo_id = :periodo_id"
                . " AND ie.estatus= :estatus"
                . " GROUP BY e.id";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":seccion_plantel_id", $seccionId, PDO::PARAM_INT);
        $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_STR);
        $resultado = $buqueda->queryScalar();
        //   var_dump($resultado); die();


        return $resultado;
    }

    public function existeSeccionPlantel($seccion_plantel_id, $plantel_id) {
        $sql = "SELECT COUNT(id) FROM gplantel.seccion_plantel WHERE id=:seccion_plantel_id AND plantel_id=:plantel_id";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        $buqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        return $buqueda->queryScalar();
    }

    public function calcularInscritosPorSeccion($seccionId, $periodo_id) {


        $sql = "SELECT  count(distinct e.id)"
                . " FROM gplantel.seccion_plantel sp"
                . " INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.seccion_plantel_id = sp.id)"
                . " INNER JOIN matricula.inscripcion_estudiante ie on (spp.id = ie.seccion_plantel_periodo_id)"
                . " INNER JOIN matricula.estudiante e on (e.id = ie.estudiante_id)"
                . " LEFT JOIN matricula.representante r on (r.id = e.representante_id)"
                . " WHERE spp.seccion_plantel_id=:seccion_plantel_id AND spp.periodo_id = :periodo_id";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":seccion_plantel_id", $seccionId, PDO::PARAM_INT);
        $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);

        $resultado = $buqueda->queryRow();
        //   var_dump($resultado); die();
        return $resultado;
    }

    public function activarSeccion($idSeccion, $periodoId) {

        $usuario_id = Yii::app()->user->id;
        $estatus = 'A';
        $fecha_ini = date('Y-m-d H:i:s');
        $fecha_elim = null;
        $fecha_act = null;
        $usuarioAct = null;

        $sql = "UPDATE gplantel.seccion_plantel_periodo
                    SET estatus=:estatus, fecha_act=:fecha_act, usuario_ini_id=:usuario_ini_id, fecha_ini=:fecha_ini, fecha_elim=:fecha_elim, usuario_act_id=:usuario_act_id
                    WHERE seccion_plantel_id=:id
                    AND periodo_id=:periodo_id";
        $guard = Yii::app()->db->createCommand($sql);

        $guard->bindParam(":usuario_ini_id", $usuario_id, PDO::PARAM_INT);
        $guard->bindParam(":usuario_act_id", $usuarioAct, PDO::PARAM_INT);
        $guard->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $guard->bindParam(":fecha_act", $fecha_act, PDO::PARAM_INT);
        $guard->bindParam(":fecha_ini", $fecha_ini, PDO::PARAM_INT);
        $guard->bindParam(":fecha_elim", $fecha_elim, PDO::PARAM_INT);
        $guard->bindParam(":id", $idSeccion, PDO::PARAM_INT);
        $guard->bindParam(":periodo_id", $periodoId, PDO::PARAM_INT);
        $resultadoGuardo = $guard->execute(); //devuelve 1 cuando actualiza y 0 cuando no actualiza
        // var_dump($resulatadoGuardo); die();
        return $resultadoGuardo;
    }

    public function estudiantesInscritos($seccionId, $periodoId) {

        if (is_numeric($seccionId) && is_numeric($periodoId)) {

            $sql = "SELECT distinct ie.estudiante_id, ie.seccion_plantel_periodo_id
                 FROM gplantel.seccion_plantel sp
                 INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.seccion_plantel_id = sp.id)
                 INNER JOIN matricula.inscripcion_estudiante ie on (spp.id = ie.seccion_plantel_periodo_id)
                 INNER JOIN matricula.estudiante e on (e.id = ie.estudiante_id)
                 LEFT JOIN matricula.representante r on (r.id = e.representante_id)
                 WHERE spp.seccion_plantel_id=:seccion_plantel_id
                 AND ie.estatus='A'
                 AND spp.periodo_id=:periodo_id
                 ORDER BY ie.estudiante_id ASC";
            $buqueda = Yii::app()->db->createCommand($sql);
            $buqueda->bindParam(":seccion_plantel_id", $seccionId, PDO::PARAM_INT);
            $buqueda->bindParam(":periodo_id", $periodoId, PDO::PARAM_INT);
            $resultado = $buqueda->queryAll();
            return $resultado;
        }
    }

    public function inactivarEstudiantesInscritos($estudiantesInscritos, $usuario_id) {

        if (is_array($estudiantesInscritos)) {

            $fecha = date('Y-m-d H:i:s');
            foreach ($estudiantesInscritos as $value) {
                $estudiante_id = (int) $value['estudiante_id'];
                $seccion_plantel_periodo_id = (int) $value['seccion_plantel_periodo_id'];
                //     var_dump($estudiante_id);
                $sql = "UPDATE matricula.inscripcion_estudiante
                            SET fecha_act=:fecha,
                            usuario_act_id=:usuario_id,
                            fecha_elim=:fecha,
                            estatus='E'
                            WHERE estudiante_id= :estudiante_id
                            AND seccion_plantel_periodo_id=:seccion_periodo_id";
                $buqueda = Yii::app()->db->createCommand($sql);
                $buqueda->bindParam(":estudiante_id", $estudiante_id, PDO::PARAM_INT);
                $buqueda->bindParam(":seccion_periodo_id", $seccion_plantel_periodo_id, PDO::PARAM_INT);
                $buqueda->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
                $buqueda->bindParam(":fecha", $fecha, PDO::PARAM_INT);
                $resultado = $buqueda->execute();
//                echo $sql;
//                die();
            }

            return $resultado;
        }
    }

    public function obtenerNivelSeccion($seccion_plantel_id) {
        $resultado = null;
        $indice = self::INDICE_SECCION_PLANTEL . $seccion_plantel_id;
        $resultado = Yii::app()->cache->get($indice);
        if (is_numeric($seccion_plantel_id) AND !$resultado) {
            $sql = "SELECT nivel_id FROM gplantel.seccion_plantel WHERE id = :seccion_plantel_id";
            $buqueda = Yii::app()->db->createCommand($sql);
            $buqueda->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
            $resultado = $buqueda->queryScalar();
            if ($resultado) {
                Yii::app()->cache->set($indice, $resultado, 86400);
            }
        }

        return $resultado;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($plantel_id, $periodo_id = null) {

        $criteria = new CDbCriteria;
        $criteria->alias = 't';
        $criteria->with = array();

        if (!is_null($periodo_id)) {
            $criteria->with = array(
                'grado' => array('alias' => 'g'),
                'seccion' => array('alias' => 's'),
                //  'seccionPlantelPeriodos' => array('alias' => 'spp'),
                'plantel' => array(
                    'alias' => 'pl',
                    'select' => 'modalidad_id'
                ),
                    /* 'cierreAsignacionDocentes' => array(
                      'alias' => 'cad',
                      'select' => 'estatus',
                      'condition'=>'cad.periodo_id=:periodo',
                      'params'=>array(':periodo'=>$periodo_id)
                      ) */
            );
            $criteria->select = "cad.estatus AS estatus_asig_doc,t.id,t.estatus,t.plantel_id,t.grado_id,t.capacidad,t.turno_id,t.seccion_id,t.plan_id,t.nivel_id,t.modalidad_id,spp.estatus AS estatus_seccion_p";

            $criteria->join .= " LEFT JOIN personal.cierre_asignacion_docente cad ON (cad.seccion_plantel_id=t.id AND cad.periodo_id=:periodo_id)";
        } else {
            $criteria->with = array(
                'grado' => array('alias' => 'g'),
                'seccion' => array('alias' => 's'),
                //     'seccionPlantelPeriodos' => array('alias' => 'spp'),
                'plantel' => array(
                    'alias' => 'pl',
                    'select' => 'modalidad_id'
                ),
            );

            $criteria->select = "t.id,t.estatus,t.plantel_id,t.grado_id,t.capacidad,t.turno_id,t.seccion_id,t.plan_id,t.nivel_id,t.modalidad_id,spp.estatus AS estatus_seccion_p";
        }
        //  var_dump(is_null($periodo_id));
        $criteria->join .= " LEFT JOIN gplantel.seccion_plantel_periodo spp ON (spp.seccion_plantel_id=t.id AND spp.periodo_id=:periodo_id)";
        $criteria->params = array(':periodo_id' => (int) (is_null($periodo_id)) ? 14 : $periodo_id);
        /* if (!is_null($periodo_id)){
          $criteria->select=array('cad.estatus as estatus_asig_doc');
          $criteria->join='LEFT JOIN personal.cierre_asignacion_docente cad ON (cad.seccion_plantel_id=t.id AND cad.periodo_id=15)';
          } */
//        var_dump($criteria);
//        die();

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.usuario_ini_id', $this->usuario_ini_id);
        $criteria->compare('t.fecha_ini', $this->fecha_ini, true);
        $criteria->compare('t.usuario_act_id', $this->usuario_act_id);
        $criteria->compare('t.fecha_act', $this->fecha_act, true);
        $criteria->compare('t.fecha_elim', $this->fecha_elim, true);

        //  $criteria->compare('estatus', $this->estatus, true);
        // $criteria->compare('plan_id', $this->plan_id);
        // $criteria->compare('nivel_id', $this->nivel_id);

        if (is_numeric($this->plan_id)) {
            if (strlen($this->plan_id) < 7) {
                $criteria->compare('t.plan_id', $this->plan_id);
            }
        }

        if (is_numeric($this->nivel_id)) {
            if (strlen($this->nivel_id) < 7) {
                $criteria->compare('t.nivel_id', $this->nivel_id);
            }
        }
        /* var_dump($this);
          if ($this->estatus_seccion_p === 'A' || $this->estatus_seccion_p === 'E' || $this->estatus_seccion_p == 'N') {
          $criteria->compare('spp.estatus', $this->estatus_seccion_p);
          } */

        $criteria->compare('t.plantel_id', $plantel_id); // filtro que los registro que muestro sean del plantel que se selecciono.

        if (is_numeric($this->grado_id)) {
            if (strlen($this->grado_id) < 7) {
                $criteria->compare('t.grado_id', $this->grado_id);
            }
        }

        if (is_numeric($this->capacidad)) {
            if (strlen($this->capacidad) <= 3) {
                $criteria->compare('to_char( t.capacidad,\'999\' )', $this->capacidad, true);
                // $criteria->addSearchCondition('capacidad', '%' . $this->capacidad . '%', false, 'AND', 'ILIKE');
            }
        }

        if (is_numeric($this->turno_id)) {
            if (strlen($this->turno_id) < 7) {
                $criteria->compare('t.turno_id', $this->turno_id);
            }
        }
        
         if (is_numeric($this->modalidad_id)) {
            if (strlen($this->modalidad_id) < 7) {
                $criteria->compare('t.modalidad_id', $this->modalidad_id);
            }
        }

        if (is_numeric($this->seccion_id)) {
            if (strlen($this->seccion_id) < 7) {
                $criteria->compare('t.seccion_id', $this->seccion_id);
            }
        }
        /*  if (!is_null($periodo_id)){
          $criteria->addCondition('cad.periodo_id=15','AND');
          $criteria->compare('cad.periodo_id', $periodo_id);
          } */
        $sort = new CSort();
        $sort->defaultOrder = 't.estatus ASC , g.nombre ASC,s.nombre ASC';
        //Va a ordenar la tabla utilizando el campo id_representacion en forma descendente "DESC",
        /* $dataProvider = new CActiveDataProvider($this, array(
          'pagination' => array(
          'pageSize' =>200
          ),
          'criteria' => $criteria,
          'sort' => $sort,
          ));
          echo "<pre>";
          print_r($dataProvider->getData());
          echo "</pre>";
          die(); */
        if (is_null($periodo_id)) {
            return new CActiveDataProvider($this, array(
                'pagination' => array(
                    'pageSize' => 10
                ),
                'criteria' => $criteria,
                'sort' => $sort,
            ));
        } else {
            return new CActiveDataProvider($this, array(
                'pagination' => array(
                    'pageSize' => 10
                ),
                'criteria' => $criteria,
                'sort' => $sort,
            ));
        }
    }

    public function insertarAsignaturaDocente($seccion_plantel_id, $usuario_ini_id, $periodo_id) {

        //die();
        $sql = " SELECT gplantel.insertar_asignatura_seccion(:seccion_plantel_id::INT,:usuario_id::INT,:periodo_id::SMALLINT)";
        $insertar = Yii::app()->db->createCommand($sql);
        $insertar->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        $insertar->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
        $insertar->bindParam(":usuario_id", $usuario_ini_id, PDO::PARAM_INT);
        $resultado = $insertar->queryScalar();
        return $resultado;
    }

    public function getCEPS($plantel_id, $periodo_id) {
        $resultado = '';
        $sql = "SELECT COUNT(DISTINCT ie.estudiante_id) as cantidad,
gr.nombre as grado,p.nombre as plan,c.nombre as credencial,m.nombre as mencion
FROM gplantel.seccion_plantel sp
INNER JOIN gplantel.seccion_plantel_periodo spp
ON sp.id=spp.seccion_plantel_id
INNER JOIN gplantel.nivel n ON n.id=sp.nivel_id
INNER JOIN gplantel.grado gr on gr.id=sp.grado_id
INNER JOIN gplantel.plan p ON (p.id=sp.plan_id)
LEFT JOIN matricula.inscripcion_estudiante ie on (ie.seccion_plantel_periodo_id=spp.id)
LEFT JOIN gplantel.mencion m ON (m.id=p.mencion_id)
LEFT JOIN gplantel.credencial c ON (c.id=p.credencial_id)
WHERE sp.plantel_id=:plantel_id AND spp.periodo_id=:periodo_id
GROUP BY gr.nombre,n.nombre,p.nombre,c.nombre,m.nombre
ORDER BY gr.nombre,n.nombre,p.nombre,c.nombre,m.nombre";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam('plantel_id', $plantel_id, PDO::PARAM_INT);
        $consulta->bindParam('periodo_id', $periodo_id, PDO::PARAM_INT);
        $resultado = $consulta->queryAll();
        return $resultado;
    }

    /**
     * @author Ignacio Salazar
     * @date 04/02/2015
     * @param integer $id ID de Plantel que se desea verificar existencia
     * @return boolean
     */
    public function existeSeccionPlantelById($id) {
        $resultado = FALSE;

        if (is_numeric($id)) {
            $sql = " SELECT CASE WHEN (COUNT(1) > 0) THEN TRUE ELSE FALSE END  FROM gplantel.seccion_plantel WHERE id = :id";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":id", $id, PDO::PARAM_INT);
            $resultado = $consulta->queryScalar();
        }

        return $resultado;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SeccionPlantel the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function calcularInscritosConCedulaEscolar($seccionId, $periodo_id) {


        $sql = "SELECT  count(distinct e.id)"
                . " FROM gplantel.seccion_plantel sp"
                . " INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.seccion_plantel_id = sp.id)"
                . " INNER JOIN matricula.inscripcion_estudiante ie on (spp.id = ie.seccion_plantel_periodo_id)"
                . " INNER JOIN matricula.estudiante e on (e.id = ie.estudiante_id)"
                . " LEFT JOIN matricula.representante r on (r.id = e.representante_id)"
                . " WHERE spp.seccion_plantel_id=:seccion_plantel_id AND spp.periodo_id = :periodo_id"
                . " AND ie.estatus= 'A'"
                . " AND e.documento_identidad is null"
                . " AND e.cedula_escolar is not null"
        ;
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":seccion_plantel_id", $seccionId, PDO::PARAM_INT);
        $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);

        $resultado = $buqueda->queryRow();
        //   var_dump($resultado); die();
        return $resultado;
    }

    public function busquedaEstudiante($documento_identidad, $tdocumento_identidad) {
        $sql = "SELECT nombres,apellidos "
                . "FROM matricula.estudiante as e "
                . "WHERE e.documento_identidad= :documento_identidad AND e.tdocumento_identidad= :tdocumento_identidad";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":documento_identidad", $documento_identidad, PDO::PARAM_STR);
        $busqueda->bindParam(":tdocumento_identidad", $tdocumento_identidad, PDO::PARAM_STR);
        $resultado = $busqueda->queryRow();
        return $resultado;
    }

    public function obtenerNivel($seccion_plantel_id) {
        $sql = "SELECT sp.nivel_id
                        FROM gplantel.seccion_plantel sp
                        WHERE sp.id=:seccion_plantel_id";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        $buscarNivel = $buqueda->queryScalar();
        return $buscarNivel;
    }

    /**
     * @author Marisela La Cruz
     * @date 23/04/2015
     * @param integer $plantel_id ID de Plantel que se desea verificar existencia
     * @param integer $seccion ID de Seccion que se desea verificar existencia
     * @param integer $grado ID de Grado que se desea verificar existencia
     * @return boolean
     */
    public function validarSeccionGrado($plantel_id, $seccion, $grado, $turno) {
        $sql = "SELECT sp.id AS seccion_plantel_id
                        FROM gplantel.seccion_plantel sp
                        INNER JOIN gplantel.plantel p ON (p.id = sp.plantel_id)
                        INNER JOIN gplantel.seccion s ON (s.id = sp.seccion_id)
                        INNER JOIN gplantel.grado g ON (g.id = sp.grado_id)
                        INNER JOIN gplantel.turno t ON (t.id = sp.turno_id)
                        WHERE sp.plantel_id=:plantel_id
                        AND sp.seccion_id=:seccion_id
                        AND sp.grado_id=:grado_id
                        AND sp.turno_id=:turno_id";
        $buscar = Yii::app()->db->createCommand($sql);
        $buscar->bindParam(':plantel_id', $plantel_id, PDO::PARAM_INT);
        $buscar->bindParam(':seccion_id', $seccion, PDO::PARAM_INT);
        $buscar->bindParam(':grado_id', $grado, PDO::PARAM_INT);
        $buscar->bindParam(':turno_id', $turno, PDO::PARAM_INT);
        $resultado = $buscar->queryScalar();
        return $resultado;
    }

    public function verificarNivel($seccion_plantel_id) {
        $sql = "SELECT nivel_id AS nivel_id
                        FROM gplantel.seccion_plantel
                        WHERE id=:seccion_plantel_id
                        AND estatus='A'";
        $buscar = Yii::app()->db->createCommand($sql);
        $buscar->bindParam(':seccion_plantel_id', $seccion_plantel_id, PDO::PARAM_INT);
        $resultado = $buscar->queryScalar();

        return $resultado;
    }

    public function testQuery($seccion_plantel_id){
        $sql = "SELECT plantel_id FROM gplantel.seccion_plantel WHERE id = :id";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":id", $seccion_plantel_id , PDO::PARAM_INT);
        $resultado = $busqueda->queryScalar();
        return $resultado;
    }
    public function buscarModalidadPlanSeccion($seccion_plantel_id){
        $sql="SELECT s.modalidad_id, s.plan_id
                FROM gplantel.seccion_plantel s
                INNER JOIN gplantel.modalidad m ON (m.id = s.modalidad_id)
                WHERE s.id=:seccion_plantel_id";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        $resultado = $buqueda->queryRow();
        return $resultado;
    }

}
