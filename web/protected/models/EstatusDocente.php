<?php

/**
 * This is the model class for table "personal.estatus_docente".
 *
 * The followings are the available columns in table 'personal.estatus_docente':
 * @property integer $id
 * @property string $nombre
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property Docente[] $docentes
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property EspecificacionEstatus[] $especificacionEstatuses
 */
class EstatusDocente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personal.estatus_docente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, fecha_ini, usuario_ini_id', 'required'),
			array('usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
            array('nombre', 'unique'),
			array('nombre', 'length', 'max'=>35),
			array('estatus', 'length', 'max'=>1),
			array('fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, fecha_ini, usuario_ini_id, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'docentes' => array(self::HAS_MANY, 'Docente', 'estatus_docente_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'especificacionEstatuses' => array(self::HAS_MANY, 'EspecificacionEstatus', 'estatus_docente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Estatus del Personal',
			'nombre' => 'Nombre',
			'fecha_ini' => 'Fecha Ini',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        if(!isset($this->fecha_ini) and ($this->fecha_ini!="" or $this->fecha_ini!=null) ){
            $this->fecha_ini= date("Y-m-d H:i:s",strtotime($this->fecha_ini));
        }
        if(!isset($this->fecha_act) and ($this->fecha_act!="" or $this->fecha_act!=null) ){
            $this->fecha_act= date("Y-m-d H:i:s",strtotime($this->fecha_act));
        }
        $criteria->compare('id',$this->id);
        $criteria->compare('nombre',$this->nombre,true);
        $criteria->compare('fecha_ini',$this->fecha_ini);
        $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        $criteria->compare('usuario_act_id',$this->usuario_act_id);
        $criteria->compare('fecha_act',$this->fecha_act);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);
        $criteria->order='estatus ASC, nombre ASC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,

		));
	}


    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EstatusDocente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
