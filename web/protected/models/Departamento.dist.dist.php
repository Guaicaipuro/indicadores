<?php

/**
 * This is the model class for table "servicio.departamento".
 *
 * The followings are the available columns in table 'servicio.departamento':
 * @property integer $id
 * @property string $nombre
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $usuario_ini
 * @property string $usuario_act
 * @property string $estatus
 * @property string $icon
 *
 * The followings are the available model relations:
 * @property JefeDepartamento[] $jefeDepartamentos
 * @property Categoria[] $categorias
 */
class Departamento extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'servicio.departamento';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, fecha_ini, usuario_ini', 'required'),
            array('nombre', 'length', 'max'=>180),
            array('usuario_ini, usuario_act', 'length', 'max'=>20),
            array('estatus', 'length', 'max'=>1),
            array('icon', 'length', 'max'=>60),
            array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombre, fecha_ini, fecha_act, usuario_ini, usuario_act, estatus, icon', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'jefeDepartamentos' => array(self::HAS_MANY, 'JefeDepartamento', 'departamento_id'),
            'categorias' => array(self::HAS_MANY, 'Categoria', 'departamento_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'nombre' => 'Nombre',
            'fecha_ini' => 'Fecha Ini',
            'fecha_act' => 'Fecha Act',
            'usuario_ini' => 'Usuario Ini',
            'usuario_act' => 'Usuario Act',
            'estatus' => 'Estatus',
            'icon' => 'Icon',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;
        if (is_numeric($this->id)) {
            $criteria->compare('id', $this->id);
        }
        if (strlen($this->nombre) > 0) {
            $criteria->compare('nombre', $this->nombre, true);
        }
        if (Utiles::isValidDate($this->fecha_ini, 'y-m-d')) {
            $criteria->compare('fecha_ini', $this->fecha_ini);
        }
        // if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
        if (Utiles::isValidDate($this->fecha_act, 'y-m-d')) {
            $criteria->compare('fecha_act', $this->fecha_act);
        }
        // if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
        if (strlen($this->usuario_ini) > 0) {
            $criteria->compare('usuario_ini', $this->usuario_ini, true);
        }
        if (strlen($this->usuario_act) > 0) {
            $criteria->compare('usuario_act', $this->usuario_act, true);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }


    public function beforeInsert() {
        parent::beforeSave();
        $this->fecha_ini = date('Y-m-d H:i:s');
        $this->usuario_ini_id = Yii::app()->user->id;
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeUpdate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeDelete() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        // $this->fecha_eli = $this->fecha_act;
        $this->estatus = 'I';
        return true;
    }

    public function beforeActivate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        $this->estatus = 'A';
        return true;
    }

    public function getDepartamentosDisponibles(){
        $sql = "SELECT d.id,d.nombre,d.icon FROM servicio.departamento d WHERE d.estatus = 'A' GROUP BY d.id,d.nombre ORDER BY d.nombre ";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
        return $result;
    }

    public function smk_font_awesome(){
        return array(
            array('value'=>'fa-500px','nombre' => '500px',
            ),array('value'=>'fa-adjust','nombre' => 'Adjust',
            ),array('value'=>'fa-adn','nombre' => 'Adn',
            ),array('value'=>'fa-align-center','nombre' => 'Align center',
            ),array('value'=>'fa-align-justify','nombre' => 'Align justify',
            ),array('value'=>'fa-align-left','nombre' => 'Align left',
            ),array('value'=>'fa-align-right','nombre' => 'Align right',
            ),array('value'=>'fa-amazon','nombre' => 'Amazon',
            ),array('value'=>'fa-ambulance','nombre' => 'Ambulance',
            ),array('value'=>'fa-anchor','nombre' => 'Anchor',
            ),array('value'=>'fa-android','nombre' => 'Android',
            ),array('value'=>'fa-angellist','nombre' => 'Angellist',
            ),array('value'=>'fa-angle-double-down','nombre' => 'Angle double down',
            ),array('value'=>'fa-angle-double-left','nombre' => 'Angle double left',
            ),array('value'=>'fa-angle-double-right','nombre' => 'Angle double right',
            ),array('value'=>'fa-angle-double-up','nombre' => 'Angle double up',
            ),array('value'=>'fa-angle-down','nombre' => 'Angle down',
            ),array('value'=>'fa-angle-left','nombre' => 'Angle left',
            ),array('value'=>'fa-angle-right','nombre' => 'Angle right',
            ),array('value'=>'fa-angle-up','nombre' => 'Angle up',
            ),array('value'=>'fa-apple','nombre' => 'Apple',
            ),array('value'=>'fa-archive','nombre' => 'Archive',
            ),array('value'=>'fa-area-chart','nombre' => 'Area chart',
            ),array('value'=>'fa-arrow-circle-down','nombre' => 'Arrow circle down',
            ),array('value'=>'fa-arrow-circle-left','nombre' => 'Arrow circle left',
            ),array('value'=>'fa-arrow-circle-o-down','nombre' => 'Arrow circle o down',
            ),array('value'=>'fa-arrow-circle-o-left','nombre' => 'Arrow circle o left',
            ),array('value'=>'fa-arrow-circle-o-right','nombre' => 'Arrow circle o right',
            ),array('value'=>'fa-arrow-circle-o-up','nombre' => 'Arrow circle o up',
            ),array('value'=>'fa-arrow-circle-right','nombre' => 'Arrow circle right',
            ),array('value'=>'fa-arrow-circle-up','nombre' => 'Arrow circle up',
            ),array('value'=>'fa-arrow-down','nombre' => 'Arrow down',
            ),array('value'=>'fa-arrow-left','nombre' => 'Arrow left',
            ),array('value'=>'fa-arrow-right','nombre' => 'Arrow right',
            ),array('value'=>'fa-arrow-up','nombre' => 'Arrow up',
            ),array('value'=>'fa-arrows','nombre' => 'Arrows',
            ),array('value'=>'fa-arrows-alt','nombre' => 'Arrows alt',
            ),array('value'=>'fa-arrows-h','nombre' => 'Arrows h',
            ),array('value'=>'fa-arrows-v','nombre' => 'Arrows v',
            ),array('value'=>'fa-asterisk','nombre' => 'Asterisk',
            ),array('value'=>'fa-at','nombre' => 'At',
            ),array('value'=>'fa-backward','nombre' => 'Backward',
            ),array('value'=>'fa-balance-scale','nombre' => 'Balance scale',
            ),array('value'=>'fa-ban','nombre' => 'Ban',
            ),array('value'=>'fa-bar-chart','nombre' => 'Bar chart',
            ),array('value'=>'fa-barcode','nombre' => 'Barcode',
            ),array('value'=>'fa-bars','nombre' => 'Bars',
            ),array('value'=>'fa-battery-empty','nombre' => 'Battery empty',
            ),array('value'=>'fa-battery-full','nombre' => 'Battery full',
            ),array('value'=>'fa-battery-half','nombre' => 'Battery half',
            ),array('value'=>'fa-battery-quarter','nombre' => 'Battery quarter',
            ),array('value'=>'fa-battery-three-quarters','nombre' => 'Battery three quarters',
            ),array('value'=>'fa-bed','nombre' => 'Bed',
            ),array('value'=>'fa-beer','nombre' => 'Beer',
            ),array('value'=>'fa-behance','nombre' => 'Behance',
            ),array('value'=>'fa-behance-square','nombre' => 'Behance square',
            ),array('value'=>'fa-bell','nombre' => 'Bell',
            ),array('value'=>'fa-bell-o','nombre' => 'Bell o',
            ),array('value'=>'fa-bell-slash','nombre' => 'Bell slash',
            ),array('value'=>'fa-bell-slash-o','nombre' => 'Bell slash o',
            ),array('value'=>'fa-bicycle','nombre' => 'Bicycle',
            ),array('value'=>'fa-binoculars','nombre' => 'Binoculars',
            ),array('value'=>'fa-birthday-cake','nombre' => 'Birthday cake',
            ),array('value'=>'fa-bitbucket','nombre' => 'Bitbucket',
            ),array('value'=>'fa-bitbucket-square','nombre' => 'Bitbucket square',
            ),array('value'=>'fa-black-tie','nombre' => 'Black tie',
            ),array('value'=>'fa-bold','nombre' => 'Bold',
            ),array('value'=>'fa-bolt','nombre' => 'Bolt',
            ),array('value'=>'fa-bomb','nombre' => 'Bomb',
            ),array('value'=>'fa-book','nombre' => 'Book',
            ),array('value'=>'fa-bookmark','nombre' => 'Bookmark',
            ),array('value'=>'fa-bookmark-o','nombre' => 'Bookmark o',
            ),array('value'=>'fa-briefcase','nombre' => 'Briefcase',
            ),array('value'=>'fa-btc','nombre' => 'Btc',
            ),array('value'=>'fa-bug','nombre' => 'Bug',
            ),array('value'=>'fa-building','nombre' => 'Building',
            ),array('value'=>'fa-building-o','nombre' => 'Building o',
            ),array('value'=>'fa-bullhorn','nombre' => 'Bullhorn',
            ),array('value'=>'fa-bullseye','nombre' => 'Bullseye',
            ),array('value'=>'fa-bus','nombre' => 'Bus',
            ),array('value'=>'fa-buysellads','nombre' => 'Buysellads',
            ),array('value'=>'fa-calculator','nombre' => 'Calculator',
            ),array('value'=>'fa-calendar','nombre' => 'Calendar',
            ),array('value'=>'fa-calendar-check-o','nombre' => 'Calendar check o',
            ),array('value'=>'fa-calendar-minus-o','nombre' => 'Calendar minus o',
            ),array('value'=>'fa-calendar-o','nombre' => 'Calendar o',
            ),array('value'=>'fa-calendar-plus-o','nombre' => 'Calendar plus o',
            ),array('value'=>'fa-calendar-times-o','nombre' => 'Calendar times o',
            ),array('value'=>'fa-camera','nombre' => 'Camera',
            ),array('value'=>'fa-camera-retro','nombre' => 'Camera retro',
            ),array('value'=>'fa-car','nombre' => 'Car',
            ),array('value'=>'fa-caret-down','nombre' => 'Caret down',
            ),array('value'=>'fa-caret-left','nombre' => 'Caret left',
            ),array('value'=>'fa-caret-right','nombre' => 'Caret right',
            ),array('value'=>'fa-caret-square-o-down','nombre' => 'Caret square o down',
            ),array('value'=>'fa-caret-square-o-left','nombre' => 'Caret square o left',
            ),array('value'=>'fa-caret-square-o-right','nombre' => 'Caret square o right',
            ),array('value'=>'fa-caret-square-o-up','nombre' => 'Caret square o up',
            ),array('value'=>'fa-caret-up','nombre' => 'Caret up',
            ),array('value'=>'fa-cart-arrow-down','nombre' => 'Cart arrow down',
            ),array('value'=>'fa-cart-plus','nombre' => 'Cart plus',
            ),array('value'=>'fa-cc','nombre' => 'Cc',
            ),array('value'=>'fa-cc-amex','nombre' => 'Cc amex',
            ),array('value'=>'fa-cc-diners-club','nombre' => 'Cc diners club',
            ),array('value'=>'fa-cc-discover','nombre' => 'Cc discover',
            ),array('value'=>'fa-cc-jcb','nombre' => 'Cc jcb',
            ),array('value'=>'fa-cc-mastercard','nombre' => 'Cc mastercard',
            ),array('value'=>'fa-cc-paypal','nombre' => 'Cc paypal',
            ),array('value'=>'fa-cc-stripe','nombre' => 'Cc stripe',
            ),array('value'=>'fa-cc-visa','nombre' => 'Cc visa',
            ),array('value'=>'fa-certificate','nombre' => 'Certificate',
            ),array('value'=>'fa-chain-broken','nombre' => 'Chain broken',
            ),array('value'=>'fa-check','nombre' => 'Check',
            ),array('value'=>'fa-check-circle','nombre' => 'Check circle',
            ),array('value'=>'fa-check-circle-o','nombre' => 'Check circle o',
            ),array('value'=>'fa-check-square','nombre' => 'Check square',
            ),array('value'=>'fa-check-square-o','nombre' => 'Check square o',
            ),array('value'=>'fa-chevron-circle-down','nombre' => 'Chevron circle down',
            ),array('value'=>'fa-chevron-circle-left','nombre' => 'Chevron circle left',
            ),array('value'=>'fa-chevron-circle-right','nombre' => 'Chevron circle right',
            ),array('value'=>'fa-chevron-circle-up','nombre' => 'Chevron circle up',
            ),array('value'=>'fa-chevron-down','nombre' => 'Chevron down',
            ),array('value'=>'fa-chevron-left','nombre' => 'Chevron left',
            ),array('value'=>'fa-chevron-right','nombre' => 'Chevron right',
            ),array('value'=>'fa-chevron-up','nombre' => 'Chevron up',
            ),array('value'=>'fa-child','nombre' => 'Child',
            ),array('value'=>'fa-chrome','nombre' => 'Chrome',
            ),array('value'=>'fa-circle','nombre' => 'Circle',
            ),array('value'=>'fa-circle-o','nombre' => 'Circle o',
            ),array('value'=>'fa-circle-o-notch','nombre' => 'Circle o notch',
            ),array('value'=>'fa-circle-thin','nombre' => 'Circle thin',
            ),array('value'=>'fa-clipboard','nombre' => 'Clipboard',
            ),array('value'=>'fa-clock-o','nombre' => 'Clock o',
            ),array('value'=>'fa-clone','nombre' => 'Clone',
            ),array('value'=>'fa-cloud','nombre' => 'Cloud',
            ),array('value'=>'fa-cloud-download','nombre' => 'Cloud download',
            ),array('value'=>'fa-cloud-upload','nombre' => 'Cloud upload',
            ),array('value'=>'fa-code','nombre' => 'Code',
            ),array('value'=>'fa-code-fork','nombre' => 'Code fork',
            ),array('value'=>'fa-codepen','nombre' => 'Codepen',
            ),array('value'=>'fa-coffee','nombre' => 'Coffee',
            ),array('value'=>'fa-cog','nombre' => 'Cog',
            ),array('value'=>'fa-cogs','nombre' => 'Cogs',
            ),array('value'=>'fa-columns','nombre' => 'Columns',
            ),array('value'=>'fa-comment','nombre' => 'Comment',
            ),array('value'=>'fa-comment-o','nombre' => 'Comment o',
            ),array('value'=>'fa-commenting','nombre' => 'Commenting',
            ),array('value'=>'fa-commenting-o','nombre' => 'Commenting o',
            ),array('value'=>'fa-comments','nombre' => 'Comments',
            ),array('value'=>'fa-comments-o','nombre' => 'Comments o',
            ),array('value'=>'fa-compass','nombre' => 'Compass',
            ),array('value'=>'fa-compress','nombre' => 'Compress',
            ),array('value'=>'fa-connectdevelop','nombre' => 'Connectdevelop',
            ),array('value'=>'fa-contao','nombre' => 'Contao',
            ),array('value'=>'fa-copyright','nombre' => 'Copyright',
            ),array('value'=>'fa-creative-commons','nombre' => 'Creative commons',
            ),array('value'=>'fa-credit-card','nombre' => 'Credit card',
            ),array('value'=>'fa-crop','nombre' => 'Crop',
            ),array('value'=>'fa-crosshairs','nombre' => 'Crosshairs',
            ),array('value'=>'fa-css3','nombre' => 'Css3',
            ),array('value'=>'fa-cube','nombre' => 'Cube',
            ),array('value'=>'fa-cubes','nombre' => 'Cubes',
            ),array('value'=>'fa-cutlery','nombre' => 'Cutlery',
            ),array('value'=>'fa-dashcube','nombre' => 'Dashcube',
            ),array('value'=>'fa-database','nombre' => 'Database',
            ),array('value'=>'fa-delicious','nombre' => 'Delicious',
            ),array('value'=>'fa-desktop','nombre' => 'Desktop',
            ),array('value'=>'fa-deviantart','nombre' => 'Deviantart',
            ),array('value'=>'fa-diamond','nombre' => 'Diamond',
            ),array('value'=>'fa-digg','nombre' => 'Digg',
            ),array('value'=>'fa-dot-circle-o','nombre' => 'Dot circle o',
            ),array('value'=>'fa-download','nombre' => 'Download',
            ),array('value'=>'fa-dribbble','nombre' => 'Dribbble',
            ),array('value'=>'fa-dropbox','nombre' => 'Dropbox',
            ),array('value'=>'fa-drupal','nombre' => 'Drupal',
            ),array('value'=>'fa-eject','nombre' => 'Eject',
            ),array('value'=>'fa-ellipsis-h','nombre' => 'Ellipsis h',
            ),array('value'=>'fa-ellipsis-v','nombre' => 'Ellipsis v',
            ),array('value'=>'fa-empire','nombre' => 'Empire',
            ),array('value'=>'fa-envelope','nombre' => 'Envelope',
            ),array('value'=>'fa-envelope-o','nombre' => 'Envelope o',
            ),array('value'=>'fa-envelope-square','nombre' => 'Envelope square',
            ),array('value'=>'fa-eraser','nombre' => 'Eraser',
            ),array('value'=>'fa-eur','nombre' => 'Eur',
            ),array('value'=>'fa-exchange','nombre' => 'Exchange',
            ),array('value'=>'fa-exclamation','nombre' => 'Exclamation',
            ),array('value'=>'fa-exclamation-circle','nombre' => 'Exclamation circle',
            ),array('value'=>'fa-exclamation-triangle','nombre' => 'Exclamation triangle',
            ),array('value'=>'fa-expand','nombre' => 'Expand',
            ),array('value'=>'fa-expeditedssl','nombre' => 'Expeditedssl',
            ),array('value'=>'fa-external-link','nombre' => 'External link',
            ),array('value'=>'fa-external-link-square','nombre' => 'External link square',
            ),array('value'=>'fa-eye','nombre' => 'Eye',
            ),array('value'=>'fa-eye-slash','nombre' => 'Eye slash',
            ),array('value'=>'fa-eyedropper','nombre' => 'Eyedropper',
            ),array('value'=>'fa-facebook','nombre' => 'Facebook',
            ),array('value'=>'fa-facebook-official','nombre' => 'Facebook official',
            ),array('value'=>'fa-facebook-square','nombre' => 'Facebook square',
            ),array('value'=>'fa-fast-backward','nombre' => 'Fast backward',
            ),array('value'=>'fa-fast-forward','nombre' => 'Fast forward',
            ),array('value'=>'fa-fax','nombre' => 'Fax',
            ),array('value'=>'fa-female','nombre' => 'Female',
            ),array('value'=>'fa-fighter-jet','nombre' => 'Fighter jet',
            ),array('value'=>'fa-file','nombre' => 'File',
            ),array('value'=>'fa-file-archive-o','nombre' => 'File archive o',
            ),array('value'=>'fa-file-audio-o','nombre' => 'File audio o',
            ),array('value'=>'fa-file-code-o','nombre' => 'File code o',
            ),array('value'=>'fa-file-excel-o','nombre' => 'File excel o',
            ),array('value'=>'fa-file-image-o','nombre' => 'File image o',
            ),array('value'=>'fa-file-o','nombre' => 'File o',
            ),array('value'=>'fa-file-pdf-o','nombre' => 'File pdf o',
            ),array('value'=>'fa-file-powerpoint-o','nombre' => 'File powerpoint o',
            ),array('value'=>'fa-file-text','nombre' => 'File text',
            ),array('value'=>'fa-file-text-o','nombre' => 'File text o',
            ),array('value'=>'fa-file-video-o','nombre' => 'File video o',
            ),array('value'=>'fa-file-word-o','nombre' => 'File word o',
            ),array('value'=>'fa-files-o','nombre' => 'Files o',
            ),array('value'=>'fa-film','nombre' => 'Film',
            ),array('value'=>'fa-filter','nombre' => 'Filter',
            ),array('value'=>'fa-fire','nombre' => 'Fire',
            ),array('value'=>'fa-fire-extinguisher','nombre' => 'Fire extinguisher',
            ),array('value'=>'fa-firefox','nombre' => 'Firefox',
            ),array('value'=>'fa-flag','nombre' => 'Flag',
            ),array('value'=>'fa-flag-checkered','nombre' => 'Flag checkered',
            ),array('value'=>'fa-flag-o','nombre' => 'Flag o',
            ),array('value'=>'fa-flask','nombre' => 'Flask',
            ),array('value'=>'fa-flickr','nombre' => 'Flickr',
            ),array('value'=>'fa-floppy-o','nombre' => 'Floppy o',
            ),array('value'=>'fa-folder','nombre' => 'Folder',
            ),array('value'=>'fa-folder-o','nombre' => 'Folder o',
            ),array('value'=>'fa-folder-open','nombre' => 'Folder open',
            ),array('value'=>'fa-folder-open-o','nombre' => 'Folder open o',
            ),array('value'=>'fa-font','nombre' => 'Font',
            ),array('value'=>'fa-fonticons','nombre' => 'Fonticons',
            ),array('value'=>'fa-forumbee','nombre' => 'Forumbee',
            ),array('value'=>'fa-forward','nombre' => 'Forward',
            ),array('value'=>'fa-foursquare','nombre' => 'Foursquare',
            ),array('value'=>'fa-frown-o','nombre' => 'Frown o',
            ),array('value'=>'fa-futbol-o','nombre' => 'Futbol o',
            ),array('value'=>'fa-gamepad','nombre' => 'Gamepad',
            ),array('value'=>'fa-gavel','nombre' => 'Gavel',
            ),array('value'=>'fa-gbp','nombre' => 'Gbp',
            ),array('value'=>'fa-genderless','nombre' => 'Genderless',
            ),array('value'=>'fa-get-pocket','nombre' => 'Get pocket',
            ),array('value'=>'fa-gg','nombre' => 'Gg',
            ),array('value'=>'fa-gg-circle','nombre' => 'Gg circle',
            ),array('value'=>'fa-gift','nombre' => 'Gift',
            ),array('value'=>'fa-git','nombre' => 'Git',
            ),array('value'=>'fa-git-square','nombre' => 'Git square',
            ),array('value'=>'fa-github','nombre' => 'Github',
            ),array('value'=>'fa-github-alt','nombre' => 'Github alt',
            ),array('value'=>'fa-github-square','nombre' => 'Github square',
            ),array('value'=>'fa-glass','nombre' => 'Glass',
            ),array('value'=>'fa-globe','nombre' => 'Globe',
            ),array('value'=>'fa-google','nombre' => 'Google',
            ),array('value'=>'fa-google-plus','nombre' => 'Google plus',
            ),array('value'=>'fa-google-plus-square','nombre' => 'Google plus square',
            ),array('value'=>'fa-google-wallet','nombre' => 'Google wallet',
            ),array('value'=>'fa-graduation-cap','nombre' => 'Graduation cap',
            ),array('value'=>'fa-gratipay','nombre' => 'Gratipay',
            ),array('value'=>'fa-h-square','nombre' => 'H square',
            ),array('value'=>'fa-hacker-news','nombre' => 'Hacker news',
            ),array('value'=>'fa-hand-lizard-o','nombre' => 'Hand lizard o',
            ),array('value'=>'fa-hand-o-down','nombre' => 'Hand o down',
            ),array('value'=>'fa-hand-o-left','nombre' => 'Hand o left',
            ),array('value'=>'fa-hand-o-right','nombre' => 'Hand o right',
            ),array('value'=>'fa-hand-o-up','nombre' => 'Hand o up',
            ),array('value'=>'fa-hand-paper-o','nombre' => 'Hand paper o',
            ),array('value'=>'fa-hand-peace-o','nombre' => 'Hand peace o',
            ),array('value'=>'fa-hand-pointer-o','nombre' => 'Hand pointer o',
            ),array('value'=>'fa-hand-rock-o','nombre' => 'Hand rock o',
            ),array('value'=>'fa-hand-scissors-o','nombre' => 'Hand scissors o',
            ),array('value'=>'fa-hand-spock-o','nombre' => 'Hand spock o',
            ),array('value'=>'fa-hdd-o','nombre' => 'Hdd o',
            ),array('value'=>'fa-header','nombre' => 'Header',
            ),array('value'=>'fa-headphones','nombre' => 'Headphones',
            ),array('value'=>'fa-heart','nombre' => 'Heart',
            ),array('value'=>'fa-heart-o','nombre' => 'Heart o',
            ),array('value'=>'fa-heartbeat','nombre' => 'Heartbeat',
            ),array('value'=>'fa-history','nombre' => 'History',
            ),array('value'=>'fa-home','nombre' => 'Home',
            ),array('value'=>'fa-hospital-o','nombre' => 'Hospital o',
            ),array('value'=>'fa-hourglass','nombre' => 'Hourglass',
            ),array('value'=>'fa-hourglass-end','nombre' => 'Hourglass end',
            ),array('value'=>'fa-hourglass-half','nombre' => 'Hourglass half',
            ),array('value'=>'fa-hourglass-o','nombre' => 'Hourglass o',
            ),array('value'=>'fa-hourglass-start','nombre' => 'Hourglass start',
            ),array('value'=>'fa-houzz','nombre' => 'Houzz',
            ),array('value'=>'fa-html5','nombre' => 'Html5',
            ),array('value'=>'fa-i-cursor','nombre' => 'I cursor',
            ),array('value'=>'fa-ils','nombre' => 'Ils',
            ),array('value'=>'fa-inbox','nombre' => 'Inbox',
            ),array('value'=>'fa-indent','nombre' => 'Indent',
            ),array('value'=>'fa-industry','nombre' => 'Industry',
            ),array('value'=>'fa-info','nombre' => 'Info',
            ),array('value'=>'fa-info-circle','nombre' => 'Info circle',
            ),array('value'=>'fa-inr','nombre' => 'Inr',
            ),array('value'=>'fa-instagram','nombre' => 'Instagram',
            ),array('value'=>'fa-internet-explorer','nombre' => 'Internet explorer',
            ),array('value'=>'fa-ioxhost','nombre' => 'Ioxhost',
            ),array('value'=>'fa-italic','nombre' => 'Italic',
            ),array('value'=>'fa-joomla','nombre' => 'Joomla',
            ),array('value'=>'fa-jpy','nombre' => 'Jpy',
            ),array('value'=>'fa-jsfiddle','nombre' => 'Jsfiddle',
            ),array('value'=>'fa-key','nombre' => 'Key',
            ),array('value'=>'fa-keyboard-o','nombre' => 'Keyboard o',
            ),array('value'=>'fa-krw','nombre' => 'Krw',
            ),array('value'=>'fa-language','nombre' => 'Language',
            ),array('value'=>'fa-laptop','nombre' => 'Laptop',
            ),array('value'=>'fa-lastfm','nombre' => 'Lastfm',
            ),array('value'=>'fa-lastfm-square','nombre' => 'Lastfm square',
            ),array('value'=>'fa-leaf','nombre' => 'Leaf',
            ),array('value'=>'fa-leanpub','nombre' => 'Leanpub',
            ),array('value'=>'fa-lemon-o','nombre' => 'Lemon o',
            ),array('value'=>'fa-level-down','nombre' => 'Level down',
            ),array('value'=>'fa-level-up','nombre' => 'Level up',
            ),array('value'=>'fa-life-ring','nombre' => 'Life ring',
            ),array('value'=>'fa-lightbulb-o','nombre' => 'Lightbulb o',
            ),array('value'=>'fa-line-chart','nombre' => 'Line chart',
            ),array('value'=>'fa-link','nombre' => 'Link',
            ),array('value'=>'fa-linkedin','nombre' => 'Linkedin',
            ),array('value'=>'fa-linkedin-square','nombre' => 'Linkedin square',
            ),array('value'=>'fa-linux','nombre' => 'Linux',
            ),array('value'=>'fa-list','nombre' => 'List',
            ),array('value'=>'fa-list-alt','nombre' => 'List alt',
            ),array('value'=>'fa-list-ol','nombre' => 'List ol',
            ),array('value'=>'fa-list-ul','nombre' => 'List ul',
            ),array('value'=>'fa-location-arrow','nombre' => 'Location arrow',
            ),array('value'=>'fa-lock','nombre' => 'Lock',
            ),array('value'=>'fa-long-arrow-down','nombre' => 'Long arrow down',
            ),array('value'=>'fa-long-arrow-left','nombre' => 'Long arrow left',
            ),array('value'=>'fa-long-arrow-right','nombre' => 'Long arrow right',
            ),array('value'=>'fa-long-arrow-up','nombre' => 'Long arrow up',
            ),array('value'=>'fa-magic','nombre' => 'Magic',
            ),array('value'=>'fa-magnet','nombre' => 'Magnet',
            ),array('value'=>'fa-male','nombre' => 'Male',
            ),array('value'=>'fa-map','nombre' => 'Map',
            ),array('value'=>'fa-map-marker','nombre' => 'Map marker',
            ),array('value'=>'fa-map-o','nombre' => 'Map o',
            ),array('value'=>'fa-map-pin','nombre' => 'Map pin',
            ),array('value'=>'fa-map-signs','nombre' => 'Map signs',
            ),array('value'=>'fa-mars','nombre' => 'Mars',
            ),array('value'=>'fa-mars-double','nombre' => 'Mars double',
            ),array('value'=>'fa-mars-stroke','nombre' => 'Mars stroke',
            ),array('value'=>'fa-mars-stroke-h','nombre' => 'Mars stroke h',
            ),array('value'=>'fa-mars-stroke-v','nombre' => 'Mars stroke v',
            ),array('value'=>'fa-maxcdn','nombre' => 'Maxcdn',
            ),array('value'=>'fa-meanpath','nombre' => 'Meanpath',
            ),array('value'=>'fa-medium','nombre' => 'Medium',
            ),array('value'=>'fa-medkit','nombre' => 'Medkit',
            ),array('value'=>'fa-meh-o','nombre' => 'Meh o',
            ),array('value'=>'fa-mercury','nombre' => 'Mercury',
            ),array('value'=>'fa-microphone','nombre' => 'Microphone',
            ),array('value'=>'fa-microphone-slash','nombre' => 'Microphone slash',
            ),array('value'=>'fa-minus','nombre' => 'Minus',
            ),array('value'=>'fa-minus-circle','nombre' => 'Minus circle',
            ),array('value'=>'fa-minus-square','nombre' => 'Minus square',
            ),array('value'=>'fa-minus-square-o','nombre' => 'Minus square o',
            ),array('value'=>'fa-mobile','nombre' => 'Mobile',
            ),array('value'=>'fa-money','nombre' => 'Money',
            ),array('value'=>'fa-moon-o','nombre' => 'Moon o',
            ),array('value'=>'fa-motorcycle','nombre' => 'Motorcycle',
            ),array('value'=>'fa-mouse-pointer','nombre' => 'Mouse pointer',
            ),array('value'=>'fa-music','nombre' => 'Music',
            ),array('value'=>'fa-neuter','nombre' => 'Neuter',
            ),array('value'=>'fa-newspaper-o','nombre' => 'Newspaper o',
            ),array('value'=>'fa-object-group','nombre' => 'Object group',
            ),array('value'=>'fa-object-ungroup','nombre' => 'Object ungroup',
            ),array('value'=>'fa-odnoklassniki','nombre' => 'Odnoklassniki',
            ),array('value'=>'fa-odnoklassniki-square','nombre' => 'Odnoklassniki square',
            ),array('value'=>'fa-opencart','nombre' => 'Opencart',
            ),array('value'=>'fa-openid','nombre' => 'Openid',
            ),array('value'=>'fa-opera','nombre' => 'Opera',
            ),array('value'=>'fa-optin-monster','nombre' => 'Optin monster',
            ),array('value'=>'fa-outdent','nombre' => 'Outdent',
            ),array('value'=>'fa-pagelines','nombre' => 'Pagelines',
            ),array('value'=>'fa-paint-brush','nombre' => 'Paint brush',
            ),array('value'=>'fa-paper-plane','nombre' => 'Paper plane',
            ),array('value'=>'fa-paper-plane-o','nombre' => 'Paper plane o',
            ),array('value'=>'fa-paperclip','nombre' => 'Paperclip',
            ),array('value'=>'fa-paragraph','nombre' => 'Paragraph',
            ),array('value'=>'fa-pause','nombre' => 'Pause',
            ),array('value'=>'fa-paw','nombre' => 'Paw',
            ),array('value'=>'fa-paypal','nombre' => 'Paypal',
            ),array('value'=>'fa-pencil','nombre' => 'Pencil',
            ),array('value'=>'fa-pencil-square','nombre' => 'Pencil square',
            ),array('value'=>'fa-pencil-square-o','nombre' => 'Pencil square o',
            ),array('value'=>'fa-phone','nombre' => 'Phone',
            ),array('value'=>'fa-phone-square','nombre' => 'Phone square',
            ),array('value'=>'fa-picture-o','nombre' => 'Picture o',
            ),array('value'=>'fa-pie-chart','nombre' => 'Pie chart',
            ),array('value'=>'fa-pied-piper','nombre' => 'Pied piper',
            ),array('value'=>'fa-pied-piper-alt','nombre' => 'Pied piper alt',
            ),array('value'=>'fa-pinterest','nombre' => 'Pinterest',
            ),array('value'=>'fa-pinterest-p','nombre' => 'Pinterest p',
            ),array('value'=>'fa-pinterest-square','nombre' => 'Pinterest square',
            ),array('value'=>'fa-plane','nombre' => 'Plane',
            ),array('value'=>'fa-play','nombre' => 'Play',
            ),array('value'=>'fa-play-circle','nombre' => 'Play circle',
            ),array('value'=>'fa-play-circle-o','nombre' => 'Play circle o',
            ),array('value'=>'fa-plug','nombre' => 'Plug',
            ),array('value'=>'fa-plus','nombre' => 'Plus',
            ),array('value'=>'fa-plus-circle','nombre' => 'Plus circle',
            ),array('value'=>'fa-plus-square','nombre' => 'Plus square',
            ),array('value'=>'fa-plus-square-o','nombre' => 'Plus square o',
            ),array('value'=>'fa-power-off','nombre' => 'Power off',
            ),array('value'=>'fa-print','nombre' => 'Print',
            ),array('value'=>'fa-puzzle-piece','nombre' => 'Puzzle piece',
            ),array('value'=>'fa-qq','nombre' => 'Qq',
            ),array('value'=>'fa-qrcode','nombre' => 'Qrcode',
            ),array('value'=>'fa-question','nombre' => 'Question',
            ),array('value'=>'fa-question-circle','nombre' => 'Question circle',
            ),array('value'=>'fa-quote-left','nombre' => 'Quote left',
            ),array('value'=>'fa-quote-right','nombre' => 'Quote right',
            ),array('value'=>'fa-random','nombre' => 'Random',
            ),array('value'=>'fa-rebel','nombre' => 'Rebel',
            ),array('value'=>'fa-recycle','nombre' => 'Recycle',
            ),array('value'=>'fa-reddit','nombre' => 'Reddit',
            ),array('value'=>'fa-reddit-square','nombre' => 'Reddit square',
            ),array('value'=>'fa-refresh','nombre' => 'Refresh',
            ),array('value'=>'fa-registered','nombre' => 'Registered',
            ),array('value'=>'fa-renren','nombre' => 'Renren',
            ),array('value'=>'fa-repeat','nombre' => 'Repeat',
            ),array('value'=>'fa-reply','nombre' => 'Reply',
            ),array('value'=>'fa-reply-all','nombre' => 'Reply all',
            ),array('value'=>'fa-retweet','nombre' => 'Retweet',
            ),array('value'=>'fa-road','nombre' => 'Road',
            ),array('value'=>'fa-rocket','nombre' => 'Rocket',
            ),array('value'=>'fa-rss','nombre' => 'Rss',
            ),array('value'=>'fa-rss-square','nombre' => 'Rss square',
            ),array('value'=>'fa-rub','nombre' => 'Rub',
            ),array('value'=>'fa-safari','nombre' => 'Safari',
            ),array('value'=>'fa-scissors','nombre' => 'Scissors',
            ),array('value'=>'fa-search','nombre' => 'Search',
            ),array('value'=>'fa-search-minus','nombre' => 'Search minus',
            ),array('value'=>'fa-search-plus','nombre' => 'Search plus',
            ),array('value'=>'fa-sellsy','nombre' => 'Sellsy',
            ),array('value'=>'fa-server','nombre' => 'Server',
            ),array('value'=>'fa-share','nombre' => 'Share',
            ),array('value'=>'fa-share-alt','nombre' => 'Share alt',
            ),array('value'=>'fa-share-alt-square','nombre' => 'Share alt square',
            ),array('value'=>'fa-share-square','nombre' => 'Share square',
            ),array('value'=>'fa-share-square-o','nombre' => 'Share square o',
            ),array('value'=>'fa-shield','nombre' => 'Shield',
            ),array('value'=>'fa-ship','nombre' => 'Ship',
            ),array('value'=>'fa-shirtsinbulk','nombre' => 'Shirtsinbulk',
            ),array('value'=>'fa-shopping-cart','nombre' => 'Shopping cart',
            ),array('value'=>'fa-sign-in','nombre' => 'Sign in',
            ),array('value'=>'fa-sign-out','nombre' => 'Sign out',
            ),array('value'=>'fa-signal','nombre' => 'Signal',
            ),array('value'=>'fa-simplybuilt','nombre' => 'Simplybuilt',
            ),array('value'=>'fa-sitemap','nombre' => 'Sitemap',
            ),array('value'=>'fa-skyatlas','nombre' => 'Skyatlas',
            ),array('value'=>'fa-skype','nombre' => 'Skype',
            ),array('value'=>'fa-slack','nombre' => 'Slack',
            ),array('value'=>'fa-sliders','nombre' => 'Sliders',
            ),array('value'=>'fa-slideshare','nombre' => 'Slideshare',
            ),array('value'=>'fa-smile-o','nombre' => 'Smile o',
            ),array('value'=>'fa-sort','nombre' => 'Sort',
            ),array('value'=>'fa-sort-alpha-asc','nombre' => 'Sort alpha asc',
            ),array('value'=>'fa-sort-alpha-desc','nombre' => 'Sort alpha desc',
            ),array('value'=>'fa-sort-amount-asc','nombre' => 'Sort amount asc',
            ),array('value'=>'fa-sort-amount-desc','nombre' => 'Sort amount desc',
            ),array('value'=>'fa-sort-asc','nombre' => 'Sort asc',
            ),array('value'=>'fa-sort-desc','nombre' => 'Sort desc',
            ),array('value'=>'fa-sort-numeric-asc','nombre' => 'Sort numeric asc',
            ),array('value'=>'fa-sort-numeric-desc','nombre' => 'Sort numeric desc',
            ),array('value'=>'fa-soundcloud','nombre' => 'Soundcloud',
            ),array('value'=>'fa-space-shuttle','nombre' => 'Space shuttle',
            ),array('value'=>'fa-spinner','nombre' => 'Spinner',
            ),array('value'=>'fa-spoon','nombre' => 'Spoon',
            ),array('value'=>'fa-spotify','nombre' => 'Spotify',
            ),array('value'=>'fa-square','nombre' => 'Square',
            ),array('value'=>'fa-square-o','nombre' => 'Square o',
            ),array('value'=>'fa-stack-exchange','nombre' => 'Stack exchange',
            ),array('value'=>'fa-stack-overflow','nombre' => 'Stack overflow',
            ),array('value'=>'fa-star','nombre' => 'Star',
            ),array('value'=>'fa-star-half','nombre' => 'Star half',
            ),array('value'=>'fa-star-half-o','nombre' => 'Star half o',
            ),array('value'=>'fa-star-o','nombre' => 'Star o',
            ),array('value'=>'fa-steam','nombre' => 'Steam',
            ),array('value'=>'fa-steam-square','nombre' => 'Steam square',
            ),array('value'=>'fa-step-backward','nombre' => 'Step backward',
            ),array('value'=>'fa-step-forward','nombre' => 'Step forward',
            ),array('value'=>'fa-stethoscope','nombre' => 'Stethoscope',
            ),array('value'=>'fa-sticky-note','nombre' => 'Sticky note',
            ),array('value'=>'fa-sticky-note-o','nombre' => 'Sticky note o',
            ),array('value'=>'fa-stop','nombre' => 'Stop',
            ),array('value'=>'fa-street-view','nombre' => 'Street view',
            ),array('value'=>'fa-strikethrough','nombre' => 'Strikethrough',
            ),array('value'=>'fa-stumbleupon','nombre' => 'Stumbleupon',
            ),array('value'=>'fa-stumbleupon-circle','nombre' => 'Stumbleupon circle',
            ),array('value'=>'fa-subscript','nombre' => 'Subscript',
            ),array('value'=>'fa-subway','nombre' => 'Subway',
            ),array('value'=>'fa-suitcase','nombre' => 'Suitcase',
            ),array('value'=>'fa-sun-o','nombre' => 'Sun o',
            ),array('value'=>'fa-superscript','nombre' => 'Superscript',
            ),array('value'=>'fa-table','nombre' => 'Table',
            ),array('value'=>'fa-tablet','nombre' => 'Tablet',
            ),array('value'=>'fa-tachometer','nombre' => 'Tachometer',
            ),array('value'=>'fa-tag','nombre' => 'Tag',
            ),array('value'=>'fa-tags','nombre' => 'Tags',
            ),array('value'=>'fa-tasks','nombre' => 'Tasks',
            ),array('value'=>'fa-taxi','nombre' => 'Taxi',
            ),array('value'=>'fa-television','nombre' => 'Television',
            ),array('value'=>'fa-tencent-weibo','nombre' => 'Tencent weibo',
            ),array('value'=>'fa-terminal','nombre' => 'Terminal',
            ),array('value'=>'fa-text-height','nombre' => 'Text height',
            ),array('value'=>'fa-text-width','nombre' => 'Text width',
            ),array('value'=>'fa-th','nombre' => 'Th',
            ),array('value'=>'fa-th-large','nombre' => 'Th large',
            ),array('value'=>'fa-th-list','nombre' => 'Th list',
            ),array('value'=>'fa-thumb-tack','nombre' => 'Thumb tack',
            ),array('value'=>'fa-thumbs-down','nombre' => 'Thumbs down',
            ),array('value'=>'fa-thumbs-o-down','nombre' => 'Thumbs o down',
            ),array('value'=>'fa-thumbs-o-up','nombre' => 'Thumbs o up',
            ),array('value'=>'fa-thumbs-up','nombre' => 'Thumbs up',
            ),array('value'=>'fa-ticket','nombre' => 'Ticket',
            ),array('value'=>'fa-times','nombre' => 'Times',
            ),array('value'=>'fa-times-circle','nombre' => 'Times circle',
            ),array('value'=>'fa-times-circle-o','nombre' => 'Times circle o',
            ),array('value'=>'fa-tint','nombre' => 'Tint',
            ),array('value'=>'fa-toggle-off','nombre' => 'Toggle off',
            ),array('value'=>'fa-toggle-on','nombre' => 'Toggle on',
            ),array('value'=>'fa-trademark','nombre' => 'Trademark',
            ),array('value'=>'fa-train','nombre' => 'Train',
            ),array('value'=>'fa-transgender','nombre' => 'Transgender',
            ),array('value'=>'fa-transgender-alt','nombre' => 'Transgender alt',
            ),array('value'=>'fa-trash','nombre' => 'Trash',
            ),array('value'=>'fa-trash-o','nombre' => 'Trash o',
            ),array('value'=>'fa-tree','nombre' => 'Tree',
            ),array('value'=>'fa-trello','nombre' => 'Trello',
            ),array('value'=>'fa-tripadvisor','nombre' => 'Tripadvisor',
            ),array('value'=>'fa-trophy','nombre' => 'Trophy',
            ),array('value'=>'fa-truck','nombre' => 'Truck',
            ),array('value'=>'fa-try','nombre' => 'Try',
            ),array('value'=>'fa-tty','nombre' => 'Tty',
            ),array('value'=>'fa-tumblr','nombre' => 'Tumblr',
            ),array('value'=>'fa-tumblr-square','nombre' => 'Tumblr square',
            ),array('value'=>'fa-twitch','nombre' => 'Twitch',
            ),array('value'=>'fa-twitter','nombre' => 'Twitter',
            ),array('value'=>'fa-twitter-square','nombre' => 'Twitter square',
            ),array('value'=>'fa-umbrella','nombre' => 'Umbrella',
            ),array('value'=>'fa-underline','nombre' => 'Underline',
            ),array('value'=>'fa-undo','nombre' => 'Undo',
            ),array('value'=>'fa-university','nombre' => 'University',
            ),array('value'=>'fa-unlock','nombre' => 'Unlock',
            ),array('value'=>'fa-unlock-alt','nombre' => 'Unlock alt',
            ),array('value'=>'fa-upload','nombre' => 'Upload',
            ),array('value'=>'fa-usd','nombre' => 'Usd',
            ),array('value'=>'fa-user','nombre' => 'User',
            ),array('value'=>'fa-user-md','nombre' => 'User md',
            ),array('value'=>'fa-user-plus','nombre' => 'User plus',
            ),array('value'=>'fa-user-secret','nombre' => 'User secret',
            ),array('value'=>'fa-user-times','nombre' => 'User times',
            ),array('value'=>'fa-users','nombre' => 'Users',
            ),array('value'=>'fa-venus','nombre' => 'Venus',
            ),array('value'=>'fa-venus-double','nombre' => 'Venus double',
            ),array('value'=>'fa-venus-mars','nombre' => 'Venus mars',
            ),array('value'=>'fa-viacoin','nombre' => 'Viacoin',
            ),array('value'=>'fa-video-camera','nombre' => 'Video camera',
            ),array('value'=>'fa-vimeo','nombre' => 'Vimeo',
            ),array('value'=>'fa-vimeo-square','nombre' => 'Vimeo square',
            ),array('value'=>'fa-vine','nombre' => 'Vine',
            ),array('value'=>'fa-vk','nombre' => 'Vk',
            ),array('value'=>'fa-volume-down','nombre' => 'Volume down',
            ),array('value'=>'fa-volume-off','nombre' => 'Volume off',
            ),array('value'=>'fa-volume-up','nombre' => 'Volume up',
            ),array('value'=>'fa-weibo','nombre' => 'Weibo',
            ),array('value'=>'fa-weixin','nombre' => 'Weixin',
            ),array('value'=>'fa-whatsapp','nombre' => 'Whatsapp',
            ),array('value'=>'fa-wheelchair','nombre' => 'Wheelchair',
            ),array('value'=>'fa-wifi','nombre' => 'Wifi',
            ),array('value'=>'fa-wikipedia-w','nombre' => 'Wikipedia w',
            ),array('value'=>'fa-windows','nombre' => 'Windows',
            ),array('value'=>'fa-wordpress','nombre' => 'Wordpress',
            ),array('value'=>'fa-wrench','nombre' => 'Wrench',
            ),array('value'=>'fa-xing','nombre' => 'Xing',
            ),array('value'=>'fa-xing-square','nombre' => 'Xing square',
            ),array('value'=>'fa-y-combinator','nombre' => 'Y combinator',
            ),array('value'=>'fa-yahoo','nombre' => 'Yahoo',
            ),array('value'=>'fa-yelp','nombre' => 'Yelp',
            ),array('value'=>'fa-youtube','nombre' => 'Youtube',
            ),array('value'=>'fa-youtube-play','nombre' => 'Youtube play',
            ),array('value'=>'fa-youtube-square','nombre' => 'Youtube square')
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Departamento the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}
