<?php

/**
 * This is the model class for table "api_servicio.aplicacion".
 *
 * The followings are the available columns in table 'api_servicio.aplicacion':
 * @property integer $id
 * @property string $nombre
 * @property string $codigo
 * @property string $key
 * @property string $password
 * @property integer $encriptacion_id
 * @property integer $plataforma_id
 * @property string $url
 * @property string $datos_consultados
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $cedula_contacto
 * @property string $nombre_apellido_contacto
 * @property string $telefono_fijo_contacto
 * @property string $telefono_celular_contacto
 * @property string $email_contacto
 * @property integer $rif_institucion
 * @property string $tdocumento_identidad
 * @property string $username
 * @property string $siglas_institucion
 *
 * The followings are the available model relations:
 * @property Encriptacion $encriptacion
 * @property Plataforma $plataforma
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 */
class Aplicacion extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'api_servicio.aplicacion';
    }

    public function crearPassword() {
        $estatus = false;
        //return CPasswordHelper::verifyPassword($clave,$this->clave);
        //$salt = md5("aplicacionMovil");      
        $salt = Yii::app()->params['salt'];
        
        // este salt debe contener el valor del main
        // md5('G-200000929|CALIFICACIONESMPPEOPSU|2014-01-08 15:05:27|ysKCJdeTd4gSkEdKxuVFV7G');
        $clave = md5($this->rif_institucion.'|'.$this->nombre.'|'.$this->fecha_ini.'|'.$this->key.$salt);
       
        return $clave;
        //----------condicion para comparar si el password es igual al que esta almacenado en la base de datos
        //                else if ($this->password === $clave){
        //    		// password is correct
        //    		return $this->password;
        //		}
        //fin de la condicion antes mencionada 
        return $clave;
    }

    public function crearKey() {
        //-----------------------------------------------------------
        $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; //posibles caracteres a usar
        $numerodeletras = 24; //numero de letras para generar el texto
        $key = ""; //variable para almacenar la cadena generada
        for ($i = 0; $i < $numerodeletras; $i++) {
            $key .= substr($caracteres, rand(0, strlen($caracteres)), 1); /* Extraemos 1 caracter de los caracteres 
              entre el rango 0 a Numero de letras que tiene la cadena */
        }
        //echo $password;
        return $key;
        //---------------------------------------------------------
        //----------condicion para comparar si el password es igual al que esta almacenado en la base de datos
//                else if ($this->password === $clave){
//    		// password is correct
//    		return $this->password;
//		}
        //fin de la condicion antes mencionada 
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, key, password, encriptacion_id, plataforma_id, url, datos_consultados, usuario_ini_id, fecha_ini, cedula_contacto, telefono_fijo_contacto, telefono_celular_contacto, email_contacto, rif_institucion, tdocumento_identidad, siglas_institucion, username', 'required'),
            array('encriptacion_id, plataforma_id, usuario_ini_id, usuario_act_id, cedula_contacto', 'numerical', 'integerOnly' => true),
            array('nombre, codigo, email_contacto', 'length', 'max' => 100),
            array('nombre', 'unique'),
            array('key, password', 'unique'),
            array('key, password', 'length', 'max' => 300),
            array('url', 'length', 'max' => 255),
            array('estatus, tdocumento_identidad', 'length', 'max' => 1),
            array('nombre_apellido_contacto', 'length', 'max' => 80),
            array('siglas_institucion', 'length', 'max' => 20),
            array('username', 'length', 'max' => 40),
            array('telefono_fijo_contacto, telefono_celular_contacto', 'length', 'max' => 20),
            array('fecha_ini,fecha_act, fecha_elim', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombre, codigo, key, password, encriptacion_id, plataforma_id, url, datos_consultados, usuario_ini_id, usuario_act_id, fecha_ini, fecha_act, fecha_elim, estatus, cedula_contacto, nombre_apellido_contacto, telefono_fijo_contacto, telefono_celular_contacto, email_contacto, rif_institucion, tdocumento_identidad, siglas_institucion, username', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'encriptacion' => array(self::BELONGS_TO, 'Encriptacion', 'encriptacion_id'),
            'plataforma' => array(self::BELONGS_TO, 'Plataforma', 'plataforma_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nombre' => 'Nombre',
            'codigo' => 'Código',
            'key' => 'Key',
            'password' => 'Password',
            'encriptacion_id' => 'Encriptación',
            'plataforma_id' => 'Plataforma',
            'url' => 'Url',
            'datos_consultados' => 'Datos Consultados',
            'usuario_ini_id' => 'Usuario Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_ini' => 'Fecha Ini',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'cedula_contacto' => 'Cédula Contacto',
            'nombre_apellido_contacto' => 'Nombre y Apellido Contacto',
            'telefono_fijo_contacto' => 'Telefono Fijo Contacto',
            'telefono_celular_contacto' => 'Telefono Celular Contacto',
            'email_contacto' => 'Email Contacto',
            'rif_institucion' => 'Rif de la Institución',
            'tdocumento_identidad' => 'Tipo de Documento de Identidad',
            'siglas_institucion' => 'Siglas de la Institución',
            'username' => 'Nombre de Usuario con hash md5',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        if (is_numeric($this->id)) {
            $criteria->compare('id', $this->id);
        }

        $criteria->compare('nombre', strtoupper($this->nombre), true);

        $criteria->compare('codigo', $this->codigo, true);
        $criteria->compare('key', $this->key, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('encriptacion_id', $this->encriptacion_id);
        $criteria->compare('plataforma_id', $this->plataforma_id);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('datos_consultados', $this->datos_consultados, true);

        if (is_numeric($this->usuario_ini_id)) {
            $criteria->compare('usuario_ini_id', $this->usuario_ini_id);
        }

        if (is_numeric($this->usuario_act_id)) {
            $criteria->compare('usuario_act_id', $this->usuario_act_id);
        }
        if (strlen($this->fecha_ini) > 0 && Utiles::dateCheck($this->fecha_ini)) {
            //$criteria->compare('fecha_ini',$this->fecha_ini,true);
            $criteria->addCondition("TO_CHAR(t.fecha_ini,'DD-MM-YYYY') = :fecha_ini");
            $criteria->params = array(':fecha_ini' => $this->fecha_ini);
        }

        if (strlen($this->fecha_act) > 0 && Utiles::dateCheck($this->fecha_act)) {
            //$criteria->compare('fecha_act',$this->fecha_act,true);
            $criteria->addCondition("TO_CHAR(t.fecha_act,'DD-MM-YYYY') = :fecha_act");
            $criteria->params = array_merge($criteria->params, array(':fecha_act' => $this->fecha_act));
        }
        $criteria->compare('fecha_elim', $this->fecha_elim, true);

        if (in_array($this->estatus, array('A', 'I', 'E', 'B'))) {
            $criteria->compare('estatus', $this->estatus, true);
        }
        $criteria->compare('cedula_contacto', $this->cedula_contacto);
        $criteria->compare('nombre_apellido_contacto', $this->nombre_apellido_contacto, true);
        $criteria->compare('telefono_fijo_contacto', $this->telefono_fijo_contacto, true);
        $criteria->compare('telefono_celular_contacto', $this->telefono_celular_contacto, true);
        $criteria->compare('email_contacto', $this->email_contacto, true);
        $criteria->compare('rif_institucion', $this->rif_institucion);
        
        $criteria->compare('siglas_institucion', $this->siglas_institucion, true);
        $criteria->compare('username', $this->username, true);

        if (in_array($this->tdocumento_identidad, array('V', 'E', 'P'))) {
            $criteria->compare('tdocumento_identidad', $this->tdocumento_identidad, true);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function login($username, $password, $serviceName=null){
        
        $cacheIndex = 'RESTCLIENT-AUTH-SERV:'.md5("$username:$password:$serviceName");
        
        $resultado = Yii::app()->cache->get($cacheIndex);
        
        if($resultado !== $password){

            $sql = "SELECT password FROM api_servicio.aplicacion a WHERE a.username = :username";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":username", $username, PDO::PARAM_STR);
            $resultado = $consulta->queryScalar();
            if($resultado === $password){
                Yii::app()->cache->get($cacheIndex, $resultado, 86400);
                return true;
            }
            
        }
        else{
            return true;
        }
        return false;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Aplicacion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
