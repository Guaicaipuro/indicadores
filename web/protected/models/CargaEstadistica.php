<?php

/**
 * This is the model class for table "gplantel.carga_estadistica".
 *
 * The followings are the available columns in table 'gplantel.carga_estadistica':
 * @property integer $id
 * @property integer $plantel_id
 * @property integer $periodo_id
 * @property integer $tipo_atencion_id
 * @property integer $mat_inicial_m
 * @property integer $mat_incorporacion_m
 * @property integer $mat_abandono_m
 * @property integer $mat_final_m
 * @property integer $mat_inicial_f
 * @property integer $mat_incorporacion_f
 * @property integer $mat_abandono_f
 * @property integer $mat_final_f
 * @property integer $pre_inicial_m
 * @property integer $pre_incorporacion_m
 * @property integer $pre_abandono_m
 * @property integer $pre_final_m
 * @property integer $pre_inicial_f
 * @property integer $pre_incorporacion_f
 * @property integer $pre_abandono_f
 * @property integer $pre_final_f
 * @property integer $pri_inicial_m
 * @property integer $pri_incorporacion_m
 * @property integer $pri_abandono_m
 * @property integer $pri_final_m
 * @property integer $pri_inicial_f
 * @property integer $pri_incorporacion_f
 * @property integer $pri_abandono_f
 * @property integer $pri_final_f
 * @property integer $meg_inicial_m
 * @property integer $meg_incorporacion_m
 * @property integer $meg_abandono_m
 * @property integer $meg_final_m
 * @property integer $meg_inicial_f
 * @property integer $meg_incorporacion_f
 * @property integer $meg_abandono_f
 * @property integer $meg_final_f
 * @property integer $met_inicial_m
 * @property integer $met_incorporacion_m
 * @property integer $met_abandono_m
 * @property integer $met_final_m
 * @property integer $met_inicial_f
 * @property integer $met_incorporacion_f
 * @property integer $met_abandono_f
 * @property integer $met_final_f
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $nivel_id
 * @property integer $pmt_inicial_f
 * @property integer $pmt_incorporacion_f
 * @property integer $pmt_abandono_f
 * @property integer $pmt_final_f
 * @property integer $pmt_inicial_m
 * @property integer $pmt_incorporacion_m
 * @property integer $pmt_abandono_m
 * @property integer $pmt_final_m
 */
class CargaEstadistica extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'gplantel.carga_estadistica';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('plantel_id, periodo_id, tipo_atencion_id, usuario_ini_id', 'required'),
            array('plantel_id, periodo_id, tipo_atencion_id, mat_inicial_m, mat_incorporacion_m, mat_abandono_m, mat_final_m, mat_inicial_f, mat_incorporacion_f, mat_abandono_f, mat_final_f, pre_inicial_m, pre_incorporacion_m, pre_abandono_m, pre_final_m, pre_inicial_f, pre_incorporacion_f, pre_abandono_f, pre_final_f, pri_inicial_m, pri_incorporacion_m, pri_abandono_m, pri_final_m, pri_inicial_f, pri_incorporacion_f, pri_abandono_f, pri_final_f, meg_inicial_m, meg_incorporacion_m, meg_abandono_m, meg_final_m, meg_inicial_f, meg_incorporacion_f, meg_abandono_f, meg_final_f, met_inicial_m, met_incorporacion_m, met_abandono_m, met_final_m, met_inicial_f, met_incorporacion_f, met_abandono_f, met_final_f, usuario_ini_id, usuario_act_id, nivel_id, pmt_inicial_f, pmt_incorporacion_f, pmt_abandono_f, pmt_final_f, pmt_inicial_m, pmt_incorporacion_m, pmt_abandono_m, pmt_final_m', 'numerical', 'integerOnly'=>true),
            array('estatus', 'length', 'max'=>1),
            array('mat_abandono_m','validarAbandono','index0'=>'mat_inicial_m', 'index1'=>'mat_incorporacion_m'),
            array('mat_abandono_f','validarAbandono','index0'=>'mat_inicial_f', 'index1'=>'mat_incorporacion_f'),
            array('pre_abandono_m','validarAbandono','index0'=>'pre_inicial_m', 'index1'=>'mat_incorporacion_m'),
            array('pre_abandono_f','validarAbandono','index0'=>'pre_inicial_f', 'index1'=>'pre_incorporacion_f'),
            array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
            array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
            array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, plantel_id, periodo_id, tipo_atencion_id, mat_inicial_m, mat_incorporacion_m, mat_abandono_m, mat_final_m, mat_inicial_f, mat_incorporacion_f, mat_abandono_f, mat_final_f, pre_inicial_m, pre_incorporacion_m, pre_abandono_m, pre_final_m, pre_inicial_f, pre_incorporacion_f, pre_abandono_f, pre_final_f, pri_inicial_m, pri_incorporacion_m, pri_abandono_m, pri_final_m, pri_inicial_f, pri_incorporacion_f, pri_abandono_f, pri_final_f, meg_inicial_m, meg_incorporacion_m, meg_abandono_m, meg_final_m, meg_inicial_f, meg_incorporacion_f, meg_abandono_f, meg_final_f, met_inicial_m, met_incorporacion_m, met_abandono_m, met_final_m, met_inicial_f, met_incorporacion_f, met_abandono_f, met_final_f, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus, nivel_id, pmt_inicial_f, pmt_incorporacion_f, pmt_abandono_f, pmt_final_f, pmt_inicial_m, pmt_incorporacion_m, pmt_abandono_m, pmt_final_m', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'plantel_id' => 'Plantel',
            'periodo_id' => 'Periodo',
            'tipo_atencion_id' => 'Tipo Atencion',
            'mat_inicial_m' => 'Maternal Inicial Masculino',
            'mat_incorporacion_m' => 'Maternal Incorporacion Masculino',
            'mat_abandono_m' => 'Maternal Abandono Masculino',
            'mat_final_m' => 'Maternal Final Masculino',
            'mat_inicial_f' => 'Maternal Inicial Femenino',
            'mat_incorporacion_f' => 'Maternal Incorporacion Femenino',
            'mat_abandono_f' => 'Maternal Abandono Femenino',
            'mat_final_f' => 'Maternal Final Femenino',
            'pre_inicial_m' => 'Preescolar Inicial Masculino',
            'pre_incorporacion_m' => 'Preescolar Incorporacion Masculino',
            'pre_abandono_m' => 'Preescolar Abandono Masculino',
            'pre_final_m' => 'Preescolar Final Masculino',
            'pre_inicial_f' => 'Preescolar Inicial Femenino',
            'pre_incorporacion_f' => 'Preescolar Incorporacion Femenino',
            'pre_abandono_f' => 'Preescolar Abandono Femenino',
            'pre_final_f' => 'Preescolar Final Femenino',
            'pri_inicial_m' => 'Primaria Inicial Masculino',
            'pri_incorporacion_m' => 'Primaria Incorporacion Masculino',
            'pri_abandono_m' => 'Primaria Abandono Masculino',
            'pri_final_m' => 'Primaria Final Masculino',
            'pri_inicial_f' => 'Primaria Inicial Femenino',
            'pri_incorporacion_f' => 'Primaria Incorporacion Femenino',
            'pri_abandono_f' => 'Primaria Abandono Femenino',
            'pri_final_f' => 'Primaria Final Femenino',
            'meg_inicial_m' => 'Media General Inicial Masculino',
            'meg_incorporacion_m' => 'Media General Incorporacion Masculino',
            'meg_abandono_m' => 'Media General Abandono Masculino',
            'meg_final_m' => 'Media General Final Masculino',
            'meg_inicial_f' => 'Media General Inicial Femenino',
            'meg_incorporacion_f' => 'Media General Incorporacion Femenino',
            'meg_abandono_f' => 'Media General Abandono Femenino',
            'meg_final_f' => 'Media General Final Femenino',
            'met_inicial_m' => 'Media Técnica Inicial Masculino',
            'met_incorporacion_m' => 'Media Técnica Incorporacion Masculino',
            'met_abandono_m' => 'Media Técnica Abandono Masculino',
            'met_final_m' => 'Media Técnica Final Masculino',
            'met_inicial_f' => 'Media Técnica Inicial Femenino',
            'met_incorporacion_f' => 'Media Técnica Incorporacion Femenino',
            'met_abandono_f' => 'Media Técnica Abandono Femenino',
            'met_final_f' => 'Media Técnica Final Femenino',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'nivel_id' => 'Nivel',
            'pmt_inicial_f' => 'Matrícula Inicial Femenino',
            'pmt_incorporacion_f' => 'Matrícula Incorporacion Femenino',
            'pmt_abandono_f' => 'Matrícula Abandono Femenino',
            'pmt_final_f' => 'Matrícula Final Femenino',
            'pmt_inicial_m' => 'Matrícula Inicial Masculino',
            'pmt_incorporacion_m' => 'Matrícula Incorporacion Masculino',
            'pmt_abandono_m' => 'Matrícula Abandono Masculino',
            'pmt_final_m' => 'Matrícula Final Masculino',
        );
    }

    public function validarAbandono($attr, $params) {
        $attributesName = $this->attributeLabels();
        if ($this->$attr>($this->$params['index0']+$this->$params['index1'])) {
            $this->addError($attr, 'El campo <strong>'.$attributesName[$attr].'</strong> no puede ser superior a <strong>'.($this->$params['index0']+$this->$params['index1']).'</strong>' );
        }
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        if(is_numeric($this->id)) $criteria->compare('id',$this->id);
        if(is_numeric($this->plantel_id)) $criteria->compare('plantel_id',$this->plantel_id);
        if(is_numeric($this->periodo_id)) $criteria->compare('periodo_id',$this->periodo_id);
        if(is_numeric($this->tipo_atencion_id)) $criteria->compare('tipo_atencion_id',$this->tipo_atencion_id);
        if(is_numeric($this->mat_inicial_m)) $criteria->compare('mat_inicial_m',$this->mat_inicial_m);
        if(is_numeric($this->mat_incorporacion_m)) $criteria->compare('mat_incorporacion_m',$this->mat_incorporacion_m);
        if(is_numeric($this->mat_abandono_m)) $criteria->compare('mat_abandono_m',$this->mat_abandono_m);
        if(is_numeric($this->mat_final_m)) $criteria->compare('mat_final_m',$this->mat_final_m);
        if(is_numeric($this->mat_inicial_f)) $criteria->compare('mat_inicial_f',$this->mat_inicial_f);
        if(is_numeric($this->mat_incorporacion_f)) $criteria->compare('mat_incorporacion_f',$this->mat_incorporacion_f);
        if(is_numeric($this->mat_abandono_f)) $criteria->compare('mat_abandono_f',$this->mat_abandono_f);
        if(is_numeric($this->mat_final_f)) $criteria->compare('mat_final_f',$this->mat_final_f);
        if(is_numeric($this->pre_inicial_m)) $criteria->compare('pre_inicial_m',$this->pre_inicial_m);
        if(is_numeric($this->pre_incorporacion_m)) $criteria->compare('pre_incorporacion_m',$this->pre_incorporacion_m);
        if(is_numeric($this->pre_abandono_m)) $criteria->compare('pre_abandono_m',$this->pre_abandono_m);
        if(is_numeric($this->pre_final_m)) $criteria->compare('pre_final_m',$this->pre_final_m);
        if(is_numeric($this->pre_inicial_f)) $criteria->compare('pre_inicial_f',$this->pre_inicial_f);
        if(is_numeric($this->pre_incorporacion_f)) $criteria->compare('pre_incorporacion_f',$this->pre_incorporacion_f);
        if(is_numeric($this->pre_abandono_f)) $criteria->compare('pre_abandono_f',$this->pre_abandono_f);
        if(is_numeric($this->pre_final_f)) $criteria->compare('pre_final_f',$this->pre_final_f);
        if(is_numeric($this->pri_inicial_m)) $criteria->compare('pri_inicial_m',$this->pri_inicial_m);
        if(is_numeric($this->pri_incorporacion_m)) $criteria->compare('pri_incorporacion_m',$this->pri_incorporacion_m);
        if(is_numeric($this->pri_abandono_m)) $criteria->compare('pri_abandono_m',$this->pri_abandono_m);
        if(is_numeric($this->pri_final_m)) $criteria->compare('pri_final_m',$this->pri_final_m);
        if(is_numeric($this->pri_inicial_f)) $criteria->compare('pri_inicial_f',$this->pri_inicial_f);
        if(is_numeric($this->pri_incorporacion_f)) $criteria->compare('pri_incorporacion_f',$this->pri_incorporacion_f);
        if(is_numeric($this->pri_abandono_f)) $criteria->compare('pri_abandono_f',$this->pri_abandono_f);
        if(is_numeric($this->pri_final_f)) $criteria->compare('pri_final_f',$this->pri_final_f);
        if(is_numeric($this->meg_inicial_m)) $criteria->compare('meg_inicial_m',$this->meg_inicial_m);
        if(is_numeric($this->meg_incorporacion_m)) $criteria->compare('meg_incorporacion_m',$this->meg_incorporacion_m);
        if(is_numeric($this->meg_abandono_m)) $criteria->compare('meg_abandono_m',$this->meg_abandono_m);
        if(is_numeric($this->meg_final_m)) $criteria->compare('meg_final_m',$this->meg_final_m);
        if(is_numeric($this->meg_inicial_f)) $criteria->compare('meg_inicial_f',$this->meg_inicial_f);
        if(is_numeric($this->meg_incorporacion_f)) $criteria->compare('meg_incorporacion_f',$this->meg_incorporacion_f);
        if(is_numeric($this->meg_abandono_f)) $criteria->compare('meg_abandono_f',$this->meg_abandono_f);
        if(is_numeric($this->meg_final_f)) $criteria->compare('meg_final_f',$this->meg_final_f);
        if(is_numeric($this->met_inicial_m)) $criteria->compare('met_inicial_m',$this->met_inicial_m);
        if(is_numeric($this->met_incorporacion_m)) $criteria->compare('met_incorporacion_m',$this->met_incorporacion_m);
        if(is_numeric($this->met_abandono_m)) $criteria->compare('met_abandono_m',$this->met_abandono_m);
        if(is_numeric($this->met_final_m)) $criteria->compare('met_final_m',$this->met_final_m);
        if(is_numeric($this->met_inicial_f)) $criteria->compare('met_inicial_f',$this->met_inicial_f);
        if(is_numeric($this->met_incorporacion_f)) $criteria->compare('met_incorporacion_f',$this->met_incorporacion_f);
        if(is_numeric($this->met_abandono_f)) $criteria->compare('met_abandono_f',$this->met_abandono_f);
        if(is_numeric($this->met_final_f)) $criteria->compare('met_final_f',$this->met_final_f);
        if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
        // if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
        if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
        if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
        // if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
        if(Utiles::isValidDate($this->fecha_elim, 'y-m-d')) $criteria->compare('fecha_elim',$this->fecha_elim);
        // if(strlen($this->fecha_elim)>0) $criteria->compare('fecha_elim',$this->fecha_elim,true);
        if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
        if(is_numeric($this->nivel_id)) $criteria->compare('nivel_id',$this->nivel_id);
        if(is_numeric($this->pmt_inicial_f)) $criteria->compare('pmt_inicial_f',$this->pmt_inicial_f);
        if(is_numeric($this->pmt_incorporacion_f)) $criteria->compare('pmt_incorporacion_f',$this->pmt_incorporacion_f);
        if(is_numeric($this->pmt_abandono_f)) $criteria->compare('pmt_abandono_f',$this->pmt_abandono_f);
        if(is_numeric($this->pmt_final_f)) $criteria->compare('pmt_final_f',$this->pmt_final_f);
        if(is_numeric($this->pmt_inicial_m)) $criteria->compare('pmt_inicial_m',$this->pmt_inicial_m);
        if(is_numeric($this->pmt_incorporacion_m)) $criteria->compare('pmt_incorporacion_m',$this->pmt_incorporacion_m);
        if(is_numeric($this->pmt_abandono_m)) $criteria->compare('pmt_abandono_m',$this->pmt_abandono_m);
        if(is_numeric($this->pmt_final_m)) $criteria->compare('pmt_final_m',$this->pmt_final_m);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }


    public function beforeInsert()
    {
        parent::beforeSave();
        $this->fecha_ini = date('Y-m-d H:i:s');
        $this->usuario_ini_id = Yii::app()->user->id;
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeUpdate()
    {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeDelete(){
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        // $this->fecha_eli = $this->fecha_act;
        $this->estatus = 'I';
        return true;
    }

    public function beforeActivate(){
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        $this->estatus = 'A';
        return true;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CargaEstadistica the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
