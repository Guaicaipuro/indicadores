<?php

/**
 * This is the model class for table "gplantel.area_comun_plantel".
 *
 * The followings are the available columns in table 'gplantel.area_comun_plantel':
 * @property integer $id
 * @property integer $area_comun_id
 * @property integer $plantel_id
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property AreaComun $areaComun
 * @property Plantel $plantel
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 */
class AreaComunPlantel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
        public $area_nombre;
        public function tableName()
	{
		return 'gplantel.area_comun_plantel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('area_comun_id, plantel_id, fecha_ini, usuario_ini_id,cantidad', 'required'),
			array('area_comun_id, plantel_id, usuario_ini_id, usuario_act_id,cantidad', 'numerical', 'integerOnly'=>true),           
			array('estatus', 'length', 'max'=>1),
                        array('area_comun_id','validarUnicidadAreaComun','on'=>'registro_modificacion'),                       
			array('fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, area_comun_id,area_nombre, plantel_id, fecha_ini, usuario_ini_id, usuario_act_id, fecha_act, fecha_elim, estatus,cantidad', 'safe', 'on'=>'search'),
                       
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'areaComun' => array(self::BELONGS_TO, 'AreaComun', 'area_comun_id'),
			'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
                        
		);
	}
         public function validarUnicidadAreaComun($atributo, $params = null) {
        if ((isset($this->area_comun_id) AND (isset($this->plantel_id)))) {

            $existe= $this->existeAreaComun($this->area_comun_id,$this->plantel_id);
            if ($existe>0){
                $this->addError($atributo, 'Esta combinación de Plantel y Área Común ya existe.');
                $this->addError('plantel_id', null);
            }
        }

    }

    public function existeAreaComun($area_comun_id,$plantel_id){
        $id=$this->id;
        //$estatus = 'A';
        $sql="SELECT COUNT(id) FROM gplantel.area_comun_plantel
              WHERE area_comun_id=:area_comun_id AND plantel_id=:plantel_id";

        if($id>0){
            $sql.=' AND id<>:id';
        }
        $busqueda = Yii::app()->db->createCommand($sql);
        if($id>0){
            $busqueda->bindParam(":id", $id, PDO::PARAM_INT);
        }
        $busqueda->bindParam(":area_comun_id", $area_comun_id, PDO::PARAM_INT);
        $busqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        return $busqueda->queryScalar();

    }
    

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'area_comun_id' => 'Area Comun',
			'plantel_id' => 'Plantel',
			'fecha_ini' => 'Fecha Ini',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
                        'cantidad'=>'Cantidad',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
              
        $criteria->join= 'INNER JOIN gplantel.area_comun a ON (a.id = "t".area_comun_id) ';
//        $criteria->distinct=true;
        $criteria->select = '"t".area_comun_id, t.estatus,"t".cantidad';
               
        $criteria->with=array(
                    'areaComun'=>array('alias'=>'ac'),
                    //'plantel'=>array('alias'=>'pl')
                    
                    
                    
                );
               
                if(isset($this->area_comun_id) AND is_string($this->area_comun_id)){
                $criteria->addSearchCondition('ac.nombre','%'.$this->area_comun_id.'%',false,'AND','ILIKE');

                } 

                if(is_numeric($this->plantel_id)){
		$criteria->compare('plantel_id',$this->plantel_id);
                }
                
//		if(strlen($this->fecha_ini)>0 && Utiles::dateCheck($this->fecha_ini)){
//                //$criteria->compare('fecha_ini',$this->fecha_ini,true);
//                $criteria->addCondition("TO_CHAR(t.fecha_ini,'DD-MM-YYYY') = :fecha_ini");
//                $criteria->params = array(':fecha_ini'=>$this->fecha_ini);
//                }
                
                if(is_numeric($this->usuario_ini_id)){
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
                }
                if(is_numeric($this->usuario_act_id)){
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
                }
//		if(strlen($this->fecha_act)>0 && Utiles::dateCheck($this->fecha_act)){
//                    //$criteria->compare('fecha_act',$this->fecha_act,true);
//                    $criteria->addCondition("TO_CHAR(t.fecha_act,'DD-MM-YYYY') = :fecha_act");
//                    $criteria->params =  array_merge($criteria->params, array(':fecha_act'=>$this->fecha_act));
//                }
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		             
                
                if(in_array($this->estatus,array('A','I'))){
                $criteria->compare('t.estatus',$this->estatus);
        }
//                
                if(is_numeric($this->cantidad)){
		$criteria->compare('cantidad',$this->cantidad);
                }
                 
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AreaComunPlantel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
