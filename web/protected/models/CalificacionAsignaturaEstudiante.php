<?php

/**
 * This is the model class for table "matricula.calificacion_asignatura_estudiante".
 *
 * The followings are the available columns in table 'matricula.calificacion_asignatura_estudiante':
 * @property integer $id
 * @property integer $asignatura_estudiante_id
 * @property integer $lapso
 * @property double $calif_cuantitativa
 * @property integer $influye_en_promedio
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property string $resumen_evaluativo
 * @property integer $calif_nominal
 * @property integer $asistencia
 * @property string $observacion
 * @property string $id_compuesta
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property AsignaturaEstudiante $asignaturaEstudiante
 * @property CalificacionNominal $califNominal
 * @property UsergroupsUser $usuarioIni
 */
class CalificacionAsignaturaEstudiante extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'matricula.calificacion_asignatura_estudiante';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        //agregar escenarios de calificacion ALEXIS
        return array(
            array('asignatura_estudiante_id, lapso, calif_cuantitativa, influye_en_promedio, usuario_ini_id, fecha_ini', 'required', 'on'=>'regularMediaGeneral'),
            array('asignatura_estudiante_id, lapso, usuario_ini_id, fecha_ini', 'required', 'on'=>'regularBasica'),
            // array('asistencia','validarAsistencia'),
            array('asignatura_estudiante_id, lapso,calif_nominal, usuario_ini_id, fecha_ini', 'required', 'on'=>'regularBasica3lapso'),
            array('asignatura_estudiante_id, lapso, calif_cuantitativa, influye_en_promedio, usuario_ini_id, usuario_act_id, asistencia', 'numerical', 'integerOnly'=>true),
            array('estatus', 'length', 'max'=>1),
            array('id_compuesta','unique','message'=>'Ya se ha evaluado durante este lapso'),
            array('fecha_act, fecha_elim, resumen_evaluativo', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, asignatura_estudiante_id, lapso, calif_cuantitativa, influye_en_promedio, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus, resumen_evaluativo, calif_nominal, asistencia', 'safe', 'on'=>'search'),
        );
    }


//    public function validarAsistencia($attributo, $params = null) {
//
//        $totalClases = $this->consultarTotalClases();
//
//        if (($this->$attributo == null OR $this->$attributo == '') AND ( $this->cedula_escolar == null OR $this->cedula_escolar == '')) {
//            $this->addError($attributo, 'Debe ingresar el Documento de Identidad o la Cedula Escolar del Estudiante');
//        } else {
//            if ($this->$attributo != null AND $this->$attributo != '' AND ( $this->tdocumento_identidad == '' OR $this->tdocumento_identidad == null)) {
//                $this->addError('tdocumento_identidad', 'Debe seleccionar un Tipo de Documento de Identidad');
//            }
//        }
//    }
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'asignaturaEstudiante' => array(self::BELONGS_TO, 'AsignaturaEstudiante', 'asignatura_estudiante_id'),
            'califNominal' => array(self::BELONGS_TO, 'CalificacionNominal', 'calif_nominal'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'asignatura_estudiante_id' => 'Asignatura Estudiante',
            'lapso' => 'Lapso',
            'calif_cuantitativa' => 'Calif Cuantitativa',
            'influye_en_promedio' => 'Influye En Promedio',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'resumen_evaluativo' => 'Resumen Evaluativo',
            'calif_nominal' => 'Calif Nominal',
            'asistencia' => 'Asistencia',
            'observacion' => 'Observacion',
            'id_compuesta' => 'Id Compuesta',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('asignatura_estudiante_id',$this->asignatura_estudiante_id);
        $criteria->compare('lapso',$this->lapso);
        $criteria->compare('calif_cuantitativa',$this->calif_cuantitativa);
        $criteria->compare('influye_en_promedio',$this->influye_en_promedio);
        $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        $criteria->compare('fecha_ini',$this->fecha_ini,true);
        $criteria->compare('usuario_act_id',$this->usuario_act_id);
        $criteria->compare('fecha_act',$this->fecha_act,true);
        $criteria->compare('fecha_elim',$this->fecha_elim,true);
        $criteria->compare('estatus',$this->estatus,true);
        $criteria->compare('resumen_evaluativo',$this->resumen_evaluativo,true);
        $criteria->compare('calif_nominal',$this->calif_nominal);
        $criteria->compare('asistencia',$this->asistencia);
        $criteria->compare('observacion',$this->observacion,true);
        $criteria->compare('id_compuesta',$this->id_compuesta,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function cargarCalificacionRegularMediaGeneral($asignaturas, $asistencia, $notas,$observaciones,$lapsoEncode){
        $usuario_ini = Yii::app()->user->id;

        $observacionesDecoded=Utiles::toPgArray($observaciones)."::TEXT[]";
        $asignaturasDecoded=Utiles::toPgArray($asignaturas);
        $asistenciaDecoded=Utiles::toPgArray($asistencia);
        $notasDecoded=Utiles::toPgArray($notas)."::REAL[]";
        $lapso=array(base64_decode($lapsoEncode));

        $lapsoDecoded=Utiles::toPgArray($lapso)."::SMALLINT[]";

        $sql = "SELECT matricula.cargar_nota_regular_media_general($asignaturasDecoded, $asistenciaDecoded, $notasDecoded, :usuario_ini, $observacionesDecoded,$lapsoDecoded)";

        $inscripcionEstudiante = Yii::app()->db->createCommand($sql);
        $inscripcionEstudiante->bindParam(":usuario_ini", $usuario_ini, PDO::PARAM_INT);
        $resultado = $inscripcionEstudiante->execute();

        return $resultado;

    }
    public function cargarCalificacionRegularMediaGeneralPorSeccion($asignaturas, $estudiantes, $notas,$observaciones,$lapso){
        $usuario_ini = Yii::app()->user->id;

        $observacionesDecoded=Utiles::toPgArray($observaciones)."::TEXT[]";
        $estudiantesDecoded=Utiles::toPgArray($estudiantes);
        $notasDecoded=Utiles::toPgArray($notas)."::REAL[]";
        $lapsoParsed = $lapso;


        $sql = "SELECT matricula.cargar_nota_regular_media_general_por_seccion($asignaturas, $estudiantesDecoded, $notasDecoded, $usuario_ini, $observacionesDecoded,$lapsoParsed)";

        $inscripcionEstudiante = Yii::app()->db->createCommand($sql);
        //$inscripcionEstudiante->bindParam(":usuario_ini", $usuario_ini, PDO::PARAM_INT);
        $resultado = $inscripcionEstudiante->execute();

        return $resultado;

    }

    public function cargarAsistenciaPorSeccion($asignaturas, $estudiantes, $asistencias,$lapso,$seccion_plantel_id){
        $usuario_ini = Yii::app()->user->id;

        $estudiantesDecoded=Utiles::toPgArray($estudiantes);
        $asistenciasDecoded=Utiles::toPgArrayNumeric($asistencias)."::INTEGER[]";
        $lapsoParsed = $lapso;


        $sql = "SELECT matricula.cargar_asistencia_por_seccion($asignaturas, $estudiantesDecoded, $asistenciasDecoded, $usuario_ini,$lapsoParsed,$seccion_plantel_id)";

        $inscripcionEstudiante = Yii::app()->db->createCommand($sql);
        $resultado = $inscripcionEstudiante->execute();

        return $resultado;

    }
    public function obtenerTotalClasesPorAsignaturaSeccionLapso($asignatura,$lapso,$seccion_plantel_id){

        $sql = "SELECT aas.total_clases
FROM matricula.asistencia_asignatura_seccion aas
WHERE
aas.seccion_plantel_id = :seccion_plantel_id
AND aas.asignatura_id = :asignatura
AND aas.lapso = :lapso";

        $inscripcionEstudiante = Yii::app()->db->createCommand($sql);
        $inscripcionEstudiante->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":asignatura", $asignatura, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":lapso", $lapso, PDO::PARAM_INT);
        $resultado = $inscripcionEstudiante->queryScalar();

        return $resultado;

    }


    public function cargarCorreosRepresentantes($estudiantes){
        $usuario_ini = 1;

        $estudiantes=Utiles::toPgArray($estudiantes)."::INT[]";

        //$lapso=array(base64_decode($lapsoEncode));

        //$lapsoDecoded=Utiles::toPgArray($lapso)."::SMALLINT[]";

        $sql = "SELECT
                 e.id,
                 e.representante_id,
                 r.correo,
                 ie.periodo_id,
                 e.nombres ||' '|| e.apellidos AS nombres,
                 p.nombre as nombre_plantel,
                 p.direccion as direccion_plantel,
                 r.nombres || ' '|| r.apellidos as representante,
                 r.direccion_dom as direccion_representante,
                 pe.periodo as anio_escolar,
                 ee.nombre as estado_nacimiento,
                 ee.capital as capital_nacimiento,
                 e.fecha_nacimiento,
                 COALESCE(e.documento_identidad,e.cedula_escolar) as documento_identidad,
                 COALESCE (e.tdocumento_identidad,e.nacionalidad) as tdocumento_identidad,
                 s.nombre as seccion,
                 g.nombre as grado,
                 p.logo,
                 p.cod_plantel,
                 sp.nivel_id
                 FROM
                 matricula.estudiante e
                 LEFT JOIN matricula.representante r ON e.representante_id = r.id
                 LEFT JOIN matricula.inscripcion_estudiante ie ON e.id=ie.estudiante_id
                 LEFT JOIN gplantel.plantel p ON (ie.plantel_id=p.id)
                 LEFT JOIN estado ee ON (ee.id=e.estado_nac_id)
                 LEFT JOIN gplantel.periodo_escolar pe ON (pe.id=ie.periodo_id)
                 LEFT JOIN gplantel.seccion_plantel_periodo spp ON (spp.id=ie.seccion_plantel_periodo_id)
                 LEFT JOIN gplantel.seccion_plantel sp ON (sp.id=spp.seccion_plantel_id)
                 LEFT JOIN gplantel.seccion s ON (sp.seccion_id=s.id)
                 LEFT JOIN gplantel.grado g ON (g.id=ie.grado_id)
                 WHERE ie.id= ANY ($estudiantes)";



        // var_dump($sql);


        $inscripcionEstudiante = Yii::app()->db->createCommand($sql);
        //$inscripcionEstudiante->bindParam(":usuario_ini", $usuario_ini, PDO::PARAM_INT);
        $resultado = $inscripcionEstudiante->queryAll();

        return $resultado;

    }




    public function cargarTotalClasesAsignatura($asignaturas, $totalClases,$seccion,$lapsoEncode){
        $usuario_ini = Yii::app()->user->id;

        $asignaturasDecoded=Utiles::toPgArray($asignaturas);
        $totalClasesDecoded=Utiles::toPgArray($totalClases);

        $sql = "SELECT matricula.cargar_total_clases($asignaturasDecoded, $totalClasesDecoded, $seccion, :usuario_ini,$lapsoEncode)";

        $inscripcionEstudiante = Yii::app()->db->createCommand($sql);
        $inscripcionEstudiante->bindParam(":usuario_ini", $usuario_ini, PDO::PARAM_INT);
        $resultado = $inscripcionEstudiante->execute();

        return $resultado;

    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CalificacionAsignaturaEstudiante the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    public function consultarCalificaciones($estudiante){

        $sql = "SELECT"
            . " e.id, "
            . " e.representante_id, "
            . " r.correo,"
            . " ie.periodo_id,"
            . " e.nombres ||' '|| e.apellidos AS nombres"
            . " FROM "
            . " matricula.estudiante e"
            . " INNER JOIN matricula.representante r ON e.representante_id = r.id"
            . " INNER JOIN matricula.inscripcion_estudiante ie ON e.id=ie.estudiante_id"
            . " WHERE r.correo IS NOT NULL AND length(r.correo) > 12 AND ie.id= ANY ()";


        $inscripcionEstudiante = Yii::app()->db->createCommand($sql);
        $inscripcionEstudiante->bindParam(":estudiante", $estudiante, PDO::PARAM_INT);
        $resultado = $inscripcionEstudiante->queryAll();

        return $resultado;

    }


//    public function consultarTotalClases($id_inscripcion, $lapso,$id_asignatura){
//
//        $sql = "SELECT"
//            . " e.id, "
//            . " e.representante_id, "
//            . " r.correo,"
//            . " ie.periodo_id,"
//            . " e.nombres ||' '|| e.apellidos AS nombres"
//            . " FROM "
//            . " matricula.estudiante e"
//            . " INNER JOIN matricula.representante r ON e.representante_id = r.id"
//            . " INNER JOIN matricula.inscripcion_estudiante ie ON e.id=ie.estudiante_id"
//            . " WHERE r.correo IS NOT NULL AND length(r.correo) > 12 AND ie.id= ANY ()";
//
//
//        $inscripcionEstudiante = Yii::app()->db->createCommand($sql);
//        $inscripcionEstudiante->bindParam(":lapso", $estudiante, PDO::PARAM_INT);
//        $inscripcionEstudiante->bindParam(":estudiante", $estudiante, PDO::PARAM_INT);
//        $inscripcionEstudiante->bindParam(":estudiante", $estudiante, PDO::PARAM_INT);
//
//        $resultado = $inscripcionEstudiante->queryAll();
//
//        return $resultado;
//
//    }

    public function AlumnosParaCalificar($seccionId,$lapso,$asignatura_id,$periodo) {
        /* Modificar para que traiga la cedula escolar o pasaporte o cedula */
        $estatus = 'A';
//        var_dump($seccionId);
//        var_dump($asignatura_id);
//        var_dump(base64_decode($periodo));
//        var_dump($lapso);
//        die();


        $sql = "SELECT
e.id,
COALESCE(e.documento_identidad,e.cedula_escolar) as cedula_escolar,
e.fecha_nacimiento,
e.nombres || ' ' || e.apellidos as nomApe,
r.documento_identidad, ie.id as id_inscripcion,
ie.materia_pendiente,
p.modalidad_id as modalidad,
a.nombre as asignatura,
cae.calif_cuantitativa as calif_cuantitativa,
cae.asistencia as asistencia,
a.id as asignatura_id,
ae.id as asignatura_estudiante_id,
cae.observacion as observacion
FROM gplantel.seccion_plantel sp
INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.seccion_plantel_id = sp.id)
INNER JOIN matricula.inscripcion_estudiante ie on (spp.id = ie.seccion_plantel_periodo_id and ie.plantel_id = sp.plantel_id)
INNER JOIN matricula.estudiante e on (e.id = ie.estudiante_id)
INNER JOIN gplantel.plantel p on (p.id = sp.plantel_id)
INNER JOIN matricula.asignatura_estudiante ae on (ae.inscripcion_id = ie.id)
INNER JOIN gplantel.asignatura a on (a.id = ae.asignatura_id)
LEFT JOIN matricula.calificacion_asignatura_estudiante cae on cae.asignatura_estudiante_id = ae.id AND cae.lapso = :lapso
LEFT JOIN matricula.representante r on (r.id = e.representante_id)
WHERE spp.seccion_plantel_id = :seccion_plantel_id
AND ie.estatus= :estatus
AND ie.periodo_id = :periodo
AND ae.asignatura_id = :asignatura_id
ORDER BY ie.id DESC";
//var_dump($sql);
//        die();


        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":seccion_plantel_id", $seccionId, PDO::PARAM_INT);
        $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $buqueda->bindParam(":lapso", $lapso, PDO::PARAM_INT);
        $buqueda->bindParam(":asignatura_id", $asignatura_id, PDO::PARAM_INT);
        $buqueda->bindParam(":periodo", base64_decode($periodo), PDO::PARAM_INT);
        $resultado = $buqueda->queryAll();
//         var_dump($resultado); die();


        return new CArrayDataProvider($resultado, array(
            'pagination' => array(
                'pageSize' => 99999,
            )
        ));
    }

}
