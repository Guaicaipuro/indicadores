<?php

/**
 * This is the model class for table "sistema.noticia".
 *
 * The followings are the available columns in table 'sistema.noticia':
 * @property integer $id
 * @property string $titulo
 * @property string $cuerpo
 * @property string $importante_o_fija
 * @property string $fecha_de_clausura
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property integer $usuario_elim_id
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioElim
 * @property UsergroupsUser $usuarioIni
 */
class Noticia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sistema.noticia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('titulo, cuerpo, importante_o_fija, fecha_de_clausura, usuario_ini_id, estatus', 'required'),
			array('usuario_ini_id, usuario_act_id, usuario_elim_id', 'numerical', 'integerOnly'=>true),
			array('titulo', 'length', 'max'=>40),
			array('importante_o_fija, estatus', 'length', 'max'=>1),
			array('fecha_ini, fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, titulo, cuerpo, importante_o_fija, fecha_de_clausura, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, usuario_elim_id, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioElim' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_elim_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),


		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'titulo' => 'Titulo',
			'cuerpo' => 'Cuerpo',
			'importante_o_fija' => 'Importante O Fija',
			'fecha_de_clausura' => 'Fecha De Clausura',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'usuario_elim_id' => 'Usuario Elim',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        if(is_numeric($this->id)){
            $criteria->compare('id',$this->id);
        }
        //$criteria->compare('id',$this->id);
		$criteria->compare('titulo',$this->titulo,true);
		$criteria->compare('cuerpo',$this->cuerpo,true);
		$criteria->compare('importante_o_fija',$this->importante_o_fija,true);
		$criteria->compare('fecha_de_clausura',$this->fecha_de_clausura);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('usuario_elim_id',$this->usuario_elim_id);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);

        $criteria->order='estatus asc,fecha_act desc';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function listaNoticias($page=1){

        $indexData = 'NEWS:PAG'.$page;
        $indexPag = 'PAGINATOR:PAG'.$page;

        $datos = Yii::app()->cache->get($indexData);
        $paginator = Yii::app()->cache->get($indexPag);

        //Yii::app()->cache->delete($indexData);
        //Yii::app()->cache->delete($indexPag);

        if(!$datos || !$paginator){
            $fecha_actual = date("Y-m-d H:i:s");

            $criteria=new CDbCriteria();
            $criteria->order='importante_o_fija asc, fecha_act desc';
            $criteria->select = '
                t.titulo,
                t.cuerpo,
                t.fecha_ini,
                t.importante_o_fija,
                t.fecha_de_clausura,
                t.fecha_act

            ';
            $criteria->condition = "((fecha_de_clausura >= :fecha_actual) and (estatus =:estatus))";
            $criteria->with = array('usuarioIni'=>array('alias'=>'u', 'select'=>'u.id, u.nombre, u.apellido'));
            $criteria->params=array(':fecha_actual'=>$fecha_actual, ':estatus'=>'A');

            $count=Noticia::model()->count($criteria);

            $paginator=new CPagination($count);

            $paginator->pageSize=10;
            $paginator->applyLimit($criteria);
            $datos=Noticia::model()->findAll($criteria);

            if(count($datos)>0 && false){
                Yii::app()->cache->set($indexData, $datos, 43200);
                Yii::app()->cache->set($indexPag, $paginator, 43201);
            }

        }

        return array($datos, $paginator);
    }

    public function getNoticias(){
        $result = array();
        $fecha_actual = date('Y-m-d');
            $sql = "
SELECT
n.id,
n.titulo,
n.cuerpo,
n.importante_o_fija,
n.fecha_de_clausura,
n.usuario_ini_id,
COALESCE (n.fecha_act,n.fecha_ini) as fecha,
n.usuario_act_id,
n.usuario_elim_id,
n.fecha_elim,
n.estatus
FROM sistema.noticia n WHERE n.fecha_de_clausura >= :fecha_actual AND estatus = 'A'";
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $command->bindParam(':fecha_actual',$fecha_actual,PDO::PARAM_STR);
            $result = $command->queryAll();
            $filter = new InputFilter(array('em', 'i', 'b', 'strong', 'ol', 'ul', 'li', 'div', 'strike'));


        for($i = 0;$i < count($result);$i++){
          //  echo "HEAD<<<<<<<<<<<";
           // var_dump($result[$i]["cuerpo"]);

            $result[$i]["cuerpo"] =htmlspecialchars_decode($filter->process(htmlspecialchars($result[$i]["cuerpo"])));
          //echo "NEW>>>>>>>>>>>>>>>>>>>>>>>>>>";
            //var_dump($result[$i]["cuerpo"]);

            // echo $result[$i]["cuerpo"];
        }
//        die();
        return $result;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Noticia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
