<?php

/**
 * This is the model class for table "matricula.estudiante_area_atencion".
 *
 * The followings are the available columns in table 'matricula.estudiante_area_atencion':
 * @property integer $id
 * @property integer $estudiante_id
 * @property integer $area_atencion_id
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $fecha_eli
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property Estudiante $estudiante
 * @property AreaAtencion $areaAtencion
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 */
class EstudianteAreaAtencion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'matricula.estudiante_area_atencion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('area_atencion_id','required','message'=>'el Campo Area de Atención no puede ser nulo'),
			array('usuario_ini_id,estatus', 'required'),
			array('estudiante_id, area_atencion_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
                        array('estatus', 'length', 'max'=>1),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, estudiante_id, area_atencion_id, usuario_ini_id, usuario_act_id, fecha_ini, fecha_act, fecha_eli, estatus', 'safe', 'on'=>'search'),
		);      
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'estudiante' => array(self::BELONGS_TO, 'Estudiante', 'estudiante_id'),
			'areaAtencion' => array(self::BELONGS_TO, 'AreaAtencion', 'area_atencion_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'estudiante_id' => 'Estudiante',
			'area_atencion_id' => 'Area Atencion',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_ini' => 'Fecha Ini',
			'fecha_act' => 'Fecha Act',
			'fecha_eli' => 'Fecha Eli',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		//if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		
                if(is_numeric($this->estudiante_id)) $criteria->compare('estudiante_id',$this->estudiante_id);                            
		if(is_numeric($this->area_atencion_id)) $criteria->compare('area_atencion_id',$this->area_atencion_id);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(Utiles::isValidDate($this->fecha_eli, 'y-m-d')) $criteria->compare('fecha_eli',$this->fecha_eli);
		// if(strlen($this->fecha_eli)>0) $criteria->compare('fecha_eli',$this->fecha_eli,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EstudianteAreaAtencion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function ObtenerDatosEstudianteAreaAtencion($estudiante_id)
        {
            $sql = " select id,nombre from matricula.area_atencion where id IN ( 
                            select area_atencion_id from matricula.estudiante_area_atencion where estudiante_id= :EstudianteId )";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":EstudianteId", $estudiante_id, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();
            return $resultado;
        }
        
         public function ObtenerAreaAtencionEstudiante($estudiante_id)
        {
            $estatus='A';
            $sql = " select id,nombre from matricula.area_atencion where id NOT IN ( 
                            select area_atencion_id from matricula.estudiante_area_atencion where estudiante_id= :EstudianteId ) AND estatus= :estatus";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":EstudianteId", $estudiante_id, PDO::PARAM_INT);
            $consulta->bindParam(":estatus", $estatus, PDO::PARAM_STR);
            $resultado = $consulta->queryAll();
            return $resultado;
        }
}
