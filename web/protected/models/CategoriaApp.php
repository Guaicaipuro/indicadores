<?php

/**
 * This is the model class for table "categoria_app".
 *
 * The followings are the available columns in table 'categoria_app':
 * @property integer $id
 * @property string $nombre
 *
 * The followings are the available model relations:
 * @property ReporteTotalApp[] $reporteTotalApps
 */
class CategoriaApp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'categoria_app';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('nombre', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'reporteTotalApps' => array(self::HAS_MANY, 'ReporteTotalApp', 'categoria_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		if(strlen($this->nombre)>0) $criteria->compare('nombre',$this->nombre,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function beforeInsert()
	{
		parent::beforeSave();
		$this->fecha_ini = date('Y-m-d H:i:s');
		$this->usuario_ini_id = Yii::app()->user->id;
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		return true;
	}

	public function beforeUpdate()
	{
		parent::beforeSave();
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		return true;
	}

	public function beforeDelete(){
		parent::beforeSave();
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		// $this->fecha_eli = $this->fecha_act;
		$this->estatus = 'I';
		return true;
	}

	public function beforeActivate(){
		parent::beforeSave();
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		$this->estatus = 'A';
		return true;
	}

	public  function getCategoriasDisponibles(){
		$sql = "SELECT c.id,c.nombre FROM categoria_app c INNER JOIN reporte_total_app r ON (r.categoria_id=c.id) GROUP BY c.id,c.nombre ORDER BY c.nombre";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$result = $command->queryAll();
		return $result;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CategoriaApp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
