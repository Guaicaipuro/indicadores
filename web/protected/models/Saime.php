<?php
/**
 * This is the model class for table "auditoria.saime".
 *
 * The followings are the available columns in table 'auditoria.saime':
 * @property string $origen
 * @property integer $cedula
 * @property string $pais_origen
 * @property string $nacionalidad
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property string $fecha_nacimiento
 * @property integer $naturalizado
 * @property string $sexo
 * @property string $fecha_registro
 */
class Saime extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.saime';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('origen, cedula, primer_nombre, primer_apellido, sexo, fecha_registro', 'required'),
			array('cedula, naturalizado', 'numerical', 'integerOnly'=>true),
			array('origen, sexo', 'length', 'max'=>1),
			array('pais_origen, nacionalidad', 'length', 'max'=>3),
			array('primer_nombre, segundo_nombre, primer_apellido, segundo_apellido', 'length', 'max'=>60),
			array('origen', 'in', 'range'=>array('V', 'E', 'P'), 'allowEmpty'=>false, 'strict'=>true,),
            array('origen, cedula','required','message'=>'El campo {attribute} no debe estar vacio','on'=>'consulta'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('origen, cedula, pais_origen, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, fecha_nacimiento, naturalizado, sexo, fecha_registro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'origen' => 'Tipo de Documento de Identidad',
			'cedula' => 'Documento de Identidad',
			'pais_origen' => 'Pais Origen',
			'nacionalidad' => 'Nacionalidad',
			'primer_nombre' => 'Primer Nombre',
			'segundo_nombre' => 'Segundo Nombre',
			'primer_apellido' => 'Primer Apellido',
			'segundo_apellido' => 'Segundo Apellido',
			'fecha_nacimiento' => 'Fecha Nacimiento',
			'naturalizado' => 'Naturalizado',
			'sexo' => 'Sexo',
			'fecha_registro' => 'Fecha Registro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(strlen($this->origen)>0) $criteria->compare('origen',$this->origen,true);
		if(is_numeric($this->cedula)) $criteria->compare('cedula',$this->cedula);
		if(strlen($this->pais_origen)>0) $criteria->compare('pais_origen',$this->pais_origen,true);
		if(strlen($this->nacionalidad)>0) $criteria->compare('nacionalidad',$this->nacionalidad,true);
		if(strlen($this->primer_nombre)>0) $criteria->compare('primer_nombre',$this->primer_nombre,true);
		if(strlen($this->segundo_nombre)>0) $criteria->compare('segundo_nombre',$this->segundo_nombre,true);
		if(strlen($this->primer_apellido)>0) $criteria->compare('primer_apellido',$this->primer_apellido,true);
		if(strlen($this->segundo_apellido)>0) $criteria->compare('segundo_apellido',$this->segundo_apellido,true);
		if(Utiles::isValidDate($this->fecha_nacimiento, 'y-m-d')) $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento);
		// if(strlen($this->fecha_nacimiento)>0) $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		if(is_numeric($this->naturalizado)) $criteria->compare('naturalizado',$this->naturalizado);
		if(strlen($this->sexo)>0) $criteria->compare('sexo',$this->sexo,true);
		if(Utiles::isValidDate($this->fecha_registro, 'y-m-d')) $criteria->compare('fecha_registro',$this->fecha_registro);
		// if(strlen($this->fecha_registro)>0) $criteria->compare('fecha_registro',$this->fecha_registro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Saime the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function buscarSaime($cedula,$origen){ //Obtener el registro del saime en base al origen y el numero de cedula
		$sql = "SELECT id,origen,cedula,pais_origen,nacionalidad, primer_nombre,
					   segundo_nombre,primer_apellido,segundo_apellido,fecha_nacimiento,
					   sexo,fecha_registro,fecha_ult_actualizacion
				FROM ".$this->tableName()." as aus
				WHERE aus.origen=:origen
				AND aus.cedula=:cedula";
		$consulta = $this->getDbConnection()->createCommand($sql);
        $consulta->bindParam(":origen", $origen, PDO::PARAM_INT);
        $consulta->bindParam(":cedula", $cedula, PDO::PARAM_INT);
        $saime = $consulta->queryRow();
        return $saime;
	}
    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection() {
        return Yii::app()->dbSaime;
    }
}
