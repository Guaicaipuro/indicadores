<?php

/**
 * This is the model class for table "personal.docente_plantel".
 *
 * The followings are the available columns in table 'personal.docente_plantel':
 * @property integer $id
 * @property integer $docente_id
 * @property integer $plantel_id
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property Docente $docente
 * @property Plantel $plantel
 */
class DocentePlantel extends CActiveRecord
{
    public $nombres=null;
    public $apellidos=null;
    public $documento_identidad=null;
    public $tdocumento_identidad=null;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'personal.docente_plantel';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('docente_id, plantel_id, fecha_ini, usuario_ini_id', 'required'),
            array('docente_id, plantel_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
            array('estatus', 'length', 'max'=>1),
            array('fecha_act, fecha_elim', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, docente_id, plantel_id, fecha_ini, usuario_ini_id, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'docente' => array(self::BELONGS_TO, 'Docente', 'docente_id'),
            'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'docente_id' => 'Docente',
            'plantel_id' => 'Plantel',
            'fecha_ini' => 'Fecha Ini',
            'usuario_ini_id' => 'Usuario Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;
        $criteria->with=array('docente'=>array('alias'=>'d','select'=>'nombres,apellidos,tdocumento_identidad,documento_identidad'));
        if(!is_null($this->nombres) AND $this->nombres !='')
            $criteria->addSearchCondition('d.nombres',$this->nombres,true,'AND','ILIKE');
        if(!is_null($this->apellidos) AND $this->apellidos !='')
            $criteria->addSearchCondition('d.apellidos',$this->apellidos,true,'AND','ILIKE');
        if(!is_null($this->apellidos) AND $this->apellidos !='')
            $criteria->addSearchCondition('d.apellidos',$this->apellidos,true,'AND','ILIKE');
        $criteria->compare('estatus',$this->estatus,true);
        $criteria->compare('plantel_id',$this->plantel_id);
        /*$criteria->compare('docente_id',$this->docente_id);
        $criteria->compare('id',$this->id);
        $criteria->compare('fecha_ini',$this->fecha_ini,true);
        $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        $criteria->compare('usuario_act_id',$this->usuario_act_id);
        $criteria->compare('fecha_act',$this->fecha_act,true);
        $criteria->compare('fecha_elim',$this->fecha_elim,true);*/


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    public function searchPlantelDocente()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;
        $criteria->with=array('plantel'=>array('alias'=>'p','select'=>'nombre,cod_plantel,modalidad_id,estado_id'));
        $criteria->compare('docente_id',$this->docente_id);
        /*
        $criteria->compare('id',$this->id);
        $criteria->compare('fecha_ini',$this->fecha_ini,true);
        $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        $criteria->compare('usuario_act_id',$this->usuario_act_id);
        $criteria->compare('fecha_act',$this->fecha_act,true);
        $criteria->compare('fecha_elim',$this->fecha_elim,true);*/


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    public function busquedaDocente($plantel_id,$docente_id){
        $resultadoCedula = null;
        if (is_numeric($plantel_id) AND is_numeric($docente_id)) {
            $sql = "SELECT COUNT(id)"
                . " FROM personal.docente_plantel"
                . " WHERE "
                . " plantel_id= :plantel_id AND "
                . " docente_id= :docente_id AND"
                . " estatus='A'";

            $buqueda = Yii::app()->db->createCommand($sql);
            $buqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_STR);
            $buqueda->bindParam(":docente_id", $docente_id, PDO::PARAM_STR);
            $resultadoCedula = $buqueda->queryScalar();
        }
        return $resultadoCedula;
    }
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DocentePlantel the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
