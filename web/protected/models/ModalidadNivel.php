<?php

/**
 * This is the model class for table "gplantel.modalidad_nivel".
 *
 * The followings are the available columns in table 'gplantel.modalidad_nivel':
 * @property integer $id
 * @property integer $modalidad_id
 * @property integer $nivel_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property Modalidad $modalidad
 * @property Nivel $nivel
 */
class ModalidadNivel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gplantel.modalidad_nivel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usuario_ini_id, fecha_ini', 'required'),
			array('modalidad_id, nivel_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('estatus', 'length', 'max'=>1),
			array('fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, modalidad_id, nivel_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'modalidad' => array(self::BELONGS_TO, 'Modalidad', 'modalidad_id'),
			'nivel' => array(self::BELONGS_TO, 'Nivel', 'nivel_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'modalidad_id' => 'Modalidad',
			'nivel_id' => 'Nivel',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('modalidad_id',$this->modalidad_id);
		$criteria->compare('nivel_id',$this->nivel_id);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public function getNivel($modalidad_dropDown_id, $plantel_id) {
            $estatus = 'A';
            if ($modalidad_dropDown_id != null && $plantel_id != null) {
                $sql = "SELECT n.id AS nivel_id, n.nombre AS nombrenivel
                            FROM gplantel.modalidad_nivel mn
                            INNER JOIN gplantel.modalidad m ON (m.id = mn.modalidad_id)
                            INNER JOIN gplantel.nivel n ON (n.id = mn.nivel_id)
                            INNER JOIN gplantel.plantel_modalidad pm ON (pm.modalidad_id = m.id)
                            INNER JOIN gplantel.plantel p ON (p.id = pm.plantel_id)
                            INNER JOIN gplantel.nivel_plantel np ON (np.nivel_id = n.id AND np.plantel_id = p.id AND np.nivel_id = mn.nivel_id)
                           WHERE p.id=:plantel_id
                           AND m.id=:modalidad_id
                           AND pm.estatus=:estatus
                           AND p.estatus=:estatus
                           AND m.estatus=:estatus
                           AND mn.estatus=:estatus
                           ORDER BY m.nombre ASC";
                $buqueda = Yii::app()->db->createCommand($sql);
                $buqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
                $buqueda->bindParam(":modalidad_id", $modalidad_dropDown_id, PDO::PARAM_INT);
                $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
                $resultado = $buqueda->queryAll();
                if ($resultado !== array())
                    return $resultado;
                else
                    return false;
            }else {
                return false;
            }
            
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ModalidadNivel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
