<?php

/**
 * This is the model class for table "legacy.talumno_acad".
 *
 * The followings are the available columns in table 'legacy.talumno_acad':
 * @property string $calumno
 * @property string $tdocumento_identidad
 * @property integer $cestadistico
 * @property integer $cgradoano
 * @property string $cseccion
 * @property integer $cplan
 * @property string $cescolaridad1
 * @property string $cescolaridad2
 * @property string $fperiodoescolar
 * @property string $fmatricula
 * @property string $dobservacion
 * @property string $fegreso
 * @property string $lapso
 * @property string $cdea
 * @property integer $cestado
 * @property string $cregimen
 * @property string $cnivel
 * @property integer $cestatusegreso
 * @property string $observacione
 * @property integer $carea_atencion
 * @property integer $tipo_matri
 * @property string $calumno2
 * @property string $curso
 * @property string $s_cedula_escolar
 * @property string $s_cedula_identidad
 * @property string $s_carnet_diplomatico
 * @property string $s_alumno_id
 * @property string $s_plantel_id
 * @property string $s_nivel_nombre
 * @property string $s_grado_nombre
 * @property string $s_periodo_nombre
 * @property string $s_modalidad_nombre
 * @property string $s_escolaridad_nombre
 * @property integer $historico
 * @property string $s_plan_id
 * @property integer $id
 * @property integer $gplantel_plantel_id
 */
class TalumnoAcad extends CActiveRecord
{
    public $tdocumento_identidad;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'legacy.talumno_acad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('calumno,cdea,curso', 'required'),
                                              //  array('calumno', 'unique'),
			array('cestadistico, calumno, cgradoano, cplan, cestado, cestatusegreso, carea_atencion, tipo_matri, historico, id, gplantel_plantel_id', 'numerical', 'integerOnly'=>true),
			array('cseccion, fperiodoescolar', 'length', 'max'=>3),
			array('cescolaridad1, cescolaridad2, cregimen, cnivel', 'length', 'max'=>2),
			array('dobservacion', 'length', 'max'=>80),
			array('lapso', 'length', 'max'=>25),
			array('cdea', 'length', 'max'=>15),
			array('observacione', 'length', 'max'=>50),
                                               // array('observacione', 'required'),
                                                array('fegreso','validarCronograma'),
			array('fmatricula, fegreso, calumno2, s_cedula_escolar, s_cedula_identidad, s_carnet_diplomatico, s_alumno_id, s_plantel_id, s_nivel_nombre, s_grado_nombre, s_periodo_nombre, s_modalidad_nombre, s_escolaridad_nombre, s_plan_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('calumno, cestadistico, cgradoano, cseccion, cplan, cescolaridad1, cescolaridad2, fperiodoescolar, fmatricula, dobservacion, fegreso, lapso, cdea, cestado, cregimen, cnivel, cestatusegreso, observacione, carea_atencion, tipo_matri, calumno2, s_cedula_escolar, s_cedula_identidad, s_carnet_diplomatico, s_alumno_id, s_plantel_id, s_nivel_nombre, s_grado_nombre, s_periodo_nombre, s_modalidad_nombre, s_escolaridad_nombre, historico, s_plan_id, id, gplantel_plantel_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
 public function validarCronograma($attr, $params){
        $fechaInicioTimestamp = strtotime($this->fmatricula);
        $fechaFinTimestamp = strtotime($this->fegreso);
        if($fechaFinTimestamp  < $fechaInicioTimestamp) {
            $this->addError('fegreso', 'La Fecha de Matricula debe ser menor a la Fecha de Egreso');
        }
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'calumno' => 'Documento de Identidad',
			'tdocumento_identidad' => 'Tipo de Documento',
			'cestadistico' => 'Código estadístico',
			'cgradoano' => 'Grado o año',
			'cseccion' => 'Sección',
			'cplan' => 'Código del Plan',
			'curso' => 'Permanencia',
			'cescolaridad1' => 'Escolaridad 1',
			'cescolaridad2' => 'Escolaridad 2',
			'fperiodoescolar' => 'Periodo Escolar',
			'fmatricula' => 'Fecha de Matricula',
			'dobservacion' => 'Observación',
			'fegreso' => 'Fecha de Egreso',
			'lapso' => 'Lapso',
			'cdea' => 'Código del Plantel',
			'cestado' => 'Estado',
			'cregimen' => 'Regimen',
			'cnivel' => 'Nivel',
			'cestatusegreso' => 'Estatus Egreso',
			'observacione' => 'Observaciones',
			'carea_atencion' => 'Carea Atencion',
			'tipo_matri' => 'Tipo Matricula',
			'calumno2' => 'Cedula Estudiante 2',
			's_cedula_escolar' => 'Cedula Escolar',
			's_cedula_identidad' => 'Cedula Identidad',
			's_carnet_diplomatico' => 'S Carnet Diplomatico',
			's_alumno_id' => 'S Alumno',
			's_plantel_id' => 'S Plantel',
			's_nivel_nombre' => 'S Nivel Nombre',
			's_grado_nombre' => 'Grado Nombre',
			's_periodo_nombre' => 'S Periodo Nombre',
			's_modalidad_nombre' => 'Modalidad',
			's_escolaridad_nombre' => 'Escolaridad',
			'historico' => 'Historico',
			's_plan_id' => 'S Plan',
			'id' => 'ID',
			'gplantel_plantel_id' => 'Gplantel Plantel',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('calumno',$this->calumno,true);
		$criteria->compare('cestadistico',$this->cestadistico);
		$criteria->compare('cgradoano',$this->cgradoano);
		$criteria->compare('cseccion',$this->cseccion,true);
		$criteria->compare('cplan',$this->cplan);
		$criteria->compare('cescolaridad1',$this->cescolaridad1,true);
		$criteria->compare('cescolaridad2',$this->cescolaridad2,true);
		$criteria->compare('fperiodoescolar',$this->fperiodoescolar,true);
		$criteria->compare('fmatricula',$this->fmatricula,true);
		$criteria->compare('dobservacion',$this->dobservacion,true);
		$criteria->compare('fegreso',$this->fegreso,true);
		$criteria->compare('lapso',$this->lapso,true);
		$criteria->compare('cdea',$this->cdea,true);
		$criteria->compare('cestado',$this->cestado);
		$criteria->compare('cregimen',$this->cregimen,true);
		$criteria->compare('cnivel',$this->cnivel,true);
		$criteria->compare('cestatusegreso',$this->cestatusegreso);
		$criteria->compare('observacione',$this->observacione,true);
		$criteria->compare('carea_atencion',$this->carea_atencion);
		$criteria->compare('tipo_matri',$this->tipo_matri);
		$criteria->compare('calumno2',$this->calumno2,true);
		$criteria->compare('s_cedula_escolar',$this->s_cedula_escolar,true);
		$criteria->compare('s_cedula_identidad',$this->s_cedula_identidad,true);
		$criteria->compare('s_carnet_diplomatico',$this->s_carnet_diplomatico,true);
		$criteria->compare('s_alumno_id',$this->s_alumno_id,true);
		$criteria->compare('s_plantel_id',$this->s_plantel_id,true);
		$criteria->compare('s_nivel_nombre',$this->s_nivel_nombre,true);
		$criteria->compare('s_grado_nombre',$this->s_grado_nombre,true);
		$criteria->compare('s_periodo_nombre',$this->s_periodo_nombre,true);
		$criteria->compare('s_modalidad_nombre',$this->s_modalidad_nombre,true);
		$criteria->compare('s_escolaridad_nombre',$this->s_escolaridad_nombre,true);
		$criteria->compare('historico',$this->historico);
		$criteria->compare('s_plan_id',$this->s_plan_id,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('gplantel_plantel_id',$this->gplantel_plantel_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TalumnoAcad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
