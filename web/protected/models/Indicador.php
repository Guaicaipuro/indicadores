<?php

/**
 * This is the model class for table "servicio.indicadores".
 *
 * The followings are the available columns in table 'servicio.indicadores':
 * @property integer $id
 * @property string $variable
 * @property integer $categoria_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $estatus
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property Categoria $categoria
 * @property ValorIndicador[] $valorIndicadors
 */
class Indicador extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'servicio.indicadores';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('variable, fecha_ini, categoria_id', 'required'),
			array('categoria_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('variable', 'length', 'max'=>100),
			array('estatus', 'length', 'max'=>1),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, variable, categoria_id, fecha_ini, fecha_act, estatus, usuario_ini_id, usuario_act_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'categoria' => array(self::BELONGS_TO, 'CategoriaDepartamento', 'categoria_id'),
			'valorIndicadors' => array(self::HAS_MANY, 'ValorIndicador', 'indicadores_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'variable' => 'Nombre de Indicador',
			'categoria_id' => 'Categoría',
			'fecha_ini' => 'Fecha Ini',
			'fecha_act' => 'Fecha Act',
			'estatus' => 'Estatus',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = array(
                    'categoria' => array('alias'=>'c', 'select'=>'c.nombre'),
                );
                
		if(is_numeric($this->id)) $criteria->compare('t.id',$this->id);
		if(strlen($this->variable)>0) $criteria->compare('t.variable',$this->variable,true);
		if(is_numeric($this->categoria_id)) $criteria->compare('t.categoria_id',$this->categoria_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('t.fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('t.fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('t.estatus',$this->estatus,true);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function findIndicadoresByCategoria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = array(
                    'categoria' => array('alias'=>'c', 'select'=>'c.nombre'),
                );
                
		if(is_numeric($this->id)) $criteria->compare('t.id',$this->id);
		if(strlen($this->variable)>0) $criteria->compare('t.variable',$this->variable,true);
		if(is_numeric($this->categoria_id)) $criteria->compare('t.categoria_id',$this->categoria_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('t.fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('t.fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('t.estatus',$this->estatus,true);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);

		return $this->findAll($criteria);
	}
        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Indicador the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
