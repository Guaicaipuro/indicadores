<?php

/**
 * This is the model class for table "foro.comentario".
 *
 * The followings are the available columns in table 'foro.comentario':
 * @property integer $id
 * @property integer $tema_id
 * @property string $titulo
 * @property string $contenido
 * @property string $status
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property Tema $tema
 */
class Comentario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'foro.comentario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tema_id, titulo, contenido', 'required'),
            array('titulo', 'unique', 'message' => 'El asunto ya existe, por favor seleccione otro.'),
            array('titulo, contenido', 'filter', 'filter'=>'trim'),
			array('tema_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('titulo', 'length', 'min'=>6, 'max'=>150),
            array('contenido', 'length', 'min'=>10),
			array('estatus', 'length', 'max'=>1),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tema_id, titulo, contenido, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus, tema.titulo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'tema' => array(self::BELONGS_TO, 'Tema', 'tema_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'tema.titulo' => 'Tema',
			'tema_id' => 'Tema',
			'titulo' => 'Asunto',
			'contenido' => 'Comentario',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Actualización',
			'fecha_elim' => 'Fecha Eliminado',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

        $criteria->join = '';

        // Buscar por el titulo del tema.
        if( isset( $_GET['Comentario']['tema_titulo'] ) && strlen( $_GET['Comentario']['tema_titulo'] ) > 0 )
        {
            $titulo = $_GET['Comentario']['tema_titulo'];
            $criteria->join .= ' INNER JOIN foro.tema f ON f.id = t.tema_id';
            $criteria->addSearchCondition('f.titulo', $titulo, true, 'AND', 'ILIKE');
        }

        // Buscar por el username.
        if( isset( $_GET['Comentario']['usuario'] ) &&  strlen( $_GET['Comentario']['usuario'] ) > 0 )
        {
            $usuario = $_GET['Comentario']['usuario'];
            $criteria->join .= ' INNER JOIN seguridad.usergroups_user u ON u.id = t.usuario_ini_id';
            $criteria->addSearchCondition('u.username', $usuario, true, 'AND', 'ILIKE');
        }

		if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		if(is_numeric($this->tema_id)) $criteria->compare('tema_id',$this->tema_id);
		if(strlen($this->titulo)>0) $criteria->compare('titulo',$this->titulo,true);
		if(strlen($this->contenido)>0) $criteria->compare('contenido',$this->contenido,true);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
        // if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(Utiles::isValidDate($this->fecha_elim, 'y-m-d')) $criteria->compare('fecha_elim',$this->fecha_elim);
		// if(strlen($this->fecha_elim)>0) $criteria->compare('fecha_elim',$this->fecha_elim,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
        if(!Yii::app()->user->pbac('foro.comentario.admin')) $criteria->compare('usuario_ini_id',Yii::app()->user->id);
        $criteria->order = 'fecha_ini DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


    public function beforeInsert()
	{
            parent::beforeSave();
            $this->estatus = Yii::app()->user->pbac('foro.tema.admin') ?  'A' : 'I';
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
    public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
    public function beforeDelete()
    {
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
    }
        
    public function beforeActivate()
    {
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
    }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Comentario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
