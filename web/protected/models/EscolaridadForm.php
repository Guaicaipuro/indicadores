<?php
/**
 * Created by PhpStorm.
 * User: isalaz01
 * Date: 05/06/15
 * Time: 09:51 AM
 */
/**
 *
 * EscolaridadForm class.
 * EscolaridadForm is the data structure for keeping
 */
class EscolaridadForm extends CFormModel
{
    public $tdocumento_identidad;
    public $documento_identidad;
    public $nombres;
    public $apellidos;
    public $inscripcion_regular =0;
    public $materia_pendiente=0;
    public $repitiente=0;
    public $doble_inscripcion=0;
    public $observacion='';
    public $periodo_id;
    public $estudiante_id;
    public $seccion_plantel_id;
    public $plantel_id;


    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('tdocumento_identidad, documento_identidad,periodo_id,seccion_plantel_id,plantel_id', 'required','on'=>'regEscolaridad'),
            array('nombres, apellidos,estudiante_id', 'safe'),
            array('inscripcion_regular','validarEscolaridad'),
            array('estudiante_id','validarInscripcion'),

            // rememberMe needs to be a boolean
        );
    }

    public function validarEscolaridad($atributo,$parametros = null){
        if(!($this->inscripcion_regular==1 OR $this->materia_pendiente == 1 OR $this->repitiente==1))
            $this->addError('error', 'Estimado usuario, debe tildar una opción en el bloque <strong>"Datos de Escolaridad"</strong>');

    }
    public function validarInscripcion($atributo,$parametros = null){
        if(!is_null($this->estudiante_id) AND !is_null($this->periodo_id)){
            $estaMatriculado = Estudiante::model()->estaMatriculado(array($this->estudiante_id),$this->periodo_id);
            if($estaMatriculado==$this->estudiante_id){
                $this->addError('error', 'Estimado usuario, el estudiante que desea escolarizar se encuentra inscrito en este periodo. Por favor revise el <strong>Historico de Estudiante</strong>');
            }
        }


    }
    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'tdocumento_identidad'=>'Tipo de Documento',
            'documento_identidad'=>'Documento de Identidad',
        );
    }

}
