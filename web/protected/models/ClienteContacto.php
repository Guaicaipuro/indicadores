<?php

/**
 * This is the model class for table "servicio.cliente_contacto".
 *
 * The followings are the available columns in table 'servicio.cliente_contacto':
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property string $documento_identidad
 * @property string $tdocumento_identidad
 * @property string $fecha_nacimiento
 * @property string $sexo
 * @property string $telefono_celular
 * @property string $telefono_oficina
 * @property string $correo
 * @property integer $cliente_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $usuario_ini
 * @property string $usuario_act
 *
 * The followings are the available model relations:
 * @property Cliente $cliente
 */
class ClienteContacto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'servicio.cliente_contacto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, apellido, documento_identidad, tdocumento_identidad, fecha_nacimiento, sexo, telefono_celular, telefono_oficina, correo, cliente_id, fecha_ini, usuario_ini', 'required'),
			array('cliente_id', 'numerical', 'integerOnly'=>true),
			array('nombre, apellido', 'length', 'max'=>120),
			array('documento_identidad', 'length', 'max'=>10),
			array('tdocumento_identidad, sexo', 'length', 'max'=>1),
			array('telefono_celular, telefono_oficina', 'length', 'max'=>11),
			array('correo', 'length', 'max'=>250),
			array('usuario_ini, usuario_act', 'length', 'max'=>20),
			array('correo', 'email'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, apellido, documento_identidad, tdocumento_identidad, fecha_nacimiento, sexo, telefono_celular, telefono_oficina, correo, cliente_id, fecha_ini, fecha_act, usuario_ini, usuario_act', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cliente' => array(self::BELONGS_TO, 'Cliente', 'cliente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'apellido' => 'Apellido',
			'documento_identidad' => 'Documento Identidad',
			'tdocumento_identidad' => 'Tdocumento Identidad',
			'fecha_nacimiento' => 'Fecha Nacimiento',
			'sexo' => 'Sexo',
			'telefono_celular' => 'Telefono Celular',
			'telefono_oficina' => 'Telefono Oficina',
			'correo' => 'Correo',
			'cliente_id' => 'Cliente',
			'fecha_ini' => 'Fecha Ini',
			'fecha_act' => 'Fecha Act',
			'usuario_ini' => 'Usuario Ini',
			'usuario_act' => 'Usuario Act',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		if(strlen($this->nombre)>0) $criteria->compare('nombre',$this->nombre,true);
		if(strlen($this->apellido)>0) $criteria->compare('apellido',$this->apellido,true);
		if(strlen($this->documento_identidad)>0) $criteria->compare('documento_identidad',$this->documento_identidad,true);
		if(strlen($this->tdocumento_identidad)>0) $criteria->compare('tdocumento_identidad',$this->tdocumento_identidad,true);
		if(Utiles::isValidDate($this->fecha_nacimiento, 'y-m-d')) $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento);
		// if(strlen($this->fecha_nacimiento)>0) $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		if(strlen($this->sexo)>0) $criteria->compare('sexo',$this->sexo,true);
		if(strlen($this->telefono_celular)>0) $criteria->compare('telefono_celular',$this->telefono_celular,true);
		if(strlen($this->telefono_oficina)>0) $criteria->compare('telefono_oficina',$this->telefono_oficina,true);
		if(strlen($this->correo)>0) $criteria->compare('correo',$this->correo,true);
		if(is_numeric($this->cliente_id)) $criteria->compare('cliente_id',$this->cliente_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(strlen($this->usuario_ini)>0) $criteria->compare('usuario_ini',$this->usuario_ini,true);
		if(strlen($this->usuario_act)>0) $criteria->compare('usuario_act',$this->usuario_act,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ClienteContacto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
