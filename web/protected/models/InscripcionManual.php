<?php

/**
 * This is the model class for table "matricula.inscripcion_manual".
 *
 * The followings are the available columns in table 'matricula.inscripcion_manual':
 * @property integer $id
 * @property string $plantel
 * @property string $cod_plantel
 * @property integer $cod_plan
 * @property integer $estudiante_id
 * @property string $origen
 * @property string $documento_identidad
 * @property string $nombres
 * @property string $apellidos
 * @property string $documento_identidad_rep
 * @property string $fecha_nacimiento
 * @property string $sexo
 * @property integer $afinidad_id
 * @property string $estatus_inscripcion
 * @property integer $plantel_id
 * @property integer $seccion_id
 * @property integer $grado_id
 * @property integer $periodo_id
 * @property integer $seccion_plantel_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $estatus
 * @property integer $estado_id
 * @property integer $turno_id
 * @property string $nombres_rep
 * @property string $apellidos_rep
 * @property string $origen_rep
 *  @property integer $repitiente_est
 *
 * The followings are the available model relations:
 * @property Grado $grado
 * @property PeriodoEscolar $periodo
 * @property Plantel $plantel
 * @property Seccion $seccion
 * @property SeccionPlantel $seccionPlantel
 * @property Turno $turno
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 */
class InscripcionManual extends CActiveRecord {

  //  public $repitiente_est;
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'matricula.inscripcion_manual';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombres, apellidos, origen, documento_identidad, sexo, fecha_nacimiento, cod_plantel, grado_id, seccion_id, turno_id', 'required'),
            array('cod_plan, estudiante_id, afinidad_id, plantel_id, seccion_id, grado_id, periodo_id, seccion_plantel_id, usuario_ini_id, usuario_act_id, estado_id, turno_id', 'numerical', 'integerOnly' => true),
            array('plantel', 'length', 'max' => 150),
            array('cod_plantel', 'length', 'max' => 15),
            array('origen, sexo, estatus_inscripcion, estatus, origen_rep', 'length', 'max' => 1),
            array('documento_identidad, documento_identidad_rep', 'length', 'max' => 20),
            array('nombres, apellidos, nombres_rep, apellidos_rep', 'length', 'max' => 120),
            array('origen', 'in', 'range' => array('V', 'E', 'P', 'C'), 'allowEmpty' => true, 'strict' => true,),
            array('estatus', 'in', 'range' => array('A', 'I', 'E'), 'allowEmpty' => true, 'strict' => true,),
            array('usuario_ini_id', 'default', 'value' => Yii::app()->user->id, 'on' => 'insert'),
            array('usuario_act_id', 'default', 'value' => Yii::app()->user->id, 'on' => 'update'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, plantel, cod_plantel, cod_plan, estudiante_id, origen, documento_identidad, nombres, apellidos, documento_identidad_rep, fecha_nacimiento, sexo, afinidad_id, estatus_inscripcion, plantel_id, seccion_id, grado_id, periodo_id, seccion_plantel_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, estatus, estado_id, turno_id, nombres_rep, apellidos_rep, origen_rep', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'grado' => array(self::BELONGS_TO, 'Grado', 'grado_id'),
            'periodo' => array(self::BELONGS_TO, 'PeriodoEscolar', 'periodo_id'),
            'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
            'seccion' => array(self::BELONGS_TO, 'Seccion', 'seccion_id'),
            'seccionPlantel' => array(self::BELONGS_TO, 'SeccionPlantel', 'seccion_plantel_id'),
            'turno' => array(self::BELONGS_TO, 'Turno', 'turno_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'plantel' => 'Plantel',
            'cod_plantel' => 'Código del Plantel',
            'cod_plan' => 'Código de Plan',
            'estudiante_id' => 'Estudiante',
            'origen' => 'Origen',
            'documento_identidad' => 'Documento Identidad',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'documento_identidad_rep' => 'Documento Identidad',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'sexo' => 'Sexo',
            'afinidad_id' => 'Afinidad',
            'estatus_inscripcion' => 'Estatus Inscripcion',
            'plantel_id' => 'Plantel',
            'seccion_id' => 'Seccion',
            'grado_id' => 'Grado',
            'periodo_id' => 'Periodo',
            'seccion_plantel_id' => 'Seccion Plantel',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'estatus' => 'Estatus',
            'estado_id' => 'Estado',
            'turno_id' => 'Turno',
            'nombres_rep' => 'Nombres',
            'apellidos_rep' => 'Apellidos',
            'origen_rep' => 'Origen',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        if (is_numeric($this->id))
            $criteria->compare('id', $this->id);
        if (strlen($this->plantel) > 0)
            $criteria->addSearchCondition('plantel', '%' . $this->plantel . '%', false, 'AND', 'ILIKE');
        if (strlen($this->cod_plantel) > 0)
            $criteria->addSearchCondition('cod_plantel', '%' . $this->cod_plantel . '%', false, 'AND', 'ILIKE');
        if (is_numeric($this->cod_plan))
            $criteria->compare('cod_plan', $this->cod_plan);
        if (is_numeric($this->estudiante_id))
            $criteria->compare('estudiante_id', $this->estudiante_id);
        if (strlen($this->origen) > 0)
            $criteria->compare('origen', $this->origen, true);
        if (strlen($this->nombres) > 0)
            $criteria->addSearchCondition('nombres', '%' . $this->nombres . '%', false, 'AND', 'ILIKE');
        if (strlen($this->apellidos) > 0)
            $criteria->addSearchCondition('apellidos', '%' . $this->apellidos . '%', false, 'AND', 'ILIKE');
        if (strlen($this->documento_identidad) > 0)
            $criteria->addSearchCondition('documento_identidad', '%' . $this->documento_identidad . '%', false, 'AND', 'ILIKE');
        if (strlen($this->nombres_rep) > 0)
            $criteria->addSearchCondition('nombres', '%' . $this->nombres_rep . '%', false, 'AND', 'ILIKE');
        if (strlen($this->apellidos_rep) > 0)
            $criteria->addSearchCondition('apellidos', '%' . $this->apellidos_rep . '%', false, 'AND', 'ILIKE');
        if (strlen($this->documento_identidad_rep) > 0)
            $criteria->addSearchCondition('documento_identidad', '%' . $this->documento_identidad_rep . '%', false, 'AND', 'ILIKE');
        if (Utiles::isValidDate($this->fecha_nacimiento, 'y-m-d'))
            $criteria->compare('fecha_nacimiento', $this->fecha_nacimiento);
        // if(strlen($this->fecha_nacimiento)>0) $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
        if (strlen($this->sexo) > 0)
            $criteria->compare('sexo', $this->sexo, true);
        if (is_numeric($this->afinidad_id))
            $criteria->compare('afinidad_id', $this->afinidad_id);
        if (strlen($this->estatus_inscripcion) > 0)
            $criteria->compare('estatus_inscripcion', $this->estatus_inscripcion, true);
        if (is_numeric($this->plantel_id))
            $criteria->compare('plantel_id', $this->plantel_id);
        if (is_numeric($this->seccion_id))
            $criteria->compare('seccion_id', $this->seccion_id);
        if (is_numeric($this->grado_id))
            $criteria->compare('grado_id', $this->grado_id);
        if (is_numeric($this->periodo_id))
            $criteria->compare('periodo_id', $this->periodo_id);
        if (is_numeric($this->seccion_plantel_id))
            $criteria->compare('seccion_plantel_id', $this->seccion_plantel_id);
        if (is_numeric($this->usuario_ini_id))
            $criteria->compare('usuario_ini_id', $this->usuario_ini_id);
        if (Utiles::isValidDate($this->fecha_ini, 'y-m-d'))
            $criteria->compare('fecha_ini', $this->fecha_ini);
        // if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
        if (is_numeric($this->usuario_act_id))
            $criteria->compare('usuario_act_id', $this->usuario_act_id);
        if (Utiles::isValidDate($this->fecha_act, 'y-m-d'))
            $criteria->compare('fecha_act', $this->fecha_act);
        // if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
        if (in_array($this->estatus, array('A', 'I', 'E')))
            $criteria->compare('estatus', $this->estatus, true);
        if (is_numeric($this->estado_id))
            $criteria->compare('estado_id', $this->estado_id);
        if (is_numeric($this->turno_id))
            $criteria->compare('turno_id', $this->turno_id);
        if (strlen($this->origen_rep) > 0)
            $criteria->compare('origen_rep', $this->origen_rep, true);

        $sort = new CSort();
        $sort->defaultOrder = 'estatus_inscripcion ASC, estatus ASC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
        ));
    }

    public function beforeInsert() {
        parent::beforeSave();
        $this->fecha_ini = date('Y-m-d H:i:s');
        $this->usuario_ini_id = Yii::app()->user->id;
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeUpdate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeDelete() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        // $this->fecha_eli = $this->fecha_act;
        $this->estatus = 'I';
        return true;
    }

    public function beforeActivate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        $this->estatus = 'A';
        return true;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return InscripcionManual the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function obtenerNombresPlantel($term, $variable = '') {
        if ($variable == '') {
            $qtxt = "SELECT DISTINCT cod_plantel as nombre
                        FROM  gplantel.plantel
                        WHERE cod_plantel LIKE :username
                        ORDER BY cod_plantel ASC limit 10";
            $command = Yii::app()->db->createCommand($qtxt);
            $command->bindValue(":username", '%' . $term . '%', PDO::PARAM_STR);
            $res = $command->queryColumn();
            return $res;
        } elseif ($variable == 1) {
            $sql = "SELECT DISTINCT p.nombre AS nombre, p.estado_id AS estado
                        FROM  gplantel.plantel p
                        LEFT JOIN estado e ON (e.id=p.estado_id)
                        WHERE p.cod_plantel=:cod_plantel
                        ORDER BY p.nombre ASC limit 1";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(":cod_plantel", $term, PDO::PARAM_STR);
            $res = $command->queryRow();
            return $res;
        }
    }

    public function updateEstatus($id, $estatus, $usuario_act_id, $fecha) {
        $sql = "UPDATE matricula.inscripcion_manual
                        SET estatus_inscripcion=:estatus,
                               usuario_act_id=:usuario_act_id,
                               fecha_act=:fecha_act
                        WHERE id=:inscripcion_id";
        $guardar = Yii::app()->db->createCommand($sql);
        $guardar->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $guardar->bindParam(":inscripcion_id", $id, PDO::PARAM_INT);
        $guardar->bindParam(":usuario_act_id", $usuario_act_id, PDO::PARAM_INT);
        $guardar->bindParam(":fecha_act", $fecha, PDO::PARAM_STR);
        $res = $guardar->execute();
        return $res;
    }

}
