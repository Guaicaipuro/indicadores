<?php

/**
 * This is the model class for table "reporte_total_app".
 *
 * The followings are the available columns in table 'reporte_total_app':
 * @property string $nombre
 * @property double $total
 * @property integer $categoria_id
 *
 * The followings are the available model relations:
 * @property CategoriaApp $categoria
 */
class ReporteTotalApp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reporte_total_app';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('categoria_id', 'required'),
			array('categoria_id', 'numerical', 'integerOnly'=>true),
			array('total', 'numerical'),
			array('nombre', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nombre, total, categoria_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categoria' => array(self::BELONGS_TO, 'CategoriaApp', 'categoria_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nombre' => 'Nombre',
			'total' => 'Total',
			'categoria_id' => 'Categoria',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(strlen($this->nombre)>0) $criteria->compare('nombre',$this->nombre,true);
		if(is_numeric($this->total)) $criteria->compare('total',$this->total);
		if(is_numeric($this->categoria_id)) $criteria->compare('categoria_id',$this->categoria_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function beforeInsert()
	{
		parent::beforeSave();
		$this->fecha_ini = date('Y-m-d H:i:s');
		$this->usuario_ini_id = Yii::app()->user->id;
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		return true;
	}

	public function beforeUpdate()
	{
		parent::beforeSave();
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		return true;
	}

	public function beforeDelete(){
		parent::beforeSave();
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		// $this->fecha_eli = $this->fecha_act;
		$this->estatus = 'I';
		return true;
	}

	public function beforeActivate(){
		parent::beforeSave();
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		$this->estatus = 'A';
		return true;
	}

	public  function getDataCategoria($id){
		$result = array();
		if(is_numeric($id)){
			$sql = "SELECT nombre,total,categoria_id FROM reporte_total_app WHERE categoria_id=:categoria_id order by orden";
			$connection = Yii::app()->db;
			$command = $connection->createCommand($sql);
			$command->bindParam('categoria_id',$id,PDO::PARAM_INT);
			$result = $command->queryAll();
		}
		return $result;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReporteTotalApp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
