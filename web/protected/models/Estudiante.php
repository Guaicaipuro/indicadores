<?php

/**
 * This is the model class for table "matricula.estudiante".
 *
 * The followings are the available columns in table 'matricula.estudiante':
 * @property integer $id
 * @property integer $cedula_escolar
 * @property integer $documento_identidad
 * @property string $nombres
 * @property string $apellidos
 * @property string $fecha_nacimiento
 * @property string $correo
 * @property string $telefono_movil
 * @property string $telefono_habitacion
 * @property string $plantel_anterior_id
 * @property string $lateralidad_mano
 * @property string $identificacion_extranjera
 * @property string $ciudad_nacimiento
 * @property integer $pais_id
 * @property integer $etnia_id
 * @property integer $estado_civil_id
 * @property integer $diversidad_funcional_id
 * @property string $sexo
 * @property integer $condicion_vivienda_id
 * @property integer $zona_ubicacion_id
 * @property integer $tipo_vivienda_id
 * @property integer $ubicacion_vivienda_id
 * @property string $ingreso_familiar
 * @property integer $condicion_infraestructura_id
 * @property integer $beca
 * @property integer $canaima
 * @property string $serial_canaima
 * @property string $nacionalidad
 * @property integer $estado_nac_id
 * @property integer $municipio_nac_id
 * @property integer $parroquia_nac_id
 * @property string $direccion_nac
 * @property integer $estado_id
 * @property integer $municipio_id
 * @property integer $parroquia_id
 * @property string $direccion_dom
 * @property integer $poblacion_id
 * @property integer $urbanizacion_id
 * @property integer $tipo_via_id
 * @property string $via
 * @property integer $representante_id
 * @property string $plantel_actual_id
 * @property string $descripcion_afinidad
 * @property integer $otro_represent_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $idioma_etnia
 * @property string $tdocumento_identidad
 *
 * The followings are the available model relations:
 * @property Representante $otroRepresent
 * @property Pais $pais
 * @property Estado $estado
 * @property Estado $estadoNac
 * @property Municipio $municipio
 * @property Municipio $municipioNac
 * @property Parroquia $parroquia
 * @property Parroquia $parroquiaNac
 * @property Poblacion $poblacion
 * @property TipoVia $tipoVia
 * @property Urbanizacion $urbanizacion
 * @property Representante $representante
 * @property TipoVivienda $tipoVivienda
 * @property UbicacionVivienda $ubicacionVivienda
 * @property CondicionInfraestructura $condicionInfraestructura
 * @property ZonaUbicacion $zonaUbicacion
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property CondicionVivienda $condicionVivienda
 * @property DiversidadFuncional $diversidadFuncional
 * @property EstadoCivil $estadoCivil
 * @property Etnia $etnia
 * @property InscripcionEstudiante[] $inscripcionEstudiantes
 * @property DatosAntropometricos[] $datosAntropometricoses
 * @property HistorialMedico[] $historialMedicos
 */
class Estudiante extends CActiveRecord {

    public $cirepresentante; /* NO ELIMINAR JEAN CARLOS */
    public $documento_identidad_representante;
    public $codigo_plantel;

    CONST INDICE_ESTUD_INSCRIP_PLANTEL_PERIODO_SECCION = 'INDICE_ESTUD_INSCRIP_PLANTEL_PERIODO_SECCION:';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'matricula.estudiante';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            //array('nombres, apellidos, fecha_nacimiento', 'required'),
            array('
                    nombres,
                    apellidos,
                    fecha_nacimiento,
                    estado_civil_id,
                    nacionalidad,
                    pais_id,
                    estado_id,
                    municipio_id,
                    parroquia_id,
                    poblacion_id,
                    urbanizacion_id,
                    tipo_via_id,
                    via,
                    direccion_dom,
                    zona_ubicacion_id,
                    tipo_vivienda_id,
                    ubicacion_vivienda_id,
                    condicion_vivienda_id,
                    condicion_infraestructura_id,
                    beca,
                    canaima',
                'required', 'on' => 'gestionEstudianteCrear'),
            array('
                    nombres,
                    apellidos,
                    fecha_nacimiento,
                    estado_civil_id,
                    nacionalidad,
                    pais_id,
                    estado_id,
                    municipio_id,
                    parroquia_id,
                    poblacion_id,
                    urbanizacion_id,
                    tipo_via_id,
                    via,
                    direccion_dom,
                    zona_ubicacion_id,
                    tipo_vivienda_id,
                    ubicacion_vivienda_id,
                    condicion_vivienda_id,
                    condicion_infraestructura_id,
                    beca,
                    canaima',
                'required', 'on' => 'gestionEstudiante'),
            array('documento_identidad,', 'validarIdentificacion', 'required', 'on' => 'moddrcee,gestionEstudiante,gestionEstudianteCrear'),
            array('documento_identidad,', 'verificarUnicidadEstudiante', 'required', 'on' => 'moddrcee,gestionEstudiante,gestionEstudianteCrear'),
            array('
                nombres,
                apellidos,
                fecha_nacimiento,
                estado_civil_id,
                nacionalidad,
                pais_id,
                estado_id',
                'required', 'on' => 'moddrcee'),
            array('
            documento_identidad,
            nombres,
                    apellidos,
                    fecha_nacimiento,
                    estado_civil_id,
                    nacionalidad,
                    pais_id,
                    estado_id,
                    municipio_id,
                    parroquia_id,
                    poblacion_id,
                    urbanizacion_id,
                    tipo_via_id,
                    via,
                    direccion_dom,
                    zona_ubicacion_id,
                    tipo_vivienda_id,
                    ubicacion_vivienda_id,
                    condicion_vivienda_id,
                    condicion_infraestructura_id,
                    beca,                   canaima',
                'required', 'on' => 'gestionRepresentante'),

            /**
             * @autor Jonathan Huaman. fecha 09-04-2015
             * Sirve para Añadir nueva validación sobre la Creación y Modificación del Estudiante.
             * Validación Consiste en Verificar: Si el Estudiante Pertenece Tiene etnia, se Valida si Habla el Idioma de la Etnia Escogida.
             */
            array('etnia_id','verificarIdiomaEtnia', 'on' => 'gestionEstudiante,gestionEstudianteCrear'),

            array('cedula_escolar, pais_id, etnia_id, estado_civil_id, diversidad_funcional_id, condicion_vivienda_id, zona_ubicacion_id, tipo_vivienda_id, ubicacion_vivienda_id, condicion_infraestructura_id, beca, canaima, estado_nac_id, municipio_nac_id, parroquia_nac_id, estado_id, municipio_id, parroquia_id, poblacion_id, urbanizacion_id, tipo_via_id, representante_id, otro_represent_id, usuario_ini_id, usuario_act_id, estatus_id', 'numerical', 'integerOnly' => true),
            array('nombres, ciudad_nacimiento, descripcion_afinidad', 'length', 'min' => 3, 'max' => 120),
            /* array('documento_identidad', 'numerical', 'integerOnly' => true, 'max' => 8), */
            array('nombres, apellidos', 'length', 'min' => 3, 'max' => 120, 'on' => 'crearEstudiante'),
            //array('nombres, apellidos', 'length', 'min' => 3, 'max' => 120, 'on' => 'estudianteMayor'),
            array('documento_identidad', 'length', 'max' => 120),
            array('cedula_escolar, estado_nac_id, representante_id, usuario_ini_id, usuario_act_id, estatus_id, plantel_actual_id, plantel_anterior_id', 'numerical', 'integerOnly' => true, 'on' => 'crearEstudiante'),
            array('correo', 'length', 'max' => 40),
            array('documento_identidad', 'required', 'message' => 'La fecha de nacimiento del Estudiante indica que es mayor de edad, ingrese la Cédula de Identidad para continuar.', 'on' => 'estudianteMayor'),
            array('nombres, apellidos, cedula_escolar, fecha_nacimiento', 'required', 'on' => 'crearEstudiante'),
            array('documento_identidad, tdocumento_identidad,nombres, apellidos, cedula_escolar, fecha_nacimiento', 'required', 'on' => 'estudianteMayor'),
            //array('cedula_escolar', 'unique', 'on' => 'crearEstudiante'),
//            array('documento_identidad', 'unique', 'on' => 'crearEstudiante'),
            //array('documento_identidad, tdocumento_identidad', 'ECompositeUniqueValidator', 'attributesToAddError'=>'cedula', 'message'=>'Ya se encuentra registrada el Documento de Identidad del estudiante.'),
            // array('cedula_escolar', 'unique', 'on' => 'estudianteMayor'),
            array('telefono_movil, telefono_habitacion, plantel_anterior_id, plantel_actual_id', 'length', 'max' => 15),
            array('lateralidad_mano', 'length', 'max' => 3),
            array('identificacion_extranjera', 'length', 'max' => 20),
            array('nacionalidad, estatus', 'length', 'max' => 1),
            array('direccion_nac', 'length', 'max' => 50),
            //array('cedula_escolar', 'unique'),
            //array('fecha_ini, fecha_act, fecha_elim', 'length', 'max' => 6),
            array('fecha_nacimiento, ingreso_familiar, serial_canaima, direccion_dom, via', 'safe'),
            // The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
            array('id, estatud_id,nombres,apellidos, cedula_escolar, documento_identidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, fecha_nacimiento, correo, telefono_movil, telefono_habitacion, plantel_anterior_id, lateralidad_mano, identificacion_extranjera, ciudad_nacimiento, pais_id, etnia_id, estado_civil_id, diversidad_funcional_id, sexo, condicion_vivienda_id, zona_ubicacion_id, tipo_vivienda_id, ubicacion_vivienda_id, ingreso_familiar, condicion_infraestructura_id, beca, canaima, serial_canaima, nacionalidad, estado_nac_id, municipio_nac_id, parroquia_nac_id, direccion_nac, estado_id, municipio_id, parroquia_id, direccion_dom, poblacion_id, urbanizacion_id, tipo_via_id, via, representante_id, plantel_actual_id, descripcion_afinidad, otro_represent_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'otroRepresent' => array(self::BELONGS_TO, 'Representante', 'otro_represent_id'),
            'pais' => array(self::BELONGS_TO, 'Pais', 'pais_id'),
            'estado' => array(self::BELONGS_TO, 'Estado', 'estado_id'),
            'estadoNac' => array(self::BELONGS_TO, 'Estado', 'estado_nac_id'),
            'municipio' => array(self::BELONGS_TO, 'Municipio', 'municipio_id'),
            'municipioNac' => array(self::BELONGS_TO, 'Municipio', 'municipio_nac_id'),
            'parroquia' => array(self::BELONGS_TO, 'Parroquia', 'parroquia_id'),
            'parroquiaNac' => array(self::BELONGS_TO, 'Parroquia', 'parroquia_nac_id'),
            /* CREANDO LAS RELACIONES DE LOS PLANTELES **BY ENRIQUEX** */
            'plantelActual' => array(self::BELONGS_TO, 'Plantel', 'plantel_actual_id'),
            'plantelAnterior' => array(self::BELONGS_TO, 'Plantel', 'plantel_anterior_id'),
            'gradoActual' => array(self::BELONGS_TO, 'Grado', 'grado_actual_id'),
            'gradoAnterior' => array(self::BELONGS_TO, 'Grado', 'grado_anterior_id'),
            /* FIN DE CREACION DE PLANTELES */
            'poblacion' => array(self::BELONGS_TO, 'Poblacion', 'poblacion_id'),
            'tipoVia' => array(self::BELONGS_TO, 'TipoVia', 'tipo_via_id'),
            'urbanizacion' => array(self::BELONGS_TO, 'Urbanizacion', 'urbanizacion_id'),
            'representante' => array(self::BELONGS_TO, 'Representante', 'representante_id'),
            'tipoVivienda' => array(self::BELONGS_TO, 'TipoVivienda', 'tipo_vivienda_id'),
            'ubicacionVivienda' => array(self::BELONGS_TO, 'UbicacionVivienda', 'ubicacion_vivienda_id'),
            'condicionInfraestructura' => array(self::BELONGS_TO, 'CondicionInfraestructura', 'condicion_infraestructura_id'),
            'zonaUbicacion' => array(self::BELONGS_TO, 'ZonaUbicacion', 'zona_ubicacion_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'condicionVivienda' => array(self::BELONGS_TO, 'CondicionVivienda', 'condicion_vivienda_id'),
            'diversidadFuncional' => array(self::BELONGS_TO, 'DiversidadFuncional', 'diversidad_funcional_id'),
            'estadoCivil' => array(self::BELONGS_TO, 'EstadoCivil', 'estado_civil_id'),
            'etnia' => array(self::BELONGS_TO, 'Etnia', 'etnia_id'),
            'inscripcionEstudiantes' => array(self::HAS_MANY, 'InscripcionEstudiante', 'estudiante_id'),
            'datosAntropometricoses' => array(self::HAS_MANY, 'DatosAntropometricos', 'estudiante_id'),
            'historialMedicos' => array(self::HAS_MANY, 'HistorialMedico', 'estudiante_id'),
            'estatusEstudiante' => array(self::BELONGS_TO, 'EstatusEstudiante', 'estatus_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'cedula_escolar' => 'Cédula Escolar',
            'documento_identidad' => 'Documento de Identidad',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'fecha_nacimiento' => 'Fecha de Nacimiento',
            'correo' => 'Correo',
            'telefono_movil' => 'Teléfono Movil',
            'telefono_habitacion' => 'Teléfono Habitacion',
            'plantel_anterior_id' => 'Plantel al que pertenecía el estudiante',
            'lateralidad_mano' => 'Lateralidad de la mano',
            'identificacion_extranjera' => 'Identificacion Extranjera',
            'ciudad_nacimiento' => 'Ciudad de Nacimiento',
            'pais_id' => 'Pais de nacimiento',
            'etnia_id' => 'Pueblos y Comunidades Indigenas',
            'estado_civil_id' => 'Estado Civil',
            'diversidad_funcional_id' => 'Area de Atención',
            'sexo' => 'Genero',
            'condicion_vivienda_id' => 'Condicion Vivienda',
            'zona_ubicacion_id' => 'Zona Ubicacion',
            'tipo_vivienda_id' => 'Tipo Vivienda',
            'ubicacion_vivienda_id' => 'Ubicacion Vivienda',
            'ingreso_familiar' => 'Ingreso Familiar',
            'condicion_infraestructura_id' => 'Condicion Infraestructura',
            'beca' => 'Beca',
            'canaima' => 'Canaima',
            'serial_canaima' => 'Serial Canaima',
            'nacionalidad' => 'Nacionalidad',
            'estado_nac_id' => 'Estado de nacimiento del estudiante',
            'municipio_nac_id' => 'Municipio de nacimiento del estudiante',
            'parroquia_nac_id' => 'Parroquia de nacimiento del estudiante',
            'direccion_nac' => 'Direccion de nacimiento del estudiante',
            'estado_id' => 'Estado de domicilio',
            'municipio_id' => 'Municipio de domicilio',
            'parroquia_id' => 'Parroquia de domicilio',
            'direccion_dom' => 'Direccion de domicilio',
            'poblacion_id' => 'Población de domicilio',
            'urbanizacion_id' => 'Urbanizacion de domicilio',
            'tipo_via_id' => 'Tipo de via de domicilio',
            'via' => 'Via de domicilio',
            'representante_id' => 'Representante legal del estudiante',
            'plantel_actual_id' => 'código del plantel al que pertenece el estudiante',
            'descripcion_afinidad' => 'descripcion de la afinidad de otro representante.',
            'otro_represent_id' => 'Otro Representante',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'estatus_id' => 'Estatus Actual del estudiante',
            'tdocumento_identidad' => 'Tipo de Documento'
        );
    }

    public function validarIdentificacion($attributo, $params = null) {
        if (($this->$attributo == null OR $this->$attributo == '') AND ( $this->cedula_escolar == null OR $this->cedula_escolar == '')) {
            $this->addError($attributo, 'Debe ingresar el Documento de Identidad o la Cedula Escolar del Estudiante');
        } else {
            if ($this->$attributo != null AND $this->$attributo != '' AND ( $this->tdocumento_identidad == '' OR $this->tdocumento_identidad == null)) {
                $this->addError('tdocumento_identidad', 'Debe seleccionar un Tipo de Documento de Identidad');
            }
        }
    }

    /**
     * @autor Jonathan Huaman Fecha: 09-04-2015.
     * @param type $attributo -> Recibe El Campo "etnia_id"
     * @param type $params
     * @version 1.0
     */
    public function verificarIdiomaEtnia($attributo,$params = null) {
        if ( ($this->$attributo != null OR $this->$attributo != '') )
        {
            if($this->idioma_etnia==null OR $this->idioma_etnia=='')
                $this->addError('idioma_etnia', 'Debe Especificar si El Estudiante habla el Idioma Seleccionado');
        }
    }

    /**
     * @autor Ignacio Salazar
     * @param type $attributo -> documento_identidad
     * @param type $params null
     * @version 1.0 (19-04-2015)
     */
    public function verificarUnicidadEstudiante($attributo,$params = null) {
        $existe = false;
        if (($this->$attributo != null AND  $this->$attributo != '' AND $this->tdocumento_identidad!=null AND $this->tdocumento_identidad!=''))
        {
            $existe = $this->existeEstudiante($this->tdocumento_identidad,$this->$attributo);
            if(isset($existe[0]) AND $existe[0] != $this->id){
                $this->addError(null, 'Ya existe un estudiante registrado con la identificacion <strong>'.$this->tdocumento_identidad.'-'.$this->$attributo.'</strong>.');
            }
        }
        if (($this->cedula_escolar != null AND  $this->cedula_escolar != ''))
        {
            $existe = $this->existeEstudiante('C',$this->cedula_escolar);
            if(isset($existe[0]) AND $existe[0] != $this->id){
                $this->addError(null, 'Ya existe un estudiante registrado con la identificacion <strong>CE-'.$this->cedula_escolar.'</strong>.');
            }
        }


    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
// @todo Please modify the following code to remove attributes that should not be searched.
        $groupId = Yii::app()->user->group;
        $usuarioId = Yii::app()->user->id;
        $ejecutar = false;
        $nombres = NULL;
        $apellidos = NULL;
        $criteria = new CDbCriteria;

        /* if (!isset($_GET['Estudiante'])) {
          $this->id = 0;
          $no_ejecutar = true;
          } else {
          $estudiante = $_GET['Estudiante'];
          if (!isset($estudiante['documento_identidad_representante'])) {
          if ($estudiante['apellidos'] == '' &&
          $estudiante['cedula_escolar'] == '' &&
          $estudiante['documento_identidad'] == '' &&
          $estudiante['nombres'] == '' &&
          $estudiante['plantel_actual_id'] == '' &&
          $estudiante['representante_id'] == ''
          ) {
          $this->id = 0;
          $no_ejecutar = true;
          }
          } else {

          if ($estudiante['apellidos'] == '' &&
          $estudiante['cedula_escolar'] == '' &&
          $estudiante['documento_identidad'] == '' &&
          $estudiante['documento_identidad_representante'] == '' &&
          $estudiante['nombres'] == '' &&
          $estudiante['codigo_plantel'] == '' &&
          $estudiante['estado_id'] == '') {
          $this->id = 0;
          $no_ejecutar = true;
          }
          }
          } */
        if (isset($this->nombres) OR isset($this->apellidos) OR isset($this->cedula_escolar) OR isset($this->documento_identidad) OR isset($this->plantel_id) OR isset($this->representante_id) OR isset($this->codigo_plantel) OR isset($this->documento_identidad_representante) OR isset($this->estado_id)) {

            if (is_numeric($this->documento_identidad)) {
                if (strlen($this->documento_identidad) <= 15) {
                    $criteria->compare('t.documento_identidad', $this->documento_identidad);
                    $ejecutar = true;
                }
            }
            if (is_numeric($this->cedula_escolar)) {
                if (strlen($this->cedula_escolar) <= 20) {
                    $criteria->compare('cedula_escolar', $this->cedula_escolar);
                    $ejecutar = true;
                }
            }
            /* if (strlen($this->nombres)>3 AND  strlen($this->apellidos)>3) {
              $criteria->addCondition("t.busqueda @@ plainto_tsquery(:configuracion, :busqueda)");
              $criteria->params = array_merge($criteria->params, array(':configuracion' => 'pg_catalog.spanish'));
              $criteria->params = array_merge($criteria->params, array(':busqueda' => trim($this->nombres) . ' ' . trim($this->apellidos)));
              $ejecutar = true;
              } */
            if (is_numeric($this->representante_id)) {
                $criteria->join .= " LEFT JOIN matricula.representante r ON r.id = t.representante_id";
                $criteria->addCondition("r.documento_identidad IS NOT NULL AND r.documento_identidad = :documento_identidad");
                $criteria->params = array_merge($criteria->params, array(':documento_identidad' => $this->representante_id));
                $ejecutar = true;
            }

            if ($groupId == UserGroups::DIRECTOR) {
                if (isset($this->plantel_actual_id) AND is_numeric($this->plantel_actual_id)) {
                    $criteria->join .= " LEFT JOIN gplantel.autoridad_plantel ON t.plantel_actual_id = gplantel.autoridad_plantel.plantel_id ";
                    $criteria->addCondition("gplantel.autoridad_plantel.usuario_id = :usuario AND t.plantel_actual_id = :plantel_id");
                    $criteria->params = array_merge($criteria->params, array(':usuario' => (int) $usuarioId));
                    $criteria->params = array_merge($criteria->params, array(':plantel_id' => (int) $this->plantel_actual_id));
                    $ejecutar = true;
                }
            }
            if ($this->plantel_actual_id) {
                $criteria->join .= " LEFT JOIN gplantel.plantel p ON p.id = t.plantel_actual_id";
                $criteria->addCondition("p.cod_plantel ILIKE :cod_plantel");
                $criteria->params = array_merge($criteria->params, array(':cod_plantel' => $this->plantel_actual_id));
                $ejecutar = true;
            }
            if (is_numeric($this->estado_id)) {
                if (strlen($this->estado_id) <= 10) {
                    $criteria->compare('t.estado_id', $this->estado_id);
                    $ejecutar = true;
                }
            }
        }

        $criteria->order = 'id DESC';

        if ($ejecutar) {
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        } else {
            return new CArrayDataProvider(array(), array(
                    'pagination' => array(
                        'pageSize' => 5,
                    ),
                )
            );
        }
    }

    public function obtenerPaises() {
        $sql = 'SELECT id, nombre FROM pais';
        $resultado = Yii::app()->db->createCommand($sql);
        return $resultado->queryAll();
    }

    public function obtenerDatosPais($id) {
        if (is_numeric($id)) {
            $sql = 'SELECT nombre FROM pais WHERE id = ' . $id;
            $resultado = Yii::app()->db->createCommand($sql);
            return $resultado->queryAll();
        } else {
            return null;
        }
    }

    public function obtenerDatosEtnia($id) {
        if (is_numeric($id)) {
            $sql = 'SELECT nombre FROM etnia WHERE id = ' . $id;
            $resultado = Yii::app()->db->createCommand($sql);
            return $resultado->queryAll();
        } else {
            return null;
        }
    }

    public function obtenerCondicionVivienda() {
        $sql = 'SELECT id, nombre FROM condicion_vivienda ORDER BY nombre ASC';
        $resultado = Yii::app()->db->createCommand($sql);
        return $resultado->queryAll();
    }

    public function obtenerTipoVivienda() {
        $sql = 'SELECT id, nombre FROM tipo_vivienda ORDER BY nombre ASC';
        $resultado = Yii::app()->db->createCommand($sql);
        return $resultado->queryAll();
    }

    public function obtenerUbicacionVivienda() {
        $sql = 'SELECT id, nombre FROM ubicacion_vivienda ORDER BY nombre ASC';
        $resultado = Yii::app()->db->createCommand($sql);
        return $resultado->queryAll();
    }

    public function obtenerEtnia() {
        $sql = 'SELECT id, nombre FROM etnia ORDER BY nombre ASC';
        $resultado = Yii::app()->db->createCommand($sql);
        return $resultado->queryAll();
    }

    public function obtenerDiversidadFuncional() {
        $sql = 'SELECT id, nombre FROM diversidad_funcional ORDER BY nombre ASC';
        $resultado = Yii::app()->db->createCommand($sql);
        return $resultado->queryAll();
    }

    public function obtenerTipoSangre() {
        $sql = 'SELECT id, nombre FROM tipo_sangre';
        $resultado = Yii::app()->db->createCommand($sql);
        return $resultado->queryAll();
    }

    public function obtenerProfesion() {
        $sql = 'SELECT id, nombre FROM profesion';
        $resultado = Yii::app()->db->createCommand($sql);
        return $resultado->queryAll();
    }

    public function optenerEstatusDirector($usuario_id, $plantel_actual_id) {
        if (is_numeric($usuario_id)) {
            $sql = "SELECT plantel_id FROM gplantel.autoridad_plantel WHERE usuario_id = :usuario_id AND plantel_id = :plantel_id AND estatus = 'A'";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $consulta->bindParam(":plantel_id", $plantel_actual_id, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();
        }
        if ($resultado == '' || $resultado == NULL) {
            $resultado[0]['plantel_id'] = '';
        }
        return $resultado;
    }

    /**
     * @param $documento_identidad
     * @param $cedula_escolar
     * @param $estudiante_id
     * @return array
     */
    public function historicoEstudiante($documento_identidad, $cedula_escolar, $estudiante_id) {
        $tipo_busqueda=0;
        $where = '';
        $result = array();
        if($documento_identidad!='' AND $documento_identidad!=null){
            $where = ' calumno=:documento_identidad ';
            $tipo_busqueda=0;
            if($cedula_escolar!='' AND $cedula_escolar!=null){
                $where .= ' OR calumno=:cedula_escolar ';
                $tipo_busqueda=1;
            }
        }
        else {
            if($cedula_escolar!='' AND $cedula_escolar!=null){
                $where = ' calumno=:cedula_escolar ';
                $tipo_busqueda=2;
            }
        }

        $sql = "SELECT
                        p.cod_plantel AS cod_plantel,
                        p.nombre AS nombre_plantel,
                        g.nombre AS nombre_grado,
                        s.nombre AS nombre_seccion,
                        m.nombre AS nombre_modalidad,
                        pe.periodo,
                        e.inscripcion_regular::text || e.repitiente::text || e.repitiente_completo::text || materia_pendiente::text || e.diferido::text || e.doble_inscripcion::text AS escolaridad,
                        es.nombre AS nombre_estado,
                        e.observacion AS observacion,'CURSO' as permanencia
                        FROM matricula.inscripcion_estudiante e
                        LEFT JOIN gplantel.plantel p ON p.id = e.plantel_id
                        LEFT JOIN gplantel.grado g ON g.id = e.grado_id
                        LEFT JOIN gplantel.seccion_plantel_periodo spp ON spp.id = e.seccion_plantel_periodo_id
                        LEFT JOIN gplantel.seccion_plantel sp ON sp.id = spp.seccion_plantel_id
                        LEFT JOIN gplantel.seccion s ON s.id = sp.seccion_id
                        LEFT JOIN gplantel.modalidad m ON m.id = p.modalidad_id
                        LEFT JOIN estado es ON es.id = p.estado_id
                        LEFT JOIN gplantel.periodo_escolar pe ON pe.id = e.periodo_id
                        WHERE estudiante_id = :estudiante_id AND e.estatus = 'A'
                        UNION
                        SELECT
                        p.cod_plantel AS cod_plantel,
                        p.nombre AS nombre_plantel,
                        a.s_grado_nombre AS nombre_grado,
                        a.cseccion AS nombre_seccion,
                        a.s_modalidad_nombre AS nombre_modalidad,
                        a.s_periodo_nombre AS nombre_periodo,
                        a.s_escolaridad_nombre AS escolaridad,
                        es.nombre AS nombre_estado,
                        a.dobservacion AS observacion,a.curso as permanencia
                        FROM legacy.talumno_acad a
                        LEFT JOIN gplantel.plantel p on p.cod_plantel = a.cdea
                        LEFT JOIN estado es ON es.id = p.estado_id
                        WHERE $where
                        ORDER BY periodo DESC limit 20;";
        $resultado = Yii::app()->db->createCommand($sql);
        $resultado->bindParam(":estudiante_id", $estudiante_id, PDO::PARAM_STR);
        switch($tipo_busqueda){
            case 0:
                $resultado->bindParam(":documento_identidad", $documento_identidad, PDO::PARAM_STR);
                break;
            case 1:
                $resultado->bindParam(":documento_identidad", $documento_identidad, PDO::PARAM_STR);
                $resultado->bindParam(":cedula_escolar", $cedula_escolar, PDO::PARAM_STR);
                break;
            case 2:
                $resultado->bindParam(":cedula_escolar", $cedula_escolar, PDO::PARAM_STR);
                break;
        }
        $result=$resultado->queryAll();

        return $result;
    }

    public function obtenerYAsignarRepresentanteAlEstudiante($documento_identidad_est, $tdocumento_identidad_est, $documento_identidad, $tdocumento_identidad) {
        $groupId = Yii::app()->user->group;
        $usuarioId = Yii::app()->user->id;
        $estatus_id = '';
        if ($groupId == UserGroups::DIRECTOR) {
            /* BUSQUEDA DEL ESTATUS DEL ESTATUS_ESTUDIANTE (ASIGNADO) */
            $sql = "SELECT id FROM matricula.estatus_estudiante WHERE nombre ILIKE '%ASIGNADO%'";
            $busqueda = Yii::app()->db->createCommand($sql);
            $estatus_id = $busqueda->queryAll();
        } else {

            /* BUSQUEDA DEL ESTATUS DEL ESTATUS_ESTUDIANTE (REGISTRADO) */
            $sql = "SELECT id FROM matricula.estatus_estudiante WHERE nombre ILIKE '%REGISTRADO%'";
            $busqueda = Yii::app()->db->createCommand($sql);
            $estatus_id = $busqueda->queryAll();
        }
        if (isset($documento_identidad_est) AND isset($tdocumento_identidad_est)) {
            if (!is_null($documento_identidad) and !is_null($tdocumento_identidad)) {
                $sql = 'SELECT id FROM matricula.representante WHERE documento_identidad = :documento_identidad AND tdocumento_identidad =:tdocumento_identidad ORDER BY id DESC';
                $resultado = Yii::app()->db->createCommand($sql);
                $resultado->bindParam(":documento_identidad", $documento_identidad, PDO::PARAM_INT);
                $resultado->bindParam(":tdocumento_identidad", $tdocumento_identidad, PDO::PARAM_INT);
                $r = $resultado->queryAll();
                if ($r != array()) {
                    $sql = 'UPDATE matricula.estudiante '
                        . 'SET '
                        . ' representante_id=:representante_id,'
                        . ' estatus_id =:estatus_id'
                        . ' WHERE documento_identidad = :documento_identidad AND tdocumento_identidad=:tdocumento_identidad';
                    $resultado = Yii::app()->db->createCommand($sql);
                    $resultado->bindParam(":documento_identidad", $documento_identidad_est, PDO::PARAM_INT);
                    $resultado->bindParam(":tdocumento_identidad", $tdocumento_identidad_est, PDO::PARAM_INT);
                    $resultado->bindParam(":estatus_id", $estatus_id[0]['id'], PDO::PARAM_INT);
                    $resultado->bindParam(":representante_id", $r[0]['id'], PDO::PARAM_INT);
                    return $resultado->execute();
                } else {
                    return true;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function obtenerEstudiantesPorInscripccion($plantel_id, $periodo_actual_id, $seccion_plantel_id) {


        $result = null;
        $indice = self::INDICE_ESTUD_INSCRIP_PLANTEL_PERIODO_SECCION . $plantel_id . ':' . $periodo_actual_id . ':' . $seccion_plantel_id;
        $result = Yii::app()->cache->get($indice);
        if (is_numeric($plantel_id) AND is_numeric($periodo_actual_id) AND is_numeric($seccion_plantel_id) AND !$result) {
            $gradosPrescolar [] = 3;
            $gradosPrescolar [] = 4;
            $gradosPrescolar [] = 7;
            $gradosPrescolar [] = 8;
            $gradosPrescolar [] = 11;
            $gradosPrescolar [] = 12;
            $periodo_anterior = $periodo_actual_id - 1;

            $periodo_anterior_id = PeriodoEscolar::model()->obtenerPeriodoAnterior($periodo_anterior);
//$datosSeccion = SeccionPlantel::model()->obtenerDatosSeccion($seccion_plantel_id, $plantel_id);
            $grado = SeccionPlantel::model()->obtenerGrado($seccion_plantel_id);
            if ($periodo_actual_id == 14) {
//            $sql = "SELECT e.id, upper(e.nombres) || ' ' || upper(e.apellidos) as nom_completo, e.cedula_escolar, e.fecha_nacimiento , r.cedula_identidad"
//                    . " FROM matricula.estudiante e"
//                    . " LEFT JOIN matricula.representante r on (e.representante_id = r.id)"
//                    . " INNER JOIN matricula.inscripcion_estudiante ie on (ie.estudiante_id = e.id)"
//                    . " INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.id = ie.seccion_plantel_periodo_id)"
//                    . " INNER JOIN gplantel.seccion_plantel spl on (spl.id = spp.seccion_plantel_id)"
//                    . " INNER JOIN gplantel.grado g on (spl.grado_id = g.id)"
//                    . " INNER JOIN gplantel.periodo_escolar pe on (pe.id = spp.periodo_id)"
//                    . " WHERE  spp.periodo_id = :periodo_id  AND spl.plantel_id = :plantel_id"
//                    . " AND (ie.estudiante_id not in ("
//                    . "     SELECT iest.estudiante_id from matricula.inscripcion_estudiante iest"
//                    . "     INNER JOIN gplantel.seccion_plantel_periodo spp_i on (spp_i.id = iest.seccion_plantel_periodo_id)"
//                    . "     INNER JOIN gplantel.seccion_plantel spl_i on (spl_i.id = spp_i.seccion_plantel_id)"
//                    . "     WHERE spp_i.periodo_id = :periodo_actual_id AND spl_i.plantel_id = :plantel_id)"
//                    . " )"
//                    . " AND g.consecutivo =
//                              (SELECT (g_i.consecutivo-1) FROM gplantel.grado g_i INNER JOIN  gplantel.seccion_plantel spl_i on (spl_i.grado_id = g_i.id)"
//                    . "        WHERE spl_i.id = :seccion_plantel_id"
//                    . "        )"
//                    . " ORDER BY e.cedula_escolar ASC, r.cedula_identidad ASC";
                if (in_array($grado, $gradosPrescolar)) {
                    $gradosPrescolar [] = 2; // PRIMER GRADO DEBE BUSCAR A LOS DE PREESCOLAR SOLO 2013-2014
                    $sql = "SELECT "
                        . " DISTINCT me.id, upper(me.nombres) || ' ' || upper(me.apellidos) as nom_completo, me.cedula_escolar, me.fecha_nacimiento , me.documento_identidad, 'no' as nuevoRegistro"
                        . " FROM matricula.estudiante me "
                        . " INNER JOIN gplantel.plantel p on me.plantel_actual_id=p.id "
                        . " INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id "
                        . " where "
                        . " ih.fperiodoescolar_id=:periodo_anterior_id "
                        . " AND me.plantel_actual_id = :plantel_id "
                        . " AND ih.grado_id = 0 "
                        . " AND  NOT EXISTS (select m_ie.estudiante_id from matricula.inscripcion_estudiante m_ie "
                        . " inner join gplantel.seccion_plantel_periodo  spp_i on (spp_i.id = m_ie.seccion_plantel_periodo_id) "
                        . " where spp_i.periodo_id = :periodo_actual_id AND me.id=m_ie.estudiante_id LIMIT 9223372036854775807 "
                        /* AND m_ie.estatus = 'A' */
                        . ") "
                        . " ORDER BY me.documento_identidad ASC, me.cedula_escolar ASC";


                    $busqueda = Yii::app()->db->createCommand($sql);
                } else {

                    $sql = "SELECT "
                        . " DISTINCT me.id, upper(me.nombres) || ' ' || upper(me.apellidos) as nom_completo, me.cedula_escolar, me.fecha_nacimiento , me.documento_identidad, 'no' as nuevoRegistro"
                        . " FROM matricula.estudiante me "
                        /* . " INNER JOIN gplantel.plantel p on me.plantel_actual_id=p.id " */
                        . " INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id "
                        . " INNER JOIN gplantel.grado g on (ih.grado_id = g.id) "
                        . " where "
                        . " ih.fperiodoescolar_id=:periodo_anterior_id "
                        . " AND ih.plantel_id = :plantel_id "
                        . " AND g.consecutivo =
                            (SELECT (g_i.consecutivo-1) FROM gplantel.grado g_i INNER JOIN  gplantel.seccion_plantel spl_i on (spl_i.grado_id = g_i.id)"
                        . "        WHERE spl_i.id = :seccion_plantel_id"
                        . "        )"
                        . " AND  NOT EXISTS (select m_ie.estudiante_id from matricula.inscripcion_estudiante m_ie "
                        . " inner join gplantel.seccion_plantel_periodo  spp_i on (spp_i.id = m_ie.seccion_plantel_periodo_id) "
                        . " where spp_i.periodo_id = :periodo_actual_id AND me.id=m_ie.estudiante_id LIMIT 9223372036854775807 "
                        /* AND m_ie.estatus = 'A' */
                        . ") "

                        /* . " UNION "
                          . " SELECT "
                          . " DISTINCT me.id, upper(me.nombres) || ' ' || upper(me.apellidos) as nom_completo, me.cedula_escolar, me.fecha_nacimiento , me.documento_identidad, 'no' as nuevoRegistro"
                          . " FROM matricula.estudiante me "
                          . " INNER JOIN gplantel.plantel p on me.plantel_actual_id=p.id "
                          . " INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id "
                          . " INNER JOIN gplantel.grado g on (ih.grado_id = g.id) "
                          . " where "
                          . " ih.fperiodoescolar_id=:periodo_anterior_id "
                          . " AND ih.repitiente = 1"
                          . " AND ih.plantel_id = :plantel_id "
                          . " AND g.consecutivo =
                          (SELECT (g_i.consecutivo) FROM gplantel.grado g_i INNER JOIN  gplantel.seccion_plantel spl_i on (spl_i.grado_id = g_i.id)"
                          . "        WHERE spl_i.id = :seccion_plantel_id"
                          . "        )"
                          . " AND me.id not in (select m_ie.estudiante_id from matricula.inscripcion_estudiante m_ie "
                          . " inner join gplantel.seccion_plantel_periodo  spp_i on (spp_i.id = m_ie.seccion_plantel_periodo_id) "
                          . " where spp_i.periodo_id = :periodo_actual_id AND m_ie.estatus = 'A')" */
                        . " ORDER BY documento_identidad ASC, cedula_escolar ASC";
//                echo '<br>' . $sql;
//                echo '<br>' . $periodo_anterior_id;
//                echo '<br>' . $periodo_actual['id'];
//                echo '<br>' . $plantel_id;
//                echo '<br>' . $seccion_plantel_id;
                    /* echo "<pre>";
                      echo $sql;
                      echo "</pre>";
                      var_dump($seccion_plantel_id,$periodo_anterior_id,$periodo_actual['id'],$plantel_id);
                      die(); */
                    $busqueda = Yii::app()->db->createCommand($sql);
                    $busqueda->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
                }
                $busqueda->bindParam(":periodo_anterior_id", $periodo_anterior_id, PDO::PARAM_INT);
                $busqueda->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
                $busqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);

                $result = $busqueda->queryAll();
            } else {
                $periodo_anterior_id = 14;
                $sql = "SELECT
                    me.id, upper(me.nombres) || ' ' || upper(me.apellidos) as nom_completo, me.cedula_escolar, me.fecha_nacimiento , me.documento_identidad,me.tdocumento_identidad, 'no' as nuevoRegistro
                    FROM matricula.estudiante me
                    INNER JOIN matricula.inscripcion_estudiante ie ON (ie.plantel_id=me.plantel_actual_id AND ie.estudiante_id=me.id)
                    INNER JOIN gplantel.plantel p ON (ie.plantel_id=p.id)
                    INNER JOIN gplantel.grado g ON (ie.grado_id=g.id AND ie.grado_id=me.grado_actual_id)
                    WHERE
                    ie.plantel_id = :plantel_id AND ie.periodo_id=:periodo_anterior_id
                    AND g.consecutivo =
                    (SELECT (g_i.consecutivo-1) FROM gplantel.grado g_i INNER JOIN gplantel.seccion_plantel spl_i ON (spl_i.grado_id = g_i.id)
                    WHERE spl_i.id = :seccion_plantel_id
                    )
                    AND  NOT EXISTS (SELECT m_ie.estudiante_id FROM matricula.inscripcion_estudiante m_ie
                    INNER JOIN gplantel.seccion_plantel_periodo  spp_i on (spp_i.id = m_ie.seccion_plantel_periodo_id)
                    where spp_i.periodo_id = :periodo_actual_id AND me.id=m_ie.estudiante_id LIMIT 9223372036854775807
                    )
                    ORDER BY me.documento_identidad ASC, me.cedula_escolar ASC;
                    ";


                $busqueda = Yii::app()->db->createCommand($sql);
                $busqueda->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
                $busqueda->bindParam(":periodo_anterior_id", $periodo_anterior_id, PDO::PARAM_INT);
                $busqueda->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
                $busqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
                $result = $busqueda->queryAll();
            }
        }
        if ($result != null AND $result != array()) {
            Yii::app()->cache->set($indice, $result, 86400);
        }

        return $result;
    }

    public function obtenerDatosEstudiantes($estudiantes, $nuevoRegistro) {
        $estudiantes_imploded = implode(',', $estudiantes);
        if ($nuevoRegistro == 'si') {
            $sql = "SELECT e.id, upper(e.nombres) || ' ' || upper(e.apellidos) as nom_completo, e.cedula_escolar, e.documento_identidad as cedula_estudiante, e.fecha_nacimiento, 'si' as nuevoRegistro"
                . " FROM matricula.estudiante e"
                . " WHERE e.id IN ($estudiantes_imploded)"
                . " ORDER BY e.documento_identidad ASC";
        } else {
            $sql = "SELECT e.id, upper(e.nombres) || ' ' || upper(e.apellidos) as nom_completo, e.cedula_escolar, e.documento_identidad as cedula_estudiante, e.fecha_nacimiento, 'no' as nuevoRegistro"
                . " FROM matricula.estudiante e"
                . " WHERE e.id IN ($estudiantes_imploded)"
                . " ORDER BY e.documento_identidad ASC";
        }
        $busqueda = Yii::app()->db->createCommand($sql);

        $resultadoEstudiantes = $busqueda->queryAll();

        return $resultadoEstudiantes;
    }

    public function actualizarDatosEstudiantes($model) {

        $plantel_actual_id = (int) $model->plantel_actual_id;
        $fecha = $model->fecha_act;
        $usuario_act_id = (int) $model->usuario_act_id;
        $estatus = 1;

        $sql = "UPDATE matricula.estudiante e"
            . " SET"
            . " plantel_anterior_id = e.plantel_actual_id,"
            . " plantel_actual_id = :plantel_actual_id,"
            . " estatus = :estatus,"
            . " usuario_act_id = :usuario_act_id,"
            . " fecha_act = :fecha_act";

        $actualizacion = Yii::app()->db->createCommand($sql);
        $actualizacion->bindParam(":plantel_actual_id", $plantel_actual_id, PDO::PARAM_INT);
        $actualizacion->bindParam(":estatus", $estatus, PDO::PARAM_INT);
        $actualizacion->bindParam(":usuario_act_id", $usuario_act_id, PDO::PARAM_INT);
        $actualizacion->bindParam(":fecha_act", $fecha, PDO::PARAM_STR);
        $resultadoEstudiantes = $actualizacion->execute();
        return $resultadoEstudiantes;
    }

    public function calcularEdad($fecha_nacimiento) {
        list($ano, $mes, $dia) = explode("-", $fecha_nacimiento);
        $ano_diferencia = date("Y") - $ano;
        $mes_diferencia = date("m") - $mes;
        $dia_diferencia = date("d") - $dia;
        if ($dia_diferencia > 0 && $mes_diferencia < 0) {
            $ano_diferencia = $ano_diferencia - 1;
        }
        /* ASI ESTABA ANTES */
//        if (date("m") == $mes && date("d") < $dia) {
//            $ano_diferencia = $ano_diferencia - 1;
//        }
        /* MODIFICADO POR ENRIQUEX */
        if (date("m") <= $mes AND $ano_diferencia > 0) {
            $ano_diferencia = $ano_diferencia - 1;
        }

        $edadCalculada = $ano_diferencia;
        if (date("m") == $mes AND date("d") <= $dia) {
            $edadCalculada++;
        }
        return $edadCalculada;
    }

    /* -------------------------------------------JEAN--------------------------------------------------- */

    public function buscarEstudiante($cedula_escolar = '', $documento_identidad = '', $nombres = '', $apellidos = '', $cirepresentante = '', $busquedaCompleta, $seccion_plantel_id, $plantel_id, $tDocumentoIdentidad = '', $periodo_id_nuevo = 14) {
        $where = '';
        $whereinscrito = '';
        if ($cedula_escolar !== '') {
            $where = "me.cedula_escolar='$cedula_escolar' ";
            $whereinscrito = "e.cedula_escolar='$cedula_escolar' ";
        }

        if ($documento_identidad !== '' AND $tDocumentoIdentidad !== '') {
            if ($where !== '') {
                $where .= " and me.documento_identidad='$documento_identidad' ";
                $where .= " and me.tdocumento_identidad='$tDocumentoIdentidad' ";

                $whereinscrito .= "and e.documento_identidad='$documento_identidad' ";
                $whereinscrito .= " and e.tdocumento_identidad='$tDocumentoIdentidad' ";
            } else {
                $where = " me.documento_identidad='$documento_identidad' ";
                $where .= " and me.tdocumento_identidad='$tDocumentoIdentidad' ";

                $whereinscrito = "e.documento_identidad='$documento_identidad' ";
                $whereinscrito .= " and e.tdocumento_identidad='$tDocumentoIdentidad' ";
            }
        }


        if ($nombres !== '' && $apellidos !== '') {
            $nombres = trim($nombres);
            $apellidos = trim($apellidos);
            $apellido1 = '';
            $apellido2 = '';
            $apellido3 = '';
            $nombre1 = '';
            $nombre2 = '';
            $nombre3 = '';

// list($apellido1, $apellido2, $apellido3) = explode(" ", $apellidos);
            $apellidos_exploded = explode(" ", $apellidos);
            $apellido1 = isset($apellidos_exploded[0]) ? $apellidos_exploded[0] : '';
            $apellido2 = isset($apellidos_exploded[1]) ? $apellidos_exploded[1] : '';
            $apellido3 = isset($apellidos_exploded[2]) ? $apellidos_exploded[2] : '';
// list($nombre1, $nombre2, $nombre3) = explode(" ", $nombres);
            $nombres_exploded = explode(" ", $nombres);
            $nombre1 = isset($nombres_exploded[0]) ? $nombres_exploded[0] : '';
            $nombre2 = isset($nombres_exploded[1]) ? $nombres_exploded[1] : '';
            $nombre3 = isset($nombres_exploded[2]) ? $nombres_exploded[2] : '';
            if ($where !== '') {

                $where .= " and busqueda @@ plainto_tsquery('pg_catalog.spanish', '" . "$nombre1 " . " $nombre2" . " $nombre3" . " $apellido1" . " $apellido2" . " $apellido3" . "')";
//                $where .= ' and busqueda @@ TO_tsquery(' . "'" . '"pg_catalog.spanish"' . "," . '"' . $nombre1 . '" & "' . $nombre2 . '" & "' . $nombre3 . '" & "' . $apellido1 . '" & "' . $apellido2 . '" & "' . $apellido3 . '"' . "'" . ')';
            } else {


                $where .= " busqueda @@ plainto_tsquery('pg_catalog.spanish', '" . "$nombre1 " . " $nombre2" . " $nombre3" . " $apellido1" . " $apellido2" . " $apellido3" . "')";
//                $where .= ' busqueda @@ TO_tsquery(' . "'" . '"pg_catalog.spanish"' . "," . '"' . $nombre1 . '" & "' . $nombre2 . '" & "' . $nombre3 . '" & "' . $apellido1 . '" & "' . $apellido2 . '" & "' . $apellido3 . '"' . "'" . ')';
//    $where = " me.nombres like '$nombres%'";
            }
        }
//pg_catalog.spanish

        if ($cirepresentante !== '') {
            if ($where !== '') {
                $where .= " and mr.documento_identidad = $cirepresentante::text";
            } else {
                $where = " mr.documento_identidad = $cirepresentante::text";
            }
        }

        $gradosPrescolar [] = 3;
        $gradosPrescolar [] = 4;
        $gradosPrescolar [] = 7;
        $gradosPrescolar [] = 8;
        $gradosPrescolar [] = 11;
        $gradosPrescolar [] = 12;
        $nombres = strtoupper($nombres);
        $apellidos = strtoupper($apellidos);
        if ($periodo_id_nuevo != 14) {
            $periodo_Escolar_id = $periodo_id_nuevo;
            $periodo_Escolar_anterior_id = $periodo_Escolar_id - 1;
        } else {
            $periodo_Escolar = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_Escolar_id = $periodo_Escolar['id'];
            $periodo_Escolar_anterior_id = $periodo_Escolar_id - 1;
        }

//$datosSeccion = SeccionPlantel::model()->obtenerDatosSeccion($seccion_plantel_id, $plantel_id);
        $grado = SeccionPlantel::model()->obtenerGrado($seccion_plantel_id);
        /* $periodo_Escolar_id = null;
          $periodo_Escolar_id = 15;
          var_dump($whereinscrito);
          echo "<br>";
          echo "<br>";
          var_dump($where . ' ******** WHERE');
          echo "<br>"; */


        if ($whereinscrito) {
            /* este query lo hago para saber si el estudiante ya se encuentra inscrito en algun lugar $whereinscrito Y LO HACE USANDO LA CEDULA ESCOLAR O LA CEDULA DE IDENTIDAD */
            $sql = "select e.nombres as nombre_estudiante, e.apellidos as apellido_estudiante,p.nombre as nombre_plantel,p.cod_estadistico as codigo_estadistico,
                s.nombre as nombre_seccion, g.nombre as nombre_grado
                from matricula.estudiante e
                inner join matricula.inscripcion_estudiante ie on ie.estudiante_id=e.id
                inner join gplantel.plantel p on p.id=e.plantel_actual_id
                inner join gplantel.grado g on g.id=e.grado_actual_id
                inner join gplantel.seccion_plantel_periodo spp on spp.id = ie.seccion_plantel_periodo_id
                inner join gplantel.seccion_plantel sp on sp.id =spp.seccion_plantel_id
                inner join gplantel.seccion s on sp.seccion_id = s.id
                where $whereinscrito
                and spp.periodo_id=$periodo_id_nuevo
                ";


            $guard = Yii::app()->db->createCommand($sql);
            $buscarEstudiante = $guard->queryAll();

            if ($buscarEstudiante) {
                $verificacion = 1;

                return array($buscarEstudiante, $verificacion);
            } else {
                $sql = "select sp.nivel_id
                        from gplantel.seccion_plantel sp
                        where sp.id=$seccion_plantel_id AND ( sp.nivel_id=1 OR  sp.nivel_id=2) ";

                $guard = Yii::app()->db->createCommand($sql);
                $buscarEstudiante = $guard->queryAll();
                if (!$buscarEstudiante) {

                    /* aqui entra todos aquellos estudiantes que no esten inscripto en algun lado y que donde lo esten inscribiendo no sea una seccion de nivel educacion primaria o educacion inicial */
                    /* echo "hola2"; */

                    if ($periodo_Escolar_id != NULL) {
                        if ($busquedaCompleta == 0) {
                            if ($periodo_Escolar_id == 14) {

                                if (in_array($grado, $gradosPrescolar)) {
                                    $sql = "SELECT "
                                        . " DISTINCT me.cedula_escolar, me.documento_identidad,me.tdocumento_identidad, me.nombres, me.apellidos,
                                                me.id, p.nombre as plantel_nombre, p.cod_plantel, ih.grado_id"
                                        /*  . " me.id, upper(me.nombres) || ' ' || upper(me.apellidos) as nom_completo, me.cedula_escolar, me.fecha_nacimiento, mr.cedula_identidad, 'no' as nuevoRegistro" */
                                        . " FROM matricula.estudiante me "
                                        . " LEFT JOIN matricula.representante mr on me.representante_id = mr.id "
                                        . " INNER JOIN gplantel.plantel p on me.plantel_actual_id = p.id "
                                        . " INNER JOIN matricula.inscripcion_historico ih on me.sigue_id = ih.s_alumno_id "
                                        . " WHERE $where"
                                        . " AND ih.fperiodoescolar_id = $periodo_Escolar_anterior_id "
                                        . " AND me.plantel_actual_id = $plantel_id "
                                        . " AND ih.grado_id = 0 "
                                        . " AND me.id not in (select m_ie.estudiante_id from matricula.inscripcion_estudiante m_ie "
                                        . " inner join gplantel.seccion_plantel_periodo spp_i on (spp_i.id = m_ie.seccion_plantel_periodo_id) "
                                        . " where spp_i.periodo_id = $periodo_Escolar_id)"
                                        . " ORDER BY me.documento_identidad ASC, me.tdocumento_identidad ASC ";
                                    $guard = Yii::app()->db->createCommand($sql);

                                    $buscarEstudiante = $guard->queryAll();
                                } else {
                                    /* este query se realiza cuando tiene peridod 14 y ingresa en la buscaqueda numero (cedula, cedula escolar) */
                                    $sql = " SELECT DISTINCT me.cedula_escolar, me.documento_identidad,me.tdocumento_identidad, me.nombres, me.apellidos,
                                            mr.nombres AS representante, me.representante_id,
                                            me.id, p.nombre as plantel_nombre, p.cod_plantel, ih.grado_id
                                            FROM matricula.estudiante me
                                            LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)
                                            INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id
                                            INNER JOIN gplantel.plantel p on ih.plantel_id=p.id
                                            INNER JOIN gplantel.grado g ON (ih.grado_id = g.id)
                                            WHERE $where
                                            AND ih.fperiodoescolar_id = $periodo_Escolar_anterior_id
                                            AND me.id not in (
                                            SELECT m_ie.estudiante_id
                                            FROM matricula.inscripcion_estudiante m_ie
                                            INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                                            WHERE spp_i.periodo_id = $periodo_Escolar_id)
                                            AND g.consecutivo = (SELECT DISTINCT (g_i.consecutivo-1)as consecutivo
                                            FROM gplantel.grado g_i
                                            INNER JOIN gplantel.seccion_plantel spl_i ON (spl_i.grado_id = g_i.id)
                                            WHERE spl_i.id = $seccion_plantel_id)
                                            UNION

                                            SELECT DISTINCT me.cedula_escolar, me.documento_identidad,me.tdocumento_identidad, me.nombres, me.apellidos, mr.nombres AS representante,
                                            me.representante_id, me.id, p.nombre as plantel_nombre, p.cod_plantel, ih.grado_id
                                            FROM matricula.estudiante me
                                            LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)
                                            INNER JOIN matricula.inscripcion_historico ih ON (me.sigue_id = ih.s_alumno_id)
                                            INNER JOIN gplantel.plantel p ON (ih.plantel_id = p.id)
                                            INNER JOIN gplantel.grado g ON (ih.grado_id = g.id)
                                            WHERE $where
                                            AND ih.fperiodoescolar_id = $periodo_Escolar_anterior_id
                                            AND ih.repitiente = 1
                                            AND
                                            me.id not in ( SELECT m_ie.estudiante_id FROM matricula.inscripcion_estudiante m_ie
                                            INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                                            WHERE spp_i.periodo_id = $periodo_Escolar_id)
                                            AND g.consecutivo = (SELECT DISTINCT (g_i.consecutivo)as consecutivo
                                            FROM gplantel.grado g_i INNER JOIN gplantel.seccion_plantel spl_i ON (spl_i.grado_id = g_i.id) WHERE spl_i.id = $seccion_plantel_id)
                                            ORDER BY documento_identidad ASC, tdocumento_identidad ASC ";


                                    $guard = Yii::app()->db->createCommand($sql);
                                    $buscarEstudiante = $guard->queryAll();
                                    if (!$buscarEstudiante) {
                                        /* este query se ejecuta cuando esta buscando un estudiante que es inscripto pero no cumple con todo los requisito para aparecer en el anterior query , aun  asi muestra los datos que se consiguio de ese estudiante */


                                        if ($cedula_escolar !== '') {

                                            $whereBuscarEstudiante = "taa.s_cedula_escolar='$cedula_escolar' ";
                                        }

                                        if ($documento_identidad !== '') {
                                            $whereBuscarEstudiante = "taa.s_cedula_identidad='$documento_identidad' ";
                                        }

                                        $sql = "select e.nombres as nombre_estudiante, e.apellidos as apellido_estudiante,p.nombre as nombre_plantel,p.cod_estadistico as codigo_estadistico,
                                                taa.cseccion as nombre_seccion, taa.s_grado_nombre as nombre_grado
                                                from matricula.estudiante e
                                                inner join legacy.talumno_acad taa on taa.s_alumno_id=e.sigue_id
                                                left join gplantel.plantel p on p.id=taa.s_plantel_id
                                                where $whereBuscarEstudiante
                                                order by nombre_grado DESC
                                                limit 1
                                                ";

                                        $guard = Yii::app()->db->createCommand($sql);
                                        $buscarEstudiante = $guard->queryAll();
                                        $verificacion = 2;
                                        return array($buscarEstudiante, $verificacion);
                                    }
                                }
                            } else {

                                /* BUSQUEDA MAYOR DEL 2014 CEDULA O CEDULA ESCOLAR */
                                $sql = " SELECT  DISTINCT me.cedula_escolar, me.documento_identidad, me.nombres, me.apellidos,
                                            me.id, p.nombre as plantel_nombre, p.cod_plantel
                                            FROM matricula.estudiante me
                                            LEFT JOIN matricula.representante mr ON (me.representante_id=mr.id)
                                            --INNER JOIN matricula.inscripcion_historico ih ON (me.sigue_id = ih.s_alumno_id)
                                            INNER JOIN gplantel.plantel p ON (me.plantel_actual_id = p.id)
                                            INNER JOIN matricula.inscripcion_estudiante ie on (ie.estudiante_id=me.id)
                                            INNER JOIN gplantel.seccion_plantel_periodo spp ON (ie.seccion_plantel_periodo_id=spp.id)
                                            INNER JOIN gplantel.grado g ON (me.grado_actual_id=g.id)
                                            WHERE $where
                                                                        AND g.consecutivo =
                                            (SELECT DISTINCT (g_i.consecutivo-1)as consecutivo
                                            FROM gplantel.grado g_i
                                            INNER JOIN  gplantel.seccion_plantel spl_i ON (spl_i.grado_id = g_i.id)
                                            WHERE spl_i.id = $seccion_plantel_id
                                            )
                                            AND spp.periodo_id < $periodo_Escolar_id
                                            ";
                                /* UNION

                                  SELECT DISTINCT me.cedula_escolar, me.documento_identidad, me.nombres, me.apellidos, me.id, p.nombre as plantel_nombre,
                                  p.cod_plantel
                                  FROM matricula.estudiante me
                                  LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)
                                  INNER JOIN matricula.inscripcion_historico ih ON (me.sigue_id = ih.s_alumno_id)
                                  INNER JOIN gplantel.plantel p ON (ih.plantel_id = p.id)
                                  INNER JOIN gplantel.grado g ON (ih.grado_id = g.id)
                                  WHERE $where
                                  AND ih.repitiente = 1
                                  AND g.consecutivo = (SELECT DISTINCT (g_i.consecutivo-1)as consecutivo
                                  FROM gplantel.grado g_i
                                  INNER JOIN gplantel.seccion_plantel spl_i ON (spl_i.grado_id = g_i.id)
                                  WHERE spl_i.id = $seccion_plantel_id )
                                  AND spp.periodo_id < $periodo_Escolar_id */


                                $guard = Yii::app()->db->createCommand($sql);

                                $buscarEstudiante = $guard->queryAll();
                            }

                            if ($buscarEstudiante == array()) {

                                $verificacion = 0;

                                return array(FALSE, $verificacion); // Retorna false cuando la busqueda no es encontrada
                            } else {
                                $verificacion = 0;

                                return array($buscarEstudiante, $verificacion);  /* Retorna un array con el resultado de la busqueda */
                            }
                        }
                        /* A partir de este momento se realiza la buscada completa */ else {


                            if (in_array($grado, $gradosPrescolar)) {
                                $sql = "SELECT "
                                    . " DISTINCT me.cedula_escolar, me.documento_identidad, me.nombres, me.apellidos,
                                            me.id, p.nombre as plantel_nombre, p.cod_plantel, ih.grado_id"
                                    /*  . " me.id, upper(me.nombres) || ' ' || upper(me.apellidos) as nom_completo, me.cedula_escolar, me.fecha_nacimiento , mr.cedula_identidad, 'no' as nuevoRegistro" */
                                    . " FROM matricula.estudiante me "
                                    . " LEFT JOIN matricula.representante mr on me.representante_id=mr.id "
                                    . " INNER JOIN gplantel.plantel p on me.plantel_actual_id=p.id "
                                    . " INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id "
                                    . " WHERE $where"
                                    . " AND ih.fperiodoescolar_id=$periodo_Escolar_anterior_id "
                                    . " AND me.plantel_actual_id = $plantel_id "
                                    . " AND ih.grado_id = 0 "
                                    . " AND me.id not in (select m_ie.estudiante_id from matricula.inscripcion_estudiante m_ie "
                                    . " inner join gplantel.seccion_plantel_periodo  spp_i on (spp_i.id = m_ie.seccion_plantel_periodo_id) "
                                    . " where spp_i.periodo_id = $periodo_Escolar_id) ";

                                $guard = Yii::app()->db->createCommand($sql);
                                $buscarEstudiante = $guard->queryAll();
                            } else {

                                $sql = "SELECT  DISTINCT me.cedula_escolar, me.documento_identidad, me.nombres, me.apellidos,
                                        me.id, p.nombre AS plantel_nombre, p.cod_plantel
                                        FROM matricula.estudiante me
                                        LEFT JOIN matricula.representante mr ON (me.representante_id=mr.id)
                                        LEFT JOIN gplantel.plantel p ON (me.plantel_actual_id=p.id)
                                        LEFT JOIN legacy.talumno_acad taa ON (me.sigue_id=taa.s_alumno_id)
                                        INNER JOIN gplantel.grado g ON (me.grado_actual_id=g.id)
                                        WHERE $where
                                        AND taa.fperiodoescolar <> '$periodo_Escolar_id'
                                        AND me.id NOT IN (
                                        SELECT m_ie.estudiante_id
                                        FROM matricula.inscripcion_estudiante m_ie
                                        INNER JOIN gplantel.seccion_plantel_periodo  spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                                        WHERE spp_i.periodo_id=$periodo_Escolar_id)
                                        AND g.consecutivo =
                                        (SELECT DISTINCT (g_i.consecutivo-1)as consecutivo
                                        FROM gplantel.grado g_i
                                        INNER JOIN  gplantel.seccion_plantel spl_i ON (spl_i.grado_id = g_i.id)
                                        WHERE spl_i.id = $seccion_plantel_id
                                        ) " . " UNION

                                        SELECT  DISTINCT me.cedula_escolar, me.documento_identidad, me.nombres, me.apellidos,
                                        me.id, p.nombre AS plantel_nombre, p.cod_plantel
                                        FROM matricula.estudiante me
                                        LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)

                                        INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id
                                        INNER JOIN gplantel.plantel p on ih.plantel_id=p.id
                                        INNER JOIN gplantel.grado g on (ih.grado_id = g.id)
                                        WHERE $where
                                        AND ih.repitiente = 1
                                        AND ih.fperiodoescolar_id = $periodo_Escolar_anterior_id AND me.id not in
                                        ( SELECT m_ie.estudiante_id FROM matricula.inscripcion_estudiante m_ie
                                        INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                                        WHERE spp_i.periodo_id = $periodo_Escolar_id)
                                        AND g.consecutivo =
                                        (SELECT DISTINCT (g_i.consecutivo)as consecutivo
                                        FROM gplantel.grado g_i INNER JOIN gplantel.seccion_plantel spl_i ON
                                        (spl_i.grado_id = g_i.id) WHERE spl_i.id = $seccion_plantel_id)";
                                $guard = Yii::app()->db->createCommand($sql);
                                $buscarEstudiante = $guard->queryAll();
                            }

                            if ($buscarEstudiante == array()) {
                                $verificacion = 0;

                                return array(false, $verificacion);
                                /* Retorna false cuando la busqueda no es encontrada */
                            } else {
                                $verificacion = 0;

                                return array($buscarEstudiante, $verificacion);
                                /* Retorna un array con el resultado de la busqueda */
                            }
                        }
                    } else {
                        return '1'; // Retorna 1 si la variable sesión esta resultando nulo
                    }
                } else {//aqui entra todos aquellos estudiantes que no esten inscripto en algun lado y que donde lo esten inscribiendo si sea una seccion de nivel educacion primaria o educacion inicial
                    $completabusqueda = $this->buscarEstudianteInicialPrimaria($where, $periodo_Escolar_anterior_id, $periodo_Escolar_id, $seccion_plantel_id, $busquedaCompleta, $periodo_id_nuevo);
                    $verificacion = 0;
                    return array($completabusqueda, $verificacion);
                }
            }
        } else {

            /* POR AQUI SE PASAS CUANDO HACES BUSQUEDA POR NOMBRE Y APELLIDO */
            $sql = "select sp.nivel_id
                    from gplantel.seccion_plantel sp
                    where sp.id=$seccion_plantel_id AND (sp.nivel_id=1 OR  sp.nivel_id=2)";

            $guard = Yii::app()->db->createCommand($sql);
            $buscarEstudiante = $guard->queryAll();



            if (!$buscarEstudiante) {
                if ($periodo_Escolar_id != NULL) {
                    if ($busquedaCompleta == 0) {
                        if ($periodo_Escolar_id == 14) {

                            if (in_array($grado, $gradosPrescolar)) {
                                $sql = "SELECT "
                                    . " DISTINCT me.cedula_escolar, me.documento_identidad, me.nombres, me.apellidos,
                                            me.id, p.nombre as plantel_nombre, p.cod_plantel, ih.grado_id"
                                    /*  . " me.id, upper(me.nombres) || ' ' || upper(me.apellidos) as nom_completo, me.cedula_escolar, me.fecha_nacimiento, mr.cedula_identidad, 'no' as nuevoRegistro" */
                                    . " FROM matricula.estudiante me "
                                    . " LEFT JOIN matricula.representante mr on me.representante_id = mr.id "
                                    . " INNER JOIN gplantel.plantel p on me.plantel_actual_id = p.id "
                                    . " INNER JOIN matricula.inscripcion_historico ih on me.sigue_id = ih.s_alumno_id "
                                    . " WHERE $where"
                                    . " AND ih.fperiodoescolar_id = $periodo_Escolar_anterior_id "
                                    . " AND me.plantel_actual_id = $plantel_id "
                                    . " AND ih.grado_id = 0 "
                                    . " AND me.id not in (select m_ie.estudiante_id from matricula.inscripcion_estudiante m_ie "
                                    . " inner join gplantel.seccion_plantel_periodo spp_i on (spp_i.id = m_ie.seccion_plantel_periodo_id) "
                                    . " where spp_i.periodo_id = $periodo_Escolar_id) ";
                                $guard = Yii::app()->db->createCommand($sql);

                                $buscarEstudiante = $guard->queryAll();
                            } else {//este query se realiza cuando colocas nombre y apellido para una periodo 14
                                $sql = "SELECT DISTINCT me.cedula_escolar, me.documento_identidad, me.nombres, me.apellidos,
                                        mr.nombres AS representante, me.representante_id,
                                        me.id, p.nombre as plantel_nombre, p.cod_plantel, ih.grado_id
                                        FROM matricula.estudiante me
                                        LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)
                                        INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id
                                        INNER JOIN gplantel.plantel p on ih.plantel_id=p.id
                                        INNER JOIN gplantel.grado g ON (ih.grado_id = g.id)
                                        WHERE $where
                                        AND ih.fperiodoescolar_id = $periodo_Escolar_anterior_id
                                        AND me.id not in (
                                        SELECT m_ie.estudiante_id
                                        FROM matricula.inscripcion_estudiante m_ie
                                        INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                                        WHERE spp_i.periodo_id = $periodo_Escolar_id)
                                        AND g.consecutivo = (SELECT DISTINCT (g_i.consecutivo-1)as consecutivo
                                        FROM gplantel.grado g_i
                                        INNER JOIN gplantel.seccion_plantel spl_i ON (spl_i.grado_id = g_i.id)
                                        WHERE spl_i.id = $seccion_plantel_id) "
                                    . "UNION
                                        SELECT DISTINCT me.cedula_escolar, me.documento_identidad, me.nombres, me.apellidos,
                                        mr.nombres AS representante, me.representante_id,
                                        me.id, p.nombre as plantel_nombre, p.cod_plantel, ih.grado_id
                                        FROM matricula.estudiante me
                                        LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)

                                        INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id
                                        INNER JOIN gplantel.plantel p on ih.plantel_id=p.id
                                        INNER JOIN gplantel.grado g on (ih.grado_id = g.id)
                                        WHERE $where
                                        AND ih.repitiente = 1
                                        AND ih.fperiodoescolar_id = $periodo_Escolar_anterior_id AND me.id not in
                                        ( SELECT m_ie.estudiante_id FROM matricula.inscripcion_estudiante m_ie
                                        INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                                        WHERE spp_i.periodo_id = $periodo_Escolar_id)
                                        AND g.consecutivo =
                                        (SELECT DISTINCT (g_i.consecutivo)as consecutivo
                                        FROM gplantel.grado g_i INNER JOIN gplantel.seccion_plantel spl_i ON
                                        (spl_i.grado_id = g_i.id) WHERE spl_i.id = $seccion_plantel_id)";

                                /* $sql = "SELECT DISTINCT me.cedula_escolar, me.cedula_identidad, me.nombres, me.apellidos,
                                  me.id, p.nombre as plantel_nombre, p.cod_plantel
                                  FROM matricula.estudiante me
                                  LEFT JOIN matricula.representante mr ON (me.representante_id=mr.id)
                                  INNER JOIN gplantel.plantel p ON (me.plantel_actual_id=p.id)
                                  INNER JOIN matricula.inscripcion_historico ih ON (me.sigue_id=ih.s_alumno_id)
                                  WHERE $where
                                  AND ih.fperiodoescolar_id=$periodo_Escolar_anterior_id
                                  AND me.id not in (
                                  SELECT m_ie.estudiante_id
                                  FROM matricula.inscripcion_estudiante m_ie
                                  INNER JOIN gplantel.seccion_plantel_periodo  spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                                  WHERE spp_i.periodo_id < $periodo_Escolar_id) "; */

                                $guard = Yii::app()->db->createCommand($sql);
                                $buscarEstudiante = $guard->queryAll();
                            }
                        } else {

                            $sql = " SELECT   me.cedula_escolar, me.documento_identidad, me.nombres, me.apellidos,
                                    me.id, p.nombre as plantel_nombre, p.cod_plantel
                                    FROM matricula.estudiante me
                                    LEFT JOIN matricula.representante mr ON (me.representante_id=mr.id)
                                    INNER JOIN gplantel.plantel p ON (me.plantel_actual_id=p.id)
                                     INNER JOIN gplantel.grado g ON (me.grado_actual_id=g.id)
                                    INNER JOIN matricula.inscripcion_estudiante ie ON (ie.estudiante_id=me.id AND ie.grado_id=me.grado_actual_id)
                                    INNER JOIN gplantel.seccion_plantel_periodo spp ON (ie.seccion_plantel_periodo_id=spp.id)

                                    WHERE $where
                                    AND g.consecutivo =
                                    (SELECT DISTINCT (g_i.consecutivo-1)as consecutivo
                                    FROM gplantel.grado g_i
                                    INNER JOIN  gplantel.seccion_plantel spl_i ON (spl_i.grado_id = g_i.id)
                                    WHERE spl_i.id = $seccion_plantel_id)
                                    AND spp.periodo_id < $periodo_Escolar_id";


                            $guard = Yii::app()->db->createCommand($sql);
                            $buscarEstudiante = $guard->queryAll();
                        }

                        if ($buscarEstudiante == array()) {

                            $verificacion = 0;

                            return array(false, $verificacion);
                            return false;  //Retorna false cuando la busqueda no es encontrada
                        } else {
                            $verificacion = 0;

                            return array($buscarEstudiante, $verificacion);
// Retorna un array con el resultado de la busqueda
                        }
                    } else {
                        if (in_array($grado, $gradosPrescolar)) {
                            $sql = "SELECT "
                                . " DISTINCT me.cedula_escolar, me.documento_identidad, me.nombres, me.apellidos, me.id, p.nombre as plantel_nombre, p.cod_plantel, ih.grado_id"
                                /*  . " me.id, upper(me.nombres) || ' ' || upper(me.apellidos) as nom_completo, me.cedula_escolar, me.fecha_nacimiento , mr.cedula_identidad, 'no' as nuevoRegistro" */
                                . " FROM matricula.estudiante me "
                                . " LEFT JOIN matricula.representante mr on me.representante_id=mr.id "
                                . " INNER JOIN gplantel.plantel p on me.plantel_actual_id=p.id "
                                . " INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id "
                                . " WHERE $where"
                                . " AND ih.fperiodoescolar_id=$periodo_Escolar_anterior_id "
                                . " AND me.plantel_actual_id = $plantel_id "
                                . " AND ih.grado_id = 0 "
                                . " AND me.id not in (select m_ie.estudiante_id from matricula.inscripcion_estudiante m_ie "
                                . " inner join gplantel.seccion_plantel_periodo  spp_i on (spp_i.id = m_ie.seccion_plantel_periodo_id) "
                                . " where spp_i.periodo_id = $periodo_Escolar_id) ";

                            $guard = Yii::app()->db->createCommand($sql);
                            $buscarEstudiante = $guard->queryAll();
                        } else {

                            $sql = "SELECT  DISTINCT me.cedula_escolar, me.documento_identidad, me.nombres, me.apellidos,
                                me.id, p.nombre AS plantel_nombre, p.cod_plantel
                                FROM matricula.estudiante me
                                LEFT JOIN matricula.representante mr ON (me.representante_id=mr.id)
                                LEFT JOIN gplantel.plantel p ON (me.plantel_actual_id=p.id)
                                LEFT JOIN legacy.talumno_acad taa ON (me.sigue_id=taa.s_alumno_id)
                                INNER JOIN gplantel.grado g ON (me.grado_actual_id=g.id)
                                WHERE $where
                                AND taa.fperiodoescolar <> '$periodo_Escolar_id'
                                AND me.id NOT IN (
                                SELECT m_ie.estudiante_id
                                FROM matricula.inscripcion_estudiante m_ie
                                INNER JOIN gplantel.seccion_plantel_periodo  spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                                WHERE spp_i.periodo_id=$periodo_Escolar_id)
                                AND g.consecutivo =
                                (SELECT DISTINCT (g_i.consecutivo-1)as consecutivo
                                FROM gplantel.grado g_i
                                INNER JOIN  gplantel.seccion_plantel spl_i ON (spl_i.grado_id = g_i.id)
                                WHERE spl_i.id = $seccion_plantel_id
                                ) " . "UNION

                                SELECT  DISTINCT me.cedula_escolar, me.documento_identidad, me.nombres, me.apellidos,
                                me.id, p.nombre AS plantel_nombre, p.cod_plantel
                                FROM matricula.estudiante me
                                LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)

                                INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id
                                INNER JOIN gplantel.plantel p on ih.plantel_id=p.id
                                INNER JOIN gplantel.grado g on (ih.grado_id = g.id)
                                WHERE $where
                                AND ih.repitiente = 1
                                AND ih.fperiodoescolar_id = $periodo_Escolar_anterior_id AND me.id not in
                                ( SELECT m_ie.estudiante_id FROM matricula.inscripcion_estudiante m_ie
                                INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                                WHERE spp_i.periodo_id = $periodo_Escolar_id)
                                AND g.consecutivo =
                                (SELECT DISTINCT (g_i.consecutivo)as consecutivo
                                FROM gplantel.grado g_i INNER JOIN gplantel.seccion_plantel spl_i ON
                                (spl_i.grado_id = g_i.id) WHERE spl_i.id = $seccion_plantel_id)";
                            $guard = Yii::app()->db->createCommand($sql);
                            $buscarEstudiante = $guard->queryAll();
                        }

                        if ($buscarEstudiante == array()) {
                            $verificacion = 0;

                            return array(false, $verificacion);
// Retorna false cuando la busqueda no es encontrada
                        } else {
                            $verificacion = 0;
                            return array($buscarEstudiante, $verificacion);
// Retorna un array con el resultado de la busqueda
                        }
                    }
                } else {
                    return '1'; // Retorna 1 si la variable sesión esta resultando nulo
                }
            } else {//aqui entra todos aquellos estudiantes que no esten inscripto en algun lado y que donde lo esten inscribiendo si sea una seccion de nivel educacion primaria o educacion inicial cuando buscas por nombre
                $completabusqueda = $this->buscarEstudianteInicialPrimaria($where, $periodo_Escolar_anterior_id, $periodo_Escolar_id, $seccion_plantel_id, $busquedaCompleta, $periodo_id_nuevo);
                $verificacion = 0;
                return array($completabusqueda, $verificacion);
            }
        }
    }

    public function buscarEstudiantePrueba($documento_identidad, $tdocumento_identidad, $cedula_escolar, $identificacion_repre, $nombres, $apellidos, $periodo_id, $seccion_plantel_id, $busqueda_completa, $grado_id, $individual, $probarInscritos) {

        list($busqueda_simple, $estudiantes, $parametros, $metodo, $linea) = $this->buscarEstudiantePorIdentificacion($documento_identidad, $tdocumento_identidad, $cedula_escolar, $identificacion_repre, $nombres, $apellidos, $periodo_id, $grado_id);
        $mensaje = '';
        $arregloEstudiantes = array();
        $existe = false;
        if ($busqueda_simple) {
            $estudiante = $estudiantes[0];
            $nombres = $estudiante['nombresestud'];
            $apellidos = $estudiante['apellidosestud'];
            $identificacionestud = $estudiante['identificacionestud'];
            $origenestud = $estudiante['origenestud'];
            $nombreplantel = $estudiante['nombreplantel'];
            $gradoestud = $estudiante['gradoestud'];
            $seccionestud = $estudiante['seccionestud'];
            $error = $estudiante['error'];

            if (isset($error) AND $error == 0) {
                if (isset($estudiante['existe_estudiante']) AND $estudiante['existe_estudiante'] == 0) {

                    /* MENSAJE NO EXISTE */
                    $mensaje = "El Estudiante no esta registrado en el Sistema";
                    return array($mensaje, $estudiante);
                } else {
                    if (isset($estudiante['existe_estudiante']) AND $estudiante['existe_estudiante'] == 1 AND isset($estudiante['inscritoperiodoactual']) AND $estudiante['inscritoperiodoactual'] == 0) {

                        /* VERIFICAR REQUISITOS */
                        if (count($probarInscritos) > 0) {
                            foreach ($probarInscritos as $value) {
                                $estudiant_id = $value['id'];

                                foreach ($estudiantes as $buscar) {
                                    $estudBusqueda = $buscar['idestud'];
                                    if ($estudiant_id == $estudBusqueda) {
                                        $existe = true;
                                        break;
                                    }
                                }
                            }
                            if ($existe) { // Valida que el estudiante que busco no sea igual al que esta atras, para matricular
                                $mensaje = 'Este estudiante ya se encuentra incluído por favor intente con otro estudiante';
                                return array($mensaje, null);
                            }
                        }

                        $identificacion = $estudiante['identificacionestud'];
                        $origen = $estudiante['origenestud'];
                        $cod_plantel = $estudiante['codplantelactual'];
                        $plantel = $estudiante['nombreplantelactual'];
                        $id = $estudiante['idestud'];
                        if ($individual) {
                            $botones = "<div class='center'>" . CHtml::link("", "", array("class" => "fa fa-plus green add-estud-individual", "data" => $id,
                                        //                   'onClick' => "Estudiante('')",
                                        "title" => "Agregar Estudiante")
                                ) .
                                "</div>";
                        } else {
                            $botones = "<div class='center'>" . CHtml::link("", "", array("class" => "fa fa-plus green add-estud", "data" => $id,
                                        //                   'onClick' => "Estudiante('')",
                                        "title" => "Agregar Estudiante")
                                ) .
                                "</div>";
                        }
                        $rawData[] = array(
                            'id' => 1,
                            'documento_identidad' => '<center>' . $this->getDocumentoIdentidad($origen, $identificacion) . '</center>',
                            'nombres' => '<center>' . $nombres . '</center>',
                            'apellidos' => '<center>' . $apellidos . '</center>',
                            'cod_plantel' => '<center>' . $cod_plantel . '</center>',
                            'nombrePlantel' => "<center><label title='$plantel'>" . $plantel . '</label></center>',
                            'boton' => '<center>' . $botones . '</center>'
                        );
                        $dataProvider = new CArrayDataProvider($rawData, array(
                                'pagination' => false,
                            )
                        );
                        return array(null, $dataProvider);
                    } else {
                        if (isset($estudiante['existe_estudiante']) AND $estudiante['existe_estudiante'] == 1 AND isset($estudiante['inscritoperiodoactual']) AND $estudiante['inscritoperiodoactual'] == 1) {


                            $mensaje = "El Estudiante <strong>$origenestud-$identificacionestud $nombres $apellidos</strong> está matriculado en el Plantel <strong>$nombreplantel</strong> en el <strong>$gradoestud '$seccionestud'</strong>";

                            return array($mensaje, $estudiante);
                        }
                    }
                }
            } else {
                if (isset($estudiante['error']) AND $estudiante['error'] == 1) {

                    /* MENSAJE ERROR DENTRO DEL STORED PROCEDURE */
                    $mensaje = "Estimado Usuario, ha ocurrido un error durante la búsqueda. Cierre la ventana e intente nuevamente.";
                    Utiles::enviarCorreo(Yii::app()->params['adminEmailSend'], 'ERROR EN SP EN EL MÉTODO ' . $metodo . ' EN LA LINEA ' . $linea, $parametros, Yii::app()->params['adminEmailSend'], Yii::app()->params['adminName']);
                    return array($mensaje, null);
                } else {

                    /* MENSAJE ERROR DENTRO DEL STORED PROCEDURE */
                    $mensaje = "Estimado Usuario, ha ocurrido un error durante la búsqueda. Cierre la ventana e intente nuevamente.";

                    return array($mensaje, null);
                }
            }
        }
    }

    public function buscarEstudiantes($documento_identidad, $tdocumento_identidad, $cedula_escolar, $identificacion_repre, $nombres, $apellidos, $periodo_id, $seccion_plantel_id, $grado_id, $individual) {

        list($busqueda_simple, $estudiantes, $parametros, $metodo, $linea) = $this->buscarEstudiantePorIdentificacion($documento_identidad, $tdocumento_identidad, $cedula_escolar, $identificacion_repre, $nombres, $apellidos, $periodo_id, $grado_id);
        $mensaje = '';
        $arregloEstudiantes = array();
        $existe = false;
        if ($busqueda_simple) {
            $estudiante = $estudiantes[0];
            $error = $estudiante['error'];
            $nombres = $estudiante['nombresEstud'];
            $apellidos = $estudiante['apellidosEstud'];
            $identificacionestud = $estudiante['identificacionEstud'];
            $origenestud = $estudiante['origenEstud'];
            $nombreplantel = $estudiante['nombrePlantel'];
            $gradoestud = $estudiante['gradoEstud'];
            $seccionestud = $estudiante['seccionEstud'];


            if (isset($error) AND $error == 0) {
                if (isset($estudiante['existeEstudiante']) AND $estudiante['existeEstudiante'] == 0) {
                    /* MENSAJE NO EXISTE */
                    $mensaje = "El Estudiante no esta registrado en el Sistema";
                    return array($mensaje, $busqueda_simple, $estudiante);
                } else {
                    if (isset($estudiante['existeEstudiante']) AND $estudiante['existeEstudiante'] == 1 AND isset($estudiante['inscritoPeriodoActual']) AND $estudiante['inscritoPeriodoActual'] == 0) {
                        return array(null, $busqueda_simple, $estudiantes);
                    } else {
                        if (isset($estudiante['existeEstudiante']) AND $estudiante['existeEstudiante'] == 1 AND isset($estudiante['inscritoPeriodoActual']) AND $estudiante['inscritoPeriodoActual'] == 1) {
                            $mensaje = "El Estudiante <strong>$origenestud-$identificacionestud $nombres $apellidos</strong> está matriculado en el Plantel <strong>$nombreplantel</strong> en el <strong>$gradoestud '$seccionestud'</strong>";
                            return array($mensaje, $busqueda_simple, $estudiante);
                        }
                    }
                }
            } else {
                if (isset($estudiante['error']) AND $estudiante['error'] == 1) {

                    /* MENSAJE ERROR DENTRO DEL STORED PROCEDURE */
                    $mensaje = "Estimado Usuario, ha ocurrido un error durante la búsqueda. Cierre la ventana e intente nuevamente.";
                    Utiles::enviarCorreo(Yii::app()->params['adminEmailSend'], 'ERROR EN SP EN EL MÉTODO ' . $metodo . ' EN LA LINEA ' . $linea, $parametros, Yii::app()->params['adminEmailSend'], Yii::app()->params['adminName']);
                    return array($mensaje, $busqueda_simple, null);
                } else {

                    /* MENSAJE ERROR DENTRO DEL STORED PROCEDURE */
                    $mensaje = "Estimado Usuario, ha ocurrido un error durante la búsqueda. Cierre la ventana e intente nuevamente.";

                    return array($mensaje, $busqueda_simple, null);
                }
            }
        } else {
            $estudiante = $estudiantes[0];
            $error = $estudiante['error'];
        }
    }

    public function getDocumentoIdentidad($origen, $documento_identidad) {
        $identificacion = 'NO POSEE';
        if ($documento_identidad != '' AND $documento_identidad != null) {
            if ($origen != '' AND $origen != null) {
                if ($origen == 'C') {
                    $origen = 'C.E';
                }
                $identificacion = $origen . '-' . $documento_identidad;
            } else {
                $identificacion = $documento_identidad;
            }
        }
        return $identificacion;
    }

    public function buscarEstudiantePorIdentificacion($documento_identidad, $tdocumento_identidad, $cedula_escolar, $identificacion_repre, $nombres, $apellidos, $periodo_id, $grado_id) {
        $busqueda_simple = false;
        $error = false;
        $identificacion = '';
        $t_identificacion = '';
        $resultado = null;
        $parametros = array();
        $metodo = __METHOD__;
        $linea = '';
        $busqueda_repre = false;
        if (!is_null($tdocumento_identidad) AND $tdocumento_identidad != null AND !is_null($documento_identidad) AND $documento_identidad != null) {
            $busqueda_simple = true;
            $identificacion = $documento_identidad;
            $t_identificacion = $tdocumento_identidad;
        } else {
            if (!is_null($cedula_escolar) AND $cedula_escolar != null) {
                $busqueda_simple = true;
                $identificacion = $cedula_escolar;
                $t_identificacion = 'C';
            } else {
                if (!is_null($identificacion_repre) AND $identificacion_repre != null) {
                    $busqueda_simple = false;
                    $busqueda_repre = true;
                } else {
                    if (!is_null($nombres) AND $nombres != '' AND !is_null($apellidos) AND $apellidos != '') {
                        $busqueda_simple = false;
                    } else {
                        $error = true;
                    }
                }
            }
        }
        if ($busqueda_simple) {
            $linea = __LINE__;
            try {
                $sql = 'SELECT  * FROM matricula.estudiante_matriculado_x_identificacion(:origen::CHARACTER,:identificacion::CHARACTER VARYING ,:grado_id::INT,:periodo_id::INT) AS
                        f("existeEstudiante" smallint,
                          "inscritoPeriodoActual" smallint,
                          "mensajeResultado" text,
                          "idEstud" integer,
                          "origenEstud" character varying(1),
                          "identificacionEstud" character varying(20),
                          "nombresEstud" character varying(120),
                          "apellidosEstud" character varying(120),
                          "fechaNacimientoEstud" date,
                          "codPlantel" character varying(15),
                          "nombrePlantel" character varying(150),
                          "codPlantelActual" character varying(15),
                          "nombrePlantelActual" character varying(150),
                          "gradoEstud" character varying(80),
                          "seccionEstud" character varying(1),
                          "consecutivoGradoAnterior" integer,
                          "consecutivoGradoInscripcion" integer,
                          "periodoIndicado" character varying(20),
                          "error" smallint
                         )';
                $buqueda = Yii::app()->db->createCommand($sql);
                $buqueda->bindParam(":origen", $t_identificacion, PDO::PARAM_STR);
                $buqueda->bindParam(":identificacion", $identificacion, PDO::PARAM_STR);
                $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
                $buqueda->bindParam(":grado_id", $grado_id, PDO::PARAM_INT);
                $resultado = $buqueda->queryAll();
                $parametros = array('error' => htmlentities($resultado[0]['mensajeResultado']), 'query' => '<pre>' . htmlentities($sql) . '</pre>', 'resultado' => $resultado, 'params' => array('origen' => $t_identificacion, 'identificacion' => $identificacion, 'periodo_id' => $periodo_id, 'grado_id' => $grado_id));
            } catch (Exception $e) {
                var_dump($e->getMessage());
            }
        } else {
            if (!$busqueda_simple AND !$error AND $busqueda_repre) {
                $sql = 'SELECT  * FROM matricula.estudiante_matriculado_x_identificacion(:origen::CHARACTER,:identificacion::CHARACTER VARYING ,:grado_id::INT,:periodo_id::INT) AS
                         f("existeEstudiante" smallint,
                          "inscritoPeriodoActual" smallint,
                          "mensajeResultado" text,
                          "idEstud" integer,
                          "origenEstud" character varying(1),
                          "identificacionEstud" character varying(20),
                          "nombresEstud" character varying(120),
                          "apellidosEstud" character varying(120),
                          "fechaNacimientoEstud" date,
                          "codPlantel" character varying(15),
                          "nombrePlantel" character varying(150),
                          "codPlantelActual" character varying(15),
                          "nombrePlantelActual" character varying(150),
                          "gradoEstud" character varying(80),
                          "seccionEstud" character varying(1),
                          "consecutivoGradoAnterior" integer,
                          "consecutivoGradoInscripcion" integer,
                          "periodoIndicado" character varying(20),
                          "error" smallint
                         )';

                $buqueda = Yii::app()->db->createCommand($sql);

                $buqueda->bindParam(":origen", $t_identificacion, PDO::PARAM_STR);
                $buqueda->bindParam(":identificacion", $identificacion, PDO::PARAM_STR);
                $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
                $buqueda->bindParam(":grado_id", $grado_id, PDO::PARAM_INT);
                $resultado = $buqueda->queryAll();
                $parametros = array('error' => htmlentities($resultado[0]['mensajeResultado']), 'query' => '<pre>' . htmlentities($sql) . '</pre>', 'resultado' => $resultado, 'params' => array('origen' => $t_identificacion, 'identificacion' => $identificacion, 'periodo_id' => $periodo_id, 'grado_id' => $grado_id));
            } else {

                /* MENSAJE ERROR NO DEBERIA OCURRIR NUNCA */
            }
        }
        return array($busqueda_simple, $resultado, nl2br(print_r($parametros, true)), $metodo, $linea);
    }

    public function buscarEstudiantePorIdentificacion_En_Desarrollo($documento_identidad, $tdocumento_identidad, $cedula_escolar, $identificacion_repre, $nombres, $apellidos, $periodo_id, $grado_id) {
        $busqueda_simple = false;
        $error = false;
        $identificacion = '';
        $t_identificacion = '';
        $resultado = null;
        $parametros = array();
        $metodo = __METHOD__;
        $linea = '';
        $busqueda_repre = false;
        if (!is_null($tdocumento_identidad) AND $tdocumento_identidad != null AND !is_null($documento_identidad) AND $documento_identidad != null) {
            $busqueda_simple = true;
            $identificacion = $documento_identidad;
            $t_identificacion = $tdocumento_identidad;
        } else {
            if (!is_null($cedula_escolar) AND $cedula_escolar != null) {
                $busqueda_simple = true;
                $identificacion = $cedula_escolar;
                $t_identificacion = 'C';
            } else {
                if (!is_null($identificacion_repre) AND $identificacion_repre != null) {
                    $busqueda_simple = false;
                    $busqueda_repre = true;
                } else {
                    if (!is_null($nombres) AND $nombres != '' AND !is_null($apellidos) AND $apellidos != '') {
                        $busqueda_simple = false;
                    } else {
                        $error = true;
                    }
                }
            }
        }

        if ($busqueda_simple) {
            $linea = __LINE__;
            try {
                $sql = 'SELECT  * FROM matricula.estudiante_matriculado_x_identificacion(:origen::CHARACTER,:identificacion::CHARACTER VARYING ,:grado_id::INT,:periodo_id::INT) AS
                        f("existeEstudiante" smallint,
                          "inscritoPeriodoActual" smallint,
                          "mensajeResultado" text,
                          "idEstud" integer,
                          "origenEstud" character varying(1),
                          "identificacionEstud" character varying(20),
                          "nombresEstud" character varying(120),
                          "apellidosEstud" character varying(120),
                          "fechaNacimientoEstud" date,
                          "codPlantel" character varying(15),
                          "nombrePlantel" character varying(150),
                          "codPlantelActual" character varying(15),
                          "nombrePlantelActual" character varying(150),
                          "gradoEstud" character varying(80),
                          "seccionEstud" character varying(1),
                          "consecutivoGradoAnterior" integer,
                          "consecutivoGradoInscripcion" integer,
                          "periodoIndicado" character varying(20),
                          "error" smallint
                         )';
                $buqueda = Yii::app()->db->createCommand($sql);
                $buqueda->bindParam(":origen", $t_identificacion, PDO::PARAM_STR);
                $buqueda->bindParam(":identificacion", $identificacion, PDO::PARAM_STR);
                $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
                $buqueda->bindParam(":grado_id", $grado_id, PDO::PARAM_INT);
                $resultado = $buqueda->queryAll();
                $parametros = array('error' => htmlentities($resultado[0]['mensajeResultado']), 'query' => '<pre>' . htmlentities($sql) . '</pre>', 'resultado' => $resultado, 'params' => array('origen' => $t_identificacion, 'identificacion' => $identificacion, 'periodo_id' => $periodo_id, 'grado_id' => $grado_id));
            } catch (Exception $e) {
                var_dump($e->getMessage());
            }
        } else {
            if (!$busqueda_simple AND !$error AND $busqueda_repre) {
                $sql = 'SELECT  * FROM matricula.estudiante_matriculado_x_identificacion(:origen::CHARACTER,:identificacion::CHARACTER VARYING ,:grado_id::INT,:periodo_id::INT) AS
                         f("existeEstudiante" smallint,
                          "inscritoPeriodoActual" smallint,
                          "mensajeResultado" text,
                          "idEstud" integer,
                          "origenEstud" character varying(1),
                          "identificacionEstud" character varying(20),
                          "nombresEstud" character varying(120),
                          "apellidosEstud" character varying(120),
                          "fechaNacimientoEstud" date,
                          "codPlantel" character varying(15),
                          "nombrePlantel" character varying(150),
                          "codPlantelActual" character varying(15),
                          "nombrePlantelActual" character varying(150),
                          "gradoEstud" character varying(80),
                          "seccionEstud" character varying(1),
                          "consecutivoGradoAnterior" integer,
                          "consecutivoGradoInscripcion" integer,
                          "periodoIndicado" character varying(20),
                          "error" smallint
                         )';

                $buqueda = Yii::app()->db->createCommand($sql);

                $buqueda->bindParam(":origen", $t_identificacion, PDO::PARAM_STR);
                $buqueda->bindParam(":identificacion", $identificacion, PDO::PARAM_STR);
                $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
                $buqueda->bindParam(":grado_id", $grado_id, PDO::PARAM_INT);
                $resultado = $buqueda->queryAll();
                $parametros = array('error' => htmlentities($resultado[0]['mensajeResultado']), 'query' => '<pre>' . htmlentities($sql) . '</pre>', 'resultado' => $resultado, 'params' => array('origen' => $t_identificacion, 'identificacion' => $identificacion, 'periodo_id' => $periodo_id, 'grado_id' => $grado_id));
            } else {

                /* MENSAJE ERROR NO DEBERIA OCURRIR NUNCA */
            }
        }
        return array($busqueda_simple, $resultado, nl2br(print_r($parametros, true)), $metodo, $linea);
    }

    /* --------------------------------- FIN------------------------------ */

    /**
     * Funcion que busca al estudiante mediante la Cédula de Identidad.
     *
     * @author Meylin Guillén
     * @param string $origen Indica la nacionalidad ingresada con la CI desde un formulario (V ó E)
     * @param int $cedula Numero de Cédula obtenido desde el controlador
     *
     */
    public function busquedaSaime($origen, $cedula) {
        $cedulaInt = $cedula;
        $sql = "SELECT cedula, (primer_nombre || ' ' || segundo_nombre) AS nombre, (primer_apellido || ' ' || segundo_apellido) AS apellido , fecha_nacimiento AS fecha "
            . " FROM saime s"
            . " WHERE "
            . " s.cedula= :cedula AND "
            . " s.origen= :origen ";

        $buqueda = Yii::app()->dbSaime->createCommand($sql);
        $buqueda->bindParam(":cedula", $cedulaInt, PDO::PARAM_INT);
        $buqueda->bindParam(":origen", $origen, PDO::PARAM_INT);
        $resultadoCedula = $buqueda->queryRow();

        if ($resultadoCedula !== array()) {
            return $resultadoCedula;
        } else {
            return false;
        }
    }

    /* -------------------------------------------------------------------fin */

    public function buscarEstudianteNuevo($cedula) {
        $sql = "SELECT *
                FROM matricula.estudiante e
                WHERE
                e.cedula_identidad=:cedula";
        $select = Yii::app()->db->createCommand($sql);
        $select->bindParam(":cedula", $cedula, PDO:: PARAM_INT);
        $resultado = $select->queryAll();
        return ($resultado);
    }

    public function datosEstudianteInscrito($id) {
        $sql = "SELECT *
                FROM matricula.estudiante e
                WHERE
                e.id=:id";
        $select = Yii::app()->db->createCommand($sql);
        $select->bindParam(":id", $id, PDO:: PARAM_INT);
        $resultado = $select->queryAll();
        return $resultado;
    }

    /**
     * Funcion que busca al estudiante que se intenta registrar sin cédula de identidad, llamando a una funcion sql.
     * Busca los siguientes casos:
     * 1- Si el representante tiene a un estudiante relacionado con la misma fecha de nacimiento y el mismo nombre y apellido.
     * 2- Si la cedula escolar asociada a ese estudiante ya esta registrada
     * 3- Si no existe ninguna coincidencia (Caso exitoso que permite el registro)
     *
     * @author Meylin Guillén
     * @param modelo $mEstudiante Modelo Estudiante cargado desde el controlador, tomando los atributos : cedula_escolar, nombres, apellidos, representante_id, fecha_nacimiento.
     *
     */
    public function validarEstudiante($mEstudiante) {
        $cedula_escolar = $mEstudiante['cedula_escolar'];
        $nombres = $mEstudiante['nombres'];
        $apellidos = $mEstudiante['apellidos'];
        $representante_id = $mEstudiante['representante_id'];
        $fecha_nacimiento = $mEstudiante['fecha_nacimiento'];

        $sql = "SELECT
            matricula.existe_estudiante(
            :cedula_escolar,
            :nombres ,
            :apellidos ,
            :representante,
            :fecha_nacimiento
            ) AS existe;";
        $select = Yii::app()->db->createCommand($sql);
        $select->bindParam(":cedula_escolar", $cedula_escolar, PDO:: PARAM_INT);
        $select->bindParam(":nombres", $nombres, PDO:: PARAM_STR);
        $select->bindParam(":apellidos", $apellidos, PDO:: PARAM_STR);
        $select->bindParam(":representante", $representante_id, PDO:: PARAM_INT);
        $select->bindParam(":fecha_nacimiento", $fecha_nacimiento, PDO:: PARAM_INT);

        $resultado = $select->queryAll();
        return ($resultado);
    }

    public function inscribirEstudiantes($estudiantes, $plantel_id, $seccion_plantel_id, $periodo, $modulo, $inscripcion_regular = '{0}', $doble_inscripcion = '{0}', $repitiente_completo = '{0}', $materia_pendiente = '{0}', $observacion = '{NULL}', $repitiente = '{0}') {

        try {
            $usuario_id = Yii::app()->user->id;
            $username = Yii::app()->user->name;
            $ip = Utiles::getRealIP();
        } catch (Exception $e) {
            $usuario_id = 1;
            $username = 'admin';
            $ip = '172.16.3.76';
        }

        $sql = "SELECT matricula.inscribir_estudiantes(:estudiantes, :seccion_plantel_id, :plantel_id, :periodo_id, :usuario_id, :direccion_ip, :username, :modulo, :inscripcion_regular, :doble_inscripcion, :repitiente_completo, :materia_pendiente, :observacion, :repitiente)";

        $inscripcionEstudiante = Yii::app()->db->createCommand($sql);
        $inscripcionEstudiante->bindParam(":estudiantes", $estudiantes, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":periodo_id", $periodo, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":direccion_ip", $ip, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":username", $username, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":modulo", $modulo, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":inscripcion_regular", $inscripcion_regular, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":doble_inscripcion", $doble_inscripcion, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":repitiente_completo", $repitiente_completo, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":materia_pendiente", $materia_pendiente, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":repitiente", $repitiente, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":observacion", $observacion, PDO::PARAM_INT);

        $resultado = $inscripcionEstudiante->execute();
        return ($resultado > 0);
    }

    public function inscribirEstudiante($estudiantes, $plantel_id, $seccion_plantel_id, $periodo, $modulo, $inscripcion_regular, $doble_inscripcion, $repitiente = '{0}', $observacion = '') {
        $usuario_id = Yii::app()->user->id;
        $username = Yii::app()->user->name;
        $ip = Utiles::getRealIP();
        /*        var_dump($estudiantes, $seccion_plantel_id, $plantel_id, $periodo, $usuario_id, $ip, $username, $modulo, $observacion, $inscripcion_regular, $doble_inscripcion);
          die(); */
        $sql = "SELECT matricula.inscribir_estudiante(:estudiantes, :seccion_plantel_id, :plantel_id, :periodo_id, :usuario_id, :direccion_ip, :username, :modulo,:observacion, :inscripcion_regular, :doble_inscripcion, :repitiente)";

        $inscripcionEstudiante = Yii::app()->db->createCommand($sql);
        $inscripcionEstudiante->bindParam(":estudiantes", $estudiantes, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":periodo_id", $periodo, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":direccion_ip", $ip, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":username", $username, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":modulo", $modulo, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":inscripcion_regular", $inscripcion_regular, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":doble_inscripcion", $doble_inscripcion, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":repitiente", $repitiente, PDO::PARAM_INT);
        $inscripcionEstudiante->bindParam(":observacion", $observacion, PDO::PARAM_INT);

        $resultado = $inscripcionEstudiante->execute();
    }

    /**
     * Permite obtener los datos básicos de un estudiante.
     * Si el estudiante no existe  o si el valor del parametro que recibe no es numerico RETORNA NULL
     * @author Ignacio Salazar
     * @param integer $estudiante_id ID del estudiante
     * @return array  Retorna un arreglo con los siguientes indices (nombres, apellidos, fecha_nacimiento, grado_actual, grado_actual_id, cedula_identidad, cedula_escolar)
     */
    public function getDatosEstudiante($estudiante_id,$simple=false) {
        $resultado = null;
        if (is_numeric($estudiante_id)) {
            if($simple){
                $sql = "    SELECT "
                    ." me.id,"
                    . "     upper(me.nombres) nombres,"
                    . "     upper(me.apellidos) apellidos"
                    . " FROM"
                    . "     matricula.estudiante me"
                    . " WHERE "
                    . "     me.id = :estudiante_id"
                    . " ORDER BY me.documento_identidad ASC";
            }
            else {
                $sql = "    SELECT "
                    . "     upper(me.nombres) nombres,"
                    . "     upper(me.apellidos) apellidos,"
                    . "     me.fecha_nacimiento, "
                    . "     g.nombre grado_actual, "
                    . "     g.id grado_actual_id, "
                    . "     me.documento_identidad, "
                    . "     me.tdocumento_identidad, "
                    . "     me.cedula_escolar,"
                    . "     p.nombre plantel_actual"
                    . " FROM"
                    . "     matricula.estudiante me"
                    . " LEFT JOIN gplantel.grado g on (g.id = me.grado_actual_id)"
                    . " LEFT JOIN gplantel.plantel p on (p.id = me.plantel_actual_id)"
                    . " WHERE "
                    . "     me.id = :estudiante_id"
                    . " ORDER BY me.documento_identidad ASC";
            }

            $busqueda = Yii::app()->db->createCommand($sql);
            $busqueda->bindParam(":estudiante_id", $estudiante_id, PDO:: PARAM_INT);

            $resultado = $busqueda->queryRow();
            if ($resultado == array())
                $resultado = null;
        }

        return ($resultado);
    }

    /*     * ********************************************** */


    /* SELECT  me.cedula_escolar, me.cedula_identidad, me.nombres, me.apellidos,
      mr.nombres as representante,me.representante_id,mr.id

      FROM matricula.estudiante me
      INNER JOIN matricula.representante mr on me.representante_id=mr.id
      where me.cedula_escolar=22 or me.cedula_identidad = null or mr.cedula_identidad=null or
      me.nombres ilike '%null%' or me.apellidos ilike '%null%'; */

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function verificacionInscrito($cedula_escolar = '', $documento_identidad = '', $nombres = '', $apellidos = '', $cirepresentante = '', $busquedaCompleta, $seccion_plantel_id, $plantel_id) {
        $where = '';
        if ($cedula_escolar !== '') {

            $where = "me.cedula_escolar=$cedula_escolar ";
        }

        if ($documento_identidad !== '') {
            if ($where !== '') {
                $where .= " and me.cedula_identidad=$documento_identidad";
            } else {
                $where = " me.cedula_identidad=$documento_identidad";
            }
        }

        if ($nombres !== '' && $apellidos !== '') {
            $nombres = trim($nombres);
            $apellidos = trim($apellidos);
            $apellido1 = '';
            $apellido2 = '';
            $apellido3 = '';
            $nombre1 = '';
            $nombre2 = '';
            $nombre3 = '';
            /* list($apellido1, $apellido2, $apellido3) = explode(" ", $apellidos); */
            $apellidos_exploded = explode(" ", $apellidos);
            $apellido1 = isset($apellidos_exploded[0]) ? $apellidos_exploded[0] : '';
            $apellido2 = isset($apellidos_exploded[1]) ? $apellidos_exploded[1] : '';
            $apellido3 = isset($apellidos_exploded[2]) ? $apellidos_exploded[2] : '';
            /* list($nombre1, $nombre2, $nombre3) = explode(" ", $nombres); */
            $nombres_exploded = explode(" ", $nombres);
            $nombre1 = isset($nombres_exploded[0]) ? $nombres_exploded[0] : '';
            $nombre2 = isset($nombres_exploded[1]) ? $nombres_exploded[1] : '';
            $nombre3 = isset($nombres_exploded[2]) ? $nombres_exploded[2] : '';
            if ($where !== '') {

                $where .= " and busqueda @@ plainto_tsquery('pg_catalog.spanish', '" . "$nombre1 " . " $nombre2" . " $nombre3" . " $apellido1" . " $apellido2" . " $apellido3" . "')";
                /*                $where .= ' and busqueda @@ TO_tsquery(' . "'" . '"pg_catalog.spanish"' . "," . '"' . $nombre1 . '" & "' . $nombre2 . '" & "' . $nombre3 . '" & "' . $apellido1 . '" & "' . $apellido2 . '" & "' . $apellido3 . '"' . "'" . ')'; */
            } else {


                $where .= " busqueda @@ plainto_tsquery('pg_catalog.spanish', '" . "$nombre1 " . " $nombre2" . " $nombre3" . " $apellido1" . " $apellido2" . " $apellido3" . "')";
                /*                $where .= ' busqueda @@ TO_tsquery(' . "'" . '"pg_catalog.spanish"' . "," . '"' . $nombre1 . '" & "' . $nombre2 . '" & "' . $nombre3 . '" & "' . $apellido1 . '" & "' . $apellido2 . '" & "' . $apellido3 . '"' . "'" . ')'; */
                /*    $where = " me.nombres like '$nombres%'"; */
            }
        }
        /* pg_catalog.spanish */

        if ($cirepresentante !== '') {
            if ($where !== '') {
                $where .= " and mr.cedula_identidad = $cirepresentante ";
            } else {
                $where = " mr.cedula_identidad = $cirepresentante";
            }
        }

        $gradosPrescolar [] = 3;
        $gradosPrescolar [] = 4;
        $gradosPrescolar [] = 7;
        $gradosPrescolar [] = 8;
        $gradosPrescolar [] = 11;
        $gradosPrescolar [] = 12;
        $nombres = strtoupper($nombres);
        $apellidos = strtoupper($apellidos);
        $periodo_Escolar = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_Escolar_id = $periodo_Escolar['id'];

        $sql = "";


        $guard = Yii::app()->db->createCommand($sql);
        $buscarEstudiante = $guard->queryAll();
    }

    /**
     * Eliminación Lógica de un Estudiante.
     *
     * @param integer $id
     * @param string $accion 'A'=Activar, 'E'=Inactivar
     * @param integer $inscripcion_id
     */
    public function cambiarEstatusEstudiante($id, $accion, $inscripcion_id) {

        $result = new stdClass();
        $result->isSuccess = false;
        $result->message = 'No existe el Estudiante Indicado.';
        $nombres = '';
        $apellidos = '';

        if (in_array($accion, array('E', 'A'))) {

            if (is_numeric($id)) {

                $estudiante = $this->findByPk($id);

                if ($estudiante) {
                    $result->message = 'Ha ocurrido un error en el Proceso.';
                    $estudiante->estatus_id = 4; // ASIGNADO
                    $estudiante->usuario_act_id = Yii::app()->user->id;
                    $estudiante->grado_actual_id = $estudiante->grado_anterior_id;

                    if ($accion == 'E') {
                        $estudiante->fecha_elim = date('Y-m-d H:i:s');
                    }

                    if ($estudiante->update()) {

                        $asignatura_estudiante = AsignaturaEstudiante::model()->inactivarAsignaturaEstudiante($inscripcion_id);
                        if ($asignatura_estudiante != null) {
                            $inscripcion_estudiante = InscripcionEstudiante::model()->inactivarInscripcionEstudiante($inscripcion_id);
                            if ($inscripcion_estudiante) {
                                $messageUsers = 'El Estudiante ha sido ' . strtr($accion, array('A' => 'incluido.', 'E' => 'excluido.'));
                                $result->isSuccess = true;
                                $nombres = (isset($estudiante->nombres) AND $estudiante->nombres != '') ? $estudiante->nombres : '';
                                $apellidos = (isset($estudiante->apellidos) AND $estudiante->apellidos != '') ? $estudiante->apellidos : '';
                                $result->message = 'Se ha ' . strtr($accion, array('A' => 'incluido', 'E' => 'excluido')) . ' el Estudiante ' . $nombres . ' ' . $apellidos;
                            }
                        }
                    }
                }
            }
        } else {
            $result->message = 'No se ha especificado la acción a tomar sobre el Estudiante, recargue la página e intentelo de nuevo.';
        }

        return $result;
    }

    public function estudiantesMatriculadosSeccion($seccionId, $periodo_id = 14) {
        $estatus = 'A';
        /* $estatus_id = 1;  MATRICULADO
          $resultado = null;
          $sql = "SELECT  e.id,e.cedula_identidad, e.cedula_escolar, e.fecha_nacimiento, e.nombres || ' ' || e.apellidos as nomApe, ie.estatus, ie.id as inscripcion_id"
          . " FROM"
          . " gplantel.seccion_plantel sp"
          . " INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.seccion_plantel_id = sp.id)"
          . " INNER JOIN matricula.inscripcion_estudiante ie on (spp.id = ie.seccion_plantel_periodo_id)"
          . " INNER JOIN matricula.estudiante e on (e.id = ie.estudiante_id)"
          . " LEFT JOIN matricula.representante r on (r.id = e.representante_id)"
          . " WHERE spp.seccion_plantel_id = :seccion_plantel_id"
          . " AND ie.estatus= :estatus"
          . " ORDER BY ie.id DESC"; */
        if (is_numeric($seccionId)) {
            $sql = "    SELECT "
                . " me.nacionalidad,"
                . " me.documento_identidad,"
                . " me.tdocumento_identidad, "
                . " me.cedula_escolar, "
                . " me.nombres, "
                . " me.apellidos, "
                . " me.fecha_nacimiento, "
                . " me.sexo, "
                . " mie.inscripcion_regular,"
                . " mie.materia_pendiente,"
                . " mie.repitiente,"
                . " mie.doble_inscripcion,"
                . " mie.observacion"
                . " FROM matricula.estudiante me"
                . " INNER JOIN matricula.inscripcion_estudiante mie ON (me.id = mie.estudiante_id)"
                . " INNER JOIN gplantel.seccion_plantel_periodo spp_i on (spp_i.id = mie.seccion_plantel_periodo_id)"
                . " INNER JOIN gplantel.seccion_plantel spl on (spl.id = spp_i.seccion_plantel_id)"
                . " WHERE spl.id = :seccion_plantel_id AND mie.periodo_id = :periodo_id AND mie.estatus = :estatus "
                . " ORDER BY me.documento_identidad ASC ";
            $buqueda = Yii::app()->db->createCommand($sql);
            $buqueda->bindParam(":seccion_plantel_id", $seccionId, PDO::PARAM_INT);
            $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
            $buqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
            /* $buqueda->bindParam(":estatus_id", $estatus_id, PDO::PARAM_STR); */
            $resultado = $buqueda->queryAll();
        }

        return $resultado;
    }

    public function buscarEstudianteInicialPrimaria($where, $periodo_Escolar_anterior_id, $periodo_Escolar_id, $seccion_plantel_id, $busquedaCompleta, $periodo_id_nuevo) {
        $gradosPrescolar [] = 3;
        $gradosPrescolar [] = 4;
        $gradosPrescolar [] = 7;
        $gradosPrescolar [] = 8;
        $gradosPrescolar [] = 11;
        $gradosPrescolar [] = 12;
        /* if($periodo_id_nuevo==14) */
        $gradosPrescolar [] = 2; // PRIMER GRADO DEBE BUSCAR EN PREESCOLAR 2013-2014

        if ($busquedaCompleta == 0) {
            $grado = SeccionPlantel::model()->obtenerGrado($seccion_plantel_id);
            if (in_array($grado, $gradosPrescolar)) {
                $sql = "
                    SELECT DISTINCT me.cedula_escolar, me.documento_identidad,me.tdocumento_identidad, me.nombres, me.apellidos, mr.nombres AS representante, me.representante_id, me.id, p.nombre as plantel_nombre, p.cod_plantel, me.grado_actual_id as grado_id
                    FROM matricula.estudiante me
                    LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)
                    LEFT JOIN gplantel.plantel p on me.plantel_actual_id=p.id
                    LEFT JOIN gplantel.grado g ON (me.grado_actual_id = g.id)
                    WHERE $where and me.fecha_nacimiento BETWEEN '2006-01-01' AND '2014-12-31'
                AND me.id not in (SELECT m_ie.estudiante_id
                    FROM matricula.inscripcion_estudiante m_ie
                    INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                    WHERE spp_i.periodo_id = $periodo_id_nuevo)
                    ";
            } else {

                $sql = "
                    SELECT DISTINCT me.cedula_escolar, me.documento_identidad,me.tdocumento_identidad, me.nombres, me.apellidos, mr.nombres AS representante, me.representante_id, me.id, p.nombre as plantel_nombre, p.cod_plantel, me.grado_actual_id as grado_id
                    FROM matricula.estudiante me
                    LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)
                    LEFT JOIN gplantel.plantel p on me.plantel_actual_id=p.id
                    LEFT JOIN gplantel.grado g ON (me.grado_actual_id = g.id)
                    WHERE $where AND me.fecha_nacimiento BETWEEN '1999-01-01' AND '2007-12-31'
                AND me.id not in (SELECT m_ie.estudiante_id
                    FROM matricula.inscripcion_estudiante m_ie
                    INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                    WHERE spp_i.periodo_id = $periodo_id_nuevo)
                    ";
                /* $sql = "
                  SELECT DISTINCT me.cedula_escolar, me.documento_identidad,me.tdocumento_identidad, me.nombres, me.apellidos, mr.nombres AS representante, me.representante_id, me.id, p.nombre as plantel_nombre, p.cod_plantel, me.grado_actual_id as grado_id
                  FROM matricula.estudiante me
                  LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)
                  INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id
                  INNER JOIN gplantel.plantel p on ih.plantel_id=p.id
                  INNER JOIN gplantel.grado g ON (ih.grado_id = g.id)
                  WHERE $where
                  AND ih.fperiodoescolar_id = $periodo_Escolar_anterior_id
                  AND me.id not in (
                  SELECT m_ie.estudiante_id
                  FROM matricula.inscripcion_estudiante m_ie
                  INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                  WHERE spp_i.periodo_id = $periodo_id_nuevo)
                  UNION
                  SELECT DISTINCT me.cedula_escolar, me.documento_identidad,me.tdocumento_identidad, me.nombres, me.apellidos, mr.nombres AS representante, me.representante_id, me.id, p.nombre as plantel_nombre, p.cod_plantel, me.grado_actual_id as grado_id
                  FROM matricula.estudiante me
                  LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)
                  INNER JOIN gplantel.plantel p on me.plantel_actual_id=p.id
                  INNER JOIN gplantel.grado g ON (me.grado_actual_id = g.id)
                  WHERE $where
                  AND me.id not in ( SELECT m_ie.estudiante_id
                  FROM matricula.inscripcion_estudiante m_ie
                  INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                  WHERE spp_i.periodo_id = $periodo_id_nuevo)
                  "; */
            }
            /* SELECT DISTINCT me.cedula_escolar, me.documento_identidad,me.tdocumento_identidad, me.nombres, me.apellidos, mr.nombres AS representante, me.representante_id, me.id, p.nombre as plantel_nombre, p.cod_plantel, me.grado_actual_id as grado_id
              FROM matricula.estudiante me
              LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)
              INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id
              INNER JOIN gplantel.plantel p on ih.plantel_id=p.id
              INNER JOIN gplantel.grado g ON (ih.grado_id = g.id)
              WHERE $where
              AND ih.fperiodoescolar_id = $periodo_Escolar_anterior_id
              AND me.id not in (
              SELECT m_ie.estudiante_id
              FROM matricula.inscripcion_estudiante m_ie
              INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
              WHERE spp_i.periodo_id = $periodo_id_nuevo)
              UNION */

            /* AND g.consecutivo = (SELECT DISTINCT (g_i.consecutivo-1)as consecutivo
              FROM gplantel.grado g_i
              INNER JOIN gplantel.seccion_plantel spl_i ON (spl_i.grado_id = g_i.id)
              WHERE spl_i.id = $seccion_plantel_id) */

            $guard = Yii::app()->db->createCommand($sql);
            $buscarEstudiante = $guard->queryAll();
            return $buscarEstudiante;
        } else {
            $sql = "SELECT  DISTINCT me.cedula_escolar, me.documento_identidad,me.tdocumento_identidad, me.nombres, me.apellidos, mr.nombres AS representante, me.representante_id, me.id, p.nombre as plantel_nombre, p.cod_plantel, me.grado_actual_id as grado_id
                    FROM matricula.estudiante me
                    LEFT JOIN matricula.representante mr ON (me.representante_id=mr.id)
                    LEFT JOIN gplantel.plantel p ON (me.plantel_actual_id=p.id)
                    LEFT JOIN legacy.talumno_acad taa ON (me.sigue_id=taa.s_alumno_id)
                    INNER JOIN gplantel.grado g ON (me.grado_actual_id=g.id)
                    WHERE $where AND me.fecha_nacimiento BETWEEN '1999-01-01' AND '2006-12-31'
                AND taa.fperiodoescolar <> '$periodo_Escolar_id'
                AND me.id NOT IN (
                    SELECT m_ie.estudiante_id
                    FROM matricula.inscripcion_estudiante m_ie
                    INNER JOIN gplantel.seccion_plantel_periodo  spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                    WHERE spp_i.periodo_id=$periodo_id_nuevo)

                    UNION
                    SELECT DISTINCT me.cedula_escolar, me.documento_identidad,me.tdocumento_identidad, me.nombres, me.apellidos, mr.nombres AS representante, me.representante_id, me.id, p.nombre as plantel_nombre, p.cod_plantel, me.grado_actual_id as grado_id
                    FROM matricula.estudiante me
                    LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)
                    LEFT JOIN gplantel.plantel p on me.plantel_actual_id=p.id
                    LEFT JOIN gplantel.grado g ON (me.grado_actual_id = g.id)
                    WHERE $where AND me.fecha_nacimiento BETWEEN '1999-01-01' AND '2006-12-31'
                AND me.id not in ( SELECT m_ie.estudiante_id
                    FROM matricula.inscripcion_estudiante m_ie
                    INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                    WHERE spp_i.periodo_id = $periodo_id_nuevo)
                    ";
            $guard = Yii::app()->db->createCommand($sql);
            $buscarEstudiante = $guard->queryAll();
            return $buscarEstudiante;
        }
    }

    public function existeEstudiante($origen, $identificacion) {
        $estatus='X';
        if (in_array($origen, array('V', 'E', 'P'))) {
            $sql = " SELECT id "
                    . " FROM matricula.estudiante "
                    . " WHERE tdocumento_identidad=:tdocumento_identidad "
                    . " AND documento_identidad=:documento_identidad "
                    //. " AND (estatus IS NULL OR estatus<>:estatus) "
                    . " LIMIT 1";
            $busqueda = Yii::app()->db->createCommand($sql);
            $busqueda->bindParam(":tdocumento_identidad", $origen, PDO::PARAM_STR);
            $busqueda->bindParam(":documento_identidad", $identificacion, PDO::PARAM_STR);
            //$busqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
            $buscarEstudiante = $busqueda->queryColumn();
        } else {
            $sql = " SELECT id "
                    . " FROM matricula.estudiante "
                    . " WHERE  cedula_escolar=:cedula_escolar "
                    //. " AND (estatus IS NULL OR estatus<>:estatus) "
                    . " LIMIT 1";
            $busqueda = Yii::app()->db->createCommand($sql);
            $busqueda->bindParam(":cedula_escolar", $identificacion, PDO::PARAM_STR);
            //$busqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
            $buscarEstudiante = $busqueda->queryColumn();
        }
        return $buscarEstudiante;
    }

    public function estaMatriculado($estudiantes, $periodo_id,$simple=false) {
        //   $periodo_id = 14; //prueba
        $arregloNuevo = array();
        $estudiantesArray = implode(',', $estudiantes);
        $sql = " SELECT DISTINCT estudiante_id "
            . " FROM matricula.inscripcion_estudiante "
            . " WHERE estudiante_id IN ($estudiantesArray)  "
            . " AND periodo_id=:periodo_id ";
        $busqueda = Yii::app()->db->createCommand($sql);
        // $busqueda->bindParam(":estudiantes", $estudiantesArray, PDO::PARAM_STR);
        $busqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_STR);

        if($simple==false){
            $buscarEstudiante = $busqueda->queryColumn();
            foreach ($estudiantes as $key => $value) {//Recorro todos los estudiantes que recibi.
                if (!in_array($value, $buscarEstudiante)) {
                    $arregloNuevo[] = $value; //Agrego el id del estudiante que esta matriculado en el periodo actual.
                }
            }
            return $arregloNuevo;
        }
        else {
            $buscarEstudiante = $busqueda->queryScalar();
            return $buscarEstudiante;
        }
    }

    public function puedeMatricular($origen, $identificacion, $periodo_id, $seccion_plantel_id) {
        $periodo_anterior_id = $periodo_id - 1;
        $x = false;
        if (in_array($origen, array('V', 'E', 'P'))) {
            $x = true;
            $where = "me.documento_identidad=:documento_identidad AND me.tdocumento_identidad=:tdocumento_identidad";
        } else {
            $where = "me.cedula_escolar=:cedula_escolar ";
        }

        $sql = "SELECT DISTINCT me.id
                FROM matricula.estudiante me
                LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)
                INNER JOIN matricula.inscripcion_historico ih on me.sigue_id=ih.s_alumno_id
                INNER JOIN gplantel.plantel p on ih.plantel_id=p.id
                INNER JOIN gplantel.grado g ON (ih.grado_id = g.id)
                WHERE $where
                AND ih.fperiodoescolar_id = :periodo_anterior
                AND me.id not in (
                    SELECT m_ie.estudiante_id
                FROM matricula.inscripcion_estudiante m_ie
                INNER JOIN gplantel.seccion_plantel_periodo spp_i ON (spp_i.id = m_ie.seccion_plantel_periodo_id)
                WHERE spp_i.periodo_id = :periodo_actual)
                AND g.consecutivo = (SELECT DISTINCT (g_i.consecutivo-1)as consecutivo
                FROM gplantel.grado g_i
                INNER JOIN gplantel.seccion_plantel spl_i ON (spl_i.grado_id = g_i.id)
                WHERE spl_i.id = :seccion_plantel_id)";
        $busqueda = Yii::app()->db->createCommand($sql);
        if ($x) {
            $busqueda->bindParam(":tdocumento_identidad", $origen, PDO::PARAM_STR);
            $busqueda->bindParam(":documento_identidad", $identificacion, PDO::PARAM_STR);
        } else {
            $busqueda->bindParam(":cedula_escolar", $identificacion, PDO::PARAM_STR);
        }
        $busqueda->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        $busqueda->bindParam(":periodo_anterior", $periodo_anterior_id, PDO::PARAM_INT);
        $busqueda->bindParam(":periodo_actual", $periodo_id, PDO::PARAM_INT);

        return $buscarEstudiante = $busqueda->queryScalar();
    }

    public function buscarEstudiantesPorId($estudiantes, $periodo_id) {
        if (is_array($estudiantes)) {
            $estudiantes_implode = implode(', ', $estudiantes);

            $sql = "SELECT e.nombres,e.apellidos,e.cedula_escolar,e.tdocumento_identidad,e.documento_identidad
                    FROM matricula.estudiante e
                    INNER JOIN matricula.inscripcion_estudiante ie ON (ie.estudiante_id=e.id)
                    WHERE e.id IN ($estudiantes_implode) AND ie.periodo_id=:periodo_id ORDER BY e.nombres,e.apellidos";
            $busqueda = Yii::app()->db->createCommand($sql);
            $busqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_STR);
            return $buscarEstudiante = $busqueda->queryAll();
        }
        return null;
    }

    public function obtenerIdInscripcion($id) {
        $sql = "SELECT id
                  FROM matricula.inscripcion_estudiante
                 WHERE estudiante_id = :id ORDER BY id DESC";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":id", $id, PDO::PARAM_STR);
        return $buscarEstudiante = $busqueda->queryScalar();
    }

//Busqueda de estudiante nueva.
    public function buscarEstudiantesRepitiente($documento_identidad, $tdocumento_identidad, $cedula_escolar, $ci_representante, $nombres, $apellidos, $periodo_id, $seccion_plantel_id, $repitiente, $grado_id, $individual, $probarInscritos) {

        $identificacion = array();
        $identificacion_representante = '';
        $resultadoNoMatriculado = array(); //Contiene los estudiantes que no han sido matriculados en el periodo actual.
        $resultadoBachillerato = array(); //Contiene los datos de los estudiantes de bachillerato que puede matricular en el periodo actual.
        $resultadoInicial = array(); //Contiene los datos de los estudiantes de inicial o primaria que puede matricular en el periodo actual.
        $estudiantes = array(); //Contiene los estudiantes que existen en la tabla estudiante que se esta buscando.
        $estudiantesInscritos = array();
        $inscritos = array();
        $busqueda = '';
        $mensaje = '';
        $resultado = '';
        $mensaje_final = '';
        $entro = false;

        if ($probarInscritos != array()) {

            foreach ($probarInscritos as $key => $value) {
                $inscritos[] = $value['id'];
            }

        }
        // var_dump($documento_identidad . ' $documento_identidad', $tdocumento_identidad . ' $tdocumento_identidad', $cedula_escolar . ' $cedula_escolar', $ci_representante . ' $ci_representante', $nombres . ' $nombres', $apellidos . ' $apellidos', $periodo_id . ' $periodo_id', $seccion_plantel_id . ' $seccion_plantel_id', $repitiente . ' $repitiente', $grado_id . ' $grado_id', $individual . ' $individual', $probarInscritos);
        //Si en la busqueda esta chequeado el campo "Estudiante Repitiente" indica $repitiente=1 sino indica $repitiente=0.
        if ($documento_identidad != '') {
            $identificacion = $documento_identidad;
            $busqueda = 0; //Indica que la busqueda es por cedula de identidad.
        } else {
            if ($nombres != '' && $apellidos != '') {
                $busqueda = 1; //Indica que la busqueda es por nombres.
            } else {
                if ($cedula_escolar != '') {
                    $identificacion = $cedula_escolar;
                    $tdocumento_identidad = 'C'; //Indica que es una cedula escolar.
                    $busqueda = 2; //Indica que la busqueda es por cedula escolar.
                } else {
                    if ($ci_representante != '') {
                        $identificacion_representante = $ci_representante;
                        $busqueda = 3; //Indica que la busqueda es por cedula del representante.
                    }
                }
            }
        }

        $modalidadPlan=SeccionPlantel::model()->buscarModalidadPlanSeccion($seccion_plantel_id);

        switch ($busqueda) {
            case 0:
                $estudiantes = $this->existeEstudiante($tdocumento_identidad, $identificacion);
                break;
            case 1:
                $estudiantes = $this->buscarEstudiantePorNombres($nombres, $apellidos);
                break;
            case 2:
                $estudiantes = $this->existeEstudiante($tdocumento_identidad, $identificacion);
                break;
            case 3:
                $estudiantes = $this->buscarEstudiantePorRepresentante($identificacion_representante);
                break;
        }
        $modalidad = $modalidadPlan['modalidad_id'];
        $plan_id = $modalidadPlan['plan_id'];

        if ($estudiantes != array() && $estudiantes != false) {
            if(!in_array($modalidad,array(Constantes::MODALIDAD_ESPECIAL))) {//Verifico si el estudiante es distinto de educación especial.
                $resultadoNoMatriculado = $this->estaMatriculado($estudiantes, $periodo_id);

            }elseif(in_array($modalidad,array(Constantes::MODALIDAD_ESPECIAL))){//Si el estudiante es de educacion especial no se verifica que este matriculado ya que puede matricularse varias veces.

                $resultadoDatosMatriculado = $this->estaMatriculadoDatos($estudiantes, $periodo_id);
                if($resultadoDatosMatriculado != array()){
                    foreach($resultadoDatosMatriculado as $key=>$datos){
                        $seccion_plantel_id_matriculado = $datos['seccion_plantel_id'];
                        $plan_id_matriculado = $datos['plan_id'];
                        if($seccion_plantel_id == $seccion_plantel_id_matriculado || $plan_id == $plan_id_matriculado){
                            $entro = true;
                            break;
                        }
                    }
                    if($entro == true){
                        $mensaje = 'El estudiante que esta buscando para matricular ya fue inscrito con el plan de estudio de esta sección plantel, por favor verifique ya que un estudiante no puede estar matriculado mas de una vez con el mismo plan de estudio.<br>';
                        echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    }else{
                        $resultadoNoMatriculado = $estudiantes;
                    }
                }else{
                    $resultadoNoMatriculado = $estudiantes;
                }
            }

            //$resultadoNoMatriculado => Contiene los estudiantes que no han sido matriculado en el periodo escolar actual.
            if (count($resultadoNoMatriculado) <= 0) {
                $mensaje = 'El estudiante que esta buscando ya se encuentra matriculado en el periodo escolar actual.<br>';
                echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                Yii::app()->end();
            } else {
                if ($repitiente == 1) {
                    $resultadoBachillerato = $this->buscarBachillerato($resultadoNoMatriculado, $periodo_id, $grado_id, $repitiente); //Indica si es repitiente

                    foreach ($resultadoBachillerato as $key => $value) {//Recorro todos los estudiantes que no estan matriculados.
                        if (!in_array($value, $inscritos)) {
                            $estudiantesInscritos[] = $value; //Agrego el id del estudiante que esta matriculado en el periodo actual.
                        }
                    }
                    $resultado = $this->datosCompletosEstudiantes($estudiantesInscritos);
                } elseif ($repitiente == 0) {
                    $nivel = SeccionPlantel::model()->obtenerNivel($seccion_plantel_id);
                    if ($nivel != false) {
                        if ($nivel == 1 || $nivel == 2) {
                            //inicial o primaria
                            if(!in_array($modalidad,array(Constantes::MODALIDAD_ESPECIAL,Constantes::MODALIDAD_INTERCULTURAL,Constantes::MODALIDAD_RURAL,Constantes::MODALIDAD_ADULTO))){
                                $resultadoInicial = $this->buscarInicialPrimaria($resultadoNoMatriculado, $nivel);
                            }
                            else {
                                $resultadoInicial = $resultadoNoMatriculado;
                            }

                            if ($resultadoInicial != array()) {
                                foreach ($resultadoInicial as $key => $value) {//Recorro todos los estudiantes que no estan matriculados.
                                    if (!in_array($value, $inscritos)) {
                                        $estudiantesInscritos[] = $value; //Agrego el id del estudiante que esta matriculado en el periodo actual.
                                    }
                                }
                                $resultado = $this->datosCompletosEstudiantes($estudiantesInscritos);
                            } else {
                                $mensaje_final = 'El Estudiante que esta buscando no tiene la edad adecuada para estar en este grado, por favor verifique la fecha de nacimiento que tiene el estudiante en el modulo <b>Estudiantes</b>, porque los rangos permitidos son de <b>1er Maternal Hasta Preescolar (2006-01-01 al 2014-12-31) y si es de 1er Grado a 6to Grado (1991-01-01 al 2008-12-31)</b>.<br>';
                                $resultado = array();
                            }
                        } else {
                            if(!in_array($modalidad,array(Constantes::MODALIDAD_ESPECIAL,Constantes::MODALIDAD_INTERCULTURAL,Constantes::MODALIDAD_RURAL,Constantes::MODALIDAD_ADULTO))){
                                $resultadoBachillerato = $this->buscarBachillerato($resultadoNoMatriculado, $periodo_id, $grado_id, $repitiente); //Indica si es de bachillerato
                            }
                            else {
                                $resultadoBachillerato = $resultadoNoMatriculado;
                            }
                            foreach ($resultadoBachillerato as $key => $value) {//Recorro todos los estudiantes que no estan matriculados.
                                if (!in_array($value, $inscritos)) {
                                    $estudiantesInscritos[] = $value; //Agrego el id del estudiante que esta matriculado en el periodo actual.
                                }
                            }
                            $resultado = $this->datosCompletosEstudiantes($estudiantesInscritos);
                        }
                    } else {
                        $mensaje = 'No se encontró los estudiantes que coincidan con la busqueda realizada, ya que el nivel al que pertenece el estudiante retorno vacio.<br>';
                        echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    }
                    //$resultadoNoRepitiente = $this->noEsRepitiente($estudiante_id, $periodo_id);
                }
                if ($resultado == array()) {
                    if ($mensaje_final == '') {
                        $mensaje = 'No se encontró los estudiantes que coincidan con la busqueda realizada.<br>';
                    } else {
                        $mensaje = $mensaje_final;
                    }
                } else {
                    $mensaje = '';
                }
                return array(
                    0 => $resultado,
                    1 => $mensaje,
                );
            }
        } else {
            $mensaje = 'No se encontró los estudiantes que coincidan con la busqueda realizada, por favor verifique los datos ingresados.<br>';
            echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
            Yii::app()->end();
        }
    }
    /**
     * Version de la busqueda por estudiante repitiente.
     * Si el estudiante no existe  o si lo encuentra en el listado de preinscritos RETORNA NULL
     * @author Alexis Moreno
     * @param integer $documento_identidad numero del documento de identidad del estudiante (cedula,cedula escolar)
     * @param integer $tdocumento_identidad tipo de documento de identidad del estudiante (cedula,cedula escolar)
     * @param string $tdocumento_identidad tipo de documento de identidad del estudiante (cedula,cedula Extranjero,cedula escolar)
     * @param integer $ci_representante cedula del representante
     * @param string $nombres nombres del estudiante
     * @param string $apellidos apellidos del estudiante
     * @param integer $periodo_id id del periodo en que realizara la busqueda
     * @param integer $seccion_plantel_id id de la seccion
     * @param integer $repitiente indica si es repitiente
     * @param integer $individual indica si la inscripcioncion es individual
     * @param array $probarInscritos arreglo de estudiante en el listado de preinscritos
     * @return array  Retorna un arreglo con los siguientes indices (nombres, apellidos, fecha_nacimiento, grado_actual, grado_actual_id, cedula_identidad, cedula_escolar)
     */
    public function buscarEstudiantesRepitienteAngular($documento_identidad, $tdocumento_identidad, $cedula_escolar, $ci_representante, $nombres, $apellidos, $periodo_id, $seccion_plantel_id, $repitiente, $grado_id, $individual, $probarInscritos) {

        $identificacion = array();
        $identificacion_representante = '';
        $resultadoNoMatriculado = array(); //Contiene los estudiantes que no han sido matriculados en el periodo actual.
        $resultadoBachillerato = array(); //Contiene los datos de los estudiantes de bachillerato que puede matricular en el periodo actual.
        $resultadoInicial = array(); //Contiene los datos de los estudiantes de inicial o primaria que puede matricular en el periodo actual.
        $estudiantes = array(); //Contiene los estudiantes que existen en la tabla estudiante que se esta buscando.
        $estudiantesInscritos = array();
        $inscritos = array();
        $busqueda = '';
        $mensaje = '';
        $resultado = '';

        if ($probarInscritos != array()) {

            foreach ($probarInscritos as $key => $value) {
                $inscritos[] = $value['id'];
            }

        }
        //Si en la busqueda esta chequeado el campo "Estudiante Repitiente" indica $repitiente=1 sino indica $repitiente=0.
        if ($documento_identidad != '') {
            $identificacion = $documento_identidad;
            $busqueda = 0; //Indica que la busqueda es por cedula de identidad.
        } else {
            if ($nombres != '' && $apellidos != '') {
                $busqueda = 1; //Indica que la busqueda es por nombres.
            } else {
                if ($cedula_escolar != '') {
                    $identificacion = $cedula_escolar;
                    $tdocumento_identidad = 'C'; //Indica que es una cedula escolar.
                    $busqueda = 2; //Indica que la busqueda es por cedula escolar.
                } else {
                    if ($ci_representante != '') {
                        $identificacion_representante = $ci_representante;
                        $busqueda = 3; //Indica que la busqueda es por cedula del representante.
                    }
                }
            }
        }

        switch ($busqueda) {
            case 0:
                $estudiantes = $this->existeEstudiante($tdocumento_identidad, $identificacion);
                break;
            case 1:
                $estudiantes = $this->buscarEstudiantePorNombres($nombres, $apellidos);
                break;
            case 2:
                $estudiantes = $this->existeEstudiante($tdocumento_identidad, $identificacion);
                break;
            case 3:
                $estudiantes = $this->buscarEstudiantePorRepresentante($identificacion_representante);
                break;
        }

        if ($estudiantes != array() && $estudiantes != false) {
            $resultadoNoMatriculado = $this->estaMatriculado($estudiantes, $periodo_id);
            //$resultadoNoMatriculado => Contiene los estudiantes que no han sido matriculado en el periodo escolar actual.
            if (count($resultadoNoMatriculado) <= 0) {
                $mensaje = 'El estudiante que esta buscando ya se encuentra matriculado en el periodo escolar actual.<br>';
                return array(
                    0 => null,
                    1 => $mensaje,
                );
            } else {
                if ($repitiente == 1) {
                    $resultadoBachillerato = $this->buscarBachillerato($resultadoNoMatriculado, $periodo_id, $grado_id, $repitiente); //Indica si es repitiente

                    foreach ($resultadoBachillerato as $key => $value) {//Recorro todos los estudiantes que no estan matriculados.
                        if (!in_array($value, $inscritos)) {
                            $estudiantesInscritos[] = $value; //Agrego el id del estudiante que esta matriculado en el periodo actual.
                        }
                    }
                    $resultado = $this->datosCompletosEstudiantes($estudiantesInscritos);
                } elseif ($repitiente == 0) {
                    $nivel = SeccionPlantel::model()->obtenerNivel($seccion_plantel_id);
                    if ($nivel != false) {
                        if ($nivel == 1 || $nivel == 2) {
                            //inicial o primaria
                            $resultadoInicial = $this->buscarInicialPrimaria($resultadoNoMatriculado);
                            foreach ($resultadoInicial as $key => $value) {//Recorro todos los estudiantes que no estan matriculados.
                                if (!in_array($value, $inscritos)) {
                                    $estudiantesInscritos[] = $value; //Agrego el id del estudiante que esta matriculado en el periodo actual.
                                }
                            }
                            $resultado = $this->datosCompletosEstudiantes($estudiantesInscritos);
                        } else {
                            $resultadoBachillerato = $this->buscarBachillerato($resultadoNoMatriculado, $periodo_id, $grado_id, $repitiente); //Indica si es de bachillerato
                            foreach ($resultadoBachillerato as $key => $value) {//Recorro todos los estudiantes que no estan matriculados.
                                if (!in_array($value, $inscritos)) {
                                    $estudiantesInscritos[] = $value; //Agrego el id del estudiante que esta matriculado en el periodo actual.
                                }
                            }
                            $resultado = $this->datosCompletosEstudiantes($estudiantesInscritos);
                        }
                    } else {
                        $mensaje = 'No se encontró los estudiantes que coincidan con la busqueda realizada.<br>';
                        return array(
                            0 => null,
                            1 => $mensaje,
                        );
                    }
                }
                if ($resultado == array()) {
                    $mensaje = 'No se encontró los estudiantes que coincidan con la busqueda realizada.<br>';
                } else {
                    $mensaje = '';
                }
                return array(
                    0 => $resultado,
                    1 => $mensaje,
                );
            }
        } else {
            $mensaje = 'No se encontró los estudiantes que coincidan con la busqueda realizada por favor verifique los datos ingresados.<br>';
            return array(
                0 => null,
                1 => $mensaje,
            );
        }
    }


    public function buscarEstudiantePorNombres($nombres, $apellidos) {

        $sql="SELECT vestudiante_id FROM matricula.estudiante_busqueda(:nombres,:apellidos);";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":nombres", $nombres, PDO::PARAM_STR);
        $busqueda->bindParam(":apellidos", $apellidos, PDO::PARAM_STR);
        $buscarEstudiante = $busqueda->queryColumn();
        return $buscarEstudiante;
//        $nombres = trim($nombres);
//        $apellidos = trim($apellidos);
//        $apellido1 = '';
//        $apellido2 = '';
//        $apellido3 = '';
//        $nombre1 = '';
//        $nombre2 = '';
//        $nombre3 = '';
//        $where = '';
//
//// list($apellido1, $apellido2, $apellido3) = explode(" ", $apellidos);
//        $apellidos_exploded = explode(" ", $apellidos);
//        $apellido1 = isset($apellidos_exploded[0]) ? $apellidos_exploded[0] : '';
//        $apellido2 = isset($apellidos_exploded[1]) ? $apellidos_exploded[1] : '';
//        $apellido3 = isset($apellidos_exploded[2]) ? $apellidos_exploded[2] : '';
//// list($nombre1, $nombre2, $nombre3) = explode(" ", $nombres);
//        $nombres_exploded = explode(" ", $nombres);
//        $nombre1 = isset($nombres_exploded[0]) ? $nombres_exploded[0] : '';
//        $nombre2 = isset($nombres_exploded[1]) ? $nombres_exploded[1] : '';
//        $nombre3 = isset($nombres_exploded[2]) ? $nombres_exploded[2] : '';
//
//        $where .= " busqueda @@ plainto_tsquery('pg_catalog.spanish', '" . "$nombre1 " . " $nombre2" . " $nombre3" . " $apellido1" . " $apellido2" . " $apellido3" . "')";
//
//        $sql = " SELECT id "
//                . " FROM matricula.estudiante "
//                . " WHERE $where ";
//        $busqueda = Yii::app()->db->createCommand($sql);
//
//        $buscarEstudiante = $busqueda->queryColumn();
//        return $buscarEstudiante;
    }

    public function buscarEstudiantePorRepresentante($identificacion_representante) {
        $sql = " SELECT e.id "
            . " FROM matricula.estudiante e"
            . " INNER JOIN matricula.representante r ON (r.id = e.representante_id)"
            . " WHERE r.documento_identidad = :documento_identidad";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":documento_identidad", $identificacion_representante, PDO::PARAM_INT);

        $buscarEstudiante = $busqueda->queryColumn();
        return $buscarEstudiante;
    }

    /** Permite validar si el estudiante tiene la edad adecuada para cursar un grado de primaria o inicial.
     * @author Marisela La Cruz
     * @date 30/04/2015
     * @param integer $estudiantesNoMatriculado Contiene un array() con los ID de estudiante que no han sido matriculados.
     * @return array()
     */
    public function buscarInicialPrimaria($estudiantesNoMatriculado, $nivel) {
        if ($nivel == 1) {
            $rango = 8;
            $anio_actual = date('Y');
            $anio_final = $anio_actual - 1;
            $anio_inicial = $anio_final - $rango;
        } elseif ($nivel == 2) {
            $rango_final = 6;
            $rango_inicial = 17;
            $anio_actual = date('Y') - 1;
            $anio_final = $anio_actual - $rango_final;
            $anio_inicial = $anio_final - $rango_inicial;
        }
        $estudiantesArray = implode(',', $estudiantesNoMatriculado);
        $sql = "SELECT DISTINCT me.id
                    FROM matricula.estudiante me
                    WHERE me.id IN ($estudiantesArray) and me.fecha_nacimiento BETWEEN '$anio_inicial-01-01' AND '$anio_final-12-31'";

        $guard = Yii::app()->db->createCommand($sql);

        $buscarEstudiante = $guard->queryColumn();
        return $buscarEstudiante;
    }

    public function buscarBachillerato($estudiantesNoMatriculado, $periodo_id, $grado_id, $repitiente) {
        $condicion = " AND g.consecutivo =
                       (SELECT (g_i.consecutivo-1) FROM gplantel.grado g_i
                       WHERE g_i.id = :grado_id
                       ) AND ie.periodo_id = :periodo_anterior_id";
        if ($repitiente) {
            $condicion = " AND ie.grado_id=:grado_id AND ie.periodo_id = :periodo_anterior_id";
        }
        $periodo_anterior_id = $periodo_id - 1;
        $estudiantesArray = implode(',', $estudiantesNoMatriculado);
//        $sql = "SELECT DISTINCT ie.id
//FROM matricula.estudiante ie
//LEFT JOIN gplantel.grado g ON (ie.grado_actual_id = g.id)
//WHERE ie.id IN ($estudiantesArray)";
        $sql = "SELECT DISTINCT ie.estudiante_id
                    FROM matricula.inscripcion_estudiante ie
                    LEFT JOIN gplantel.grado g ON (ie.grado_id = g.id)
                    WHERE ie.estudiante_id IN ($estudiantesArray) $condicion ";

        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":periodo_anterior_id", $periodo_anterior_id, PDO::PARAM_INT);
        $busqueda->bindParam(":grado_id", $grado_id, PDO::PARAM_INT);

        $buscarEstudiante = $busqueda->queryColumn();
        return $buscarEstudiante;
    }

//Matriculación Manual

    /** Permite validar si el estudiante curso el grado anterior y puede ser matriculado en el periodo escolar actual.
     * @author Marisela La Cruz
     * @date 30/04/2015
     * @param array() $estudiantesNoMatriculado Contiene un array() con los ID de estudiante que no han sido matriculados.
     * @param integer $periodo_id ID del periodo escolar.
     * @param integer $grado_id ID de grado.
     * @return array()
     */
    public function validarBachillerato($estudiantesNoMatriculado, $periodo_id, $grado_id, $repitiente) {

        $grado =  Grado::model()->findByPk($grado_id);
        $gradoNombre = (isset($grado) && $grado != null)? $grado['nombre']:'';
        $periodo_anterior_id = (string) $periodo_id - 1;
        $condicion = " AND g.consecutivo =
                       (SELECT (g_i.consecutivo-1) FROM gplantel.grado g_i
                       WHERE g_i.id=$grado_id
                       ) AND taa.fperiodoescolar = :periodo_anterior_id";
        if ($repitiente) {
            $condicion = " AND REPLACE(UPPER(taa.s_grado_nombre),'.','') = '$gradoNombre' AND taa.fperiodoescolar = :periodo_anterior_id";
        }

        $estudiantesArray = implode(',', $estudiantesNoMatriculado);
        $sql = "SELECT DISTINCT e.id AS estudiante_id
                    FROM matricula.estudiante e
                    LEFT JOIN legacy.talumno_acad taa ON (taa.s_alumno_id = e.sigue_id)
                    LEFT JOIN gplantel.grado g ON (g.nombre = REPLACE(UPPER(taa.s_grado_nombre),'.',''))
                    WHERE e.id IN ($estudiantesArray) $condicion";
//    LEFT JOIN gplantel.grado g ON (g.id = e.grado_actual_id)
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":periodo_anterior_id", $periodo_anterior_id, PDO::PARAM_STR);
        $buscarEstudiante = $busqueda->queryColumn();
        return $buscarEstudiante;
    }

    /** Permite registrar los datos del representante y los datos del estudiante en bd
     * @author Marisela La Cruz
     * @date 30/04/2015
     * @param integer $model Contiene todos los datos del estudiante
     * @return boolean
     */
    public function registrarEstudiante($model) {

        $usuario_id = Yii::app()->user->id;
        $estatus = 'A';
        //Datos Estudiantes
        $tdocumento_identidad = $model['origen'];
        $documento_identidad = $model['documento_identidad'];
        $nombres = $model['nombres'];
        $apellidos = $model['apellidos'];
        $fecha_nacimiento = $model['fecha_nacimiento'];
        $sexo = $model['sexo'];
        $grado = $model['grado_id'];
        $plantel_id = $model['plantel_id'];
        //Fin
        //Datos Representante
        $origen_rep = $model['origen_rep'];
        $documento_identidad_rep = $model['documento_identidad_rep'];
        $nombre_rep = $model['nombres_rep'];
        $apellido_rep = $model['apellidos_rep'];
        $afinidad_id = $model['afinidad_id'];
        //Fin

        $sql = "SELECT id AS representante"
            . " FROM matricula.representante"
            . " WHERE documento_identidad=:documento_identidad_rep"
            . " AND tdocumento_identidad=:origen_rep"
            . " LIMIT 1";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":origen_rep", $origen_rep, PDO::PARAM_STR);
        $busqueda->bindParam(":documento_identidad_rep", $documento_identidad_rep, PDO::PARAM_STR);
        $buscarRepresentante = $busqueda->queryScalar();

        if ($buscarRepresentante == false) {
            $sql = "INSERT INTO matricula.representante"
                . " (tdocumento_identidad, documento_identidad, nombres, apellidos, afinidad_id, usuario_ini_id, estatus)"
                . " VALUES (:origen_rep, :documento_identidad_rep, :nombre_rep, :apellido_rep, :afinidad_id, :usuario_id, :estatus) returning id AS representante";
            $busqueda = Yii::app()->db->createCommand($sql);
            $busqueda->bindParam(":origen_rep", $origen_rep, PDO::PARAM_STR);
            $busqueda->bindParam(":documento_identidad_rep", $documento_identidad_rep, PDO::PARAM_STR);
            $busqueda->bindParam(":nombre_rep", $nombre_rep, PDO::PARAM_STR);
            $busqueda->bindParam(":apellido_rep", $apellido_rep, PDO::PARAM_STR);
            $busqueda->bindParam(":afinidad_id", $afinidad_id, PDO::PARAM_INT);
            $busqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
            $busqueda->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $registrarRepresentante = $busqueda->queryScalar();

            if ($registrarRepresentante != false) {
                $sql = "INSERT INTO matricula.estudiante"
                    . " (tdocumento_identidad, documento_identidad, nombres, apellidos, fecha_nacimiento, sexo, grado_actual_id, grado_anterior_id, representante_id, plantel_actual_id, plantel_anterior_id, usuario_ini_id, estatus, afinidad_id)"
                    . " VALUES (:origen, :documento_identidad, :nombre, :apellido, :fecha_nacimiento, :sexo, :grado, :grado, :representante_id, :plantel_id, :plantel_id, :usuario_id, :estatus, :afinidad_id)";
                $busqueda = Yii::app()->db->createCommand($sql);
                $busqueda->bindParam(":representante_id", $registrarRepresentante, PDO::PARAM_STR);
                $busqueda->bindParam(":origen", $tdocumento_identidad, PDO::PARAM_STR);
                $busqueda->bindParam(":documento_identidad", $documento_identidad, PDO::PARAM_STR);
                $busqueda->bindParam(":nombre", $nombres, PDO::PARAM_STR);
                $busqueda->bindParam(":apellido", $apellidos, PDO::PARAM_STR);
                $busqueda->bindParam(":fecha_nacimiento", $fecha_nacimiento, PDO::PARAM_STR);
                $busqueda->bindParam(":sexo", $sexo, PDO::PARAM_STR);
                $busqueda->bindParam(":grado", $grado, PDO::PARAM_INT);
                $busqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
                $busqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
                $busqueda->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
                $busqueda->bindParam(":afinidad_id", $afinidad_id, PDO::PARAM_INT);
                $buscarEstudiante = $busqueda->execute();
                return ($buscarEstudiante > 0);
            }
        } else {
            $sql = "INSERT INTO matricula.estudiante"
                . " (tdocumento_identidad, documento_identidad, nombres, apellidos, fecha_nacimiento, sexo, grado_actual_id, grado_anterior_id, representante_id, plantel_actual_id, plantel_anterior_id, usuario_ini_id, estatus, afinidad_id)"
                . " VALUES (:origen, :documento_identidad, :nombre, :apellido, :fecha_nacimiento, :sexo, :grado, :grado, :representante_id, :plantel_id, :plantel_id, :usuario_id, :estatus, :afinidad_id)";
            $busqueda = Yii::app()->db->createCommand($sql);
            $busqueda->bindParam(":representante_id", $buscarRepresentante, PDO::PARAM_STR);
            $busqueda->bindParam(":origen", $tdocumento_identidad, PDO::PARAM_STR);
            $busqueda->bindParam(":documento_identidad", $documento_identidad, PDO::PARAM_STR);
            $busqueda->bindParam(":nombre", $nombres, PDO::PARAM_STR);
            $busqueda->bindParam(":apellido", $apellidos, PDO::PARAM_STR);
            $busqueda->bindParam(":fecha_nacimiento", $fecha_nacimiento, PDO::PARAM_STR);
            $busqueda->bindParam(":sexo", $sexo, PDO::PARAM_STR);
            $busqueda->bindParam(":grado", $grado, PDO::PARAM_INT);
            $busqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
            $busqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
            $busqueda->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $busqueda->bindParam(":afinidad_id", $afinidad_id, PDO::PARAM_INT);
            $buscarEstudiante = $busqueda->execute();
            //    var_dump($buscarEstudiante);
            return ($buscarEstudiante > 0);
        }
    }

//Fin

    public function datosCompletosEstudiantes($estudiantes) {
        $estudiantesArray = implode(',', $estudiantes);
        if ($estudiantesArray != '') {
            $sql = "SELECT DISTINCT me.cedula_escolar, me.documento_identidad,me.tdocumento_identidad, me.nombres, me.apellidos ,(me.nombres || ' ' || me.apellidos) as nom_completo, mr.nombres AS representante, me.representante_id, me.id, p.nombre as plantel_nombre, p.cod_plantel, me.grado_actual_id as grado_id, me.fecha_nacimiento as fecha_nacimiento

                    FROM matricula.estudiante me
                    LEFT JOIN matricula.representante mr ON (me.representante_id = mr.id)
                    LEFT JOIN gplantel.plantel p on me.plantel_actual_id=p.id
                    LEFT JOIN gplantel.grado g ON (me.grado_actual_id = g.id)
                    WHERE me.id IN ($estudiantesArray)";
            $busqueda = Yii::app()->db->createCommand($sql);
            $estudiante = $busqueda->queryAll();
        } else {
            $estudiante = array();
        }
        return $estudiante;
    }


    public function estaMatriculadoDatos($estudiantes, $periodo_id){
        //$periodo_id=14;
        $estudiantesArray = implode(',', $estudiantes);
        if ($estudiantesArray != '') {
            $sql = " SELECT DISTINCT ie.estudiante_id, sp.plan_id, sp.id as seccion_plantel_id
                      FROM matricula.inscripcion_estudiante ie
                      INNER JOIN gplantel.seccion_plantel_periodo spp ON (spp.id = ie.seccion_plantel_periodo_id)
                      INNER JOIN gplantel.seccion_plantel sp ON (sp.id = spp.seccion_plantel_id)
                      WHERE ie.estudiante_id IN ($estudiantesArray)
                      AND ie.periodo_id=:periodo_id ";
            $busqueda = Yii::app()->db->createCommand($sql);
            $busqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
            $estudiante = $busqueda->queryAll();
        } else {
            $estudiante = array();
        }
        return $estudiante;
    }
}
