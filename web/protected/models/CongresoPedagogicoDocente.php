<?php

/**
 * This is the model class for table "gplantel.congreso_pedagogico_docente".
 *
 * The followings are the available columns in table 'gplantel.congreso_pedagogico_docente':
 * @property integer $id
 * @property integer $congreso_id
 * @property string $tdocumento_identidad
 * @property string $documento_identidad
 * @property string $nombres
 * @property string $apellidos
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property CongresoPedagogico $congreso
 */

/* EDITADO @usuario Pedro Chacon
   Fecha 09/04/2015
   funciones : datosInscritos, docentesInscritos, docentesInscritosVal */
class CongresoPedagogicoDocente extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'gplantel.congreso_pedagogico_docente';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('congreso_id, tdocumento_identidad, documento_identidad, nombres, apellidos, usuario_ini_id, fecha_ini', 'required'),
            array('congreso_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('tdocumento_identidad, estatus', 'length', 'max'=>1),
			array('documento_identidad', 'length', 'max'=>15),
			array('nombres, apellidos', 'length', 'max'=>60),
			array('fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, congreso_id, tdocumento_identidad, documento_identidad, nombres, apellidos, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'congreso' => array(self::BELONGS_TO, 'CongresoPedagogico.php', 'congreso_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'congreso_id' => 'Congreso',
			'tdocumento_identidad' => 'Tipo de Documento',
			'documento_identidad' => 'Documento de Identidad',
			'nombres' => 'Nombres',
			'apellidos' => 'Apellidos',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;


		$criteria->compare('id',$this->id);
		$criteria->compare('congreso_id',$this->congreso_id);
		$criteria->compare('tdocumento_identidad',$this->tdocumento_identidad,true);
		$criteria->compare('documento_identidad',$this->documento_identidad,true);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('apellidos',$this->apellidos,true);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchDocentesInscritos($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;


		
		$criteria->compare('congreso_id',$id);
		
		//return $this->findAll($criteria);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function datosInscritos($cedula, $tipo_documento){ //FUNCION QUE RETORNA LOS DATOS DEL DOCENTE ENVIADO SI ESTE ESTA REGISTRADO EN LA BASE DE DATOS DEL SAIME
		$sql = "select origen,cedula, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido
				from auditoria.saime 
				where cedula = :cedula
				and origen = :tipo_documento";
		$consulta = Yii::app()->db->createCommand($sql);
		$consulta->bindParam(":cedula", $cedula, PDO::PARAM_INT);
		$consulta->bindParam(":tipo_documento", $tipo_documento, PDO::PARAM_INT);
		$datos = $consulta->queryRow();
        return $datos;
	}

	public function docentesInscritos($id){ //FUNCION QUE RETORNA LOS DATOS DE TODOS LOS DOCENTES INSCRITOS EN EL CONGRESO SELECCIONADO

		$sql = "select documento_identidad, nombres, apellidos
				from gplantel.congreso_pedagogico_docente
				where congreso_id = :congreso";
		$consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":congreso", $id, PDO::PARAM_INT);
        $docentesInscritos = $consulta->queryAll();
        return $docentesInscritos;
	}

		public function docentesInscritosVal($id, $cedula){ // FUNCION QUE RETORNA LOS DATOS DEL DOCENTE, SI ESTE ESTA INSCRITO EN EL CONGRESO SELECCIONADO

		$sql = "select documento_identidad, nombres, apellidos
				from gplantel.congreso_pedagogico_docente
				where congreso_id = :congreso
				and documento_identidad = :cedula";
		$consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":congreso", $id, PDO::PARAM_INT);
        $consulta->bindParam(":cedula", $cedula, PDO::PARAM_INT);
        $docentesInscritos = $consulta->queryRow();
        return $docentesInscritos;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CongresoPedagogicoDocente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
