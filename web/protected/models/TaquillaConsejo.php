<?php

/**
 * This is the model class for table "gplantel.taquilla_consejo".
 *
 * The followings are the available columns in table 'gplantel.taquilla_consejo':
 * @property string $id
 * @property integer $estado_id
 * @property integer $municipio_id
 * @property integer $parroquia_id
 * @property integer $poblacion_id
 * @property integer $urbanizacion_id
 * @property integer $tipo_via_id
 * @property string $direccion
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property string $via
 *
 * The followings are the available model relations:
 * @property Estado $estado
 * @property Municipio $municipio
 * @property Parroquia $parroquia
 * @property Poblacion $poblacion
 * @property TipoVia $tipoVia
 * @property Urbanizacion $urbanizacion
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property ConsejoEducativo[] $consejoEducativos
 */
class TaquillaConsejo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gplantel.taquilla_consejo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('estado_id, municipio_id, parroquia_id, poblacion_id, urbanizacion_id, tipo_via_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('estado_id, municipio_id, parroquia_id, poblacion_id, urbanizacion_id, tipo_via_id, via, direccion','required'),
			array('estatus', 'length', 'max'=>1),
			array('via, direccion','filter','filter'=>'strtoupper'),
			array('via', 'length', 'max'=>70),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			//array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			//array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			array('direccion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, estado_id, municipio_id, parroquia_id, poblacion_id, urbanizacion_id, tipo_via_id, direccion, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus, via', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'estado' => array(self::BELONGS_TO, 'Estado', 'estado_id'),
			'municipio' => array(self::BELONGS_TO, 'Municipio', 'municipio_id'),
			'parroquia' => array(self::BELONGS_TO, 'Parroquia', 'parroquia_id'),
			'poblacion' => array(self::BELONGS_TO, 'Poblacion', 'poblacion_id'),
			'tipoVia' => array(self::BELONGS_TO, 'TipoVia', 'tipo_via_id'),
			'urbanizacion' => array(self::BELONGS_TO, 'Urbanizacion', 'urbanizacion_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'consejoEducativos' => array(self::HAS_MANY, 'ConsejoEducativo', 'taquilla_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'estado_id' => 'Estado',
			'municipio_id' => 'Municipio',
			'parroquia_id' => 'Parroquia',
			'poblacion_id' => 'Población',
			'urbanizacion_id' => 'Urbanización',
			'tipo_via_id' => 'Tipo Via',
			'direccion' => 'Dirección',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
			'via' => 'Via',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(strlen($this->id)>0) $criteria->compare('id',$this->id,true);
		if(is_numeric($this->estado_id)) $criteria->compare('estado_id',$this->estado_id);
		if(is_numeric($this->municipio_id)) $criteria->compare('municipio_id',$this->municipio_id);
		if(is_numeric($this->parroquia_id)) $criteria->compare('parroquia_id',$this->parroquia_id);
		if(is_numeric($this->poblacion_id)) $criteria->compare('poblacion_id',$this->poblacion_id);
		if(is_numeric($this->urbanizacion_id)) $criteria->compare('urbanizacion_id',$this->urbanizacion_id);
		if(is_numeric($this->tipo_via_id)) $criteria->compare('tipo_via_id',$this->tipo_via_id);
		if(strlen($this->direccion)>0) $criteria->compare('direccion',$this->direccion,true);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(Utiles::isValidDate($this->fecha_elim, 'y-m-d')) $criteria->compare('fecha_elim',$this->fecha_elim);
		// if(strlen($this->fecha_elim)>0) $criteria->compare('fecha_elim',$this->fecha_elim,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
		if(strlen($this->via)>0) $criteria->compare('via',$this->via,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
           	$this->fecha_elim = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TaquillaConsejo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function datosTaquilla(){

		$taquilla_id = $this->id;

		if(is_numeric($taquilla_id)){

			$sql ="SELECT 
				e.nombre as estado_nombre,
				m.nombre as municipio_nombre,
				par.nombre as parroquia_nombre,
				pob.nombre as poblacion_nombre,
				u.nombre as urbanizacion_nombre,
				tv.nb_tipo_via as tipo_via_nombre,
				t.direccion
			FROM
				gplantel.taquilla_consejo as t INNER JOIN estado as e ON t.estado_id = e.id
			INNER JOIN
				public.municipio as m ON t.municipio_id = m.id
			INNER JOIN
				public.parroquia as par ON t.parroquia_id = par.id
			INNER JOIN
				public.poblacion as pob ON t.poblacion_id = pob.id
			INNER JOIN
				public.urbanizacion as u ON t.urbanizacion_id = u.id
			INNER JOIN
				public.tipo_via as tv ON t.tipo_via_id = tv.id

			where t.id =:taquilla_id";
			$consulta = Yii::app()->db->createCommand($sql);
			$consulta->bindParam(":taquilla_id", $taquilla_id, PDO::PARAM_INT);
			$resultado = $consulta->queryRow();

			return $resultado;
		}
		
	}	
}