<?php

/**
 * This is the model class for table "servicio.jefe_departamento".
 *
 * The followings are the available columns in table 'servicio.jefe_departamento':
 * @property integer $id
 * @property integer $departamento_id
 * @property integer $usuario_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $usuario_ini
 * @property string $usuario_act
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuario
 * @property Departamento $departamento
 */
class JefeDepartamento extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'servicio.jefe_departamento';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('fecha_ini, usuario_ini', 'required'),
            array('departamento_id, usuario_id', 'numerical', 'integerOnly' => true),
            array('usuario_ini, usuario_act', 'length', 'max' => 20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, departamento_id, usuario_id, fecha_ini, fecha_act, usuario_ini, usuario_act', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'usuario' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_id'),
            'departamento' => array(self::BELONGS_TO, 'Departamento', 'departamento_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'departamento_id' => 'Departamento',
            'usuario_id' => 'Usuario',
            'fecha_ini' => 'Fecha Ini',
            'fecha_act' => 'Fecha Act',
            'usuario_ini' => 'Usuario Ini',
            'usuario_act' => 'Usuario Act',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        if (is_numeric($this->id)) {
            $criteria->compare('id', $this->id);
        }
        if (is_numeric($this->departamento_id)) {
            $criteria->compare('departamento_id', $this->departamento_id);
        }
        if (is_numeric($this->usuario_id)) {
            $criteria->compare('usuario_id', $this->usuario_id);
        }
        if (Utiles::isValidDate($this->fecha_ini, 'y-m-d')) {
            $criteria->compare('fecha_ini', $this->fecha_ini);
        }
        // if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
        if (Utiles::isValidDate($this->fecha_act, 'y-m-d')) {
            $criteria->compare('fecha_act', $this->fecha_act);
        }
        // if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
        if (strlen($this->usuario_ini) > 0) {
            $criteria->compare('usuario_ini', $this->usuario_ini, true);
        }
        if (strlen($this->usuario_act) > 0) {
            $criteria->compare('usuario_act', $this->usuario_act, true);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeInsert() {
        parent::beforeSave();
        $this->fecha_ini = date('Y-m-d H:i:s');
        $this->usuario_ini_id = Yii::app()->user->id;
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeUpdate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeDelete() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        // $this->fecha_eli = $this->fecha_act;
        $this->estatus = 'I';
        return true;
    }

    public function beforeActivate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        $this->estatus = 'A';
        return true;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return JefeDepartamento the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
