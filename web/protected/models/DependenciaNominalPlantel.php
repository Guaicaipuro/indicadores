<?php

/**
 * This is the model class for table "gplantel.dependencia_nominal_plantel".
 *
 * The followings are the available columns in table 'gplantel.dependencia_nominal_plantel':
 * @property integer $id
 * @property integer $dependencia_nominal
 * @property integer $plantel_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property Plantel $plantel
 * @property UsergroupsUser $usuarioIni
 */
class DependenciaNominalPlantel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gplantel.dependencia_nominal_plantel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dependencia_nominal, plantel_id, usuario_ini_id', 'required'),
                                                array('dependencia_nominal','unique'),
			array('dependencia_nominal, plantel_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('estatus', 'length', 'max'=>1),
			array('dependencia_nominal', 'validacionGeneralDependencia'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, dependencia_nominal, plantel_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dependencia_nominal' => 'Código de la Dependencia Nominal',
			'plantel_id' => 'Plantel',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}
        
         /*
          * Funcion que muestra el estado al cual pertenece el plantel para luego ser comparado con el estado de la dependencia Nominal. 
          */   
    public function extraerEstadoPlantel($plantelId) {
        
                $sql = "SELECT  p.estado_id FROM gplantel.plantel p where p.id = :id";
 
      
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":id", $plantelId, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();
        
       return $resultado;
    }


    public function extraerEstadoDependencia($dependencia) {
        
                $sql = "SELECT  p.estado_id FROM gplantel.dependencia_nominal_estado p where p.cod_dependencia=:id";
 
      
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":id", $dependencia, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();
        
       return $resultado;
    }

        
        
           /* ESTA FUNCION TE PERMITE VALIDAR SI EL CODIGO INGRESADO DE LA DEPENDENCIA NOMINAL EXISTE Y SI CORRESPONDE CON 
            * EL PLANTEL REGISTRADO EN LA TABLA "dependencia_nominal_estado"- Meylin */
    
        
    public function validarDependencia($dependencia) {
        
                $sql = "SELECT count (distinct d.id) as result FROM gplantel.dependencia_nominal_estado d WHERE d.cod_dependencia=:dependencia";
       
   
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":dependencia", $dependencia, PDO::PARAM_INT);
            $resultado = $consulta->queryRow();


        return $resultado;
    }
    
    //Permite verificar si el estado del plantel consultado corresponde al de la dependencia ingresada 

      public function validarEstadoDependencia($estado, $dependencia) {
        
                $sql = "SELECT count (distinct d.id) as result FROM gplantel.dependencia_nominal_estado d WHERE d.estado_id=:estado AND d.cod_dependencia=:dependencia";
       
   
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":estado", $estado, PDO::PARAM_INT);
            $consulta->bindParam(":dependencia", $dependencia, PDO::PARAM_INT);
            $resultado = $consulta->queryRow();


        return $resultado;
    }


   public function extraerDatosDependenciaPlantel($dependencia){
          
          	$sql = "SELECT nom_dependencia_nominal as nombre, id as dependencia_nominal_id FROM gplantel.dependencia_nominal_estado WHERE cod_dependencia=:dependencia";
       
   
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":dependencia", $dependencia, PDO::PARAM_INT);
            $resultado = $consulta->queryRow();


        return $resultado;

   }

   //Verifica si la dependencia no fue guardada anteriormente

    public function verificarRegistroDependenciaPlantel($dependencia,$plantelId){
   	
   			$sql = "SELECT  count (distinct gp.id) as result FROM gplantel.dependencia_nominal_plantel gp  WHERE gp.dependencia_nominal=:dependencia AND gp.plantel_id=:plantel";
       
   
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":dependencia", $dependencia, PDO::PARAM_INT);
            $consulta->bindParam(":plantel", $plantelId, PDO::PARAM_INT);
            $resultado = $consulta->queryRow();


        return $resultado;

    }

    //funcion que permite validar la existencia y la coincidencia de los estados desde la validacion del modelo

    public function validacionGeneralDependencia($dependencia, $params = null) {
       
       if ((isset($this->dependencia_nominal) AND (isset($this->plantel_id)))) {

       		$busqueda= $this->validarDependencia($this->dependencia_nominal);
       		$existe=(isset($busqueda['result']))?$busqueda['result']:0;
       		//var_dump($busqueda);die();
           	if ($existe==1){
       		
           $busqueda_estado= $this->extraerEstadoPlantel($this->plantel_id);
           $estado_plantel=(isset($busqueda_estado[0]['estado_id']))?$busqueda_estado[0]['estado_id']:NULL;
//var_dump($busqueda_estado);die();
           if($estado_plantel!=NULL){

           $validarEstadoDependencia= $this->validarEstadoDependencia($estado_plantel,$this->dependencia_nominal);
           $coincide=(isset($validarEstadoDependencia['result']))?$validarEstadoDependencia['result']:0;
           		if($coincide==0){

           			$this->addError($dependencia, 'El estado de la dependencia '.$this->dependencia_nominal.' no coincide con el <b>estado</b> del plantel.');
           		}
           }else{
           	$this->addError($dependencia, 'El plantel consultado no posee un estado asociado.');
           }

            }      	else{
                $this->addError($dependencia, 'La dependencia '.$this->dependencia_nominal.' no existe.');
            	}
     }  
            
    }
    

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dependencia_nominal',$this->dependencia_nominal);
		$criteria->compare('plantel_id',$this->plantel_id);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DependenciaNominalPlantel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}