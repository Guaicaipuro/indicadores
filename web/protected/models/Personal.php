<?php

/**
 * This is the model class for table "personal.personal".
 *
 * The followings are the available columns in table 'personal.personal':
 * @property integer $id
 * @property string $nombres
 * @property string $apellidos
 * @property string $tdocumento_identidad
 * @property string $documento_identidad
 * @property string $fecha_nacimiento
 * @property string $correo
 * @property string $telefono_fijo
 * @property string $telefono_celular
 * @property integer $usuario_id
 * @property integer $grado_instruccion_id
 * @property integer $especificacion_estatus_id
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property PersonalPlantel[] $personalPlantels
 * @property UsergroupsUser $usuarioAct
 * @property EspecificacionEstatus $especificaciónEstatus
 * @property GradoInstruccion $gradoInstruccion
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuario
 */
class Personal extends CActiveRecord
{
    public $plantel_id=null;
    public $tipo_personal_id=null;
    public $especialidad_id=null;
    public $errores="";
    CONST INDEX_REP_PERSONAL_GRAL='INDEX_REP_PERSONAL_GRAL';
    CONST INDEX_HORA_REP_PERSONAL_GRAL='INDEX_HORA_REP_PERSONAL_GRAL';
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'personal.personal';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(

            // validaciones que aplican al escenario de registro
            array('nombres, apellidos, tdocumento_identidad, documento_identidad,fecha_nacimiento,grado_instruccion_id, fecha_ini, usuario_ini_id,especificacion_estatus_id,sexo', 'required', 'message' => 'El campo: {attribute}, no debe estar vacio', 'on'=>'personalRegistro'),
            array('usuario_id, grado_instruccion_id, especificacion_estatus_id, usuario_ini_id, usuario_act_id,documento_identidad,telefono_fijo,telefono_celular', 'numerical', 'integerOnly'=>true,'message' => 'El campo: {attribute}, debe ser un valor numérico','on'=>'personalRegistro'),
            array('nombres, apellidos', 'length', 'max'=>60,'on'=>'personalRegistro'),
            array('tdocumento_identidad, estatus', 'length', 'max'=>1,'on'=>'personalRegistro'),
            array('documento_identidad', 'length', 'max'=>15,'on'=>'personalRegistro'),
            array('correo', 'length', 'max'=>150,'on'=>'personalRegistro'),
            array('correo', 'email','on'=>'personalRegistro'),
            array('telefono_fijo, telefono_celular', 'length','min'=>11, 'max'=>11, 'on'=>'personalRegistro'),

            // validaciones que aplican al escenario de registro de asignaturas a docentes
            array('nombres, apellidos, tdocumento_identidad, documento_identidad', 'required', 'message' => 'El campo: {attribute}, no debe estar vacio', 'on'=>'asignaturaDocente'),


            //array('fecha_act, fecha_elim', 'safe'),


            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            // para la busqueda
            array('id, nombres, apellidos, tdocumento_identidad, documento_identidad, correo, telefono_fijo, telefono_celular, usuario_id, grado_instruccion_id, especificacion_estatus_id, fecha_ini, usuario_ini_id, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'personalPlantels' => array(self::HAS_MANY, 'PersonalPlantel', 'personal_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'especificacionEstatus' => array(self::BELONGS_TO, 'EspecificacionEstatus', 'especificacion_estatus_id'),
            'gradoInstruccion' => array(self::BELONGS_TO, 'GradoInstruccion', 'grado_instruccion_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'usuario' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'tdocumento_identidad' => 'Tipo de Documento',
            'documento_identidad' => 'Documento de Identidad',
            'identificacion' => 'Documento de Identidad',
            'grado_instruccion_id' => 'Grado de Instrucción',
            'estatus_docente_id' => 'Estatus del Docente',
            'especificacion_estatus_id' => 'Especificacion del Estatus',
            'correo' => 'Correo Eléctronico',
            'telefono_fijo' => 'Teléfono Fijo',
            'telefono_celular' => 'Teléfono Celular',
            'tipo_personal_id' => 'Tipo de Personal',
            'especialidad_id' => 'Especialidad',
            'fecha_ini' => 'Fecha de Creación',
            'usuario_ini_id' => 'Creado Por',
            'usuario_act_id' => 'Actualizado Por',
            'fecha_act' => 'Fecha Actualización',
            'fecha_elim' => 'Fecha Eliminación',
            'estatus' => 'Estatus del Registro',
        );
    }

    public  function validarUnicidad($attribute,$params){
        $sql_extra = " AND id <> :id ";
        $tdocumento_identidad =$this->tdocumento_identidad;
        $documento_identidad =$this->documento_identidad;
        $id =$this->id;

        $sql = " SELECT COUNT(id) FROM personal.personal WHERE tdocumento_identidad = :tdocumento_identidad AND documento_identidad=:documento_identidad ";
        if(!is_null($this->id))
            $sql .= $sql_extra;
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":tdocumento_identidad", $tdocumento_identidad, PDO::PARAM_STR);
        $consulta->bindParam(":documento_identidad", $documento_identidad, PDO::PARAM_STR);
        if(!is_null($id))
            $consulta->bindParam(":id", $id, PDO::PARAM_INT);
        $resultado = $consulta->queryScalar();

        if($resultado > 0) {
            $this->addError('documento_identidad','Esta combinación ya existe enel sistema.');
            $this->addError('tdocumento_identidad',null);
        }
    }
    public function existeDocente($documentoIdentidad,$tDocumentoIdentidad){
        $resultadoCedula = null;
        if (in_array($tDocumentoIdentidad, array('V', 'E','P')) AND is_numeric($documentoIdentidad)) {
            $sql = "SELECT id"
                . " FROM personal.personal"
                . " WHERE "
                . " documento_identidad= :documento_identidad AND "
                . " tdocumento_identidad= :tdocumento_identidad ";

            $buqueda = Yii::app()->db->createCommand($sql);
            $buqueda->bindParam(":documento_identidad", $documentoIdentidad, PDO::PARAM_STR);
            $buqueda->bindParam(":tdocumento_identidad", $tDocumentoIdentidad, PDO::PARAM_STR);
            $resultadoCedula = $buqueda->queryScalar();
        }
        return $resultadoCedula;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('nombres',$this->nombres,true);
        $criteria->compare('apellidos',$this->apellidos,true);
        $criteria->compare('tdocumento_identidad',$this->tdocumento_identidad,true);
        $criteria->compare('documento_identidad',$this->documento_identidad,true);
        $criteria->compare('correo',$this->correo,true);
        $criteria->compare('telefono_fijo',$this->telefono_fijo,true);
        $criteria->compare('telefono_celular',$this->telefono_celular,true);
        $criteria->compare('usuario_id',$this->usuario_id);
        $criteria->compare('grado_instruccion_id',$this->grado_instruccion_id);
        $criteria->compare('especificacion_estatus_id',$this->especificacion_estatus_id);
        $criteria->compare('fecha_ini',$this->fecha_ini,true);
        $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        $criteria->compare('usuario_act_id',$this->usuario_act_id);
        $criteria->compare('fecha_act',$this->fecha_act,true);
        $criteria->compare('fecha_elim',$this->fecha_elim,true);
        $criteria->compare('estatus',$this->estatus,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Personal the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }




    public function getBuscarPersonal($tdocumento_identidad,$documento_identidad)
    {
        $criteria = new CDbCriteria();
        //$criteria->order = 'nombre ASC';
        $criteria->condition = " tdocumento_identidad='$tdocumento_identidad' and documento_identidad='$documento_identidad'  ";
        $resultado = Personal::model()->findAll($criteria);
        return $resultado;
    } // fin del metodo para buscar los datos del personal, esquema personal


    public function getValidarPersonaPorActualizar($id,$tdocumento_identidad,$documento_identidad)
    {
        $criteria = new CDbCriteria();
        //$criteria->order = 'nombre ASC';
        $criteria->condition = " id <> '$id' and tdocumento_identidad='$tdocumento_identidad' and documento_identidad='$documento_identidad'  ";
        $resultado = Personal::model()->findAll($criteria);
        return $resultado;
    } // fin del metodo para buscar los datos del personal, esquema personal


    public function getBuscarAreaComunPlantel($plantel_id)
    {
        $criteria = new CDbCriteria();
        //$criteria->order = 'nombre ASC';
        $criteria->condition = " plantel_id='$plantel_id' and estatus='".Constantes::ESTATUS_ACTIVO."' ";
        $resultado = AreaComunPlantel::model()->findAll($criteria);
        return $resultado;
    } // fin del metodo para buscar los datos del area comun del plantel

    public function getErroresAdicionales($erroresAdicionales)
    {


        $erroresAdicionales.="</br></br>".' <b>Nota: </b> En  caso de persistir el error o tiene alguna duda al respecto, por favor contacte al personal de soporte mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>';
        $this->addError('erroresAdicionales',$erroresAdicionales);



    } // fin de la funcion para mostrar los errores adicionales generados despues del metodo validate

    public function getNacionalidad()
    {

        $data=array(
            array( 'id'=>  Constantes::NAC_VENEZOLANO,'nombre'=>  Constantes::DESC_NAC_VENEZOLANO),
            array('id'=>  Constantes::NAC_EXTRANJERO, 'nombre'=>  Constantes::DESC_NAC_EXTRANJERO),
            array('id'=>  Constantes::NAC_PASAPORTE, 'nombre'=>  Constantes::DESC_NAC_PASAPORTE),
        );

        return $data;

    } // fin del metodo para retornar las nacionalidades

    public function getTipoPersonal()
    {

        $criteria = new CDbCriteria();
        $criteria->order = 'nombre ASC';
        $criteria->condition = "estatus='".Constantes::ESTATUS_ACTIVO."' ";
        $resultado = TipoPersonal::model()->findAll($criteria);
        return $resultado;

    } // fin de la funcion para obtener la lista de tipos de personal

    public function getGradoInstruccion()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'nombre ASC';
        $criteria->condition = "estatus='".Constantes::ESTATUS_ACTIVO."' ";
        $resultado = GradoInstruccion::model()->findAll($criteria);
        return $resultado;

    } // funcion para obtener la listas  del grado de instruccion

    public function getEstatusDocente()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'nombre ASC';
        $criteria->condition = "estatus='".Constantes::ESTATUS_ACTIVO."' ";
        $resultado = EstatusDocente::model()->findAll($criteria);
        return $resultado;

    } // fin de la funcion para obtener la lista del estatus de docente

    public function getEspecificacionEstatus()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'nombre ASC';
        $criteria->condition = "estatus='".Constantes::ESTATUS_ACTIVO."' ";
        $resultado = EspecificacionEstatus::model()->findAll($criteria);
        return $resultado;

    } // fin de la funcion para traer la lista de especificacion estatus


    public function getEspecialidad()
    {

        $criteria = new CDbCriteria();
        //$criteria->select = '*, esp_tp.fecha_ini';
        //$criteria->join= 'INNER JOIN personal.especialidad_tipo_personal esp_tp ON (esp_tp.especialidad_id = "t".id) ';
        $criteria->order = 'nombre ASC';
        //$criteria->condition = "estatus='".Constantes::ESTATUS_ACTIVO."' ";
        $resultado = Especialidad::model()->findAll($criteria);
        return $resultado;

    } // fin de la funcion para traer el listado de especialidad

    public function getFuncion()
    {

        $criteria = new CDbCriteria();
        $criteria->order = 'nombre ASC';
        $criteria->condition = "estatus='".Constantes::ESTATUS_ACTIVO."' ";
        $resultado = Funcion::model()->findAll($criteria);
        return $resultado;

    } // fin de la funcion para traer el listado de especialidad

    /**
     * Se utiliza para generar el reporte general del personal (administrativo,docente y obrero)
     * @author Ignacio Salazar
     * @return array|bool|mixed
     */
    public function reportePersonalGeneral()
    {
        $resultado = false;
        $resultado = Yii::app()->cache->get(self::INDEX_REP_PERSONAL_GRAL);
        if(!$resultado){
            $sql = "SELECT
                    e.nombre as estado,
                    COUNT(DISTINCT pp.personal_id) AS personal,
                    SUM(CASE WHEN (tp.id=1) THEN 1 ELSE 0 END) AS administrativo,
                    SUM(CASE WHEN (tp.id=3) THEN 1 ELSE 0 END) AS docente,
                    SUM(CASE WHEN (tp.id=2) THEN 1 ELSE 0 END) AS obrero
                    FROM personal.personal_plantel pp
                    INNER JOIN personal.tipo_personal tp ON (tp.id=pp.tipo_personal_id)
                    INNER JOIN gplantel.plantel p ON (p.id=pp.plantel_id)
                    INNER JOIN estado e ON (e.id=p.estado_id)
                    GROUP BY estado
                    ORDER BY estado
                    ";
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $resultado = $command->queryAll();

            if ($resultado !== array()){
                Yii::app()->cache->set(self::INDEX_REP_PERSONAL_GRAL,$resultado,43200);
                Yii::app()->cache->set(self::INDEX_HORA_REP_PERSONAL_GRAL,date('Y-m-d H:i:s'),43200);
            }
            else
                $resultado= false;
        }
        return $resultado;
    }

    /**
     * Retorna Fecha y Hora cuando se ejecutò el Reporte de Personal de Planteles, se utiliza para mostrar la informaciòn en la action (/control/personal/reporteGeneral)
     * @return mixed
     */
    public function getFechaReporteGeneral(){
        return Yii::app()->cache->get(self::INDEX_HORA_REP_PERSONAL_GRAL);
}


}


