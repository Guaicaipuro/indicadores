<?php

/**
 * This is the model class for table "gplantel.consejo_educativo".
 *
 * The followings are the available columns in table 'gplantel.consejo_educativo':
 * @property string $id
 * @property string $rif
 * @property string $fecha_registro
 * @property string $comite_economia
 * @property string $circuito_educativo
 * @property string $peic
 * @property integer $taquilla_id
 * @property integer $banco_id
 * @property integer $plantel_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $numero_cuenta
 *
 * The followings are the available model relations:
 * @property TaquillaConsejo $taquilla
 * @property Plantel $plantel
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property Banco $banco
 * @property ConsejoCategoria[] $consejoCategorias
 */
class ConsejoEducativo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gplantel.consejo_educativo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rif, fecha_registro, taquilla_id, plantel_id, comite_economia, circuito_educativo, numero_cuenta, banco_id', 'required'),
			array('taquilla_id, banco_id, plantel_id, usuario_ini_id, usuario_act_id, numero_cuenta', 'numerical', 'integerOnly'=>true),
			array('rif, numero_cuenta', 'unique'),
			array('plantel_id', 'unique','message'=>'El plantel ya posee un consejo educativo'),
			array('fecha_registro', 'date', 'format'=>'yyyy-MM-dd'),
			array('rif', 'length', 'is'=>11),
			array('numero_cuenta', 'length', 'is'=>20),
			array('comite_economia, circuito_educativo', 'length', 'max'=>2),
			array('comite_economia, circuito_educativo', 'in', 'range'=>array('SI', 'NO'), 'allowEmpty'=>false, 'strict'=>true,),
			array('peic','filter','filter'=>'strtoupper'),
			array('peic','filter','filter'=>'trim'),			
			array('peic', 'length', 'max'=>160),
			array('estatus', 'length', 'max'=>1),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			//array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			//array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, rif, fecha_registro, comite_economia, circuito_educativo, peic, taquilla_id, banco_id, plantel_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus, numero_cuenta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'taquilla' => array(self::BELONGS_TO, 'TaquillaConsejo', 'taquilla_id'),
			'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'banco' => array(self::BELONGS_TO, 'Banco', 'banco_id'),
			'consejoCategorias' => array(self::HAS_MANY, 'ConsejoCategoria', 'consejo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'rif' => 'RIF',
			'fecha_registro' => 'Última Fecha de Registro en Taquilla',
			'comite_economia' => 'Comite Economía Escolar',
			'circuito_educativo' => 'Pertenece a un Circuito Educativo Comunitario',
			'peic' => 'Proyecto Educativo Integral Comunitario (PEIC)',
			'taquilla_id' => 'Taquilla',
			'banco_id' => 'Banco',
			'plantel_id' => 'Plantel',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
			'numero_cuenta' => 'Número de Cuenta',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(isset($_REQUEST['Plantel'])){
			$groupId = Yii::app()->user->group;
       		$usuarioId = Yii::app()->user->id;
        	$userEstadoId = Yii::app()->user->estado;
			

			if(isset($_REQUEST['Plantel']['cod_estadistico']) and !empty($_REQUEST['Plantel']['cod_estadistico'])){

				$cod_estadistico = $_REQUEST['Plantel']['cod_estadistico'];
		        if (is_numeric($cod_estadistico)) {
		            if (strlen($cod_estadistico) < 15) {
		                $criteria->compare('p.cod_estadistico', $cod_estadistico);
		            }
		        }
			}

			if(isset($_REQUEST['Plantel']['cod_plantel']) and !empty($_REQUEST['Plantel']['cod_plantel'])){

				$cod_plantel = $_REQUEST['Plantel']['cod_plantel'];
	       		$criteria->addSearchCondition('p.cod_plantel', '%' . $cod_plantel . '%', false, 'AND', 'ILIKE');		   
			}

			if(isset($_REQUEST['Plantel']['nombre']) and !empty($_REQUEST['Plantel']['nombre'])){

				$nombre = $_REQUEST['Plantel']['nombre'];
	        	$criteria->addSearchCondition('p.nombre', '%' .$nombre . '%', false, 'AND', 'ILIKE');
			}

			if(isset($_REQUEST['Plantel']['denominacion_id']) and !empty($_REQUEST['Plantel']['denominacion_id'])){

				$denominacion_id = $_REQUEST['Plantel']['denominacion_id'];
		        
		        if (is_numeric($denominacion_id)) {
		            if (strlen($denominacion_id) < 10) {
		                $criteria->compare('p.denominacion_id', $denominacion_id);
		            }
		        }
			}

			if(isset($_REQUEST['Plantel']['tipo_dependencia_id']) and !empty($_REQUEST['Plantel']['tipo_dependencia_id'])){

				$tipo_dependencia_id = $_REQUEST['Plantel']['tipo_dependencia_id'];

		        if (is_numeric($tipo_dependencia_id)) {
		            if (strlen($tipo_dependencia_id) < 10) {
		                $criteria->compare('p.tipo_dependencia_id', $tipo_dependencia_id);
		            }
		        }
	        }

			if(isset($_REQUEST['Plantel']['estado_id']) and !empty($_REQUEST['Plantel']['estado_id'])){

				$estado_id = $_REQUEST['Plantel']['estado_id'];

		        if (is_numeric($estado_id) || $estado_id == '') {
		            if (strlen($estado_id) < 10) {
		                // Lógica de Permisología
		                if (in_array(Yii::app()->user->group, array(49, UserGroups::DIRECTOR, UserGroups::JEFE_DRCEE, UserGroups::ADMIN_DRCEE, UserGroups::DESARROLLADOR, UserGroups::root, UserGroups::ATENCIONTELEFONICA, UserGroups::MISION_RIBAS_NAC, UserGroups::FEDE_LECTURA))) {
		                    $criteria->compare('p.estado_id', $estado_id, false);
		                } else {
		                    $criteria->compare('p.estado_id', Yii::app()->user->estado, false);
		                }
		            }
		        }
	        }

			if(isset($_REQUEST['Plantel']['municipio_id']) and !empty($_REQUEST['Plantel']['municipio_id']))
			{
				$municipio_id = $_REQUEST['Plantel']['municipio_id'];

		        if (is_numeric($municipio_id)) {
		            if (strlen($municipio_id) < 10) {
		                $criteria->compare('p.municipio_id', $municipio_id);
		            }
		        }
	        }

			if(isset($_REQUEST['Plantel']['parroquia_id']) 
				and !empty($_REQUEST['Plantel']['parroquia_id']))
			{
				$parroquia_id = $_REQUEST['Plantel']['parroquia_id'];

		        if (is_numeric($parroquia_id)) {
		            if (strlen($parroquia_id) < 10) {
		                $criteria->compare('p.parroquia_id', $parroquia_id);
		            }
		        }
	        }

	        
			if(	isset($_REQUEST['Plantel']['estatus_plantel_id']) 
				and !empty($_REQUEST['Plantel']['estatus_plantel_id']))
			{
				$estatus_plantel_id = $_REQUEST['Plantel']['estatus_plantel_id'];
		        if (is_numeric($estatus_plantel_id)) {
		            if (strlen($estatus_plantel_id) < 10) {
		                $criteria->compare('p.estatus_plantel_id', $estatus_plantel_id);
		            }
		        }
	        }

			if(	isset($_REQUEST['Plantel']['modalidad_id']) 
				and !empty($_REQUEST['Plantel']['modalidad_id']))
			{
				$modalidad_id = $_REQUEST['Plantel']['modalidad_id'];
		        if (is_numeric($modalidad_id)) {
		            if (strlen($modalidad_id) < 10) {
		                $criteria->compare('p.modalidad_id', $modalidad_id);
		            }
		        }
	        }

			if(	isset($_REQUEST['Plantel']['zona_ubicacion_id']) 
				and !empty($_REQUEST['Plantel']['zona_ubicacion_id']))
			{
				$zona_ubicacion_id = $_REQUEST['Plantel']['zona_ubicacion_id'];
		        if (is_numeric($zona_ubicacion_id)) {
		            if (strlen($zona_ubicacion_id) < 10) {
		                $criteria->compare('p.zona_ubicacion_id', $zona_ubicacion_id);
		            }
		        }
	        }

	        /**
	         * La variable "p" representa "gplantel.plantel"
	         * La variable "t" representa "gplantel.consejo_educativo"
	         */
	        $criteria->join = 'LEFT JOIN gplantel.plantel as p ON t.plantel_id = p.id';
	        $criteria->join .= ' LEFT JOIN gplantel.estatus_plantel ON p.estatus_plantel_id = gplantel.estatus_plantel.id ';
	        $criteria->join .= 'LEFT JOIN gplantel.tipo_dependencia ON p.tipo_dependencia_id = gplantel.tipo_dependencia.id ';
	        $criteria->join .= 'LEFT JOIN estado ON p.estado_id = estado.id ';
	        $criteria->join .= 'LEFT JOIN municipio ON p.municipio_id = municipio.id ';

	       /**
	         * Si el usuario es coordinador de la zona educativa (25)
	         */
	        if ($groupId == 25) {
	            $criteria->join .= " LEFT JOIN gplantel.autoridad_zona_educativa ON p.zona_educativa_id = gplantel.autoridad_zona_educativa.zona_educativa_id ";
	            $criteria->addCondition("gplantel.autoridad_zona_educativa.usuario_id = :usuario");
	            $criteria->params = array_merge($criteria->params, array(':usuario' => (int) $usuarioId));
	        }

	        /**
	         * Si el usuario es coordinador de control de estudio y evaluacion de plantel (26)
	         * Si el usuario es Director (29)
	         * Si el usuario ees secretaria o transcriptor
	         */
	        if (($groupId == 26) || ($groupId == 29) || ($groupId == 30) || ($groupId == 49)) {
	            $criteria->join .= " LEFT JOIN gplantel.autoridad_plantel ON p.id = gplantel.autoridad_plantel.plantel_id ";
	            $criteria->addCondition("gplantel.autoridad_plantel.usuario_id = :usuario AND gplantel.autoridad_plantel.estatus = 'A'");
	            $criteria->params = array_merge($criteria->params, array(':usuario' => (int) $usuarioId));

	            //$criteria->bindParam(":usuario", $usuarioId, PDO::PARAM_INT);
	        }

	        if ($groupId == UserGroups::MISION_RIBAS_NAC) {
	            $criteria->join .= " LEFT JOIN gplantel.denominacion d ON d.id = p.denominacion_id ";
	            $criteria->addCondition("d.id = :id");
	            $criteria->params = array_merge($criteria->params, array(':id' => UserGroups::DENOMINACION_ID));
	        }

	        if ($groupId == UserGroups::MISION_RIBAS_REG) {

	            $criteria->join .= " LEFT JOIN gplantel.denominacion d ON d.id = p.denominacion_id ";
	            $criteria->addCondition('p.estado_id = :estado_id');
	            $criteria->addCondition("d.id = :id");
	            $criteria->params = array_merge($criteria->params, array(':id' => UserGroups::DENOMINACION_ID));
	            $criteria->params = array_merge($criteria->params, array(':estado_id' => $userEstadoId));
	        }

	        /* BUSQUEDA POR CEDULA DEL DIRECTOR */
	        if (isset($_REQUEST['Plantel']['cedula_director'])) {
	            $cedulaDirectorSearch = $_REQUEST['Plantel']['cedula_director'];
	            if (is_numeric($cedulaDirectorSearch)) {
	                if (strlen($cedulaDirectorSearch) < 10) {
	                    $criteria->join .= ' INNER JOIN gplantel.autoridad_plantel ON p.id = gplantel.autoridad_plantel.plantel_id ';
	                    $criteria->join .= ' INNER JOIN seguridad.usergroups_user ON seguridad.usergroups_user.id =  gplantel.autoridad_plantel.usuario_id ';
	                    $criteria->addCondition("seguridad.usergroups_user.cedula = :cedula AND seguridad.usergroups_user.group_id = 29 AND gplantel.autoridad_plantel.estatus = 'A'");
	                    $criteria->params = array_merge($criteria->params, array(':cedula' => (int) $cedulaDirectorSearch));
	                }
	            }
	        }

	        /* BUSQUEDA SI POSEE POLIGONAL DE GEOREFERENCIACION (LATITUD Y LONGITUD) */

	        if( isset($_REQUEST['Plantel']['poligonal']) && !empty($_REQUEST['Plantel']['poligonal']) ){

	        	$poligonal = $_REQUEST['Plantel']['poligonal'];

	        	if( $poligonal =='SI'){

	        		$criteria->addCondition("p.longitud IS NOT NULL AND p.longitud !=0 AND p.latitud IS NOT NULL AND p.latitud !=0");
	        	}elseif ($poligonal =='NO') {
	        		
	        		$criteria->addCondition("p.longitud IS NULL OR p.longitud =0 OR p.latitud IS NULL OR p.latitud =0");
	        	}
	        }
		}
		if( $this->fecha_registro =='A'){

			$mktime = mktime(0, 0, 0, date("m")  , date("d"), date("Y")-1);
			$fecha_registro = date('Y-m-d',$mktime);
			$criteria->addCondition("t.fecha_registro >= :fecha_registro");
			$criteria->params = array_merge($criteria->params, array(':fecha_registro' => (string) $fecha_registro));
		}else if($this->fecha_registro =='N'){

			$mktime = mktime(0, 0, 0, date("m")  , date("d"), date("Y")-1);
			$fecha_registro = date('Y-m-d',$mktime);
			$criteria->addCondition("t.fecha_registro < :fecha_registro");
			$criteria->params = array_merge($criteria->params, array(':fecha_registro' => (string) $fecha_registro));
		}


		if(strlen($this->id)>0) $criteria->compare('id',$this->id,true);
		if(strlen($this->rif)>0) $criteria->compare('rif',$this->rif,true);
		//if(Utiles::isValidDate($this->fecha_registro, 'y-m-d')) $criteria->compare('fecha_registro',$this->fecha_registro);
		//if(strlen($this->fecha_registro)>0) $criteria->compare('fecha_registro','2014-01-01'/*$this->fecha_registro*/,false);
		if(strlen($this->comite_economia)>0) $criteria->compare('comite_economia',$this->comite_economia,true);
		if(strlen($this->circuito_educativo)>0) $criteria->compare('circuito_educativo',$this->circuito_educativo,true);
		if(strlen($this->peic)>0) $criteria->compare('peic',$this->peic,true);
		if(is_numeric($this->taquilla_id)) $criteria->compare('taquilla_id',$this->taquilla_id);
		if(is_numeric($this->banco_id)) $criteria->compare('banco_id',$this->banco_id);
		if(is_numeric($this->plantel_id)) $criteria->compare('plantel_id',$this->plantel_id);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		//if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		//if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		//if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		//if(Utiles::isValidDate($this->fecha_elim, 'y-m-d')) $criteria->compare('fecha_elim',$this->fecha_elim);
		// if(strlen($this->fecha_elim)>0) $criteria->compare('fecha_elim',$this->fecha_elim,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('t.estatus',$this->estatus,true);
		if(is_numeric($this->numero_cuenta)) $criteria->compare('numero_cuenta',$this->numero_cuenta);
		
		$sort = new CSort();
        $sort->defaultOrder = 'fecha_registro DESC';

		Yii::app()->session['dataProvider'] = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>false,
		));
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_ini = null;
            $this->usuario_ini_id = null;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->fecha_elim = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->fecha_elim = null;
            $this->estatus = 'A';
            return true;
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConsejoEducativo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function obtenerPoblacion($parroquia) {

        $sql = "SELECT DISTINCT poblacion.nombre as nombre ,poblacion.id as id
            from poblacion
            inner join sector on poblacion.id=sector.poblacion_id
            where sector.parroquia_id=:parroquia order by nombre ASC";
        $commandBusqueda = $this->getDbConnection()->createCommand($sql);
        $commandBusqueda->bindParam(':parroquia', $parroquia, PDO::PARAM_INT);
        $result = $commandBusqueda->queryAll();
        return $result;
    }

    public function obtenerUrbanizacion($parroquia) {

        $sql = "SELECT DISTINCT urbanizacion.nombre as nombre ,urbanizacion.id as id
            from urbanizacion
            inner join sector_urbanizacion on urbanizacion.id=sector_urbanizacion.urbanizacion_id
            inner join sector on sector.id=sector_urbanizacion.sector_id
            where sector.parroquia_id=:parroquia order by nombre ASC";
        $commandBusqueda = $this->getDbConnection()->createCommand($sql);
        $commandBusqueda->bindParam(':parroquia', $parroquia, PDO::PARAM_INT);
        $result = $commandBusqueda->queryAll(); // Devuelve 1 si encuentra el registro, sino no lo encuentra devuelve 0
        // var_dump($result);die();
        return $result;
    }

    public function obtenerTipoVia() {

        $sql = "SELECT DISTINCT
              tv.nb_tipo_via as nombre,
              tv.id as id
            FROM
              public.tipo_via tv
            ORDER BY tv.nb_tipo_via ASC;";
        $commandBusqueda = $this->getDbConnection()->createCommand($sql);
        //$commandBusqueda->bindParam('parroquia', $parroquia, PDO::PARAM_INT);
        $result = $commandBusqueda->queryAll(); // Devuelve 1 si encuentra el registro, sino no lo encuentra devuelve 0
        // var_dump($result);die();
        return $result;
    }


	public function existeCategoria($consejo_id,$categoria_id){

		if(is_numeric($consejo_id) AND is_numeric($categoria_id)){

			$sql = "SELECT id FROM  gplantel.consejo_categoria
	        WHERE consejo_id =:consejo_id AND categoria_id =:categoria_id";
	        $consulta = Yii::app()->db->createCommand($sql);
	        $consulta->bindParam(":consejo_id", $consejo_id, PDO::PARAM_INT);
	        $consulta->bindParam(":categoria_id", $categoria_id, PDO::PARAM_INT);
	        $resultado = $consulta->queryRow();

	        return $resultado;
		}
	}

	public function categoriaSeleccionada($consejo_id){


		if(is_numeric($consejo_id)){

			$sql = "SELECT categoria_id as id FROM  gplantel.consejo_categoria
	        WHERE consejo_id =:consejo_id AND estatus='A'";
	        $consulta = Yii::app()->db->createCommand($sql);
	        $consulta->bindParam(":consejo_id", $consejo_id, PDO::PARAM_INT);
	        $resultado = $consulta->queryAll();

	        return $resultado;
		}
	}
	
	/******************FUNCION PARA VERIFICAR SI EL PLANTEL YA POSEE UN CONSEJO EDUCATIVO*/
	public function plantelHasConsejo($plantel_id){

		if(is_numeric($plantel_id)){

			$sql = "SELECT id FROM  gplantel.consejo_educativo
	        WHERE plantel_id =:plantel_id";
	        $consulta = Yii::app()->db->createCommand($sql);
	        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
	        $resultado = $consulta->queryRow();
	        return $resultado;
		}
	}

	// funcion para buscar los proyectos y categorias que posee un consejo educativo
	public function proyectosAndCategorias(){

		$consejo_id = $this->id;

		if( is_numeric($consejo_id)){

			$sql = "SELECT
					pc.nombre,
				  string_agg(DISTINCT ctg.nombre, ';') as nombre_categoria
				FROM 
					gplantel.consejo_educativo as ce INNER JOIN gplantel.consejo_categoria as c_cat ON ce.id = c_cat.consejo_id
					INNER JOIN gplantel.categoria_consejo as ctg ON ctg.id = c_cat.categoria_id
					INNER JOIN gplantel.proyecto_consejo as pc ON pc.id = ctg.proyecto_id
				WHERE  
					ctg.estatus='A'
				AND
					c_cat.estatus='A'
				AND	
					ce.id =:consejo_id
				GROUP BY pc.nombre";
			
			$consulta = Yii::app()->db->createCommand($sql);
			$consulta->bindParam(":consejo_id", $consejo_id, PDO::PARAM_INT);
			$resultado = $consulta->queryAll();
			
			return $resultado;
		}
	}

	public function estadisticasPorEstados($estado){

		/* SI $estado CONTIENE ALGUN VALOR SE REALIZA LA BUSQUEDA CON LOS MUNICIPIOS DE ESE ESTADO
		DE LO CONTRARIO SE HACE LA BUSQUEDA CON TODOS LOS ESTADOS DE VENEZUELA*/

		if( empty($estado) ||  is_null($estado) ){
			$tabla = 'public.estado';
			$atributo = 'estado_id';
		}else{
			$tabla = 'public.municipio';
			$atributo = 'municipio_id';
			$estado_id = $estado;
		}

        $mktime = mktime(0, 0, 0, date("m")  , date("d"), date("Y")-1);
        $fecha_actual = date('Y-m-d',$mktime);
		$sql ="SELECT 
				e.id, e.nombre,

				SUM (CASE WHEN (p.estatus='A')THEN 1 ELSE 0 END) as total_planteles,

				SUM (CASE WHEN (ce.estatus='A')THEN 1 ELSE 0 END) as total_consejos,
				
				SUM (CASE WHEN (:fecha_actual<= ce.fecha_registro AND ce.estatus='A')THEN 1 ELSE 0 END) AS cant_actualizados,
				
				SUM (CASE WHEN (:fecha_actual> ce.fecha_registro AND ce.estatus='A')THEN 1 ELSE 0 END) AS cant_desactualizados,

				SUM (CASE WHEN (ce.circuito_educativo ='SI' AND ce.estatus='A')THEN 1 ELSE 0 END) AS circuito_educativo,
				
				SUM (CASE WHEN (ce.comite_economia ='SI' AND ce.estatus='A')THEN 1 ELSE 0 END) AS comite_economia,

				(SELECT 
					COUNT(DISTINCT consejo_id) 
				FROM 
					gplantel.plantel AS pi 
					INNER JOIN gplantel.consejo_educativo AS cei ON pi.id = cei.plantel_id 
					INNER JOIN gplantel.consejo_categoria AS cc ON cei.id = cc.consejo_id 
				
				WHERE 
					e.id = pi.estado_id 
					AND cc.estatus='A'

				) AS con_proyectos

			FROM 
				$tabla AS e 
				LEFT JOIN gplantel.plantel AS p ON e.id = p.$atributo
				LEFT JOIN gplantel.consejo_educativo AS ce ON p.id = ce.plantel_id";

		if(isset($estado_id) and !empty($estado_id)){
			
			$sql .= " WHERE  e.estado_id=:estado_id";
		}

		$sql .="
			group by e.nombre, e.id
			order by e.nombre asc;";

		$consulta = Yii::app()->db->createCommand($sql);
		$consulta->bindParam(":fecha_actual", $fecha_actual, PDO::PARAM_STR);
		
		if(isset($estado_id) and !empty($estado_id)){

			$consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_STR);
		}
		
		$resultado = $consulta->queryAll();


		Yii::app()->session['dataProviderEstados'] = new CArrayDataProvider($resultado,array('pagination'=>false));

		return new 	CArrayDataProvider($resultado,array('pagination'=>false));
	}
    

	/* FUNCION PARA DETERMINAR CUANTOS CONSEJOS EDUCATIVOS POSEEN CADA UNO DE LOS PROYECTOS */
	public function estadisticasProyectosPorEstados($estado){

		/* SI $_REQUEST['estado'] CONTIENE ALGUN VALOR SE REALIZA LA BUSQUEDA CON LOS MUNICIPIOS DE ESE ESTADO
		DE LO CONTRARIO SE HACE LA BUSQUEDA CON TODOS LOS ESTADOS DE VENEZUELA*/

		$proyectos = ProyectoConsejo::model()->findAll(array('condition'=>"estatus='A'"));

		if( empty($estado) ||  is_null($estado) ){
			$tabla = 'public.estado';
			$atributo = 'estado_id';
		}else{
			$tabla = 'public.municipio';
			$atributo = 'municipio_id';
			$estado_id = $estado;
		}

		$sql ="SELECT 
				e.id, e.nombre,

				SUM (CASE WHEN (ce.estatus='A')THEN 1 ELSE 0 END) as total_consejos,				
				
				(SELECT 
					COUNT(DISTINCT consejo_id) 
				FROM 
					gplantel.plantel AS pi 
					INNER JOIN gplantel.consejo_educativo AS cei ON pi.id = cei.plantel_id 
					INNER JOIN gplantel.consejo_categoria AS cc ON cei.id = cc.consejo_id 
				
				WHERE 
					e.id = pi.estado_id 
					AND cc.estatus='A'

				) AS con_proyectos";
				
		foreach ($proyectos as $key => $datos) {
			

		$sql.=",(SELECT 
					COUNT(DISTINCT consejo_id) 

				FROM 
					gplantel.plantel AS pi 
					INNER JOIN gplantel.consejo_educativo AS cei ON pi.id = cei.plantel_id 
					INNER JOIN gplantel.consejo_categoria AS cc ON cei.id = cc.consejo_id 
					INNER JOIN gplantel.categoria_consejo as cat_c ON cc.categoria_id = cat_c.id 

				WHERE 
					e.id = pi.$atributo 
					AND cc.estatus='A' 
					AND cat_c.proyecto_id =". $datos['id']." 

				) AS c".$datos['id'];
		}


		$sql.=" FROM 
				$tabla AS e 
				LEFT JOIN gplantel.plantel AS p ON e.id = p.$atributo
				LEFT JOIN gplantel.consejo_educativo AS ce ON p.id = ce.plantel_id";

		if(isset($estado_id) and !empty($estado_id)){
			
			$sql .= " WHERE  e.estado_id=:estado_id";
		}

		$sql .="
			group by e.nombre, e.id
			order by e.nombre asc;";

		$consulta = Yii::app()->db->createCommand($sql);
		
		if(isset($estado_id) and !empty($estado_id)){

			$consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_STR);
		}

		$resultado = $consulta->queryAll();


		Yii::app()->session['dataProviderProyectosPorEstados'] = new CArrayDataProvider($resultado,array('pagination'=>false));

		return new 	CArrayDataProvider($resultado,array('pagination'=>false));
	}


	// /* FUNCION PARA CALCULAR LOS TOTALES( EL FOOTER DEL CGRIDVIEW) DE LAS ESTADISTICAS POR ESTADO*/
 //    public function totalPorColumna($data,$columna){
 //    	/* $data es lo que se obtiene de $model->estadisticasPorEstado()*/
 //    	 $columna CONTIENE EL NOMBRE DE LA COLUMNA A LA QUE SE QUIERE OBTENER EL TOTAL
 //    	$total_columna = 0;
    	
 //    	foreach ($data->rawData as $key => $value) {
 //    		/* rawData ES EL ARRAY QUE CONTIENE LOS VALORES DE LA CONSULTA*/
	// 	$total_columna += $value[$columna];
 //    	}
 //        return $total_columna;
 //    } 

    /*
    	FUNCION PARA OBTENER LOS ESTUDIANTES INSCRITOS EN UN PLANTEL Y PERIODO ESPECIFICO
    	DIVIDIDOS POR SEXO	
    */
    public function matriculaEstudiante($plantel_id,$periodo_id){

    	if(!empty($plantel_id) && is_numeric($plantel_id) && !empty($periodo_id) && is_numeric($periodo_id) ){

	    	$sql ="SELECT count(DISTINCT ie.estudiante_id) AS TOTAL, 
				SUM (CASE WHEN(e.sexo ='M' AND e.estatus = 'A')THEN 1 ELSE 0 END) AS m,
				SUM (CASE WHEN(e.sexo ='F' AND e.estatus = 'A')THEN 1 ELSE 0 END) AS f,
				SUM (CASE WHEN( (e.sexo IS NULL OR  e.sexo = '' ) AND e.estatus = 'A')THEN 1 ELSE 0 END) AS sin_sexo
				FROM matricula.inscripcion_estudiante AS ie
				INNER JOIN matricula.estudiante AS e ON ie.estudiante_id = e.id
				WHERE ie.periodo_id = :periodo_id
				AND ie.plantel_id = :plantel_id
				AND  ie.estatus = 'A';";

			$consulta = Yii::app()->db->createCommand($sql);
			$consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
			$consulta->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
			$resultado = $consulta->queryRow();
			return $resultado;
    	}else{

    		return false;	
    	}
    }
}
