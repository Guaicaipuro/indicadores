<?php

/**
 * This is the model class for table "matricula.constancia".
 *
 * The followings are the available columns in table 'matricula.constancia':
 * @property integer $id
 * @property string $codigo_qr
 * @property string $url
 * @property integer $inscripcion_estudiante_id
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $tipo_constancia_id
 * @property string $nombre_archivo
 * @property string $nombre_estudiante
 * @property string $apellido_estudiante
 * @property string $nombre_plantel
 * @property string $codigo_estadistico
 * @property string $nombre_seccion
 * @property string $nombre_grado
 * @property integer $documento_identidad
 * @property string $nomnbre_estado
 * @property string $nombre_capital
 * @property string $nombre_nivel
 * @property string $fecha_nacimiento
 * @property string $estado_nac
 * @property string $estado_plantel
 * @property string $municipio_plantel
 * @property string $parroquia_plantel
 * @property string $periodo_escolar
 * @property string $direccion_plantel
 * @property string $zona_educativa
 *
 * The followings are the available model relations:
 * @property TipoConstancia $tipoConstancia
 * @property InscripcionEstudiante $inscripcionEstudiante
 */
class Constancia extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'matricula.constancia';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('inscripcion_estudiante_id, usuario_ini_id, usuario_act_id, tipo_constancia_id, documento_identidad', 'numerical', 'integerOnly'=>true),
            array('codigo_qr, url', 'length', 'max'=>400),
            array('fecha_ini, fecha_act, fecha_elim', 'length', 'max'=>400),
            array('estatus, nombre_seccion', 'length', 'max'=>1),
            array('nombre_archivo, nombre_grado, nombre_estado, nombre_capital', 'length', 'max'=>400),
            array('nombre_estudiante, apellido_estudiante, nombre_nivel', 'length', 'max'=>400),
            array('nombre_plantel, codigo_estadistico', 'length', 'max'=>400),
            array('fecha_nacimiento', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, codigo_qr, url, inscripcion_estudiante_id, usuario_ini_id, usuario_act_id, fecha_ini, fecha_act, fecha_elim, estatus, tipo_constancia_id, nombre_archivo, nombre_estudiante, apellido_estudiante, nombre_plantel, codigo_estadistico, nombre_seccion, nombre_grado, documento_identidad, nomnbre_estado, nombre_capital, nombre_nivel, fecha_nacimiento', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'tipoConstancia' => array(self::BELONGS_TO, 'TipoConstancia', 'tipo_constancia_id'),
            'inscripcionEstudiante' => array(self::BELONGS_TO, 'InscripcionEstudiante', 'inscripcion_estudiante_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'codigo_qr' => 'Codigo Qr',
            'url' => 'Url',
            'inscripcion_estudiante_id' => 'Inscripcion Estudiante',
            'usuario_ini_id' => 'Usuario Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_ini' => 'Fecha Ini',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'tipo_constancia_id' => 'Tipo Constancia',
            'nombre_archivo' => 'Nombre Archivo',
            'nombre_estudiante' => 'Nombre Estudiante',
            'apellido_estudiante' => 'Apellido Estudiante',
            'nombre_plantel' => 'Nombre Plantel',
            'codigo_estadistico' => 'Codigo Estadistico',
            'nombre_seccion' => 'Nombre Seccion',
            'nombre_grado' => 'Nombre Grado',
            'documento_identidad' => 'Documento Identidad',
            'nombre_estado' => 'Nomnbre Estado',
            'nombre_capital' => 'Nombre Capital',
            'nombre_nivel' => 'Nombre Nivel',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'estado_nac' => 'Estado de Nacimiento',
            'direccion_plantel' => 'Dirección del Plantel',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('codigo_qr',$this->codigo_qr,true);
        $criteria->compare('url',$this->url,true);
        $criteria->compare('inscripcion_estudiante_id',$this->inscripcion_estudiante_id);
        $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        $criteria->compare('usuario_act_id',$this->usuario_act_id);
        $criteria->compare('fecha_ini',$this->fecha_ini,true);
        $criteria->compare('fecha_act',$this->fecha_act,true);
        $criteria->compare('fecha_elim',$this->fecha_elim,true);
        $criteria->compare('estatus',$this->estatus,true);
        $criteria->compare('tipo_constancia_id',$this->tipo_constancia_id);
        $criteria->compare('nombre_archivo',$this->nombre_archivo,true);
        $criteria->compare('nombre_estudiante',$this->nombre_estudiante,true);
        $criteria->compare('apellido_estudiante',$this->apellido_estudiante,true);
        $criteria->compare('nombre_plantel',$this->nombre_plantel,true);
        $criteria->compare('codigo_estadistico',$this->codigo_estadistico,true);
        $criteria->compare('nombre_seccion',$this->nombre_seccion,true);
        $criteria->compare('nombre_grado',$this->nombre_grado,true);
        $criteria->compare('documento_identidad',$this->documento_identidad);
        $criteria->compare('nombre_estado',$this->nomnbre_estado,true);
        $criteria->compare('nombre_capital',$this->nombre_capital,true);
        $criteria->compare('nombre_nivel',$this->nombre_nivel,true);
        $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Constancia the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    public function buscarEstudiante($cedula, $nacionalidad,$periodo_id) {

        $sql = "select
                ie.id as id_inscripcion,
                e.id as estudiante_id,
                e.nombres as nombre_estudiante,
                e.apellidos as apellido_estudiante,
                p.nombre as nombre_plantel,
                p.cod_estadistico as codigo_estadistico,
                s.nombre as nombre_seccion,
                g.nombre as nombre_grado,
                e.documento_identidad,
                e.cedula_escolar,
                e.tdocumento_identidad,
                est.nombre as estado,
                est.capital as capital,
                niv.nombre as nivel,fecha_nacimiento
                from matricula.estudiante e
                inner join matricula.inscripcion_estudiante ie on ie.estudiante_id=e.id
                inner join gplantel.plantel p on p.id=ie.plantel_id
                inner join gplantel.seccion_plantel_periodo spp on spp.id = ie.seccion_plantel_periodo_id
                inner join gplantel.seccion_plantel sp on sp.id =spp.seccion_plantel_id
                inner join gplantel.grado g on g.id=sp.grado_id
                inner join gplantel.seccion s on sp.seccion_id = s.id
                INNER JOIN gplantel.nivel niv ON (niv.id=sp.nivel_id)
                inner join public.estado est on p.estado_id=est.id
                WHERE 1=1 AND ie.periodo_id=:periodo_id";
        if(!is_null($nacionalidad) AND $nacionalidad!='')
        {
            $sql.=" AND e.documento_identidad=:cedula AND (e.nacionalidad=:nacionalidad OR e.tdocumento_identidad=:nacionalidad)";
        }else
        {
            $sql.= " AND e.cedula_escolar= :cedulaEscolar";
        }
        //echo "<pre> $sql </pre>";
        $buqueda = Yii::app()->db->createCommand($sql);

        if(!is_null($nacionalidad) AND $nacionalidad!='')
        {
            $buqueda->bindParam(":cedula", $cedula, PDO::PARAM_STR);
            $buqueda->bindParam(":nacionalidad", $nacionalidad, PDO::PARAM_STR);
        }else
        {
            $buqueda->bindParam(":cedulaEscolar", $cedula, PDO::PARAM_INT);
        }
        $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
        $resultadoCedula = $buqueda->queryRow();
        return $resultadoCedula;
    }

    public function insertarConstancia($row) {
        $usuario_id = Yii::app()->user->id;
        $fecha=date('Y-m-d');
        $sql = "insert into matricula.constancia(codigo_qr,url,inscripcion_estudiante_id,fecha_ini) values('".$row['codigo_qr']."','".$row['url']."','".$row['inscripcion_estudiante_id']."','".$fecha."')";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":codigo_qr", $row['codigo_qr'], PDO::PARAM_INT);
        $consulta->bindParam(":codigo_qr", $row['url'], PDO::PARAM_STR);
        $consulta->bindParam(":inscripcion_estudiante_id", $row['inscripcion_estudiante_id'], PDO::PARAM_INT);
        //$consulta->bindParam(":usuario_ini_id", $usuario_id, PDO::PARAM_INT);
        $consulta->bindParam(":fecha_ini", $fecha, PDO::PARAM_STR);
        $resultado = $consulta->execute();
        return $resultado;
    }
    public function verificarQr($codigo_verificacion){
        $resultado="";
        $sql="
            SELECT c.*, tp.nombre as documento
            FROM matricula.constancia c
            INNER JOIN matricula.tipo_constancia tp ON (tp.id=c.tipo_constancia_id)
            WHERE c.codigo_qr=:codigo_qr";
        //echo "<pre>   $sql </pre>"; die();
        $consulta=Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":codigo_qr", $codigo_verificacion,PDO::PARAM_STR);
        $resultado=$consulta->queryRow();
        return $resultado;
    }

    public function verificarQrCedula($cedula,$tipoConstancia){

        $largoCedula=strlen($cedula);

        if($largoCedula>9){
            $cedulaEscolar= $cedula;
        }

        $resultado="";
        $sql="select c.id, c.codigo_qr, c.url, c.nombre_estudiante,c.apellido_estudiante,c.nombre_plantel,
            c.codigo_estadistico,c.nombre_seccion,c.nombre_grado,c.documento_identidad,
             c.nombre_estado,c.nombre_capital,c.nombre_nivel,c.fecha_nacimiento
            from matricula.constancia c where 1=1 AND c.tipo_constancia_id=:tipo_contancia ";
        if(isset($cedulaEscolar)){
            $sql.= " AND c.cedula_escolar= :cedulaEscolar;";
        }else{
            $sql.= " AND c.documento_identidad= :cedula";
        }

        $consulta=Yii::app()->db->createCommand($sql);

        if(isset($cedulaEscolar)){
            $consulta->bindParam(":cedulaEscolar", $cedulaEscolar,PDO::PARAM_STR);
        }else{
            $consulta->bindParam(":cedula", $cedula,PDO::PARAM_INT);
        }
        $consulta->bindParam(":tipo_contancia", $tipoConstancia,PDO::PARAM_INT);
        $resultado=$consulta->queryRow();
        return $resultado;
    }

    /**
     *
     * @param type $datos
     * @return un Arreglo con todos los Datos del Estudiante Necesario para generar la Constancia.
     * @author Jonathan Huaman Fecha 30-04-2015
     * Nota: Este Query Realizar una Busqueda con los parámetros necesarios en base a la tabla matricula.inscripcion_estudiante, en Caso de No conseguir resultados, Se Realiza una Segunda Consulta en Base a la tabla legacy.talumno_acad.
     */
    public function buscarEstudiantePromocion($datos)
    {
        //print_r($datos); die();

        //$tercerNivel=11;   // Id del Registro de Tercer Nivel de la Tabla gplantel.grado.
        //$sextoGrado =18;  // Id del Registro de 6TO Grado de la Tabla gplantel.grado.
        //$periodoId  =15; //  Id del Periodo Escolar ->en este Caso de Prueba año 2013-2014.
        $estudiante = array();
        $sql = "SELECT
                ie.id as inscripcion_estudiante_id,
                e.documento_identidad,
                e.cedula_escolar,
                e.nombres,
                e.apellidos,
                e.fecha_nacimiento,
                e.sexo,
                z.nombre as zona_educativa,
                est.nombre as estado_nac,
                p.nombre as plantel,
                esp.nombre as estado_plantel,
                mup.nombre as municipio_plantel,
                pap.nombre as parroquia_plantel,
                p.direccion as direccion_plantel,
                per.periodo as periodo_escolar

                FROM

                matricula.estudiante as e
                INNER JOIN matricula.inscripcion_estudiante as ie ON (ie.estudiante_id=e.id)
                INNER JOIN gplantel.periodo_escolar as per ON (per.id=ie.periodo_id)
                INNER JOIN gplantel.plantel p ON (p.id=ie.plantel_id)
                INNER JOIN gplantel.zona_educativa z ON (z.id=p.zona_educativa_id)
                INNER JOIN estado as esp ON (esp.id=p.estado_id)
                LEFT JOIN municipio as mup ON (mup.id=p.municipio_id)
                LEFT JOIN parroquia as pap ON (pap.id=p.parroquia_id)
                LEFT JOIN estado as est ON (est.id=e.estado_nac_id)
                WHERE 1=1 AND ie.periodo_id=:periodo_id AND ie.grado_id=:gradoId
                ";

        if($datos[1]!='NULL' && ($datos[1]=='V' || $datos[1]=='E' ) ){
            $sql.=" AND e.documento_identidad= :documentoIdentidad AND (e.nacionalidad=:nacionalidad OR e.tdocumento_identidad=:nacionalidad) ORDER BY ie.id LIMIT 1";
        }
        else{
            $sql.=" AND e.cedula_escolar= :cedulaEscolar ORDER BY ie.id LIMIT 1";
        }

        $buqueda = Yii::app()->db->createCommand($sql);
        if($datos[1]!='NULL' && ($datos[1]=='V' || $datos[1]=='E' ) )
        {
            $buqueda->bindParam(":documentoIdentidad", $datos[0], PDO::PARAM_STR);
            $buqueda->bindParam(":nacionalidad", $datos[1], PDO::PARAM_STR);
        }else{
            $buqueda->bindParam(":cedulaEscolar", $datos[0], PDO::PARAM_STR);
        }
        $buqueda->bindParam(":gradoId", $datos[3], PDO::PARAM_INT);
        $buqueda->bindParam(":periodo_id", $datos[4], PDO::PARAM_INT);
        $estudiante = $buqueda->queryRow();
        return $estudiante;



    }

    /**
     *
     * @param type $seccion_plantel_id
     * @param type $periodo_id
     * @return type arreglo con los Datos Necesarios para Generar el PDF.
     * @author Jonathan Huaman Fecha 30-04-2015.
     * Funcion que Permite Obtener los Datos del Docente  para  Generar la Correspondiente Constancia de Promocion sea 3er Nivel o 6to Grado.
     */
    public function buscarDocentePromocion($seccion_plantel_id,$periodo_id)
    {
        $sql="  select ad.periodo_id,ad.seccion_plantel_id, p.nombres ||' '|| p.apellidos as nombre_docente, p.documento_identidad as cedula_docente
                    FROM personal.asignatura_docente ad
                    inner join gplantel.seccion_plantel sp on ad.seccion_plantel_id=sp.id
                    inner join gplantel.periodo_escolar pe on ad.periodo_id=pe.id
                    inner join gplantel.asignatura a on ad.asignatura_id=a.id
                    left join personal.personal_plantel pp on ad.personal_plantel_id=pp.id
                    left join personal.personal p on pp.personal_id=p.id
                    WHERE pe.id=:periodoId and sp.id= :seccionPlantelId
                    ORDER BY ad.id
                    limit 1
                ";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":periodoId", $periodo_id, PDO::PARAM_INT);
        $buqueda->bindParam(":seccionPlantelId", $seccion_plantel_id, PDO::PARAM_INT);
        $docente = $buqueda->queryRow();
        return $docente;
    }
}
