** Instalación **

Si ya está cargado el Framework Yii en el directorio "yii" puedes ir directo al paso 4.

1.- Descargar el Framework Yii 1.*.*.

2.- Descomprimir el archivo en la raíz del proyecto.

3.- Cambiar el nombre del directorio creado después de la descompresión a "yii".

4.- Crear los directorios "web/assets", ""web/protected/runtime"", "web/public/uploads/fundamentoJuridico", "web/public/uploads/ticket", "web/public/uploads/instructivo", "web/public/upload/titulo", "web/public/downloads", "web/public/downloads/qr/tituloDigital", "web/public/downloads/pdf/tituloDigital" a partir de la raíz del proyecto.

5.1.- Crear una clase Db en el archivo "/protected/protected/configDb.php" (ver archivo "/protected/protected/configDb.php.dist") la misma debe contener lo siguiente:

    <?php

    class Db{

        public static $hostDb = 'localhost'; // Host de Base de Datos
        public static $nameDb = 'layout'; // Nombre de la Base de Datos
        public static $userDb = 'postgres'; // Usuario de la Base de Datos
        public static $passwordDb = 'postgres'; // Password de la base de datos
        public static $portDb = '5432'; // Puerto de la base de datos

        public static $hostGescolar = 'localhost'; // Host de Base de Datos
        public static $nameGescolar = 'gescolar'; // Nombre de la Base de Datos
        public static $userGescolar = 'postgres'; // Usuario de la Base de Datos
        public static $passwordGescolar = 'postgres'; // Password de la base de datos
        public static $portGescolar = '5432'; // Puerto de la base de datos

    
        public static $hostEstadistica = 'localhost'; // Host de Base de Datos
        public static $nameEstadistica = 'estadistica'; // Nombre de la Base de Datos
        public static $userEstadistica = 'postgres'; // Usuario de la Base de Datos
        public static $passwordEstadistica = 'postgres'; // Password de la base de datos
        public static $portEstadistica = '5432'; // Puerto de la base de datos

        public static function getCacheConfig($cache=null){
            if ($cache == 'redis') {
                $cache_config = array(
                    'class' => 'packages.redis.ARedisCache'
                );
            } elseif ($cache == 'apc') {
                $cache_config = array(
                    'class' => 'system.caching.CApcCache'
                );
            } elseif ($cache == 'file') {
                $cache_config = array(
                    'class' => 'system.caching.CFileCache'
                );
            } else {
                $cache_config = array(
                    'class'=>'system.caching.CDbCache',
                );
            }
            return $cache_config;
        }

    }

5.2.- En el archivo de configuración principal /web/protected/config/main.php deben estar definidos los siguientes parámetros:

        'params' => array(
		// this is used in contact page
		'adminEmail'     => 'soporte.gescolar_no_responder@me.gob.ve',
		'adminEmailSend' => 'soporte_gescolar@me.gob.ve',
		'adminGmail'     => 'gescolar.mppe@gmail.com',
		'adminName'      => 'Soporte Gescolar',
                'webUrl'         => 'http://guaicaipuro.me.gob.ve'
		'testing'        => false,
	),

6.- No debes modificar, eliminar o mover los archivos "web/protected/config/main.php", "web/protected/config/configDb.php.dist" o cualquier otro archivo común sin antes consultar al grupo.

7.- Debemos hacer lo posible para cumplir con las siguientes reglas:

  * a.- Vamos a tratar de hacer la aplicación por módulos respetando la filosofía del framework Yii.

  * b.- Los modelos serán los únicos componentes de la aplicación que permanecerán en el directorio models externo. Directorio "web/protected/models" el código del módulo tanto vistas, controladores e incluso componentes debe estar en el directorio "web/protected/modules/xxxx".

  * c.- Debemos respetar los estilos "css" de la plantilla, hacer todo para que la interfaz quede limpia.

  * d.- Las consultas SQL o ORM deben efectuarse en los modelos bajo un método que podamos consultar solo desde los controladores y luego pasemos a las vistas en forma de variable.

  * f.- Evitar en lo posible hacer consultas a la base de datos mientras se pueda, solo debemos hacer consultas cuando sea muy necesario. Si podemos guardar en variables de sesión datos de tablas catálogos hagámoslo. Siempre cuidando la filosofía del framework Yii.

  * g.- Debemos entender que hacer código de calidad y ordenado no implica perdida de tiempo sino ganancia de tiempo.

Para checkear el rol de un usuario podemos hacer uso del siguiente código en nuestros controladores:

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete', 'ajaxList'),
                'expression' => "UserIdentity::checkAccess(array('ROLE_DEVELOPER', 'ROLE_ADMIN'))"
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
8.- Para incluir js propios debemos hacer uso del siguiente metodo (en cualquier parte de la vista)

    <?php 
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/plantel.js',CClientScript::POS_END);
    ?>

9.- Para acceder al Periodo Escolar Actual podemos obtenerlo desde las variables de sesión de Yii, de la siguiente forma:

    Yii::app()->session->get('periodoEscolarActual')